# Changelog, QBO
All notable changes to this project will be documented in this file.


##	[1.0.7] - 2023-03-23

###	Added
	-	NetOff Consolidation
	-	sorting of lineItems in individual orders in case of SKU details are off
	-	dropship chnages in Purchase Credit

###	Fixed
	-	Fixed some issues in EmailAlerts, added Krisha and Priyanka Verma as default receipient
	-	channel Id overriding issue in consol
	-	fixed the issue when order have multiple journals at different times others left pending except one
	-	replacing the column with blank while posting the customer

###	Change
	-	default Item mapping
	-	modified the logStoring functionality in all fetching documents from BP
	-	modifed the description of special item (only for aimee)
	-	chnaged the description of other amazon fees 
	-	modified some code while posting the consol cogs journals (only fetching cogs Orders from main orders table)
	-	unsetting TxnTaxDetails in so,sc,po,pc only for double qbo
	-	overriding the paymentdate for some perticuler channel in salesPayment only for bisuiteers
	-	some chnages in view of other amazon fees







##	[1.0.6] - 2023-01-04

###	Added
	-	GRNI journals
	-	Consol Based on API fields



##	[1.0.6] - 2022-12-23

###	Change
	-	qty format changed to decimal from int
	-	item posting url chnaged (do not update historical txn)
###	Added
	-	netoff consolidation
###	Fixed
	-	contact posting in case of generic and consol
	
	
	
	
##	[1.0.5] - 2022-12-02

###	Change
	-	changes in archiving
	-	add pending contact posting
###	Added
	-	Default item Mapping
	
	
	
	
##	[1.0.4] - 2022-11-21

###	Added
	-	Exchnage Rate new Dev
	-	COGS and amazon Fee archiving
	-	Other Amazon Fees
	-	auto db update
	
	
	
	
##	[1.0.3] - 2022-10-31

###	Added
	-	Refund Receipt individual and consolidation
	-	Tools option for document update
	
###	Change
	-	disable SKU details on SO/SC/PO
	-	send TxnTaxDeatils at the time of editing the SO/SC for Giftcard, issue occured in forfront golf
	-	add nominal mapping in all scenarios in Individual and consol COGS
	
	
	
	
##	[1.0.2] - 2022-09-01

###	Added
	-	Purchase consolidation (batch PO)
	
###	Change
	-	Service product type is chnaged to NonInventory, Minorversion issue is also closed so SKU will work now





##	[1.0.1] - 2022-08-09

###	Added
	-	posting option in consolidation mapping to select either COGS or Orders for posted
	-	maximum lenght of string in Sales/Purchase custom field is 35
### Fixed
	-	default nominal in consol stock adustment based of nominal number range(asset account when 1000-1999, else stock account)
###	Change
	-	coupon amount logic in Buldleproduct case
	
	
	
	
##	[1.0.0] - 2022-05-30

### Added
	-	consolidation based on brightpearl base currency
	-	skip posting if exchangeRate not fetched from QBO (client specific, currently bbhugme)
	-	send either 'Company Name' or 'Customer Full' name in QBO display name, currently we send 'CompanyName - Fullname' (client specific, currently Sullivan)
	-	storing response headers for 'intuid_id' in the database
	-	added code to check parentOrder payment method in case of backorder payment method (OTHER) in individual Sales
	-	added line item sorting in consolidation Sales/Credit
	
### Changed
	-	hide clientID and clientSecret in the qbo account setting interface