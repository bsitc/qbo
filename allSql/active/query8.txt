{
	"addTable": [],
	"addColumn": [],
	"changeColumn": [
		"ALTER TABLE `sales_order` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_order` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_order_archived` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_order_archived` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_order` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_order` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_order_archived` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_order_archived` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_credit_order` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_credit_order` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_credit_order_archived` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `sales_credit_order_archived` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_credit_order` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_credit_order` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_credit_order_archived` CHANGE `totalAmount` `totalAmount` FLOAT(10,2) NULL DEFAULT NULL;",
		"ALTER TABLE `purchase_credit_order_archived` CHANGE `totalTax` `totalTax` FLOAT(10,2) NULL DEFAULT NULL;"
	]
}