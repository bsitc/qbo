<?php
//qbo_demo
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Autoarchive extends CI_Controller {
	public $file_path;
	public function __construct(){
		parent::__construct();
		$this->load->library('mailer');
		$this->defaultPhp	= "8.2";
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
	}
	public function runTaskQBO($taskName = 'ProcessArchive'){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$syncFilePath	= FCPATH.'/jobsoutput/'.date('Y').'/'.date('m').'/'.date('d').'/synclogs/';
				if(!is_dir(($syncFilePath))){
					mkdir(($syncFilePath),0777,true);
					chmod(($syncFilePath), 0777);
				}
				$syncFileName	= $taskName.date('H-i-s').'.logs';
				
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php Autoarchive '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php Autoarchive '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				
				if(is_file($syncFilePath.$syncFileName)){
					$filecontent	= file_get_contents($syncFilePath.$syncFileName);
					if(strlen($filecontent) > 0){
						if((substr_count($filecontent,"Type:        TypeError")) OR (substr_count($filecontent,"Fatal error")) OR (substr_count($filecontent,"Database error")) OR (substr_count($filecontent,"Type:        ValueError")) OR (substr_count($filecontent,"Type:        Error"))){
							$appName		= trim($this->globalConfig['app_name']);
							$Receipents		= trim($this->globalConfig['emailReceipents']);
							if($Receipents == ''){
								$Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,krisha@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com';
							}
							$smtpUsername	= trim($this->globalConfig['smtpUsername']);
							$smtpPassword	= trim($this->globalConfig['smtpPassword']);
							$subject		= 'Alert '.$appName.' - '.$taskName.' Run time error';
							$from			= ['info@bsitc-apps.com' => $this->appName];
							$mailBody		= 'Hi,<br><br><p>Automation not completed successfully, please check the attached cron output file.</p><br><br><br><br><br>Thanks & Regards<br>BSITC Team';
							$this->mailer->send($Receipents, $subject, $mailBody, $from, $syncFilePath.$syncFileName, $smtpUsername, $smtpPassword);
						}
					}
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'ProcessArchive'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php Autoarchive '.$checkTask);	
			foreach($outputs as $output){
				$output	= strtolower($output);
				if(substr_count($output,$fcpath)){
					if(substr_count($output,$checkTask)){
						if(!substr_count($output,'runtask')){ 
							$return	= true;
						}
					}
				}
			}
		}
		return $return;
	}
	public function ProcessArchive(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$autoArchivePeriod	= $this->globalConfig['autoArchivePeriod'];
		if($autoArchivePeriod){
			$checkDate	= strtotime('-'.$autoArchivePeriod.' day');
		}
		else{
			$checkDate	= strtotime('-90 day');
		}
		
		$archiveTypes		= array();
		$archiveTypeDocs	= $this->globalConfig['archiveType'];
		if($archiveTypeDocs){
			$AllDocsType	= explode(",",$archiveTypeDocs);
			foreach($AllDocsType as $AllDocsTypes){
				$archiveTypes[]	= strtolower($AllDocsTypes);
			}
			if($archiveTypes){
				if(in_array('so',$archiveTypes)){
					$this->allOrderArchive($checkDate, 'sales_order', 'sales_order_archived', 'z_old_salesIds');
				}
				if(in_array('sc',$archiveTypes)){
					$this->allOrderArchive($checkDate, 'sales_credit_order', 'sales_credit_order_archived', 'z_old_salesCreditIds');
				}
				if(in_array('po',$archiveTypes)){
					$this->allOrderArchive($checkDate, 'purchase_order', 'purchase_order_archived', 'z_old_purchaseIds');
				}
				if(in_array('pc',$archiveTypes)){
					$this->allOrderArchive($checkDate, 'purchase_credit_order', 'purchase_credit_order_archived', 'z_old_purchaseCreditIds');
				}
				if(in_array('rr',$archiveTypes)){
					$this->allOrderArchive($checkDate, 'refund_receipt', 'refund_receipt_archived', 'z_old_refundReceiptIds');
				}
				if(in_array('cogs',$archiveTypes)){
					$this->cogsArchive($checkDate);
				}
				if(in_array('amazon',$archiveTypes)){
					$this->amazonArchive($checkDate);
				}
				if(in_array('amazonOther',$archiveTypes)){
					$this->amazonOtherArchive($checkDate);
				}
				if(in_array('stock',$archiveTypes)){
					$this->stockJournalArchive($checkDate);
					$this->stockAdjustmentArchive($checkDate);
				}
			}
		}
		$this->logClearing();
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function allOrderArchive($checkDate = '', $mainTable = '', $archivedTable = '', $archivedIdTable = ''){
		if(strlen($checkDate) == 0){return false;}
		if(strlen($mainTable) == 0){return false;}
		if(strlen($archivedTable) == 0){return false;}
		
		$oldTableData	= array();
		$oldOrderIds	= array();
		$batchOrderIds	= array();
		
		$oldTableData		= $this->db->select('orderId')->order_by('orderId','asc')->get_where($mainTable,array('orderId <> ' => ''))->result_array();
		if(!empty($oldTableData)){
			$oldOrderIds		= array_column($oldTableData,'orderId');
			$batchOrderIds		= array_chunk($oldOrderIds,100);
			foreach($batchOrderIds as $batchOrderId){
				$archivedIdDataAll	= array();
				$archivedIdData		= array();
				$archivedDataAll	= array();
				$archivedData		= array();
				$deleteOrderKey		= array();
				$batchOrderInfo		= array();
				if(!empty($batchOrderId)){
					$batchOrderInfo	= $this->db->where_in('orderId' ,$batchOrderId)->get_where($mainTable, array('status <>' => 0))->result_array();
					if(!empty($batchOrderInfo)){
						foreach($batchOrderInfo as $datas){
							if($datas['status'] != 0){
								$rowData		= json_decode($datas['rowData'],true);
								$invoiceDate	= strtotime($rowData['invoices']['0']['taxDate']);
								if($invoiceDate <= $checkDate){
									$deleteOrderKey[]		= $datas['id'];
									unset($datas['id']);
									$archivedDataAll[]		= $datas;
									
									$archivedIdDataAll[]	= array(
										'orderId'				=> $datas['orderId'],
										'invoiceRef'			=> $rowData['invoices']['0']['invoiceReference'],
										'taxDate'				=> $rowData['invoices']['0']['taxDate'],
										'account2Id'			=> $datas['account2Id'],
										'account2ApiId'			=> $datas['createOrderId'],
									);
								}
							}
						}
						$isOrderMoved	= 0;
						if(!empty($archivedDataAll)){
							$archivedData	= array_chunk($archivedDataAll,100,true);
							foreach($archivedData as $archivedDataData){
								$isOrderMoved	= $this->db->insert_batch($archivedTable, $archivedDataData);
							}
							if($deleteOrderKey AND $isOrderMoved){
								$this->db->where_in('id',$deleteOrderKey)->delete($mainTable); 
							}
						}
						if(!empty($archivedIdDataAll)){
							if(strlen($archivedIdTable) > 0){
								$archivedIdData	= array_chunk($archivedIdDataAll,100,true);
								foreach($archivedIdData as $archivedIdDataData){
									$this->db->insert_batch($archivedIdTable, $archivedIdDataData);
								}
							}
						}
					}
				}
			}
		}
	}
	public function cogsArchive($checkDate = ''){
		if(strlen($checkDate) == 0){return false;}
		$batchCogsIds		= array();
		$savedArchivedIDs	= array();
		
		$savedArchivedData	= $this->db->select('orderId')->order_by('orderId','asc')->get_where('sales_order_archived',array('orderId <> ' => ''))->result_array();
		if(!empty($savedArchivedData)){
			$savedArchivedIDs	= array_column($savedArchivedData,'orderId');
			$batchCogsIds		= array_merge($batchCogsIds,$savedArchivedIDs);
		}
		
		$savedArchivedData	= $this->db->select('orderId')->order_by('orderId','asc')->get_where('purchase_order_archived',array('orderId <> ' => ''))->result_array();
		if(!empty($savedArchivedData)){
			$savedArchivedIDs	= array_column($savedArchivedData,'orderId');
			$batchCogsIds		= array_merge($batchCogsIds,$savedArchivedIDs);
		}
		
		$savedArchivedData	= $this->db->select('orderId')->order_by('orderId','asc')->get_where('sales_credit_order_archived',array('orderId <> ' => ''))->result_array();
		if(!empty($savedArchivedData)){
			$savedArchivedIDs	= array_column($savedArchivedData,'orderId');
			$batchCogsIds		= array_merge($batchCogsIds,$savedArchivedIDs);
		}
		
		$savedArchivedData	= $this->db->select('orderId')->order_by('orderId','asc')->get_where('purchase_credit_order_archived',array('orderId <> ' => ''))->result_array();
		if(!empty($savedArchivedData)){
			$savedArchivedIDs	= array_column($savedArchivedData,'orderId');
			$batchCogsIds		= array_merge($batchCogsIds,$savedArchivedIDs);
		}
		
		if(!empty($batchCogsIds)){
			$batchCogsIds	= array_filter($batchCogsIds);
			$batchCogsIds	= array_chunk($batchCogsIds,100);
			foreach($batchCogsIds as $batchCogsIdsTemp){
				$archivedIdDataAll	= array();
				$archivedIdData		= array();
				$archiveBatch		= array();
				$deleteKey			= array();
				$isDataMoved		= 0;
				$mainCogsData		= $this->db->where_in('orderId' ,$batchCogsIdsTemp)->get_where('cogs_journal', array('status <>' => 0))->result_array();
				if(!empty($mainCogsData)){
					foreach($mainCogsData as $mainCogsDataTemp){
						$rowData		= json_decode($mainCogsDataTemp['params'],true);
						$deleteKey[]	= $mainCogsDataTemp['id'];
						unset($mainCogsDataTemp['id']);
						$archiveBatch[]	= $mainCogsDataTemp;
						
						$archivedIdDataAll[]	= array(
							'journalsId'			=> $mainCogsDataTemp['journalsId'],
							'orderId'				=> $mainCogsDataTemp['orderId'],
							'taxDate'				=> $rowData['taxDate'],
							'account2Id'			=> $mainCogsDataTemp['account2Id'],
							'account2ApiId'			=> $mainCogsDataTemp['createdJournalsId'],
						);
						
					}
					if(!empty($archiveBatch)){
						$isDataMoved	= $this->db->insert_batch('cogs_journal_archived', $archiveBatch);
						if($deleteKey AND $isDataMoved){
							$this->db->where_in('id',$deleteKey)->delete('cogs_journal'); 
						}
					}
					if(!empty($archivedIdDataAll)){
						$archivedIdData	= array_chunk($archivedIdDataAll,100,true);
						foreach($archivedIdData as $archivedIdDataData){
							$this->db->insert_batch('z_old_cogsIds', $archivedIdDataData);
						}
					}
				}
			}
		}
	}
	public function amazonArchive($checkDate = ''){
		if(strlen($checkDate) == 0){return false;}
		$batchAmazonIds		= array();
		$savedArchivedIDs	= array();
		
		$savedArchivedData		= $this->db->select('orderId')->order_by('orderId','asc')->get_where('sales_order_archived',array('orderId <> ' => ''))->result_array();
		if(!empty($savedArchivedData)){
			$savedArchivedIDs	= array_column($savedArchivedData,'orderId');
			$batchAmazonIds		= array_merge($batchAmazonIds,$savedArchivedIDs);
			$batchAmazonIds		= array_filter($batchAmazonIds);
			$batchAmazonIds		= array_chunk($batchAmazonIds,100);
		}
		
		if(!empty($batchAmazonIds)){
			foreach($batchAmazonIds as $batchAmazonIdsTemp){
				$archivedIdDataAll	= array();
				$archivedIdData		= array();
				$archiveAmzInsert	= array();
				$deleteOrderKey		= array();
				$IsDataMoved		= 0;
				$mainAmzData		= $this->db->where_in('orderId' ,$batchAmazonIdsTemp)->get_where('amazon_ledger', array('status <>' => 0))->result_array();
				
				if(!empty($mainAmzData)){
					foreach($mainAmzData as $mainAmzDataTemp){
						$rowData			= json_decode($mainAmzDataTemp['params'],true);
						$deleteOrderKey[]	= $mainAmzDataTemp['id'];
						unset($mainAmzDataTemp['id']);
						$archiveAmzInsert[]	= $mainAmzDataTemp;
						
						$archivedIdDataAll[]	= array(
							'journalsId'			=> $mainAmzDataTemp['journalsId'],
							'orderId'				=> $mainAmzDataTemp['orderId'],
							'taxDate'				=> $rowData['taxDate'],
							'account2Id'			=> $mainAmzDataTemp['account2Id'],
							'account2ApiId'			=> $mainAmzDataTemp['createdJournalsId'],
						);
					}
					if(!empty($archiveAmzInsert)){
						$IsDataMoved	= $this->db->insert_batch('amazon_ledger_archived', $archiveAmzInsert);
						if($deleteOrderKey AND $IsDataMoved){
							$this->db->where_in('id',$deleteOrderKey)->delete('amazon_ledger'); 
						}
					}
					if(!empty($archivedIdDataAll)){
						$archivedIdData	= array_chunk($archivedIdDataAll,100,true);
						foreach($archivedIdData as $archivedIdDataData){
							$this->db->insert_batch('z_old_amazonIds', $archivedIdDataData);
						}
					}
				}
			}
		}
	}
	public function amazonOtherArchive($checkDate = ''){
		if(strlen($checkDate) == 0){return false;}
		
		$savedJournalIds	= array();
		$saveJournalsData	= $this->db->select('journalId')->order_by('journalId','asc')->get_where('amazonFeesOther',array('journalId <> ' => ''))->result_array();
		if(!empty($saveJournalsData)){
			$savedJournalIds	= array_column($saveJournalsData,'journalId');
			$savedJournalIds	= array_filter($savedJournalIds);
			$savedJournalIds	= array_chunk($savedJournalIds,100);
			
			if(!empty($savedJournalIds)){
				foreach($savedJournalIds as $savedJournalIdsTemp){
					$archivedIdDataAll	= array();
					$archivedIdData		= array();
					$deleteKeys			= array();
					$archiveBatches		= array();
					$saveJournalsData	= $this->db->where_in('journalId' ,$savedJournalIdsTemp)->get_where('amazonFeesOther', array('status <>' => 0))->result_array();
					if(!empty($saveJournalsData)){
						foreach($saveJournalsData as $datas){
							if($datas['status'] != 0){
								$rowData		= json_decode($datas['params'],true);
								$invoiceDate	= strtotime($rowData['taxDate']);
								if($invoiceDate <= $checkDate){
									$deleteKeys[]	= $datas['id'];
									unset($datas['id']);
									$archiveBatches[]	= $datas;
									
									$archivedIdDataAll[]	= array(
										'journalId'				=> $datas['journalId'],
										'taxDate'				=> $rowData['taxDate'],
										'account2Id'			=> $datas['account2Id'],
										'account2ApiId'			=> $datas['qboTxnId'],
									);
									
								}
							}
						}
						$isDataMoved	= 0;
						if(!empty($archiveBatches)){
							$archiveBatch	= array_chunk($archiveBatches,100,true);
							foreach($archiveBatch as $archiveBatchTemp){
								$isDataMoved		= $this->db->insert_batch('amazonFeesOther_archived', $archiveBatchTemp);
							}
							if($deleteKeys AND $isDataMoved){
								$this->db->where_in('id',$deleteKeys)->delete('amazonFeesOther'); 
							}
						}
						if(!empty($archivedIdDataAll)){
							$archivedIdData	= array_chunk($archivedIdDataAll,100,true);
							foreach($archivedIdData as $archivedIdDataData){
								$this->db->insert_batch('z_old_amazonFeeOtherIds', $archivedIdDataData);
							}
						}
					}
				}
			}
		}
	}
	public function stockJournalArchive($checkDate = ''){
		if(strlen($checkDate) == 0){return false;}
		
		$savedJournalIds	= array();
		$saveJournalsData	= $this->db->select('journalId')->order_by('journalId','asc')->get_where('stock_journals',array('journalId <> ' => ''))->result_array();
		if(!empty($saveJournalsData)){
			$savedJournalIds	= array_column($saveJournalsData,'journalId');
			$savedJournalIds	= array_filter($savedJournalIds);
			$savedJournalIds	= array_chunk($savedJournalIds,100);
			
			if(!empty($savedJournalIds)){
				foreach($savedJournalIds as $savedJournalIdsTemp){
					$archivedIdDataAll	= array();
					$archivedIdData		= array();
					$deleteKeys			= array();
					$archiveBatches		= array();
					$saveJournalsData	= $this->db->where_in('journalId' ,$savedJournalIdsTemp)->get_where('stock_journals', array('status <>' => 0))->result_array();
					if(!empty($saveJournalsData)){
						foreach($saveJournalsData as $datas){
							if($datas['status'] != 0){
								$rowData		= json_decode($datas['params'],true);
								$invoiceDate	= $rowData['taxDate'];
								if($invoiceDate <= $checkDate){
									$deleteKeys[]	= $datas['id'];
									unset($datas['id']);
									$archiveBatches[]	= $datas;
									
									$archivedIdDataAll[]	= array(
										'journalId'				=> $datas['journalId'],
										'taxDate'				=> $rowData['taxDate'],
										'account2Id'			=> $datas['account2Id'],
										'account2ApiId'			=> $datas['created_journalId'],
									);
								}
							}
						}
						$isDataMoved	= 0;
						if(!empty($archiveBatches)){
							$archiveBatch	= array_chunk($archiveBatches,100,true);
							foreach($archiveBatch as $archiveBatchTemp){
								$isDataMoved		= $this->db->insert_batch('stock_journals_archived', $archiveBatchTemp);
							}
							if($deleteKeys AND $isDataMoved){
								$this->db->where_in('id',$deleteKeys)->delete('stock_journals'); 
							}
						}
						if(!empty($archivedIdDataAll)){
							$archivedIdData	= array_chunk($archivedIdDataAll,100,true);
							foreach($archivedIdData as $archivedIdDataData){
								$this->db->insert_batch('z_old_stockJournalIds', $archivedIdDataData);
							}
						}
					}
				}
			}
		}
	}
	public function stockAdjustmentArchive($checkDate = ''){
		if(strlen($checkDate) == 0){return false;}
		
		$savedStocksIds		= array();
		$savedStocksData	= $this->db->select('orderId')->order_by('orderId','asc')->get_where('stock_adjustment',array('orderId <> ' => ''))->result_array();
		if(!empty($savedStocksData)){
			$savedStocksIds	= array_column($savedStocksData,'orderId');
			$savedStocksIds	= array_filter($savedStocksIds);
			$savedStocksIds	= array_chunk($savedStocksIds,100);
			
			if(!empty($savedStocksIds)){
				foreach($savedStocksIds as $savedStocksIdsTemp){
					$archivedIdDataAll	= array();
					$archivedIdData		= array();
					$deleteKeys			= array();
					$archiveBatches		= array();
					$savedStocksData	= $this->db->where_in('orderId' ,$savedStocksIdsTemp)->get_where('stock_adjustment', array('status <>' => 0))->result_array();
					if(!empty($savedStocksData)){
						foreach($savedStocksData as $datas){
							if($datas['status'] != 0){
								$rowData		= json_decode($datas['rowData'],true);
								$invoiceDate	= $rowData['updatedOn'];
								if($invoiceDate <= $checkDate){
									$deleteKeys[]	= $datas['id'];
									unset($datas['id']);
									$archiveBatches[]	= $datas;
									
									$archivedIdDataAll[]	= array(
										'goodsMovementId'		=> $datas['orderId'],
										'taxDate'				=> $rowData['updatedOn'],
										'account2Id'			=> $datas['account2Id'],
										'account2ApiId'			=> $datas['createdOrderId'],
									);
								}
							}
						}
						$isDataMoved	= 0;
						if(!empty($archiveBatches)){
							$archiveBatch	= array_chunk($archiveBatches,100,true);
							foreach($archiveBatch as $archiveBatchTemp){
								$isDataMoved		= $this->db->insert_batch('stock_adjustment_archive', $archiveBatchTemp);
							}
							if($deleteKeys AND $isDataMoved){
								$this->db->where_in('id',$deleteKeys)->delete('stock_adjustment'); 
							}
						}
						if(!empty($archivedIdDataAll)){
							$archivedIdData	= array_chunk($archivedIdDataAll,100,true);
							foreach($archivedIdData as $archivedIdDataData){
								$this->db->insert_batch('z_old_stockAdjustmentIds', $archivedIdDataData);
							}
						}
					}
				}
			}
		}
	}
	
	public function logClearing(){
		$dir	= FCPATH . 'logs';
		if(is_dir($dir)){
			$this->listFolderFiles($dir);
		}
	}
	function listFolderFiles($dir){
		foreach(new DirectoryIterator($dir) as $fileInfo){
			if(!$fileInfo->isDot()){
				if(!is_dir($fileInfo->getPathname())){
					$FileName		= $fileInfo->getPathname();
					$lastModified	= filemtime($FileName);
					if($lastModified < strtotime("-30 days")){
						unlink($FileName);
					}
				}
				if($fileInfo->isDir()){
					$this->listFolderFiles($fileInfo->getPathname());
				}
			}
		}
	}
}