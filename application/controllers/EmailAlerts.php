<?php
//QBO EmailAlerts
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class EmailAlerts extends CI_Controller {
	public $ci;
	public function __construct(){
		parent::__construct();
		$this->load->library('mailer');
		$this->clientcode	= $this->config->item('clientcode');
		$this->ci			= &get_instance();
		$this->appName		= trim($this->globalConfig['app_name']);
		$this->from			= array('info@bsitc-apps.com' => $this->appName);
		$this->Receipents	= trim($this->globalConfig['emailReceipents']);
		$this->smtpUsername	= trim($this->globalConfig['smtpUsername']);
		$this->smtpPassword	= trim($this->globalConfig['smtpPassword']);
		$this->defaultPhp	= "8.2";
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
		if($this->Receipents == ''){
			$this->Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,krisha@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com';
		}
	}
	public function runTaskQBO($taskName = 'processEmailAlerts'){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$syncFilePath	= FCPATH.'/jobsoutput/'.date('Y').'/'.date('m').'/'.date('d').'/synclogs/';
				if(!is_dir(($syncFilePath))){
					mkdir(($syncFilePath),0777,true);
					chmod(($syncFilePath), 0777);
				}
				$syncFileName	= $taskName.date('H-i-s').'.logs';
				
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php EmailAlerts '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php EmailAlerts '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				
				if(is_file($syncFilePath.$syncFileName)){
					$filecontent	= file_get_contents($syncFilePath.$syncFileName);
					if(strlen($filecontent) > 0){
						if((substr_count($filecontent,"Type:        TypeError")) OR (substr_count($filecontent,"Fatal error")) OR (substr_count($filecontent,"Database error")) OR (substr_count($filecontent,"Type:        ValueError")) OR (substr_count($filecontent,"Type:        Error"))){
							$appName		= trim($this->globalConfig['app_name']);
							$Receipents		= trim($this->globalConfig['emailReceipents']);
							if($Receipents == ''){
								$Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,krisha@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com';
							}
							$smtpUsername	= trim($this->globalConfig['smtpUsername']);
							$smtpPassword	= trim($this->globalConfig['smtpPassword']);
							$subject		= 'Alert '.$appName.' - '.$taskName.' Run time error';
							$from			= ['info@bsitc-apps.com' => $this->appName];
							$mailBody		= 'Hi,<br><br><p>Automation not completed successfully, please check the attached cron output file.</p><br><br><br><br><br>Thanks & Regards<br>BSITC Team';
							$this->mailer->send($Receipents, $subject, $mailBody, $from, $syncFilePath.$syncFileName, $smtpUsername, $smtpPassword);
						}
					}
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'processEmailAlerts'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php EmailAlerts '.$checkTask);	
			foreach($outputs as $output){
				$output	= strtolower($output);
				if(substr_count($output,$fcpath)){
					if(substr_count($output,$checkTask)){
						if(!substr_count($output,'runtask')){ 
							$return	= true;
						}
					}
				}
			}
		}
		return $return;
	}
	public function processEmailAlerts(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$noFetchOrderData		= '';
		$pendingCogsJournal		= '';
		$noPostOrdersData		= '';
		$duplicatePayments		= '';
		$pendingOrdersData		= '';
		$pendingPaymentsData	= '';
		$noAmazonfeeFetchData	= '';
		$FilePath				= '';
		$FilePathArray			= array();
		
		
		$noFetchOrderData		= $this->noDataFetch();
		$noPostOrdersData		= $this->noDataPost();
		$duplicatePayments		= $this->duplicatePayments();
		$pendingOrdersData		= $this->pendingOrders();
		$pendingPaymentsData	= $this->pendingPaymentAlert();
		if($this->globalConfig['enableamazonfee']){
			$noAmazonfeeFetchData	= $this->noAmazonfeeFetch();
		}
		if($this->globalConfig['enableCOGSJournals']){
			$pendingCogsJournal		= $this->pendingCogsAlert();
		}
		
		if($noFetchOrderData OR $noPostOrdersData OR $duplicatePayments OR $pendingOrdersData OR $pendingPaymentsData OR $noAmazonfeeFetchData OR $pendingCogsJournal){
			$subject	= '"'.$this->appName.'" Process Report';
			$mainBody	= '';
			$mainBody	= 'Hi,<br>Here are the process Report for '.$this->appName.'<br>';
			if($noFetchOrderData){
				$mainBody	.= $noFetchOrderData;
			}
			if($noPostOrdersData){
				$mainBody	.= $noPostOrdersData;
			}
			if($noAmazonfeeFetchData){
				$mainBody	.= $noAmazonfeeFetchData;
			}
			if($duplicatePayments){
				$mainBody	.= $duplicatePayments;
			}
			if($pendingCogsJournal){
				$mainBody	.= $pendingCogsJournal;
			}
			if((is_array($pendingPaymentsData)) AND (isset($pendingPaymentsData['OrderIds']))){
				$mainBody	.= $pendingPaymentsData['OrderIds'];
			}
			if((is_array($pendingOrdersData)) AND (isset($pendingOrdersData['mailBody']))){
				$mainBody	.= $pendingOrdersData['mailBody'];
			}
			if(php_sapi_name() === 'cli'){
				if((is_array($pendingPaymentsData)) AND (isset($pendingPaymentsData['PaymentsErrorLogs']))){
					$ErrorLogFile		= str_replace(' ', '', (strtolower($this->appName))).'_payments_error_log_'.date('Ymd').'.csv';
					$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR ;
					if(!is_dir($FilePath)){
						mkdir($FilePath,0777,true);
						chmod(dirname($FilePath), 0777);
					}
					$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR .$ErrorLogFile;
					$FilePathArray[]	= $FilePath;
					$fp					= fopen($FilePath, 'w');
					$header				= array('OrderType', 'OrderId', 'Message');
					fputcsv($fp, $header);
					foreach($pendingPaymentsData['PaymentsErrorLogs'] as $orderType => $Logs){
						foreach($Logs as $orderIds => $Messagess){
							$AllError	= array();
							$row		= array();
							foreach($Messagess as $Message){
								$AllError[]	= $Message['Message'].', '.$Message['Detail'];
							}
							if($AllError){
								$row	= array($orderType,$orderIds,implode("~~",$AllError));
							}
							fputcsv($fp, $row);
						}
					}
					fclose($fp);
				}
				if((is_array($pendingOrdersData)) AND (isset($pendingOrdersData['ErrorLogs']))){
					$ErrorLogFile		= str_replace(' ', '', (strtolower($this->appName))).'_error_log_'.date('Ymd').'.csv';
					$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR ;
					if(!is_dir($FilePath)){
						mkdir($FilePath,0777,true);
						chmod(dirname($FilePath), 0777);
					}
					$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR .$ErrorLogFile;
					$FilePathArray[]	= $FilePath;
					$fp					= fopen($FilePath, 'w');
					$header				= array('OrderType', 'OrderId', 'Message');
					fputcsv($fp, $header);
					foreach($pendingOrdersData['ErrorLogs'] as $orderType => $Logs){
						foreach($Logs as $orderIds => $Messagess){
							$AllError	= array();
							$row		= array();
							foreach($Messagess as $Message){
								$AllError[]	= $Message['Message'].', '.$Message['Detail'];
							}
							if($AllError){
								$row	= array($orderType,$orderIds,implode("~~",$AllError));
							}
							fputcsv($fp, $row);
						}
					}
					fclose($fp);
				}
			}
			$mainBody	.= '<br>Thanks & Regards<br>BSITC Team';
			if($FilePathArray){
				$this->mailer->send($this->Receipents, $subject, $mainBody, $this->from, $FilePathArray, $this->smtpUsername, $this->smtpPassword);
			}
			else{
				$this->mailer->send($this->Receipents, $subject, $mainBody, $this->from, '', $this->smtpUsername, $this->smtpPassword);
			}
		}
		if(php_sapi_name() === 'cli'){
			$this->db->truncate('ci_sessions');
		}
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
	}
	public function noAmazonfeeFetch(){
		$AmazonFeeFetchData	= $this->db->select_max('fetchDate')->get_where('amazon_ledger',array('journalsId <>' => ''))->row_array();
		$isFetched			= 1;
		$fetchBody	= '';
		if($AmazonFeeFetchData){
			if(strtotime($AmazonFeeFetchData['fetchDate']) < strtotime('-16 days')){
				$isFetched	= 0;
				$fetchBody	= '';
				$fetchBody	.= '<br><b># No Amazon Fees has been fetched in last 15 days.</b><br>';
			}
		}
		if(!$isFetched){
			return $fetchBody;
		}
	}
	public function noDataFetch(){
		$FetchCheckDate	= date('Y-m-d',strtotime('-1 day'));
		$SOFetchedData	= $this->db->where("date(`InvoicedTime`) >= ","date('".$FetchCheckDate."')",false)->get_where('sales_order')->result_array();
		$isFetched		= 1;
		if(!$SOFetchedData){
			$isFetched	= 0;
			$fetchBody	= '';
			$fetchBody	.= '<br><b># No SalesOrder has been fetched in last 1 day.</b><br>';
		}
		if(!$isFetched){
			return $fetchBody;
		}
	}
	public function noDataPost(){
		$PostCheckDate	= date('Y-m-d',strtotime('-2 day'));
		$SOPostedData	= $this->db->where("date(`PostedTime`) >= ","date('".$PostCheckDate."')",false)->get_where('sales_order')->result_array();
		$isPosted		= 1;
		if(!$SOPostedData){
			$isPosted	= 0;
			$PostBody	= '';
			$PostBody	.= '<br><b># No SalesOrder has been posted in last 2 days.</b><br>';
		}
		if(!$isPosted){
			return $PostBody;
		}
	}
	public function duplicatePayments(){
		$this->brightpearl->reInitialize();
		$SaveOrdersInDB		= array();
		$SalesOrdersData	= $this->ci->db->select('orderId,totalAmount')->get_where('sales_order',array('paymentDetails <>' => '', 'status <>' => '0'))->result_array();
		foreach($SalesOrdersData as $SalesOrdersDatas){
			$SaveOrdersInDB['SO'][$SalesOrdersDatas['orderId']]		= $SalesOrdersDatas;
		}
		$SalesCreditData	= $this->ci->db->select('orderId,totalAmount')->get_where('sales_credit_order',array('paymentDetails <>' => '', 'status <>' => '0'))->result_array();
		foreach($SalesCreditData as $SalesCreditDatas){
			$SaveOrdersInDB['SC'][$SalesCreditDatas['orderId']]		= $SalesCreditDatas;
		}
		$PurchaseOrderData	= $this->ci->db->select('orderId,totalAmount')->get_where('purchase_order',array('paymentDetails <>' => '', 'status <>' => '0'))->result_array();
		foreach($PurchaseOrderData as $PurchaseOrderDatas){
			$SaveOrdersInDB['PO'][$PurchaseOrderDatas['orderId']]	= $PurchaseOrderDatas;
		}
		$PurchaseCreditData	= $this->ci->db->select('orderId,totalAmount')->get_where('purchase_credit_order',array('paymentDetails <>' => '', 'status <>' => '0'))->result_array();
		foreach($PurchaseCreditData as $PurchaseCreditDatas){
			$SaveOrdersInDB['PC'][$PurchaseCreditDatas['orderId']]	= $PurchaseCreditDatas;
		}
		
		$DuplicateOrderIds		= array();
		//CHECK DUPLICATE PYAMENTS FOR SO/SC
		$SOSCpaymentResponses	= array();
		foreach($this->brightpearl->accountDetails as  $account1Id => $accountDetails){
			$url		= '/accounting-service/customer-payment-search?createdOn='.date('Y-m-d',strtotime('-1 days')).'/';
			$response	= $this->brightpearl->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
			if($response['results']){
				$SOSCpaymentResponses[]	= $response;
				while($response['metaData']['morePagesAvailable']){
					$url1		= $url . '&firstResult=' . $response['metaData']['lastResult'];
					$response	= $this->brightpearl->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if($response['results']){
						$SOSCpaymentResponses[]	= $response;
					}
				}
			}
		}
		$AllSOSCOrdersPayments	= array();
		foreach($SOSCpaymentResponses as $SOSCpaymentResponse){
			$headers			= array_column($SOSCpaymentResponse['metaData']['columns'],'name');
			foreach($SOSCpaymentResponse['results'] as $results){
				$row			= array_combine($headers,$results);
				$AllSOSCOrdersPayments[$row['orderId']][$row['paymentType']][$row['amountPaid']][]	= $row;
			}
		}
		
		foreach($AllSOSCOrdersPayments as $orderId => $AllSOSCOrdersPaymentsType){
			foreach($AllSOSCOrdersPaymentsType as $Type => $AllSOSCOrdersPaymentsTypeAmt){
				foreach($AllSOSCOrdersPaymentsTypeAmt as $Amt => $AllSOSCOrdersPaymentsTypeAmtData){
					if($Type == 'RECEIPT'){
						$ReceiptCount	= count($AllSOSCOrdersPaymentsTypeAmtData);
						if($ReceiptCount > 1){
							if(isset($AllSOSCOrdersPayments[$orderId]['PAYMENT'][$Amt])){
								$PaymentCount	= count($AllSOSCOrdersPayments[$orderId]['PAYMENT'][$Amt]);
								if($PaymentCount > 0){
									if($SaveOrdersInDB['SO'][$orderId]){
										$DuplicateOrderIds['SO'][]	= $orderId;
									}
									elseif($SaveOrdersInDB['SC'][$orderId]){
										$DuplicateOrderIds['SC'][]	= $orderId;
									}
								}
							}
						}
						if($ReceiptCount > 1){
							if($SaveOrdersInDB['SO'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllSOSCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['SO'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['SO'][]	= $orderId;
								}
							}
							if($SaveOrdersInDB['SC'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllSOSCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['SC'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['SC'][]	= $orderId;
								}
							}
						}
					}
					elseif($Type == 'PAYMENT'){
						$PaymentCount	= count($AllSOSCOrdersPaymentsTypeAmtData);
						if($PaymentCount > 1){
							if(isset($AllSOSCOrdersPayments[$orderId]['RECEIPT'][$Amt])){
								$ReceiptCount	= count($AllSOSCOrdersPayments[$orderId]['RECEIPT'][$Amt]);
								if($ReceiptCount > 0){
									if($SaveOrdersInDB['SO'][$orderId]){
										$DuplicateOrderIds['SO'][]	= $orderId;
									}
									elseif($SaveOrdersInDB['SC'][$orderId]){
										$DuplicateOrderIds['SC'][]	= $orderId;
									}
								}
							}
						}
						if($PaymentCount > 1){
							if($SaveOrdersInDB['SO'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllSOSCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['SO'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['SO'][]	= $orderId;
								}
							}
							if($SaveOrdersInDB['SC'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllSOSCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['SC'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['SC'][]	= $orderId;
								}
							}
						}
					}
				}
			}
		}
		
		//CHECK DUPLICATE PYAMENTS FOR PO/PC
		$POPCpaymentResponses	= array();
		foreach($this->brightpearl->accountDetails as  $account1Id => $accountDetails){
			$url		= '/accounting-service/supplier-payment-search?createdOn='.date('Y-m-d',strtotime('-1 days')).'/';
			$response	= $this->brightpearl->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
			if($response['results']){
				$POPCpaymentResponses[]	= $response;
				while($response['metaData']['morePagesAvailable']){
					$url1		= $url . '&firstResult=' . $response['metaData']['lastResult'];
					$response	= $this->brightpearl->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if($response['results']){
						$POPCpaymentResponses[]	= $response;
					}
				}
			}
		}
		$AllPOPCOrdersPayments	= array();
		foreach($POPCpaymentResponses as $POPCpaymentResponse){
			$headers			= array_column($POPCpaymentResponse['metaData']['columns'],'name');
			foreach($POPCpaymentResponse['results'] as $results){
				$row			= array_combine($headers,$results);
				$AllPOPCOrdersPayments[$row['orderId']][$row['paymentType']][$row['amountPaid']][]	= $row;
			}
		}
		
		foreach($AllPOPCOrdersPayments as $orderId => $AllPOPCOrdersPaymentsType){
			foreach($AllPOPCOrdersPaymentsType as $Type => $AllPOPCOrdersPaymentsTypeAmt){
				foreach($AllPOPCOrdersPaymentsTypeAmt as $Amt => $AllPOPCOrdersPaymentsTypeAmtData){
					if($Type == 'RECEIPT'){
						$ReceiptCount	= count($AllPOPCOrdersPaymentsTypeAmtData);
						if($ReceiptCount > 1){
							if(isset($AllPOPCOrdersPayments[$orderId]['PAYMENT'][$Amt])){
								$PaymentCount	= count($AllPOPCOrdersPayments[$orderId]['PAYMENT'][$Amt]);
								if($PaymentCount > 0){
									if($SaveOrdersInDB['PO'][$orderId]){
										$DuplicateOrderIds['PO'][]	= $orderId;
									}
									elseif($SaveOrdersInDB['PC'][$orderId]){
										$DuplicateOrderIds['PC'][]	= $orderId;
									}
								}
							}
						}
						if($ReceiptCount > 1){
							if($SaveOrdersInDB['PO'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllPOPCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['PO'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['PO'][]	= $orderId;
								}
							}
							if($SaveOrdersInDB['PC'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllPOPCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['PC'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['PC'][]	= $orderId;
								}
							}
						}
					}
					elseif($Type == 'PAYMENT'){
						$PaymentCount	= count($AllPOPCOrdersPaymentsTypeAmtData);
						if($PaymentCount > 1){
							if(isset($AllPOPCOrdersPayments[$orderId]['RECEIPT'][$Amt])){
								$ReceiptCount	= count($AllPOPCOrdersPayments[$orderId]['RECEIPT'][$Amt]);
								if($ReceiptCount > 0){
									if($SaveOrdersInDB['PO'][$orderId]){
										$DuplicateOrderIds['PO'][]	= $orderId;
									}
									elseif($SaveOrdersInDB['PC'][$orderId]){
										$DuplicateOrderIds['PC'][]	= $orderId;
									}
								}
							}
						}
						if($PaymentCount > 1){
							if($SaveOrdersInDB['PO'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllPOPCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['PO'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['PO'][]	= $orderId;
								}
							}
							if($SaveOrdersInDB['PC'][$orderId]){
								$totalBPAmt	= array_sum(array_column($AllPOPCOrdersPaymentsTypeAmtData, 'amountPaid'));
								if($SaveOrdersInDB['PC'][$orderId]['totalAmount'] < $totalBPAmt){
									$DuplicateOrderIds['PC'][]	= $orderId;
								}
							}
						}
					}
				}
			}
		}
		$DuplicatePayentsOrderBody	= '';
		if($DuplicateOrderIds){
			$DuplicatePayentsOrderBody	= '<br><b># Following Orders have duplicate payments in Brightpearl -:</b><br>';
			$DuplicatePayentsOrderBody	.= '<table border=1><thead><tr><th>OrderType</th><th>OrderIds</th></tr></thead><tbody>';
			if($DuplicateOrderIds['SO']){
				$DuplicateOrderIds['SO']	= array_filter($DuplicateOrderIds['SO']);
				$DuplicateOrderIds['SO']	= array_unique($DuplicateOrderIds['SO']);
				$DuplicatePayentsOrderBody	.= '<tr><td>Sales Order('.count($DuplicateOrderIds['SO']).')</td><td>'.implode(", ",$DuplicateOrderIds['SO']).'</td></tr>';
			}
			if($DuplicateOrderIds['SC']){
				$DuplicateOrderIds['SC']	= array_filter($DuplicateOrderIds['SC']);
				$DuplicateOrderIds['SC']	= array_unique($DuplicateOrderIds['SC']);
				$DuplicatePayentsOrderBody	.= '<tr><td>Sales Credit('.count($DuplicateOrderIds['SC']).')</td><td>'.implode(", ",$DuplicateOrderIds['SC']).'</td></tr>';
			}
			if($DuplicateOrderIds['PO']){
				$DuplicateOrderIds['PO']	= array_filter($DuplicateOrderIds['PO']);
				$DuplicateOrderIds['PO']	= array_unique($DuplicateOrderIds['PO']);
				$DuplicatePayentsOrderBody	.= '<tr><td>Purchase Order('.count($DuplicateOrderIds['PO']).')</td><td>'.implode(", ",$DuplicateOrderIds['PO']).'</td></tr>';
			}
			if($DuplicateOrderIds['PC']){
				$DuplicateOrderIds['PC']	= array_filter($DuplicateOrderIds['PC']);
				$DuplicateOrderIds['PC']	= array_unique($DuplicateOrderIds['PC']);
				$DuplicatePayentsOrderBody	.= '<tr><td>Purchase Credit('.count($DuplicateOrderIds['PC']).')</td><td>'.implode(", ",$DuplicateOrderIds['PC']).'</td></tr>';
			}
			$DuplicatePayentsOrderBody	.= '</tbody></table>';
		}
		if($DuplicatePayentsOrderBody){
			return	$DuplicatePayentsOrderBody;
		}
	}
	public function pendingCogsAlert(){
		$FetchCheckDate	= date('Y-m-d',strtotime('-2 days'));
		$Pendingbody	= '';
		$alljournalData		= $this->db->order_by('orderId','desc')->select('orderId,journalsId,journalTypeCode')->group_start()->where("date(`fetchdate`) < ","date('".$FetchCheckDate."')",false)->or_group_start()->where('fetchdate', NULL)->group_end()->group_end()->get_where('cogs_journal',array('status' => '0'))->result_array();
		if($alljournalData){
			$allPendingCogs	= array();
			$Pendingbody	.= '<br><b># Pending COGS Journals -:</b><br>';
			$Pendingbody	.= '<table  width="100%" border=1 style="border-collapse:collapse"><thead><tr><th>COGS Type</th><th>OrderIds</th></tr></thead><tbody>';
			
			foreach($alljournalData as $alljournalDataTemp){
				$allPendingCogs[$alljournalDataTemp['journalTypeCode']][]	= $alljournalDataTemp['orderId'];
			}
			if($allPendingCogs){
				foreach($allPendingCogs as $cogsType => $allPendingCogsTemp){
					$Pendingbody	.= '<tr><td>'.$cogsType.'('.count($allPendingCogsTemp).')</td><td>'.implode(", ",$allPendingCogsTemp).'</td></tr>';
				}
				$Pendingbody	.= '</tbody></table>';
			}
		}
		return $Pendingbody;
	}
	public function pendingOrders(){
		$FetchCheckDate	= date('Y-m-d',strtotime('-2 days'));
		$PendingBodyData	= array();
		
		$stuckedSO		= $this->db->order_by('orderId','desc')->select('orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$FetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('sales_order',array('status' => 0))->result_array();
		$stuckedSC		= $this->db->order_by('orderId','desc')->select('orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$FetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('sales_credit_order',array('status' => 0))->result_array();
		$stuckedPO		= $this->db->order_by('orderId','desc')->select('orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$FetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('purchase_order',array('status' => 0))->result_array();
		$stuckedPC		= $this->db->order_by('orderId','desc')->select('orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$FetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('purchase_credit_order',array('status' => 0))->result_array();
		$isPending		= 0;
		
		if($stuckedSO OR $stuckedSC OR $stuckedPO OR $stuckedPC){
			$AllErroLogs	= array();
			$isPending		= 1;
			$Pendingbody	= '';
			$Pendingbody	.= '<br><b># Pending Order from more than 48 hours -:</b><br>';
			
			$Pendingbody	.= '<table  width="100%" border=1 style="border-collapse:collapse"><thead><tr><th>OrderType</th><th>Message</th><th>OrderIds</th></tr></thead><tbody>';
			
			if($stuckedSO){
				$pendingSO		= array();
				foreach($stuckedSO as $stuckedSOs){
					if((strlen($stuckedSOs['message']) > 0) or ($stuckedSOs['uninvoiced'])){
						if(substr_count(strtolower($stuckedSOs['message']),'customer not sent')){
							$pendingSO['Customer Not Sent'][]						= $stuckedSOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSOs['message']),'missing sku')){
							$pendingSO['Missing SKU'][]								= $stuckedSOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSOs['message']),'dropshippo')){
							$pendingSO['Dropship PO not sent yet'][]				= $stuckedSOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSOs['message']),'docnumber is already exists in qbo')){
							$pendingSO['DocNumber is already exists in QBO'][]		= $stuckedSOs['orderId'];
						}
						elseif($stuckedSOs['uninvoiced']){
							$pendingSO['Order Un-invoiced in Brightpearl'][]		= $stuckedSOs['orderId'];
						}
						else{
							$pendingSO['Others'][]	= $stuckedSOs['orderId'];	
						}
					}
					else{
						$pendingSO['Others'][]	= $stuckedSOs['orderId'];	
					}
					if($stuckedSOs['createdRowData']){
						$createdRowData	= json_decode($stuckedSOs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Fault']['Error']){
							$AllErroLogs['SO'][$stuckedSOs['orderId']]	= $createdRowData['Response data	: ']['Fault']['Error'];
						}
					}
				}
				if($pendingSO){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>SalesOrder</td></td>';
					if(isset($pendingSO['Customer Not Sent'])){
						$notSet	= 0;
						$Pendingbody	.= '<td>Customer Not Sent('.count($pendingSO['Customer Not Sent']).')</td><td>'.implode(", ",$pendingSO['Customer Not Sent']).'</td></tr>';
					}
					if(isset($pendingSO['Missing SKU'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Missing SKU('.count($pendingSO['Missing SKU']).')</td><td>'.implode(", ",$pendingSO['Missing SKU']).'</td></tr>';
					}
					if(isset($pendingSO['Dropship PO not sent yet'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Dropship PO not sent yet('.count($pendingSO['Dropship PO not sent yet']).')</td><td>'.implode(", ",$pendingSO['Dropship PO not sent yet']).'</td></tr>';
					}
					if(isset($pendingSO['DocNumber is already exists in QBO'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>DocNumber is already exists in QBO('.count($pendingSO['DocNumber is already exists in QBO']).')</td><td>'.implode(", ",$pendingSO['DocNumber is already exists in QBO']).'</td></tr>';
					}
					if(isset($pendingSO['Order Un-invoiced in Brightpearl'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingSO['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingSO['Order Un-invoiced in Brightpearl']).'</td></tr>';
					}
					if(isset($pendingSO['Others'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Others('.count($pendingSO['Others']).')</td><td>'.implode(", ",$pendingSO['Others']).'</td></tr>';
					}
				}
			}
			if($stuckedSC){
				$pendingSC	= array();
				foreach($stuckedSC as $stuckedSCs){
					if((strlen($stuckedSCs['message']) > 0) or ($stuckedSCs['uninvoiced'])){
						if(substr_count(strtolower($stuckedSCs['message']),'customer not sent')){
							$pendingSC['Customer Not Sent'][]							= $stuckedSCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSCs['message']),'missing sku')){
							$pendingSC['Missing SKU'][]									= $stuckedSCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSCs['message']),'docnumber is already exists in qbo')){
							$pendingSC['DocNumber is already exists in QBO'][]			= $stuckedSCs['orderId'];
						}
						elseif($stuckedSCs['uninvoiced']){
							$pendingSC['Order Un-invoiced in Brightpearl'][]			= $stuckedSCs['orderId'];
						}
						else{
							$pendingSC['Others'][]	= $stuckedSCs['orderId'];	
						}
					}
					else{
						$pendingSC['Others'][]	= $stuckedSCs['orderId'];	
					}
					if($stuckedSCs['createdRowData']){
						$createdRowData	= json_decode($stuckedSCs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Fault']['Error']){
							$AllErroLogs['SC'][$stuckedSCs['orderId']]	= $createdRowData['Response data	: ']['Fault']['Error'];
						}
					}
				}
				if($pendingSC){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>SalesCredit</td></td>';
					if(isset($pendingSC['Customer Not Sent'])){
						$notSet	= 0;
						$Pendingbody	.= '<td>Customer Not Sent('.count($pendingSC['Customer Not Sent']).')</td><td>'.implode(", ",$pendingSC['Customer Not Sent']).'</td></tr>';
					}
					if(isset($pendingSC['Missing SKU'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Missing SKU('.count($pendingSC['Missing SKU']).')</td><td>'.implode(", ",$pendingSC['Missing SKU']).'</td></tr>';
					}
					if(isset($pendingSC['DocNumber is already exists in QBO'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>DocNumber is already exists in QBO('.count($pendingSC['DocNumber is already exists in QBO']).')</td><td>'.implode(", ",$pendingSC['DocNumber is already exists in QBO']).'</td></tr>';
					}
					if(isset($pendingSC['Order Un-invoiced in Brightpearl'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingSC['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingSC['Order Un-invoiced in Brightpearl']).'</td></tr>';
					}
					if(isset($pendingSC['Others'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Others('.count($pendingSC['Others']).')</td><td>'.implode(", ",$pendingSC['Others']).'</td></tr>';
					}
				}
			}
			if($stuckedPO){
				$pendingPO	= array();
				foreach($stuckedPO as $stuckedPOs){
					if((strlen($stuckedPOs['message']) > 0) or ($stuckedPOs['uninvoiced'])){
						if(substr_count(strtolower($stuckedPOs['message']),'supplier not sent')){
							$pendingPO['Supplier Not Sent'][]							= $stuckedPOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPOs['message']),'missing sku')){
							$pendingPO['Missing SKU'][]									= $stuckedPOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPOs['message']),'parent so is not fetched yet')){
							$pendingPO['Parent SO is Not fetched yet'][]				= $stuckedPOs['orderId'];
						}
						elseif($stuckedPOs['uninvoiced']){
							$pendingPO['Order Un-invoiced in Brightpearl'][]			= $stuckedPOs['orderId'];
						}
						else{
							$pendingPO['Others'][]	= $stuckedPOs['orderId'];	
						}
					}
					else{
						$pendingPO['Others'][]	= $stuckedPOs['orderId'];	
					}
					if($stuckedPOs['createdRowData']){
						$createdRowData	= json_decode($stuckedPOs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Fault']['Error']){
							$AllErroLogs['PO'][$stuckedPOs['orderId']]	= $createdRowData['Response data	: ']['Fault']['Error'];
						}
					}
				}
				if($pendingPO){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>PurchaseOrder</td></td>';
					if(isset($pendingPO['Supplier Not Sent'])){
						$notSet	= 0;
						$Pendingbody	.= '<td>Supplier Not Sent('.count($pendingPO['Supplier Not Sent']).')</td><td>'.implode(", ",$pendingPO['Supplier Not Sent']).'</td></tr>';
					}
					if(isset($pendingPO['Missing SKU'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Missing SKU('.count($pendingPO['Missing SKU']).')</td><td>'.implode(", ",$pendingPO['Missing SKU']).'</td></tr>';
					}
					if(isset($pendingPO['Order Un-invoiced in Brightpearl'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingPO['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingPO['Order Un-invoiced in Brightpearl']).'</td></tr>';
					}
					if(isset($pendingPO['Parent SO is Not fetched yet'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Parent SO is Not fetched yet('.count($pendingPO['Parent SO is Not fetched yet']).')</td><td>'.implode(", ",$pendingPO['Parent SO is Not fetched yet']).'</td></tr>';
					}
					if(isset($pendingPO['Others'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Others('.count($pendingPO['Others']).')</td><td>'.implode(", ",$pendingPO['Others']).'</td></tr>';
					}
				}
			}
			if($stuckedPC){
				$pendingPC	= array();
				foreach($stuckedPC as $stuckedPCs){
					if((strlen($stuckedPCs['message']) > 0) or ($stuckedPCs['uninvoiced'])){
						if(substr_count(strtolower($stuckedPCs['message']),'supplier not sent')){
							$pendingPC['Supplier Not Sent'][]							= $stuckedPCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPCs['message']),'missing sku')){
							$pendingPC['Missing SKU'][]									= $stuckedPCs['orderId'];
						}
						elseif($stuckedPCs['uninvoiced']){
							$pendingPC['Order Un-invoiced in Brightpearl'][]			= $stuckedPCs['orderId'];
						}
						else{
							$pendingPC['Others'][]	= $stuckedPCs['orderId'];	
						}
					}
					else{
						$pendingPC['Others'][]	= $stuckedPCs['orderId'];	
					}
					if($stuckedPCs['createdRowData']){
						$createdRowData	= json_decode($stuckedPCs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Fault']['Error']){
							$AllErroLogs['PC'][$stuckedPCs['orderId']]	= $createdRowData['Response data	: ']['Fault']['Error'];
						}
					}
				}
				if($pendingPC){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>PurchaseCredit</td></td>';
					if(isset($pendingPC['Supplier Not Sent'])){
						$notSet	= 0;
						$Pendingbody	.= '<td>Supplier Not Sent('.count($pendingPC['Supplier Not Sent']).')</td><td>'.implode(", ",$pendingPC['Supplier Not Sent']).'</td></tr>';
					}
					if(isset($pendingPC['Missing SKU'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Missing SKU('.count($pendingPC['Missing SKU']).')</td><td>'.implode(", ",$pendingPC['Missing SKU']).'</td></tr>';
					}
					if(isset($pendingPC['Order Un-invoiced in Brightpearl'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingPC['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingPC['Order Un-invoiced in Brightpearl']).'</td></tr>';
					}
					if(isset($pendingPC['Others'])){
						if(!$notSet){
							$Pendingbody	.= '<tr><td></td>';
						}
						$notSet	= 0;
						$Pendingbody	.= '<td>Others('.count($pendingPC['Others']).')</td><td>'.implode(", ",$pendingPC['Others']).'</td></tr>';
					}
				}
			}
			$Pendingbody	.= '</tbody></table>';
		}
		if($isPending){
			$PendingBodyData['mailBody']		= $Pendingbody;
			if($AllErroLogs){
				$PendingBodyData['ErrorLogs']	= $AllErroLogs;
			}
			return	$PendingBodyData;
		}
	}
	public function pendingPaymentAlert(){
		if($this->clientcode == 'coloradokayak'){
			$salesOrdersDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id,rowData')->get_where('sales_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
			$salesCreditDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id,rowData')->get_where('sales_credit_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
		elseif($this->clientcode == 'vitacupqbo'){
			$salesOrdersDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id,rowData')->get_where('sales_order',array('status <>' => '0','sendInAggregation' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
			$salesCreditDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id,rowData')->get_where('sales_credit_order',array('status <>' => '0','sendInAggregation' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
		else{
			$salesOrdersDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id')->get_where('sales_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
			$salesCreditDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id')->get_where('sales_credit_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
		$purchaseOrderDatas		= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id')->get_where('purchase_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		$purchaseCreditDatas	= $this->ci->db->order_by('orderId','desc')->select('orderId,createOrderId,paymentDetails,account1Id,account2Id')->get_where('purchase_credit_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
			
		$pendingPaymentSalesOrderIds		= array();
		$pendingPaymentSalesCreditIds		= array();
		$pendingPaymentPurchaseOrderIds		= array();
		$pendingPaymentPurchaseCreditIds	= array();
		$TotalPendingpayments				= 0;
		if($salesOrdersDatas){
			$saveIDsData = array();
			foreach($salesOrdersDatas as $salesOrdersData){
				$paymentDetails	= json_decode($salesOrdersData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if($key != 'amazonFeeinfo'){
						if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
							$isPending		= 1;
							break;
						}
					}
				}
				if($isPending){
					$this->qbo->reInitialize();
					$this->brightpearl->reInitialize();
					$qboID			= $salesOrdersData['createOrderId'];
					$BrightpearlID	= $salesOrdersData['orderId'];
					$account1Id		= $salesOrdersData['account1Id'];
					$account2Id		= $salesOrdersData['account2Id'];
					$updateArray	= array();
					if(!$qboID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					if($this->clientcode == 'coloradokayak'){
						$rowData	= json_decode($salesOrdersData['rowData'],true);
						$taxDate	= $rowData['invoices']['0']['taxDate'];
						$taxDate	= date('Ymd',strtotime($rowData['invoices']['0']['taxDate']));
						if($taxDate < '20211231'){
							continue;
						}
					}
					
					if($saveIDsData[$qboID]){
						$qboResults	= $saveIDsData[$qboID];
					}
					else{
						$QBOUrl					= 'invoice/'.$qboID.'?minorversion=41';
						$qboResults				= $this->qbo->getCurl($QBOUrl, "GET", '', 'json', $account2Id)[$account2Id];
						$saveIDsData[$qboID]	= $qboResults;
					}
					if($qboResults['Invoice']['Id']){
						if(abs($qboResults['Invoice']['Balance']) > 0){
							$pendingPaymentSalesOrderIds[]	= $salesOrdersData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['isvoided']){
										$paymentDetails[$pkey]['DeletedOnBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('sales_order',$updateArray);
							}
							else{
								$pendingPaymentSalesOrderIds[]	= $salesOrdersData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		if($salesCreditDatas){
			foreach($salesCreditDatas as $salesCreditData){
				$paymentDetails	= json_decode($salesCreditData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
						$isPending		= 1;
						break;
					}
				}
				if($isPending){
					$this->qbo->reInitialize();
					$this->brightpearl->reInitialize();
					$qboID				= $salesCreditData['createOrderId'];
					$BrightpearlID		= $salesCreditData['orderId'];
					$account1Id			= $salesCreditData['account1Id'];
					$account2Id			= $salesCreditData['account2Id'];
					$updateArray		= array();
					if(!$qboID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					if($this->clientcode == 'coloradokayak'){
						$rowData	= json_decode($salesCreditData['rowData'],true);
						$taxDate	= $rowData['invoices']['0']['taxDate'];
						$taxDate	= date('Ymd',strtotime($rowData['invoices']['0']['taxDate']));
						if($taxDate < '20211231'){
							continue;
						}
					}
					
					$QBOUrl		= 'creditmemo/'.$qboID.'?minorversion=41';
					$qboResults	= $this->qbo->getCurl($QBOUrl, "GET", '', 'json', $account2Id)[$account2Id];
					if($qboResults['CreditMemo']['Id']){
						if(abs($qboResults['CreditMemo']['RemainingCredit']) > 0){
							$pendingPaymentSalesCreditIds[]	= $salesCreditData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['isvoided']){
										$paymentDetails[$pkey]['DeletedOnBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('sales_credit_order',$updateArray);
							}
							else{
								$pendingPaymentSalesCreditIds[]	= $salesCreditData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		if($purchaseOrderDatas){
			foreach($purchaseOrderDatas as $purchaseOrderData){
				$paymentDetails	= json_decode($purchaseOrderData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
						$isPending		= 1;
						break;
					}
				}
				if($isPending){
					$this->qbo->reInitialize();
					$this->brightpearl->reInitialize();
					$qboID				= $purchaseOrderData['createOrderId'];
					$BrightpearlID		= $purchaseOrderData['orderId'];
					$account1Id			= $purchaseOrderData['account1Id'];
					$account2Id			= $purchaseOrderData['account2Id'];
					$updateArray		= array();
					if(!$qboID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					$QBOUrl		= 'bill/'.$qboID.'?minorversion=41';
					$qboResults	= $this->qbo->getCurl($QBOUrl, "GET", '', 'json', $account2Id)[$account2Id];
					if($qboResults['Bill']['Id']){
						if(abs($qboResults['Bill']['Balance']) > 0){
							$pendingPaymentPurchaseOrderIds[]	= $purchaseOrderData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['isvoided']){
										$paymentDetails[$pkey]['DeletedOnBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('purchase_order',$updateArray);
							}
							else{
								$pendingPaymentPurchaseOrderIds[]	= $purchaseOrderData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		if($purchaseCreditDatas){
			foreach($purchaseCreditDatas as $purchaseCreditData){
				$paymentDetails	= json_decode($purchaseCreditData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
						$isPending		= 1;
						break;
					}
				}
				if($isPending){
					$this->qbo->reInitialize();
					$this->brightpearl->reInitialize();
					$qboID				= $purchaseCreditData['createOrderId'];
					$BrightpearlID		= $purchaseCreditData['orderId'];
					$account1Id			= $purchaseCreditData['account1Id'];
					$account2Id			= $purchaseCreditData['account2Id'];
					$updateArray		= array();
					if(!$qboID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					$QBOUrl		= 'vendorcredit/'.$qboID.'?minorversion=41';
					$qboResults	= $this->qbo->getCurl($QBOUrl, "GET", '', 'json', $account2Id)[$account2Id];
					if($qboResults['VendorCredit']['Id']){
						if(abs($qboResults['VendorCredit']['Balance']) > 0){
							$pendingPaymentPurchaseCreditIds[]	= $purchaseCreditData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['isvoided']){
										$paymentDetails[$pkey]['DeletedOnBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('purchase_credit_order',$updateArray);
							}
							else{
								$pendingPaymentPurchaseCreditIds[]	= $purchaseCreditData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		
		$PaymentErrorLogs	= array();
		if($pendingPaymentSalesOrderIds){
			$PendingSalesPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentSalesOrderIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('sales_order',array('orderId <>' => '',))->result_array();
			if($PendingSalesPaymentsOrderData){
				foreach($PendingSalesPaymentsOrderData as $TempNewData){
					$PaymentInfo	= array();
					$SalesOrderId	= $TempNewData['orderId'];
					$createdRowData	= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'qbo payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'qbo gift card response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['Fault']['Error'])){
							$PaymentErrorLogs['SO'][$SalesOrderId]	= $PaymentInfo['Fault']['Error'];
						}
						elseif(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['SO'][$SalesOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		if($pendingPaymentSalesCreditIds){
			$PendingSalesCreditPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentSalesCreditIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('sales_credit_order',array('orderId <>' => '',))->result_array();
			if($PendingSalesCreditPaymentsOrderData){
				foreach($PendingSalesCreditPaymentsOrderData as $TempNewData){
					$PaymentInfo		= array();
					$SalesCreditOrderId	= $TempNewData['orderId'];
					$createdRowData		= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'qbo payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'qbo gift card response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['Fault']['Error'])){
							$PaymentErrorLogs['SC'][$SalesCreditOrderId]	= $PaymentInfo['Fault']['Error'];
						}
						elseif(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['SC'][$SalesCreditOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		if($pendingPaymentPurchaseOrderIds){
			$PendingPurchaseOrderPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentPurchaseOrderIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('purchase_order',array('orderId <>' => '',))->result_array();
			if($PendingPurchaseOrderPaymentsOrderData){
				foreach($PendingPurchaseOrderPaymentsOrderData as $TempNewData){
					$PaymentInfo		= array();
					$PurchaseOrderId	= $TempNewData['orderId'];
					$createdRowData		= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['PO'][$PurchaseOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		if($pendingPaymentPurchaseCreditIds){
			$PendingPurchaseCreditPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentPurchaseCreditIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('purchase_credit_order',array('orderId <>' => '',))->result_array();
			if($PendingPurchaseCreditPaymentsOrderData){
				foreach($PendingPurchaseCreditPaymentsOrderData as $TempNewData){
					$PaymentInfo			= array();
					$PurchaseCreditOrderId	= $TempNewData['orderId'];
					$createdRowData			= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['PC'][$PurchaseCreditOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		
		$PendingPaymentbody	= '';
		if($pendingPaymentSalesOrderIds OR $pendingPaymentSalesCreditIds OR $pendingPaymentPurchaseOrderIds OR $pendingPaymentPurchaseCreditIds){
			$PendingPaymentbody	= '<br></br><b># Pending payments report :-</b><br>';
			$PendingPaymentbody	.= '<table border=1><thead><tr><th>OrderType</th><th>OrderIds</th></tr></thead><tbody>';
			if($pendingPaymentSalesOrderIds){
				$PendingPaymentbody	.= '<tr><td>Sales Order('.count($pendingPaymentSalesOrderIds).')</td><td>'.implode(", ",$pendingPaymentSalesOrderIds).'</td></tr>';
			}
			if($pendingPaymentSalesCreditIds){
				$PendingPaymentbody	.= '<tr><td>Sales Credit('.count($pendingPaymentSalesCreditIds).')</td><td>'.implode(", ",$pendingPaymentSalesCreditIds).'</td></tr>';
			}
			if($pendingPaymentPurchaseOrderIds){
				$PendingPaymentbody	.= '<tr><td>Purchase Order('.count($pendingPaymentPurchaseOrderIds).')</td><td>'.implode(", ",$pendingPaymentPurchaseOrderIds).'</td></tr>';
			}
			if($pendingPaymentPurchaseCreditIds){
				$PendingPaymentbody	.= '<tr><td>Purchase Credit('.count($pendingPaymentPurchaseCreditIds).')</td><td>'.implode(", ",$pendingPaymentPurchaseCreditIds).'</td></tr>';
			}
			$PendingPaymentbody	.= '</tbody></table>';
		}
		$PaymentsMailBody	= array();
		if($PendingPaymentbody){
			$PaymentsMailBody['OrderIds']				= $PendingPaymentbody;
			if($PaymentErrorLogs){
				$PaymentsMailBody['PaymentsErrorLogs']	= $PaymentErrorLogs;
			}
			return	$PaymentsMailBody;
		}
	}
}