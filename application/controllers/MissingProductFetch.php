<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class MissingProductFetch extends CI_Controller {
	public $ci;
	public function __construct(){
		parent::__construct();
		$this->load->library('mailer');
		$this->ci			= &get_instance();
		$this->appName		= $this->globalConfig['app_name'];
		$this->from			= array('info@bsitc-apps.com' => $this->appName);
		$this->defaultPhp	= '8.2';
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
	}
	public function FetchMissingProduct(){
		echo"<pre>";print_r('startTime : '.date('c'));echo"<pre>";
		$allMissingSKU		= array();
		$AllOrdersDatas		= array();
		$AllFetchProducts	= array();
		$AllOrdersDatas[]	= $this->db->select('message,rowData')->like('message', "Missing")->get_where('sales_order',array('status' => '0'))->result_array();
		$AllOrdersDatas[]	= $this->db->select('message,rowData')->like('message', "Missing")->get_where('sales_credit_order',array('status' => '0'))->result_array();
		$AllOrdersDatas[]	= $this->db->select('message,rowData')->like('message', "Missing")->get_where('purchase_order',array('status' => '0'))->result_array();
		$AllOrdersDatas[]	= $this->db->select('message,rowData')->like('message', "Missing")->get_where('purchase_credit_order',array('status' => '0'))->result_array();
		
		
		foreach($AllOrdersDatas as $AllOrdersDatasType){
			foreach($AllOrdersDatasType as $salesDatas){
				$orderMissingSKU	= array();
				$message			= '';
				$message			= $salesDatas['message'];
				$message			= substr($message,14);
				$rowData			= json_decode($salesDatas['rowData'],true);
				if(strlen($message) > 0){
					$orderMissingSKU	= explode(",",$message);
					if($orderMissingSKU){
						foreach($orderMissingSKU as $orderMissingSKUs){
							if(strlen($orderMissingSKUs) > 0){
								$allMissingSKU[]	= $orderMissingSKUs;
							}
						}
					}
				}
				foreach($rowData['orderRows'] as $orderRows){
					if(in_array($orderRows['productSku'], $allMissingSKU)){
						$AllFetchProducts[]	= $orderRows['productId'];
					}
				}
			}
		}
		
		if($AllFetchProducts){
			$AllFetchProducts	= array_filter($AllFetchProducts);
			$AllFetchProducts	= array_unique($AllFetchProducts);
			$this->ci->brightpearl->fetchProducts($AllFetchProducts);
			$this->ci->qbo->postProducts($AllFetchProducts);
		}
		
		
		$AllOrdersDatas		= array();
		$AllOrdersDatas[]	= $this->db->select('customerId')->like('message', "Customer")->get_where('sales_order',array('status' => '0'))->result_array();
		$AllOrdersDatas[]	= $this->db->select('customerId')->like('message', "Customer")->get_where('sales_credit_order',array('status' => '0'))->result_array();
		$AllOrdersDatas[]	= $this->db->select('customerId')->like('message', "Supplier")->get_where('purchase_order',array('status' => '0'))->result_array();
		$AllOrdersDatas[]	= $this->db->select('customerId')->like('message', "Supplier")->get_where('purchase_credit_order',array('status' => '0'))->result_array();
		$allPendingCustomer	= array();
		
		if($AllOrdersDatas){
			foreach($AllOrdersDatas as $AllOrdersDatasType){
				foreach($AllOrdersDatasType as $salesDatas){
					$allPendingCustomer[]	= $salesDatas['customerId'];
				}
			}
			if($allPendingCustomer){
				$allPendingCustomer	= array_filter($allPendingCustomer);
				$allPendingCustomer	= array_unique($allPendingCustomer);
				$this->ci->brightpearl->fetchCustomers($allPendingCustomer);
				$this->ci->qbo->postCustomers($allPendingCustomer);
			}
		}
		echo"<pre>";print_r('endTime : '.date('c'));echo"<pre>";
	}
	public function runTaskQBO($taskName = 'FetchMissingProduct'){
		if($taskName){
			$isRunning	= $this->getRunningTask($taskName);
			if(!$isRunning){
				$syncFilePath	= FCPATH . '/jobsoutput/' . date('Y/m/d') . '/synclogs/';
				if(!is_dir(($syncFilePath))){
					mkdir(($syncFilePath),0777,true);
					chmod(($syncFilePath), 0777);
				}
				$syncFileName	= $taskName.date('H-i-s').'.logs';
				
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php MissingProductFetch '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php MissingProductFetch '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				
				if(is_file($syncFilePath.$syncFileName)){
					$filecontent	= file_get_contents($syncFilePath.$syncFileName);
					if(strlen($filecontent) > 0){
						if((substr_count($filecontent,"Type:        TypeError")) OR (substr_count($filecontent,"Fatal error")) OR (substr_count($filecontent,"Database error")) OR (substr_count($filecontent,"Type:        ValueError")) OR (substr_count($filecontent,"Type:        Error"))){
							$appName		= trim($this->globalConfig['app_name']);
							$Receipents		= trim($this->globalConfig['emailReceipents']);
							if($Receipents == ''){
								$Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,krisha@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com';
							}
							$smtpUsername	= trim($this->globalConfig['smtpUsername']);
							$smtpPassword	= trim($this->globalConfig['smtpPassword']);
							$subject		= 'Alert '.$appName.' - '.$taskName.' Run time error';
							$from			= ['info@bsitc-apps.com' => $this->appName];
							$mailBody		= 'Hi,<br><br><p>Automation not completed successfully, please check the attached cron output file.</p><br><br><br><br><br>Thanks & Regards<br>BSITC Team';
							$this->mailer->send($Receipents, $subject, $mailBody, $from, $syncFilePath.$syncFileName, $smtpUsername, $smtpPassword);
						}
					}
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'FetchMissingProduct'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php MissingProductFetch '.$checkTask);
			foreach($outputs as $output){
				$output	= strtolower($output);
				if(strpos($output, $fcpath) !== false){
					if(strpos($output, $checkTask) !== false){
						if(strpos($output, 'runtask') === false){
							$return	= true;
							break;
						}
					}
				}
			}
		}
		return $return;
	}

	public function closePayment2(){
		$SalesOrderPendingPaymentOrderID		= array();
		$SalesCreditPendingPaymentOrderID		= array();
		$PurchaseOrderPendingPaymentOrderID		= array();
		$PurchaseCreditPendingPaymentOrderID	= array();
		$DBTable								= '';
		$allpayments							= array();
		$PendingPaymentOrderIDs					= array();
		if($SalesOrderPendingPaymentOrderID){
			$DBTable				= 'sales_order';
			$PendingPaymentOrderIDs	= $SalesOrderPendingPaymentOrderID;
		}
		elseif($SalesCreditPendingPaymentOrderID){
			$DBTable	= 'sales_credit_order';
			$PendingPaymentOrderIDs	= $SalesCreditPendingPaymentOrderID;
		}
		elseif($PurchaseOrderPendingPaymentOrderID){
			$DBTable	= 'purchase_order';
			$PendingPaymentOrderIDs	= $PurchaseOrderPendingPaymentOrderID;
		}
		elseif($PurchaseCreditPendingPaymentOrderID){
			$DBTable	= 'purchase_credit_order';
			$PendingPaymentOrderIDs	= $PurchaseCreditPendingPaymentOrderID;
		}
		if($DBTable){
			$allpayments	= $this->db->where_in('orderId',$PendingPaymentOrderIDs)->select('orderId,paymentDetails')->get_where($DBTable)->result_array();
		}
		if($allpayments){
			foreach($allpayments as $allpaymentsData){
				$updateArray	= array();
				$orderId		= $allpaymentsData['orderId'];
				$paymentDetails	= json_decode($allpaymentsData['paymentDetails'],true);
				if(!$paymentDetails){
					continue;
				}
				foreach($paymentDetails as $pkey => $paymentDetail){
					$paymentDetails[$pkey]['status']	= 1;
				}
				$updateArray['paymentDetails']		= json_encode($paymentDetails);
				$updateArray['isPaymentCreated']	= 1;
				$updateArray['status']				= 3;
				$this->db->where(array('orderId' => $orderId))->update($DBTable,$updateArray);
			}
		}
	}
}