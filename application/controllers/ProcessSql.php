<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class ProcessSql extends CI_Controller {
	public $ci, $tableInfos = array();
	public function __construct(){
		parent::__construct();
		$this->ci			= &get_instance();
		$this->tableInfos	= $this->getTableContents();
	}
	public function getTableContents(){
		$tableStructure	= array();
		$tables			= $this->db->list_tables();
		if(is_array($tables)){
			foreach($tables as $tableName){
				$fields = $this->db->list_fields($tableName);
				if(is_array($fields)){
					foreach($fields as $fieldName){
						$tableStructure[$tableName][$fieldName]	= $fieldName;
					}
				}
			}
		}
		return $tableStructure;
		/* $tables = $this->db->list_tables(); */
		/* $fields = $this->db->list_fields('tableName'); */
		/* $fields = $this->db->field_data('tableName'); */
		/* $fields = $this->db->field_exists('columnName', 'tableName'); */
	}
	public function runSQL(){
		$clientcode		= $this->config->item('clientcode');
		$excludeFiles	= array('.','..');
		$allArchiveFile	= array();
		$queryFailed	= 0;

		if($clientcode != 'qbo_demo'){
			$fullArchivePath	= FCPATH. DIRECTORY_SEPARATOR . 'allSql/archive';
			if(!is_dir($fullArchivePath)){
				mkdir($fullArchivePath,0777,true);
				chmod(dirname($fullArchivePath), 0777);
			}
			$archiveScan	= scandir(FCPATH . DIRECTORY_SEPARATOR . 'allSql/archive');
			$activeScan		= scandir(FCPATH. DIRECTORY_SEPARATOR . 'allSql/active');
			foreach($activeScan as $activeFile){
				$isExecute	= 0;
				if(in_array($activeFile, $excludeFiles)){
					continue;
				}
				$sqlFilePath	= FCPATH. DIRECTORY_SEPARATOR . 'allSql/active/'.$activeFile;
				$archivePath	= FCPATH. DIRECTORY_SEPARATOR . 'allSql/archive/'.$activeFile;
				$fileSql		= file_get_contents($sqlFilePath);
				if(strlen($fileSql)){
					$allSql		= json_decode($fileSql,true);
					if(is_array($allSql)){
						foreach($allSql as $sqlOperation => $sqlArray){
							if($sqlOperation == 'addTable'){
								foreach($sqlArray as $sqlArrayTemp){
									$tableName = $sqlArrayTemp['tableName'];
									if(!isset($this->tableInfos[$tableName])){
										$query		= $sqlArrayTemp['query'];
										$isExecute	= $this->db->query($query);
										if(!$isExecute){
											$queryFailed	= 1;
										}
									}
								}
							}
							if($sqlOperation == 'addColumn'){
								foreach($sqlArray as $sqlArrayTemp){
									$tableName	= $sqlArrayTemp['tableName'];
									$columnName	= $sqlArrayTemp['columnName'];
									$columnType	= $sqlArrayTemp['columnType'];
									$isNull		= $sqlArrayTemp['isNull'];
									$DEFAULT	= $sqlArrayTemp['DEFAULT'];
									if(!isset($this->tableInfos[$tableName][$columnName])){
										$query		= "ALTER TABLE $tableName ADD $columnName $columnType $isNull ";
										if($DEFAULT){
											$query	.= "DEFAULT $DEFAULT";
										}
										$isExecute	= $this->db->query($query);
										if(!$isExecute){
											$queryFailed	= 1;
										}
									}
								}
							}
							if($sqlOperation == 'changeColumn'){
								foreach($sqlArray as $sqlArrayTemp){
									$query		= $sqlArrayTemp;
									$isExecute	= $this->db->query($query);
									if(!$isExecute){
										$queryFailed	= 1;
									}
								}
							}
						}
					}
				}
				echo"sqlFilePath<pre>";print_r($sqlFilePath);echo"<pre>";
				echo"archivePath<pre>";print_r($archivePath);echo"<pre>";
				if($queryFailed == 0){
					rename($sqlFilePath, $archivePath);
				}
			}
		}
	}
}