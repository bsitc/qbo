<?php
//QBO Demo
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Webhooks extends CI_Controller {
	public $file_path;
	public function __construct(){
		parent::__construct();
		$this->load->library('mailer');
		$this->defaultPhp	= "8.2";
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
	}	
	/*******Save QBO Exchange Rate ******/
	public function getQboExchangeRate(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->qbo->getQboExchangeRate();
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function refreshToken($qboId = ''){
		$this->qbo->refreshToken($qboId);
	}
	
	/* public function mytest($qboId = ''){
		$this->brightpearl->fetchPayment();
	} */
	
	public function disconnectApp($qboId = ''){
		if($qboId){
			$this->db->where('id', $qboId)->update('account_qbo_account', array('accessToken' => '', 'refreshToken' => '', 'tokenFetchTime' => ''));
			$appDisconnectLogs	= array();
			$fileName			= 'disconnectAppLog.txt';
			$fileName			= FCPATH.'disconnectAppLog.json';
			if(is_file($fileName)){
				$fileContents					= file_get_contents($fileName);
				$appDisconnectLogs				= json_decode($fileContents, true);
				$appDisconnectLogs[date('c')]	= array(
					'qbo_account_id'				=> $qboId,
					'disconnectTime'				=> date('c'),
				);
				file_put_contents($fileName,json_encode($appDisconnectLogs));
			}
			else{
				$appDisconnectLogs[date('c')]	= array(
					'qbo_account_id'				=> $qboId,
					'disconnectTime'				=> date('c'),
				);
				file_put_contents($fileName,json_encode($appDisconnectLogs));
			}
		}
	}
	
	
	
	/*	automation FOR QBODEMO QBO		-	//qbodemo.bsitc-apps.com/					*/
	public function	processQBODemo(){
		echo"<pre>";print_r("");echo"<pre>";die(__FILE__.' : Line No :'.__LINE__);
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('receipt/'.$this->globalConfig['account2Liberary'].'/refundreceipt_overridemodel','',TRUE);
		$this->refundreceipt_overridemodel->fetchRefundreceipt();
		$this->refundreceipt_overridemodel->postRefundreceipt();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		$this->load->model('cogsjournal/grnijournal_model','',TRUE);
		$this->grnijournal_model->fetchGrnijournal();
		$this->grnijournal_model->postGrnijournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function	processQBODemoConsol(){
		echo"<pre>";print_r("");echo"<pre>";die(__FILE__.' : Line No :'.__LINE__);
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('receipt/'.$this->globalConfig['account2Liberary'].'/refundreceipt_overridemodel','',TRUE);
		$this->refundreceipt_overridemodel->fetchRefundreceipt();
		$this->refundreceipt_overridemodel->postConsolRefundReceipt();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('journal/otheramazonfee_model','',TRUE);
		$this->otheramazonfee_model->fetchAmazonFeeOther();
		$this->otheramazonfee_model->postAmazonFeeOther();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*	automation FOR Monica&Andy QBO			-	//bsitc-bridge29.com/monicaandandy/				*/
	public function	processMonicaandAndyQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase(); */
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processMonicaandAndyConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR FlexCompany QBO			-	//bsitc-bridge46.com/flexqbo/					*/
	public function	processFlexQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR Ironman4x4 QBO			-	//ironman4x4qbo.bsitc-apps.com/					*/
	public function	processIronmanQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR SixtyStores QBO			-	//sixtystoresqbo.bsitc-apps.com/				*/
	public function	processSixtyStoresQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processSixtyStoresConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR Rivent&Hide QBO			-	//bsitc-bridge33.com/rivetandhideqbo/			*/
	public function	processUpdateRivetAndHideQBO(){
		$this->load->model('taxupdate/updatedsales_model','',TRUE);
		$this->updatedsales_model->fetchAllSalesForUpdate();
		$this->updatedsales_model->fetchAllCreditForUpdate();
		$this->updatedsales_model->postAllUpdatedSales();
		$this->updatedsales_model->postAllUpdatedCredit();
		
	}
	public function	processRivetAndHideQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}

	/*	automation FOR HouseOfNomad QBO			-	//houseofnomadqbo.bsitc-apps.com/				*/
	public function	processHouseofNomadQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processHouseofNomadConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR vitacupqbo QBO			-	//vitacupqbo.bsitc-apps.com/					*/
	public function	processVitacupQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		//$this->load->model('journal/journal_model','',TRUE);
		//$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processVitacupConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}

	/*	automation FOR wexelartqbo QBO			-	//wexelartqbo.bsitc-apps.com/					*/
	public function	processWexelArtQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR ozonebilliardsqbo QBO	-	//ozonebilliardsqbo.bsitc-apps.com/				*/
	public function	processozonebilliardsQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR wyfqbom US QBO			-	//wyfqbom.bsitc-apps.com/						*/
	public function	processWYFUSAQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR wyfqbom CA QBO			-	//wyfqbomca.bsitc-apps.com/						*/
	public function	processWYFCAQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR SullivanBleeker  QBO		-	//sandbqbo.bsitc-apps.com/						*/
	public function	processSullivanBleekerQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processSullivanBleekerStockConsol(){
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR DyslexiaShop QBO			-	//dyslexiashopqbo.bsitc-apps.com/				*/
	public function	processDyslexiaShopQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processDyslexiaShopConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR ColoradoKayak QBO		-	//bsitc-bridge43.com/coloradokayak/				*/
	public function processColoradoKayak(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR Arbsession QBO			-	//arbsessionqbo.bsitc-apps.com/					*/
	public function	processArbsession(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR TheHairShop QBO			-	//hairshopqbo.bsitc-apps.com/					*/
	public function	processHairShopQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase(); */
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();	 */	
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processHairShopConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR BargooseTaxtiles QBO		-	//bargoosehometextilesqbo.bsitc-apps.com/		*/
	public function	processBargooseHomeQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}

	/*	automation FOR dryRobe QBO				-	//dryrobeqbo.bsitc-apps.com/					*/
	public function	processDryRobeQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR ecfqbo QBO				-	//ecfqbo.bsitc-apps.com/						*/
	public function	processEastCoastFabricsQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processEastCoastFabricsConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR bbhugmeqbo QBO			-	//bbhugmeqbo.bsitc-apps.com/					*/
	public function	processBBHugmeQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processBBHugmeConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR BISON QBO				-	//bisonqbo.bsitc-apps.com						*/
	public function	processBisonQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processBisonConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/* automation FOR balloonsdirectqbo  QBO	-	//balloonsdirectqbo.bsitc-apps.com/				*/
	public function	processBalloonsDirectQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for BatteryComponents QBO	-	//bsitc-bridge29.com/batterycomponents/			*/
	public function processBatteryComponents(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE);
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();

		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR Solecity QBO				-	//solecityshoesqbo.bsitc-apps.com/				*/
	public function	processSolecityQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR DapperInk QBO			-	//dapperinkqbo.bsitc-apps.com/					*/
	public function	processDapperInkQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processDapperInkConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR vibeKayks QBO			-	//vibekayaksqbo.bsitc-apps.com/					*/
	public function	processVibekayakQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR dakotastones QBO			-	//dakotastonesqbo.bsitc-apps.com/				*/
	public function	processDakotaStonesQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processDakotaStonesConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR USALab QBO				-	//usalabqbo.bsitc-apps.com/						*/
	public function	processUSALabQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR franceluxeqbo QBO		-	//franceluxeqbo.bsitc-apps.com/					*/
	public function	processFranceLuxeQBOs(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function	processFranceLuxeConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR biscuiteersqbo QBO		-	//biscuiteersqbo.bsitc-apps.com/				*/
	public function  processBiscuiteersQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('singlestocktx/singlestocktx_model','',TRUE);
		$this->singlestocktx_model->fetchSinglestocktx();
		$this->singlestocktx_model->postSinglestocktx();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function  processBiscuiteersConsolQBO(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR ForeFrontGolf QBO			-	//forefrontgolfqbo.bsitc-apps.com/				*/
	public function	processForeFrontGolfQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit(); */
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation FOR dostqbo QBO					-	//dostqbo.bsitc-apps.com/				*/
	public function  processDostBikesQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		
		//additional changes
		$this->customers_model->postCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	
	/*	automation FOR nitrossnowbqbm QBO		-	//nitrossnowbqbm.bsitc-apps.com/				*/
	public function  processNitroSnowboardsQBO(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	GLOBAL FUNCTION FOR PROCESS AUTOMATION OF QBO	*/
	public function runTaskQBO($taskName = 'processQBO'){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$syncFilePath	= FCPATH.'/jobsoutput/'.date('Y').'/'.date('m').'/'.date('d').'/synclogs/';
				if(!is_dir(($syncFilePath))){
					mkdir(($syncFilePath),0777,true);
					chmod(($syncFilePath), 0777);
				}
				$syncFileName	= $taskName.date('H-i-s').'.logs';
				
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php webhooks '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php webhooks '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				
				if(is_file($syncFilePath.$syncFileName)){
					$filecontent	= file_get_contents($syncFilePath.$syncFileName);
					if(strlen($filecontent) > 0){
						if((substr_count($filecontent,"Type:        TypeError")) OR (substr_count($filecontent,"Fatal error")) OR (substr_count($filecontent,"Database error")) OR (substr_count($filecontent,"Type:        ValueError")) OR (substr_count($filecontent,"Type:        Error"))){
							$appName		= trim($this->globalConfig['app_name']);
							$Receipents		= trim($this->globalConfig['emailReceipents']);
							if($Receipents == ''){
								$Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,krisha@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com';
							}
							$smtpUsername	= trim($this->globalConfig['smtpUsername']);
							$smtpPassword	= trim($this->globalConfig['smtpPassword']);
							$subject		= 'Alert '.$appName.' - '.$taskName.' Run time error';
							$from			= ['info@bsitc-apps.com' => $this->appName];
							$mailBody		= 'Hi,<br><br><p>Automation not completed successfully, please check the attached cron output file.</p><br><br><br><br><br>Thanks & Regards<br>BSITC Team';
							$this->mailer->send($Receipents, $subject, $mailBody, $from, $syncFilePath.$syncFileName, $smtpUsername, $smtpPassword);
						}
					}
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'processQBO'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php webhooks '.$checkTask);	
			foreach($outputs as $output){
				$output	= strtolower($output);
				if(substr_count($output,$fcpath)){
					if(substr_count($output,$checkTask)){
						if(!substr_count($output,'runtask')){ 
							$return	= true;
						}
					}
				}
			}
		}
		return $return;
	}
	public function closeRunner(){
		$cpid			= posix_getpid();
		exec('ps aux | grep php', $outputs);
		$currentTime	= date('YmdHis',strtotime('-120 min'));
		foreach($outputs as $output){
			$ps						= preg_split('/ +/', $output);
			$pid					= $ps[1];
			$cronStartTimeTimeStamp	= strtotime($ps['8']);
			if(substr_count($output,'runTask/')){
				exec("kill -9 $pid", $outputs);	
			}
			elseif(substr_count($output,'/index.php')){
				if(date('Y',$cronStartTimeTimeStamp) == date('Y')){
					$cronStartTime	= date('YmdHis',$cronStartTimeTimeStamp);
					if($cronStartTime < $currentTime){
						exec("kill -9 $pid", $outputs);
					}
				}
				elseif($pid != $cpid){
					exec("kill  -9 $pid", $outputs);
				}
			}
		}
	}
	
	/*	GLOBAL FUNCTION FOR PROCESS CUSTOMERS OF QBO LARGE VOLUME	*/
	public function processSavecustomers(){
		$data	= $this->db->get_where('temp',array('status'=> 0,'type' => 'customer'))->row_array();
		if($data){
			$this->brightpearl->fetchCustomers('',$data['id']);
		}
	}
	/*	GLOBAL FUNCTION FOR PROCESS PRODUCTS OF QBO LARGE VOLUME	*/
	public function processSaveproducts(){
		$data	= $this->db->get_where('temp',array('status'=> 0,'type' => 'products'))->row_array();
		if($data){
			$this->brightpearl->fetchProducts('',$data['id']);
		}
	}
}