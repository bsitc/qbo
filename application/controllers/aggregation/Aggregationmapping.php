<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Aggregationmapping extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('aggregation/Aggregationmapping_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->Aggregationmapping_model->get();
		$data['account1ChannelId']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1CurrencyId']		= $this->{$this->globalConfig['account1Liberary']}->getAllCurrency();
		$data['account1CustomFieldId']	= $this->{$this->globalConfig['account1Liberary']}->getAllSalesCustomFieldConsol();
		$data['account2ChannelId']		= array();
		$this->template->load_template("aggregation/aggregationmapping",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->Aggregationmapping_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->Aggregationmapping_model->delete($id);
		}
	}
}
?>