<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Aggregationtax extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('aggregation/aggregationtax_model');	
	}
	public function index(){
		$data					= array();
		$data					= $this->aggregationtax_model->get();
		$data['account1TaxId']	= $this->{$this->globalConfig['account1Liberary']}->getAllTax();
		$this->template->load_template("aggregation/aggregationtax",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->aggregationtax_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->aggregationtax_model->delete($id);
		}
	}
}
?>