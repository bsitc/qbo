<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Config extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('aggregation/config_model');
	}
	public function index(){
		$data	= array();
		$data	= $this->config_model->get();
		$this->template->load_template("aggregation/config",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->config_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->config_model->delete($id);
		}
	}
}
?>