<?php
//qbo
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Salesreport extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('aggregation/salesreport_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("aggregation/salesreport",$data,$this->session_data);
	}
	public function getSales(){
		$records	= $this->salesreport_model->getSales();
		echo json_encode($records);
	}
	public function exportSales(){
		error_reporting('0');
		$where				= array();
		$query				= $this->db;
		$start_date			= '';
		$end_date			= '';
		$report_file_name	= '';
		$Taxend_date		= '';
		$Taxstart_date		= '';
		
		if(trim($this->input->get('channelName'))){
			$exportedChannel			= trim($this->input->get('channelName'));
			$where['channelName']		= trim($this->input->get('channelName'));
		}
		if(trim($this->input->get('orderId'))){
			$where['orderId']			= trim($this->input->get('orderId'));
		}
		if(trim($this->input->get('createOrderId'))){
			$where['createOrderId']		= trim($this->input->get('createOrderId'));
		}
		if(trim($this->input->get('totalAmount'))){
			$where['totalAmount']		= trim($this->input->get('totalAmount'));
		}
		if(trim($this->input->get('bpInvoiceNumber'))){
			$where['bpInvoiceNumber']	= trim($this->input->get('bpInvoiceNumber'));
		}
		if(trim($this->input->get('invoiceRef'))){
			$where['invoiceRef']		= trim($this->input->get('invoiceRef'));
		}
		if (trim($this->input->get('orderNo'))) {
			$where['orderNo']			= trim($this->input->get('orderNo'));
		}
		if(trim($this->input->get('paymentStatus')) >= 0){
			$where['paymentStatus']		= trim($this->input->get('paymentStatus'));
		}
		if(trim($this->input->get('created_from'))){
			$start_date	= date('Ymd',strtotime(trim($this->input->get('created_from'))));
			$query->where('date(created) >= ', "date('" . $this->input->get('created_from') . "')", false);
		}
		if(trim($this->input->get('created_to'))){
			$end_date	= date('Ymd',strtotime(trim($this->input->get('created_to'))));
			$query->where('date(created) <= ', "date('" . $this->input->get('created_to') . "')", false);
		}
		if(trim($this->input->get('taxDate_from'))){
			$Taxstart_date	= date('Ymd',strtotime(trim($this->input->get('taxDate_from'))));
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
		}
		if(trim($this->input->get('taxDate_to'))){
			$Taxend_date	= date('Ymd',strtotime(trim($this->input->get('taxDate_to'))));
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$datas				= $query->select('id,channelName,orderId,createOrderId,bpInvoiceNumber,invoiceRef,totalAmount,paymentStatus,taxDate,created,rowData,orderNo,paymentDetails')->get_where('sales_order',array('sendInAggregation' => '1','createOrderId <>' => '','status <>' => '0'))->result_array();
		
		$AllConsolOrdersID	= array();
		$allAmazonFeesInfo		= array();
		$allCOGSAmountInfo		= array();
		$AllConsolPaymentInfo	= array();
		if($datas){
			foreach($datas as $SODatas){
				$AllConsolOrdersID[]	= $SODatas['orderId'];
			}
			if($AllConsolOrdersID){
				$allAmazonOrdersData	= array();
				$allAmazonOrdersData	= $this->db->where_in('orderId',$AllConsolOrdersID)->select('orderId,amount')->get_where('amazon_ledger')->result_array();
				if($allAmazonOrdersData){
					foreach($allAmazonOrdersData as $allAmazonOrdersDatas){
						if(isset($allAmazonFeesInfo[$allAmazonOrdersDatas['orderId']])){
							$allAmazonFeesInfo[$allAmazonOrdersDatas['orderId']]	+= $allAmazonOrdersDatas['amount'];
						}
						else{
							$allAmazonFeesInfo[$allAmazonOrdersDatas['orderId']]	= $allAmazonOrdersDatas['amount'];
						}
					}
				}
				
				$allCOGSOrderData		= array();
				$allCOGSOrderData		= $this->db->where_in('orderId',$AllConsolOrdersID)->select('orderId,creditAmount')->get_where('cogs_journal')->result_array();
				if($allCOGSOrderData){
					foreach($allCOGSOrderData as $allCOGSOrderDatas){
						if(isset($allCOGSAmountInfo[$allCOGSOrderDatas['orderId']])){
							$allCOGSAmountInfo[$allCOGSOrderDatas['orderId']]	+= $allCOGSOrderDatas['creditAmount'];
						}
						else{
							$allCOGSAmountInfo[$allCOGSOrderDatas['orderId']]	= $allCOGSOrderDatas['creditAmount'];
						}
					}
				}
				
				foreach($datas as $AllPaymentdata){
					if($AllPaymentdata['paymentDetails']){
						$TotalOrderPayment	= 0;
						$PaymentParams		= json_decode($AllPaymentdata['paymentDetails'],true);
						if($PaymentParams){
							foreach($PaymentParams as $PaymentParam){
								if($PaymentParam['sendPaymentTo'] == 'qbo'){
									if(isset($AllConsolPaymentInfo[$AllPaymentdata['orderId']])){
										$AllConsolPaymentInfo[$AllPaymentdata['orderId']]	+= $PaymentParam['amount'];
									}
									else{
										$AllConsolPaymentInfo[$AllPaymentdata['orderId']]	= $PaymentParam['amount'];
									}
								}
							}
						}
					}
				}
			}
		}
		
		$report_file_name	= 'SO-Aggregation';
		if($Taxstart_date OR $Taxend_date){
			if($Taxstart_date){
				$report_file_name	.= '-'.$Taxstart_date;
			}
			else{
				$report_file_name	.= '-na';
			}
			if($Taxend_date){
				$report_file_name	.= '-'.$Taxend_date;
			}
			else{
				$report_file_name	.= '-na';
			}
			$report_file_name	.= ".csv";
		}
		else{
			$report_file_name	.= '-'.date('Ymd').".csv";
		}
		$fp				= fopen('php://output', 'w');
		$header			= array('Channel', 'OrderID', 'InvoiceRef', 'Reference', 'QBOID', 'DocNumber', 'TotalAmt', 'AmazonFees', 'COGSAmount', 'PaymentReceived', 'PaymentStatus', 'TaxDate', 'CreatedOn');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$report_file_name);
		fputcsv($fp, $header);
		$paymentStatus	= array('0' => 'Un-Paid','1' => 'Paid','2' => 'Partially Paid');
		foreach($datas as $data){
			$FeesAmount			= 0;
			$COGSAmount			= 0;
			$FetchedAmount		= 0;
			$params				= json_decode($data['rowData'],true);
			$bpInvoiceNumber	= '';
			$bpInvoiceNumber	= @$data['bpInvoiceNumber'];
			if(!$bpInvoiceNumber){
				$bpInvoiceNumber	= $params['invoices'][0]['invoiceReference'];
			}
			if($allAmazonFeesInfo[$data['orderId']]){
				$FeesAmount			= $allAmazonFeesInfo[$data['orderId']];
			}
			if($allCOGSAmountInfo[$data['orderId']]){
				$COGSAmount			= $allCOGSAmountInfo[$data['orderId']];
			}
			if($AllConsolPaymentInfo[$data['orderId']]){
				$FetchedAmount		= $AllConsolPaymentInfo[$data['orderId']];
			}
			
			$row	= array(
				@$data['channelName'],
				@$data['orderId'],
				@$bpInvoiceNumber,
				@$data['orderNo'],
				@$data['createOrderId'],
				@$data['invoiceRef'],
				@$data['totalAmount'],
				@$FeesAmount,
				@$COGSAmount,
				@$FetchedAmount,
				@$paymentStatus[$data['paymentStatus']],
				date('Y-m-d',strtotime($data['taxDate'])),
				date('Y-m-d',strtotime($data['created'])),
			);
			fputcsv($fp, $row);
		}
	}
}