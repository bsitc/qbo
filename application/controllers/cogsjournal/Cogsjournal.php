<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Cogsjournal extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("cogsjournal/cogsjournal",$data,$this->session_data);
	}
	public function getCogsjournal(){
		$records	= $this->cogsjournal_model->getCogsjournal(); 
		echo json_encode($records);
	}
	public function fetchCogsjournal($orderId = ''){
		$this->cogsjournal_model->fetchCogsjournal($orderId);
	}
	public function postCogsjournal($orderId = ''){
		$this->cogsjournal_model->postCogsjournal($orderId);
	}
	public function postConsolCogsjournal($orderId = ''){
		$this->cogsjournal_model->postConsolCogsjournal($orderId);
	}
	public function cogsjournalInfo($orderId = ''){
		$data['cogsjournalInfo']	= $this->db->get_where('cogs_journal',array('journalsId' => $orderId))->row_array();
		$this->template->load_template("cogsjournal/cogsjournalInfo",$data,$this->session_data);
	}
	public function exportReport(){
		error_reporting('0');
		$where				= array();
        $query				= $this->db;
		$report_file_name	= '';
		
		
		if(trim($this->input->get('account1Id'))){
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if(trim($this->input->get('account2Id'))){
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if(trim($this->input->get('orderId'))){
			$where['orderId']			= trim($this->input->get('orderId'));
		}
		if(trim($this->input->get('journalsId'))){
			$where['journalsId']		= trim($this->input->get('journalsId'));
		}
		if(trim($this->input->get('createdJournalsId'))){
			$where['createdJournalsId']	= trim($this->input->get('createdJournalsId'));
		}
		if(trim($this->input->get('journalTypeCode'))){
			$where['journalTypeCode']	= trim($this->input->get('journalTypeCode'));
		}
		if(trim($this->input->get('OrderType'))){
			$where['OrderType']	= trim($this->input->get('OrderType'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']			= trim($this->input->get('status'));
		}
		if(trim($this->input->get('isConsolidated')) >= '0'){
			$where['isConsolidated']	= trim($this->input->get('isConsolidated'));
		}
		
		if(trim($this->input->get('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
		}
		if(trim($this->input->get('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$appName			= $this->globalConfig['app_name'];
		$datas				= $query->select('account1Id, account2Id, orderId, journalsId, createdJournalsId, journalTypeCode, OrderType, creditAmount, isConsolidated, status, taxDate')->get_where('cogs_journal')->result_array();	
		$report_file_name	= $appName.'-COGS-Report-'.date('Ymd').".csv";
		
		$fp					= fopen('php://output', 'w');
		$header				= array('BrightpearlAccount', 'QBOAccount', 'BPOrderID', 'JournalID', 'QBOJournalID', 'JournalType', 'OrderType', 'Amount', 'TaxDate', 'isConsolidated', 'status');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$report_file_name);
		fputcsv($fp, $header);
		$status				= array('0' => 'Pending', '1' => 'Sent', '4' => 'Archive');
		
		$account1Mappings	= array();
		$account2Mappings	= array();
		$account1MappingTemps	= $this->db->get_where('account_brightpearl_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_qbo_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$row	= array(
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
                @$data['orderId'],
                @$data['journalsId'],
                @$data['createdJournalsId'],
                @$data['journalTypeCode'],
                @$data['OrderType'],
                @$data['creditAmount'],
                date('Y-m-d',strtotime($data['taxDate'])),
                @$data['isConsolidated'],
                @$status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}