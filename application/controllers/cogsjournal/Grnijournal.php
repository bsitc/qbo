<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Grnijournal extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('cogsjournal/grnijournal_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("cogsjournal/grnijournal",$data,$this->session_data);
	}
	public function getGrnijournal(){
		$records	= $this->grnijournal_model->getGrnijournal(); 
		echo json_encode($records);
	}
	public function fetchGrnijournal($journalId = ''){
		$this->grnijournal_model->fetchGrnijournal($journalId);
	}
	public function postGrnijournal($journalId = ''){
		$this->grnijournal_model->postGrnijournal($journalId);
	}
	public function grnijournalInfo($journalId = ''){
		$data['grnijournalInfo']	= $this->db->get_where('grni_journal',array('journalsId' => $journalId))->row_array();
		$this->template->load_template("cogsjournal/grnijournalInfo",$data,$this->session_data);
	}
	public function exportGrni(){
		error_reporting('0');
		$this->db->reset_query();
		
		$where	= array();
        $query	= $this->db;
		
		if (trim($this->input->get('account1Id'))) {
			$where['account1Id']			= trim($this->input->get('account1Id'));
		}
		if (trim($this->input->get('account2Id'))) {
			$where['account2Id']			= trim($this->input->get('account2Id'));
		}
		if (trim($this->input->get('journalsId'))) {
			$where['journalsId']			= trim($this->input->get('journalsId'));
		}
		if (trim($this->input->get('createdJournalsId'))) {
			$where['createdJournalsId']		= trim($this->input->get('createdJournalsId'));
		}
		if (trim($this->input->get('reversedJournalsId'))) {
			$where['reversedJournalsId']	= trim($this->input->get('reversedJournalsId'));
		}
		if (trim($this->input->get('invoiceReference'))) {
			$where['invoiceReference']		= trim($this->input->get('invoiceReference'));
		}
		if (trim($this->input->get('orderId'))) {
			$where['orderId']				= trim($this->input->get('orderId'));
		}
		if (trim($this->input->get('journalTypeCode'))) {
			$where['journalTypeCode']		= trim($this->input->get('journalTypeCode'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']				= trim($this->input->get('status'));
		}
		if (trim($this->input->get('orderTaxdate_from'))) {
			$query->where('date(orderTaxdate) >= ', "date('" . $this->input->get('orderTaxdate_from') . "')", false);
        }
        if (trim($this->input->get('orderTaxdate_to'))) {
			$query->where('date(orderTaxdate) <= ', "date('" . $this->input->get('orderTaxdate_to') . "')", false);
		}
		if (trim($this->input->get('taxDate_from'))) {
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
        }
        if (trim($this->input->get('taxDate_to'))) {
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$datas	= $query->select('id,account1Id,account2Id,journalsId,createdJournalsId,reversedJournalsId,orderId,journalTypeCode,status,taxDate,orderTaxdate,debitAmount,created')->get_where('grni_journal')->result_array();	
		
		$account1Mappings	= array();
		$account2Mappings	= array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$allBPWarehouse			= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		$file	= date('Ymd').'_CogsReport.csv';
		$fp		= fopen('php://output', 'w');
		$header	= array('BPAccount','QBOAccount','JournalID','OrderID','QBOJournalID','QBOReverseJournalID','JournalType','TaxDate','OrderTaxdate','Amount','Status');
		
		
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$file);
		fputcsv($fp, $header);
		$Status	= array('0' => 'Pending','1' => 'Sent','3' => 'Error','4'=> 'Archive', '2' => 'Reversed');
		foreach($datas as $data){
			if(!$data['orderTaxdate']){
				$orderTaxdate = '';
			}else{
				$orderTaxdate = date('Y-m-d',strtotime($data['orderTaxdate']));
			}
			$row	= array(
				$account1Mappings[$data['account1Id']]['name'],
				$account2Mappings[$data['account2Id']]['name'],
				$data['journalsId'],
				$data['orderId'],
                $data['createdJournalsId'],
                $data['reversedJournalsId'],
                $data['journalTypeCode'],
                date('Y-m-d',strtotime($data['taxDate'])),
                $orderTaxdate,
                $data['debitAmount'],
                $Status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}