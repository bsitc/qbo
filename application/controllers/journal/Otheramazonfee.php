<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Otheramazonfee extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('journal/otheramazonfee_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("journal/otheramazonfee",$data);
	}
	public function getAmazonFeeOther(){
		$records	= $this->otheramazonfee_model->getAmazonFeeOther();
		echo json_encode($records);
	}
	public function fetchAmazonFeeOther($journalId = ''){
		$this->otheramazonfee_model->fetchAmazonFeeOther($journalId);
	}
	public function postAmazonFeeOther($journalId = ''){
		$this->otheramazonfee_model->postAmazonFeeOther($journalId);
	}
	public function amazonFeeOtherInfo($journalId = ''){
		$data['amazonFeeOtherInfo']	= $this->db->get_where('amazonFeesOther',array('journalId' => $journalId))->row_array();
		$this->template->load_template("journal/amazonFeeOtherInfo",$data,$this->session_data);
	}
}