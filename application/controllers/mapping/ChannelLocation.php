<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class ChannelLocation extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/channelLocation_model');	
	}
	public function index(){
		$data = array();
		$data = $this->channelLocation_model->get();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1LocationId']	= $this->{$this->globalConfig['account1Liberary']}->getAllSalesCustomField();
		$data['account2ChannelId']	= $this->{$this->globalConfig['account2Liberary']}->getAllChannelMethod();
		$this->template->load_template("mapping/channelLocation",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->channelLocation_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->channelLocation_model->delete($id);
		}
	}
}
?>