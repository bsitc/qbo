<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Currency extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/currency_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->currency_model->get();
		$data['account1CurrencyId']		= $this->{$this->globalConfig['account1Liberary']}->getAllCurrency();
		$data['account2DepositAccId']	= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$this->template->load_template("mapping/currency",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->currency_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->currency_model->delete($id);
		}
	}
}
?>