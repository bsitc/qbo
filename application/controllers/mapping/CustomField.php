<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class CustomField extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/Customfield_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->Customfield_model->get();
		$data['account1CustomField']	= $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();
		$data['account1CustomFieldPO']	= $this->{$this->globalConfig['account1Liberary']}->purchaseOrderFieldConfig();
		$data['account2CustomField']	= array();
		$this->template->load_template("mapping/customfield",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->Customfield_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->Customfield_model->delete($id);
		}
	}
}
?>