<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Customertype extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/customertype_model');	
	}
	public function index(){
		$data = array();
		$data = $this->customertype_model->get();
		$data['account1customertypeId'] = array(); 
		$data['account2customertypeId'] = $this->{$this->globalConfig['account2Liberary']}->getAllCustomerType();
		$this->template->load_template("mapping/customertype",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->customertype_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->customertype_model->delete($id);
		}
	}
}
?>