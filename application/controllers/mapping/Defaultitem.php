<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Defaultitem extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/defaultitem_model');
	}
	public function index(){
		$data	= array();
		$data	= $this->defaultitem_model->get();
		$data['account1ChannelId']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['itemIdentifyNominal']	= $this->{$this->globalConfig['account1Liberary']}->nominalCode();
		$this->template->load_template("mapping/defaultitem",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->defaultitem_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->defaultitem_model->delete($id);
		}
	}
}
?>