<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Nominalclass extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/nominalclass_model');	
	}
	public function index(){
		$data = array();
		$data = $this->nominalclass_model->get();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1NominalId']	= $this->{$this->globalConfig['account1Liberary']}->nominalCode();
		$data['account2ClassId']	= $this->{$this->globalConfig['account2Liberary']}->getAllChannelMethod();
		$this->template->load_template("mapping/nominalclass",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->nominalclass_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->nominalclass_model->delete($id);
		}
	}
}
?>