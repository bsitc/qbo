<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Nominalqbo extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/nominalqbo_model');	
	}
	public function index(){
		$data = array();
		$data = $this->nominalqbo_model->get();
		$data['account1NominalId'] = $this->{$this->globalConfig['account1Liberary']}->nominalCode();
		$data['account1ChannelId'] = $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2NominalId'] = $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$this->template->load_template("mapping/nominalqbo",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->nominalqbo_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->nominalqbo_model->delete($id);
		}
	}
}
?>