<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Paymentterms extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/paymentterms_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->paymentterms_model->get();
		$data['account1SalesTermId']		= $this->{$this->globalConfig['account1Liberary']}->getSalesTermsCustomField();
		$data['account1PurchaseTermId']		= $this->{$this->globalConfig['account1Liberary']}->getPurchaseTermsCustomField();
		$data['account2TermId']				= $this->{$this->globalConfig['account2Liberary']}->getAllTerms();
		$this->template->load_template("mapping/paymentterms",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->paymentterms_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->paymentterms_model->delete($id);
		}
	}
}
?>