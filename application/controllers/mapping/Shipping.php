<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Shipping extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/shipping_model');	
	}
	public function index(){
		$data = array();
		$data = $this->shipping_model->get();
		$data['account1ShippingId'] = $this->{$this->globalConfig['account1Liberary']}->getAllShippingMethod();
		$data['account2ShippingId'] = $this->{$this->globalConfig['account2Liberary']}->getAllShippingMethod();
		$this->template->load_template("mapping/shipping",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->shipping_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->shipping_model->delete($id);
		}
	}
}
?>