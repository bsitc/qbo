<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Terms extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/terms_model');	
	}
	public function index(){
		$data = array();
		$data = $this->terms_model->get();
		$data['account1TermsId'] = array(); 
		$data['account2TermsId'] = $this->{$this->globalConfig['account2Liberary']}->getAllTerms();
		$this->template->load_template("mapping/terms",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->terms_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->terms_model->delete($id);
		}
	}
}
?>