<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Warehouseclass extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/warehouseclass_model');
	}
	public function index(){
		$data	= array();
		$data	= $this->warehouseclass_model->get();
		$data['account1WarehouseId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
		$data['account2ChannelId']		= $this->{$this->globalConfig['account2Liberary']}->getAllChannelMethod();
		$this->template->load_template("mapping/warehouseclass",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->warehouseclass_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->warehouseclass_model->delete($id);
		}
	}
}
?>