<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Receipt extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('purchase/receipt_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("purchase/receipt",$data,$this->session_data);
	}
	public function getReceipt(){
		$records = $this->receipt_model->getReceipt();
		echo json_encode($records);
	}
	public function fetchReceipt($orderId = ''){
		$this->receipt_model->fetchReceipt($orderId);
	}
	public function postReceipt($orderId = ''){
		$this->receipt_model->postReceipt($orderId);
	}

	
}