<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Consolmapping extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('receipt/Consolmapping_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->Consolmapping_model->get();
		$data['account1ChannelId']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1PaymentId']		= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
		$data['account1CurrencyId']		= $this->{$this->globalConfig['account1Liberary']}->getAllCurrency();
		$data['account2ChannelId']		= array();
		$this->template->load_template("receipt/consolmapping",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->Consolmapping_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->Consolmapping_model->delete($id);
		}
	}
}
?>