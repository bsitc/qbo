<?php
//qbo refundReceipt controller
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Refundreceipt extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('receipt/'.$this->globalConfig['account2Liberary'].'/refundreceipt_overridemodel','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("receipt/refundreceipt",$data,$this->session_data);
	}
	public function getRefundreceipt(){
		$records	= $this->refundreceipt_overridemodel->getRefundreceipt();
		echo json_encode($records);
	}
	public function fetchRefundreceipt($orderId = ''){
		$this->refundreceipt_overridemodel->fetchRefundreceipt($orderId);
	}
	public function postRefundreceipt($orderId = ''){
		$this->refundreceipt_overridemodel->postRefundreceipt($orderId);
	}
	public function postConsolRefundReceipt($orderId = ''){
		$this->refundreceipt_overridemodel->postConsolRefundReceipt($orderId);
	}
	public function refundreceiptInfo($orderId = ''){
		$data['salesInfo']	= $this->db->get_where('refund_receipt',array('orderId' => $orderId))->row_array();
		$this->template->load_template("receipt/refundreceiptInfo",$data,$this->session_data);
	}
	public function refundreceiptItem($orderId){
		$data				= array();
		$data['orderInfo']	= $this->db->get_where('refund_receipt',array('orderId' => $orderId))->row_array();
		$data['address']	= $this->db->get_where('refund_receipt_address',array('orderId' => $data['orderInfo']['orderId']))->result_array();
		$data['items']		= $this->refundreceipt_overridemodel->refundreceiptItem($orderId);
		if(!$data['address']){
			$data['address']['0']			= $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1']			= $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type']	= 'ST';
			$data['address']['1']['type']	= 'BY';
		}
		$this->template->load_template("receipt/refundreceiptItem",$data,@$this->session_data); 
	}
}