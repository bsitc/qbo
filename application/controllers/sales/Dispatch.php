<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Dispatch extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('sales/dispatch_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("sales/dispatch",$data,$this->session_data);
	}
	public function getDispatch(){
		$records	= $this->dispatch_model->getDispatch();
		echo json_encode($records);
	}
	public function fetchDispatch($orderId = ''){
		$this->dispatch_model->fetchDispatch($orderId);
	}
	public function postDispatch($orderId = ''){
		$this->dispatch_model->postDispatch($orderId);
	}
}