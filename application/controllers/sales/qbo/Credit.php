<?php
//qbo credit controller
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Credit extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("sales/credit",$data,$this->session_data);
	}
	public function getCredit(){ 
		$records	= $this->credit_overridemodel->getCredit();
		echo json_encode($records);
	}
	public function fetchSalesCredit($orderId = ''){
		$this->credit_overridemodel->fetchSalesCredit($orderId);
	}
	public function postSalesCredit($orderId = ''){
		$this->credit_overridemodel->postSalesCredit($orderId);
	}
	public function postaggregationSalescredit($orderId = ''){
		$this->credit_overridemodel->postaggregationSalescredit($orderId);
	}
	public function postNetOffConsolOrder($orderId = ''){
		$this->credit_overridemodel->postNetOffConsolOrder($orderId);
	}
	public function creditInfo($orderId = ''){
		$data['salesInfo']	= $this->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("sales/creditInfo",$data,$this->session_data);
	}
	public function creditItem($orderId){
		$data				= array();
		$data['orderInfo']	= $this->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$data['address']	= $this->db->get_where('sales_credit_address',array('orderId' => $data['orderInfo']['orderId']))->result_array();
		$data['items']		= $this->credit_overridemodel->getCreditItem($orderId);
		if(!$data['address']){
			$data['address']['0']			= $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1']			= $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type']	= 'ST';
			$data['address']['1']['type']	= 'BY';
		}
		$this->template->load_template("sales/creditItem",$data,@$this->session_data); 
	}
	public function exportSalesCredit(){
		error_reporting('0');
		$where				= array();
        $query				= $this->db;
		$report_file_name	= '';
		
		
		if(trim($this->input->get('account1Id'))){
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if(trim($this->input->get('account2Id'))){
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if(trim($this->input->get('orderId'))){
			$where['orderId']			= trim($this->input->get('orderId'));                
		}
		if(trim($this->input->get('bpInvoiceNumber'))){
			$where['bpInvoiceNumber']	= trim($this->input->get('bpInvoiceNumber'));
		}
		if(trim($this->input->get('createOrderId'))){
			$where['createOrderId']		= trim($this->input->get('createOrderId'));
		}
		if(trim($this->input->get('channelName'))){
			$where['channelName']		= trim($this->input->get('channelName'));
		}
		if(trim($this->input->get('delAddressName'))){
			$where['delAddressName']	= trim($this->input->get('delAddressName'));
		}
		if(trim($this->input->get('customerEmail'))){
			$where['customerEmail']		= trim($this->input->get('customerEmail'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']			= trim($this->input->get('status'));
		}
		if(trim($this->input->get('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
		}
		if(trim($this->input->get('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$appName			= $this->globalConfig['app_name'];
		$datas				= $query->select('account1Id, account2Id, orderId, bpInvoiceNumber, createOrderId, channelName, delAddressName, customerEmail, totalAmount, status, taxDate')->get_where('sales_credit_order')->result_array();	
		$report_file_name	= $appName.'-SC-Report-'.date('Ymd').".csv";
		
		$fp					= fopen('php://output', 'w');
		$header				= array('BrightpearlAccount', 'QBOAccount', 'BPOrderID', 'CreditNumber', 'QBOOrderID', 'Channel', 'Customer', 'Email', 'Total', 'TaxDate', 'Status');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$report_file_name);
		fputcsv($fp, $header);
		$status				= array('0' => 'Pending', '1' => 'Order Sent', '2' => 'Invoice Created', '3' => 'Payment Created', '4' => 'Archive');
		
		$account1Mappings	= array();
		$account2Mappings	= array();
		$account1MappingTemps	= $this->db->get_where('account_brightpearl_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_qbo_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$row	= array(
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
                @$data['orderId'],
                @$data['bpInvoiceNumber'],
                @$data['createOrderId'],
                @$data['channelName'],
                @$data['delAddressName'],
                @$data['customerEmail'],
                @$data['totalAmount'],
                date('Y-m-d',strtotime($data['taxDate'])),
				@$status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}