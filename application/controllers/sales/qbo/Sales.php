<?php
//qbo sales controller
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Sales extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("sales/sales",$data,$this->session_data);
	}
	public function getSales(){
		$records	= $this->sales_overridemodel->getSales(); 
		echo json_encode($records);
	}
	public function fetchSales($orderId = ''){
		$this->sales_overridemodel->fetchSales($orderId);
	}
	public function postSales($orderId = ''){
		$this->sales_overridemodel->postSales($orderId);
	}
	public function postaggregationSales($orderId = ''){
		$this->sales_overridemodel->postaggregationSales($orderId);
	}
	public function postNetOffConsolOrder($orderId = ''){ 
		$this->sales_overridemodel->postNetOffConsolOrder($orderId);
	}
	public function salesInfo($orderId = ''){
		$data['salesInfo']	= $this->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("sales/salesInfo",$data,$this->session_data);
	}
	public function salesItem($orderId){
		$data				= array();
		$data['orderInfo']	= $this->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
		$data['address']	= $this->db->order_by('id','desc')->limit(2,0)->get_where('sales_address',array('orderId' => $orderId))->result_array();
		$data['items']		= $this->sales_overridemodel->getSalesItem($orderId);
		if(!$data['address']){
			$data['address']['0']			= $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1']			= $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type']	= 'ST';
			$data['address']['1']['type']	= 'BY';
		}
		$this->template->load_template("sales/salesItem",$data,@$this->session_data); 
	}
	public function exportSales(){
		error_reporting('0');
		$where				= array();
        $query				= $this->db;
		$report_file_name	= '';
		
		
		if(trim($this->input->get('account1Id'))){
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if(trim($this->input->get('account2Id'))){
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if(trim($this->input->get('orderId'))){
			$where['orderId']			= trim($this->input->get('orderId'));                
		}
		if(trim($this->input->get('bpInvoiceNumber'))){
			$where['bpInvoiceNumber']	= trim($this->input->get('bpInvoiceNumber'));
		}
		if(trim($this->input->get('createOrderId'))){
			$where['createOrderId']		= trim($this->input->get('createOrderId'));
		}
		if(trim($this->input->get('channelName'))){
			$where['channelName']		= trim($this->input->get('channelName'));
		}
		if(trim($this->input->get('delAddressName'))){
			$where['delAddressName']	= trim($this->input->get('delAddressName'));
		}
		if(trim($this->input->get('customerEmail'))){
			$where['customerEmail']		= trim($this->input->get('customerEmail'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']			= trim($this->input->get('status'));
		}
		if(trim($this->input->get('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
		}
		if(trim($this->input->get('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$appName			= $this->globalConfig['app_name'];
		$datas				= $query->select('account1Id, account2Id, orderId, bpInvoiceNumber, createOrderId, channelName, delAddressName, customerEmail, totalAmount, status, taxDate')->get_where('sales_order')->result_array();	
		$report_file_name	= $appName.'-SO-Report-'.date('Ymd').".csv";
		
		$fp					= fopen('php://output', 'w');
		$header				= array('BrightpearlAccount', 'QBOAccount', 'BPOrderID', 'InvoiceNumber', 'QBOOrderID', 'Channel', 'Customer', 'Email', 'Total', 'TaxDate', 'Status');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$report_file_name);
		fputcsv($fp, $header);
		$status				= array('0' => 'Pending', '1' => 'Order Sent', '2' => 'Invoice Created', '3' => 'Payment Created', '4' => 'Archive');
		
		$account1Mappings	= array();
		$account2Mappings	= array();
		$account1MappingTemps	= $this->db->get_where('account_brightpearl_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_qbo_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$row	= array(
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
                @$data['orderId'],
                @$data['bpInvoiceNumber'],
                @$data['createOrderId'],
                @$data['channelName'],
                @$data['delAddressName'],
                @$data['customerEmail'],
                @$data['totalAmount'],
                date('Y-m-d',strtotime($data['taxDate'])),
				@$status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}