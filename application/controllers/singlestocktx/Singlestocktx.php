<?php
//qbo
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Singlestocktx extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('singlestocktx/singlestocktx_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("singlestocktx/singlestocktx",$data);
	}
	public function getSinglestocktx(){
		$records	= $this->singlestocktx_model->getSinglestocktx();
		echo json_encode($records);
	}
	public function fetchSinglestocktx($sku = ''){
		$this->singlestocktx_model->fetchSinglestocktx($sku);
	}
	public function postSinglestocktx($sku = ''){
		$this->singlestocktx_model->postSinglestocktx($sku);
	}
	public function singlestocktxInfo($recordId = ''){
		$data['salesInfo']	= $this->db->get_where('single_company_stock_transfers',array('id' => $recordId))->row_array();
		$this->template->load_template("singlestocktx/singlestocktxInfo",$data,$this->session_data);
	}
	
	public function exportReport(){
		error_reporting('0');
		
		$this->db->reset_query();
		$allUniqueProducts		= $this->db->distinct('productId')->select('productId')->get_where('single_company_stock_transfers',array('productId <>' => ''))->result_array();
		$allUniqueProductsIds	= array();
		$allUniqueProductsIds	= array_column($allUniqueProducts,'productId');
		
		$this->db->reset_query();
		$productMappings		= array();
		$productMappingsSKU		= array();
		if($allUniqueProductsIds){
			$this->db->where_in('productId',$allUniqueProductsIds);
		}
		$productMappingsTemps	= $this->db->select('productId,sku')->get_where('products',array('productId <>' => ''))->result_array();
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			$productMappingsSKU[$productMappingsTemp['sku']]	= $productMappingsTemp;
		}
		
		
		$where				= array();
        $query				= $this->db;
		
		if (trim($this->input->get('account1Id'))) {
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if (trim($this->input->get('account2Id'))) {
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if (trim($this->input->get('goodsMovementId'))) {
			$where['goodsMovementId']	= trim($this->input->get('goodsMovementId'));
		}
		if (trim($this->input->get('goodsNoteId'))) {
			$where['goodsNoteId']		= trim($this->input->get('goodsNoteId'));
		}
		if (trim($this->input->get('stockTransferId'))) {
			$where['stockTransferId']	= trim($this->input->get('stockTransferId'));
		}
		if (trim($this->input->get('createdOrderId'))) {
			$where['createdOrderId']	= trim($this->input->get('createdOrderId'));
		}
		if(trim($this->input->get('sourceWarehouseId'))){
			$where['sourceWarehouseId']	= trim($this->input->get('sourceWarehouseId'));
		}
		if(trim($this->input->get('targetWarehouseId'))){
			$where['targetWarehouseId']	= trim($this->input->get('targetWarehouseId'));
		}
		if (trim($this->input->get('productId'))) {
			$where['productId']			= trim($this->input->get('productId'));
		}
		if(trim($this->input->get('sku'))){
			if($productMappingsSKU[trim($this->input->get('sku'))]){
				$checkProductID			= $productMappingsSKU[trim($this->input->get('sku'))]['productId'];
				$where['productId']		= $checkProductID;
			}
		}
		if (trim($this->input->get('qty'))) {
			$where['qty']				= trim($this->input->get('qty'));
		}
		if (trim($this->input->get('price'))) {
			$where['price']				= trim($this->input->get('price'));
		}
		if (trim($this->input->get('totalValue'))) {
			$where['totalValue']		= trim($this->input->get('totalValue'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']			= trim($this->input->get('status'));
		}
		if (trim($this->input->get('shippedOn_from'))) {
			$query->where('date(shippedOn) >= ', "date('" . $this->input->get('shippedOn_from') . "')", false);
        }
        if (trim($this->input->get('shippedOn_to'))) {
			$query->where('date(shippedOn) <= ', "date('" . $this->input->get('shippedOn_to') . "')", false);
		}
		if (trim($this->input->get('created_from'))) {
			$query->where('date(created) >= ', "date('" . $this->input->get('created_from') . "')", false);
        }
        if (trim($this->input->get('created_to'))) {
			$query->where('date(created) <= ', "date('" . $this->input->get('created_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$datas	= $query->get_where('single_company_stock_transfers')->result_array();	
		
		$account1Mappings = array();
		$account2Mappings = array();
		$allMappedWarehouseName = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSingleCompanyStocktx'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSingleCompanyStocktx'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$allBPWarehouse			= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		$file	= date('Ymd').'_StockTransfersReport.csv';
		$fp		= fopen('php://output', 'w');
		$header	= array('BPAccount','QBOAccount','goodsMovementId','goodsNoteId','stockTransferId', 'QBOID', 'sourceWarehouse','targetWarehouse','productId','sku','qty','price','totalValue','shippedOn','created','status');
		
		
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$file);
		fputcsv($fp, $header);
		$Status	= array('0' => 'Pending','1' => 'Sent','3' => 'Error','4'=> 'Archive');
		foreach($datas as $data){
			$row	= array(
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				@$data['goodsMovementId'],
				@$data['goodsNoteId'],
                @$data['stockTransferId'],
                @$data['createdOrderId'],
                $allMappedWarehouseName[$data['sourceWarehouseId']],
                $allMappedWarehouseName[$data['targetWarehouseId']],
                @$data['productId'],
				$productMappings[$data['productId']]['sku'],
                @$data['qty'],
                @$data['price'],
				@$data['totalValue'],
                date('Y-m-d',strtotime($data['shippedOn'])),
                date('Y-m-d',strtotime($data['created'])),
                @$Status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}