<?php
//qbo
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Adjustment extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('stock/adjustment_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("stock/adjustment",$data);
	}
	public function getAdjustment(){
		$records	= $this->adjustment_model->getAdjustment();
		echo json_encode($records);
	}
	public function fetchStockAdjustment($sku = ''){
		$this->adjustment_model->fetchStockAdjustment($sku);
	}
	public function postStockAdjustment($sku = ''){
		$this->adjustment_model->postStockAdjustment($sku);
	}
	public function adjustmentInfo($recordId = ''){
		$data['salesInfo']	= $this->db->get_where('stock_adjustment',array('id' => $recordId))->row_array();
		$this->template->load_template("stock/adjustmentinfo",$data,$this->session_data);
	}
	public function exportStocks(){
		error_reporting('0');
		$this->db->reset_query();
		$productMappings		= array();
		$productMappingsSKU		= array();
		$productMappingsTemps	= $this->db->select('productId,sku')->get_where('products',array('productId <>' => ''))->result_array();
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			$productMappingsSKU[$productMappingsTemp['sku']]	= $productMappingsTemp;
		}
		
		
		$where				= array();
        $query				= $this->db;
		
		if(trim($this->input->get('sku'))){
			if($productMappingsSKU[trim($this->input->get('sku'))]){
				$checkProductID		= $productMappingsSKU[trim($this->input->get('sku'))]['productId'];
				$where['productId']	= $checkProductID;
				
			}
		}
		if (trim($this->input->get('account1Id'))) {
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if (trim($this->input->get('account2Id'))) {
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if (trim($this->input->get('orderId'))) {
			$where['orderId']			= trim($this->input->get('orderId'));
		}
		if (trim($this->input->get('ActivityId'))) {
			$where['ActivityId']		= trim($this->input->get('ActivityId'));
		}
		if (trim($this->input->get('productId'))) {
			$where['productId']			= trim($this->input->get('productId'));
		}
		if (trim($this->input->get('qty'))) {
			$where['qty']				= trim($this->input->get('qty'));
		}
		if (trim($this->input->get('price'))) {
			$where['price']				= trim($this->input->get('price'));
		}
		if (trim($this->input->get('GoodNotetype'))) {
			$where['GoodNotetype']		= trim($this->input->get('GoodNotetype'));
		}
		if (trim($this->input->get('goodsNoteId'))) {
			$where['goodsNoteId']		= trim($this->input->get('goodsNoteId'));
		}
		if (trim($this->input->get('createdOrderId'))) {
			$where['createdOrderId']	= trim($this->input->get('createdOrderId'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']			= trim($this->input->get('status'));
		}
		if (trim($this->input->get('updated_from'))) {
			$query->where('date(created) >= ', "date('" . $this->input->get('updated_from') . "')", false);
        }
        if (trim($this->input->get('updated_to'))) {
			$query->where('date(created) <= ', "date('" . $this->input->get('updated_to') . "')", false);
		}
		if(trim($this->input->get('warehouseId'))){
			$query->where('warehouseId', $this->input->get('warehouseId'));
		}
		if ($where) {
            $query->like($where);
        }
		$datas	= $query->get_where('stock_adjustment')->result_array();	
		
		$account1Mappings = array();
		$account2Mappings = array();
		$allMappedWarehouseName = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$allBPWarehouse			= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		$file	= 'StockReport.csv';
		$fp		= fopen('php://output', 'w');
		$header	= array('BPAccount','QBOAccount','GoodsMovementID','GoodsNoteId','StockTransferID', 'QBOID', 'Warehouse','ProductID','SKU','Quantity','Price','Value','GoodSNoteType','Created','Status');
		
		
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$file);
		fputcsv($fp, $header);
		$Status	= array('0' => 'Pending','1' => 'Sent','3' => 'Error','4'=> 'Archive');
		foreach($datas as $data){
			$transfertype	= 'Inventory Adjustment';
			if($data['GoodNotetype'] == 'GO'){
				$transfertype	= 'Inventory Transfer';
			}
			
			$row	= array(
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				@$data['orderId'],
				@$data['goodsNoteId'],
                @$data['ActivityId'],
                @$data['createdOrderId'],
                $allMappedWarehouseName[$data['warehouseId']],
                @$data['productId'],
				$productMappings[$data['productId']]['sku'],
                @$data['qty'],
                @$data['price'],
				@($data['price'] * $data['qty']),
                @$transfertype,
                date('Y-m-d',strtotime($data['created'])),
                @$Status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}