<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Sync extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('stock/sync_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("stock/sync",$data);
	}
	public function getSync(){
		$records	= $this->sync_model->getSync();
		echo json_encode($records);
	}
	public function fetchSync($sku = ''){
		$this->sync_model->fetchSync($sku);
	}
	public function postSync($sku = ''){
		$this->sync_model->postSync($sku);
	}
}