<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Synclog extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('stock/synclog_model','',TRUE);
	}	
	public function index(){
		$data		= array();
		$this->template->load_template("stock/synclog",$data);
	}
	public function getSynclog(){
		$records	= $this->synclog_model->getSynclog();
		echo json_encode($records);
	}
}