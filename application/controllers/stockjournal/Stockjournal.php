<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Stockjournal extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("stockjournal/stockjournal",$data,$this->session_data);
	}
	public function getStockjournal(){
		$records	= $this->stockjournal_model->getStockjournal(); 
		echo json_encode($records);
	}
	public function fetchStockjournal($journalId = ''){
		$this->stockjournal_model->fetchStockjournal($journalId);
	}
	public function postConsolStockjournal($journalId = ''){
		$this->stockjournal_model->postConsolStockjournal($journalId);
	}
	public function stockjournalinfo($journalId = ''){
		$data['stockjournalinfo']	= $this->db->get_where('stock_journals',array('journalId' => $journalId))->row_array();
		$this->template->load_template("stockjournal/stockjournalinfo",$data,$this->session_data);
	}
	public function exportdata(){
		error_reporting('0');
		$where	= array();
        $query	= $this->db;
		
		if (trim($this->input->get('account1Id'))) {
			$where['account1Id']			= trim($this->input->get('account1Id'));
		}
		if (trim($this->input->get('account2Id'))) {
			$where['account2Id']			= trim($this->input->get('account2Id'));
		}
		if (trim($this->input->get('journalId'))) {
			$where['journalId']				= trim($this->input->get('journalId'));
		}
		if (trim($this->input->get('created_journalId'))) {
			$where['created_journalId']		= trim($this->input->get('created_journalId'));
		}
		if (trim($this->input->get('InvoiceNumber'))) {
			$where['InvoiceNumber']			= trim($this->input->get('InvoiceNumber'));
		}
		if (trim($this->input->get('credit_nominalCode'))) {
			$where['credit_nominalCode']	= trim($this->input->get('credit_nominalCode'));
		}
		if (trim($this->input->get('debit_nominalCode'))) {
			$where['debit_nominalCode']		= trim($this->input->get('debit_nominalCode'));
		}
		if (trim($this->input->get('total_amt'))) {
			$where['total_amt']				= trim($this->input->get('total_amt'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']				= trim($this->input->get('status'));
		}
		if (trim($this->input->get('message'))) {
			$where['message']			= trim($this->input->get('message'));
		}
		if (trim($this->input->get('taxDate_from'))) {
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
        }
        if (trim($this->input->get('taxDate_to'))) {
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if (trim($this->input->get('createdOn_from'))) {
			$query->where('date(createdOn) >= ', "date('" . $this->input->get('createdOn_from') . "')", false);
        }
        if (trim($this->input->get('createdOn_to'))) {
			$query->where('date(createdOn) <= ', "date('" . $this->input->get('createdOn_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$datas	= $query->select('id, account1Id, account2Id, journalId, created_journalId, InvoiceNumber, credit_nominalCode, debit_nominalCode, total_amt, status, message, taxDate, createdOn')->get_where('stock_journals')->result_array();	
		
		$account1Mappings = array();
		$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchStockJournal'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postStockJournal'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		
		$file	= date('Ymd').'-StockAdjustmentJournalReport.csv';
		$fp		= fopen('php://output', 'w');
		$header	= array('Brightpearl', 'QBO', 'Journal ID', 'QBO Journal ID', 'DocNumber', 'Credit Nominal', 'Debit Nominal', 'Total Amount', 'TaxDate', 'CreatedOn', 'Status', 'Message');
		
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$file);
		fputcsv($fp, $header);
		$Status	= array('0' => 'Pending','1' => 'Sent','3' => 'Error','4'=> 'Archive');
		foreach($datas as $data){
			$row	= array(
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				@$data['journalId'],
				@$data['created_journalId'],
                @$data['InvoiceNumber'],
                @$data['credit_nominalCode'],
                @$data['debit_nominalCode'],
                @$data['total_amt'],
                date('Y-m-d',strtotime($data['taxDate'])),
                date('Y-m-d',strtotime($data['createdOn'])),
                @$Status[$data['status']],
                @$data['message'],
			);
			fputcsv($fp, $row);
		}
	}
}