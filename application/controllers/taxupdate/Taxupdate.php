<?php 
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Taxupdate extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('taxupdate/taxupdate_model');
	}
	public function index(){
		$data	= array();
		$data	= $this->taxupdate_model->get();
		$this->template->load_template("taxupdate/taxupdate",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->taxupdate_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->taxupdate_model->delete($id);
		}
	}
}