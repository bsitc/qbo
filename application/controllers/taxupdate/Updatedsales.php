<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Updatedsales extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('taxupdate/updatedsales_model');
	}
	public function index(){
		$data		= array();
		$this->template->load_template("taxupdate/updatedsales",$data,$this->session_data);
	}
	public function fetchAllSalesForUpdate($orderId = ''){
		$this->updatedsales_model->fetchAllSalesForUpdate($orderId);
		$this->updatedsales_model->fetchAllCreditForUpdate($orderId);
	}
	public function postAllUpdatedSales($orderId = ''){
		$this->updatedsales_model->postAllUpdatedSales($orderId);
		$this->updatedsales_model->postAllUpdatedCredit($orderId);
	}
	public function postAllUpdatedSales1($orderId = ''){
		$this->updatedsales_model->postAllUpdatedSales($orderId);
	}
	public function postAllUpdatedCredit($orderId = ''){
		$this->updatedsales_model->postAllUpdatedCredit($orderId);
	}
	public function getupdatedsales(){
		$records	= $this->updatedsales_model->getupdatedsales(); 
		echo json_encode($records);
	}
	public function updatedsalesInfo($orderId = ''){
		$data['updatedsalesInfo']	= $this->db->get_where('sales_order_customisation',array('orderId' => $orderId))->row_array();
		$this->template->load_template("taxupdate/updatedsalesInfo",$data,$this->session_data);
	}
}
?>