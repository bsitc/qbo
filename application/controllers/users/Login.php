<?php
//qbo
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('users/login_model', '', true);
		$this->load->library('form_validation');
		$this->session->set_userdata('global_config', $this->db->get('global_config')->row_array());
	}
	public function index(){
		$user_session_data	= $this->session->userdata('login_user_data');
		if(isset($user_session_data['username']) && $user_session_data['username'] != ""){
			redirect('dashboard', 'refresh');
		}
		else{
			$this->load->helper(array('form'));
			$this->load->view('users/login');
		}
	}
	public function checkLogin(){
		$username	= $this->input->post('username');
		$password	= $this->input->post('password');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|require');
		$result		= $this->login_model->login($username, $password);
		if($result){
			echo "1";
			$this->check_database($password);
		}
		else{
			echo "0";
		}
		die();
	}
	public function submit(){
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
		if($this->form_validation->run() == false){
			$this->load->view('users/login');
		}
		else{
			redirect('dashboard', 'refresh');
		}
	}
	public function check_database($password){
		$username	= $this->input->post('username');
		$result		= $this->login_model->login($username, $password);
		if($result){
			$sess_array	= array();
			foreach($result as $row){
				if($row->is_active == 1){
					$sess_array		= array(
						'user_id'		=> $row->user_id,
						'firstname'		=> $row->firstname,
						'lastname'		=> $row->lastname,
						'email'			=> $row->email,
						'username'		=> $row->username,
						'role'			=> $row->role,
						'profileimage'	=> $this->config->item('base_url').'/logos/bp_qbo.png',
					);
					$data			= array(
						'user_id'		=> $row->user_id,
						'logdate'		=> date('Y-m-d H:i:s'),
						'lognum'		=> $row->lognum + 1,
					);
					$this->login_model->update($data);
					$this->session->set_userdata('login_user_data', $sess_array);
					return true;
				}
				else{
					$this->form_validation->set_message('check_database', 'Your account is inactive,contact to administrator');
					return false;
				}
			}
		}
		else{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('', '');
	}
}