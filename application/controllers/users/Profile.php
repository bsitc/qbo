<?php
//qbo
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Profile extends My_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('users/profile_model');
	}
	public function index(){
		$data	= array(
			'user'				=> $this->profile_model->getUserDetails(),
			'globalConfig'		=> $this->profile_model->getGlobalConfig(),
			'AutomationConfig'	=> $this->profile_model->getAutomationConfig(),
		);
		$this->template->load_template("users/profile", array('data' => $data), $this->session_data);
	}
	public function saveBasic(){
		$data	= array(
			'firstname'	=> $this->input->post('firstname'),
			'lastname'	=> $this->input->post('lastname'),
			'phone'		=> $this->input->post('phone'),
			'email'		=> $this->input->post('email'),
		);
		$this->profile_model->saveBasic($data);
		$return	= array(
			'status'	=> true,
			'message'	=> 'Data saved successfully',
		);
		echo json_encode($return);
		die();
	}
	public function updatePassword(){
		$data	= array(
			'password'		=> $this->input->post('password'),
			'newpassword'	=> $this->input->post('newpassword'),
			'newpassword2'	=> $this->input->post('newpassword2'),
		);
		$message	= 'Password changed successfully';
		$status		= '1';
		if(trim($data['password']) == ''){
			$message	= 'Please enter Password';
			$status		= 0;
		}
		elseif(trim($data['newpassword']) == ''){
			$message	= 'Please enter New Password';
			$status		= 0;
		}
		elseif(trim($data['newpassword']) != trim($data['newpassword2'])){
			$message	= 'New Password and Re-type New Password not matched';
			$status		= 0;
		}
		else{
			$res	= $this->profile_model->updatePassword($data);
			if($res){
				$message	= 'Data saved successfully';
				$status		= 1;
			}
			else{
				$message	= 'Problem in saving data';
				$status		= 0;
			}
		}
		$return	= array(
			'status'	=> $status,
			'message'	=> $message,
		);
		echo json_encode($return);
		die();
	}
	public function updateProfilePic(){
		$img		= $this->input->post('file');
		$return		= array(
			'status'	=> '0',
			'message'	=> "Please select file",
		);
		if($img){
			$uploadFileName	= uniqid() . '.png';
			$img			= str_replace('data:image/png;base64,', '', $img);
			if(substr_count($img, 'image/jpg')){
				$uploadFileName	= uniqid() . '.jpg';
				$img			= str_replace('data:image/jpg;base64,', '', $img);
			}
			$img		= str_replace(' ', '+', $img);
			$data		= base64_decode($img);
			$file		= FCPATH . 'assets/layouts/layout/img/profile/' . $uploadFileName;
			$success	= file_put_contents($file, $data);
			$return		= array(
				'status'	=> '1',
				'message'	=> "Profile image successfully uploaded",
			);
			$saveData	= array('profileimage' => 'assets/layouts/layout/img/profile/' . $uploadFileName);
			$this->profile_model->uploadedProfiePic($saveData);
		}
		echo json_encode($return);
		die();
	}
	public function saveGlobalConfig(){
		$mappings	= $this->config->item('mapping');
		$data		= array(
			'app_name'							=> $this->input->post('app_name'),
			'account1Name'						=> $this->input->post('account1Name'),
			'account1Liberary'					=> $this->input->post('account1Liberary'),
			'account2Name'						=> $this->input->post('account2Name'),
			'account2Liberary'					=> $this->input->post('account2Liberary'),
			'enableProduct'						=> $this->input->post('enableProduct'),
			'fetchProduct'						=> $this->input->post('fetchProduct'),
			'postProduct'						=> $this->input->post('postProduct'),
			'enablePrebook'						=> $this->input->post('enablePrebook'),
			'enableCustomer'					=> $this->input->post('enableCustomer'),
			'fetchCustomer'						=> $this->input->post('fetchCustomer'),
			'postCustomer'						=> $this->input->post('postCustomer'),
			'enableSalesOrder'					=> $this->input->post('enableSalesOrder'),
			'fetchSalesOrder'					=> $this->input->post('fetchSalesOrder'),
			'postSalesOrder'					=> $this->input->post('postSalesOrder'),
			'enableReceipt'						=> $this->input->post('enableReceipt'),
			'fetchReceipt'						=> $this->input->post('fetchReceipt'),
			'postReceipt'						=> $this->input->post('postReceipt'),
			'enableDispatchConfirmation'		=> $this->input->post('enableDispatchConfirmation'),
			'fetchDispatchConfirmation'			=> $this->input->post('fetchDispatchConfirmation'),
			'postDispatchConfirmation'			=> $this->input->post('postDispatchConfirmation'),
			'enablePurchaseOrder'				=> $this->input->post('enablePurchaseOrder'),
			'fetchPurchaseOrder'				=> $this->input->post('fetchPurchaseOrder'),
			'postPurchaseOrder'					=> $this->input->post('postPurchaseOrder'),
			'enableStockAdjustment'				=> $this->input->post('enableStockAdjustment'),
			'fetchStockAdjustment'				=> $this->input->post('fetchStockAdjustment'),
			'postStockAdjustment'				=> $this->input->post('postStockAdjustment'),
			'enableStockSync'					=> $this->input->post('enableStockSync'),
			'fetchStockSync'					=> $this->input->post('fetchStockSync'),
			'enblePreorder'						=> $this->input->post('enblePreorder'),
			'postStockSync'						=> $this->input->post('postStockSync'),
			'enableMapping'						=> $this->input->post('enableMapping'),
			'enableInventoryadvice'				=> $this->input->post('enableInventoryadvice'),
			'fetchInventoradvice'				=> $this->input->post('fetchInventoradvice'),
			'postInventoradvice'				=> $this->input->post('postInventoradvice'),
			'enableSalesCredit'					=> $this->input->post('enableSalesCredit'),
			'fetchSalesCredit'					=> $this->input->post('fetchSalesCredit'),
			'postSalesCredit'					=> $this->input->post('postSalesCredit'),
			'enablePurchaseCredit'				=> $this->input->post('enablePurchaseCredit'),
			'fetchPurchaseCredit'				=> $this->input->post('fetchPurchaseCredit'),
			'postPurchaseCredit'				=> $this->input->post('postPurchaseCredit'),
			'enableInventoryManagement'			=> $this->input->post('enableInventoryManagement'),
			'enableamazonfee'					=> $this->input->post('enableamazonfee'),
			'enableAmazonFeeOther'				=> $this->input->post('enableAmazonFeeOther'),
			'enableAdvanceTaxMapping'			=> $this->input->post('enableAdvanceTaxMapping'),
			'enableSendSCasPO'					=> $this->input->post('enableSendSCasPO'),
			'enableAggregation'					=> $this->input->post('enableAggregation'),
			'enableInventoryTransfer'			=> $this->input->post('enableInventoryTransfer'),
			'enableTaxCustomisation'			=> $this->input->post('enableTaxCustomisation'),
			'enableCOGSJournals'				=> $this->input->post('enableCOGSJournals'),
			'enableRefundReceipt'				=> $this->input->post('enableRefundReceipt'),
			'fetchRefundReceipt'				=> $this->input->post('fetchRefundReceipt'),
			'postRefundReceipt'					=> $this->input->post('postRefundReceipt'),
			'enableGRNIjournal'					=> $this->input->post('enableGRNIjournal'),
			'enableAggregationOnAPIfields'		=> $this->input->post('enableAggregationOnAPIfields'),
			
			
			'disableSOpaymentbptoqbo'					=> $this->input->post('disableSOpaymentbptoqbo'),
			'disableSOpaymentqbotobp'					=> $this->input->post('disableSOpaymentqbotobp'),
			'disableSCpaymentbptoqbo'					=> $this->input->post('disableSCpaymentbptoqbo'),
			'disableSCpaymentqbotobp'					=> $this->input->post('disableSCpaymentqbotobp'),
			'disablePOpaymentqbotobp'					=> $this->input->post('disablePOpaymentqbotobp'),
			'disablePCpaymentqbotobp'					=> $this->input->post('disablePCpaymentqbotobp'),
			'disableConsolSOpayments'					=> $this->input->post('disableConsolSOpayments'),
			'disableConsolSCpayments'					=> $this->input->post('disableConsolSCpayments'),
			'emailReceipents'							=> $this->input->post('emailReceipents'),
			'smtpUsername'								=> $this->input->post('smtpUsername'),
			'smtpPassword'								=> $this->input->post('smtpPassword'),
			'autoArchivePeriod'							=> $this->input->post('autoArchivePeriod'),
			'archiveType'								=> $this->input->post('archiveType'),
			'enableAggregationAdvance'					=> $this->input->post('enableAggregationAdvance'),
			'enableConsolidationMappingCustomazation'	=> $this->input->post('enableConsolidationMappingCustomazation'),
			'enableNetOffConsol'						=> $this->input->post('enableNetOffConsol'),
			'enableGenericCustomerMappingCustomazation'	=> $this->input->post('enableGenericCustomerMappingCustomazation'),
			
			'enableSingleCompanyStocktx'				=> $this->input->post('enableSingleCompanyStocktx'),
			'fetchSingleCompanyStocktx'					=> $this->input->post('fetchSingleCompanyStocktx'),
			'postSingleCompanyStocktx'					=> $this->input->post('postSingleCompanyStocktx'),
			
			'enableConsolStockAdjustment'				=> $this->input->post('enableConsolStockAdjustment'),
			'fetchStockJournal'							=> $this->input->post('fetchStockJournal'),
			'postStockJournal'							=> $this->input->post('postStockJournal'),
			'enableRefundReceiptConsol'					=> $this->input->post('enableRefundReceiptConsol'),
		);
		foreach($mappings as $key => $mapping){
			$valKey			= 'enable'.ucwords($key).'Mapping';
			$data[$valKey]	= @(int)$this->input->post($valKey);
		}
		$this->profile_model->saveGlobalConfig($data);
		$return	= array(
			'status'	=> '1',
			'message'	=> "Connector configuration saved successfully",
		);
		echo json_encode($return);
		die();
	}
	public function updateMetadata(){
		$orderTypeArray		= array("sales_order","sales_credit_order","purchase_order","purchase_credit_order");
		$updateTypeArray	= array("customers","products");
		$data				= array(
			'Ids'				=> trim($this->input->post('Ids')),
			'datatype'			=> trim($this->input->post('datatype')),
			'actionRequired'	=> trim($this->input->post('actionRequired')),
			'account2Name'		=> trim($this->input->post('account2Name')),
		);
		$message	= 'Data Changed successfully';
		$status		= '1';
		if(trim($data['Ids']) == ''){
			$message	= 'Please enter IDs';
			$status		= 0;
		}
		elseif($data['datatype'] == ''){
			$message	= 'Please Select DataType';
			$status		= 0;
		}
		elseif($data['actionRequired'] == ''){
			$message	= 'Please Select Action';
			$status		= 0;
		}
		elseif($data['actionRequired'] == ''){
			$message	= 'Please Select Action';
			$status		= 0;
		}
		elseif((($data['actionRequired'] == '111') OR ($data['actionRequired'] == '3')) AND (!in_array($data['datatype'], $orderTypeArray))){
			$message	= 'Action Not Supported for selected DataType';
			$status		= 0;
		}
		elseif((($data['actionRequired'] == '222') OR ($data['actionRequired'] == '2')) AND (!in_array($data['datatype'], $updateTypeArray))){
			$message	= 'Action Not Supported for selected DataType';
			$status		= 0;
		}
		elseif(($data['actionRequired'] == '222') AND (!$data['account2Name'])){
			$message	= 'Select Account 2 For this Action';
			$status		= 0;
		}
		else{
			$res	= $this->profile_model->updateMetadata($data);
			if($res){
				$message	= 'Data Changed successfully';
				$status		= 1;
			}
			else{
				$message	= 'Problem in updating data';
				$status		= 0;
			}
		}
		$return		= array(
			'status'	=> $status,
			'message'	=> $message,
		);
		echo json_encode($return);
		die();
	}
	public function updateDataIds(){
		$data			= array(
			'brightpearlId'	=> trim($this->input->post('brightpearlId')),
			'newupdateId'	=> trim($this->input->post('newupdateId')),
			'datatype'		=> trim($this->input->post('datatype')),
			'account2Name'	=> trim($this->input->post('account2Name')),
		);
		$message	= 'Data Changed successfully';
		$status		= '1';
		if(trim($data['brightpearlId']) == ''){
			$message	= 'Please enter Brightpearl ID';
			$status		= 0;
		}
		elseif(trim($data['newupdateId']) == ''){
			$message	= 'Please enter New Updated ID';
			$status		= 0;
		}
		elseif(trim($data['datatype']) == ''){
			$message	= 'Please Select DataType';
			$status		= 0;
		}
		elseif(trim($data['account2Name']) == ''){
			$message	= 'Please Select Account 2';
			$status		= 0;
		}
		else{
			$res	= $this->profile_model->updateDataIds($data);
			if($res){
				$message	= 'Data Changed successfully';
				$status		= 1;
			}
			else{
				$message	= 'Problem in updating data';
				$status		= 0;
			}
		}
		$return		= array(
			'status'	=> $status,
			'message'	=> $message,
		);
		echo json_encode($return);
		die();
	}
	public function saveAutomation(){
		$data	= array(
			'enableSA'				=> $this->input->post('enableSA'),
			'enableSO'				=> $this->input->post('enableSO'),
			'enableSC'				=> $this->input->post('enableSC'),
			'enablePO'				=> $this->input->post('enablePO'),
			'enablePC'				=> $this->input->post('enablePC'),
			'enableAmazonFeesAuto'	=> $this->input->post('enableAmazonFeesAuto'),
			'enableCogsAuto'		=> $this->input->post('enableCogsAuto'),
			'enableConsolSO'		=> $this->input->post('enableConsolSO'),
			'enableConsolSC'		=> $this->input->post('enableConsolSC'),
			'enableConsolCogsAuto'	=> $this->input->post('enableConsolCogsAuto'),
		);
		$this->profile_model->saveAutomation($data);
		$return		= array(
			'status'	=> true,
			'message'	=> 'Data saved successfully',
		);
		echo json_encode($return);
		die();
	}
	
}