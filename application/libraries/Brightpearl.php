<?php
//qbo:brightpearl
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Brightpearl{
	public $apiurl, $headers, $accountDetails, $accountConfig, $account2Details, $account1id, $account2Id, $getByIdKey, $authToken, $response;
	public function __construct(){
		$this->ci			= &get_instance();
		$this->headers		= array();
		$this->devToken		= 'PxurwyOcrRloIbmq/n6hybcD41MUdyNOcMrEZlvJGIo=';
		$this->devSecrete	= 'XDrmqBOo9KFLU09GWw+OVuwHv4ne8Zp9OPmi5oO9JwM=';
	}
	public function reInitialize($account1Id = ''){
		$this->accountDetails	= $this->ci->account1Account;
		$this->account2Details	= array();
		$this->account2Config	= array();
		$this->accountConfig	= array();
		foreach($this->ci->account1Config as $account1Config){
			$this->accountConfig[$account1Config[$this->ci->globalConfig['account1Liberary'].'AccountId']]		= $account1Config;
		}
		foreach($this->ci->account2Account as $account2Id => $account2Account){
			$this->account2Details[$account2Account['account1Id']][$account2Id]									= $account2Account;
		}		
		foreach($this->ci->account2Config as $account2Id => $account2Account){
			$this->account2Config[$account2Account[$this->ci->globalConfig['account2Liberary'].'AccountId']]	= $account2Account;
		}
	}
	public function generateToken($accountId = ''){
		$this->reInitialize();
		foreach($this->accountDetails as $accountId => $accountDetail){
			$postDatas	= array(
				'apiAccountCredentials'	=> array(
					'emailAddress'			=> $accountDetail['email'],
					'password'				=> $accountDetail['password'],
				),
			);
			$ch			= curl_init($accountDetail['authUrl']);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postDatas));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response	= json_decode(curl_exec($ch), true);
			if($response['response']){
				$this->authToken[$accountId]	= $response['response'];
			}
		}
	}
	public function getCurl($suburl, $method = 'GET', $field = '', $type = 'json', $account2Id = ''){
		$returnData	= array();
		$accountDetails	=  array();
		if(@$account2Id){
			foreach($this->accountDetails as $t1){
				if($t1['id'] == $account2Id){
					$accountDetails	= array($t1);
				}
			}
		}
		else{
			$accountDetails	= $this->accountDetails;
		}
		$orgSubUrl	= $suburl;
		foreach($accountDetails as $accountDetail){
			$milliseconds	= round(microtime(true) * 1000);
			$calMin			= sprintf("%.5f",($milliseconds / 60000));
			$nextMilisec	= ((int)$calMin + 1) - $calMin;
			$nextMilisec	= (int)((int) ($nextMilisec * 100)) / 1.666;
			$remainingSec	= $nextMilisec + 3;
			$limitName		= (int)($calMin); 
			$insertData		= array(
				'name'			=> $limitName,
				'limitcount'	=> 0,
			);
			$sql			= $this->ci->db->insert_string('api_call', $insertData) . ' ON DUPLICATE KEY UPDATE limitcount=limitcount + 1';
			$this->ci->db->query($sql);
			$limitRate		= $this->ci->db->get_where('api_call',array('name' => $limitName))->row_array();
			if($limitRate['limitcount'] >= 190){
				sleep($remainingSec);
			}
			elseif($limitRate['limitUsed']){
				sleep($remainingSec);
			}
			if(!$accountDetail['authmod']){
				$accountDetail['authmod']	= 'user';
			}
			if($accountDetail['authmod'] == 'user'){
				if(@!$this->authToken[$accountDetail['id']]){
					$this->generateToken($accountDetail);
				}
			}
			if($accountDetail['authmod'] == 'token'){
				$appToken		= $accountDetail['token'];
				$authToken		= base64_encode(hash_hmac("sha256", $appToken, $this->devSecrete,true));
				$this->headers	= array(
					'Content-Type:application/json;charset=UTF-8',
					'brightpearl-dev-ref:bsitcbpdev',
					'brightpearl-app-ref:'.$accountDetail['reference'],
					'brightpearl-account-token:'.$authToken,
				);
			}
			$this->appurl	= $accountDetail['url'] . '/';
			$url			= $this->appurl . ltrim($suburl, "/");
			if(is_array($field)){
				$postvars	= http_build_query($field);
			}
			else{
				$postvars	= $field;
			}
			$ch	= curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			if($postvars){
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			}
			if($accountDetail['authmod'] == 'user'){
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("brightpearl-auth: " . $this->authToken[$accountDetail['id']], 'Content-Type: application/json'));
			}
			if($accountDetail['authmod'] == 'token'){
				curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
			}
			$results	= json_decode(curl_exec($ch), true);
			$account1Id	= ($accountDetail['id']) ? ($accountDetail['id']) : ($accountDetail['account1Id']);
			$this->response[$account1Id]	= $results;
			if(@$results['response'] == 'You have sent too many requests. Please wait before sending another request'){
				$insertData		= array(
					'name'		=> $limitName,
					'limitUsed'	=> 1,
				);
				$this->ci->db->where(array('name' => $limitName))->update('api_call',$insertData);
				$remainingSec	= $remainingSec + 5;
				sleep($remainingSec);
				return $this->getCurl($orgSubUrl,$method,$field,$type,$account2Id);
			}
			else{
				$return		= $results;
				if(@$results['response']){
					$return	= $results['response'];
					if(strtolower($method) == 'get'){
						if($this->getByIdKey){
							$return	= array();
							foreach($results['response'] as $result){
								$return[$result[$this->getByIdKey]]	= $result;
							}
						}
					}
				}
				$returnData[$account1Id]	= $return;
			}
		}		
		return $returnData;
	}
	function range_string($number_array){
		sort($number_array);
		$previous_number	= intval(array_shift($number_array)); 
		$range				= false;
		$range_string		= "" . $previous_number; 
		foreach($number_array as $number){
			$number	= intval($number);
			if($number == $previous_number + 1){
				$range	= true;
			}
			else{
				if($range){
					$range_string	.= "-$previous_number";
				$range		= false;
				}
				$range_string	.= ",$number";
			}
			$previous_number	=  $number;
		}
		if($range){
			$range_string	.= "-$previous_number";
		}
		return $range_string;
	}
	public function getResultById($ids,$subUrl,$account1Id,$chunkLimit = 200,$returnSameKey = 0,$searchUrl = ''){
		$return	= array();
		if($ids){
			$ids	= array_filter($ids);
			$ids	= array_unique($ids);
			sort($ids);
			$ids	= array_chunk($ids,$chunkLimit);
			foreach($ids as $id){
				$range	= $this->range_string($id);
				if($subUrl == '/warehouse-service/goods-movement-search?goodsNoteId='){
					$url	= rtrim($subUrl,"=").'='.$range;
				}
				else{
					$url	= rtrim($subUrl,"/").'/'.$range;
				}
				if($searchUrl){
					$url	= $url.$searchUrl;
				}
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if(is_array($response) && @!isset($response['errors'])){
					if(!$returnSameKey){
						if(count($response) == '1'){
							foreach($response as $reKey => $responseVal){
								if(!is_numeric($reKey)){
									$response	= $response[$reKey];
									break;
								}
							}
						}
						$return	= @array_merge($return,$response);
					}
					else{
						foreach($response as $key => $res){
							$return[$key]	= $res;
						}
					}
				}
			}
		}
		return $return;
	}
	public function fetchProducts($objectId = '', $cronTime=''){
		return $this->callFunction('fetchProducts',$objectId,$cronTime);
	}
	public function fetchCustomers($objectId = '', $cronTime=''){
		return $this->callFunction('fetchCustomers',$objectId,$cronTime);
	}
	public function fetchSales($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchSales',$objectId,$cronTime,$accountId);
	}
	public function fetchAllSalesForUpdate($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchAllSalesForUpdate',$objectId,$cronTime,$accountId);
	}
	public function fetchAllCreditForUpdate($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchAllCreditForUpdate',$objectId,$cronTime,$accountId);
	}
	public function postAllUpdatedSales($objectId = '', $accountId = ''){
		return $this->callFunction('postAllUpdatedSales',$objectId, '', $accountId = '');
	}
	public function postAllUpdatedCredit($objectId = '', $accountId = ''){
		return $this->callFunction('postAllUpdatedCredit',$objectId, '', $accountId = '');
	}
	public function fetchSalesPayment($objectId = ''){
		$this->callFunction('fetchSalesPayment',$objectId);
	}
	public function postSalesPayment(){
		$this->callFunction('postSalesPayment');
	}
	public function fetchPurchase($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPurchase',$objectId,$cronTime,$accountId);
	}
	public function postPurchasePayment(){
		$this->callFunction('postPurchasePayment');
	}
	public function postPurchaseConsolPayment(){
		$this->callFunction('postPurchaseConsolPayment');
	}
	public function postPurchase($objectId = '', $accountId = '',$cronTime = ''){
		$this->callFunction('postPurchase',$objectId,$cronTime,$accountId);
	}
	public function postSalesCreditPayment(){
		$this->callFunction('postSalesCreditPayment');
	}
	public function fetchSalesCreditPayment($objectId = ''){
		$this->callFunction('fetchSalesCreditPayment',$objectId);
	}
	public function fetchRefundreceiptpayment($objectId = ''){
		$this->callFunction('fetchRefundreceiptpayment',$objectId);
	}
	public function postPurchaseCreditPayment(){
		$this->callFunction('postPurchaseCreditPayment');
	}
	public function postGoodsDispatch($objectId = '',$cronTime = ''){ 
		$this->callFunction('postGoodsDispatch',$objectId,$cronTime);
	}
	public function postSalesCreditConfimation($objectId = '',$cronTime = ''){ 
		$this->callFunction('postSalesCreditConfimation',$objectId,$cronTime);
	}
	public function postAcknowledgement($objectId = '',$cronTime = ''){ 
		$this->callFunction('postAcknowledgement',$objectId,$cronTime);
	}
	public function updateProductCustomFileds($objectId = '', $cronTime=''){
		$this->callFunction('updateProductCustomFileds',$objectId,$cronTime);
	}
	public function fetchSalesCredit($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchSalesCredit',$objectId,$cronTime,$accountId);
	}
	public function fetchRefundreceipt($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchRefundreceipt',$objectId,$cronTime,$accountId);
	}
	public function fetchPurchaseCredit($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPurchaseCredit',$objectId,$cronTime,$accountId);
	}
	public function fetchStockAdjustment($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchStockAdjustment',$objectId,$cronTime,$accountId);
	}
	public function fetchSinglestocktx($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchSinglestocktx',$objectId,$cronTime,$accountId);
	}
	public function fetchJournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchJournal',$objectId,$cronTime,$accountId);
	}
	public function fetchAmazonFeeOther($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchAmazonFeeOther',$objectId,$cronTime,$accountId);
	}
	public function fetchCogsjournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchCogsjournal',$objectId,$cronTime,$accountId);
	}
	public function fetchStockjournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchStockjournal',$objectId,$cronTime,$accountId);
	}
	public function fetchGrnijournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchGrnijournal',$objectId,$cronTime,$accountId);
	}
	public function fetchPayment($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPayment',$objectId,$cronTime,$accountId);
	}
	public function fetchBpJournals($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchBpJournals',$objectId,$cronTime,$accountId);
	}
	
	public function callFunction($functionName = '',$objectId = '',$cronTime = '',$accountId = ''){
		$this->response	= array();
		$return			= array();
		$returns		= array();
		$saveCronTime	= array();
		$updatedTimes	= array();
		if($functionName){
			if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR . APPNAME . DIRECTORY_SEPARATOR .$functionName.'.php')){
				if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR . APPNAME . DIRECTORY_SEPARATOR . CLIENTCODE . DIRECTORY_SEPARATOR .$functionName.'.php')){
					include(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
				}
				else if(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR .APPNAME. DIRECTORY_SEPARATOR .$functionName.'.php'){
					include(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR .APPNAME. DIRECTORY_SEPARATOR .$functionName.'.php');
				} 
			}
			else{ 
				include(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
		}
		if(@$returns){
			return $returns;
		}
		else if(@$updatedTimes){
			return array( 'return' => $return,'saveTime' => @max($updatedTimes) );
		}
		else if(@$saveCronTime){
			return array( 'return' => $return,'saveTime' => @max($saveCronTime) );
		}
		else{
			return @$return;
		}
	}
	public function fetchJournalByIds($journalIds = array()){
		if(!$journalIds){
			return false;
		}
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$datas	= $this->getResultById($journalIds,'/accounting-service/journal/',$account1Id,'200',0);
			if($datas){
				//foreach($datas['journals'] as $journals){
				foreach($datas as $journals){
					$return[$journals['id']]	= $journals;
				}
			}
		}
		return $return;
	}
	public function fetchInventoradvice(){
		$proDatas	= $this->ci->db->select('max(productId) as max, min(productId) as min')->get_where('products',array('isLive' => '1'))->row_array();
		$productIds	= $proDatas['min'].'-'.$proDatas['max'];
		$url		= '/warehouse-service/product-availability/'.$productIds.'?includeOptional=allocatedOrders';
		$this->reInitialize();
		$return		= array();
		$results	= $this->getCurl($url);
		foreach($results as $account1Id => $result){
			$account2Ids	= $this->account2Details[$account1Id];
			foreach($account2Ids as $account2Id){
				$saveAccId1 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
				foreach($result as $productId => $row){
					$return[$saveAccId1][$saveAccId2][$productId]	= $row;
				}
			}
		}
		$return	= $this->fetchBundleProductData($return);
		return $return;
	}
	public function fetchBundleProductData($return,$productId = ''){
		if($productId){  
			$this->ci->db->where_in('productId',$productId);
		}
		$datas	= $this->ci->db->get_where('product_bundle')->result_array();
		if($datas){
			if(@!$this->accountDetails){
				$this->reInitialize();
			}
			foreach($datas as $data){
				$account1Id	= $data['account1Id'];
				$productId	= $data['productId'];
				$url		= '/warehouse-service/bundle-availability/' . $data['productId'];
				$response	= @$this->getCurl($url,'get','','json',$account1Id)[$account1Id][$data['productId']];
				if($response['total']){
					$account2Ids	= $this->account2Details[$account1Id];
					foreach($account2Ids as $account2Id){
						$saveAccId1 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
						$saveAccId2 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
						$return[$saveAccId1][$saveAccId2][$productId]	= $response;
					}
				}
			}
		}
		return $return; 
	}
	public function getProductStock($productIds = array()){
		if(!is_array($productIds)){
			$productIds	= array($productIds);
		}
		sort($productIds);
		$url		= '/warehouse-service/product-availability/'.implode(",", $productIds);
		$this->reInitialize();
		$return		= array();
		$results	= $this->getCurl($url);
		foreach($results as $account1Id => $result){
			$account2Ids	= $this->account2Details[$account1Id];
			foreach($account2Ids as $account2Id){
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
				foreach($result as $productId => $row){
					$return[$saveAccId1][$saveAccId2][$productId]	= $row;
				}
			}
		}
		return $return;
	}
	
	public function getCustomerByCompany($companyName,$accountId = ''){
		if(!$accountId){
			return false;
		}
		if(!$companyName){
			return false;
		}
		$url	= '/contact-service/contact-search?isSupplier=true&companyName='.rawurlencode($companyName); 
		if(!isset($this->accountDetails[$accountId])){
			$this->reInitialize();
		}
		$this->getByIdKey	= '';
		$response			= $this->getCurl($url,'get','','json',$accountId)[$accountId];
		$return				= array();
		$header				= array_column($response['metaData']['columns'],'name');
		if($response['results']){
			foreach($response['results'] as $results){
				$return[]	= array_combine($header,$results);
			}
		}
		return @$return;
	}
	public function getCustomerByAllEmail($email,$accountId = ''){
		if(!$accountId){
			return false;
		}
		if(!$email){
			return false;
		}
		$url	= '/contact-service/contact-search?isSupplier=true&allEmail='.rawurlencode($email); 
		if(!isset($this->accountDetails[$accountId])){
			$this->reInitialize();
		}
		$this->getByIdKey	= '';
		$response			= $this->getCurl($url,'get','','json',$accountId)[$accountId];
		$return				= array();
		$header				= array_column($response['metaData']['columns'],'name');
		if($response['results']){
			foreach($response['results'] as $results){
				$return[]	= array_combine($header,$results);
			}
		}
		return @$return;
	}
	public function getCustomerByFirstNameLastName($fname,$lname,$accountId = ''){
		if(!$accountId){
			return false;
		}
		if(!$fname){
			return false;
		}
		if(!$lname){
			return false;
		}
		$url	= '/contact-service/contact-search?isSupplier=true&firstName='.rawurlencode($fname).'&lastName='.rawurlencode($lname); 
		if(!isset($this->accountDetails[$accountId])){
			$this->reInitialize();
		}
		$this->getByIdKey	= '';
		$response			= $this->getCurl($url,'get','','json',$accountId)[$accountId];
		$return				= array();
		$header				= array_column($response['metaData']['columns'],'name');
		if($response['results']){
			foreach($response['results'] as $results){
				$return[]	= array_combine($header,$results);
			}
		}
		return @$return;
	}
	public function createCustomerPostalAddress($createAddress,$accountId){
		$this->reInitialize();
		$url		= '/contact-service/postal-address/';
		$response	= $this->getCurl($url,'post',json_encode($createAddress),'json',$accountId)[$accountId];
		return $response;
	}
	public function getCustomerByEmail($email = '',$accountId = '',$isSupplier = '0'){
		$customerId	= '';
		if(!$email){
			return false;
		}
		if(!$this->accountDetails[$accountId]){ $this->reInitialize();}
		$url	= "/contact-service/contact-search?isCustomer=true&allEmail=".$email;
		if($isSupplier){
			$url	= "/contact-service/contact-search?isSupplier=true&allEmail=".$email;
		}
		if($accountId){
			$response	= $this->getCurl($url,'get','','json',$accountId)[$accountId];
		}
		else{
			$response	= reset($this->getCurl($url));
		}
		foreach($response['results'] as $results){
			if(strtolower($results['1']) == strtolower($email)){
				$customerId	= $results['0'];
				break;
			}
		}
		if(!$customerId){
			foreach($response['results'] as $results){
				if(strtolower($results['2']) == strtolower($email)){
					$customerId	= $results['0'];
					break;
				}
			}
		}
		if(!$customerId){
			foreach($response['results'] as $results){
				if(strtolower($results['3']) == strtolower($email)){
					$customerId	= $results['0'];
					break;
				}
			}
		}		
		return $customerId;
	}
	public function getProductPriceList($proIds, $priceListId = '0'){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$return = array();
		if(!$proIds){
			return false;
		}
		if(is_string($proIds)){
			$proIds	= array($proIds);
		}
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$result	= $this->getResultById($proIds,'/product-service/product-price/',$account1Id);
			foreach($result as $row){
				foreach($row['priceLists'] as $priceLists){
					$return[$row['productId']][$priceLists['priceListId']]	= (@$priceLists['quantityPrice']['1']) ? ($priceLists['quantityPrice']['1']) : '0.00';
				}
			}
		}
		return $return;
	}
	public function getAllShippingMethod($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/warehouse-service/shipping-method';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getExchangeRate($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize($accountId);
		}
		$url		= '/accounting-service/exchange-rate/';
		$response	= $this->getCurl($url,'get','json','',$accountId);
		return $response;
	}
	public function getAllCurrency($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/currency-search';
		$returnDatas		= $this->getCurl($url);
		$return				= array();
		foreach($returnDatas as $accountId => $returnData){
			foreach($returnData['results'] as $key => $results){
				$return[$accountId][$results['0']]	= array(
					'id'		=> $results['0'],
					'name'		=> $results['1'],
					'code'		=> $results['2'],
					'symbol'	=> $results['3'],
				);
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllLocation($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/warehouse-service/warehouse';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getAllChannel($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/channel';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getAllOrderStatus($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/order-service/order-status';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $orderStatusId => $orderStatus){
				$return[$accountIId][$orderStatus['statusId']]			= $orderStatus;
				$return[$accountIId][$orderStatus['statusId']]['id']	= $orderStatus['statusId'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllCategoryMethod($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/brightpearl-category';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getAllTax($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= 'id';
		$url				= '/accounting-service/tax-code';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $taxId => $tax){
				$return[$accountIId][$taxId]			= $tax;
				$return[$accountIId][$taxId]['name']	= $tax['code'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getSeason($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url ='';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
	}
	public function getAllPriceList($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/price-list';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $priceListId => $pricelist){
				$return[$accountIId][$pricelist['id']]			= $pricelist;
				$return[$accountIId][$pricelist['id']]['name']	= $pricelist['code'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function nominalCode($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/nominal-code-search';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){ 
			foreach($result['results'] as  $nominalCodes){
				$return[$accountIId][$nominalCodes['0']]			= $nominalCodes;
				$return[$accountIId][$nominalCodes['0']]['id']		= $nominalCodes['0'];
				$return[$accountIId][$nominalCodes['0']]['name']	= '( '.$nominalCodes['0'] . ' ) '. $nominalCodes['1'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllTag($accountId = ''){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/contact-service/tag';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $priceListId => $pricelist){
				$return[$accountIId][$priceListId]			= $pricelist;
				$return[$accountIId][$priceListId]['name']	= $pricelist['tagName'];
				$return[$accountIId][$priceListId]['id']	= $pricelist['tagId'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllSalesrep(){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/contact-service/contact-search?isStaff=true';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$return[$accountId][$result['0']]	= array('id' => $result['0'],'name' => $result['4'] .' '. $result['5'],'email' => $result['1']);
			}
		}
		return $return;
	}
	public function getAllProjects(){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/contact-service/project/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['projectId']]	= $result;
			}
		}
		return $return;
	}
	public function getAllBrand($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/brand-search';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$return[$accountId][$result['0']]	= array('id' => $result['0'],'name' => $result['1']);
			}
		}
		return $return;
	}
	public function getAllLeadsource($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/contact-service/lead-source';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']]	= $result;
			}
		}
		return $return;
	}
	public function getAllTeam($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/contact-service/contact-group/';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']]	= $result;
			} 
		}		
		return $return;
	}
	public function getAllPaymentMethod($accountId = ''){
		$morePages			= 0;
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/payment-method-search';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			if($results['metaData']['morePagesAvailable']){
				$morePages	= 1;
			}
			foreach($results['results'] as $result){
				$result['id']						= $result['1'];
				$result['name']						= $result['2'];
				$return[$accountId][$result['id']]	= $result;
			} 
		}
		if($morePages){
			$morePages	= 0;
			$url		= '/accounting-service/payment-method-search?firstResult=500';
			$resultss	=  $this->getCurl($url);
			foreach($resultss as $accountId => $results){
				if($results['metaData']['morePagesAvailable']){
					$morePages	= 1;
				}
				foreach($results['results'] as $result){
					$result['id']						= $result['1'];
					$result['name']						= $result['2'];
					$return[$accountId][$result['id']]	= $result;
				} 
			}
		}
		if($morePages){
			$morePages	= 0;
			$url		= '/accounting-service/payment-method-search?firstResult=1000';
			$resultss	=  $this->getCurl($url);
			foreach($resultss as $accountId => $results){
				if($results['metaData']['morePagesAvailable']){
					$morePages	= 1;
				}
				foreach($results['results'] as $result){
					$result['id']						= $result['1'];
					$result['name']						= $result['2'];
					$return[$accountId][$result['id']]	= $result;
				} 
			}
		}
		return $return;
	}
	public function getAccountInfo($accountId = ''){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/integration-service/account-configuration';
		$return				= $this->getCurl($url);
		return $return;
	}
	public function getAllAllowance(){
		$datasTemps	= $this->ci->db->get('products')->result_array();
		$return		= array();
		foreach($datasTemps as $datasTemp){
			$datasTemp['id']											= $datasTemp['productId'];
			$datasTemp['name']											= $datasTemp['sku'];
			$return[$datasTemp['account1Id']][$datasTemp['productId']]	= $datasTemp;
		}
		return $return;
	}
	public function getAllChannelMethod($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/channel';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']]	= $result;
			}
		}
		return $return;
	}
	public function getAllSalesCustomField($accountId = ''){
		$bpconfig	= $this->ci->account1Config[$accountId];
		if(!$bpconfig){
			$bpconfig	= $this->ci->account1Config['1'];
		}
		
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				if($result['code'] != $bpconfig['SOLocationCustomField']){
					continue;
				}
				else{
					foreach($result['options'] as $options){
						$return[$accountId][$options['id']]	= $options;
					}
				}
			}
		}
		return $return;
	}
	public function getAllSalesCustomFieldConsol($accountId = ''){
		$bpconfig	= $this->ci->account1Config[$accountId];
		if(!$bpconfig){
			$bpconfig	= $this->ci->account1Config['1'];
		}
		
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				if($result['code'] != $bpconfig['CustomFieldMappingFieldName']){
					continue;
				}
				else{
					foreach($result['options'] as $options){
						$return[$accountId][$options['id']]	= $options;
					}
				}
			}
		}
		return $return;
	}
	public function getSalesTermsCustomField($accountId = ''){
		$bpconfig	= $this->ci->account1Config[$accountId];
		if(!$bpconfig){
			$bpconfig	= $this->ci->account1Config['1'];
		}
		
		$this->reInitialize($accountId);
		$url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		$return[1]['NA']['id']		= 'NA';
		$return[1]['NA']['value']	= 'NA';
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				if($result['code'] != $bpconfig['SalesTermsFiledName']){
					continue;
				}
				else{
					foreach($result['options'] as $options){
						$return[$accountId][$options['id']]	= $options;
					}
				}
			}
		}
		return $return;
	}
	public function getPurchaseTermsCustomField($accountId = ''){
		$bpconfig	= $this->ci->account1Config[$accountId];
		if(!$bpconfig){
			$bpconfig	= $this->ci->account1Config['1'];
		}
		
		$this->reInitialize($accountId);
		$url				= '/order-service/purchase/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		$return[1]['NA']['id']		= 'NA';
		$return[1]['NA']['value']	= 'NA';
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				if($result['code'] != $bpconfig['PurchaseTermsFiledName']){
					continue;
				}
				else{
					foreach($result['options'] as $options){
						$return[$accountId][$options['id']]	= $options;
					}
				}
			}
		}
		return $return;
	}
	public function salesOrderFieldConfig(){
		$this->reInitialize();
		$returnData = array();
		$fieldsDatas = array("id" => "id","parentOrderId" => "parentOrderId","reference" =>  "reference","orderPaymentStatus" =>  "orderPaymentStatus","placedOn"=>  "placedOn","createdOn"=>  "createdOn","updatedOn"=>  "updatedOn","closedOn"=>  "closedOn","createdById" =>  "createdById","priceListId"=>  "priceListId","delivery.deliveryDate" =>  "deliveryDate","delivery.shippingMethodId" =>  "shippingMethodId","invoices.0.invoiceReference" =>  "invoice Reference","invoices.0.taxDate" =>  "invoice taxDate","invoices.0.dueDate" =>  "invoice dueDate","currency.accountingCurrencyCode" =>  "accountingCurrencyCode","currency.orderCurrencyCode" =>  "orderCurrencyCode","currency.exchangeRate" =>  "exchangeRate","currency.fixedExchangeRate" =>  "fixedExchangeRate","totalValue.net" =>  "net","totalValue.taxAmount" =>  "taxAmount","totalValue.baseNet" =>  "baseNet","totalValue.baseTaxAmount" =>  "baseTaxAmount","totalValue.baseTotal" =>  "baseTotal","totalValue.total" =>  "total","assignment.current.staffOwnerContactId" =>  "staffOwnerContactId","assignment.current.projectId" =>  "projectId","assignment.current.channelId" =>  "channelId","assignment.current.leadSourceId" =>  "leadSourceId","assignment.current.teamId" =>  "teamId","parties.customer.contactId" => "Customer contactId","parties.customer.addressFullName" => "Customer addressFullName","parties.customer.companyName" => "Customer companyName","parties.customer.addressLine1" => "Customer addressLine1","parties.customer.addressLine2" => "Customer addressLine2","parties.customer.addressLine3" => "Customer addressLine3","parties.customer.addressLine4" => "Customer addressLine4","parties.customer.postalCode" => "Customer postalCode","parties.customer.country" => "Customer country","parties.customer.countryIsoCode" => "Customer countryIsoCode","parties.customer.countryIsoCode3" => "Customer countryIsoCode3","parties.customer.telephone" => "Customer telephone","parties.customer.mobileTelephone" => "Customer mobileTelephone","parties.customer.email" => "Customer email","parties.delivery.contactId" => "Delivery contactId","parties.delivery.addressFullName" => "Delivery addressFullName","parties.delivery.companyName" => "Delivery companyName","parties.delivery.addressLine1" => "Delivery addressLine1","parties.delivery.addressLine2" => "Delivery addressLine2","parties.delivery.addressLine3" => "Delivery addressLine3","parties.delivery.addressLine4" => "Delivery addressLine4","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.delivery.telephone" => "Delivery telephone","parties.delivery.mobileTelephone" => "Delivery mobileTelephone","parties.delivery.email" => "Delivery email","parties.billing.contactId" => "Billing contactId","parties.billing.addressFullName" => "Billing addressFullName","parties.billing.companyName" => "Billing companyName","parties.billing.addressLine1" => "Billing addressLine1","parties.billing.addressLine2" => "Billing addressLine2","parties.billing.addressLine3" => "Billing addressLine3","parties.billing.addressLine4" => "Billing addressLine4","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","parties.billing.telephone" => "Billing telephone","parties.billing.mobileTelephone" => "Billing mobileTelephone","parties.billing.email" => "Billing email","warehouseId"=> "warehouseId");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array(
				'id'	=> $id,
				'name'	=> $name
			);
		}
		$return				= array();
		$url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		if($resultss){
			foreach($resultss as $accountId => $results){
				foreach($results as $result){
					if($result['code'] AND $result['name']){
						$row	= array(
							'id'	=> 'customFields.'.$result['code'],
							'name'	=> $result['code'].' - '.$result['name'],
						);
						$return[$row['id']] = $row;	
					}
				} 
			}
		}
		$returnData	= $returnData + $return;
		return $returnData;
	}
	public function purchaseOrderFieldConfig(){
		$this->reInitialize();
		$returnData = array();
		$fieldsDatas	= array("id" => "id","parentOrderId" => "parentOrderId","reference" =>  "reference","orderPaymentStatus" =>  "orderPaymentStatus","placedOn"=>  "placedOn","createdOn"=>  "createdOn","updatedOn"=>  "updatedOn","closedOn"=>  "closedOn","createdById" =>  "createdById","priceListId"=>  "priceListId","delivery.deliveryDate" =>  "deliveryDate","delivery.shippingMethodId" =>  "shippingMethodId","invoices.0.invoiceReference" =>  "invoice Reference","invoices.0.taxDate" =>  "invoice taxDate","invoices.0.dueDate" =>  "invoice dueDate","currency.accountingCurrencyCode" =>  "accountingCurrencyCode","currency.orderCurrencyCode" =>  "orderCurrencyCode","currency.exchangeRate" =>  "exchangeRate","currency.fixedExchangeRate" =>  "fixedExchangeRate","totalValue.net" =>  "net","totalValue.taxAmount" =>  "taxAmount","totalValue.baseNet" =>  "baseNet","totalValue.baseTaxAmount" =>  "baseTaxAmount","totalValue.baseTotal" =>  "baseTotal","totalValue.total" =>  "total","assignment.current.staffOwnerContactId" =>  "staffOwnerContactId","assignment.current.channelId" =>  "channelId","assignment.current.leadSourceId" =>  "leadSourceId","parties.supplier.contactId" => "Supplier contactId","parties.supplier.addressFullName" => "Supplier addressFullName","parties.supplier.companyName" => "Supplier companyName","parties.supplier.addressLine1" => "Supplier addressLine1","parties.supplier.addressLine2" => "Supplier addressLine2","parties.supplier.addressLine3" => "Supplier addressLine3","parties.supplier.addressLine4" => "Supplier addressLine4","parties.supplier.postalCode" => "Supplier postalCode","parties.supplier.country" => "Supplier country","parties.supplier.countryIsoCode" => "Supplier countryIsoCode","parties.supplier.countryIsoCode3" => "Supplier countryIsoCode3","parties.supplier.telephone" => "Supplier telephone","parties.supplier.mobileTelephone" => "Supplier mobileTelephone","parties.supplier.email" => "Supplier email","parties.delivery.contactId" => "Delivery contactId","parties.delivery.addressFullName" => "Delivery addressFullName","parties.delivery.companyName" => "Delivery companyName","parties.delivery.addressLine1" => "Delivery addressLine1","parties.delivery.addressLine2" => "Delivery addressLine2","parties.delivery.addressLine3" => "Delivery addressLine3","parties.delivery.addressLine4" => "Delivery addressLine4","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.delivery.telephone" => "Delivery telephone","parties.delivery.mobileTelephone" => "Delivery mobileTelephone","parties.delivery.email" => "Delivery email","parties.billing.contactId" => "Billing contactId","parties.billing.addressFullName" => "Billing addressFullName","parties.billing.companyName" => "Billing companyName","parties.billing.addressLine1" => "Billing addressLine1","parties.billing.addressLine2" => "Billing addressLine2","parties.billing.addressLine3" => "Billing addressLine3","parties.billing.addressLine4" => "Billing addressLine4","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","parties.billing.telephone" => "Billing telephone","parties.billing.mobileTelephone" => "Billing mobileTelephone","parties.billing.email" => "Billing email","warehouseId"=> "warehouseId"); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array(
				'id'	=> $id,
				'name'	=> $name
			);
		}
		$return				= array();
		$url				= '/order-service/purchase/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		if($resultss){
			foreach($resultss as $accountId => $results){
				foreach($results as $result){
					if($result['code'] AND $result['name']){
						$row	= array(
							'id'	=> 'customFields.'.$result['code'],
							'name'	=> $result['code'].' - '.$result['name'],
						);
						$return[$row['id']] = $row;	
					}
				} 
			}
		}
		$returnData = $returnData + $return;
		return $returnData;
	}
	public function salesOrderAPIField(){
		$this->reInitialize();
		$returnData		= array();
		$fieldsDatas	= array(
			"reference"									=> "reference",
			"createdById"								=> "createdById",
			"currency.accountingCurrencyCode"			=> "accountingCurrencyCode",
			"assignment.current.staffOwnerContactId"	=> "staffOwnerContactId",
			"assignment.current.leadSourceId"			=> "leadSourceId",
			"parties.customer.contactId"				=> "Customer contactId",
			"parties.customer.companyName"				=> "Customer companyName",
			"parties.customer.postalCode"				=> "Customer postalCode",
			"parties.customer.country"					=> "Customer country",
			"parties.customer.countryIsoCode"			=> "Customer countryIsoCode",
			"parties.customer.countryIsoCode3"			=> "Customer countryIsoCode3",
			"parties.delivery.contactId"				=> "Delivery contactId",
			"parties.delivery.companyName"				=> "Delivery companyName",
			"parties.delivery.postalCode"				=> "Delivery postalCode",
			"parties.delivery.country"					=> "Delivery country",
			"parties.delivery.countryIsoCode"			=> "Delivery countryIsoCode",
			"parties.delivery.countryIsoCode3"			=> "Delivery countryIsoCode3",
			"parties.billing.contactId"					=> "Billing contactId",
			"parties.billing.companyName"				=> "Billing companyName",
			"parties.billing.postalCode"				=> "Billing postalCode",
			"parties.billing.country"					=> "Billing country",
			"parties.billing.countryIsoCode"			=> "Billing countryIsoCode",
			"parties.billing.countryIsoCode3"			=> "Billing countryIsoCode3",
			"warehouseId"								=> "warehouseId"
		);
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array(
				'id'	=> $id,
				'name'	=> $name
			);
		}
		return $returnData;
	}
	public function getPayments($cronTime = '', $account1Id = '', $startPoint = 0, $totalRecord = 0, $objectId = array()){
		$returnData	= array();
		if($cronTime AND $account1Id AND $startPoint){
			$datas		= $this->ci->db->limit(500, $startPoint)->order_by('id', 'asc')->where("date(`createdOnTemp`) >= ","date('".$cronTime."')",false)->get_where('bp_payments',array('account1Id' => $account1Id))->result_array();
			
			$returnData	= array(
				'results'			=> $datas,
				'metaData'	=> array(
					'resultsAvailable'	=> $totalRecord,
				),
			);
		}
		elseif($cronTime AND $account1Id){
			$totalRecord	= $this->ci->db->order_by('id', 'asc')->select('count("id") as resultsAvailable')->where("date(`createdOnTemp`) >= ","date('".$cronTime."')",false)->get_where('bp_payments',array('account1Id' => $account1Id))->row_array()['resultsAvailable'];
			
			if($totalRecord > 0){
				$datas		= $this->ci->db->limit(500, $startPoint)->order_by('id', 'asc')->where("date(`createdOnTemp`) >= ","date('".$cronTime."')",false)->get_where('bp_payments',array('account1Id' => $account1Id))->result_array();
				
				$returnData	= array(
					'results'	=> $datas,
					'metaData'	=> array(
						'resultsAvailable'	=> $totalRecord,
					),
				);
			}
		}
		elseif((!empty($objectId)) AND ($account1Id)){
			$datas		= $this->ci->db->order_by('id', 'asc')->where_in('orderId', $objectId)->get_where('bp_payments',array('account1Id' => $account1Id))->result_array();
			
			$returnData	= array(
				'results'			=> $datas,
				'metaData'	=> array(
					'resultsAvailable'	=> count($datas),
				),
			);
		}
		return $returnData;
	}
	public function searchJournal($cronTime = '', $account1Id = '', $JournalTypeCode = '', $SaveJournalAccountCode = 0, $startPoint = 0, $totalRecord = 0){
		$returnData	= array();
		if($cronTime AND $account1Id AND $JournalTypeCode AND $SaveJournalAccountCode AND $startPoint){
			$datas		= $this->ci->db->limit(500, $startPoint)->order_by('id', 'asc')->where("date(`journalDate`) >= ","date('".$cronTime."')",false)->get_where('bp_journal_search',array('account1Id' => $account1Id, 'journalType' => $JournalTypeCode, 'nominalCode' => $SaveJournalAccountCode))->result_array();
			
			$returnData	= array(
				'results'			=> $datas,
				'metaData'	=> array(
					'resultsAvailable'	=> $totalRecord,
				),
			);
		}
		elseif($cronTime AND $account1Id AND $JournalTypeCode AND $SaveJournalAccountCode){
			$totalRecord	= $this->ci->db->order_by('id', 'asc')->select('count("id") as resultsAvailable')->where("date(`journalDate`) >= ","date('".$cronTime."')",false)->get_where('bp_journal_search',array('account1Id' => $account1Id, 'journalType' => $JournalTypeCode, 'nominalCode' => $SaveJournalAccountCode))->row_array()['resultsAvailable'];
			
			if($totalRecord > 0){
				$datas		= $this->ci->db->limit(500, $startPoint)->order_by('id', 'asc')->where("date(`journalDate`) >= ","date('".$cronTime."')",false)->get_where('bp_journal_search',array('account1Id' => $account1Id, 'journalType' => $JournalTypeCode, 'nominalCode' => $SaveJournalAccountCode))->result_array();
				
				$returnData	= array(
					'results'	=> $datas,
					'metaData'	=> array(
						'resultsAvailable'	=> $totalRecord,
					),
				);
			}
		}
		return $returnData;
	}
	public function getJournals($objectId = array(), $account1Id = '', $limit = 200){
		$returnData	= array();
		$returnKey	= 0;
		if($objectId AND $account1Id){
			$objectId	= array_chunk($objectId, $limit);
			foreach($objectId as $objectIdTemp){
				$datas	= $this->ci->db->where_in('journalId', $objectIdTemp)->get_where('bp_journals',array('account1Id' => $account1Id))->result_array();
				if(!empty($datas)){
					foreach($datas as $datasTemp){
						$rowData	= json_decode($datasTemp['rowData'], true);
						$returnData[$rowData['id']]	= $rowData;
						$returnKey++;
					};
				}
			};
		}
		return $returnData;
	}
}