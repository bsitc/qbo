<?php
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
#[\AllowDynamicProperties]
class Qbo{
	public $apiurl, $headers, $accountDetails, $accountConfig, $account2Config, $account2Details, $account1id, $account2Id, $response, $extraProductParams, $postProductAccount;
	public function __construct(){
		$this->ci			= &get_instance();
		$this->headers		= array();
		$this->enableApache	= 0;
	}
	public function reInitialize($account1Id = ''){
		$this->accountDetails	= $this->ci->account2Account;
		$this->accountConfig	= array();
		foreach($this->ci->account2Config as $account2Config){
			$this->accountConfig[$account2Config[$this->ci->globalConfig['account2Liberary'].'AccountId']]	= $account2Config;
		}
	}
	public function getCurl($suburl, $method = 'GET', $field = '', $type = 'json', $account2Id = '',$oauthProcess = 0){
		$return	= array();
		$accountDetails	= array();
		if(@$account2Id){
			foreach($this->accountDetails as $t1){
				if($t1['id'] == $account2Id){
					$accountDetails	= array($t1);
				}
			}
		}
		else{
			$accountDetails	= $this->accountDetails;
		}
		foreach($accountDetails as $acId => $accountDetail){
			usleep(300);
			if((!$oauthProcess) && ((int)strtotime("now") > (int)($accountDetail['tokenFetchTime'] + $accountDetail['expiresIn']))){
				$this->refreshToken($accountDetail['id']);
				$accountDetail	= $this->accountDetails[$accountDetail['id']];
			}
			if(!$oauthProcess){
				$url	= rtrim($accountDetail['baseUrl'],"/") .'/v3/company/'. $accountDetail['companyId'] .'/' . ltrim($suburl, "/");
			}
			else{
				$url	= $suburl;
			}
			if(is_array($field)){
				$postvars	= http_build_query($field);
			}
			else{
				$postvars	= $field;
			}
			$ch	= curl_init();
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_CUSTOMREQUEST,strtoupper($method));
			if($postvars){
				curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
			}
			if($type=='json'){
				$authorizationheader	= 'bearer ' . $accountDetail['accessToken'];
				$header					= array('Accept: application/json','Content-Type: application/json','Authorization: ' .$authorizationheader. '');
			}
			else{
				$header	= array('Content-Type: application/x-www-form-urlencoded');	
			}
			if($oauthProcess){
				$header	= $this->headers;
			}
			curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			$response		= curl_exec($ch);
			$curlinfo		= curl_getinfo($ch);
			
			$header_size	= curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$ResponeHeaders	= substr($response, 0, $header_size);
			$response		= substr($response, $header_size);
			
			$api_call_log	= array(
				'ApiEndPoint'	=> $url,
				'ApiResponse'	=> $response,
				'CurlInfo'		=> json_encode($curlinfo,true).'<br>'.$ResponeHeaders,
				'ApiRequest'	=> $field,
			);
			if(is_array($field)){
				$api_call_log['ApiRequest']	= json_encode($field);
			}
			if($curlinfo['size_download'] > 10000){
				$api_call_log['ApiResponse']	= 'Response Is too big to store';
			}
			$this->ci->db->insert('api_call_log', $api_call_log);
			$account1Id	= ($accountDetail['id']) ? ($accountDetail['id']) : ($accountDetail['account1Id']);
			$this->response[$account1Id]	= $response; 
			$return[$account1Id]			= json_decode($response, true);
		}
		return $return;
	}
	public function refreshToken($orgAccountId = ''){
		$code	= trim($this->ci->input->get('code'));
		if($code){
			$accountDetails	= $this->ci->session->userdata('accountDetails');
			if(!$accountDetails){
				return false;
			}
			$data	= array(
				'grant_type'	=> 'authorization_code',
				'code'			=> $code,
				'redirect_uri'	=> $accountDetails['redirectUrl'],
			);
			$data	= array(
				'code' 			=> $code,
				'grant_type'	=> 'authorization_code',
				'client_id' 	=> $accountDetails['clientId'],
				'client_secret' => $accountDetails['clientSecret'],
				'redirect_uri' 	=> $accountDetails['redirectUrl']
		
			);
			$encodedClientIDClientSecrets	= base64_encode($accountDetails['clientId'] . ':' . $accountDetails['clientSecret']);
			$authorizationheader			= 'Basic ' . $encodedClientIDClientSecrets;
			$this->headers	= array(
				'Accept'		=> 'application/json',
				'Authorization'	=> $authorizationheader,
				'Content-Type'	=> 'application/x-www-form-urlencoded'
			);
			$authenticationURL	= 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer';
			if(@!$this->accountDetails){
				$this->reInitialize($accountDetails['id']);
			}
			$result	= $this->getCurl($authenticationURL,'POST',$data,'', $accountDetails['id'],'1')[$accountDetails['id']];
			if(@$result['access_token']){
				$saveData	= array(
					"id" 			=> $accountDetails['id'],
					"accessToken"	=> $result['access_token'],
					"tokenType" 	=> $result['token_type'],
					"expires" 		=> $result['x_refresh_token_expires_in'],
					"expiresIn" 	=> $result['expires_in'],
					"refreshToken"  => $result['refresh_token'],
				);
				$this->ci->db->where(array('id' => $accountDetails['id']))->update('account_qbo_account',$saveData);
			}
		}
		else{
			if(@!$this->accountDetails){
				$this->reInitialize($orgAccountId);
			}
			foreach($this->accountDetails as $accountId => $accountDetails){
				if($accountId == $orgAccountId){	
					$accountDetails	= $this->ci->db->get_where('account_qbo_account',array('id' => $accountId ))->row_array();
					if(@!$accountDetails['accessToken']){
						 $parameters	= array(
							'client_id'		=> $accountDetails['clientId'],
							'scope'			=> 'com.intuit.quickbooks.accounting',
							'redirect_uri'	=> $accountDetails['redirectUrl'],
							'response_type'	=> 'code',
							'state'			=> 'RandomState',
						);
						$authorizationRequestUrl	= 'https://appcenter.intuit.com/connect/oauth2?' . http_build_query($parameters, null, '&', PHP_QUERY_RFC1738);
						$this->ci->session->set_userdata('accountDetails', $accountDetails);
						header('Location: '.$authorizationRequestUrl);
					}
					else{
						$data			= array(
							'client_id'		=> $accountDetails['clientId'],
							'client_secret'	=> $accountDetails['clientSecret'],
							'redirect_uri'	=> $accountDetails['redirectUrl'],
							'grant_type'	=> 'refresh_token',
							'refresh_token' => $accountDetails['refreshToken'],
						);
						$encodedClientIDClientSecrets	= base64_encode($accountDetails['clientId'] . ':' . $accountDetails['clientSecret']);
						$authorizationheader			= 'Basic ' . $encodedClientIDClientSecrets;
						$this->headers	= array(
							'Accept' 		=> 'application/json',
							'Authorization'	=> $authorizationheader,
							'Content-Type' 	=> 'application/x-www-form-urlencoded'
						);
						$url			= 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer';
						$tokenFetchTime	= strtotime("now") - 60;;
						$result			= $this->getCurl($url,'POST',$data,'application/x-www-form-urlencoded',$accountId,'1')[$accountId];
						if($result['access_token']){
							$saveData	= array(
								"accessToken"		=> $result['access_token'],
								"tokenType"			=> $result['token_type'],
								"expires"			=> $result['x_refresh_token_expires_in'],
								"refreshToken"		=> $result['refresh_token'],
								"expiresIn"			=> $result['expires_in'],
								"tokenFetchTime"	=> $tokenFetchTime, 
							);
							$this->ci->db->where(array('id' => $accountDetails['id']))->update('account_qbo_account',$saveData);
							$this->accountDetails[$accountId]['accessToken']	= $result['access_token'];
							$this->accountDetails[$accountId]['refreshToken']	= $result['refresh_token']; 
							$this->accountDetails[$accountId]['tokenType']		= $result['token_type'];
							$this->accountDetails[$accountId]['expires']		= $result['x_refresh_token_expires_in'];
							$this->accountDetails[$accountId]['expiresIn']		= $result['expires_in'];
							$this->accountDetails[$accountId]['tokenFetchTime']	= $tokenFetchTime; 
						}
					}
				}
			}
		}
	}
	public function getSupplierById($supplierId = ''){
		if(!$supplierId){
			return false;
		}
		$this->reInitialize();
		$query		= "select * from vendor where id = '".$supplierId."'";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json'); 
		return $serchRes;
	}
	public function getAllChannelMethod($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$query		= "select * from Class STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$return		= array();
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			foreach($serchRe['QueryResponse']['Class'] as $Payment){
				$return[$accountId][$Payment['Id']]			= $Payment;
				$return[$accountId][$Payment['Id']]['id']	= $Payment['Id'];
				$return[$accountId][$Payment['Id']]['name']	= $Payment['Name'];
			}
		}
		return $return;
	}
	public function getAllPaymentMethod($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$query		= "select * from PaymentMethod STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$return		= array();
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			foreach($serchRe['QueryResponse']['PaymentMethod'] as $Payment){
				$return[$accountId][$Payment['Id']]			= $Payment;
				$return[$accountId][$Payment['Id']]['id']	= $Payment['Id'];
				$return[$accountId][$Payment['Id']]['name']	= $Payment['Name'];
			}
		}
		return $return;  
	}
	public function getAccountDetails($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$return			= array();
		$newArrayAcc	= array();
		
		$query		= "select * from account STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			if($serchRe['QueryResponse']['Account']){
				foreach($serchRe['QueryResponse']['Account'] as $Account){
					$return[$accountId][$Account['Id']]			= $Account;
					$return[$accountId][$Account['Id']]['id']	= $Account['Id'];
					$return[$accountId][$Account['Id']]['name']	= $Account['AcctNum'] .' - '. $Account['Name'];
				}
			}
		}
		
		$accountId	= '';
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		
		$query		= "select * from account STARTPOSITION 999 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			if($serchRe['QueryResponse']['Account']){
				foreach($serchRe['QueryResponse']['Account'] as $Account){
					$return[$accountId][$Account['Id']]			= $Account;
					$return[$accountId][$Account['Id']]['id']	= $Account['Id'];
					$return[$accountId][$Account['Id']]['name']	= $Account['AcctNum'] .' - '. $Account['Name'];
				}
			}
		}
		return $return;
	}
	public function getAllCustomerType($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$query		= "Select * From CustomerType STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$return		= array();
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			foreach($serchRe['QueryResponse']['CustomerType'] as $CustomerType){
				$return[$accountId][$CustomerType['Id']]			= $CustomerType;
				$return[$accountId][$CustomerType['Id']]['id']		= $CustomerType['Id'];
				$return[$accountId][$CustomerType['Id']]['name']	= $CustomerType['Name'];
			}
		}
		return $return;
	}
	public function getAllTax($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$query		= "SELECT * FROM TaxCode STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$return		= array();
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			foreach($serchRe['QueryResponse']['TaxCode'] as $TaxCode){
				$return[$accountId][$TaxCode['Id']]			= $TaxCode;
				$return[$accountId][$TaxCode['Id']]['id']	= $TaxCode['Id'];
				$return[$accountId][$TaxCode['Id']]['name']	= $TaxCode['Name'];
			}
		}
		return $return;
	}
	public function getAllTaxRate($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$query		= "SELECT * FROM TaxRate STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$return		= array();
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			foreach($serchRe['QueryResponse']['TaxRate'] as $TaxCode){
				if(@!isset($TaxCode['Active']) || ($TaxCode['Active'])){
					$return[$accountId][$TaxCode['Id']]			= $TaxCode;
					$return[$accountId][$TaxCode['Id']]['id']	= $TaxCode['Id'];
					$return[$accountId][$TaxCode['Id']]['name']	= $TaxCode['Name'];
				}
			}
		}
		return $return; 
	}
	public function getAllTerms($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize();
		}
		$query		= "SELECT * FROM Term STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=4&query=".rawurlencode($query);
		$return		= array();
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$accountId);
		foreach($serchRes as $accountId => $serchRe){
			foreach($serchRe['QueryResponse']['Term'] as $Term){
				$return[$accountId][$Term['Id']]			= $Term;
				$return[$accountId][$Term['Id']]['id']		= $Term['Id'];
				$return[$accountId][$Term['Id']]['name']	= $Term['Name'];
			}
		}
		return $return;
	}
	public function createWebhooks($accountId = ''){
		$this->reInitialize($accountId);
		$postData	= 'data={"url":"'.base_url('webhooks/qboProductWebhook').'","active":true,"type":"product.update"}';
		$postData	= 'data={"url":"'.base_url('webhooks/qboSalesWebhook').'","active":true,"type":"sale.update"}';
		$postData	= 'data={"url":"'.base_url('webhooks/qboCustomerWebhook').'","active":true,"type":"customer.update"}';
		$postData	= 'data={"url":"'.base_url('webhooks/qboTransferWebhook').'","active":true,"type":"consignment.send"}'; 
		$postData	= 'data={"url":"'.base_url('webhooks/qboTransferWebhook').'","active":true,"type":"consignment.receive"}';
		$postData	= 'data={"url":"'.base_url('webhooks/qboStockWebhook').'","active":true,"type":"inventory.update"}';
		$url		= 'webhooks';
		$res		= $this->getCurl($url, 'POST', $postData , '', '','1');
	}
	public function listWebhooks($accountId = ''){
		$this->reInitialize($accountId);
		$url	= 'webhooks';
		$res	= $this->getCurl($url, 'GET', '', 'json');
	}
	public function deleteWebhooks($webhooksId , $accountId = ''){
		$this->reInitialize($accountId);
		$url	= 'webhooks/'.$webhooksId;
		$res	= $this->getCurl($url, 'DELETE', '', 'json');
	}
	public function postProducts($orgObjectId = '',$postedAccount2Id = ''){
		$this->callFunction('postProducts',$orgObjectId,$postedAccount2Id);
	}
	public function postLoadedProduct($orgObjectId = ''){
		$this->callFunction('postLoadedProduct',$orgObjectId);
	}
	public function postCustomers($orgObjectId = '',$postedAccount2Id = ''){
		$this->callFunction('postCustomers',$orgObjectId,$postedAccount2Id);
	}
	public function postPurchase($orgObjectId = ''){
		$this->callFunction('postPurchase',$orgObjectId);
	}
	public function fetchPurchase($orgObjectId = ''){
		$this->callFunction('fetchPurchase',$orgObjectId);
	}
	public function fetchPurchasePayment($orgObjectId = ''){
		$this->callFunction('fetchPurchasePayment',$orgObjectId);
	}
	public function postPurchaseCredit($orgObjectId = ''){
		$this->callFunction('postPurchaseCredit',$orgObjectId);
		$this->fetchPurchaseCreditPayment();
	}
	public function fetchPurchaseCreditPayment($orgObjectId = ''){
		$this->callFunction('fetchPurchaseCreditPayment',$orgObjectId);
	}
	public function postSales($orgObjectId = ''){
		$this->callFunction('postSales',$orgObjectId);
	}
	public function postaggregationSales($orgObjectId = ''){
		$this->callFunction('postaggregationSales',$orgObjectId);
	}
	public function postaggregationSalesPayment($orgObjectId = ''){
		$this->callFunction('postaggregationSalesPayment',$orgObjectId);
	}
	public function postaggregationSalescredit($orgObjectId = ''){
		$this->callFunction('postaggregationSalescredit',$orgObjectId);
	}
	public function postNetOffConsolOrder($orgObjectId = ''){
		$this->callFunction('postNetOffConsolOrder',$orgObjectId);
	}
	public function postaggregationSalescreditPayment($orgObjectId = ''){
		$this->callFunction('postaggregationSalescreditPayment',$orgObjectId);
	}
	public function postConsolidatedJournal($orgObjectId = ''){
		$this->callFunction('postConsolidatedJournal',$orgObjectId);
	}
	public function postJournal($orgObjectId = ''){
		$this->callFunction('postJournal',$orgObjectId);
	}
	public function postAmazonFeeOther($orgObjectId = ''){
		$this->callFunction('postAmazonFeeOther',$orgObjectId);
	}
	public function fetchSalesPayment($orgObjectId = ''){
		$this->callFunction('fetchSalesPayment',$orgObjectId);
	}
	public function postSalesPayment($orgObjectId = ''){ 
		$this->callFunction('postSalesPayment',$orgObjectId); 
	}
	public function postSalesCredit($orgObjectId = ''){
		$this->callFunction('postSalesCredit',$orgObjectId);
	}
	public function postRefundreceipt($orgObjectId = ''){
		$this->callFunction('postRefundreceipt',$orgObjectId);
	}
	public function sendSalesCreditInBill($orgObjectId = ''){
		$this->callFunction('sendSalesCreditInBill',$orgObjectId); 
	}
	public function fetchSalesCreditPayment($orgObjectId = ''){
		$this->callFunction('fetchSalesCreditPayment',$orgObjectId);
	}
	public function postSalesCreditPayment($orgObjectId = ''){
		$this->callFunction('postSalesCreditPayment',$orgObjectId); 
	}
	public function postStockAdjustment($orgObjectId = ''){
		 $this->callFunction('postStockAdjustment',$orgObjectId); 
	}
	public function postCogsjournal($orgObjectId = ''){
		 $this->callFunction('postCogsjournal',$orgObjectId); 
	}
	public function postConsolCogsjournal($orgObjectId = ''){
		 $this->callFunction('postConsolidatedCogsJournal',$orgObjectId); 
	}
	public function postSinglestocktx($orgObjectId = ''){
		 $this->callFunction('postSinglestocktx',$orgObjectId); 
	}
	public function postConsolStockjournal($orgObjectId = ''){
		 $this->callFunction('postConsolStockjournal',$orgObjectId); 
	}
	public function postPurchaseBatchInvoice($orgObjectId = ''){
		 $this->callFunction('postPurchaseBatchInvoice',$orgObjectId); 
	}
	public function fetchPurchaseConsolPayment($orgObjectId = ''){
		 $this->callFunction('fetchPurchaseConsolPayment',$orgObjectId); 
	}
	public function postConsolRefundReceipt($orgObjectId = ''){
		 $this->callFunction('postConsolRefundReceipt',$orgObjectId); 
	}
	public function postGrnijournal($orgObjectId = ''){
		 $this->callFunction('postGrnijournal',$orgObjectId); 
	}
	
	public function callFunction($functionName = '',$orgObjectId = '',$fetchType=''){
		if($functionName){
			$postedAccount2Id	= '';
			if(($functionName == 'postCustomers') OR ($functionName == 'postProducts')){
				$postedAccount2Id = $fetchType;
			}
			if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
			else if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . $functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . $functionName.'.php');
			}
			else{
				include(dirname(__FILE__). DIRECTORY_SEPARATOR .'qbo'. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
		}
	}
	public function getAllLocation($accountId = ''){
		return false;
	}
	public function getAllRegisters($accountId = ''){
		return false;
	}
	public function getAllBrand($accountId = ''){
		return false;
	}
	public function getAllSupplier($accountId = ''){
		return false;
	}
	public function getAllPriceList(){
		return false;
	}
	public function getAccountInfo(){
		return false;
	}
	public function getExchangeRate(){
		$this->reInitialize();
		$query		= "select * from exchangerate where AsOfDate = '".date('Y-m-d')."' STARTPOSITION 0 MAXRESULTS 1000";
		$url		= "query?minorversion=45&query=".rawurlencode($query);
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json');
		$return		= array();
		foreach($serchRes as $accId => $serchRe){
			foreach($serchRe['QueryResponse']['ExchangeRate'] as $ExchangeRate){
				$return[$accId][strtolower($ExchangeRate['SourceCurrencyCode'])][strtolower($ExchangeRate['TargetCurrencyCode'])] = $ExchangeRate;
			}
		}
		return $return;
	}
	/******* QboExchangeRate Saved in DB ******/
	public function getQboExchangeRate(){
		$this->reInitialize();
		$return		= array();
		$exchangeRateMappings		= array();
		$exchangeRateTemps	= $this->ci->db->get('qbo_exchange_rate')->result_array();
		if($exchangeRateTemps){
			foreach($exchangeRateTemps as $exchangeRateTemp){
				$exchangeRateMappings[$exchangeRateTemp['account2Id']][strtolower($exchangeRateTemp['sourceCurrency'])][strtolower($exchangeRateTemp['targetCurrnecy'])][$exchangeRateTemp['asOfDate']]	= $exchangeRateTemp;
			}
		}
		if(empty($exchangeRateMappings)){
			$startDate = date('Y-m-d',strtotime('-31 Days'));
			for($i=1; $i <= 32; $i++){
				$query		= "select * from exchangerate where AsOfDate = '".$startDate."' STARTPOSITION 0 MAXRESULTS 1000";
				$url		= "query?minorversion=45&query=".rawurlencode($query);
				$serchRes	= @$this->getCurl($url, 'GET', '', 'json');
				foreach($serchRes as $accId => $serchRe){
				foreach($serchRe['QueryResponse']['ExchangeRate'] as $ExchangeRate){
					if(isset($exchangeRateMappings[$accId][strtolower($ExchangeRate['SourceCurrencyCode'])][strtolower($ExchangeRate['TargetCurrencyCode'])][$ExchangeRate['AsOfDate']])){continue;}
						$return[] = array(
							'account2Id'	=> $accId,
							'exchangeRate'	=> $ExchangeRate['Rate'],
							'sourceCurrency'=> $ExchangeRate['SourceCurrencyCode'],
							'targetCurrnecy'=> $ExchangeRate['TargetCurrencyCode'],
							'asOfDate'		=> $ExchangeRate['AsOfDate'],
							'created'		=> $ExchangeRate['MetaData']['LastUpdatedTime'],
						);
					}
				}
				$dateAddObj		= date_create($startDate);
				date_add($dateAddObj,date_interval_create_from_date_string('+1 days'));
				$startDate		= date_format($dateAddObj,"Y-m-d");
			}
		}
		else{
			$query		= "select * from exchangerate where AsOfDate = '".date('Y-m-d')."' STARTPOSITION 0 MAXRESULTS 1000";
			$url		= "query?minorversion=45&query=".rawurlencode($query);
			$serchRes	= @$this->getCurl($url, 'GET', '', 'json');
			foreach($serchRes as $accId => $serchRe){
				foreach($serchRe['QueryResponse']['ExchangeRate'] as $ExchangeRate){
					if(isset($exchangeRateMappings[$accId][strtolower($ExchangeRate['SourceCurrencyCode'])][strtolower($ExchangeRate['TargetCurrencyCode'])][$ExchangeRate['AsOfDate']])){continue;}
					$return[] = array(
						'account2Id'	=> $accId,
						'exchangeRate'	=> $ExchangeRate['Rate'],
						'sourceCurrency'=> $ExchangeRate['SourceCurrencyCode'],
						'targetCurrnecy'=> $ExchangeRate['TargetCurrencyCode'],
						'asOfDate'		=> date('Y-m-d',strtotime($ExchangeRate['AsOfDate'])),
						'created'		=> $ExchangeRate['MetaData']['LastUpdatedTime'],
					);
				}
			}
		}
		if($return){
			$batchInserts = array_chunk($return, 200);
			foreach ($batchInserts as $key => $batchInsert) {
				if($batchInsert['0']){
					$this->ci->db->insert_batch('qbo_exchange_rate', $batchInsert);
				}
			}
			 
		}
	}
	
	public function getQboExchangeRateByDb($account2Id='',$sourceCurrency='',$targetCurrnecy='',$asOfDate=''){
		if(!$account2Id || !$sourceCurrency || !$targetCurrnecy || !$asOfDate){ return false;}
		$return			= '';
		$asOfDate		= date('Y-m-d',strtotime($asOfDate));
		$exchangeRate	= $this->ci->db->order_by('created','desc')->select('exchangeRate')->get_where('qbo_exchange_rate',array('account2Id' => $account2Id,'sourceCurrency' => $sourceCurrency,'targetCurrnecy' => $targetCurrnecy,'asOfDate' => $asOfDate))->row_array();
		if($exchangeRate){
			$return = $exchangeRate['exchangeRate'];
		}
		return $return;
	}
	/*************/
	public function getPreferences(){
		$this->reInitialize();
		$query		= "select * from Preferences";
		$url		= "query?minorversion=45&query=".rawurlencode($query);
		$serchRes	= @$this->getCurl($url, 'GET', '', 'json');
		$return		= array();
		foreach($serchRes as $accId => $serchRe){
			foreach($serchRe['QueryResponse']['Preferences'] as $Preferences){
				$return[$accId]['CurrencyPrefs']	= $Preferences['CurrencyPrefs']['HomeCurrency']['value'];
			}
		}
		return $return;
	}
}
?>