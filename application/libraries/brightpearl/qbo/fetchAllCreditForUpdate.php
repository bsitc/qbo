<?php
$this->reInitialize();
$enableTaxCustomisation	= $this->ci->globalConfig['enableTaxCustomisation'];
$returns				= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$enableTaxCustomisation){continue;}
	$allBPchannelsData	= $this->getAllChannelMethod();
	$channelMaped		= array();
	if((is_array($allBPchannelsData)) AND (!empty($allBPchannelsData))){
		foreach($allBPchannelsData as $acc1 => $allBPchannelsDatas){
			foreach($allBPchannelsDatas as $cID	=> $allBPchannelsDatass){
				$channelMaped[$cID]	= $allBPchannelsDatass;
			}
		}
	}
	
	$return				= array();
	$orderIds			= array();
	$saveCronTime		= array();
	$this->config		= $this->accountConfig[$account1Id];
	
	$cronData			= $this->ci->db->order_by('id','desc')->get_where('cron_management', array('type' => 'allupdatesalescredit'.$account1Id))->row_array();
	$cronTime			= (!empty($cronData['saveTime'])) ? ($cronData['saveTime']) : (strtotime('-30 days'));
	$saveCronTime[]		= $cronTime;
	
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= str_replace("+","%2B",$datetime->format(DateTime::ATOM));
	
	$orderUpdateConfig	= $this->ci->db->get_where('taxupdate_config', array('brightpearlAccountId' => $account1Id))->row_array();
	$fetchChannelIds	= (!empty($orderUpdateConfig['channelIds'])) ? (explode(",",trim($orderUpdateConfig['channelIds']))) : array();
	$fetchCountry		= (!empty($orderUpdateConfig['countries'])) ? (explode(",",trim($orderUpdateConfig['countries']))) : array();
	$fetchCountries		= (!empty($fetchCountry)) ? (array_map(strtolower,$fetchCountry)) : array();
	
	$url				= '/order-service/order-search?orderTypeId=3&isClosed=false&placedOn='.$cronTime.'/';
	$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if((is_array($response)) AND (!empty($response)) AND (isset($response['results']))){
		$orderIds	= array_column($response['results'], 0);
		if($response['metaData']['resultsAvailable'] > 500){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url.'&firstResult='.$i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response1)) AND (!empty($response1)) AND (isset($response1['results']))){
					$orderIds	= array_merge($orderIds, array_column($response1['results'], 0));
				}
			}
		}
	}
	
	if(!empty($orderIds)){
		$orderIds	= array_unique(array_filter($orderIds));
		sort($orderIds);
		
		$orderDatas	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');	
		if((is_array($orderDatas)) AND (!empty($orderDatas))){
			foreach($orderDatas as $orderInfoList){
				$orderId			= $orderInfoList['id'];
				$channelId			= $orderInfoList['assignment']['current']['channelId'];
				$channelName		= (($channelId) AND (!empty($channelMaped)) AND (isset($channelMaped[$channelId]))) ? ($channelMaped[$channelId]['name']) : ('');
				$countryIsoCode3	= strtolower($orderInfoList['parties']['delivery']['countryIsoCode3']);
				
				if($orderInfoList['invoices']['0']['invoiceReference']){continue;}
				if(($fetchChannelIds) AND ($channelId) AND (!in_array($channelId,$fetchChannelIds))){continue;}
				if(($fetchCountries) AND ($countryIsoCode3) AND (!in_array($countryIsoCode3,$fetchCountries))){continue;}
				
				$return[$account1Id][$orderId]['orders']	= array(
					'account1Id'		=> $account1Id, 
					'orderId'			=> $orderId,
					'orderType'			=> 'SC',
					'parentOrderId'		=> $orderInfoList['parentOrderId'],
					'reference'			=> $orderInfoList['reference'], 
					'channelId'			=> $orderInfoList['assignment']['current']['channelId'],
					'channelName'		=> $channelName,
					'Country'			=> strtoupper($countryIsoCode3),
					'status'			=> 0,
					'created'			=> date('Y-m-d H:i:s', strtotime($orderInfoList['createdOn'])),
					'rowData'			=> json_encode($orderInfoList),
				);
				$saveCronTime[]		= strtotime($orderInfoList['placedOn']);
			}
		}
	}
	$returns[$account1Id]	= array('return' => $return,'saveTime' => max($saveCronTime));
}
?>