<?php
$this->reInitialize();
$enableTaxCustomisation	= $this->ci->globalConfig['enableTaxCustomisation'];
$returns				= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$enableTaxCustomisation){
		continue;
	}
	$AllBPchannelsData	= $this->getAllChannelMethod();
	$channelMaped	= array();
	if($AllBPchannelsData){
		foreach($AllBPchannelsData as $acc1 => $AllBPchannelsDatas){
			foreach($AllBPchannelsDatas as $cID	=> $AllBPchannelsDatass){
				$channelMaped[$cID]	= $AllBPchannelsDatass;
			}
		}
	}
	$return				= array();
	$orderIds			= array();
	$saveCronTime		= array();
	$this->config		= $this->accountConfig[$account1Id];
	$datas				= $this->ci->db->order_by('id','desc')->get_where('cron_management', array('type' => 'allupdatesales'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	if(!$cronTime){
		$cronTime	= strtotime('-30 days');
	}
	$saveCronTime[]		= $cronTime;
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	
	$FetchChannelIds	= array();
	$FetchCountries		= array();
	$FetchCountry		= array();
	$OrderUpdateConfigDatas	= $this->ci->db->get_where('taxupdate_config', array('brightpearlAccountId' => $account1Id))->row_array();
	if($OrderUpdateConfigDatas){
		if($OrderUpdateConfigDatas['channelIds']){
			$FetchChannelIds	= explode(",",$OrderUpdateConfigDatas['channelIds']);
		}
		if($OrderUpdateConfigDatas['countries']){
			$FetchCountry		= explode(",",$OrderUpdateConfigDatas['countries']);
		}
		if($FetchCountry){
			foreach($FetchCountry as $FetchCountryData){
				$FetchCountries[]	= strtolower($FetchCountryData);
			}
		}
	}
	
	$url		= '/order-service/order-search?orderTypeId=1&isClosed=false&placedOn='.$cronTime.'/';
	$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	$header		= array_column($response['metaData']['columns'],'name');
	if($response['results']){
		foreach($response['results'] as $result){
			$orderIds[$result['0']]	= $result['0'];
		}
		if($response['metaData']){
			for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if($response1['results']){
					foreach($response1['results'] as $result){
						$orderIds[$result['0']]	= $result['0'];
					}
				}
			}
		}
	}
	if($orderIds){
		$orderIds	= array_filter($orderIds);
		sort($orderIds);
		$orderDatas	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');	
		foreach($orderDatas as $OrderInfoList){
			$orderId		= $OrderInfoList['id'];
			$Country		= strtolower($OrderInfoList['parties']['delivery']['countryIsoCode3']);
			$channelId		= $OrderInfoList['assignment']['current']['channelId'];
			$IsInvoiced		= $OrderInfoList['invoices']['0']['invoiceReference'];
			$channelName	= '';
			if($IsInvoiced){
				continue;
			}
			if($FetchChannelIds AND $channelId){
				if(!in_array($channelId,$FetchChannelIds)){
					continue;
				}
			}
			if($FetchCountries AND $Country){
				if(!in_array($Country,$FetchCountries)){
					continue;
				}
			}
			if($channelId){
				if($channelMaped[$channelId]){
					$channelName	= $channelMaped[$channelId]['name'];
				}
			}
			$return[$account1Id][$orderId]['orders']	= array(
				'account1Id'		=> $account1Id, 
				'orderId'			=> $orderId,
				'orderType'			=> 'SO',
				'parentOrderId'		=> $OrderInfoList['parentOrderId'],
				'reference'			=> $OrderInfoList['reference'], 
				'channelId'			=> $OrderInfoList['assignment']['current']['channelId'],
				'channelName'		=> $channelName,
				'Country'			=> strtoupper($Country),
				'status'			=> 0,
				'created'			=> date('Y-m-d H:i:s', strtotime($OrderInfoList['createdOn'])),
				'rowData'			=> json_encode($OrderInfoList),
			);
			$saveCronTime[]		= strtotime($OrderInfoList['placedOn']);
		}
	}
	$returns[$account1Id]	= array('return' => $return,'saveTime' => max($saveCronTime));
}
?>