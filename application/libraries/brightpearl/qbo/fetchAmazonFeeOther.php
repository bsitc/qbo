<?php
//Qbo
$this->reInitialize();
$returns				= array();
$enableAmazonFeeOther	= $this->ci->globalConfig['enableAmazonFeeOther'];
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$enableAmazonFeeOther){continue;}
	
	$returnKey			= 0;
	$return				= array();
	$journalId			= array();
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'amazonfeeother'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	if(!$cronTime){
		$cronTime	= strtotime('-120 days');
	}
	$saveCronTime		= array();
	$saveCronTime[]		= $cronTime;
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	
	$excludeJournalDes	= $this->config['details'];
	$saveTaxDate		= $this->config['taxDate'];
	$SaveJournalAccount	= array();
	$SaveJournalTypes	= array();
	$JournalTypeCodes	= $this->config['amazonFeeOtherJournalType'];
	$JournalTypeCode	= explode(",",$JournalTypeCodes);
	$JournalAccountCode	= $this->config['amazonFeeOtherJournalAccount'];
	$JournalAccountAll	= explode(",",$JournalAccountCode);
	foreach($JournalAccountAll as $journalAcc){
		if($journalAcc){
			$SaveJournalAccount[]	= trim(strtoupper($journalAcc));
		}
	}
	foreach($JournalTypeCode as $JournalTypeCodess){
		if($JournalTypeCodess){
			$SaveJournalTypes[]	= trim(strtoupper($JournalTypeCodess));
		}
	}
	
	if(!$excludeJournalDes OR !$SaveJournalTypes OR !$SaveJournalAccount){
		continue;
	}
	
	if($SaveJournalAccount AND $SaveJournalTypes){
		foreach($SaveJournalTypes as $SaveJournalTypesss){
			foreach($SaveJournalAccount as $SaveJournalAccountCode){
				$url		= '/accounting-service/journal-search?journalType='.$SaveJournalTypesss.'&nominalCode='.$SaveJournalAccountCode.'&journalDateEntered=' . $cronTime . '/';
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id);
				if($response[$account1Id]){
					$header	= array_column($response[$account1Id]['metaData']['columns'],"name");
					if($response[$account1Id]){
						foreach($response[$account1Id]['results'] as $result){
							$row			= array_combine($header,$result);
							$journalId[]	= $row['journalId'];
						}
					}
					if($response[$account1Id]['metaData']){
						for($i = 500; $i <= $response[$account1Id]['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if($response1['results']){
								foreach($response1['results'] as $result){
									$row			= array_combine($header,$result);
									$journalId[]	= $row['journalId'];
								}
							}
						}
					}
				}
			}
		}
	}
	$journalId	= array_unique($journalId);
	$journalId	= array_filter($journalId);
	if(!$journalId){
		continue;
	}
	
	$resDatas	= $this->getResultById($journalId,'/accounting-service/journal',$account1Id,200,'0','');
	if($resDatas){
		$allStoredFesData	= array();
		$allStoredFesTemp	= $this->ci->db->select('journalId')->get_where('amazonFeesOther',array('journalId <>' => ''))->result_array();
		if($allStoredFesTemp){
			foreach($allStoredFesTemp as $allStoredFesTemps){
				$allStoredFesData[$allStoredFesTemps['journalId']]	= $allStoredFesTemps;
			}
		}
		foreach($resDatas as $resData){
			foreach($account2Ids as $account2Id){
				$journalId		= $resData['id'];
				$description	= $resData['description'];
				$channelid		= '';
				$totalAmt		= 0;
				$saveAccId1		= $account1Id;
				$saveAccId2		= '';
				$config2     	= $this->account2Config[$account2Id['id']];
				if($allStoredFesData[$journalId]){continue;}
				if(substr_count(strtolower($description),strtolower($excludeJournalDes))){continue;}
				
				$taxDate		= date('Ymd',strtotime($resData['taxDate']));
				$taxDate		= $resData['taxDate'];
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$QboOffset		= 0;
				$diff			= $BPDateOffset - $QboOffset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate	= $date->format('Ymd');
				if($taxDate){
					if($taxDate < $saveTaxDate){
						continue;
					}
				}
				
				$debits			= $resData['debits'];
				$credits		= $resData['credits'];
				$dabitsDatas	= array();
				$creditsDatas	= array();
				if(!$debits['0']){
					$debits		= array($debits);
				}
				if(!$credits['0']){
					$credits	= array($credits);
				}
				foreach($credits as $credit){
					if(!$channelid){
						$channelid	= $credit['assignment']['channelId'];
					}
					$totalAmt	+= $credit['transactionAmount'];
				}
				foreach($debits as $debit){
					if(!$channelid){
						$channelid	= $debit['assignment']['channelId'];
					}
				}
				
				if($channelid){
					if($config2['channelIds']){
						$config2Channels	= explode(",",$config2['channelIds']);
						if($config2Channels){
							if(in_array($channelid,$config2Channels)){
								$saveAccId2	= $account2Id['id'];
							}
						}
					}
					else{
						$saveAccId2	= $account2Id['id'];
					}
				}
				else{continue;
				}
				if(!$saveAccId2){continue;}
				
				$return[$saveAccId1][$journalId]	= array(
					'account1Id'		=> $saveAccId1,
					'account2Id'		=> $saveAccId2,
					'journalId'			=> $journalId,
					'journalTypeCode'	=> $resData['journalTypeCode'],
					'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
					'createdOn'			=> date('Y-m-d H:i:s', strtotime($resData['createdOn'])),
					'currencyId'		=> $resData['currencyId'],
					'channelid'			=> $channelid,
					'exchangeRate'		=> $resData['exchangeRate'],
					'totalAmt'			=> $totalAmt,
					'params'			=> json_encode($resData),
				);
				$saveCronTime[]		= strtotime($resData['createdOn']);
			}
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
}
return $returns;