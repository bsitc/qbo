<?php
$this->reInitialize();
$returns	= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	
	$header				= array();
	$journalIds			= array();
	$saveCronTime		= array();
	$allJournalAcc		= array();
	$allJournalCode		= array();
	$journalSearchData	= array();
	
	$cronTime		= '';
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'bpjournals'.$account1Id))->row_array();
	if((is_array($datas)) AND (isset($datas['saveTime']))){
		$cronTime	= $datas['saveTime'];
	}
	if(!$cronTime){$cronTime	= strtotime('-60 days');}
	
	$saveCronTime[]	= $cronTime;
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	
	$this->config				= $this->accountConfig[$account1Id];
	$fetchCOGSJournalType		= explode(",",$this->config['FetchCOGSJournalType']);
	$fetchCOGSJournalNominal	= explode(",",$this->config['FetchCOGSJournalNominal']);
	
	if(!empty($fetchCOGSJournalType)){
		foreach($fetchCOGSJournalType as $fetchCOGSJournalTypeTemp){
			if($fetchCOGSJournalTypeTemp){
				$allJournalCode[]	= strtoupper($fetchCOGSJournalTypeTemp);
			}
		}
	}
	if(!empty($fetchCOGSJournalNominal)){
		foreach($fetchCOGSJournalNominal as $fetchCOGSJournalNominalTemp){
			if($fetchCOGSJournalNominalTemp){
				$allJournalAcc[]	= strtoupper($fetchCOGSJournalNominalTemp);
			}
		}
	}
	
	if($this->ci->globalConfig['enableamazonfee']){
		$fetchCOGSJournalType		= explode(",",$this->config['journalType']);
		$fetchCOGSJournalNominal	= explode(",",$this->config['journalAccount']);
		if(!empty($fetchCOGSJournalType)){
			foreach($fetchCOGSJournalType as $fetchCOGSJournalTypeTemp){
				if($fetchCOGSJournalTypeTemp){
					$allJournalCode[]	= strtoupper($fetchCOGSJournalTypeTemp);
				}
			}
		}
		if(!empty($fetchCOGSJournalNominal)){
			foreach($fetchCOGSJournalNominal as $fetchCOGSJournalNominalTemp){
				if($fetchCOGSJournalNominalTemp){
					$allJournalAcc[]	= strtoupper($fetchCOGSJournalNominalTemp);
				}
			}
		}
	}
	
	if((!empty($allJournalAcc)) AND (!empty($allJournalCode))){
		foreach($allJournalAcc as $allJournalAccTemp){
			foreach($allJournalCode as $allJournalCodeTemp){
				$url		= '/accounting-service/journal-search?journalType='.$allJournalCodeTemp.'&nominalCode='.$allJournalAccTemp.'&journalDateEntered=' . $cronTime . '/';
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response)) AND (isset($response['results']))){
					$header	= array_column($response['metaData']['columns'],"name");
					foreach($response['results'] as $result){
						$row			= array_combine($header,$result);
						$journalIds[]	= $row['journalId'];
						$journalSearchData[$row['journalId']]	= $row;
					}
					if($response['metaData']['resultsAvailable'] > 500){
						for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$row			= array_combine($header,$result);
									$journalIds[]	= $row['journalId'];
									$journalSearchData[$row['journalId']]	= $row;
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(!empty($journalSearchData)){
		$savedJournalIds	= array();
		$savedJournalData	= $this->ci->db->select('journalId')->get_where('bp_journal_search')->result_array();
		if(!empty($savedJournalData)){
			$savedJournalIds	= array_column($savedJournalData,'journalId');
			$savedJournalIds	= array_filter($savedJournalIds);
			$savedJournalIds	= array_unique($savedJournalIds);
		}
		
		$journalSearchData	= array_chunk($journalSearchData, 500);
		foreach($journalSearchData as $journalSearchDataTemp){
			$sequence		= 0;
			$isInserted		= 0;
			$insertNewBatch	= array();
			foreach($journalSearchDataTemp as $journalSearchDataTempTemp){
				if(!empty($savedJournalIds)){
					if(in_array($journalSearchDataTempTemp['journalId'],$savedJournalIds)){
						continue;
					}
				}
				$insertNewBatch[$sequence]	= $journalSearchDataTempTemp;
				$insertNewBatch[$sequence]['account1Id']			= $account1Id;
				$insertNewBatch[$sequence]['journalDate']			= ($journalSearchDataTempTemp['journalDate']) ? date('Y-m-d H:i:s', strtotime($journalSearchDataTempTemp['journalDate'])) : '';
				$insertNewBatch[$sequence]['journalDueDate']		= ($journalSearchDataTempTemp['journalDueDate']) ? date('Y-m-d H:i:s', strtotime($journalSearchDataTempTemp['journalDueDate'])) : '';
				$insertNewBatch[$sequence]['journalDateEntered']	= ($journalSearchDataTempTemp['journalDateEntered']) ? date('Y-m-d H:i:s', strtotime($journalSearchDataTempTemp['journalDateEntered'])) : '';
				$insertNewBatch[$sequence]['taxReconciliationDate']	= ($journalSearchDataTempTemp['taxReconciliationDate']) ? date('Y-m-d H:i:s', strtotime($journalSearchDataTempTemp['taxReconciliationDate'])) : '';
				$saveCronTime[]	= strtotime($journalSearchDataTempTemp['journalDateEntered']);
				$sequence++;
			};
			if(!empty($insertNewBatch)){
				$isInserted	= $this->ci->db->insert_batch('bp_journal_search', $insertNewBatch); 
			}
		};
		$journalSearchData	= array();
	}
	if(!empty($journalIds)){
		$journalIds	= array_unique($journalIds);
		$journalIds	= array_filter($journalIds);
		sort($journalIds);
	}
	if(empty($journalIds)){continue;}
	
	
	$savedJournalIds	= array();
	$savedJournalData	= $this->ci->db->select('journalId')->get_where('bp_journals')->result_array();
	if(!empty($savedJournalData)){
		$savedJournalIds	= array_column($savedJournalData,'journalId');
		$savedJournalIds	= array_filter($savedJournalIds);
		$savedJournalIds	= array_unique($savedJournalIds);
	}
	
	
	$isInserted	= 0;
	$journalIds	= array_chunk($journalIds, 200);
	foreach($journalIds as $journalIdsTemp){
		$sequence		= 0;
		$insertNewBatch	= array();
		$journalResults	= $this->getResultById($journalIdsTemp,'/accounting-service/journal',$account1Id,200,'0','');
		if(!empty($journalResults)){
			foreach($journalResults as $journalResultsTemp){
				if(!empty($savedJournalIds)){
					if(in_array($journalResultsTemp['id'],$savedJournalIds)){
						continue;
					}
				}
				$insertNewBatch[$sequence]	= array(
					'account1Id'	=> $account1Id,
					'journalId'		=> $journalResultsTemp['id'],
					'taxDate'		=> ($journalResultsTemp['taxDate']) ? date('Y-m-d H:i:s', strtotime($journalResultsTemp['taxDate'])) : '',
					'rowData'		=> json_encode($journalResultsTemp),
				);
				$sequence++;
			}
		}
		if(!empty($insertNewBatch)){
			$isInserted	= $this->ci->db->insert_batch('bp_journals', $insertNewBatch); 
		}
	};
	
	if((!empty($saveCronTime)) AND ($isInserted)){
		$saveTime	= max($saveCronTime);
		$saveTime	= $saveTime - (60*10);
		$this->ci->db->insert('cron_management', array('type' => 'bpjournals'.$account1Id, 'runTime' => $saveTime, 'saveTime' => $saveTime));
	}
}