<?php
$this->reInitialize();
$AllCurrencyCodes	= $this->getAllCurrency();
$returns			= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$return			= array();
	$saveCronTime	= array();
	$this->config	= $this->accountConfig[$account1Id];
	$account2Ids	= $this->account2Details[$account1Id];
	
	$cronTime			= strtotime('-60 days');
	$saveCronTime[]		= $cronTime;
	$cronTime			= date('Y-m-d H:i:s', $cronTime);
	
	$journalId				= array();
	$SaveJournalTypeCodes	= array();
	$SaveJournalAccount		= array();
	$JournalTypeCodeAll		= explode(",",$this->config['FetchCOGSJournalType']);
	$JournalAccountAll		= explode(",",$this->config['FetchCOGSJournalNominal']);
	$saveTaxDate			= $this->config['taxDate'];
	
	$AllCurrencyCode		= $AllCurrencyCodes[$account1Id];
	foreach($JournalTypeCodeAll as $journalTypes){
		if($journalTypes){
			$SaveJournalTypeCodes[]	= strtoupper($journalTypes);
		}
	}
	foreach($JournalAccountAll as $journalAcc){
		if($journalAcc){
			$SaveJournalAccount[]	= strtoupper($journalAcc);
		}
	}
	
	if((!empty($SaveJournalAccount)) AND (!empty($SaveJournalTypeCodes))){
		foreach($SaveJournalAccount as $SaveJournalAccountCode){
			foreach($SaveJournalTypeCodes as $SaveJournalTypeCode){
				$url		= '/accounting-service/journal-search?journalType='.$SaveJournalTypeCode.'&nominalCode='.$SaveJournalAccountCode.'&journalDate=' . $cronTime . '/';
				$response	= $this->searchJournal($cronTime, $account1Id, $SaveJournalTypeCode, $SaveJournalAccountCode);
				if((is_array($response)) AND (isset($response['results']))){
					foreach($response['results'] as $result){
						$journalId[]	= $result['journalId'];
					}
					if($response['metaData']['resultsAvailable'] > 500){
						for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->searchJournal($cronTime, $account1Id, $SaveJournalTypeCode, $SaveJournalAccountCode, $i, $response['metaData']['resultsAvailable']);
							if((is_array($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$journalId[]	= $result['journalId'];
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(!empty($journalId)){
		$journalId	= array_filter($journalId);
		$journalId	= array_unique($journalId);
		sort($journalId);
	}
	
	if(empty($journalId)){continue;}
	
	$resDatas	= array();
	$resDatas	= $this->getJournals($journalId, $account1Id, 200);
	$returnKey	= 0;
	if(!empty($resDatas)){
		$allStoredSalesdata		= array();
		$allStoredCreditdata	= array();
		$allStoredPurchasedata	= array();
		$allStoredRefunddata	= array();
		
		$allStoredSales			= $this->ci->db->select('account1Id,account2Id,orderId,bpInvoiceNumber')->get_where('sales_order',array('orderId <>' => ''))->result_array();
		if(!empty($allStoredSales)){
			foreach($allStoredSales as $allStoredSalesdatas){
				$allStoredSalesdata[$allStoredSalesdatas['orderId']]		= $allStoredSalesdatas;
			}
		}
		
		$allStoredCredit		= $this->ci->db->select('account1Id,account2Id,orderId,bpInvoiceNumber')->get_where('sales_credit_order',array('orderId <>' => ''))->result_array();
		if(!empty($allStoredCredit)){
			foreach($allStoredCredit as $allStoredCreditdatas){
				$allStoredCreditdata[$allStoredCreditdatas['orderId']]		= $allStoredCreditdatas;
			}
		}
		
		$allStoredPurchase		= $this->ci->db->select('account1Id,account2Id,orderId')->get_where('purchase_order',array('orderId <>' => ''))->result_array();
		if(!empty($allStoredPurchase)){
			foreach($allStoredPurchase as $allStoredPurchasedatas){
				$allStoredPurchasedata[$allStoredPurchasedatas['orderId']]	= $allStoredPurchasedatas;
			}
		}
		
		$allStoredRefund		= $this->ci->db->select('account1Id,account2Id,orderId,bpInvoiceNumber')->get_where('refund_receipt',array('orderId <>' => ''))->result_array();
		if(!empty($allStoredRefund)){
			foreach($allStoredRefund as $allStoredRefunddatas){
				$allStoredRefunddata[$allStoredRefunddatas['orderId']]		= $allStoredRefunddatas;
			}
		}
		foreach($resDatas as $resData){
			$journalTypeCode		= $resData['journalTypeCode'];
			if($journalTypeCode){
				if(!in_array($journalTypeCode,$SaveJournalTypeCodes)){
					continue;
				}
			}
			//taxdate chanages
			$taxDate		= $resData['taxDate'];
			/* $BPDateOffset	= (int)substr($taxDate,23,3);
			$QboOffset		= 0;
			$diff			= $BPDateOffset - $QboOffset;
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff	.= ' hour';
				$date->modify($diff);
			}
			$taxDate	= $date->format('Ymd');
			if($taxDate){
				if($taxDate < $saveTaxDate){
					continue;
				}
			} */
			$journalsId		= $resData['id'];
			$saveAccId1		= $account1Id;
			$debits			= $resData['debits'];
			$credits		= $resData['credits'];
			$dabitsDatas	= array();
			$creditsDatas	= array();
			if(!$debits['0']){
				$debits		= array($debits);
			}
			if(!$credits['0']){
				$credits	= array($credits);
			}
			foreach($debits as $debit){
				if($debit['orderId']){
					if(isset($dabitsDatas[$debit['orderId']])){
						$dabitsDatas[$debit['orderId']]['transactionAmount']	+= $debit['transactionAmount'];
						if($debit['assignment']['channelId']){
							$dabitsDatas[$debit['orderId']]['assignment']['channelId'] = $debit['assignment']['channelId'];
						}
					}
					else{
						$dabitsDatas[$debit['orderId']]	= $debit;
					}
				}
			}
			foreach($credits as $credit){
				if($credit['orderId']){
					if(isset($creditsDatas[$credit['orderId']])){
						$creditsDatas[$credit['orderId']]['transactionAmount']	+= $credit['transactionAmount'];
						if($credit['assignment']['channelId']){
							$creditsDatas[$credit['orderId']]['assignment']['channelId'] = $credit['assignment']['channelId'];
						}
					}
					else{
						$creditsDatas[$credit['orderId']]	= $credit;
					}
				}
			}
			$channelId			= '';
			$taxCode			= '';
			$BPorderId			= '';
			$invoiceReference	= '';
			$debitAmount		= '';
			$creditAmount		= '';
			$debitNominalCode	= '';
			$creditNominalCode	= '';
			$saveAccId2			= '';
			$skipJournal		= 0;
			$currencyCode		= '';
			foreach($dabitsDatas as $orderId => $dabitsData){
				$saveAccId2	= '';
				if((!$allStoredSalesdata[$orderId]) AND (!$allStoredCreditdata[$orderId]) AND (!$allStoredPurchasedata[$orderId]) AND (!$allStoredRefunddata[$orderId])){
					$skipJournal	= 1;
					continue;
				}
				if($allStoredSalesdata[$orderId]){
					$saveAccId2	= $allStoredSalesdata[$orderId]['account2Id'];
				}
				elseif($allStoredCreditdata[$orderId]){
					$saveAccId2	= $allStoredCreditdata[$orderId]['account2Id'];
				}
				elseif($allStoredRefunddata[$orderId]){
					$saveAccId2	= $allStoredRefunddata[$orderId]['account2Id'];
				}
				elseif($allStoredPurchasedata[$orderId]){
					if($journalTypeCode != 'GO'){
						$skipJournal	= 1;
						continue;
					}
					else{
						$saveAccId2	= $allStoredPurchasedata[$orderId]['account2Id'];
					}
				}
				else{
					$skipJournal	= 1;
					continue;
				}
				$BPorderId			= $orderId;
				if($dabitsData['assignment']['channelId']){
					$channelId			= $dabitsData['assignment']['channelId'];
				}
				if($dabitsData['taxCode']){
					$taxCode			= $dabitsData['taxCode'];
				}
				if($dabitsData['invoiceReference']){
					$invoiceReference	= $dabitsData['invoiceReference'];
				}
				if($dabitsData['transactionAmount']){
					$debitAmount		= $dabitsData['transactionAmount'];
				}
				if($dabitsData['nominalCode']){
					$debitNominalCode	= $dabitsData['nominalCode'];
				}
			}
			foreach($creditsDatas as $orderId => $creditsData){
				if($skipJournal){
					continue;
				}
				$BPorderId			= $orderId;
				if($creditsData['assignment']['channelId']){
					$channelId			= $creditsData['assignment']['channelId'];
				}
				if($creditsData['taxCode']){
					$taxCode			= $creditsData['taxCode'];
				}
				if($creditsData['invoiceReference']){
					$invoiceReference	= $creditsData['invoiceReference'];
				}
				if($creditsData['transactionAmount']){
					$creditAmount		= $creditsData['transactionAmount'];
				}
				if($creditsData['nominalCode']){
					$creditNominalCode	= $creditsData['nominalCode'];
				}
			}
			if(!$BPorderId){
				continue;
			}
			if(!$saveAccId2){
				continue;
			}
			if(!$invoiceReference){
				if($allStoredSalesdata[$BPorderId]){
					$invoiceReference	= $allStoredSalesdata[$BPorderId]['bpInvoiceNumber'];
				}
				elseif($allStoredCreditdata[$BPorderId]){
					$invoiceReference	= $allStoredCreditdata[$BPorderId]['bpInvoiceNumber'];
				}
				elseif($allStoredRefunddata[$BPorderId]){
					$invoiceReference	= $allStoredRefunddata[$BPorderId]['bpInvoiceNumber'];
				}
				/* elseif($allStoredPurchasedata[$BPorderId]){
					$invoiceReference	= $allStoredPurchasedata[$BPorderId]['bpInvoiceNumber'];
				} */
				else{
					//
				}
			}
			$OrderType	= '';
			if($allStoredSalesdata[$BPorderId]){
				$OrderType	= 'SO';
			}
			elseif($allStoredCreditdata[$BPorderId]){
				$OrderType	= 'SC';
			}
			elseif($allStoredRefunddata[$BPorderId]){
				$OrderType	= 'RR';
			}
			elseif($allStoredPurchasedata[$BPorderId]){
				$OrderType	= 'PO';
			}
				
			$return[$saveAccId1][$journalsId]	= array(
				'account1Id'		=> $saveAccId1,
				'account2Id'		=> $saveAccId2,
				'journalsId'		=> $journalsId,
				'orderId'			=> $BPorderId,
				'invoiceReference'	=> $invoiceReference,
				'creditAmount'		=> $creditAmount,
				'debitAmount'		=> $debitAmount,
				'journalTypeCode'	=> $resData['journalTypeCode'],
				'creditNominalCode'	=> $creditNominalCode,
				'debitNominalCode'	=> $debitNominalCode,
				'taxCode'        	=> $taxCode,
				'channelId'        	=> $channelId,
				'currencyCode'		=> $AllCurrencyCode[$resData['currencyId']]['code'],
				'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
				'params'			=> json_encode($resData),
				'created'			=> date('Y-m-d H:i:s', strtotime($resData['createdOn'])),
				'OrderType'			=> $OrderType,
			);
			$saveCronTime[]		= strtotime($resData['createdOn']);
			$returnKey++;
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
}
return $returns;