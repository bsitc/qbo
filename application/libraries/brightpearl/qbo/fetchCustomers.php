<?php
//QBO fetchCustomer LIbrary
$this->reInitialize();
$returns	= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$path	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'customers'. DIRECTORY_SEPARATOR;
	if(!is_dir($path)){
		mkdir($path,0777,true);
		chmod(dirname($path), 0777);
	}
	
	$customerIds		= array();
	$AllcustomerIds		= array();
	$cronTime			= '';
	$startTime			= '';
	$webTaskID			= '';
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$customerIds		= $objectId;
		$FetchCustomerID	= $objectId;
	}
	elseif($cronTime){
		$fetchIDs	= $this->ci->db->get_where('temp', array('id' => $cronTime))->row_array();
		if($fetchIDs){
			$customerIds	= json_decode($fetchIDs['rowData'],true);
			$this->ci->db->update('temp',array('status' => 1), array('id' => $cronTime));
			$startTime		= strtotime('now');
			$webTaskID		= $cronTime;
			$cronTime		= '';
		}
	}
	$saveBundleProId	= array();
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'bpcustomer'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	$FetchCustomerID	= '';
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$customerIds		= $objectId;
		$FetchCustomerID	= $objectId;
	}
	if(!$customerIds){
		$customerId	= array();
		if(!$cronTime){
			$url		= '/contact-service/contact/';
			$response	= $this->getCurl($url, "OPTIONS", '', 'json', $account1Id)[$account1Id];
			if((is_array($response)) AND (isset($response['getUris']))){
				foreach($response['getUris'] as $getUris){
					$url		= '/contact-service/' . $getUris;
					$response	= $this->getCurl($url, 'GET', '', 'json', $account1Id)[$account1Id];
					if((is_array($response)) AND (!empty($response))){
						foreach($response as $result){
							$customerId[]	= $result['contactId'];
						}
						
					}
				}
			}
			/* $customerId	= array_chunk($customerId,10000);
			foreach($customerId as $rowCust){
				$this->ci->db->insert('temp',array('type' => 'customer','rowData' =>json_encode($rowCust)));
			}
			return false; */
			
		} 
		else{
			$datetime	= new DateTime(date('c',$cronTime));
			$cronTime	= $datetime->format(DateTime::ATOM);
			$cronTime	= str_replace("+","%2B",$cronTime);
			$urls		= array('/contact-service/contact-search?updatedOn=' . $cronTime . '/', '/contact-service/contact-search?createdOn=' . $cronTime . '/');
			foreach($urls as $url){
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response)) AND (isset($response['results']))){
					foreach($response['results'] as $result){
						$customerId[]	= $result['0'];
					}
					if($response['metaData']['resultsAvailable'] > 500){
						for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$customerId[]	= $result['0'];
								}
							}
						}
					}
				}
			}
		}
		$customerIds	= array_unique($customerId);
		if(!$customerIds){
			continue;
		}
	}
	if(is_string($customerIds)){
		$customerIds	= array($customerIds);
	}
	if(!$customerIds){
		continue;
	}
	$AllcustomerIds		= array_chunk($customerIds,200,true);
	$updatedTimes		= array();
	$MaxUpdatedTimes	= '';
	$saveTime			= '';
	$checkInsertFlag	= 0;
	foreach($AllcustomerIds as $AllcustomerId){
		$returnKey				= 0;
		$return					= array();
		$CustomersDatas			= array();
		$AllcustomerId			= array_filter($AllcustomerId);
		$resDatas				= $this->getResultById($AllcustomerId,'/contact-service/contact/',$account1Id,200,0,'?includeOptional=customFields,postalAddresses');
		foreach($resDatas as $fetchedCustomers){
			$customerId			= $fetchedCustomers['contactId'];
			foreach($account2Ids as $account2Id){
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
				
				$bilAddress	= @$fetchedCustomers['postalAddresses'][$fetchedCustomers['postAddressIds']['BIL']];
				$delAddress	= @$fetchedCustomers['postalAddresses'][$fetchedCustomers['postAddressIds']['DEL']];
				$return[$saveAccId1][$returnKey]['customers']	= array(
					'account1Id'		=> $saveAccId1,
					'account2Id'		=> $saveAccId2,
					'customerId'		=> @$fetchedCustomers['contactId'],
					'email'				=> @$fetchedCustomers['communication']['emails']['PRI']['email'],
					'fname'				=> @$fetchedCustomers['firstName'],
					'lname'				=> @$fetchedCustomers['lastName'],
					'phone'				=> @$fetchedCustomers['communication']['telephones']['PRI'], 
					'addressFname'		=> @$fetchedCustomers['firstName'],
					'addressLname'		=> @$fetchedCustomers['lastName'],
					'address1'			=> @$delAddress['addressLine1'],
					'address2'			=> @$delAddress['addressLine2'],
					'city'				=> @$delAddress['addressLine3'],
					'state'				=> @$delAddress['addressLine4'],
					'zip'				=> @$delAddress['postalCode'],
					'company'			=> (@$fetchedCustomers['organisation']['name']) ? (@$fetchedCustomers['organisation']['name']) : (''),
					'countryCode'		=> @$delAddress['countryIsoCode'],
					'isSupplier'		=> @$fetchedCustomers['relationshipToAccount']['isSupplier'],
					'isPrimary'			=> @$fetchedCustomers['isPrimaryContact'],
					'created'			=> date('Y-m-d H:i:s', strtotime($fetchedCustomers['createdOn'])),
					'updated'			=> date('Y-m-d H:i:s', strtotime($fetchedCustomers['updatedOn'])),
					'params'			=> json_encode($fetchedCustomers),
					'accountReference'	=> (@$fetchedCustomers['assignment']['current']['accountReference']) ? (@$fetchedCustomers['assignment']['current']['accountReference']) : (''),
				);
				if(@$fetchedCustomers['updatedOn']){
					$updatedTimes[]	= strtotime(@$fetchedCustomers['updatedOn']);
				}
				$returnKey++;
			}
			
			//new logStoringFunctionality
			$logsData		= array();
			$allStoredLogs	= array();
			$fileLogPath	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'customers'. DIRECTORY_SEPARATOR;
			$fileLogPath	= $fileLogPath.$customerId.'.logs';
			if(file_exists($fileLogPath)){
				$latestStoredLogs	= array();
				$allStoredLogs		= json_decode(file_get_contents($fileLogPath), true);
				$tempStoredLogs		= $allStoredLogs;
				if((is_array($tempStoredLogs)) AND (!empty($tempStoredLogs))){
					krsort($tempStoredLogs);
					foreach($tempStoredLogs as $logsKey => $tempLogsTemp){
						$latestStoredLogs	= $tempLogsTemp;
						break;
					}
				}
				if(!empty($latestStoredLogs)){
					$tempFetchedLogs	= $fetchedCustomers;
					unset($tempFetchedLogs['createdOn']);
					unset($tempFetchedLogs['updatedOn']);
					
					unset($latestStoredLogs['createdOn']);
					unset($latestStoredLogs['updatedOn']);
					
					if(md5(json_encode($latestStoredLogs)) != md5(json_encode($tempFetchedLogs))){
						$allStoredLogs[date('c', strtotime("now"))]	= $fetchedCustomers;
						file_put_contents($fileLogPath,json_encode($allStoredLogs));
					}
				}
				else{
					file_put_contents($fileLogPath,json_encode($allStoredLogs));
				}
			}
			else{
				$logsData	= array(date('c', strtotime("now"))	=> $fetchedCustomers);
				file_put_contents($fileLogPath,json_encode($logsData));
			}
		}
		
		//Inserting the customers in Database in chunk of 200
		$saveDatasTemps			= $this->ci->db->select('id,account1Id,account2Id,customerId,status')->get_where('customers')->result_array(); 
		$saveCustomerDatas		= array();
		if($saveDatasTemps){
			foreach($saveDatasTemps as $saveDatasTemp){
				$key						= @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($saveDatasTemp['account1Id'] . '_' .$saveDatasTemp['account2Id'] . '_' . $saveDatasTemp['customerId'] )));
				$saveCustomerDatas[$key]	= $saveDatasTemp;
			}
		}
		
		$batchCustomerInsert	= array(); 
		$batchCustomerUpdate	= array();
		$batchAddressInsert		= array();
		$batchAddressUpdate		= array();
		$fetchCustomersDatas	= $return;
		foreach($fetchCustomersDatas as $account1Id => $fetchCustomersData){
			foreach($fetchCustomersData as $customerId => $fetchCustomers){
				$cust		= $fetchCustomers['customers'];
				$customerId	= $cust['customerId'];
				$key		= @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($cust['account1Id'] . '_' .$cust['account2Id'] . '_' . $cust['customerId'])));
				if(@$saveCustomerDatas[$key]){
					$cust['id']	= $saveCustomerDatas[$key]['id'];
					$SetStatus	= $saveCustomerDatas[$key]['status'];
					if(($SetStatus == 3) OR ($SetStatus == 4)){
						continue;
					}
					$this->ci->db->where(array('id' => $cust['id']))->update('customers',$cust);
					$afftectedRows	= $this->ci->db->affected_rows();
					if($afftectedRows){
						$cust['status']			= '2';
						$batchCustomerUpdate[]	= $cust;
					}
					unset($saveCustomerDatas[$key]);
				} 
				else{
					$batchCustomerInsert[]	= $cust;
				}
			}
		}
		if($batchCustomerInsert){
			$checkInsertFlag	= true;
			$this->ci->db->insert_batch('customers', $batchCustomerInsert);
		}
		if($batchCustomerUpdate){
			$checkInsertFlag	= true;
			$this->ci->db->update_batch('customers', $batchCustomerUpdate, 'id');
		}
	}
	$saveTime				= strtotime(date('Y-m-d\TH:i:s',strtotime('-5 days')));
	if($updatedTimes){
		$MaxUpdatedTimes	= max($updatedTimes);
		if($MaxUpdatedTimes){
			$saveTime	= ($MaxUpdatedTimes - (60*10));
		}
	}
	if($checkInsertFlag){
		if(!$FetchCustomerID){
			$this->ci->db->insert('cron_management', array('type' => 'bpcustomer'.$account1Id, 'saveTime' => $saveTime));
		}
	}
	if($webTaskID){
		if($startTime){
			$endTime			= strtotime('now');
			$processedSecond	= ($endTime - $startTime); 
			$this->ci->db->update('temp',array('executionSeconds' => $processedSecond), array('id' => $webTaskID));
		}
	}
}
?>