<?php
$this->reInitialize();
$AllCurrencyCodes	= $this->getAllCurrency();
$returns			= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$return					= array();
	$this->config			= $this->accountConfig[$account1Id];
	$account2Ids			= $this->account2Details[$account1Id];
	$AllCurrencyCode		= $AllCurrencyCodes[$account1Id];
	$datas					= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'grnijournal'.$account1Id))->row_array();
	$saveCronTime			= array();
	$saveCronTime[]			= $datas['saveTime'];
	$cronTime				= $datas['saveTime'];
	if(!$cronTime){
		$cronTime	= strtotime('-60 days');
	}
	$datetime				= new DateTime(date('c',$cronTime));
	$cronTime				= $datetime->format(DateTime::ATOM);
	$cronTime				= str_replace("+","%2B",$cronTime);
	$SaveJournalTypeCodes	= array();
	$SaveJournalAccount		= array();
	$JournalTypeCodeAll		= explode(",",'PG,LC');
	$JournalAccountAll		= explode(",",$this->config['FetchGRNIJournalNominal']);
	$saveTaxDate			= $this->config['taxDate'];
	
	foreach($JournalTypeCodeAll as $journalTypes){
		if($journalTypes){
			$SaveJournalTypeCodes[]	= strtoupper($journalTypes);
		}
	}
	foreach($JournalAccountAll as $journalAcc){
		if($journalAcc){
			$SaveJournalAccount[]	= strtoupper($journalAcc);
		}
	}
	
	$grniOrderIds	= array();
	$journalId		= array();
	
	if((!empty($SaveJournalAccount)) AND (!empty($SaveJournalTypeCodes))){
		foreach($SaveJournalAccount as $SaveJournalAccountCode){
			foreach($SaveJournalTypeCodes as $SaveJournalTypeCode){
				$url		= '/accounting-service/journal-search?journalType='.$SaveJournalTypeCode.'&nominalCode='.$SaveJournalAccountCode.'&journalDateEntered=' . $cronTime . '/';
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response)) AND (isset($response['results']))){
					$header	= array_column($response['metaData']['columns'],"name");
					foreach($response['results'] as $result){
						$row	= array_combine($header,$result);
						if(strlen($row['orderId']) > 1){
							$journalId[]	= $row['journalId'];
							$grniOrderIds[]	= $row['orderId'];
						}
						else{
							continue;
						}
					}
					if($response['metaData']['resultsAvailable'] > 500){
						for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$row	= array_combine($header,$result);
									if(strlen($row['orderId']) > 1){
										$journalId[]	= $row['journalId'];
										$grniOrderIds[]	= $row['orderId'];
									}
									else{
										continue;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(!empty($journalId)){
		$journalId		= array_filter($journalId);
		$journalId		= array_unique($journalId);
		sort($journalId);
	}
	if(!empty($grniOrderIds)){
		$grniOrderIds	= array_filter($grniOrderIds);
		$grniOrderIds	= array_unique($grniOrderIds);
		sort($grniOrderIds);
	}
	
	if((empty($journalId)) OR (empty($grniOrderIds))){
		continue;
	}
	
	$journalDatas	= array();
	$journalDatas	= $this->getResultById($journalId,'/accounting-service/journal',$account1Id,200,'0','');
	
	$grniOrderDatas	= array();
	$grniOrderDatas	= $this->getResultById($grniOrderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	
	$ordersInfoMapping	= array();
	if(!empty($grniOrderDatas)){
		foreach($grniOrderDatas as $grniOrderDatasTemp){
			if(isset($grniOrderDatasTemp['orderTypeCode']) AND (strtolower($grniOrderDatasTemp['orderTypeCode']) == 'po')){
				$ordersInfoMapping[$grniOrderDatasTemp['id']]	= array(
					'orderId'			=> $grniOrderDatasTemp['id'],
					'invoiceReference'	=> $grniOrderDatasTemp['invoices'][0]['invoiceReference'],
					'taxDate'			=> $grniOrderDatasTemp['invoices'][0]['taxDate'],
					'channelId'			=> $grniOrderDatasTemp['assignment']['current']['channelId'],
					'warehouseId'		=> $grniOrderDatasTemp['warehouseId'],
				);
			}
		}
	}
	
	if(!empty($journalDatas)){
		$savedPurchaseOrderData		= array();
		$savedPurchaseOrderDataTemp	= $this->ci->db->select('id,orderId,bpInvoiceNumber')->get_where('purchase_order')->result_array();
		if(!empty($savedPurchaseOrderDataTemp)){
			foreach($savedPurchaseOrderDataTemp as $savedPurchaseOrderDataTemps){
				$savedPurchaseOrderData[$savedPurchaseOrderDataTemps['orderId']]	= $savedPurchaseOrderDataTemps;
			}
		}
		
		foreach($journalDatas as $journalDatasTemp){
			foreach($account2Ids as $account2Id){
				$journalTypeCode	= $journalDatasTemp['journalTypeCode'];
				if($journalTypeCode){
					if(!in_array($journalTypeCode,$SaveJournalTypeCodes)){
						continue;
					}
				}
				
				$config2Channels	= array();
				$config2Warehouses	= array();
				$taxDate			= $journalDatasTemp['taxDate'];
				$journalsId			= $journalDatasTemp['id'];
				$saveAccId1			= $account1Id;
				$saveAccId2			= '';
				$config2			= $this->account2Config[$account2Id['id']];
				if($config2['channelIdspopc']){
					$config2Channels	= explode(",",$config2['channelIdspopc']);
				}
				if($config2['warehousespopc']){
					$config2Warehouses	= explode(",",$config2['warehousespopc']);
				}
				/* if($allStoredFesData[$journalId]){continue;} */
				$debits				= $journalDatasTemp['debits'];
				$credits			= $journalDatasTemp['credits'];
				$creditAmount		= 0;
				$debitAmount		= 0;
				$journalOrderId		= '';
				
				if(!$debits['0']){
					$debits		= array($debits);
				}
				if(!$credits['0']){
					$credits	= array($credits);
				}
				foreach($credits as $credit){
					if(!$journalOrderId){
						$journalOrderId	= $credit['orderId'];
					}
					$creditAmount	+= $credit['transactionAmount'];
				}
				foreach($debits as $debit){
					if(!$journalOrderId){
						$journalOrderId	= $debit['orderId'];
					}
					$debitAmount	+= $debit['transactionAmount'];
				}
				
				if(!$journalOrderId){continue;}
				if(!isset($ordersInfoMapping[$journalOrderId])){continue;}
				
				if(!empty($savedPurchaseOrderData)){
					if(isset($savedPurchaseOrderData[$journalOrderId])){
						if(strlen($savedPurchaseOrderData[$journalOrderId]['bpInvoiceNumber']) > 0){
							continue;
						}
					}
				}
				
				$mainOrderData	= $ordersInfoMapping[$journalOrderId];
				$warehouseId	= $mainOrderData['warehouseId'];
				$channelId		= $mainOrderData['channelId'];
				
				if($config2Channels || $config2Warehouses){
					if((!$channelId) AND (!$warehouseId)){
						continue;
					}
					if($config2Channels){
						if(!in_array($channelId,$config2Channels)){
							if((!$channelId) AND (in_array('NullChannel',$config2Channels))){
								//
							}
							else{
								continue;
							}
						}
					}
					if($config2Warehouses){
						if(!in_array($warehouseId,$config2Warehouses)){
							continue;
						}
					}
				}
				$saveAccId2	= $account2Id['id'];
				if(!$saveAccId2){continue;}
				$return[$saveAccId1][$journalsId]	= array(
					'account1Id'		=> $saveAccId1,
					'account2Id'		=> $saveAccId2,
					'journalsId'		=> $journalsId,
					'orderId'			=> $journalOrderId,
					'invoiceReference'	=> ($mainOrderData['invoiceReference']) ? $mainOrderData['invoiceReference'] : '',
					'creditAmount'		=> $creditAmount,
					'debitAmount'		=> $debitAmount,
					'journalTypeCode'	=> $journalDatasTemp['journalTypeCode'],
					'creditNominalCode'	=> $credits[0]['nominalCode'],
					'debitNominalCode'	=> $debits[0]['nominalCode'],
					'channelId'			=> $channelId,
					'currencyCode'		=> $AllCurrencyCode[$journalDatasTemp['currencyId']]['code'],
					'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
					'params'			=> json_encode($journalDatasTemp),
					'created'			=> date('Y-m-d H:i:s', strtotime($journalDatasTemp['createdOn'])),
					'OrderType'			=> 'PO',
					'orderTaxdate'		=> ($mainOrderData['taxDate']) ? $mainOrderData['taxDate'] : '',
					'orderInvoiceRef'	=> ($mainOrderData['invoiceReference']) ? $mainOrderData['invoiceReference'] : '',
					'isOrderInvoiced'	=> ($mainOrderData['invoiceReference']) ? 1 : 0,
				);
				$saveCronTime[]	= strtotime($journalDatasTemp['createdOn']);
			}
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
}