<?php
//qbo
$this->reInitialize();
$returns			= array();
$enableAmazonFees	= $this->ci->globalConfig['enableamazonfee'];
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$enableAmazonFees){continue;}
	
	$return				= array();
	$saveCronTime		= array();
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	
	$cronTime			= strtotime('-60 days');
	$saveCronTime[]		= $cronTime;
	$cronTime			= date('Y-m-d H:i:s', $cronTime);
	
	$JournalTypeCode	= $this->config['journalType'];
	$JournalAccountCode	= $this->config['journalAccount'];
	$JournalDescription	= $this->config['details'];
	$saveTaxDate		= $this->config['taxDate'];
	$SaveJournalAccount	= array();
	$JournalAccountAll	= explode(",",$this->config['journalAccount']);
	foreach($JournalAccountAll as $journalAcc){
		if($journalAcc){
			$SaveJournalAccount[]	= strtoupper($journalAcc);
		}
	}
	
	$journalId	= array();
	if(!empty($SaveJournalAccount)){
		foreach($SaveJournalAccount as $SaveJournalAccountCode){
			$url		= '/accounting-service/journal-search?journalType='.$JournalTypeCode.'&nominalCode='.$SaveJournalAccountCode.'&journalDate=' . $cronTime . '/';
			$response	= $this->searchJournal($cronTime, $account1Id, $JournalTypeCode, $SaveJournalAccountCode);
			
			if((is_array($response)) AND (isset($response['results']))){
				foreach($response['results'] as $result){
					$journalId[]	= $result['journalId'];
				}
				if($response['metaData']['resultsAvailable'] > 500){
					for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
						$url1		= $url . '&firstResult=' . $i;
						$response1	= $this->searchJournal($cronTime, $account1Id, $JournalTypeCode, $SaveJournalAccountCode, $i, $response['metaData']['resultsAvailable']);
						if((is_array($response1)) AND (isset($response1['results']))){
							foreach($response1['results'] as $result){
								$journalId[]	= $result['journalId'];
							}
						}
					}
				}
			}
		}
	}
	
	if(!empty($journalId)){
		$journalId	= array_filter($journalId);
		$journalId	= array_unique($journalId);
		sort($journalId);
	}
	
	if(empty($journalId)){continue;}
	
	$resDatas	= array();
	$resDatas	= $this->getJournals($journalId, $account1Id, 200);
	$returnKey	= 0;
	
	if(!empty($resDatas)){
		$allStoredSalesdata	= array();
		$allStoredSales		= $this->ci->db->select('account1Id,account2Id,orderId')->get_where('sales_order',array('orderId <>' => ''))->result_array();
		if(!empty($allStoredSales)){
			foreach($allStoredSales as $allStoredSalesdatas){
				$allStoredSalesdata[$allStoredSalesdatas['orderId']]	= $allStoredSalesdatas;
			}
		}
		
		foreach($resDatas as $resData){
			$description	= $resData['description'];
			if($JournalDescription){
				if(!substr_count(strtolower($description),strtolower($JournalDescription))){
					continue;
				}
			}
			else{
				continue;
			}
			$taxDate	= date('Ymd',strtotime($resData['taxDate']));
			$taxDate	= $resData['taxDate'];
			//taxdate chanages
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$QboOffset		= 0;
			$diff			= $BPDateOffset - $QboOffset;
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff	.= ' hour';
				$date->modify($diff);
			}
			$taxDate	= $date->format('Ymd');
			if($taxDate){
				if($taxDate < $saveTaxDate){
					continue;
				}
			}
			$journalsId		= $resData['id'];
			$saveAccId1		= $account1Id;
			$debits			= $resData['debits'];
			$credits		= $resData['credits'];
			$dabitsDatas	= array();
			$creditsDatas	= array();
			if(!$debits['0']){
				$debits		= array($debits);
			}
			if(!$credits['0']){
				$credits	= array($credits);
			}
			foreach($credits as $credit){
				if($credit['orderId']){
					if(!isset($creditsDatas[$credit['orderId']])){
						$creditsDatas[$credit['orderId']]['nominalCode']	= $credit['nominalCode'];
					}
				}
			}
			foreach($debits as $debit){
				if($debit['orderId']){
					if(isset($dabitsDatas[$debit['orderId']])){
						if(in_array($debit['nominalCode'],$SaveJournalAccount)){
							$dabitsDatas[$debit['orderId']]['transactionAmount']	+= $debit['transactionAmount'];
						}
					}
					else{
						if(in_array($debit['nominalCode'],$SaveJournalAccount)){
							$dabitsDatas[$debit['orderId']]	= $debit;
						}
					}
				}
			}
			foreach($dabitsDatas as $orderId => $dabitsData){
				$saveAccId2	= '';
				if(!$allStoredSalesdata[$orderId]){
					continue;
				}
				$saveAccId2	= $allStoredSalesdata[$orderId]['account2Id'];
				if(!$saveAccId2){
					continue;
				}
				if(!$saveAccId1){
					continue;
				}
				$return[$saveAccId1][$returnKey]	= array(
					'account1Id'		=> $saveAccId1,
					'account2Id'		=> $saveAccId2,
					'journalsId'		=> $journalsId,
					'orderId'			=> $orderId,
					'amount'			=> $dabitsData['transactionAmount'],
					'journalTypeCode'	=> $resData['journalTypeCode'],
					'debitNominalCode'	=> $dabitsData['nominalCode'],
					'taxCode'        	=> $dabitsData['taxCode'],
					'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
					'params'			=> json_encode($resData), 
					'fetchDate'			=> date('Y-m-d H:i:s'),
				);
				if($creditsDatas[$orderId]){
					$return[$saveAccId1][$returnKey]['creditNominalCode']	= $creditsDatas[$orderId]['nominalCode'];
				}
				$saveCronTime[]		= strtotime($resData['createdOn']);
				$returnKey++;
			}
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
}
return $returns;