<?php
$this->reInitialize();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$saveCronTime	= array();
	$paymentResult	= array();
	$headerKeys		= array();
	
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'bppayment'.$account1Id))->row_array();
	$cronTime		= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-90 days');
	}
	
	$saveCronTime[]	= $cronTime;
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	
	$url			= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	$response		= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if((is_array($response)) AND (isset($response['results']))){
		$headerKeys			= array_column($response['metaData']['columns'],'name');
		$paymentResult[]	= $response['results'];
		if($response['metaData']['resultsAvailable'] > 500){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response1)) AND (isset($response1['results']))){
					$paymentResult[]	= $response1['results'];
				}
			}
		}
	}
	
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$objectId	= array_filter($objectId);
		$objectId	= array_unique($objectId);
		sort($objectId);
		$objectIds	= array_chunk($objectId,50);
		if($objectIds){
			foreach($objectIds as $objectId){
				$url		= '/accounting-service/customer-payment-search?orderId='.implode(",",$objectId).'&sort=createdOn.DESC';
				$response1	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response1)) AND (isset($response1['results']))){
					$headerKeys			= array_column($response['metaData']['columns'],'name');
					$paymentResult[]	= $response1['results'];
				}
			}
		}
	}
	
	if(!empty($paymentResult)){
		$savedPaymentIds	= array();
		$savedPaymentData	= $this->ci->db->select('paymentId')->get_where('bp_payments')->result_array();
		if(!empty($savedPaymentData)){
			$savedPaymentIds	= array_column($savedPaymentData,'paymentId');
			$savedPaymentIds	= array_filter($savedPaymentIds);
			$savedPaymentIds	= array_unique($savedPaymentIds);
		}
		foreach($paymentResult as $paymentDatas){
			$insertData	= array();
			$sequence	= 0;
			foreach($paymentDatas as $result){
				$result	= array_combine($headerKeys,$result);
				if(!empty($savedPaymentIds)){
					if(in_array($result['paymentId'],$savedPaymentIds)){
						continue;
					}
				}
				$insertData[$sequence]	= array(
					'paymentId'				=> $result['paymentId'],
					'transactionRef'		=> $result['transactionRef'],
					'transactionCode'		=> $result['transactionCode'],
					'paymentMethodCode'		=> $result['paymentMethodCode'],
					'paymentType'			=> $result['paymentType'],
					'orderId'				=> $result['orderId'],
					'currencyId'			=> $result['currencyId'],
					'currencyCode'			=> $result['currencyCode'],
					'amountAuthorized'		=> $result['amountAuthorized'],
					'amountPaid'			=> $result['amountPaid'],
					'expires'				=> $result['expires'],
					'paymentDate'			=> $result['paymentDate'],
					'createdOn'				=> $result['createdOn'],
					'journalId'				=> $result['journalId'],
					'createdOnTemp'			=> date('Y-m-d H:i:s', strtotime($result['createdOn'])),
					'account1Id'			=> $account1Id,
				);
				$saveCronTime[]	= strtotime($result['createdOn']);
				$sequence++;
			}
			if(!empty($insertData)){
				$this->ci->db->insert_batch('bp_payments', $insertData); 
			}
		}
	}
	if(!empty($saveCronTime)){
		$saveTime	= max($saveCronTime);
		$saveTime	= $saveTime - (60*10);
		$this->ci->db->insert('cron_management', array('type' => 'bppayment'.$account1Id, 'runTime' => $saveTime, 'saveTime' => $saveTime));
	}
}
?>