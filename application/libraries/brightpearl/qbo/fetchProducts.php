<?php
$this->reInitialize();
$returns	= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$path	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'products'. DIRECTORY_SEPARATOR;
	if(!is_dir($path)){
		mkdir($path,0777,true);
		chmod(dirname($path), 0777);
	}
	
	
	$saveBundleProId	= array();
	$productIds			= array();
	$AllproductIds		= array();
	$startTime			= '';
	$cronTime			= '';
	$webTaskID			= '';
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$productIds		= $objectId;
		$FetchProductID	= $objectId;
	}
	elseif($cronTime){
		$fetchIDs	= $this->ci->db->get_where('temp', array('id' => $cronTime))->row_array();
		if($fetchIDs){
			$productIds		= json_decode($fetchIDs['rowData'],true);
			$this->ci->db->update('temp',array('status' => 1), array('id' => $cronTime));
			$startTime		= strtotime('now');
			$webTaskID		= $cronTime;
			$cronTime		= '';
		}
	}
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'bpproduct'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	$FetchProductID		= '';
	if($objectId){
		$FetchProductID	= $objectId;
	}
	if(!$productIds){
		$productIds	= array();
		if(!$cronTime){
			$url		= '/product-service/product';
			$response	= $this->getCurl($url, "OPTIONS", '', 'json', $account1Id)[$account1Id];
			if((is_array($response)) AND (isset($response['getUris']))){
				foreach($response['getUris'] as $getUris){
					$url		= '/product-service/' . $getUris;
					$response	= $this->getCurl($url, 'GET', '', 'json', $account1Id)[$account1Id];
					if((is_array($response)) AND (!empty($response))){
						foreach($response as $result){
							$productIds[]	= $result['id'];
						}
					}
				}
			}
			/* $productIds	= array_chunk($productIds,10000);
			foreach($productIds as $rowCust){
				$this->ci->db->insert('temp',array('type' => 'products','rowData' =>json_encode($rowCust)));
			}
			return false; */
		}
		else{
			$datetime	= new DateTime(date('c',$cronTime));
			$cronTime	= $datetime->format(DateTime::ATOM);
			$cronTime	= str_replace("+","%2B",$cronTime);
			$urls		= array('/product-service/product-search?updatedOn=' . $cronTime . '/', '/product-service/product-search?createdOn=' . $cronTime . '/');
			foreach($urls as $url){
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response)) AND (isset($response['results']))){
					foreach($response['results'] as $result){
						$productIds[]	= $result['0'];
					}
					if($response['metaData']['resultsAvailable'] > 500){
						for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$productIds[]	= $result['0'];
								}
							}
						}
					}
				}
			}
		}
		$productIds	= array_unique($productIds);
		if(!$productIds){
			continue;
		}
	}
	if(is_string($productIds)){
		$productIds	= array($productIds);
	}
	if(!$productIds){
		continue;
	}
	$AllproductIds		= array_chunk($productIds,200,true);
	$checkInsertFlag	= 0;
	$saveTime			= '';
	$updatedTimes		= array();
	
	foreach($AllproductIds as $AllproductId){
		$returnKey			= 0;
		$MaxUpdatedTimes	= '';
		$saveTime			= '';
		$return				= array();
		$saveBundleProId	= array();
		$productDatas		= array();
		$bundleProDatas		= $this->ci->db->get_where('product_bundle', array('account1Id' => $account1Id))->result_array(); 
		$bundleProducts		= array_column($bundleProDatas, 'productId');
		$AllproductId		= array_filter($AllproductId);
		$resDatas			= $this->getResultById($AllproductId,'/product-service/product/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
		foreach($resDatas as $resData){
			$productId		= $resData['id'];
			if($productId < 1002){continue;}
			foreach($account2Ids as $account2Id){
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
				$isLIve		= '1';
				if($resData['status'] != 'LIVE'){
					$isLIve	= '0';
				}
				if(($resData['composition']['bundle']) AND (!in_array($resData['id'], $bundleProducts))){
					$saveBundleProId[$saveAccId1][]	= array('productId' => $resData['id'], 'account1Id' => $account1Id);
				}
				if($this->config['productCustField']){
					if(!$resData['customFields'][$this->config['productCustField']]){
						$isLIve	= '0';
						if(@!$this->saveProductIds[$resData['id']]){
							continue;
						}
					}
				}
				$color	= '';
				$size	= ''; 
				$length	= '';
				if(@$resData['variations']){
					foreach($resData['variations'] as $variations){
						if((strtolower($variations['optionName']) == 'color')||(strtolower($variations['optionName']) == 'colour')){
							$color	= @$variations['optionValue'];
						}
						if(strtolower($variations['optionName']) == 'size'){
							$size	= @$variations['optionValue'];
						}
						if(strtolower($variations['optionName']) == 'sleeve length'){
							$length	= @$variations['optionValue'];
						}
					}
				}
				$newSku	= $resData['salesChannels']['0']['productName'];
				if($color){
					$newSku .= ' - ' . $color;
				}
				if($length){
					$newSku	.= ' - ' . $length;
				}
				if($this->config['productStyleNumber']){
					if(@$resData['customFields'][$this->config['productStyleNumber']]){
						$newSku	= $resData['customFields'][$this->config['productStyleNumber']];
					}
				} 
				if(@$resData['updatedOn']){
					$updatedTimes[]	= strtotime(@$resData['updatedOn']);
				}
				$return[$saveAccId1][$returnKey]	= array(
					'account1Id'		=> $saveAccId1,
					'account2Id'		=> $saveAccId2,
					'productId'			=> $resData['id'],
					'name'				=> $resData['salesChannels']['0']['productName'],
					'sku'				=> @$resData['identity']['sku'],
					'newSku'			=> $newSku,  
					'ean'				=> @$resData['identity']['ean'],
					'barcode'			=> @($resData['identity']['barcode']) ? ($resData['identity']['barcode']) : '',
					'upc'				=> @$resData['identity']['upc'],
					'isBundle'			=> @($resData['composition']['bundle']) ? ($resData['composition']['bundle']) : '0',
					'color'				=> $color,
					'isLIve'			=> $isLIve,
					'size'				=> $size,
					'description'		=> $resData['salesChannels']['0']['description']['text'],
					'created'			=> @date('Y-m-d H:i:s', strtotime($resData['createdOn'])),
					'updated'			=> @$resData['updatedOn'] ? @date('Y-m-d H:i:s', strtotime(@$resData['updatedOn'])) : '',
					'params'			=> json_encode($resData),
				);
				$returnKey++; 
			}
			
			//new logStoringFunctionality
			$logsData		= array();
			$allStoredLogs	= array();
			$fileLogPath	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'products'. DIRECTORY_SEPARATOR;
			$fileLogPath	= $fileLogPath.$productId.'.logs';
			if(file_exists($fileLogPath)){
				$latestStoredLogs	= array();
				$allStoredLogs		= json_decode(file_get_contents($fileLogPath), true);
				$tempStoredLogs		= $allStoredLogs;
				if((is_array($tempStoredLogs)) AND (!empty($tempStoredLogs))){
					krsort($tempStoredLogs);
					foreach($tempStoredLogs as $logsKey => $tempLogsTemp){
						$latestStoredLogs	= $tempLogsTemp;
						break;
					}
				}
				if(!empty($latestStoredLogs)){
					$tempFetchedLogs	= $resData;
					unset($tempFetchedLogs['createdOn']);
					unset($tempFetchedLogs['updatedOn']);
					
					unset($latestStoredLogs['createdOn']);
					unset($latestStoredLogs['updatedOn']);
					
					if(md5(json_encode($latestStoredLogs)) != md5(json_encode($tempFetchedLogs))){
						$allStoredLogs[date('c', strtotime("now"))]	= $resData;
						file_put_contents($fileLogPath,json_encode($allStoredLogs));
					}
				}
				else{
					file_put_contents($fileLogPath,json_encode($allStoredLogs));
				}
			}
			else{
				$logsData	= array(date('c', strtotime("now"))	=> $resData);
				file_put_contents($fileLogPath,json_encode($logsData));
			}
		}
		
		//Inserting the product in Database in chunk of 200
		$saveDatasTemps	= $this->ci->db->select('id,productId,account1Id,account2Id,sku,size,color,size,status')->get_where('products')->result_array();
		$saveDatas		= array();
		if($saveDatasTemps){
			foreach($saveDatasTemps as $saveDatasTemp){
				$key				= trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($saveDatasTemp['account1Id'].'_'.$saveDatasTemp['account2Id'].'_'.$saveDatasTemp['productId'])));
				$saveDatas[$key]	= $saveDatasTemp;
			}
		}
		$batchInsert	= array();
		$batchUpdate	= array();
		$productDatas	= $return;
		foreach($productDatas as $account1Id => $productData){
			foreach($productData as $row){
				if((!$row['account1Id']) OR (!$row['account2Id'])){
					continue;
				}
				$key	=  trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($row['account1Id'].'_'.$row['account2Id'].'_'.$row['productId'])));
				if(@$saveDatas[$key]){
					$row['id']	= $saveDatas[$key]['id'];
					$SetStatus	= $saveDatas[$key]['status'];
					if(($SetStatus == 3) OR ($SetStatus == 4)){
						continue;
					}
					$this->ci->db->where(array('id' => $row['id']))->update('products',$row);
					$afftectedRows		= $this->ci->db->affected_rows();
					if($afftectedRows){
						$row['status']	= ($saveDatas[$key]['status']) ? (2) : 0;
						$row['sendTo']	= $this->ci->globalConfig['postProduct'] ;
						$batchUpdate[]	= $row; 
					}
				}
				else{
					$row['sendTo']	= $this->ci->globalConfig['postProduct'];
					$batchInsert[]	= $row;
				}
			}
		}
		if($batchInsert){
			$batchInserts		= array_chunk($batchInsert, 200);
			$checkInsertFlag	= 1;
			foreach($batchInserts as $key => $batchInsert){
				$insertedBatchRes	= $this->ci->db->insert_batch('products', $batchInsert);
			}
		}
		if($batchUpdate){
			$batchUpdates		= array_chunk($batchUpdate, 200);
			$checkInsertFlag	= 1;
			foreach($batchUpdates as $key => $batchUpdate){
				$batchUpdateRes	= $this->ci->db->update_batch('products',$batchUpdate, 'id');
			}
		}
		if($saveBundleProId){
			foreach($saveBundleProId as $account1Id => $saveBundlePro){
				$saveBundlePro	= array_unique($saveBundlePro); 
				$saveBundlePro	= array_chunk($saveBundlePro,200);
				foreach($saveBundlePro as $saveBundle){
					$this->ci->db->insert_batch('product_bundle', $saveBundle);
				}
			}
		}
	}
	$saveTime				= strtotime(date('Y-m-d\TH:i:s',strtotime('-5 days')));
	if($updatedTimes){
		$MaxUpdatedTimes	= max($updatedTimes);
		if($MaxUpdatedTimes){
			$saveTime		= ($MaxUpdatedTimes - (60*10));
		}
	}
	if($checkInsertFlag){
		if(!$FetchProductID){
			$this->ci->db->insert('cron_management', array('type' => 'bpproduct'.$account1Id, 'saveTime' => $saveTime)); 
		}
	}
	if($webTaskID){
		if($startTime){
			$endTime			= strtotime('now');
			$processedSecond	= ($endTime - $startTime); 
			$this->ci->db->update('temp',array('executionSeconds' => $processedSecond), array('id' => $webTaskID));
		}
	}
}
?>