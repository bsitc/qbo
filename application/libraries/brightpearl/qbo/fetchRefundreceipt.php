<?php
$this->reInitialize();
$returns			= array();
$UnInvoicingEnabled	= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	$AllBPchannelsData	= $this->getAllChannelMethod();
	$channelMaped	= array();
	if($AllBPchannelsData){
		foreach($AllBPchannelsData as $acc1 => $AllBPchannelsDatas){
			foreach($AllBPchannelsDatas as $cID	=> $AllBPchannelsDatass){
				$channelMaped[$cID]	= $AllBPchannelsDatass;
			}
		}
	}
	$return				= array();
	$orderIds			= array();
	$saveCronTime		= array();
	$this->config		= $this->accountConfig[$account1Id];
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'rufundreceipt'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-30 days');
	}
	$saveCronTime			= array();
	$saveCronTime[]			= $cronTime;
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);

	$journalCronTime	= strtotime('-90 days');
	$datetime			= new DateTime(date('c',$journalCronTime));
	$journalCronTime	= $datetime->format(DateTime::ATOM);
	$journalCronTime	= str_replace("+","%2B",$journalCronTime);
	$journalOrderIds	= array();
	if($UnInvoicingEnabled){
		$invoice_refs	= $this->ci->db->select('TempAcc2ID,account2Id,orderId,invoiceRef,generalIds,uninvoiced')->get_where('refund_receipt')->result_array(); 
		$invoicedatas	= array();
		foreach($invoice_refs as $invoice_ref){
			$invoicedatas[$invoice_ref['orderId']]	= $invoice_ref;
		}
		$url				= '/accounting-service/journal-search?journalType=SC&nominalCode=9999&journalDate='.$journalCronTime.'/';
		$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id]; 
		$journalOrderIds	= array();
		if((is_array($response)) AND (isset($response['results']))){
			$header	= array_column($response['metaData']['columns'],'name');
			foreach($response['results'] as $result){
				$journalFetchRow	= array_combine($header,$result);
				$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
			}
			if((isset($response['metaData']['resultsAvailable'])) AND ($response['metaData']['resultsAvailable'] > 500)){
				for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if((is_array($response1)) AND (isset($response1['results']))){
						foreach($response1['results'] as $result){
							$journalFetchRow	= array_combine($header,$result);
							$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
						}
					}
				}
			}
		}
	}
	$account2Ids	= $this->account2Details[$account1Id]; 
	$goLiveDate		= $this->config['goLiveDate'];
	$saveTaxDate	= $this->config['taxDate'];
	
	$fetchStatusIdsExclude	= explode(",",$this->config['fetchRefundReceiptStatus']);
	$url			= '/order-service/order-search?orderTypeId=3&updatedOn='.$cronTime.'/';
	$response		= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  
	if((is_array($response)) AND (isset($response['results']))){
		$header		= array_column($response['metaData']['columns'],'name');
		foreach($response['results'] as $result){
			$row			= array_combine($header,$result);
			$created		= $row['createdOn'];
			$taxDate		= $row['taxDate'];
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$QBOoffset		= 0;
			$diff			= 0;
			$diff			= $BPDateOffset - $QBOoffset;
			$date1			= new DateTime($taxDate);
			$date2			= new DateTime($created);
			$BPTimeZone		= 'GMT';
			$date1->setTimezone(new DateTimeZone($BPTimeZone));
			$date2->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff	.= ' hour';
				$date1->modify($diff);
				$date2->modify($diff);
			}
			$taxDate		= $date1->format('Ymd');
			$created		= $date2->format('Ymd');
			if($saveTaxDate){
				if($taxDate < $saveTaxDate){
					continue;
				} 
			}			
			if($goLiveDate){
				if($created < $goLiveDate){
					continue;
				}
			}
			$orderIds[$result['0']]	= $result['0'];
		}
		if((isset($response['metaData']['resultsAvailable'])) AND ($response['metaData']['resultsAvailable'] > 500)){
			for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response1)) AND (isset($response1['results']))){
					foreach($response1['results'] as $result){
						$row			= array_combine($header,$result);
						$created		= $row['createdOn'];
						$taxDate		= $row['taxDate'];
						$BPDateOffset	= (int)substr($taxDate,23,3);
						$QBOoffset		= 0;
						$diff			= 0;
						$diff			= $BPDateOffset - $QBOoffset;
						$date1			= new DateTime($taxDate);
						$date2			= new DateTime($created);
						$BPTimeZone		= 'GMT';
						$date1->setTimezone(new DateTimeZone($BPTimeZone));
						$date2->setTimezone(new DateTimeZone($BPTimeZone));
						if($diff){
							$diff	.= ' hour';
							$date1->modify($diff);
							$date2->modify($diff);
						}
						$taxDate		= $date1->format('Ymd');
						$created		= $date2->format('Ymd');
						if($saveTaxDate){
							if($taxDate < $saveTaxDate){
								continue;
							} 
						}			
						if($goLiveDate){
							if($created < $goLiveDate){
								continue;
							}
						}
						$orderIds[$result['0']]	= $result['0'];
					}
				}
			}
		}
	}
	
	$orderDatas	= array();
	$jOrderIds	= array();
	$jOrderIds	= array_keys($journalOrderIds);
	if(!empty($orderIds)){
		$orderIds	= array_merge($orderIds,$jOrderIds);
		$orderIds	= array_filter($orderIds);		
		sort($orderIds);
		$orderDatas	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
		if(!empty($orderDatas)){
			foreach($orderDatas as $OrderInfoList){
				foreach($account2Ids as $account2Id){
					if(strtolower($OrderInfoList['orderPaymentStatus']) != 'paid'){
						continue;
					}
					if(strtolower($OrderInfoList['invoices']['0']['invoiceReference']) == 'pending'){
						continue;
					}
					$orderId			= $OrderInfoList['id'];
					$channelName		= '';
					$channelId			= @$OrderInfoList['assignment']['current']['channelId'];
					$statusId			= $OrderInfoList['orderStatus']['orderStatusId'];
					$warehouseId		= $OrderInfoList['warehouseId'];
					$invoiceReference	= $OrderInfoList['invoices']['0']['invoiceReference'];
					$tempSaveAcc1		= $account1Id;
					$tempSaveAcc2		= '';
					$config2			= $this->account2Config[$account2Id['id']];
					$uninvoiced  		= 0;
					$invoiced			= 0;
					if($invoiceReference){
						$invoiced	= 1;
					}
					if($channelId){
						if($channelMaped[$channelId]){
							$channelName	= $channelMaped[$channelId]['name'];
						}
					}
					$created		= $OrderInfoList['createdOn'];
					$taxDate		= $OrderInfoList['invoices']['0']['taxDate'];
					$BPDateOffset	= (int)substr($taxDate,23,3);
					$QBOoffset		= 0;
					$diff			= 0;
					$diff			= $BPDateOffset - $QBOoffset;
					$date1			= new DateTime($taxDate);
					$date2			= new DateTime($created);
					$BPTimeZone		= 'GMT';
					$date1->setTimezone(new DateTimeZone($BPTimeZone));
					$date2->setTimezone(new DateTimeZone($BPTimeZone));
					if($diff){
						$diff	.= ' hour';
						$date1->modify($diff);
						$date2->modify($diff);
					}
					$taxDate		= $date1->format('Ymd');
					$created		= $date2->format('Ymd');
					if($taxDate){
						if($taxDate < $saveTaxDate){
							continue;
						} 
					}			
					if($goLiveDate){
						if($created < $goLiveDate){
							continue;
						}
					}
					$totalJournalIds	= array();
					if($UnInvoicingEnabled){
						$journalOrderId	= $journalOrderIds[$orderId];
						$saveJournalIds = $invoicedatas[$orderId];
						if($saveJournalIds){
							$saveJournalIds	= json_decode($saveJournalIds['generalIds'],true);
							foreach($saveJournalIds as $saveJournalId){
								$totalJournalIds[$saveJournalId]	= $saveJournalId;
								unset($journalOrderId[$saveJournalId]);
							}
						}
						if($journalOrderId){
							foreach($journalOrderId as $jid){
								$totalJournalIds[$jid]	= $jid;
							}						
							$uninvoiced	= 1;
						}
					}
					if((!$uninvoiced) AND (!$invoiceReference)){
						continue;
					}
					if(!$uninvoiced){
						if(($fetchStatusIdsExclude) AND (in_array($statusId,$fetchStatusIdsExclude))){
							continue;
						}
					}
					if($uninvoiced){
						if(!isset($invoicedatas[$orderId])){
							$invoiced	= 1;
						}
					}
					if($invoicedatas[$orderId]['uninvoiced'] == 1){
						$uninvoiced	= 1;
					}
					$config2Warehouses	= array();
					$config2Channels	= array();
					if($config2['warehouses_refund_sales_receipt']){
						$config2Warehouses = explode(",",$config2['warehouses_refund_sales_receipt']);
					}
					if($config2['channelIds_refund_sales_receipt']){
						$config2Channels	= explode(",",$config2['channelIds_refund_sales_receipt']);
					}
					if($config2Channels OR $config2Warehouses){
						if((!$channelId) AND (!$warehouseId)){
							continue;
						}
						if($config2Channels){
							if(!in_array($channelId,$config2Channels)){
								continue;
							}
						}	
						if($config2Warehouses){
							if(!in_array($warehouseId,$config2Warehouses)){
								continue;
							}
						}
						$tempSaveAcc2	= $account2Id['id'];
					}
					else{
						$tempSaveAcc2	= $account2Id['id'];
					}
					if(!$tempSaveAcc2){
						continue;
					}
					$TempAcc2ID	= 0;
					if($uninvoiced){
						if($invoicedatas[$orderId]['TempAcc2ID']){
							$TempAcc2ID	= $invoicedatas[$orderId]['TempAcc2ID'];
						}
						else{
							if($invoicedatas[$orderId]['account2Id']){
								$TempAcc2ID	= $invoicedatas[$orderId]['account2Id'];
							}
						}
					}
					$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;			
					$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;
					
					
					if($clientcode == 'solecityshoesqbo'){
						$allPaymentmethod	= array();
						$url				= '/accounting-service/customer-payment-search?orderId='.$orderId;
						$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
						if((is_array($response)) AND (isset($response['results'])) AND (isset($response['results']))){
							foreach($response['results'] as $paymentResults){
								$allPaymentmethod[$paymentResults[3]]	= $paymentResults[3];
							}
						}
						if((!empty($allPaymentmethod)) AND (count($allPaymentmethod) > 1)){
							continue;
						}
					}
					
					
					$return[$account1Id][$orderId]['orders']		= array(
						'account1Id'    	=> $saveAccId1,
						'account2Id'    	=> $saveAccId2,
						'orderId'       	=> $orderId,
						'customerEmail' 	=> $OrderInfoList['parties']['customer']['email'],
						'customerId'    	=> $OrderInfoList['parties']['customer']['contactId'],
						'reference'    		=> $OrderInfoList['reference'], 
						'parentOrderId' 	=> $OrderInfoList['parentOrderId'], 
						'totalAmount'   	=> $OrderInfoList['totalValue']['total'],
						'totalTax'      	=> $OrderInfoList['totalValue']['taxAmount'],
						'created'       	=> date('Y-m-d H:i:s', strtotime($OrderInfoList['createdOn'])),
						'rowData'       	=> json_encode($OrderInfoList),
						'uninvoiced'		=> $uninvoiced,
						'invoiced'			=> $invoiced,
						'generalIds'		=> json_encode($totalJournalIds),
						'InvoicedTime'		=> date('Y-m-d H:i:s', strtotime('now')),
						'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
						'bpInvoiceNumber'	=> $invoiceReference,
						'channelId'			=> $channelId,
						'channelName'		=> $channelName,
						'TempAcc2ID'		=> $TempAcc2ID,
					);
					if($invoicedatas[$orderId]){
						unset($return[$account1Id][$orderId]['orders']['InvoicedTime']);
					}
					$saveCronTime[]	= strtotime($OrderInfoList['updatedOn']);
				}
				
				//new logStoringFunctionality
				$logsData		= array();
				$allStoredLogs	= array();
				$fileLogPath	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'refundReceipt'. DIRECTORY_SEPARATOR;
				if(!is_dir($fileLogPath)){
					mkdir($fileLogPath,0777,true);
					chmod(dirname($fileLogPath), 0777);
				}
				$fileLogPath	= $fileLogPath.$OrderInfoList['id'].'.logs';
				if(file_exists($fileLogPath)){
					$latestStoredLogs	= array();
					$allStoredLogs		= json_decode(file_get_contents($fileLogPath), true);
					$tempStoredLogs		= $allStoredLogs;
					if((is_array($tempStoredLogs)) AND (!empty($tempStoredLogs))){
						krsort($tempStoredLogs);
						foreach($tempStoredLogs as $logsKey => $tempLogsTemp){
							$latestStoredLogs	= $tempLogsTemp;
							break;
						}
					}
					if(!empty($latestStoredLogs)){
						$tempFetchedLogs	= $OrderInfoList;
						unset($tempFetchedLogs['createdOn']);
						unset($tempFetchedLogs['updatedOn']);
						
						unset($latestStoredLogs['createdOn']);
						unset($latestStoredLogs['updatedOn']);
						
						if(md5(json_encode($latestStoredLogs)) != md5(json_encode($tempFetchedLogs))){
							$allStoredLogs[date('c', strtotime("now"))]	= $OrderInfoList;
							file_put_contents($fileLogPath,json_encode($allStoredLogs));
						}
					}
					else{
						file_put_contents($fileLogPath,json_encode($allStoredLogs));
					}
				}
				else{
					$logsData	= array(date('c', strtotime("now"))	=> $OrderInfoList);
					file_put_contents($fileLogPath,json_encode($logsData));
				}
			}
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime) );
}
?>