<?php
$this->reInitialize();
foreach($this->accountDetails as $account1Id => $accountDetails){
	
	$paymentResponses	= array();
	$cronTime	= strtotime('-90 days');
	$cronTime	= date('Y-m-d H:i:s', $cronTime);
	
	$url		= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	$response	= $this->getPayments($cronTime, $account1Id);
	if((is_array($response)) AND (isset($response['results']))){
		$paymentResponses[]	= $response;
		if($response['metaData']['resultsAvailable'] > 500){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getPayments($cronTime, $account1Id, $i, $response['metaData']['resultsAvailable']);
				if((is_array($response1)) AND (isset($response1['results']))){
					$paymentResponses[]	= $response1;
				}
			}
		}
	}
	
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$objectId	= array_filter($objectId);
		$objectId	= array_unique($objectId);
		sort($objectId);
		$objectIds	= array_chunk($objectId,50);
		
		if(!empty($objectIds)){
			foreach($objectIds as $objectId){
				$url		= '/accounting-service/customer-payment-search?orderId='.implode(",",$objectId).'&sort=createdOn.DESC';
				$response1	= $this->getPayments('', $account1Id, 0, 0, $objectId);
				if((is_array($response1)) AND (isset($response1['results']))){
					$paymentResponses[]	= $response1;
				}
			}
		}
	}
	if(!empty($paymentResponses)){
		$allPayKeys			= array();
		$batchUpdates		= array();
		$pendingPayments	= array();
		
		$this->ci->db->reset_query();
		$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('refund_receipt',array('status' => 0,'account1Id' => $account1Id))->result_array();
		if(!empty($pendingPaymentsTemps)){
			foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
				$pendingPayments[$pendingPaymentsTemp['orderId']]	= $pendingPaymentsTemp;
			}
			foreach($pendingPaymentsTemps as $mappedpayment){
				if($mappedpayment['paymentDetails']){
					$allPayments	= json_decode($mappedpayment['paymentDetails'],true);
					foreach($allPayments as $PKEY => $mappedpaymentdatas){
						if($mappedpaymentdatas['amount'] != 0){
							if($mappedpaymentdatas['sendPaymentTo'] == 'qbo'){
								$allPayKeys[]	= $PKEY;
							}
						}
					}
				}
			}
			sort($allPayKeys);
		}
		
		foreach($paymentResponses as $paymentDatas){
			if(isset($paymentDatas['results'])){
				foreach($paymentDatas['results'] as $result){
					$orderId		= $result['orderId'];
					$journalId		= $result['journalId'];
					$paymentId		= $result['paymentId'];
					$amount			= $result['amountPaid'];
					$paymentMethod	= $result['paymentMethodCode'];
					$transactionRef	= $result['transactionRef'];
					$currencyCode	= $result['currencyCode'];
					$paymentDate	= $result['paymentDate'];
					$paymentType	= $result['paymentType'];
					
					if(!isset($pendingPayments[$orderId])){
						continue;
					}
					if($allPayKeys){
						if(in_array($paymentId,$allPayKeys)){
							continue;
						}
					}
					if(strtolower($paymentType) == 'receipt'){
						$amount	= '-'.$amount;
					}
					if(!isset($batchUpdates[$orderId]['paymentDetails'])){
						$paymentDetails	= @json_decode($pendingPayments[$orderId]['paymentDetails'],true);
					}
					else{
						$paymentDetails	= $batchUpdates[$orderId]['paymentDetails'];
					}
					if(($paymentDetails[$journalId]['amount'] > 0) OR ($paymentDetails[$journalId]['amount'] < 0)){
						continue;
					}
					if($paymentDetails[$paymentId]){
						if($paymentDetails[$paymentId]['sendPaymentTo'] == 'qbo'){
							continue;
						}
					}
					$paymentDetails[$paymentId]		= array(
						'amount'						=> $amount,
						'sendPaymentTo'					=> $this->ci->globalConfig['account2Liberary'],
						'status'						=> '0',
						'Reference'						=> $transactionRef,
						'journalId'						=> $journalId,
						'currency'						=> $currencyCode,
						'CurrencyRate'					=> '',
						'paymentMethod'					=> $paymentMethod,
						'paymentType'					=> $paymentType,
						'paymentDate'					=> $paymentDate,
						'paymentInitiatedIn'			=> 'bp',
						'Desc'							=> '',
					);
					if(strtolower($paymentDetails[$paymentId]['paymentType']) == 'auth'){
						$paymentDetails[$paymentId]['status']	= 1;
					}
					if($journalId){
						if($result['journalId'] != $result['paymentId']){
							$paymentDetails[$journalId]	= array(
								'amount'					=> 0.00,
								'sendPaymentTo'				=> $this->ci->globalConfig['account2Liberary'],
								'status'					=> '1',
								'Reference'					=> $transactionRef,
								'journalId'					=> $journalId,
								'currency'					=> $currencyCode,
								'CurrencyRate'				=> '',
								'paymentMethod'				=> $paymentMethod,
							);
						}
					}
					$batchUpdates[$orderId]			= array(
						'paymentDetails'				=> $paymentDetails,
						'orderId'						=> $orderId,
						'sendPaymentTo'					=> $this->ci->globalConfig['account2Liberary'],
					);
					if(strtolower($batchUpdates[$orderId]['paymentDetails'][$paymentId]['paymentType']) == 'receipt'){
						$batchUpdates[$orderId]['isPaymentCreated']	= 0;
					}
					$updatedTimes[]					= strtotime($result['createdOn']);
				}
			}
		}
		if($batchUpdates){
			foreach($batchUpdates as $key => $batchUpdate){
				$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
			} 
			$batchUpdates	= array_chunk($batchUpdates,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate){
					$this->ci->db->update_batch('refund_receipt',$batchUpdate,'orderId');
				}
			}
		}
	}
}
?>