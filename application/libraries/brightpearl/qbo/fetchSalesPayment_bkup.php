<?php
//qbo fetchsales_payment from brightpearl
$this->reInitialize();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$disableSOpaymentbptoqbo	= $this->ci->globalConfig['disableSOpaymentbptoqbo'];
foreach($this->accountDetails as $account1Id => $accountDetails){
	if($disableSOpaymentbptoqbo){
		continue;
	}
	
	$updatedTimes	= array();
	$this->config	= $this->accountConfig[$account1Id];
	
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'salespayment'.$account1Id))->row_array();
	$cronTime		= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-90 days');
	}
	$cronTime		= strtotime('-90 days');
	
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	
	$paymentResponses	= array();
	$url				= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if($response['results']){
		$paymentResponses[]	= $response;
		if($response['metaData']['resultsAvailable'] > 500){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if($response1['results']){
					$paymentResponses[]	= $response1;
				}
			}
		}
	}
	
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$objectId	= array_filter($objectId);
		$objectId	= array_unique($objectId);
		sort($objectId);
		$objectIds	= array_chunk($objectId,50);
		if($objectIds){
			foreach($objectIds as $objectId){
				$url		= '/accounting-service/customer-payment-search?orderId='.implode(",",$objectId).'&sort=createdOn.DESC';
				$response1	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if($response1['results']){
					$paymentResponses[]	= $response1;
				}
			}
		}
	}
	
	if($paymentResponses){
		$allPayKeys			= array();
		$batchUpdates		= array();
		$pendingPayments	= array();
		
		$this->ci->db->reset_query();
		$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,sendInAggregation')->get_where('sales_order',array('isNetOff' => 0, 'orderId <>' => '','account1Id' => $account1Id))->result_array();
		if($pendingPaymentsTemps){
			foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
				$pendingPayments[$pendingPaymentsTemp['orderId']]	= $pendingPaymentsTemp;
			}
			foreach($pendingPaymentsTemps as $mappedpayment){
				if($mappedpayment['paymentDetails']){
					$allPayments	= json_decode($mappedpayment['paymentDetails'],true);
					foreach($allPayments as $PKEY => $mappedpaymentdatas){
						if($mappedpaymentdatas['amount'] != 0){
							if($mappedpaymentdatas['sendPaymentTo'] == 'qbo'){
								$allPayKeys[]	= $PKEY;
							}
						}
					}
				}
			}
			sort($allPayKeys);
		}
		
		foreach($paymentResponses as $paymentDatas){
			if($paymentDatas['results']){
				$headerKeys	= array_column($paymentDatas['metaData']['columns'],'name');
				foreach($paymentDatas['results'] as $result){
					$result			= array_combine($headerKeys,$result);
					$orderId		= $result['orderId'];
					$journalId		= $result['journalId'];
					$paymentId		= $result['paymentId'];
					$amount			= $result['amountPaid'];
					$paymentMethod	= $result['paymentMethodCode'];
					$transactionRef	= $result['transactionRef'];
					$currencyCode	= $result['currencyCode'];
					$paymentDate	= $result['paymentDate'];
					$paymentType	= $result['paymentType'];
					
					if($pendingPayments[$orderId]['sendInAggregation'] AND $pendingPayments[$orderId]['isPaymentCreated']){
						continue;
					}
					
					if(!isset($pendingPayments[$orderId])){
						continue;
					}
					if($allPayKeys){
						if(in_array($paymentId,$allPayKeys)){
							continue;
						}
					}
					if(strtolower($paymentType) == 'payment'){
						$amount	= '-'.$amount;
					}
					if(!isset($batchUpdates[$orderId]['paymentDetails'])){
						$paymentDetails	= @json_decode($pendingPayments[$orderId]['paymentDetails'],true);
					}
					else{
						$paymentDetails	= $batchUpdates[$orderId]['paymentDetails'];
					}
					if(($paymentDetails[$journalId]['amount'] > 0) OR ($paymentDetails[$journalId]['amount'] < 0)){
						continue;
					}
					if($paymentDetails[$paymentId]){
						if($paymentDetails[$paymentId]['sendPaymentTo'] == 'qbo'){
							continue;
						}
						if(isset($paymentDetails[$paymentId]['AmountCreditedIn'])){
							continue;
						}
						if(isset($paymentDetails[$paymentId]['AmountReversedIn'])){
							continue;
						}
					}
					$paymentDetails[$paymentId]		= array(
						'amount'						=> $amount,
						'sendPaymentTo'					=> $this->ci->globalConfig['account2Liberary'],
						'status'						=> '0',
						'Reference'						=> $transactionRef,
						'journalId'						=> $journalId,
						'currency'						=> $currencyCode,
						'CurrencyRate'					=> '',
						'paymentMethod'					=> $paymentMethod,
						'paymentType'					=> $paymentType,
						'paymentDate'					=> $paymentDate,
						'paymentInitiatedIn'			=> 'brightpearl',
						'Desc'							=> '',
					);
					if(strtolower($paymentDetails[$paymentId]['paymentType']) == 'auth'){
						$paymentDetails[$paymentId]['status']	= 1;
					}
					if($journalId){
						if($result['journalId'] != $result['paymentId']){
							$paymentDetails[$journalId]	= array(
								'amount'					=> 0.00,
								'sendPaymentTo'				=> $this->ci->globalConfig['account2Liberary'],
								'status'					=> '1',
								'Reference'					=> $transactionRef,
								'journalId'					=> $journalId,
								'currency'					=> $currencyCode,
								'CurrencyRate'				=> '',
								'paymentMethod'				=> $paymentMethod,
							);
						}
					}
					$batchUpdates[$orderId]			= array(
						'paymentDetails'				=> $paymentDetails,
						'orderId'						=> $orderId,
						'sendPaymentTo'					=> $this->ci->globalConfig['account2Liberary'],
					);
					if(strtolower($batchUpdates[$orderId]['paymentDetails'][$paymentId]['paymentType']) == 'payment'){
						$batchUpdates[$orderId]['isPaymentCreated']	= 0;
					}
					$updatedTimes[]					= strtotime($result['createdOn']);
				}
			}
		}
		if($batchUpdates){
			if($updatedTimes){
				$saveTime	= max($updatedTimes) - (60*60);
				$this->ci->db->insert('cron_management', array('type' => 'salespayment'.$account1Id, 'runTime' => date('c'), 'saveTime' => $saveTime));
			}
			foreach($batchUpdates as $key => $batchUpdate){
				$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
			} 
			$batchUpdates	= array_chunk($batchUpdates,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate){
					$this->ci->db->update_batch('sales_order',$batchUpdate,'orderId');
				}
			}
		}
	}
}
?>