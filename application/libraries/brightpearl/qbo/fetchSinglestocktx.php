<?php
//QBOde
$this->reInitialize();
$enableWarehouseclassMapping	= $this->ci->globalConfig['enableWarehouseclassMapping'];
$enableSingleCompanyStocktx		= $this->ci->globalConfig['enableSingleCompanyStocktx'];
$returns						= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$enableSingleCompanyStocktx){
		continue;
	}
	
	$return			= array();
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'singlestocktx'.$account1Id))->row_array();
	$cronTime		= $datas['saveTime'];	
	$saveCronTime	= array();
	if(!$cronTime){
		$cronTime	= strtotime('-30 days');
	}
	$saveCronTime			= array();
	$saveCronTime[]			= $cronTime;
	$cronTime		= strtotime('-60 days');
	
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	
	
	$this->config	= $this->accountConfig[$account1Id];
	$goLiveDate		= $this->config['taxDate'];	
	$account2Ids	= $this->account2Details[$account1Id];
	
	$responses		= array();
	$headers		= array();
	$url			= '/warehouse-service/goods-movement-search?sort=goodsNoteId.DESC&goodsNoteTypeCode=GO&updatedOn='.$cronTime.'/';
	$response		= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	
	if($response['results']){
		$responses[]	= $response; 
	}
	if($response['metaData']){
		$headers	= array_column($response['metaData']['columns'],'name');
	}
	if($response['metaData']){
		for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
			$url1		= $url . '&firstResult=' . $i;
			$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
			if($response1['results']){
				$responses[]	= $response1; 
			}
		}
	}
	
	$AllStockTxGoodsNoteId	= array();
	$unique_result			= array();
	if($responses){
		foreach($responses as $response){
			foreach($response['results'] as $txdataTemps){
				$rowResults	= array_combine($headers,$txdataTemps);
				if($rowResults['salesOrderRowId'] OR $rowResults['purchaseOrderRowId']){
					if($rowResults['orderId']){
						continue;
					}
				}
				if($rowResults['goodsNoteTypeCode'] == 'GO'){
					$AllStockTxGoodsNoteId[]	= $rowResults['goodsNoteId'];
				}
				else{
					continue;
				}
			}
		}
	}
	
	$AllStockTxGoodsNoteId	= array_filter($AllStockTxGoodsNoteId);
	$AllStockTxGoodsNoteId	= array_unique($AllStockTxGoodsNoteId);
	sort($AllStockTxGoodsNoteId);
	
	$AllStockTxData	= array();
	$AllUniqueRes	= array();
	$AllUniqueRes1	= array();
	if($AllStockTxGoodsNoteId){
		$AllStockTxData	= $this->getResultById($AllStockTxGoodsNoteId,'/warehouse-service/order/*/goods-note/goods-out/',$account1Id,200,1);
		$goodsIdChunks	= array_chunk($AllStockTxGoodsNoteId, 25, true);
		foreach($goodsIdChunks as $goodsIdChunksTemp){
			$AllUniqueRes1	= $this->getResultById($goodsIdChunksTemp,'/warehouse-service/goods-movement-search?goodsNoteId=',$account1Id,25,1);
			if($AllUniqueRes1['results']){
				foreach($AllUniqueRes1['results'] as $AllUniqueRes1Temp){
					$AllUniqueRes[]	= $AllUniqueRes1Temp;
				}
			}
		}
	}
	else{
		continue;
	}
	
	if($AllUniqueRes AND $AllStockTxData){
		foreach($AllUniqueRes as $txdataTemp){
			$rowResults	= array_combine($headers,$txdataTemp);
			if($rowResults['salesOrderRowId'] OR $rowResults['purchaseOrderRowId']){
				if($rowResults['orderId']){
					continue;
				}
			}
			if($rowResults['goodsNoteTypeCode'] != 'GO'){
				continue;
			}
			$mainStockTxData	= $AllStockTxData[$rowResults['goodsNoteId']];
			if(!$mainStockTxData){
				continue;
			}
			if(!$mainStockTxData['status']['shipped']){
				continue;
			}
			if($mainStockTxData['orderId']){
				continue;
			}
			
			$createdOn			= $mainStockTxData['createdOn'];
			$ShippedDate		= $mainStockTxData['status']['shippedOn'];
			$ShippedDate		= date('Ymd',strtotime($ShippedDate));
			if($goLiveDate){
				if($ShippedDate < $goLiveDate){
					continue; 
				}
			}
			
			$sourceWarehouseId	= $mainStockTxData['warehouseId'];
			$targetWarehouseId	= $mainStockTxData['targetWarehouseId'];
			$stockTransferId	= $mainStockTxData['stockTransferId'];
			
			foreach($account2Ids as $account2Id){
				$tempSaveAcc1	= $account1Id; 
				$tempSaveAcc2	= '';
				$config2		= $this->account2Config[$account2Id['id']];
				$warehouseForStockTx		= array();
				if(!$config2['warehouseForStockTx']){
					continue;
				}
				else{
					$warehouseForStockTx	= explode(",",$config2['warehouseForStockTx']);
				}
				if(!in_array($sourceWarehouseId,$warehouseForStockTx)){
					continue;
				}
				if(!in_array($targetWarehouseId,$warehouseForStockTx)){
					continue;
				}
				$tempSaveAcc2	= $account2Id['id'];
				if(!$tempSaveAcc2 OR !$tempSaveAcc1){
					continue;
				}
				$return[$account1Id][$rowResults['goodsMovementId']]	= array(
					'account1Id'			=> $tempSaveAcc1,
					'account2Id'    		=> $tempSaveAcc2,
					'goodsMovementId'		=> $rowResults['goodsMovementId'],
					'goodsNoteId' 			=> $rowResults['goodsNoteId'],
					'stockTransferId'		=> $stockTransferId,
					'sourceWarehouseId'		=> $sourceWarehouseId,
					'targetWarehouseId'		=> $targetWarehouseId,
					'destinationLocationId'	=> $rowResults['destinationLocationId'],
					'productId' 			=> $rowResults['productId'],
					'sku'					=> '',
					'qty'					=> $rowResults['quantity'],
					'price'					=> $rowResults['productValue'],
					'totalValue'			=> ($rowResults['productValue'] * $rowResults['quantity']),
					'currencyCode'			=> strtolower($rowResults['currencyCode']),
					'shippedOn'				=> date('c',strtotime($ShippedDate)),
					'created'				=> date('c',strtotime($createdOn)),
					'rowDataTx'				=> json_encode($mainStockTxData),
					'rowData'				=> json_encode($rowResults),
				);
				$saveCronTime[]		= strtotime($rowResults['updatedOn']);
			}
		}
	}
	$returns[$account1Id]	= array('return' => $return,'saveTime' => @max($saveCronTime)); 
}
?>