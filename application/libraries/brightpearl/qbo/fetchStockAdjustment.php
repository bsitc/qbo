<?php
//QBO
$this->reInitialize();
$enableInventoryTransfer	= $this->ci->globalConfig['enableInventoryTransfer'];
$returns					= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$WareHousedatas			= $this->ci->db->get_where('mapping_warehouse', array('account1Id' => $account1Id))->result_array();
	$account2WareHouses		= array();
	if($WareHousedatas){
		foreach($WareHousedatas as $WareHousedata){
			$account2WareHouses[$WareHousedata['account2Id']][]	= $WareHousedata['account1WarehouseId'];
		}
	}
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'stockadjustment'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];	
	$return				= array();
	$saveCronTime		= array();
	if(!$cronTime){
		$cronTime	= strtotime('-30 days');
	}
	$saveCronTime			= array();
	$saveCronTime[]			= $cronTime;
	$responses				= array();
	$stockAdjusmentDatas	= array();
	$oldStockTransferData	= array();
	$datetime				= new DateTime(date('c',$cronTime));
	$cronTime				= $datetime->format(DateTime::ATOM);
	$cronTime				= str_replace("+","%2B",$cronTime);
	$this->config			= $this->accountConfig[$account1Id];			
	$account2Ids			= $this->account2Details[$account1Id];
	$excludewarehouses		= $this->config['excludewarehouseStockadjustment'];
	if($excludewarehouses){
		$excludewarehouses	= explode(",",$excludewarehouses);
	}
	$url		= '/warehouse-service/goods-movement-search?sort=goodsNoteId.DESC&goodsNoteTypeCode=SC&updatedOn='.$cronTime.'/';
	$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	$headers			= array();
	
	if((is_array($response)) AND (isset($response['metaData']))){
		$headers	= array_column($response['metaData']['columns'],'name');
	}
	
	if((is_array($response)) AND (isset($response['results']))){
		$responses[]	= $response; 
		if(isset($response['metaData'])){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response1)) AND (isset($response1['results']))){
					$responses[]	= $response1; 
				}
			}
		}
	}
	
	if($enableInventoryTransfer){
		$url		= '/warehouse-service/goods-movement-search?sort=goodsNoteId.DESC&goodsNoteTypeCode=GO&updatedOn='.$cronTime.'/';
		$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id]; 
		
		if((is_array($response)) AND (isset($response['metaData']))){
			$headers	= array_column($response['metaData']['columns'],'name');
		}
		
		if((is_array($response)) AND (isset($response['results']))){
			$responses[]	= $response; 
			if(isset($response['metaData'])){
				for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if((is_array($response1)) AND (isset($response1['results']))){
						$responses[]	= $response1; 
					}
				}
			}
		}
	}
	
	if(!empty($responses)){
		foreach($responses as $response){
			foreach($response['results'] as $mydata){
				$row	= array_combine($headers,$mydata);
				if($row['goodsNoteTypeCode'] == 'GO'){
					$goodoutIDS[]	= $row['goodsNoteId'];
				}
			}
		}
	}
	
	if(!empty($goodoutIDS)){
		$goodoutIDS	= array_filter($goodoutIDS);
		$goodoutIDS	= array_unique($goodoutIDS);
		sort($goodoutIDS);
	}
	
	if(!empty($goodoutIDS)){
		$stockAdjusmentDatas	= $this->getResultById($goodoutIDS,'/warehouse-service/order/*/goods-note/goods-out/',$account1Id,200,1);
		$oldStockTransferData	= $this->getResultById($goodoutIDS,'/warehouse-service/goods-movement-search?goodsNoteId=',$account1Id,200,1);
	}
	
	if(!empty($oldStockTransferData)){
		$responses[]	= $oldStockTransferData;
	}
	
	$unique_result	= array();
	$readyData		= array();
	
	if(!empty($responses)){
		foreach($responses as $response){
			$all_goodnote[]	= $response['results'];
		}
		foreach($all_goodnote as $all_goodnotetemp){
			foreach($all_goodnotetemp as $unique_results){
				if($unique_results[14] != 'GO'){
					if(empty($unique_result[$unique_results[0]][$unique_results[1]])){
						$unique_result[$unique_results[0]][$unique_results[1]]	= $unique_results;
					}
				}
				else{
					if($unique_results[4] < 0){
						if(empty($unique_result[$unique_results[0]][$unique_results[1]])){
							$unique_result[$unique_results[0]][$unique_results[1]]	= $unique_results;
						}
					}
				}
			}
		}
	}
	
	if(!empty($unique_result)){
		foreach($unique_result as $TransferDatas){
			foreach($TransferDatas as $TransferData){
				$readyData[]	= $TransferData;	
			}
		}
	}
	
	$goLiveDate	= $this->config['goLiveDate'];
	if(!empty($readyData)){
		foreach($readyData as $results){
			$row		= array_combine($headers,$results);
			$updated	= date('Ymd',strtotime($row['updatedOn']));
			
			if($row['goodsNoteTypeCode'] == 'GO'){
				$NewTransferData	= $stockAdjusmentDatas[$row['goodsNoteId']];
				$ShippedDate		= $NewTransferData['status']['shippedOn'];
				$ShippedDate		= date('Ymd',strtotime($ShippedDate));
				if($goLiveDate){
					if($ShippedDate < $goLiveDate){
						continue; 
					}
				}
			}
			else{
				if($goLiveDate){
					if($updated < $goLiveDate){
						continue; 
					}
				}
			}
			
			if($row['salesOrderRowId'] OR $row['purchaseOrderRowId']){
				if($row['orderId']){
					continue;
				}
			}
			
			$warehouseId	= $row['warehouseId'];
			if(is_array($excludewarehouses) AND !empty($excludewarehouses)){
				if(in_array($warehouseId,$excludewarehouses)){
					continue;
				}
			}
			foreach($account2Ids as $account2Id){
				$tempSaveAcc1	= $account1Id; 
				$tempSaveAcc2	= '';
				$config2		= $this->account2Config[$account2Id['id']];
				if($row['goodsNoteTypeCode'] != 'GO' AND $row['goodsNoteTypeCode'] != 'SC'){
					continue;
				}
				$config2Warehouses	= $account2WareHouses[$account2Id['id']];
				if($config2Warehouses){
					if((!$warehouseId)){
						continue;
					}
					if(!in_array($warehouseId,$config2Warehouses)){
						continue;
					}
					$tempSaveAcc2	= $account2Id['id'];
				}
				else{
					continue;
					$tempSaveAcc2	= $account2Id['id'];
				}
				if(!$tempSaveAcc2){
					continue;
				}
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;			
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;			
				if($row['goodsNoteTypeCode'] == 'GO'){
					$goodsNoteId		= $row['goodsNoteId'];
					$stockAdjusmentData	= $stockAdjusmentDatas[$goodsNoteId];
					if(!$stockAdjusmentData){
						continue;
					}
					if(!$stockAdjusmentData['status']['shipped']){
						continue;
					}
					$shippingDate		= $stockAdjusmentData['status']['shippedOn'];
					$sourceWarehouse	= $stockAdjusmentData['warehouseId'];
					$targetWarehouse	= $stockAdjusmentData['targetWarehouseId'];
					$GOid				= $stockAdjusmentData['stockTransferId'];
					if(!$GOid){
						continue;
					}
					if($stockAdjusmentData['orderId']){
						continue;
					}
					if($sourceWarehouse == $targetWarehouse){
						continue;
					}
					foreach($account2WareHouses as $AccountIdNew => $account2WareHousesData){
						$saveWarehouses	= $account2WareHousesData;
						if((in_array($sourceWarehouse,$saveWarehouses)) AND (!in_array($targetWarehouse,$saveWarehouses))){
							$saveAccId2		= $AccountIdNew;
							$return[$account1Id][$row['goodsMovementId']][uniqid()]	= array(
								'account1Id'	=> $saveAccId1,
								'account2Id'    => $saveAccId2,
								'orderId' 		=> $row['goodsMovementId'],
								'warehouseId'	=> $sourceWarehouse,
								'productId' 	=> $row['productId'],
								'qty' 			=> $row['quantity'],
								'price' 		=> $row['productValue'],
								'created' 		=> $row['updatedOn'],
								'rowData' 		=> json_encode($row),
								'GoodNotetype'	=> 'GO',
								'ActivityId'	=> $GOid,
								'goodsNoteId'	=> $row['goodsNoteId'],
								'shippedDate'	=> $shippingDate,
							);
						}
						if((in_array($targetWarehouse,$saveWarehouses)) AND (!in_array($sourceWarehouse,$saveWarehouses))){
							$saveAccId2		= $AccountIdNew;
							$return[$account1Id][$row['goodsMovementId']][uniqid()]	= array(
								'account1Id'	=> $saveAccId1,
								'account2Id'    => $saveAccId2,
								'orderId' 		=> $row['goodsMovementId'],
								'warehouseId'	=> $targetWarehouse,
								'productId' 	=> $row['productId'],
								'qty' 			=> (-1) * ($row['quantity']),
								'price' 		=> $row['productValue'],
								'created' 		=> $row['updatedOn'],
								'rowData' 		=> json_encode($row),
								'GoodNotetype'	=> 'GO',
								'ActivityId'	=> $GOid,
								'goodsNoteId'	=> $row['goodsNoteId'],
								'shippedDate'	=> $shippingDate,
							);
						}
					}
				}
				else if($row['goodsNoteTypeCode'] == 'SC'){
					$return[$account1Id][$row['goodsMovementId']][$row['productId']]	= array(
						'account1Id'		=> $saveAccId1,
						'account2Id'        => $saveAccId2,
						'orderId' 		    => $row['goodsMovementId'],
						'warehouseId'	    => $row['warehouseId'],
						'productId' 	    => $row['productId'],
						'qty' 			    => $row['quantity'],
						'price' 		    => $row['productValue'],
						'created' 		    => $row['updatedOn'],
						'rowData' 		    => json_encode($row),
						'GoodNotetype'	    => 'SC',
						'ActivityId'	    => '',
						'goodsNoteId'		=> $row['goodsNoteId'],
						'shippedDate'		=> date('Y-m-d'),
					);
				}
				$saveCronTime[]	= strtotime($results['15']);
			}
		}
	}
	$returns[$account1Id]	= array('return' => $return,'saveTime' => @max($saveCronTime)); 
}
?>