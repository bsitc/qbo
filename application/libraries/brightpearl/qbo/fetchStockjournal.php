<?php
//QBo : BRIGHTPEARL INTEGRATION
$this->reInitialize();
$AllCurrencyCodes	= $this->getAllCurrency();
$returns			= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$return				= array();
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	$newAccount2Data	= array();
	foreach($account2Ids as $account2Id){
		if(!$newAccount2Data){
			$newAccount2Data	= $account2Id;
		}
		else{
			break;
		}
	}
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'stockjournals'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	
	if(!$cronTime){
		$cronTime		= strtotime('-60 days');
	}
	$saveCronTime			= array();
	$saveCronTime[]			= $cronTime;
	
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	$saveTaxDate		= $this->config['taxDate'];
	$AllCurrencyCode	= $AllCurrencyCodes[$account1Id];
	
	$url				= '/accounting-service/journal-search?journalType=IA&journalDateEntered='.$cronTime.'/';
	$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	$journalId = array();
	if((is_array($response)) AND (isset($response['results']))){
		$header	= array_column($response['metaData']['columns'],"name");
		foreach($response['results'] as $result){
			$row			= array_combine($header,$result);
			$journalId[]	= $row['journalId'];
		}
		if((isset($response['metaData']['resultsAvailable'])) AND ($response['metaData']['resultsAvailable'] > 500)){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response1)) AND (isset($response1['results']))){
					foreach($response1['results'] as $result){
						$row			= array_combine($header,$result);
						$journalId[]	= $row['journalId'];
					}
				}
			}
		}
	}
	if(is_string($journalId)){
		$journalId	= array($journalId);
	}
	$journalId	= array_filter($journalId);
	$journalId	= array_unique($journalId);
	if(!$journalId){
		continue;
	}
	
	$journalDatas	= array();
	$journalId		= array_filter($journalId);
	$journalDatas	= $this->getResultById($journalId,'/accounting-service/journal',$account1Id,200,'0','');
	
	if(!empty($journalDatas)){
		foreach($journalDatas as $journalData){
			if($journalData['journalTypeCode'] != 'IA'){
				continue;
			}
			if(date('Ymd',strtotime($journalData['taxDate'])) < $saveTaxDate){
				continue;
			}
			$saveAccId1			= $account1Id;
			$saveAccId2			= $newAccount2Data['id'];
			if((!$saveAccId1) OR (!$saveAccId2)){
				continue;
			}
			
			$credits					= $journalData['credits'];
			$debits						= $journalData['debits'];
			$currencyId					= $journalData['currencyId'];
			$credit_transactionAmount	= 0;
			$debit_transactionAmount	= 0;
			$credit_nominalCode			= '';
			$debit_nominalCode			= '';
			$credit_taxCode				= '';
			$debit_taxCode				= '';
			
			if(!$credits['0']){
				$credits	= array($credits);
			}
			if(!$debits['0']){
				$debits		= array($debits);
			}
			foreach($credits as $credit){
				$credit_transactionAmount	+= $credit['transactionAmount'];
				$credit_nominalCode			= $credit['nominalCode'];
				$credit_taxCode				= $credit['taxCode'];
			}
			foreach($debits as $debit){
				$debit_transactionAmount	+= $debit['transactionAmount'];
				$debit_nominalCode			= $debit['nominalCode'];
				$debit_taxCode				= $debit['taxCode'];
			}
			
			$return[$saveAccId1][$journalData['id']]	= array(
				'account1Id'				=> $saveAccId1,
				'account2Id'				=> $saveAccId2,
				'journalId'					=> $journalData['id'],
				'journalTypeCode'			=> $journalData['journalTypeCode'],
				'currencyId'				=> $journalData['currencyId'],
				'currencyCode'				=> $AllCurrencyCode[$journalData['currencyId']]['code'],
				'exchangeRate'				=> $journalData['exchangeRate'],
				'description'				=> $journalData['description'],
				'credit_nominalCode'		=> $credit_nominalCode,
				'debit_nominalCode'			=> $debit_nominalCode,
				'credit_transactionAmount'	=> $credit_transactionAmount,
				'debit_transactionAmount'	=> $debit_transactionAmount,
				'total_amt'					=> $debit_transactionAmount,
				'credit_taxCode'			=> $credit_taxCode,
				'debit_taxCode'				=> $debit_taxCode,
				'taxDate'					=> date('Y-m-d H:i:s', strtotime($journalData['taxDate'])),
				'createdOn'					=> date('Y-m-d H:i:s', strtotime($journalData['createdOn'])),
				'params'					=> json_encode($journalData),
			);
			$saveCronTime[]	= strtotime($journalData['createdOn']);
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
}
return $returns;