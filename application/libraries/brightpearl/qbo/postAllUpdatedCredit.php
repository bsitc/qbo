<?php
$this->reInitialize();
$enableTaxCustomisation	= $this->ci->globalConfig['enableTaxCustomisation'];
$AllTaxRates			= $this->getAllTax();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$enableTaxCustomisation){
		continue;
	}
	$this->ci->db->reset_query();
	if($objectId){
		$this->ci->db->where_in('orderId',$objectId);
	}
	$datas	= $this->ci->db->get_where('sales_order_customisation',array('status' => '0', 'orderType' => 'SC', 'account1Id' => $account1Id))->result_array();
	if(!$datas){
		continue;
	}
	$OrderUpdateConfigDatas	= $this->ci->db->get_where('taxupdate_config', array('brightpearlAccountId' => $account1Id))->row_array();
	$UpdateOrderStatus		= '';
	if($OrderUpdateConfigDatas){
		$UpdateOrderStatus	= $OrderUpdateConfigDatas['SetSalesCreditStatus'];
	}
	if($datas){
		foreach($datas as $orderDatas){
			$orderId				= $orderDatas['orderId'];
			$rowData				= json_decode($orderDatas['rowData'],true);
			$processedOrderRowsIds	= json_decode($orderDatas['processedOrderRowsIds'],true);
			$orderRows				= $rowData['orderRows'];
			$createdRowData			= array();
			$statusUpdateRequest	= array();
			$SaveOrderRowsIDs		= array();
			$FailOrderRowsIDs		= array();
			$SuccessRowsIDs			= array();
			$IsError				= 0;
			$IsSuccess				= 0;
			if($orderRows){
				foreach($orderRows as $orderRowId => $orderRowsData){
					if($processedOrderRowsIds){
						if(in_array($orderRowId,$processedOrderRowsIds)){
							$IsSuccess	= 1;
							continue;
						}
					}
					$UpdateRequest	= array();
					$taxClassId		= $orderRowsData['rowValue']['taxClassId'];
					$taxCode		= $orderRowsData['rowValue']['taxCode'];
					$taxRate		= $orderRowsData['rowValue']['taxRate'];
					$rowNetvalue	= $orderRowsData['rowValue']['rowNet']['value'];
					$rowTaxvalue	= $orderRowsData['rowValue']['rowTax']['value'];
					if(!$taxRate){
						$taxRate	= $AllTaxRates[$account1Id][$taxClassId]['rate'];
					}
					$TaxAmount		= (($rowNetvalue * $taxRate) / (100 + $taxRate));
					$rowNetvalue	= ($rowNetvalue - $TaxAmount);
					$rowTaxvalue	= $TaxAmount;
					
					$UpdateRequest	= array(
						array(
							'op'		=> 'replace',
							'path'		=> '/rowValue/rowNet/value',
							'value'		=> sprintf("%.4f",$rowNetvalue),
						),
						array(
							'op'		=> 'replace',
							'path'		=> '/rowValue/rowTax/value',
							'value'		=> sprintf("%.4f",$rowTaxvalue),
						),
					);
					$UpdateResponse	= array();
					if($UpdateRequest){
						$RowUpdateUrl	= '/order-service/order/'.$orderId.'/row/'.$orderRowId;
						$UpdateResponse	= $this->getCurl($RowUpdateUrl, "PATCH", json_encode($UpdateRequest), 'json' , $account1Id )[$account1Id];
						$createdRowData['OrderRows Update Request	: '.$orderRowId]	= $UpdateRequest;
						$createdRowData['OrderRows Update Response	: '.$orderRowId]	= $UpdateResponse;
						$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order_customisation',array('createdRowData' => json_encode($createdRowData)));
						if(!$UpdateResponse['errors']){
							$SuccessRowsIDs[]	= $orderRowId;
							$SaveOrderRowsIDs	= json_encode($SuccessRowsIDs);
							$IsSuccess			= 1;
							$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order_customisation',array('processedOrderRowsIds' => $SaveOrderRowsIDs));
						}
						else{
							$FailOrderRowsIDs[]	= $orderRowId;
							$IsError	= 1;
							$IsSuccess	= 0;
						}
					}
				}
			}
			if(($IsSuccess) AND (!$IsError)){
				if($UpdateOrderStatus){
					$statusUpdateRequest	= array(
						'orderStatusId'			=> $UpdateOrderStatus,
					);
				}
				elseif(!$FailOrderRowsIDs){
					$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order_customisation',array('status' => 1));
				}
				if($statusUpdateRequest){
					$statusUpdateUrl		= '/order-service/order/'.$orderId.'/status';
					$statusUpdateResponse	= $this->getCurl($statusUpdateUrl, "PUT", json_encode($statusUpdateRequest), 'json' , $account1Id )[$account1Id];
					$createdRowData['Order Status Update Request	:']	= $statusUpdateRequest;
					$createdRowData['Order Status Update Response	:']	= $statusUpdateResponse;
					$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order_customisation',array('createdRowData' => json_encode($createdRowData)));
					if(!$statusUpdateResponse['errors']){
						$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order_customisation',array('status' => 1));
					}
				}
			}
		}
	}
}
