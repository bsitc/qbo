<?php
$this->reInitialize();
$return						= array();
$disablePOpaymentqbotobp	= $this->ci->globalConfig['disablePOpaymentqbotobp'];
foreach($this->accountDetails as $account1Id => $accountDetails){
	if($disablePOpaymentqbotobp){continue;}
	$bpConfig	= $this->accountConfig[$account1Id];
	$timezone	= $bpConfig['timezone'];
	$datas		= array();
	
	$allData	= $this->ci->db->order_by('totalAmount', 'DESC')->get_where('purchase_order',array('isPaymentCreated' => 0, 'sendInAggregation' => 1, 'createOrderId <>' => '', 'paymentDetails <>' => '', 'status <=' => '3', 'account1Id' => $account1Id))->result_array();
	if(!$allData){continue;}
	
	/****paymentMappings******/
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account1Id' => $account1Id,'applicableOn' => 'purchase'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account2Id	= $paymentMappingsTemp['account2Id'];
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$account2PaymentId	= strtolower(trim($paymentMappingsTemp['account2PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$account2Id][$paymentchannelId][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$account2Id][$paymentchannelId][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[$account2Id][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account2Id][$account2PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	/*****end******/
	
	if($allData){
		foreach($allData as $allDataTemp){
			$datas[$allDataTemp['aggregationId']]['Orders'][$allDataTemp['orderId']]	= $allDataTemp;
			$datas[$allDataTemp['aggregationId']]['paymentInfo']	= $allDataTemp['paymentDetails'];;
		}
	}
	
	$orderIds		= array_column($allData,'orderId');
	$bpOrderInfos	= array();
	$bpInfosResult	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	if($bpInfosResult){
		foreach($bpInfosResult as $bpInfosResultTemp){
			$bpOrderInfos[$bpInfosResultTemp['id']]	= $bpInfosResultTemp;
		}
	}
	
	if($datas){
		foreach($datas as $aggregationId => $dataTemp){
			$paymentPosted			= 0;
			$globalPaymentDetails	= json_decode($dataTemp['paymentInfo'], true);
			foreach($dataTemp['Orders'] as $orderId => $data){
				$orderId		= $data['orderId'];
				$rowData		= json_decode($data['rowData'],true);
				$channelId		= $rowData['assignment']['current']['channelId'];
				$createdRowData	= json_decode($data['createdRowData'],true);
				$orderTotal		= $rowData['totalValue']['total'];
				$dueAmount		= $rowData['totalValue']['total'];
				$paidAmountOnBP	= 0;
				if(!$globalPaymentDetails){continue;}
				
				if($bpOrderInfos[$orderId]){
					if($bpOrderInfos[$orderId]['orderPaymentStatus'] == 'PAID'){
						continue;
					}
					elseif($bpOrderInfos[$orderId]['orderPaymentStatus'] == 'PARTIALLY_PAID'){
						$paymentFetchUrl	= '/accounting-service/supplier-payment-search?orderId='.$orderId.'&sort=createdOn.DESC';
						$paymentFetchResult	= $this->getCurl($paymentFetchUrl, "GET", '', 'json', $account1Id)[$account1Id];
						if($paymentFetchResult['results']){
							foreach($paymentFetchResult['results'] as $resultsTemp){
								if($resultsTemp[3] == 'PAYMENT'){
									$paidAmountOnBP += $resultsTemp[7];
								}
								elseif($resultsTemp[3] == 'RECEIPT'){
									$paidAmountOnBP += ((-1) * ($resultsTemp[7]));
								}
							}
						}
					}
				}
				
				$dueAmount		= ($dueAmount - $paidAmountOnBP);
				foreach($globalPaymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['status'] == 0){
						if($paymentDetail['sendPaymentTo'] == 'brightpearl'){
							if($paymentDetail['consolPayment'] == '1'){
								if($paymentDetail['amount'] > 0){
									if($dueAmount <= 0){continue;}
									
									$payRequest		= array();
									$sentAmount		= 0;
									$exchangeRate	= 1;
									$reference		= "Purchase Order Payment for OrderID : ".$orderId;
									$currencyCode	= $rowData['currency']['accountingCurrencyCode'];
									$paymentMethod	= $bpConfig['defaultPaymentMethod'];
									$paymentDate	= date('Y-m-d');
									if($paymentDetail['amount'] <= $dueAmount){
										$sentAmount	= $paymentDetail['amount'];
									}
									else{
										$sentAmount	= $dueAmount;
									}
									if(!$sentAmount){continue;}
									
									if($paymentDetail['Reference']){
										$reference			= $paymentDetail['Reference'];
									}
									if($paymentDetail['CurrencyRate']){
										$exchangeRate		= $paymentDetail['CurrencyRate'];
									}
									if($paymentDetail['currency']){
										$currencyCode		= $paymentDetail['currency'];
									}
									/* if($paymentDetail['paymentMethod']){
										$paymentMathodMappings	= $this->ci->db->get_where('mapping_paymentpurchase',array('account2Id' => $data['account2Id'],'account2PaymentId' => $paymentDetail['paymentMethod'],'account1Id' => $account1Id))->row_array();
										if($paymentMathodMappings){
											$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
										}
									} */
									
									if(isset($paymentMappings1[$data['account2Id']][$channelId][strtolower($currencyCode)][strtolower($paymentDetail['paymentMethod'])])){
										$paymentMethod		= $paymentMappings1[$data['account2Id']][$channelId][strtolower($currencyCode)][strtolower($paymentDetail['paymentMethod'])]['account1PaymentId'];
									}
									
									else if(isset($paymentMappings2[$data['account2Id']][$channelId][strtolower($paymentDetail['paymentMethod'])])){
										$paymentMethod		= $paymentMappings2[$data['account2Id']][$channelId][strtolower($paymentDetail['paymentMethod'])]['account1PaymentId'];
									}
									
									else if(isset($paymentMappings3[$data['account2Id']][strtolower($currencyCode)][strtolower($paymentDetail['paymentMethod'])])){
										$paymentMethod		= $paymentMappings3[$data['account2Id']][strtolower($currencyCode)][strtolower($paymentDetail['paymentMethod'])]['account1PaymentId'];
									}
									
									else if(isset($paymentMappings[$data['account2Id']][strtolower($paymentDetail['paymentMethod'])])){
										$paymentMethod		= $paymentMappings[$data['account2Id']][strtolower($paymentDetail['paymentMethod'])]['account1PaymentId'];
									}
									
									
									if($paymentDetail['paymentDate']){
										$paymentDate	= $paymentDetail['paymentDate'];
										$dDate			= date('Y-m-d',strtotime($paymentDate));
										$date			= new DateTime($dDate); 
										$date->setTimezone(new DateTimeZone($timezone));
										$paymentDate	= $date->format('Y-m-d');
									}
									$paymentRequest	= array(
										"paymentMethodCode"		=> $paymentMethod,
										"paymentType"			=> "PAYMENT",
										"orderId"				=> $orderId,
										"currencyIsoCode"		=> strtoupper($currencyCode),
										"exchangeRate"			=> $exchangeRate,
										"amountPaid"			=> $sentAmount,
										"paymentDate"			=> $paymentDate,
										"journalRef"			=> $reference,
									);
									$paymentUrl		= '/accounting-service/supplier-payment';
									$paymentResults	= $this->getCurl($paymentUrl, "POST", json_encode($paymentRequest), 'json' , $account1Id)[$account1Id];
									$createdRowData['Brightpearl Payment Request	:'.$paymentKey]	= $paymentRequest;
									$createdRowData['Brightpearl Payment Response	:'.$paymentKey]	= $paymentResults;
									$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('purchase_order',array('createdRowData' => json_encode($createdRowData)));
									if(!isset($paymentResults['errors'])){
										if(is_int($paymentResults)){
											$paymentPosted	= 1;
											$updateArray	= array();
											$dueAmount		= ($dueAmount - $sentAmount);
											$globalPaymentDetails[$paymentKey]['amount']	= ($paymentDetail['amount'] - $sentAmount);
											if($globalPaymentDetails[$paymentKey]['amount'] <= 0){
												$globalPaymentDetails[$paymentKey]['status']	= 1;
											}
											$globalPaymentDetails[$paymentKey]['brightpearlPayID'][]	= $paymentResults;
											$globalPaymentDetails[$paymentKey]['brightpearlOrderID'][]	= $orderId;
											$globalPaymentDetails[$paymentResults]	= array(
												'amount' 			=> $sentAmount,
												'status' 			=> '1',
												'amountCreditedIn'	=> 'brightpearl',
												'parentPaymentId'	=> $sentAmount,
												'paymentMethod'		=> $paymentMethod,
												'currency'			=> strtoupper($currencyCode),
												"exchangeRate"		=> $exchangeRate,
											);
											$OrderInfoUrl		= '/order-service/order/'.$orderId;
											$OrderInfoResults	= $this->getCurl($OrderInfoUrl, 'get', '', 'json', $account1Id)[$account1Id];
											if($OrderInfoResults){
												$PaymentStatus	= $OrderInfoResults[0]['orderPaymentStatus'];
												if(($PaymentStatus == 'PAID') OR ($PaymentStatus == 'NOT_APPLICABLE')){
													$updateArray	= array(
														'isPaymentCreated'	=> '1',
														'status'			=> '3',
													);
												}
											}
											if($updateArray){
												$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('purchase_order',$updateArray); 
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if($paymentPosted){
				$this->ci->db->where(array('aggregationId' => $aggregationId))->update('purchase_order',array('paymentDetails' => json_encode($globalPaymentDetails))); 
			}
		
		}
	}
}