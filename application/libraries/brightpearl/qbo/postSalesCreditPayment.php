<?php
$this->reInitialize();
$return						= array();
$PaymentReversalEnabled		= 1;
$disableSCpaymentqbotobp	= $this->ci->globalConfig['disableSCpaymentqbotobp'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	if($disableSCpaymentqbotobp){
		continue;
	}
	
	$exchangeDatas	= $this->getExchangeRate($account1Id)[$account1Id];
	$this->config	= $this->accountConfig[$account1Id];
	$orderDatas		= $this->ci->db->order_by('id', 'desc')->get_where('sales_credit_order',array('isNetOff' => 0, 'account1Id' => $account1Id,'status < ' => '4'))->result_array();
	if(!$orderDatas){
		continue;
	}
	/****paymentMappings******/
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account1Id' => $account1Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account2Id	= $paymentMappingsTemp['account2Id'];
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$account2PaymentId	= strtolower(trim($paymentMappingsTemp['account2PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$account2Id][$paymentchannelId][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$account2Id][$paymentchannelId][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[$account2Id][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account2Id][$account2PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	/*****end******/
	
	$orderIds				= array_column($orderDatas,'orderId');
	$orderInfosDatasTemps	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	$orderInfosDatas = array();
	foreach($orderInfosDatasTemps as $orderInfosDatasTemp){
		$orderInfosDatas[$orderInfosDatasTemp['id']]	= $orderInfosDatasTemp;
	}
	foreach($orderDatas as $orderData){
		if($orderData['sendInAggregation']){
			continue;
		}
		$orderId		= $orderData['orderId'];
		$orderRowData	= json_decode($orderData['rowData'],true);
		$channelId		= $orderRowData['assignment']['current']['channelId'];
		$createdRowData	= json_decode($orderData['createdRowData'],true);
		$paymentDetails	= json_decode($orderData['paymentDetails'],true);
		$paymentMethod	= $this->config['defaultPaymentMethod'];
		$currencyCode	= $orderRowData['currency']['accountingCurrencyCode'];
		
		if($clientcode == 'coloradokayak'){
			$taxDate	= $orderRowData['invoices']['0']['taxDate'];
			$taxDate	= date('Ymd',strtotime($taxDate));
			if($taxDate < '20210401'){
				continue;
			}
		}
		
		$payurl			= '/accounting-service/customer-payment';
		if(!$paymentDetails){
			continue;
		}
		$orderPaidOnBP		= 0;
		$orderInfosData		= array();
		$orderInfosData		= $orderInfosDatas[$orderId];
		if($orderInfosData['orderPaymentStatus'] == 'PAID'){
			$orderPaidOnBP	= 1;
		}
		$isFoundError		= 0;
		$issend				= 0;
		$totalReceivedPaidAmount	= 0;
		if($PaymentReversalEnabled){
			foreach($paymentDetails as $Mykey => $paymentDetail){
				if(($paymentDetail['status'] == 0)){
					if((strtolower($paymentDetail['sendPaymentTo']) == 'brightpearl') AND (strtolower($paymentDetail['isvoided']) == 'yes') AND (strtolower($paymentDetail['DeletedOnBrightpearl']) == 'no')){
						$updateArray	= array();
						$reversepayurl	= "/accounting-service/customer-payment";
						$reverseRequest	= array(
							"paymentMethodCode"		=> $paymentMethod,
							"paymentType"			=> "RECEIPT",
							"orderId"				=> $orderId,
							"currencyIsoCode"		=> strtoupper($currencyCode),
							"exchangeRate"			=> 1,
							"amountPaid"			=> $paymentDetail['amount'],
							"paymentDate"			=> date('c'),
							"journalRef"			=> "Reverse Payment From QBO"
						);
						$ReverseResponse	= $this->getCurl($reversepayurl, "POST", json_encode($reverseRequest), 'json' , $account1Id )[$account1Id];
						if(!$ReverseResponse['errors']){
							if(is_int($ReverseResponse)){
								$paymentDetails[$Mykey]['status']				= 1;
								$paymentDetails[$Mykey]['DeletedOnBrightpearl']	= "YES"; 
								$paymentDetails[$ReverseResponse]	= array(
									'amount' 				=> $paymentDetail['amount'],
									'status' 				=> '1',
									'AmountReversedIn'		=> 'brightpearl',
									'ParentQBOReverseId'	=> $Mykey,
								);
								$createdRowData['Send Void Payment To Brightpearl Request	: '.$Mykey]	= $reverseRequest;
								$createdRowData['Send Void Payment To Brightpearl Response	: '.$Mykey]	= $ReverseResponse;
								$updateArray['isPaymentCreated']	= 0;
								$updateArray['status']				= 1;
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['createdRowData']		= json_encode($createdRowData);
								$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_credit_order',$updateArray); 
							}
						}
						else{
							continue;
						}
					}
				}
			}
		}
		$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true);
		
		foreach($paymentDetails as $paykey => $paymentDetail){
			if((!$paymentDetail['isvoided']) AND (!$paymentDetail['AmountReversedIn']) AND (!$paymentDetail['AmountCreditedIn'])){
				$totalReceivedPaidAmount	+= $paymentDetail['amount'];
			}
			if(($paymentDetail['sendPaymentTo'] == 'brightpearl') AND ($paymentDetail['status'] == '0')){
				$amount	= 0;
				$amount	= $paymentDetail['amount'];
				if($amount > 0 ){
					if($paymentDetail['isvoided']){
						continue;
					}
					if($orderPaidOnBP){
						continue;
					}
					$reference			= '';
					$paymentDate		= '';
					$exchangeRate		= 1;
					$orderPaymentMethod	= '';
					$account2PaymentId	= '';
					$paymentMethod		= $this->config['defaultPaymentMethod'];
					$currencyCode		= $orderRowData['currency']['accountingCurrencyCode'];
					if(@$paymentDetail['Reference']){
						$reference			= $paymentDetail['Reference'];
					}
					if(@$paymentDetail['paymentMethod']){
						$orderPaymentMethod	= $paymentDetail['paymentMethod'];
					}
					if(@$paymentDetail['DepositToAccountRef']){
						$account2PaymentId	= $paymentDetail['DepositToAccountRef'];
					}
					if(@$paymentDetail['CurrencyRate']){
						$exchangeRate		= $paymentDetail['CurrencyRate'];
					}
					if(@$paymentDetail['currency']){
						$currencyCode		= $paymentDetail['currency'];
					}
					if(@$paymentDetail['paymentDate']){
						$paymentDate		= $paymentDetail['paymentDate'];
					}
					else{
						$paymentDate		= date('c');
					}
					/* if($account2PaymentId AND $orderPaymentMethod){
						$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $account2PaymentId,'paymentValue' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							if($orderPaymentMethod){
								$paymentMethod		= $paymentMathodMappings['account1PaymentId'];
							}
						}
					}
					elseif($account2PaymentId){
						$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $account2PaymentId,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							if($orderPaymentMethod){
								$paymentMethod		= $paymentMathodMappings['account1PaymentId'];
							}
						}
					}
					elseif($orderPaymentMethod){
						$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'paymentValue' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							if($orderPaymentMethod){
								$paymentMethod		= $paymentMathodMappings['account1PaymentId'];
							}
						}
					} */
					
					if(isset($paymentMappings1[$orderData['account2Id']][$channelId][strtolower($currencyCode)][strtolower($account2PaymentId)])){
						$paymentMethod		= $paymentMappings1[$orderData['account2Id']][$channelId][strtolower($currencyCode)][strtolower($account2PaymentId)]['account1PaymentId'];
					}
					
					else if(isset($paymentMappings2[$orderData['account2Id']][$channelId][strtolower($account2PaymentId)])){
						$paymentMethod		= $paymentMappings2[$orderData['account2Id']][$channelId][strtolower($account2PaymentId)]['account1PaymentId'];
					}
					
					else if(isset($paymentMappings3[$orderData['account2Id']][strtolower($currencyCode)][strtolower($account2PaymentId)])){
						$paymentMethod		= $paymentMappings3[$orderData['account2Id']][strtolower($currencyCode)][strtolower($account2PaymentId)]['account1PaymentId'];
					}
					
					else if(isset($paymentMappings[$orderData['account2Id']][strtolower($account2PaymentId)])){
						$paymentMethod		= $paymentMappings[$orderData['account2Id']][strtolower($account2PaymentId)]['account1PaymentId'];
					}
					
					$customerPaymentRequest	= array(
						"paymentMethodCode"		=> $paymentMethod,
						"paymentType"			=> "PAYMENT",
						"orderId"				=> $orderData['orderId'],
						"currencyIsoCode"		=> strtoupper($currencyCode),
						"exchangeRate"			=> (1 / $exchangeRate),
						"amountPaid"			=> $amount,
						"paymentDate"			=> $paymentDate,
						"journalRef"			=> ($reference) ? ($reference) : "Sales Credit Payment For OrderID : ".$orderData['orderId'],
					);
					$customerPaymentRequestRes	= $this->getCurl( $payurl, "POST", json_encode($customerPaymentRequest), 'json' , $account1Id )[$account1Id];
					$createdRowData['Send Payment To Brightpearl Request	: '.$paykey]	= $customerPaymentRequest;
					$createdRowData['Send Payment To Brightpearl Response	: '.$paykey]	= $customerPaymentRequestRes;
					$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)));
					if(!isset($customerPaymentRequestRes['errors'])){
						if(is_int($customerPaymentRequestRes)){
							$issend	= 1;
							$paymentDetails[$paykey]['status']			= '1';
							$paymentDetails[$paykey]['brightpearlID']	= $customerPaymentRequestRes;
							$paymentDetails[$customerPaymentRequestRes]	= array(
								'amount'			=> $amount,
								'status' 			=> '1',
								'AmountCreditedIn'	=> 'brightpearl',
								'ParentPaymentId'	=> $paykey,
							);
							if($totalReceivedPaidAmount >= $orderRowData['totalValue']['total']){
								$updateArray	= array(
									'isPaymentCreated'	=> '1',
									'status'			=> '3',
								);
							}
							$updateArray['paymentDetails']	= json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',$updateArray); 
						}
					}
				}
				else{
					continue;
				}
			}
		}
	}
}
?>