<?php
$this->reInitialize();
$disablePOpaymentqbotobp	= $this->ci->globalConfig['disablePOpaymentqbotobp'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	$batchUpdates	= array();
	$config			= $this->accountConfig[$account2Id];
	if(!$config['enablePurchaseConsol']){continue;}
	if($disablePOpaymentqbotobp){continue;}
	
	$basedOn				= 'createOrderId';
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,aggregationId,paymentDetails,'.$basedOn)->get_where('purchase_order',array('isPaymentCreated' => 0, 'sendInAggregation' => 1,'account2Id' => $account2Id,'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp[$basedOn]){
			$pendingPayments[$pendingPaymentsTemp[$basedOn]]	= $pendingPaymentsTemp;
		}
	}
	if(!$pendingPayments){continue;}
	
	$cronTime		= date('Y-m-d\TH:i:s',strtotime('-30 days'));	
	$query			= "select * from BillPayment Where Metadata.LastUpdatedTime >'".$cronTime."' order by Metadata.LastUpdatedTime DESC STARTPOSITION 0 MAXRESULTS 1000";
	$url			= "query?minorversion=4&query=".rawurlencode($query);
	$serchResults	= @$this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
	$newBillFound	= array();
	
	if(@$serchResults['QueryResponse']['BillPayment']){
		foreach($serchResults['QueryResponse']['BillPayment'] as $BillPayment){
			if((substr_count(strtolower($BillPayment['PrivateNote']),'voided')) OR (substr_count(strtolower($BillPayment['PrivateNote']),'anulado'))){
				continue;
			}
			$paymentMethod			= '';
			$paymentMethodDetails	= @$BillPayment[$BillPayment['PayType'].'Payment'];
			if($paymentMethodDetails){
				foreach($paymentMethodDetails as $paymentMethodDetail){
					if(@isset($paymentMethodDetail['value'])){
						$paymentMethod	= $paymentMethodDetail['value'];
					}
				}
			}
			$Invoices	= $BillPayment['Line'];
			if($Invoices){
				foreach($Invoices as $Invoice){
					$LinkedTxns	= $Invoice['LinkedTxn'];
					if($LinkedTxns){
						foreach($LinkedTxns as $LinkedTxn){
							if($LinkedTxn['TxnType'] == 'Bill'){
								$inoviceId	= @$LinkedTxn['TxnId'];
								if(!$inoviceId){continue;}
								if(!isset($pendingPayments[$inoviceId])){
									continue;
								}
								$paymentDetails	= array();
								if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
									$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
								}
								else{
									$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
								}
								if(@$paymentDetails[$BillPayment['Id']]){
									continue;
								}
								$newBillFound[$inoviceId]	= $inoviceId;
								@$paymentDetails[$BillPayment['Id']]	= array(
									'amount' 				=> $Invoice['Amount'],
									'sendPaymentTo'			=> 'brightpearl',
									'status' 				=> '0',
									'Reference' 			=> $BillPayment['DocNumber'],
									'paymentMethod' 		=> $paymentMethod,
									'currency' 				=> $BillPayment['CurrencyRef']['value'],
									'CurrencyRate' 			=> $BillPayment['ExchangeRate'], 
									'paymentDate' 			=> $BillPayment['TxnDate'], 
									'paymentInitiatedIn'	=> 'QBO', 
									'DeletedOnBrightpearl'	=> 'NO',
									'consolPayment'			=> 1,
								);
								$batchUpdates[$inoviceId]	= array(
									'paymentDetails' 	=> $paymentDetails,
									$basedOn 			=> $inoviceId,
									'sendPaymentTo' 	=> 'brightpearl',
								);
					
							}
						}
					}
				}
			}
		}
	}
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']		= json_encode($batchUpdate['paymentDetails']);
		}
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('purchase_order',$batchUpdate,'createOrderId');
			}
		}
	}
}
?>