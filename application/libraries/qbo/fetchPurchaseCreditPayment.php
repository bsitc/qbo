<?php
$this->reInitialize();
$disablePCpaymentqbotobp	= $this->ci->globalConfig['disablePCpaymentqbotobp'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	$batchUpdates				= array();
	if($disablePCpaymentqbotobp){
		continue;
	}
	
	$config	= $this->accountConfig[$account2Id];
	
	//PAYMENT REVERSAL CODE
	$paymentDataTemp	= array();
	$FetchPaymentDate	= date('Y-m-d H:i:s',strtotime('-90 days'));
	$allpayments		= $this->ci->db->select('orderId,createOrderId,paymentDetails,created')->where("date(`created`) > ","date('".$FetchPaymentDate."')",false)->get_where('purchase_credit_order',array('account2Id' => $account2Id, 'createOrderId <>' => '', 'paymentDetails <>' => ''))->result_array();
	if($allpayments){
		foreach($allpayments as $allpaymentsData){
			$paymentDataTemp[$allpaymentsData['createOrderId']]	= json_decode($allpaymentsData['paymentDetails'],true);
		} 
	}
	foreach($paymentDataTemp as $orderIDSS => $paymentDataTemps){
		foreach($paymentDataTemps as $key => $paymentDataTempsss){
			if(@$paymentDataTempsss['paymentInitiatedIn'] != 'QBO'){
				unset($paymentDataTemp[$orderIDSS][$key]);
			}
		}
	}
	foreach($paymentDataTemp as $orderIDSSS => $paymentDataTempTT){
		if(!$paymentDataTempTT){
			unset($paymentDataTemp[$orderIDSSS]);
		}
	}
	$cronTime		= date('Y-m-d\TH:i:s',strtotime('-24 hours'));	
	$query			= "select * from BillPayment Where Metadata.LastUpdatedTime >'".$cronTime."' order by Metadata.LastUpdatedTime DESC STARTPOSITION 0 MAXRESULTS 1000";			
	$url			= "query?minorversion=4&query=".rawurlencode($query);
	$serchResults	= $this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
	$InvoiceIDSS = '';
	$QBOID = '';
	if($serchResults){
		if(@$serchResults['QueryResponse']['BillPayment']){
			foreach($serchResults['QueryResponse']['BillPayment'] as $BillPayment){
				if((substr_count(strtolower($BillPayment['PrivateNote']),'voided')) OR (substr_count(strtolower($BillPayment['PrivateNote']),'anulado'))){
					$voidedpaymentFromQBO = $BillPayment['Id'];
					if($voidedpaymentFromQBO){
						foreach($paymentDataTemp as $orderIDS => $paymentDataTempSS){
							foreach($paymentDataTempSS as $key => $paymentDataTempSSSSS){
								if($voidedpaymentFromQBO == $key){
									$InvoiceIDSS	= $orderIDS;
									$QBOID			= $key;
									break;
								}
							}
						}
						if($InvoiceIDSS){
							$paymentDetailsNewROW	= $this->ci->db->select('orderId,createOrderId,paymentDetails')->get_where('purchase_credit_order',array('account2Id' => $account2Id, 'createOrderId' => $InvoiceIDSS, 'paymentDetails <>' => ''))->row_array();
							if($paymentDetailsNewROW['paymentDetails']){
								$paymentDetailsNew	= json_decode($paymentDetailsNewROW['paymentDetails'],true);
								if($paymentDetailsNew  AND (@$paymentDetailsNew[$voidedpaymentFromQBO]['DeletedOnBrightpearl'] == 'NO')){
									$paymentDetailsNew[$QBOID]['isvoided']		= 'YES';
									$paymentDetailsNew[$QBOID]['status']		= 0;
									$paymentDetailsNew[$QBOID]['paymentDate']	= $BillPayment['TxnDate']; 
									$updateArray['paymentDetails']				= json_encode($paymentDetailsNew);
									$updateArray['isPaymentCreated']			= 0;
									$updateArray['status']						= 1;
									
									$this->ci->db->where(array('account2Id' => $account2Id, 'createOrderId' => $InvoiceIDSS))->update('purchase_credit_order',$updateArray);
								}
							}
						}
						continue;
					}
				}
			}
		}
	}
	
	//CREATE PENDING PAYMENTS PURCHASE CREDITS MAPPINGS ON 'createOrderId' KEY	-:
	$basedOn				= 'createOrderId';
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,'.$basedOn)->get_where('purchase_credit_order',array('account2Id' => $account2Id, 'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp[$basedOn]){
			$pendingPayments[$pendingPaymentsTemp[$basedOn]]	= $pendingPaymentsTemp;
		}
	}
	
	if($serchResults){
		if($serchResults['QueryResponse']['BillPayment']){
			foreach($serchResults['QueryResponse']['BillPayment'] as $BillPayment){
				$paymentMethod			= '';
				$paymentMethodDetails	= @$BillPayment[$BillPayment['PayType'].'Payment'];
				if($paymentMethodDetails){
					foreach($paymentMethodDetails as $paymentMethodDetail){
						if(isset($paymentMethodDetail['value'])){
							$paymentMethod	= $paymentMethodDetail['value'];
						}
					}
				}
				$Invoices	= $BillPayment['Line']; 
				if($Invoices){
					foreach($Invoices as $Invoice){
						$LinkedTxns	= $Invoice['LinkedTxn'];
						if($LinkedTxns){
							foreach($LinkedTxns as $LinkedTxn){
								if($LinkedTxn['TxnType'] == 'VendorCredit'){
									$inoviceId	= @$LinkedTxn['TxnId'];
									if(!$inoviceId){
										continue;
									}						
									if(!isset($pendingPayments[$inoviceId])){
										continue;
									}
									$paymentDetails	= array();
									if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
										$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
									}
									else{
										$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
									}
									if(@$paymentDetails[$BillPayment['Id']]['amount'] > 0){
										continue;
									}
									@$paymentDetails[$BillPayment['Id']]	= array(
										'amount' 				=> $Invoice['Amount'],
										'sendPaymentTo'			=> 'brightpearl',
										'status' 				=> '0',
										'Reference' 			=> '',
										'paymentMethod' 		=> $paymentMethod,
										'currency' 				=> $BillPayment['CurrencyRef']['value'],
										'CurrencyRate' 			=> $BillPayment['ExchangeRate'],  
										'paymentDate' 			=> $BillPayment['TxnDate'],  
										"paymentInitiatedIn"	=> "QBO",
										"DeletedOnBrightpearl"	=> "NO",
									);
									$batchUpdates[$inoviceId]				= array(
										'paymentDetails'	=> $paymentDetails,
										$basedOn 			=> $inoviceId,
										'sendPaymentTo' 	=> 'brightpearl',
									);
								}
							}
						}
					}
				}
			}
		}
	}
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
		} 
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('purchase_credit_order',$batchUpdate,$basedOn); 
			}
		}
	}
}
?>