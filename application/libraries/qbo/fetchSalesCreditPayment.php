<?php
$this->reInitialize();
$disableSCpaymentqbotobp	= $this->ci->globalConfig['disableSCpaymentqbotobp'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$batchUpdates				= array(); 		
	if($disableSCpaymentqbotobp){
		continue;
	}
	
	//PAYMENT REVERSAL CODE
	$paymentDataTemp	= array();
	$FetchPaymentDate	= date('Y-m-d H:i:s',strtotime('-90 days'));
	$allpayments		= $this->ci->db->select('orderId,createOrderId,paymentDetails,created,sendInAggregation')->where("date(`created`) > ","date('".$FetchPaymentDate."')",false)->get_where('sales_credit_order',array('isNetOff' => 0, 'createOrderId <>' => '', 'paymentDetails <>' => '','account2Id' => $account2Id))->result_array();
	if($allpayments){
		foreach($allpayments as $allpaymentsData){
			$paymentDataTemp[$allpaymentsData['createOrderId']]	= json_decode($allpaymentsData['paymentDetails'],true);
		} 
	}
	foreach($paymentDataTemp as $orderIDSS => $paymentDataTemps){
		foreach($paymentDataTemps as $key => $paymentDataTempsss){
			if(@$paymentDataTempsss['paymentInitiatedIn'] != 'QBO'){
				unset($paymentDataTemp[$orderIDSS][$key]);
			}
		}
	}
	foreach($paymentDataTemp as $orderIDSSS => $paymentDataTempTT){
		if(!$paymentDataTempTT){
			unset($paymentDataTemp[$orderIDSSS]);
		}
	}
	
	$cronTime		= date('Y-m-d\TH:i:s',strtotime('-5 days'));	
	$query			= "select * from Payment Where Metadata.LastUpdatedTime >'".$cronTime."' order by Metadata.LastUpdatedTime DESC STARTPOSITION 0 MAXRESULTS 1000";			
	$url			= "query?minorversion=4&query=".rawurlencode($query);
	$serchResults	= @$this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
	$InvoiceIDSS = '';
	$QBOID = '';
	if(@$serchResults['QueryResponse']['Payment']){
		foreach($serchResults['QueryResponse']['Payment'] as $BillPayment){
			if((substr_count(strtolower($BillPayment['PrivateNote']),'voided')) OR (substr_count(strtolower($BillPayment['PrivateNote']),'anulado'))){
				$voidedpaymentFromQBO = $BillPayment['Id'];
				if($voidedpaymentFromQBO){
					foreach($paymentDataTemp as $orderIDS => $paymentDataTempSS){
						foreach($paymentDataTempSS as $key => $paymentDataTempSSSSS){
							if($voidedpaymentFromQBO == $key){
								$InvoiceIDSS	= $orderIDS;
								$QBOID			= $key;
								break;
							}
						}
					}
					if($InvoiceIDSS){
						$paymentDetailsNewROW	= $this->ci->db->select('orderId,createOrderId,paymentDetails,sendInAggregation')->get_where('sales_credit_order',array('isNetOff' => 0, 'createOrderId' => $InvoiceIDSS, 'paymentDetails <>' => '','account2Id' => $account2Id))->row_array();
						if($paymentDetailsNewROW['paymentDetails']){
							if($paymentDetailsNewROW['sendInAggregation']){
								continue;
							}
							$paymentDetailsNew	= json_decode($paymentDetailsNewROW['paymentDetails'],true);
							if($paymentDetailsNew  AND (@$paymentDetailsNew[$voidedpaymentFromQBO]['DeletedOnBrightpearl'] == 'NO')){
								$paymentDetailsNew[$QBOID]['isvoided']	= 'YES';
								$paymentDetailsNew[$QBOID]['status']	= 0;
								
								$updateArray	= array(
									'paymentDetails'	=> json_encode($paymentDetailsNew),
									'isPaymentCreated'	=> 0,
									'status'			=> 1,
								);
								$this->ci->db->where(array('createOrderId' => $InvoiceIDSS,'account2Id' => $account2Id))->update('sales_credit_order',$updateArray);
							}
						}
					}
					continue;
				}
			}
		}
	}
	//CREATE PENDING PAYMENTS SALES CREDIT MAPPINGS ON 'createOrderId' KEY	-:
	$basedOn				= 'createOrderId';
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,sendInAggregation,paymentDetails,'.$basedOn)->get_where('sales_credit_order',array('isNetOff' => 0, 'account2Id' => $account2Id,'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp['sendInAggregation']){
			continue;
		}
		if($pendingPaymentsTemp[$basedOn]){
			$pendingPayments[$pendingPaymentsTemp[$basedOn]]	= $pendingPaymentsTemp;
		}
	}
	
	$checkPyaments	= array();
	foreach($pendingPayments as $pendingPaymentssss){
		$paydetails	= json_decode($pendingPaymentssss['paymentDetails'],true);
		foreach($paydetails as $checkKey => $paydetailsss){
			if($paydetailsss["Purchase"]){
				$checkPyaments[$checkKey]	= $paydetailsss;
			}
		}
	}
	if(@$serchResults['QueryResponse']['Payment']){
		foreach($serchResults['QueryResponse']['Payment'] as $BillPayment){
			if((substr_count(strtolower($BillPayment['PrivateNote']),'voided')) OR (substr_count(strtolower($BillPayment['PrivateNote']),'anulado'))){
				continue;
			}
			$skipthepayment		= 0;
			$Reference			= @$BillPayment['PaymentRefNum'];					
			$paymentMethod		= @$BillPayment['PaymentMethodRef']['value'];					
			$Invoices			= $BillPayment['Line'];
			if($BillPayment['Line']){
				foreach($BillPayment['Line'] as $BillPaymentss){
					foreach($BillPaymentss['LinkedTxn'] as $BillPaymentLinks){
						if($BillPaymentLinks['TxnType'] == 'Check'){
							$CheckID	= $BillPaymentLinks['TxnId'];
							if($checkPyaments[$CheckID]){
								$skipthepayment		= 1;
							}
						}
					}
				}
			}
			if($skipthepayment){
				continue;
			}
			if($Invoices){
				foreach($Invoices as $Invoice){
					$LinkedTxns	= $Invoice['LinkedTxn'];
					if($LinkedTxns){
						foreach($LinkedTxns as $LinkedTxn){
							if($LinkedTxn['TxnType'] == 'CreditMemo'){
								$inoviceId	= @$LinkedTxn['TxnId'];
								if(!$inoviceId){
									continue;
								}						
								if(!isset($pendingPayments[$inoviceId])){
									continue;
								}
								if($pendingPayments[$inoviceId]['sendInAggregation']){
									continue;
								}
								$paymentDetails	= array();
								if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
									$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
								}
								else{
									$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
								}
								if(@$paymentDetails[$BillPayment['Id']]['amount'] > 0){
									continue;
								}
								
								if($clientcode == 'unvqbom'){
									$checkPaymentDate	= date('Ymd', strtotime($BillPayment['TxnDate']));
									if($checkPaymentDate < '20240301'){
										continue;
									}
								}
								
								@$paymentDetails[$BillPayment['Id']]	= array(
									'amount'				=> $Invoice['Amount'],
									'sendPaymentTo'			=> 'brightpearl',
									'status' 				=> '0',
									'Reference' 			=> $Reference,
									'paymentMethod' 		=> $paymentMethod,
									'DepositToAccountRef'	=> $BillPayment['DepositToAccountRef']['value'],
									'currency' 				=> $BillPayment['CurrencyRef']['value'],
									'CurrencyRate' 			=> $BillPayment['ExchangeRate'],  
									'paymentDate' 			=> $BillPayment['TxnDate'],
									'paymentInitiatedIn'	=> 'QBO',
									'DeletedOnBrightpearl'	=> 'NO'
								);
								$batchUpdates[$inoviceId]				= array(
									'paymentDetails'		=> $paymentDetails,
									 $basedOn 				=> $inoviceId,
									'sendPaymentTo' 		=> 'brightpearl',
								);
					
							}
						}
					}
				}
			}
		}
	}
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
		} 
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('sales_credit_order',$batchUpdate,$basedOn); 
			}
		}
	}
}
?>