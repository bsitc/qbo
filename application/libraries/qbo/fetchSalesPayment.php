<?php
$this->reInitialize();
$basedOn					= 'createOrderId';
$disableSOpaymentqbotobp	= $this->ci->globalConfig['disableSOpaymentqbotobp'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$batchUpdates				= array();
	if($disableSOpaymentqbotobp){
		continue;
	}
	//PAYMENT REVERSAL CODE
	$paymentDataTemp	= array();
	$FetchPaymentDate	= date('Y-m-d H:i:s',strtotime('-90 days'));
	$allpayments		= $this->ci->db->select('orderId,createOrderId,paymentDetails,created,sendInAggregation')->where("date(`created`) > ","date('".$FetchPaymentDate."')",false)->get_where('sales_order',array('isNetOff' => 0, 'createOrderId <>' => '', 'paymentDetails <>' => '','account2Id' => $account2Id))->result_array();
	if($allpayments){
		foreach($allpayments as $allpaymentsData){
			if($allpaymentsData['sendInAggregation']){
				continue;
			}
			else{
				$paymentDataTemp[$allpaymentsData['createOrderId']]	= json_decode($allpaymentsData['paymentDetails'],true);
			}
		} 
	}
	foreach($paymentDataTemp as $orderIDSS => $paymentDataTemps){
		foreach($paymentDataTemps as $key => $paymentDataTempsss){
			if(@$paymentDataTempsss['paymentInitiatedIn'] != 'QBO'){
				unset($paymentDataTemp[$orderIDSS][$key]);
			}
		}
	}
	foreach($paymentDataTemp as $orderIDSSS => $paymentDataTempTT){
		if(!$paymentDataTempTT){
			unset($paymentDataTemp[$orderIDSSS]);
		}
	}
	
	$cronTime		= date('Y-m-d\TH:i:s',strtotime('-30 days'));
	$query			= "select * from payment Where Metadata.LastUpdatedTime >'".$cronTime."' order by Metadata.LastUpdatedTime DESC STARTPOSITION 0 MAXRESULTS 1000";
	$url			= "query?minorversion=39&query=".rawurlencode($query);
	$serchResults	= @$this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
	$InvoiceIDSS = '';
	$QBOID = '';
	$newBillFound	= array();
	if($serchResults['QueryResponse']['Payment']){
		foreach($serchResults['QueryResponse']['Payment'] as $BillPayment){
			if((substr_count(strtolower($BillPayment['PrivateNote']),'voided')) OR (substr_count(strtolower($BillPayment['PrivateNote']),'anulado'))){
				$voidedpaymentFromQBO = $BillPayment['Id'];
				if($voidedpaymentFromQBO){
					foreach($paymentDataTemp as $orderIDS => $paymentDataTempSS){
						foreach($paymentDataTempSS as $key => $paymentDataTempSSSSS){
							if($voidedpaymentFromQBO == $key){
								$InvoiceIDSS	= $orderIDS;
								$QBOID			= $key;
								break;
							}
						}
					}
					if($InvoiceIDSS){
						$paymentDetailsNewROW	= $this->ci->db->select('orderId,createOrderId,paymentDetails,created,sendInAggregation')->get_where('sales_order',array('isNetOff' => 0, 'createOrderId' => $InvoiceIDSS, 'paymentDetails <>' => '','account2Id' => $account2Id))->row_array();
						if($paymentDetailsNewROW['sendInAggregation']){
							continue;
						}
						if($paymentDetailsNewROW['paymentDetails']){
							$paymentDetailsNew		= json_decode($paymentDetailsNewROW['paymentDetails'],true);
							if($paymentDetailsNew  AND ($paymentDetailsNew[$voidedpaymentFromQBO]['DeletedOnBrightpearl'] == 'NO')){
								$paymentDetailsNew[$QBOID]['isvoided']	= 'YES';
								$paymentDetailsNew[$QBOID]['status']	= 0;
								
								$updateArray['paymentDetails']		= json_encode($paymentDetailsNew);
								$updateArray['isPaymentCreated']	= 0;
								$updateArray['status']				= 1;
								$this->ci->db->where(array('createOrderId' => $InvoiceIDSS,'account2Id' => $account2Id))->update('sales_order',$updateArray);
							}
						}
					}
					continue;
				}
			}
		}
	}
	//CREATE PENDING PAYMENTS SALES ORDERS MAPPINGS ON 'createOrderId' KEY	-:
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,sendInAggregation')->get_where('sales_order',array('isNetOff' => 0, 'account2Id' => $account2Id, /* 'isPaymentCreated' => '0', */'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		$pendingPayments[$pendingPaymentsTemp['createOrderId']]	= $pendingPaymentsTemp;
	}
	if($serchResults['QueryResponse']['Payment']){
		foreach($serchResults['QueryResponse']['Payment'] as $BillPayment){
			if((substr_count(strtolower($BillPayment['PrivateNote']),'voided')) OR (substr_count(strtolower($BillPayment['PrivateNote']),'anulado'))){
				continue;
			}
			$Reference		= $BillPayment['PaymentRefNum'];
			$paymentMethod	= $BillPayment['PaymentMethodRef']['value'];
			$Invoices		= $BillPayment['Line']; 
			if($Invoices){
				foreach($Invoices as $Invoice){
					$LinkedTxns	= $Invoice['LinkedTxn'];
					if($LinkedTxns){
						foreach($LinkedTxns as $LinkedTxn){
							if($LinkedTxn['TxnType'] == 'Invoice'){
								$inoviceId	= @$LinkedTxn['TxnId'];
								if($pendingPayments[$inoviceId]){
									if($pendingPayments[$inoviceId]['sendInAggregation']){
										continue;
									}
								}
								if(!$inoviceId){
									continue;
								}
								if(!isset($pendingPayments[$inoviceId])){
									continue;
								}
								$paymentDetails	= array();
								if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
									$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
								}
								else{
									$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
								}
								if(@$paymentDetails[$BillPayment['Id']]['amount'] > 0){
									continue;
								}
								$newBillFound[$inoviceId]	= $inoviceId;
								
								if($clientcode == 'unvqbom'){
									$checkPaymentDate	= date('Ymd', strtotime($BillPayment['TxnDate']));
									if($checkPaymentDate < '20240301'){
										continue;
									}
								}
								
								@$paymentDetails[$BillPayment['Id']]	= array(
									'amount'				=> $Invoice['Amount'],
									'sendPaymentTo'			=> 'brightpearl',
									'status'				=> '0',
									'Reference'				=> $Reference,
									'paymentMethod'			=> $paymentMethod,
									'DepositToAccountRef'	=> $BillPayment['DepositToAccountRef']['value'],
									'currency'				=> $BillPayment['CurrencyRef']['value'],
									'CurrencyRate'			=> $BillPayment['ExchangeRate'],    
									'paymentDate'			=> $BillPayment['TxnDate'],    
									'paymentInitiatedIn'	=> 'QBO',
									'DeletedOnBrightpearl'	=> 'NO'
								);
								$batchUpdates[$inoviceId]				= array(
									'paymentDetails'	=> $paymentDetails,
									'createOrderId'		=> $inoviceId,
									'sendPaymentTo'		=> 'brightpearl',
								);
					
							}
						}
					}
				}
			}
		}
	}
	
	
	
	/* $depositeItemRef	= array('184');
	$query				= "select * from invoice Where Metadata.LastUpdatedTime >'".$cronTime."' and Balance = '0' order by Metadata.LastUpdatedTime DESC STARTPOSITION 0 MAXRESULTS 1000";
	$url				= "query?minorversion=4&query=".rawurlencode($query);
	$serchResults		= @$this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
	if(isset($serchResults['QueryResponse']['Invoice'])){
		$newBillIds	= @array_column($serchResults['QueryResponse']['Invoice'],'Id');
		if($newBillIds){
			foreach($newBillIds as $newBillId){
				$newBillFound[$newBillId]	= $newBillId;
			}
		}
	}
	if($newBillFound){
		$foundBIllIds	= array_keys($newBillFound);
		$foundBIllIds	= array_filter($foundBIllIds);
		$foundBIllIds	= array_chunk($foundBIllIds,100);
		foreach($foundBIllIds as $foundBIllId){
			$query	= "select * from invoice Where id IN ('".implode("','",$foundBIllId)."')";
			$url	= "query?minorversion=4&query=".rawurlencode($query);
			$billResultsTemps = @$this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
			if($billResultsTemps){
				$billResults	= $billResultsTemps['QueryResponse']['Invoice'];
				if(!$billResults['0']){
					$billResults	= array($billResults);
				}
				foreach($billResults as $billResult){
					$taxDetails	= array();
					$TaxLines	= $billResult['TxnTaxDetail']['TaxLine'];
					if(!isset($TaxLines['0'])){
						$TaxLines	= array($TaxLines);
					}
					foreach($TaxLines as $TaxLine){
						$taxDetails[$TaxLine['TaxLineDetail']['TaxRateRef']['value']] = $TaxLine;
					}
					$inoviceId	= @$billResult['Id'];
					if($pendingPayments[$inoviceId]){
						if($pendingPayments[$inoviceId]['sendInAggregation']){
							continue;
						}
					}
					if(!$inoviceId){
						continue;
					}
					if(!isset($pendingPayments[$inoviceId])){
						continue;
					}
					$paymentDetails	= array();
					if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
						$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
					}
					else{
						$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
					}
					$paymentId	= $inoviceId;
					if(@$paymentDetails[$paymentId]['amount'] > 0){
						continue;
					}
					$appliedAmt	= 0;
					foreach($billResult['Line'] as $Line){
						if($Line['DetailType'] == 'SalesItemLineDetail'){
							if(in_array($Line['SalesItemLineDetail']['ItemRef']['value'],$depositeItemRef)){
								if($Line['Amount'] < 0){
									$limeAmt	= $Line['Amount'];
									$taxValue	= @$Line['AccountBasedExpenseLineDetail']['TaxCodeRef']['value'];
									if(isset($taxDetails[$taxValue])){
										$taxDetail	= $taxDetails[$taxValue];
										$taxRate	= $taxDetail['TaxLineDetail']['TaxPercent'];
										if($taxRate > 0){
											$limeAmt	= $limeAmt + ( ($limeAmt * $taxRate) / 100 );
										}
									}
									$appliedAmt	+= $limeAmt;
								}
							}
						}
					}
					if($appliedAmt < 0){
						@$paymentDetails[$paymentId]	= array(
							'amount'			=> (-1) * $appliedAmt,
							'sendPaymentTo'		=> 'brightpearl',
							'status'			=> '0',
							'Reference'			=> 'Customer Deposit',
							'currency'			=> $billResult['CurrencyRef']['value'],
							'CurrencyRate'		=> $billResult['ExchangeRate'],
							'isPrePayment'		=> '1',
						);
						$batchUpdates[$inoviceId]		= array(
							'paymentDetails'	=> $paymentDetails,
							'createOrderId'		=> $inoviceId,
							'sendPaymentTo'		=> 'brightpearl',
						);
					}
				}
			}
		}
	} */
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
		} 
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('sales_order',$batchUpdate,'createOrderId');
			}
		}
	}
}
?>