<?php
//qbo_demo
$this->reInitialize();
$enableAmazonFeeOther	= $this->ci->globalConfig['enableAmazonFeeOther'];
$exchangeRates		= $this->getExchangeRate();
$allBpCurrencies		= $this->ci->brightpearl->getAllCurrency();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableAmazonFeeOther){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$this->ci->db->reset_query();
	$amazonFeeData	= $this->ci->db->get_where('amazonFeesOther',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(!$amazonFeeData){continue;}
	
	$exchangeRateQbo	= $exchangeRates[$account2Id];
	
	$nominalMappings		= array();
	if($this->ci->globalConfig['enableNominalMapping']){
		$this->ci->db->reset_query();
		$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
		$nominalChannelMappings	= array();
		if(!empty($nominalMappingTemps)){
			foreach($nominalMappingTemps as $nominalMappingTemp){
				if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
					$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
				}
				else{
					$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
				}
			}
		}
	}
	
	if($this->ci->globalConfig['enableChannelMapping']){
		$this->ci->db->reset_query();
		$channelMappings		= array();
		$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
		if($channelMappingsTemps){
			foreach($channelMappingsTemps as $channelMappingsTemp){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	if($amazonFeeData){
		$consolAmazonFeeDatas	= array();
		foreach($amazonFeeData as $amazonFeeDataTemp){
			$journalId			= $amazonFeeDataTemp['journalId'];
			$account1Id			= $amazonFeeDataTemp['account1Id'];
			$config1			= $this->ci->account1Config[$account1Id];
			$journalTypeCode	= $amazonFeeDataTemp['journalTypeCode'];
			$channelid			= $amazonFeeDataTemp['channelid'];
			$currencyId			= $amazonFeeDataTemp['currencyId'];
			$currencyCode		= $allBpCurrencies[$account1Id][$currencyId]['code'];
			$amazonFeeParam		= json_decode($amazonFeeDataTemp['params'],true);
			$taxDate			= $amazonFeeParam['taxDate'];
			$BPDateOffset		= (int)substr($taxDate,23,3);
			$Acc2Offset			= 0;
			$diff				= $BPDateOffset - $Acc2Offset;
			$date				= new DateTime($taxDate);
			$BPTimeZone			= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff	.= ' hour';
				$date->modify($diff);
			}
			$taxDate		= $date->format('Ymd');
			
			$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
			
			if($taxDate > $fetchTaxDate){
				continue;
			}
			
			$consolAmazonFeeDatas[$taxDate][$journalTypeCode][$channelid][$currencyCode][$journalId]	= $amazonFeeDataTemp;
		}
		
		if($consolAmazonFeeDatas){
			foreach($consolAmazonFeeDatas as $consolTaxDate => $consolAmazonFeeDatas11){
				foreach($consolAmazonFeeDatas11 as $consolJournalType => $consolAmazonFeeDatas1){
					foreach($consolAmazonFeeDatas1 as $consolChannel => $consolAmazonFeeDatas2){
						foreach($consolAmazonFeeDatas2 as $consolCurrency => $consolAmazonFeeDatas3){
							$consolBankTxnRequest	= array();
							$consolItemLineRequest	= array();
							$lineItmeNominalArray	= array();
							$processedJournalIds	= array();
							$createdRowData			= array();
							$bpconfig				= array();
							$lineItemSequence		= 0;
							$amazonFeeCount			= 0;
							$exchangeRate			= '';
							$defaultBankAccount		= '';
							$Reference				= '';
							$amazonFeeContactId		= $config['otherAmazonFeeContactId'];
							
							if(strtolower($consolJournalType) == 'bp'){
								foreach($consolAmazonFeeDatas3 as $journalId => $consolAmazonFeeData){
									$bpconfig		= $this->ci->account1Config[$consolAmazonFeeData['account1Id']];
									$missingNominal	= 0;
									$amazonFeeParam	= json_decode($consolAmazonFeeData['params'],true);
									$credits		= $amazonFeeParam['credits'];
									$debits			= $amazonFeeParam['debits'];
									$exchangeRate	= $consolAmazonFeeData['exchangeRate'];
									if(!$credits['0']){
										$credits	= array($credits);
									}
									if(!$debits['0']){
										$debits		= array($debits);
									}
									foreach($debits as $debit){
										$defaultNominal	= '';
										$transactionAmount	= abs($debit['transactionAmount']);
										if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
											$defaultNominal	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']])) AND ($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'])){
											$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'];
										}
										if(!$defaultNominal){
											$missingNominal	= 1;
											break;
										}
									}
									if($missingNominal){
										$this->ci->db->where(array('journalId' => $journalId))->update('amazonFeesOther',array('message' => 'Nominal Mapping Not Found'));
										continue;
									}
									foreach($credits as $credit){
										if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
											$defaultBankAccount	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']])) AND ($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'])){
											$defaultBankAccount	= $nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'];
										}
									}
									foreach($debits as $debit){
										$defaultNominal		= '';
										$transactionAmount	= abs($debit['transactionAmount']);
										if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
											$defaultNominal	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']])) AND ($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'])){
											$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'];
										}
										
										if(isset($lineItmeNominalArray[$defaultNominal]['netAmt'])){
											$lineItmeNominalArray[$defaultNominal]['netAmt']	+= $transactionAmount;
										}
										else{
											$lineItmeNominalArray[$defaultNominal]['netAmt']	= $transactionAmount;
										}
									}
									$processedJournalIds[]	= $journalId;
									$amazonFeeCount++;
								}
								
								if($lineItmeNominalArray){
									if(!$defaultBankAccount){
										echo "<pre>";print_r("Bank Account Nominal Mapping Missing"); echo "</pre>";
										continue;
									}
									
									
									foreach($lineItmeNominalArray as $lineNominal => $lineItmeNominalArray1){
										$consolItemLineRequest[$lineItemSequence]	= array(
											"Id"			=> $lineItemSequence + 1,
											"Description"	=> 'Consolidated Non-order related Amazon Fees',
											"Amount"		=> sprintf("%.4f",($lineItmeNominalArray1['netAmt'])),
											"DetailType"	=> "AccountBasedExpenseLineDetail",
											"AccountBasedExpenseLineDetail"	=> array(
												"AccountRef"	=> array("value" => $lineNominal),
												"TaxCodeRef"	=> array("value" => $config['salesNoTaxCode']),
											),
										);
										if(isset($channelMappings[$consolChannel])){
											$consolItemLineRequest[$lineItemSequence]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $channelMappings[$consolChannel]['account2ChannelId']);
										}
										$lineItemSequence++;
									}
									if($consolItemLineRequest){
										$Reference				= $consolTaxDate.'-AMZ-'.$consolCurrency.'-'.strtoupper($consolJournalType);
										$consolBankTxnRequest	=  array(
											"DocNumber"		=> $Reference,
											"AccountRef"	=> array("value" => $defaultBankAccount),
											"PaymentType"	=> 'Cash',
											"TxnDate"		=> date('Y-m-d',strtotime($consolTaxDate)),
											"CurrencyRef"	=> array("value" => $consolCurrency),
											'ExchangeRate'	=> sprintf("%.4f",(1 / $exchangeRate)),
											"Line"			=> $consolItemLineRequest,
										);
										if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
											$exRate = $this->getQboExchangeRateByDb($account2Id,$consolCurrency,$config['defaultCurrrency'],$consolTaxDate);
											if($exRate){
												$consolBankTxnRequest['ExchangeRate'] = $exRate;
											}
											else{
												$exRate	= $exchangeRateQbo[strtolower($consolCurrency)][strtolower($config['defaultCurrrency'])]['Rate'];
												if($exRate){
													$consolBankTxnRequest['ExchangeRate'] = $exRate;
												}
												else{
													echo 'ExchangeRate Not found Line no - 266';continue;
													unset($consolBankTxnRequest['ExchangeRate']);
												}
											}
										}
										if(!$consolBankTxnRequest['ExchangeRate']){
											unset($consolBankTxnRequest['ExchangeRate']);
										}
										if($consolBankTxnRequest){
											$url				= 'purchase?minorversion=45';
											$Purchaseresults	= $this->getCurl($url, 'POST', json_encode($consolBankTxnRequest), 'json', $account2Id)[$account2Id];
											$createdRowData['QBO Amazon other fee Request Data']	= $consolBankTxnRequest;
											$createdRowData['QBO Amazon other fee Response Data']	= $Purchaseresults;
											if((isset($Purchaseresults['Purchase']['Id']))){
												$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('amazonFeesOther',array('qboTxnId' => $Purchaseresults['Purchase']['Id'], 'qboRefNo' => $Reference, 'status' => '1', 'createdParams' => json_encode($createdRowData)));
											}
											else{
												$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('amazonFeesOther',array('createdParams' => json_encode($createdRowData)));
											}
										}
									}
								}
							}
							elseif(strtolower($consolJournalType) == 'br'){
								foreach($consolAmazonFeeDatas3 as $journalId => $consolAmazonFeeData){
									$bpconfig		= $this->ci->account1Config[$consolAmazonFeeData['account1Id']];
									$missingNominal	= 0;
									$amazonFeeParam	= json_decode($consolAmazonFeeData['params'],true);
									$credits		= $amazonFeeParam['credits'];
									$debits			= $amazonFeeParam['debits'];
									$exchangeRate	= $consolAmazonFeeData['exchangeRate'];
									if(!$credits['0']){
										$credits	= array($credits);
									}
									if(!$debits['0']){
										$debits		= array($debits);
									}
									foreach($credits as $credit){
										$defaultNominal	= '';
										$transactionAmount	= abs($credit['transactionAmount']);
										if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
											$defaultNominal	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']])) AND ($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'])){
											$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'];
										}
										if(!$defaultNominal){
											$missingNominal	= 1;
											break;
										}
									}
									if($missingNominal){
										$this->ci->db->where(array('journalId' => $journalId))->update('amazonFeesOther',array('message' => 'Nominal Mapping Not Found'));
										continue;
									}
									foreach($debits as $debit){
										if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
											$defaultBankAccount	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']])) AND ($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'])){
											$defaultBankAccount	= $nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'];
										}
									}
									foreach($credits as $credit){
										$defaultNominal		= '';
										$transactionAmount	= abs($credit['transactionAmount']);
										if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
											$defaultNominal	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']])) AND ($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'])){
											$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'];
										}
										if(isset($lineItmeNominalArray[$defaultNominal]['netAmt'])){
											$lineItmeNominalArray[$defaultNominal]['netAmt']	+= $transactionAmount;
										}
										else{
											$lineItmeNominalArray[$defaultNominal]['netAmt']	= $transactionAmount;
										}
									}
									$processedJournalIds[]	= $journalId;
									$amazonFeeCount++;
								}
								
								if($lineItmeNominalArray){
									if(!$defaultBankAccount){
										echo "<pre>";print_r("Bank Account Nominal Mapping Missing"); echo "</pre>";
										continue;
									}
									
									
									foreach($lineItmeNominalArray as $lineNominal => $lineItmeNominalArray1){
										$consolItemLineRequest[$lineItemSequence]	= array(
											"LineNum"			=> $lineItemSequence + 1,
											"Amount"			=> sprintf("%.4f",($lineItmeNominalArray1['netAmt'])),
											"Description"		=>  'Consolidated Non-order related Amazon Fees',
											"DetailType"		=> "DepositLineDetail",
											"DepositLineDetail"	=> array(
												"AccountRef"		=> array("value" => $lineNominal),
												"CheckNum"			=> $consolTaxDate.'-AMZ-'.$consolCurrency.'-'.strtoupper($consolJournalType),
											),
										);
										if(isset($channelMappings[$consolChannel])){
											$consolItemLineRequest[$lineItemSequence]['DepositLineDetail']['ClassRef']	= array('value' => $channelMappings[$consolChannel]['account2ChannelId']);
										}
										$lineItemSequence++;
									}
									if($consolItemLineRequest){
										$Reference				= $consolTaxDate.'-AMZ-'.$consolCurrency.'-'.strtoupper($consolJournalType);
										$consolBankTxnRequest	=  array(
											"DepositToAccountRef"	=> array("value" => $defaultBankAccount),
											"TxnDate"				=> date('Y-m-d',strtotime($consolTaxDate)),
											"CurrencyRef"			=> array("value" => $consolCurrency),
											'ExchangeRate'			=> sprintf("%.4f",(1 / $exchangeRate)),
											"Line"					=> $consolItemLineRequest,
										);
										if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
											$exRate = $this->getQboExchangeRateByDb($account2Id,$consolCurrency,$config['defaultCurrrency'],$consolTaxDate);
											if($exRate){
												$consolBankTxnRequest['ExchangeRate'] = $exRate;
											}
											else{
												$exRate	= $exchangeRateQbo[strtolower($consolCurrency)][strtolower($config['defaultCurrrency'])]['Rate'];
												if($exRate){
													$consolBankTxnRequest['ExchangeRate'] = $exRate;
												}
												else{
													echo 'ExchangeRate Not found Line no - 266';continue;
													unset($consolBankTxnRequest['ExchangeRate']);
												}
											}
										}
										if(!$consolBankTxnRequest['ExchangeRate']){
											unset($consolBankTxnRequest['ExchangeRate']);
										}
										if($consolBankTxnRequest){
											$url			= 'deposit?minorversion=45';
											$depositResults	= $this->getCurl($url, 'POST', json_encode($consolBankTxnRequest), 'json', $account2Id)[$account2Id];
											$createdRowData['QBO Amazon other fee Request Data']	= $consolBankTxnRequest;
											$createdRowData['QBO Amazon other fee Response Data']	= $depositResults;
											if((isset($depositResults['Deposit']['Id']))){
												$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('amazonFeesOther',array('qboTxnId' => $depositResults['Deposit']['Id'], 'qboRefNo' => $Reference, 'status' => '1', 'createdParams' => json_encode($createdRowData)));
											}
											else{
												$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('amazonFeesOther',array('createdParams' => json_encode($createdRowData)));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}