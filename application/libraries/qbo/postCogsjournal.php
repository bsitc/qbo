<?php
$this->reInitialize();
$enableCOGSJournals	= $this->ci->globalConfig['enableCOGSJournals'];
$exchangeRates		= $this->getExchangeRate();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableCOGSJournals){continue;}
	if($clientcode == 'bbhugmeqbo'){if(!$exchangeRates){continue;}}
	
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$cogsDataPO			= $this->ci->db->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 0, 'OrderType' => 'PO'))->result_array();
	
	if((is_array($cogsDataPO)) AND (!empty($cogsDataPO))){
		$AllPOCogsByOrderID	= array();
		$AllVoidableIds		= array();
		foreach($cogsDataPO as $cogsDataPOTemp){
			if($cogsDataPOTemp['OrderType'] == 'PO'){
				$AllPOCogsByOrderID[$cogsDataPOTemp['orderId']][]	= $cogsDataPOTemp;
			}
		}
		if(!empty($AllPOCogsByOrderID)){
			foreach($AllPOCogsByOrderID as $purchaseOrderID => $AllPOCogsByOrderIDTemp){
				$allPOCogsIds	= array();
				if(count($AllPOCogsByOrderIDTemp) > 1){
					$allPOCogsIds	= array_column($AllPOCogsByOrderIDTemp,'journalsId');
					$allPOCogsIds	= array_filter($allPOCogsIds);
					$allPOCogsIds	= array_unique($allPOCogsIds);
					rsort($allPOCogsIds);
				}
				if(!empty($allPOCogsIds)){
					foreach($allPOCogsIds as $poCogsIdKey => $allPOCogsIdsTemp){
						if($poCogsIdKey != 0){
							$AllVoidableIds[]	= $allPOCogsIdsTemp;
						}
					}
				}
			}
			if(!empty($AllVoidableIds)){
				$this->ci->db->where_in('journalsId',$AllVoidableIds)->update('cogs_journal',array('status' => 4, 'message' => 'Mispostings Journal'));
			}
		}
	}
	
	$allPostedPOCogsAll	= array();
	$this->ci->db->reset_query();
	$allPostedPOCogs	= $this->ci->db->select('orderId,journalsId')->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 1, 'OrderType' => 'PO', 'createdJournalsId <>' => ''))->result_array();
	if((is_array($allPostedPOCogs)) AND (!empty($allPostedPOCogs))){
		foreach($allPostedPOCogs as $allPostedPOCogsTemp){
			$allPostedPOCogsAll[$allPostedPOCogsTemp['orderId']][]	= $allPostedPOCogsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('journalsId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if((!is_array($datas)) OR (empty($datas))){continue;}
	
	$AllProcessableIds	= array();
	if((is_array($datas)) AND (!empty($datas))){
		foreach($datas as $datasForIDs){
			$AllProcessableIds[]	= $datasForIDs['orderId'];
		}
		$AllProcessableIds	= array_filter($AllProcessableIds);
		$AllProcessableIds	= array_unique($AllProcessableIds);
	}
	
	$this->ci->db->reset_query();
	$allSaveSalesCredits	= array();
	if((is_array($AllProcessableIds)) AND (!empty($AllProcessableIds))){
		$this->ci->db->where_in('orderId',$AllProcessableIds);
	}
	$allSaveSalesCreditsDatas	= $this->ci->db->select('orderId,createOrderId,rowData,sendInAggregation, qboContactID')->get_where('sales_credit_order',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation <>' => 1))->result_array();
	if((is_array($allSaveSalesCreditsDatas)) AND (!empty($allSaveSalesCreditsDatas))){
		foreach($allSaveSalesCreditsDatas as $allSaveSalesCreditsData){
			if($allSaveSalesCreditsData['sendInAggregation']){continue;}
			if($allSaveSalesCreditsData['createOrderId']){
				$allSaveSalesCredits[$allSaveSalesCreditsData['orderId']]	= $allSaveSalesCreditsData;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$allSaveRefundReceipt	= array();
	if((is_array($AllProcessableIds)) AND (!empty($AllProcessableIds))){
		$this->ci->db->where_in('orderId',$AllProcessableIds);
	}
	$allSaveRefundReceiptDatas	= $this->ci->db->select('orderId,createOrderId,rowData,sendInAggregation')->get_where('refund_receipt',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation <>' => 1))->result_array();
	if((is_array($allSaveRefundReceiptDatas)) AND (!empty($allSaveRefundReceiptDatas))){
		foreach($allSaveRefundReceiptDatas as $allSaveRefundReceiptData){
			if($allSaveRefundReceiptData['sendInAggregation']){continue;}
			if($allSaveRefundReceiptData['createOrderId']){
				$allSaveRefundReceipt[$allSaveRefundReceiptData['orderId']]	= $allSaveRefundReceiptData;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$allSaveSalesOrders		= array();
	if((is_array($AllProcessableIds)) AND (!empty($AllProcessableIds))){
		$this->ci->db->where_in('orderId',$AllProcessableIds);
	}
	$allSaveSalesOrdersDatas	= $this->ci->db->select('orderId,createOrderId,rowData,sendInAggregation, qboContactID')->get_where('sales_order',array('status <>' => '0','sendInAggregation <>' => 1))->result_array();
	if((is_array($allSaveSalesOrdersDatas)) AND (!empty($allSaveSalesOrdersDatas))){
		foreach($allSaveSalesOrdersDatas as $allSaveSalesOrdersData){
			if($allSaveSalesOrdersData['sendInAggregation']){continue;}
			if($allSaveSalesOrdersData['createOrderId']){
				$allSaveSalesOrders[$allSaveSalesOrdersData['orderId']]	= $allSaveSalesOrdersData;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$allSavePurchaseOrders	= array();
	if((is_array($AllProcessableIds)) AND (!empty($AllProcessableIds))){
		$this->ci->db->where_in('orderId',$AllProcessableIds);
	}
	$allSavePurchaseOrdersDatas	= $this->ci->db->select('orderId,createOrderId,rowData, qboContactID')->get_where('purchase_order',array('status <>' => '0', 'account2Id' => $account2Id))->result_array();
	if((is_array($allSavePurchaseOrdersDatas)) AND (!empty($allSavePurchaseOrdersDatas))){
		foreach($allSavePurchaseOrdersDatas as $allSavePurchaseOrdersData){
			if($allSavePurchaseOrdersData['createOrderId']){
				$allSavePurchaseOrders[$allSavePurchaseOrdersData['orderId']]	= $allSavePurchaseOrdersData;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if((is_array($nominalMappingTemps)) AND (!empty($nominalMappingTemps))){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if((is_array($channelMappingsTemps)) AND (!empty($channelMappingsTemps))){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$ChannelLocationMappings		= array();
	$ChannelLocationMappingsTemps	= $this->ci->db->get_where('mapping_channelLocation',array('account2Id' => $account2Id))->result_array();
	if((is_array($ChannelLocationMappingsTemps)) AND (!empty($ChannelLocationMappingsTemps))){
		foreach($ChannelLocationMappingsTemps as $ChannelLocationMappingsTemp){
			$ChannelLocationMappings[$ChannelLocationMappingsTemp['account1LocationId']][$ChannelLocationMappingsTemp['account1ChannelId']]	= $ChannelLocationMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	if((is_array($datas)) AND (!empty($datas))){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['createdJournalsId']){continue;}
			if((is_array($allPostedPOCogsAll)) AND (!empty($allPostedPOCogsAll))){
				if(isset($allPostedPOCogsAll[$orderDatas['orderId']])){
					continue;
				}
			}
			
			if($orderDatas['journalTypeCode'] == 'GO'){
				if((!isset($allSaveSalesOrders[$orderDatas['orderId']])) AND (!isset($allSavePurchaseOrders[$orderDatas['orderId']]))){continue;}
				if((isset($allSaveSalesOrders[$orderDatas['orderId']])) AND ($allSaveSalesOrders[$orderDatas['orderId']]['sendInAggregation'])){continue;}
			}
			elseif(($orderDatas['journalTypeCode'] == 'SG') OR ($orderDatas['journalTypeCode'] == 'PG')){
				if((!isset($allSaveSalesCredits[$orderDatas['orderId']])) AND (!isset($allSaveRefundReceipt[$orderDatas['orderId']]))){continue;}
				if((isset($allSaveSalesCredits[$orderDatas['orderId']])) AND ($allSaveSalesCredits[$orderDatas['orderId']]['sendInAggregation'])){continue;}
				if((isset($allSaveRefundReceipt[$orderDatas['orderId']])) AND ($allSaveRefundReceipt[$orderDatas['orderId']]['sendInAggregation'])){continue;}
			}
			else{continue;}
			
			$bpOrderId			= $orderDatas['orderId'];
			$journalTypeCode	= $orderDatas['journalTypeCode'];
			$qboContactID		= '';
			$entityType			= '';
			$MainOrderRowData	= array();
			
			if($journalTypeCode == 'GO'){
				if(isset($allSaveSalesOrders[$bpOrderId])){
					$MainOrderRowData	= json_decode($allSaveSalesOrders[$bpOrderId]['rowData'],true);
					$qboContactID		= $allSaveSalesOrders[$bpOrderId]['qboContactID'];
					$entityType			= 'Customer';
				}
				elseif(isset($allSavePurchaseOrders[$bpOrderId])){
					$MainOrderRowData	= json_decode($allSavePurchaseOrders[$bpOrderId]['rowData'],true);
					$qboContactID		= $allSavePurchaseOrders[$bpOrderId]['qboContactID'];
					$entityType			= 'Vendor';
				}
			}
			elseif(($journalTypeCode == 'SG') OR ($journalTypeCode == 'PG')){
				if(isset($allSaveSalesCredits[$bpOrderId])){
					$MainOrderRowData	= json_decode($allSaveSalesCredits[$bpOrderId]['rowData'],true);
					$qboContactID		= $allSaveSalesCredits[$bpOrderId]['qboContactID'];
					$entityType			= 'Customer';
				}
				elseif(isset($allSaveRefundReceipt[$bpOrderId])){
					$MainOrderRowData	= json_decode($allSaveRefundReceipt[$bpOrderId]['rowData'],true);
					$entityType			= 'Customer';
				}
			}
			else{continue;}
			
			$exRate				= '';
			$config1			= $this->ci->account1Config[$orderDatas['account1Id']];
			$invoiceReference	= $orderDatas['invoiceReference'];
			$invoiceReference	= $MainOrderRowData['invoices']['0']['invoiceReference'];
			$CustomerRef		= $MainOrderRowData['reference'];
			$journalsId			= $orderDatas['journalsId'];
			$params				= json_decode($orderDatas['params'],true);
			$createdParams		= json_decode($orderDatas['createdParams'],true);
			$channelId			= $orderDatas['channelId'];
			$taxCode			= $orderDatas['taxCode'];
			$creditAmount		= $orderDatas['creditAmount'];
			$debitAmount		= $orderDatas['debitAmount'];
			$creditNominalCode	= $orderDatas['creditNominalCode'];
			$debitNominalCode	= $orderDatas['debitNominalCode'];
			$CustomLocationID	= $MainOrderRowData['customFields'][$config1['SOLocationCustomField']]['id'];
			$taxDate			= $params['taxDate'];
			
			if($clientcode == 'unvqbom'){$taxDate = $MainOrderRowData['invoices']['0']['taxDate'];}
			
			$BPDateOffset		= (int)substr($taxDate,23,3);
			$QBOoffset			= 0;
			$diff				= $BPDateOffset - $QBOoffset;
			$date				= new DateTime($taxDate);
			$BPTimeZone			= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
			}
			$taxDate			= $date->format('Y-m-d');
			$exchangeRate		= $params['exchangeRate'];
			$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
			if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$orderDatas['currencyCode'],$config['defaultCurrrency'],$taxDate);
				if($exRate){
					$exchangeRate		= $exRate;
				}
				else{
					$exRate = $exchangeRatesAll[strtolower($orderDatas['currencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$exchangeRate		= $exRate;
					}
					else{
						$exchangeRate		= 0;
					}
				}
			}
			
			$request				= array();
			$DocNumber				= '';
			$InvoiceLineAdd			= array();
			$AllCredits				= array();
			$AllDebits				= array();
			$ItemSequence			= 0;
			$debitAccRef			= '';
			$creditAccRef			= '';
			$defaultNominalPurchase	= $config['DefaultCOGSforPurchase'];
			
			if($orderDatas['OrderType'] != 'RR'){
				if($config['entityIdOnJournal']){
					if(!$qboContactID){
						$this->ci->db->update('cogs_journal',array('message' => 'Contact ID is Missing'),array('id' => $orderDatas['id']));
						continue;
					}
				}
			}
			
			if($allSavePurchaseOrders[$bpOrderId]){
				$blockPosting	= 0;
				$allCredits		= $params['credits'];
				foreach($allCredits as $key => $journalLineTemp){
					if($journalLineTemp['transactionAmount'] == 0){continue;}
					$lineAccRef		= '';
					$journalNominal	= $journalLineTemp['nominalCode'];
					if(($journalNominal >= 5000) AND ($journalNominal <= 5999)){
						if($nominalMappings[$journalNominal]['account2NominalId']){
							$lineAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
							$lineAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
						}
						if(!$lineAccRef){
							$blockPosting	= 1;
							break;
						}
					}
					else{
						$lineAccRef	= $defaultNominalPurchase;
					}
					
					if($lineAccRef){
						$InvoiceLineAdd[$ItemSequence]	= array(
							'LineNum'					=> ($ItemSequence + 1),
							'Description'				=> 'COGS Journal',
							'Amount'					=> sprintf("%.4f",($journalLineTemp['transactionAmount'])),
							'DetailType'				=> 'JournalEntryLineDetail',
							'JournalEntryLineDetail'	=> array(
								'PostingType'				=> 'Credit',
								'AccountRef'				=> array('value' => $lineAccRef),
							),
						);
						if($config['entityIdOnJournal']){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']					= $entityType;
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
						}
						if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
						}
						elseif($ChannelLocationMappings[$CustomLocationID][$channelId]){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId']);
						}
						elseif($channelMappings[$channelId]['account2ChannelId']){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
						}
						
						$ItemSequence++;
					}
				}
				$allDebits	= $params['debits'];
				foreach($allDebits as $key => $journalLineTemp){
					if($journalLineTemp['transactionAmount'] == 0){continue;}
					$lineAccRef		= '';
					$journalNominal	= $journalLineTemp['nominalCode'];
					if(($journalNominal >= 5000) AND ($journalNominal <= 5999)){
						if($nominalMappings[$journalNominal]['account2NominalId']){
							$lineAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
							$lineAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
						}
						if(!$lineAccRef){
							$blockPosting	= 1;
							break;
						}
					}
					else{
						$lineAccRef	= $defaultNominalPurchase;
					}
					if($lineAccRef){
						$InvoiceLineAdd[$ItemSequence]	= array(
							'LineNum'					=> ($ItemSequence + 1),
							'Description'				=> 'COGS Journal',
							'Amount'					=> sprintf("%.4f",($journalLineTemp['transactionAmount'])),
							'DetailType'				=> 'JournalEntryLineDetail',
							'JournalEntryLineDetail'	=> array(
								'PostingType'				=> 'Debit',
								'AccountRef'				=> array('value' => $lineAccRef),
							),
						);
						if($config['entityIdOnJournal']){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']					= $entityType;
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
						}
						if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
						}
						elseif($ChannelLocationMappings[$CustomLocationID][$channelId]){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId']);
						}
						elseif($channelMappings[$channelId]['account2ChannelId']){
							$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
						}
						$ItemSequence++;
					}
				}
				if($blockPosting){
					$this->ci->db->update('cogs_journal',array('message' => 'NominalMapping is Missing'),array('id' => $orderDatas['id']));
					continue;
				}
			}
			else{
				$allCredits		= $params['credits'];
				foreach($allCredits as $key => $journalLineTemp){
					$lineAccRef		= '';
					$journalNominal	= $journalLineTemp['nominalCode'];
					if($journalTypeCode == 'GO'){
						$lineAccRef	= $config['COGSCreditNominalSO'];
					}
					if($journalTypeCode == 'PG'){
						$lineAccRef	= $config['COGSCreditNominalSCPG'];
					}
					if($journalTypeCode == 'SG'){
						$lineAccRef	= $config['COGSCreditNominalSC'];
					}
					if($nominalMappings[$journalNominal]['account2NominalId']){
						$lineAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
						$lineAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
					}
					$InvoiceLineAdd[$ItemSequence]	= array(
						'LineNum'					=> ($ItemSequence + 1),
						'Description'				=> 'COGS Journal',
						'Amount'					=> sprintf("%.4f",($journalLineTemp['transactionAmount'])),
						'DetailType'				=> 'JournalEntryLineDetail',
						'JournalEntryLineDetail'	=> array(
							'PostingType'				=> 'Credit',
							'AccountRef'				=> array('value' => $lineAccRef),
						),
					);
					if($config['entityIdOnJournal']){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']					= $entityType;
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
					}
					if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
					}
					elseif($ChannelLocationMappings[$CustomLocationID][$channelId]){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId']);
					}
					elseif($channelMappings[$channelId]['account2ChannelId']){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
					}
					$ItemSequence++;
				}
				
				$allDebits		= $params['debits'];
				foreach($allDebits as $key => $journalLineTemp){
					$lineAccRef		= '';
					$journalNominal	= $journalLineTemp['nominalCode'];
					if($journalTypeCode == 'SG'){
						$lineAccRef	= $config['COGSDebitNominalSC'];
					}
					if($journalTypeCode == 'GO'){
						$lineAccRef	= $config['COGSDebitNominalSO'];
					}
					if($journalTypeCode == 'PG'){
						$lineAccRef	= $config['COGSDebitNominalSCPG'];
					}
					if($nominalMappings[$journalNominal]['account2NominalId']){
						$lineAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
						$lineAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
					}
					$InvoiceLineAdd[$ItemSequence]	= array(
						'LineNum'						=> ($ItemSequence + 1),
						'Description'					=> 'COGS Journal',
						'Amount'						=> sprintf("%.4f",($journalLineTemp['transactionAmount'])),
						'DetailType'					=> 'JournalEntryLineDetail',
						'JournalEntryLineDetail'		=> array(
							'PostingType'	=> 'Debit',
							'AccountRef'	=> array('value' => $lineAccRef),
						),
					);
					if($config['entityIdOnJournal']){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']					= $entityType;
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
					}
					if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
					}
					elseif($ChannelLocationMappings[$CustomLocationID][$channelId]){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId']);
					}
					elseif($channelMappings[$channelId]['account2ChannelId']){
						$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
					}
					$ItemSequence++;
				}
			}
			/**added on 08-08-2024 as confrimed by Kinnari**/
			$finalInvoiceLineNum = 0;
			$finalInvoiceLineAdd	= array();
			$finalInvoiceLineAddTemps	= array();
			foreach($InvoiceLineAdd as $InvoiceLineAdds){
				$classRef	= 'NA';
				if(isset($InvoiceLineAdds['JournalEntryLineDetail']['ClassRef']['value'])){
					$classRef	= $InvoiceLineAdds['JournalEntryLineDetail']['ClassRef']['value'];
				}
				$PostingType = $InvoiceLineAdds['JournalEntryLineDetail']['PostingType'];
				$AccountRef = $InvoiceLineAdds['JournalEntryLineDetail']['AccountRef']['value'];
				
				if(isset($finalInvoiceLineAddTemps[$PostingType][$AccountRef][$classRef])){
					$finalInvoiceLineAddTemps[$PostingType][$AccountRef][$classRef]['totalAmount'] = $finalInvoiceLineAddTemps[$PostingType][$AccountRef][$classRef]['totalAmount'] + $InvoiceLineAdds['Amount'];
				}
				else{
					$finalInvoiceLineAddTemps[$PostingType][$AccountRef][$classRef]['totalAmount'] = $InvoiceLineAdds['Amount'];
				}
			}
			
			foreach($finalInvoiceLineAddTemps as $PostingType => $finalInvoiceLineAddTemp){
				foreach($finalInvoiceLineAddTemp as $AccountRef => $finalInvoiceLineAdds){
					foreach($finalInvoiceLineAdds as $classRef => $journalLineTemp){
						$finalInvoiceLineAdd[$finalInvoiceLineNum]	= array(
							'LineNum'						=> ($finalInvoiceLineNum + 1),
							'Description'					=> 'COGS Journal',
							'Amount'						=> sprintf("%.4f",($journalLineTemp['totalAmount'])),
							'DetailType'					=> 'JournalEntryLineDetail',
							'JournalEntryLineDetail'		=> array(
								'PostingType'	=> $PostingType,
								'AccountRef'	=> array('value' => $AccountRef),
							),
						);
						
						if($config['entityIdOnJournal'] && $entityType && $qboContactID){
							$finalInvoiceLineAdd[$finalInvoiceLineNum]['JournalEntryLineDetail']['Entity']['Type']					= $entityType;
							$finalInvoiceLineAdd[$finalInvoiceLineNum]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
						}
						if($classRef != 'NA'){
							$finalInvoiceLineAdd[$finalInvoiceLineNum]['JournalEntryLineDetail']['ClassRef']	= array('value' => $classRef);
						}
						
						$finalInvoiceLineNum++;	
					}
				}
			}
			/**end***/
			if(!$finalInvoiceLineAdd){continue;}
			
			if($config['UseAsDocNumbersosc']){
				$account1FieldIds	= explode(".",$config['UseAsDocNumbersosc']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $MainOrderRowData[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$DocNumber	= $fieldValueTmps;
				}
				else{
					$DocNumber	= $MainOrderRowData['invoices']['0']['invoiceReference'];
				}
			}
			if(!$DocNumber){
				$DocNumber	= $bpOrderId;
			}
			if($finalInvoiceLineAdd){
				$DocNumber	= substr($DocNumber,0,21);
				$request	= array(
					'DocNumber'		=> $DocNumber,
					'TxnDate'		=> $taxDate,
					'ExchangeRate'	=> $exchangeRate,
					'CurrencyRef'	=> array('value' => $orderDatas['currencyCode']),
					'Line'			=> $finalInvoiceLineAdd,
				);
				if(!$request['ExchangeRate']){
					echo 'ExchangeRate Not found Line no - 522';continue;
					unset($request['ExchangeRate']);
				}
			}
			
			$url				= 'journalentry?minorversion=37';  
			$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdParams['Request data	: ']	= $request;
			$createdParams['Response data	: ']	= $results;
			$this->ci->db->update('cogs_journal',array('createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
			if(@$results['JournalEntry']['Id']){
				$this->ci->db->update('cogs_journal',array('status' => '1','createdJournalsId' => $results['JournalEntry']['Id'],'message' => ''),array('id' => $orderDatas['id']));
			}
		}
	}
}
?>