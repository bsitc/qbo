<?php
//qbo_demo		:	 PostConsolidation RefundReceipt Invoices to qbo
$dateResults	= array(1=> array('name'=> 'JAN','lastDate'=> '31',),2=> array('name'=> 'FEB','lastDate'=> '28','lastDate2'=> '29',),3=> array('name'=> 'MAR','lastDate'=> '31',),4=> array('name'=> 'APR','lastDate'=> '30',),5=> array('name'=> 'MAY','lastDate'=> '31',),6=> array('name'=> 'JUN','lastDate'=> '30',),7=> array('name'=> 'JUL','lastDate'=> '31',),8=> array('name'=> 'AUG','lastDate'=> '31',),9=> array('name'=> 'SEP','lastDate'=> '30',),10=> array('name'=> 'OCT','lastDate'=> '31',),11=> array('name'=> 'NOV','lastDate'=> '30',),12=> array('name'=> 'DEC','lastDate'=> '31',),);

$this->reInitialize();
$exchangeRates				= $this->getExchangeRate();
$enableRefundReceiptConsol	= $this->ci->globalConfig['enableRefundReceiptConsol'];
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	$exchangeRatetemp	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$datas	= $this->ci->db->get_where('refund_receipt',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	if(!$datas){continue;}
	
	$AggregationMappings		= array();
	if($this->ci->globalConfig['enableRefundReceiptConsol']){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_receiptConsol',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$AggregationMappings[$AggregationMappingsTemp['account1ChannelId']][strtolower($AggregationMappingsTemp['account1CurrencyId'])][$AggregationMappingsTemp['account1PaymentId']]	= $AggregationMappingsTemp;
				
			}
		}
		if(!$AggregationMappings){continue;}
	}
	
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
	$paymentMappings		= array();
	$GlobalPaymnetMehod	= '';
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			if(!$GlobalPaymnetMehod){
				$GlobalPaymnetMehod	= $paymentMappingsTemp['account1PaymentId'];
			}
			$paymentMappings[$paymentMappingsTemp['account1PaymentId']]	= $paymentMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	if($this->ci->globalConfig['enableNominalMapping']){
		$this->ci->db->reset_query();
		$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
		$nominalMappings		= array();
		$nominalChannelMappings	= array();
		if(!empty($nominalMappingTemps)){
			foreach($nominalMappingTemps as $nominalMappingTemp){
				if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
					$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
				}
				else{
					$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
				}
			}
		}
	}
	
	$productMappings		= array();
	if($this->ci->globalConfig['enableAdvanceTaxMapping']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	if($this->ci->globalConfig['enableTaxMapping']){
		$this->ci->db->reset_query();
		$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
		$taxMappings		= array();
		if($taxMappingsTemps){
			foreach($taxMappingsTemps as $taxMappingsTemp){
				if($this->ci->globalConfig['enableAdvanceTaxMapping']){
					if($taxMappingsTemp['stateName']){
						$isStateEnabled	= 1;
					}
					if($taxMappingsTemp['countryName']){
						$isStateEnabled = 1;
					}
				}
			}
			foreach($taxMappingsTemps as $taxMappingsTemp){
				$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
				if($taxMappingsTemp['stateName']){
					foreach($stateTemp as $Statekey => $stateTemps){
						$stateName			= strtolower(trim($stateTemps));
						$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
						$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
						if($this->ci->globalConfig['enableAdvanceTaxMapping']){
							if($isStateEnabled){
								if($account1ChannelId){
									$isChannelEnabled		= 1;
									$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
									foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
										$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
										$taxMappings[strtolower($key)]	= $taxMappingsTemp;
									}
								}
								else{
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'];
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
				}
				else{
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($isStateEnabled){
						if($account1ChannelId){
							$isChannelEnabled	= 1;
							$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
							foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}			
				}
				if(!$isStateEnabled){
					$key							= $taxMappingsTemp['account1TaxId'];
					$taxMappings[strtolower($key)]	= $taxMappingsTemp;
				}
			}
		}
	}
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard	= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	
	if($datas){
		$journalIds		= array();
		foreach($datas as $orderDatas){
			if($orderDatas['createOrderId']){continue;}
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'qbo'){
						if($paymentKey){
							$journalIds[]	= $paymentDetail['journalId'];
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		
		
		$dueAmountOnOrders	= array();
		$bpconfig				= '';
		$OrdersByChannelCurrencyTaxDate	= array();
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['paymentDetails']){continue;}
			
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$taxDate				= '';
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
			
			if(strtolower($rowDatas['orderPaymentStatus']) != 'paid'){
				continue;
			}
			$currentPaidAmount	= 0;
			foreach($paymentDetails as $key => $paymentDetail){
				if($paymentDetail['amount'] != 0){
					$currentPaidAmount += $paymentDetail['amount'];
				}
			}
			
			if(sprintf("%.4f",$currentPaidAmount) != sprintf("%.4f",$rowDatas['totalValue']['total'])){
				$dueAmountOnOrders[]	= $orderId;
				continue;
			}
			
			
			$taxDate				= $rowDatas['invoices'][0]['taxDate'];
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
			if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
				continue;
			}
			
			$paymentMethod		= '';
			$CurrencyRate		= $rowDatas['currency']['exchangeRate'];
			$paymentDate		= $taxDate;
			foreach($paymentDetails as $key => $paymentDetail){
				if($paymentDetail['paymentType'] == 'PAYMENT'){
					if($paymentDetail['amount'] > 0){
						if($paymentDetail['paymentMethod'] != $bpconfig['giftCardPayment']){
							$paymentMethod	= $paymentDetail['paymentMethod'];
							$paymentDate	= $paymentDetail['paymentDate'];
							if(isset($journalDatas[$paymentDetail['journalId']])){
								$CurrencyRate	= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
							}
							break;
						}
					}
				}
			}
			
			if(!$paymentMethod){
				$totalGiftPaymnet	= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if($paymentDetail['paymentMethod'] == $bpconfig['giftCardPayment']){
						$totalGiftPaymnet	+= $paymentDetail['amount'];
					}
				}
				if($totalGiftPaymnet >= $rowDatas['totalValue']['total']){
					$paymentMethod	= $GlobalPaymnetMehod;
					if(!$paymentMethod){
						$this->ci->db->update('refund_receipt',array('message' => 'Payment Method Not Found'),array('orderId' => $orderId));
						continue;
					}
				}
				else{
					$this->ci->db->update('refund_receipt',array('message' => 'Payment Method Not Found'),array('orderId' => $orderId));
					continue;
				}
			}
			if(!$paymentMappings[$paymentMethod]){
				$this->ci->db->update('refund_receipt',array('message' => 'Payment Mapping is Missing'),array('orderId' => $orderId));
				continue;
			}
			if((!$paymentMappings[$paymentMethod]['account2PaymentId']) OR (!$paymentMappings[$paymentMethod]['paymentValue'])){
				$this->ci->db->update('refund_receipt',array('message' => 'Payment Mapping is Missing'),array('orderId' => $orderId));
				continue;
			}
			
			if(!$AggregationMappings[$channelId][strtolower($CurrencyCode)][$paymentMethod] AND !$AggregationMappings[$channelId]['bpaccountingcurrency'][$paymentMethod]){
				continue;
			}
			
			$AggregationMappingData	= array();
			if($AggregationMappings){
				if($AggregationMappings[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'][$paymentMethod];
				}
				else{
					$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)][$paymentMethod];
				}
			}
			
			$consolFrequency		= $AggregationMappingData['consolFrequency'];
			if(!$consolFrequency){
				$consolFrequency	= 1;
			}
			
			$BPDateOffset	= (int)substr($paymentDate,23,3);
			$Acc2Offset		= 0;
			$diff			= $BPDateOffset - $Acc2Offset;
			$date			= new DateTime($paymentDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff	.= ' hour';
				$date->modify($diff);
			}
			$paymentDate		= $date->format('Ymd');
			
			//consolFrequency : 1 => Daily, 2 => Monthly
			if($consolFrequency == 1){
				$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
				$invoiceMonth	= date('m',strtotime($paymentDate));
				$currentMonth	= date('m');
				$currentDate	= date('d');
				
				if($paymentDate > $fetchTaxDate){
					continue;
				}
				
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency'][$paymentMethod]){
						$OrdersByChannelCurrencyTaxDate[$paymentDate][$channelId]['bpaccountingcurrency'][$paymentMethod][$orderId]		= $orderDatas;
					}
					elseif($AggregationMappings[$channelId][strtolower($CurrencyCode)][$paymentMethod]){
						$OrdersByChannelCurrencyTaxDate[$paymentDate][$channelId][strtolower($CurrencyCode)][$paymentMethod][$orderId]	= $orderDatas;
					}
				}
			}
			elseif($consolFrequency == 2){
				$invoiceMonth	= date('m',strtotime($paymentDate));
				$currentMonth	= date('m');
				
				if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
					continue;
				}
				
				$batchKey	= date('y-m',strtotime($paymentDate));
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId]['bpaccountingcurrency'][$paymentMethod][$orderId]		= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId][strtolower($CurrencyCode)][$paymentMethod][$orderId]		= $orderDatas;
					}
				}
			}
		}
		
		if($dueAmountOnOrders){
			$this->ci->db->where_in('orderId',$dueAmountOnOrders)->update('refund_receipt',array('message' => 'Order is not paid.'));
		}
		
		if($OrdersByChannelCurrencyTaxDate){
			foreach($OrdersByChannelCurrencyTaxDate as $taxDateAsPaymentDate => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $rootCurrency => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $consolPaymentMethod => $OrdersByChannels){
							$invalidConsolOrderIds	= array();
							$request				= array();
							$results				= array();
							$AggregationMapping		= array();
							$BrightpealBaseCurrency	= '';
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$consolFrequency		= 0;
							$BPChannelName			= '';
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)][$consolPaymentMethod];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('refund_receipt',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							$ChannelCustomer	= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef	= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount		= 0;
							$BPTotalAmountBase	= 0;
							$OrderCount			= 0;
							$invoiceLineCount	= 0;
							$linNumber			= 1;
							$request			= array();
							$InvoiceLineAdd		= array();
							$AllorderId			= array();
							$ProductArray		= array();
							$TotalTaxLines		= array();
							$ProductArrayCount	= 0;
							$totalItemDiscount	= 0;
							$exchangeRate		= 1;
							$exchangeRateSet	= 0;
							$totalGiftAmount	= 0;
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$bpconfig			= $this->ci->account1Config[$orderDatas['account1Id']];
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
									
									if(!$exchangeRateSet){
										$exchangeRate		= 1;
										$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									}
									
									foreach($paymentDetails as $key => $paymentDetail){
										if(!$exchangeRateSet){
											if($paymentDetail['paymentType'] == 'PAYMENT'){
												if($paymentDetail['amount'] > 0){
													if($paymentDetail['paymentMethod'] == $consolPaymentMethod){
														if(isset($journalDatas[$paymentDetail['journalId']])){
															$exchangeRateSet	= 1;
															$exchangeRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
														}
													}
												}
											}
										}
									}
									
									$countryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
												}
											}
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bundleParentID	= '';
										$isBundleChild	= $rowdatass['composition']['bundleChild'];
										if($isBundleChild){
											$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
										}
										
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										if($isBundleChild AND $bundleParentID){
											$bpTaxID	= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
										}
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $bpconfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										$taxMapping				= array();
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowdatass['rowValue']['rowNet']['value'] == 0){
												continue;
											}
										}
										
										if($kidsTaxCustomField){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
											}
										}
										
										$taxMappingKey		= $rowdatass['rowValue']['taxClassId'];
										$taxMappingKey1		= $rowdatass['rowValue']['taxClassId'];
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode;
											}
										}
										
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
										if($taxMapping){
											if($taxMapping['account2LineTaxId']){
												$bpTaxID 	= $taxMapping['account2LineTaxId'];
												if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'])){
													$bpTaxID  = $taxMapping['account2KidsLineTaxId'];
												}
											}
											else{
												$bpTaxID 	= $config['salesNoTaxCode'];
											}
										}
										else{
											$bpTaxID 	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										if(!$AggregationMapping['SendTaxAsLine']){
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpItemNonimalCode][$bpTaxID])){
														$ProductArray['discount'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$bpItemNonimalCode][$bpTaxID])){
																	$ProductArray['discountCoupon'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$bpItemNonimalCode][$bpTaxID][$customItemIdQbo])){
														$ProductArray['customItems'][$bpItemNonimalCode][$bpTaxID][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$bpItemNonimalCode][$bpTaxID][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
													}
												}
												else{
													if(isset($ProductArray[$bpItemNonimalCode][$bpTaxID])){
														$ProductArray[$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray[$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
													}
												}
												
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
										}
										else{
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice = $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice = round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpItemNonimalCode])){
														$ProductArray['discount'][$bpItemNonimalCode]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpItemNonimalCode]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$bpItemNonimalCode])){
																	$ProductArray['discountCoupon'][$bpItemNonimalCode]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$bpItemNonimalCode]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$bpItemNonimalCode][$customItemIdQbo])){
														$ProductArray['customItems'][$bpItemNonimalCode][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$bpItemNonimalCode][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
													}
												}
												else{
													if(isset($ProductArray['aggregationItem'][$bpItemNonimalCode])){
														$ProductArray['aggregationItem'][$bpItemNonimalCode]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['aggregationItem'][$bpItemNonimalCode]['TotalNetAmt']	= $rowNet;
													}
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpItemNonimalCode])){
													$ProductArray['shipping'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpItemNonimalCode])){
													$ProductArray['giftcard'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpItemNonimalCode])){
													$ProductArray['couponitem'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									
									
									foreach($paymentDetails as $key => $paymentDetail){
										if($paymentDetail['paymentType'] == 'PAYMENT'){
											if($paymentDetail['amount'] > 0){
												if($paymentDetail['paymentMethod'] == $bpconfig['giftCardPayment']){
													$totalGiftAmount	+= 	sprintf("%.4f",$paymentDetail['amount']);
												}
											}
										}
									}
									
									$dueDate		= $rowDatas['invoices']['0']['dueDate'];
									$taxDate		= $rowDatas['invoices']['0']['taxDate'];
									$orderId		= $orderDatas['orderId'];
									$AllorderId[]	= $orderId;
									$BPChannelName	= $orderDatas['channelName'];
									$CurrencyCode	= $rowDatas['currency']['orderCurrencyCode'];
									
									if($orderForceCurrency){
										$CurrencyCode	= strtoupper($brightpearlConfig['currencyCode']);
										$exchangeRate	= 1;
									}
									
									$BPTotalAmount	+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$OrderCount++;
								}
								if($ProductArray){
									if(!$AggregationMapping['SendTaxAsLine']){
										foreach($ProductArray as $keyproduct => $ProductArrayDatas){
											if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
												foreach($ProductArrayDatas as $keynominal1 => $ProductArrayDatas22){
													$IncomeAccountRef	= $config['IncomeAccountRef'];
													if((isset($nominalMappings[$keynominal1])) AND ($nominalMappings[$keynominal1]['account2NominalId'])){
														$IncomeAccountRef	= $nominalMappings[$keynominal1]['account2NominalId'];
													}
													if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$keynominal1])) AND ($nominalChannelMappings[strtolower($SalesChannel)][$keynominal1]['account2NominalId'])){
														$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$keynominal1]['account2NominalId'];
													}
													$nominalClassID		= '';
													if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($SalesChannel)][$keynominal1]))){
														$nominalClassID	= $nominalClassMapping[strtolower($SalesChannel)][$keynominal1]['account2ClassId'];
													}
													foreach($ProductArrayDatas22 as $keyproduct1 => $datas){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> "Consolidation of ".$OrderCount." invoices",
															'Amount'				=> sprintf("%.4f",$datas['TotalNetAmt']),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $AggregationMapping['AggregationItem']),
																'Qty'					=> 1,
																'UnitPrice'				=> sprintf("%.4f",$datas['TotalNetAmt']),
																'TaxCodeRef'			=> array('value'=> $keyproduct1),
																'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
															),
														);
														if($nominalClassID){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']			= array('value' => $nominalClassID);
														}
														if($keyproduct == 'shipping'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['shippingItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Shipping';
														}
														if($keyproduct == 'giftcard'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['giftCardItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'GiftCard';
														}
														if($keyproduct == 'couponitem'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon';
														}
														if($keyproduct == 'discount'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['discountItem'];
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Item Discount';
														}
														if($keyproduct == 'discountCoupon'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon Discount';
														}
														$invoiceLineCount++;
													}
												}
											}
											elseif($keyproduct == 'customItems'){
												foreach($ProductArrayDatas as $BPNominalCode => $ProductArrayDatasss){
													$IncomeAccountRef	= $config['IncomeAccountRef'];
													if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
														$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
													}
													if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($SalesChannel)][$BPNominalCode]['account2NominalId'])){
														$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$BPNominalCode]['account2NominalId'];
													}
													$nominalClassID		= '';
													if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($SalesChannel)][$BPNominalCode]))){
														$nominalClassID	= $nominalClassMapping[strtolower($SalesChannel)][$BPNominalCode]['account2ClassId'];
													}
													foreach($ProductArrayDatasss as $Taxkeyproduct1 => $ProductArrayDatass){
														foreach($ProductArrayDatass as $customItemId	=> $customItemIdTemp){
															$InvoiceLineAdd[$invoiceLineCount]	= array(
																'LineNum'				=> $linNumber++,
																'Description'			=> 'Special Item',
																'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
																'DetailType'			=> 'SalesItemLineDetail',
																'SalesItemLineDetail'	=> array(
																	'ItemRef'				=> array('value' => $customItemId),
																	'Qty'					=> 1,
																	'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
																	'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
																	'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
																),
															);
															if($nominalClassID){
																$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
															}
															$invoiceLineCount++;
														}
													}
												}
											}
											else{
												$IncomeAccountRef	= $config['IncomeAccountRef'];
												if((isset($nominalMappings[$keyproduct])) AND ($nominalMappings[$keyproduct]['account2NominalId'])){
													$IncomeAccountRef	= $nominalMappings[$keyproduct]['account2NominalId'];
												}
												if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$keyproduct])) AND ($nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]['account2NominalId'])){
													$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]['account2NominalId'];
												}
												$nominalClassID		= '';
												if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($SalesChannel)][$keyproduct]))){
													$nominalClassID	= $nominalClassMapping[strtolower($SalesChannel)][$keyproduct]['account2ClassId'];
												}
												foreach($ProductArrayDatas as $keyproduct => $ProductArrayDatas22){
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> "Consolidation of ".$OrderCount." invoices",
														'Amount'				=> sprintf("%.4f",$ProductArrayDatas22['TotalNetAmt']),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $AggregationMapping['AggregationItem']),
															'Qty'					=> 1,
															'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatas22['TotalNetAmt']),
															'TaxCodeRef'			=> array('value'=> $keyproduct),
															'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
														),
													);
													if($nominalClassID){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
													}
													$invoiceLineCount++;
												}
											}
										}
									}
									else{
										foreach($ProductArray as $keyproduct => $ProductArrayDatas22){
											foreach($ProductArrayDatas22 as $keynominal1 => $ProductArrayDatas){
												$IncomeAccountRef	= $config['IncomeAccountRef'];
												if((isset($nominalMappings[$keynominal1])) AND ($nominalMappings[$keynominal1]['account2NominalId'])){
													$IncomeAccountRef	= $nominalMappings[$keynominal1]['account2NominalId'];
												}
												if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$keynominal1])) AND ($nominalChannelMappings[strtolower($SalesChannel)][$keynominal1]['account2NominalId'])){
													$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$keynominal1]['account2NominalId'];
												}
												$nominalClassID		= '';
												if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($SalesChannel)][$keynominal1]))){
													$nominalClassID	= $nominalClassMapping[strtolower($SalesChannel)][$keynominal1]['account2ClassId'];
												}
												if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> "Consolidation of ".$OrderCount." invoices",
														'Amount'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $AggregationMapping['AggregationItem']),
															'Qty'					=> 1,
															'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
															'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
															'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
														),
													);
													if($nominalClassID){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']			= array('value' => $nominalClassID);
													}
													if($keyproduct == 'shipping'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['shippingItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Shipping';
													}
													if($keyproduct == 'giftcard'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['giftCardItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
													}
													if($keyproduct == 'couponitem'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon';
													}
													if($keyproduct == 'discount'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['discountItem'];
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Item Discount';
													}
													if($keyproduct == 'discountCoupon'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon Discount';
													}
													$invoiceLineCount++;
												}
												elseif($keyproduct == 'customItems'){
													foreach($ProductArrayDatas as $customItemId	=> $customItemIdTemp){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> 'Special Item',
															'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $customItemId),
																'Qty'					=> 1,
																'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
																'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
																'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
															),
														);
														$invoiceLineCount++;
													}
												}
												elseif($keyproduct == 'allTax'){
													if($ProductArrayDatas['TotalNetAmt'] == 0){
														continue;
													}
													$mappedtaxItem	= '';
													$mappedtaxItem	= $AggregationMapping['defaultTaxItem'];
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> 'Total Tax Amount',
														'Amount'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $mappedtaxItem),
															'Qty'					=> 1,
															'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
															'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
															'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
														),
													);
													$invoiceLineCount++;
												}
											}
										}
									}
								}
							}
							if($InvoiceLineAdd){
								$taxDate		= date('Y-m-d',strtotime($taxDateAsPaymentDate));
								$CurrencyCode1	= strtoupper($rootCurrency);
								if($consolFrequency == 1){
									$sendTaxDate		= $taxDate;
									$sendDueDate		= $taxDate;
									$RefForInvoice		= $taxDate;
									$ReferenceNumber	= date('Ymd',strtotime($RefForInvoice)).'-'.$uniqueInvoiceRef.'-'.$CurrencyCode1;
								}
								else{
									$isLeap				= 0;
									$invMonth			= (int)substr($taxDateAsPaymentDate,3,2);
									$invYear			= (int)substr($taxDateAsPaymentDate,0,2);
									$invYearFull		= '20'.$invYear;
									
									if(((int)$invYearFull % 4) == 0){
										if(((int)$invYearFull % 100) == 0){
											if(((int)$invYearFull % 400) == 0){
												$isLeap	= 1;
											}
											else{
												$isLeap	= 0;
											}
										}
										else{
											$isLeap	= 1;
										}
									}
									else{
										$isLeap	= 0;
									}
									
									$invDate	= $dateResults[$invMonth]['lastDate'];
									if($invMonth == 2){
										if($isLeap){
											$invDate	= $dateResults[$invMonth]['lastDate2'];										
										}
									}
									$invMonthName	= $dateResults[$invMonth]['name'];
									if(strlen($invMonth) == 1){
										$invMonth	= (string)('0'.$invMonth);
									}
									$sendTaxDate		= $invYearFull.'-'.$invMonth.'-'.$invDate;
									$sendDueDate		= $sendTaxDate;
									$RefForInvoice		= $invMonthName.$invYear;
									$ReferenceNumber	= $RefForInvoice.'-'.$uniqueInvoiceRef.'-'.$CurrencyCode1;
								}
								$aggreagationID			= uniqid((strtoupper(trim($BPChannelName))).'-');
								$ReferenceNumber		= substr($ReferenceNumber,0,21);
								
								$invoiceLineAddNew		= array();
								$invoiceLineFormatted	= array();
								$newItemLineCount		= 0;
								foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $AggregationMapping['AggregationItem']){
										$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['Description'] == 'Special Item'){
										$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['discountItem']){
										$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['couponItem']){
										$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['shippingItem']){
										$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $AggregationMapping['defaultTaxItem']){
										$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['giftCardItem']){
										$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
										continue;
									}
								}
								ksort($invoiceLineAddNew);
								foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
									foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
										$invoiceLineFormatted[$newItemLineCount]			= $invoiceLineAddNewTempTemp;
										$invoiceLineFormatted[$newItemLineCount]['LineNum']	= ($newItemLineCount + 1);
										$newItemLineCount++;
									}
								}
								if($invoiceLineFormatted){
									$InvoiceLineAdd		= $invoiceLineFormatted;
									$invoiceLineCount	= $newItemLineCount;
									$linNumber			= ($newItemLineCount + 1);
								}
								
								if($totalGiftAmount > 0){
									$InvoiceLineAdd[$invoiceLineCount] = array(
										'LineNum'				=> $linNumber++,
										'Description'			=> 'Gift Card Payment',
										'Amount'				=> ((-1) * (sprintf("%.4f",$totalGiftAmount))),
										'DetailType'			=> 'SalesItemLineDetail',
										'SalesItemLineDetail'	=> array(
											'ItemRef'				=> array('value' => $config['giftCardItem']),
											'Qty' 					=> 1,
											'UnitPrice' 			=> ((-1) * (sprintf("%.4f",$totalGiftAmount))),
											'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
										),	
									);
									$invoiceLineCount++;
								}
								
								$DepositToAccountRef	= $paymentMappings[$consolPaymentMethod]['account2PaymentId'];
								$PaymentMethodRef		= $paymentMappings[$consolPaymentMethod]['paymentValue'];
								
								//chnageReq added on 8th fab 2023 req by Tushar
								if($clientcode == 'aimeeqbo'){
									foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
										if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
											$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Shipping Charges';
										}
									}
								}
								
								foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
									if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
										unset($InvoiceLineAdd[$lineItemIdNewTemp]['Description']);
									}
								}
								
								$lineLevelClassId	= '';
								if((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($SalesChannel) AND (isset($channelMappings[$SalesChannel]))){
									$lineLevelClassId	= $channelMappings[$SalesChannel]['account2ChannelId'];
								}
								if($lineLevelClassId){
									foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
										if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
											continue;
										}
										else{
											$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
										}
									}
								}
								
								$request				= array(
									'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
									'Line'					=> $InvoiceLineAdd,
									'CurrencyRef'			=> array('value' => $CurrencyCode1),
									'TxnDate'				=> $sendTaxDate,
									'DocNumber'				=> $ReferenceNumber,
									'CustomerRef'			=> array('value' => $ChannelCustomer),
									'PaymentMethodRef'		=> array('value' => $PaymentMethodRef),
									'PrintStatus'			=> 'NotSet',
									'ExchangeRate'			=> sprintf("%.4f",(1 / $exchangeRate)),
								);
								if(isset($channelMappings[$SalesChannel])){
									$request['ClassRef']	= array('value' => $channelMappings[$SalesChannel]['account2ChannelId']);
								}
								
								if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
									$exRate = $this->getQboExchangeRateByDb($account2Id,$CurrencyCode1,$config['defaultCurrrency'],$sendTaxDate);
									if($exRate){
										$request['ExchangeRate'] = $exRate;
									}
									else{
										$exRate = $exchangeRatetemp[strtolower($CurrencyCode1)][strtolower($config['defaultCurrrency'])]['Rate'];
										if($exRate){
											$request['ExchangeRate'] = $exRate;
										}
										else{
											echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
											unset($request['ExchangeRate']);
										}
									}
								}
								
								if($request AND $InvoiceLineAdd){
									if($disableSalesPosting){
										$createdRowData	= array(
											'Request data	: '	=> $request,
											'Response data	: '	=> 'POSTING_IS_DISABLED',
										);
										$this->ci->db->where_in('orderId',$AllorderId)->update('refund_receipt',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
									}
									else{
										$url		= 'refundreceipt?minorversion=37';
										$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
										$createdRowData	= array(
											'Request data	: '	=> $request,
											'Response data	: '	=> $results,
										);
										$this->ci->db->where_in('orderId',$AllorderId)->update('refund_receipt',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
									}
								}
								else{
									continue;
								}
								if((isset($results['RefundReceipt']['Id']))){
									$consolPostOptions	= 0;
									if($disableCOGSPosting){
										$consolPostOptions	= 2;
									}
									
									$this->ci->db->where_in('orderId',$AllorderId)->update('refund_receipt',array('uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['RefundReceipt']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'consolPostOptions'=> $consolPostOptions));
									
									$QBOTotalAmount			= $results['RefundReceipt']['TotalAmt'];
									if($orderForceCurrency){
										$NetRoundOff	= $BPTotalAmountBase - $QBOTotalAmount;
									}
									else{
										$NetRoundOff	= $BPTotalAmount - $QBOTotalAmount;
									}
									$RoundOffCheck		= abs($NetRoundOff);
									$RoundOffCheck		= sprintf("%.4f",$RoundOffCheck);
									$RoundOffApplicable = 0;
									if($RoundOffCheck != 0){
										if($RoundOffCheck < 0.99){
											$RoundOffApplicable = 1;
										}
										if($RoundOffApplicable){
											$InvoiceLineAdd[$invoiceLineCount] = array(
												'LineNum'				=> $linNumber++,
												'Description'			=> 'RoundOffItem',
												'Amount'				=> sprintf("%.4f",$NetRoundOff),
												'DetailType'			=> 'SalesItemLineDetail',
												'SalesItemLineDetail'	=> array(
													'ItemRef' 				=> array(
														'value'					=> $config['roundOffItem']
													),
													'Qty' 					=> 1,
													'UnitPrice' 			=> sprintf("%.4f",$NetRoundOff),
													'TaxCodeRef' 			=> array(
														'value'					=> $config['salesNoTaxCode'] 
													),
												),
											);
											$request['Line']		= $InvoiceLineAdd;
											$request['SyncToken']	= 0;
											$request['Id']			= $results['RefundReceipt']['Id'];
											$url					= 'refundreceipt?minorversion=37';  
											$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
											$createdRowData['Rounding Request data	: ']	= $request;
											$createdRowData['Rounding Response data	: ']	= $results2;
											if((isset($results['RefundReceipt']['Id']))){
												$this->ci->db->where_in('orderId',$AllorderId)->update('refund_receipt',array('status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['RefundReceipt']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'createdRowData' => json_encode($createdRowData)));
											}
											else{
												$this->ci->db->where_in('orderId',$AllorderId)->update('refund_receipt',array('message' => 'Unablle to add Rounding Item' , 'createdRowData' => json_encode($createdRowData)));
											}
										}
									}
								}
								else{
									if($disableSalesPosting){
										$fakecreateOrderId	= uniqid("QBO_ID_").strtotime("now");
										$this->ci->db->where_in('orderId',$AllorderId)->update('refund_receipt',array('uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c'),'consolPostOptions'=> 1));
									}
								}
							}
						}
					}
				}
			}
		}
	}
}   