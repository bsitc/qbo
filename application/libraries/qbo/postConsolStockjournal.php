<?php
//qbo_demo
$this->reInitialize();
$enableStockAdjustment			= $this->ci->globalConfig['enableStockAdjustment'];
$enableConsolStockAdjustment	= $this->ci->globalConfig['enableConsolStockAdjustment'];
$exchangeRates					= $this->getExchangeRate();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($enableStockAdjustment){continue;}
	if(!$enableConsolStockAdjustment){continue;}
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){
			continue;
		}
	}
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];

	$this->ci->db->reset_query();
	$StockDatas	= $this->ci->db->get_where('stock_journals',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(!$StockDatas){
		continue;
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '', 'account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	if($StockDatas){
		$aggregatedStockDatas	= array();
		foreach($StockDatas as $StockData){
			$journalId			= $StockData['journalId'];
			$currencyCode		= $StockData['currencyCode'];
			$taxDate			= $StockData['taxDate'];
			$stockJournalParam	= json_decode($StockData['params'],true);
			$taxDate			= $stockJournalParam['taxDate'];
			$BPDateOffset		= (int)substr($taxDate,23,3);
			$Acc2Offset			= 0;
			$diff				= $BPDateOffset - $Acc2Offset;
			$date				= new DateTime($taxDate);
			$BPTimeZone			= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff	.= ' hour';
				$date->modify($diff);
			}
			$taxDate		= $date->format('Ymd');
			
			$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
			if($taxDate > $fetchTaxDate){
				continue;
			}
			$aggregatedStockDatas[$taxDate][$currencyCode][$journalId]	= $StockData;
		}
		
		if($aggregatedStockDatas){
			foreach($aggregatedStockDatas as $consolTaxDate => $aggregatedStockDatasTemp1){
				foreach($aggregatedStockDatasTemp1 as $consolCurrency => $aggregatedStockDatasTemp2){
					$consolJournalRequest	= array();
					$consolJournalResults	= array();
					$consolItemLineRequest	= array();
					$lineItmeNominalArray	= array();
					$processedJournalIds	= array();
					$lineItemSequence		= 0;
					$StockCount				= 0;
					$exchangeRate			= 0;
					foreach($aggregatedStockDatasTemp2 as $journalId => $aggregatedStockData){
						$config1			= $this->ci->account1Config[$aggregatedStockData['account1Id']];
						$exRate				= '';
						$stockJournalParam	= json_decode($aggregatedStockData['params'],true);
						$exchangeRate		= $stockJournalParam['exchangeRate'];
						$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
						
						if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
							$exRate = $this->getQboExchangeRateByDb($account2Id,$consolCurrency,$config['defaultCurrrency'],$consolTaxDate);
							if($exRate){
								$exchangeRate	= $exRate;
							}
							else{
								$exRate = $exchangeRatesAll[strtolower($consolCurrency)][strtolower($config['defaultCurrrency'])]['Rate'];
								if($exRate){
									$exchangeRate	= $exRate;
								}
								else{
									$exchangeRate	= 0;
								}
							}
						}
						
						$credits			= $stockJournalParam['credits'];
						$debits				= $stockJournalParam['debits'];
						if(!$credits['0']){
							$credits	= array($credits);
						}
						if(!$debits['0']){
							$debits		= array($debits);
						}
						foreach($credits as $credit){
							$credit['transactionAmount']	= ((-1) * $credit['transactionAmount']);
							if(($credit['nominalCode'] >= 1000) AND ($credit['nominalCode'] <= 1999)){
								$defaultNominal	= $config['AssetAccountRef'];
							}
							else{
								$defaultNominal	= $config['accRefForStockAdjustment'];
							}
							if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
								$defaultNominal	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
							}
							elseif($nominalChannelMappings[$credit['nominalCode']]['account2NominalId']){
								$defaultNominal	= $nominalChannelMappings[$credit['nominalCode']]['account2NominalId'];
							}
							
							if(isset($lineItmeNominalArray['allLine'][$defaultNominal]['netAmt'])){
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	+= $credit['transactionAmount'];
							}
							else{
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	= $credit['transactionAmount'];
							}
						}
						foreach($debits as $debit){
							$debit['transactionAmount']		= ((1) * $debit['transactionAmount']);
							if(($debit['nominalCode'] >= 1000) AND ($debit['nominalCode'] <= 1999)){
								$defaultNominal	= $config['AssetAccountRef'];
							}
							else{
								$defaultNominal	= $config['accRefForStockAdjustment'];
							}
							if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
								$defaultNominal	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
							}
							elseif($nominalChannelMappings[$debit['nominalCode']]['account2NominalId']){
								$defaultNominal	= $nominalChannelMappings[$debit['nominalCode']]['account2NominalId'];
							}
							if(isset($lineItmeNominalArray['allLine'][$defaultNominal]['netAmt'])){
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	+= $debit['transactionAmount'];
							}
							else{
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	= $debit['transactionAmount'];
							}
						}
						$processedJournalIds[]	= $journalId;
						$StockCount++;
					}
					
					if($lineItmeNominalArray){
						foreach($lineItmeNominalArray as $lineType => $lineItmeNominalArrayTemp){
							foreach($lineItmeNominalArrayTemp as $lineNominal => $lineItmeNominalArrayTemp2){
								$postingType	= 'Debit';
								$processedAmt	= $lineItmeNominalArrayTemp2['netAmt'];
								if($processedAmt < 0){
									$postingType	= 'Credit';
									$processedAmt	= ((-1) * ($processedAmt));
								}
								
								$consolItemLineRequest[$lineItemSequence]	= array(
									'LineNum'					=> ($lineItemSequence + 1),
									'Description'				=> 'Consol of '.$StockCount.' stock adjustments',
									'Amount'					=> sprintf("%.4f",($processedAmt)),
									'DetailType'				=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'	=> array(
										'PostingType'				=> $postingType,
										'AccountRef'				=> array('value' => $lineNominal),
									),
								);
								$lineItemSequence++;
							}
						}
						if($consolItemLineRequest){
							$DocNumber				= $consolTaxDate.'-SA-'.$consolCurrency;
							$consolJournalRequest	= array(
								'DocNumber'				=> $DocNumber,
								'TxnDate'				=> date('Y-m-d',strtotime($consolTaxDate)),
								'ExchangeRate'			=> $exchangeRate,
								'CurrencyRef'			=> array('value' => strtoupper($consolCurrency)),
								'Line'					=> $consolItemLineRequest,
							);
							if(!$consolJournalRequest['ExchangeRate']){
								echo 'ExchangeRate Not found Line no - 184';continue;
								unset($consolJournalRequest['ExchangeRate']);
							}
							
							$url					= 'journalentry?minorversion=37';
							$consolJournalResults	= $this->getCurl($url, 'POST', json_encode($consolJournalRequest), 'json', $account2Id)[$account2Id];
							$createdParams			= array(
								'Request data	: ' => $consolJournalRequest,
								'Response data	: ' => $consolJournalResults,
							);
							if($consolJournalResults['JournalEntry']['Id']){
								$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('stock_journals',array('createdParams' => json_encode($createdParams), 'created_journalId' => $consolJournalResults['JournalEntry']['Id'], 'InvoiceNumber' => $DocNumber, 'status' => '1'));
							}
							else{
								$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('stock_journals',array('createdParams' => json_encode($createdParams)));
							}
						}
					}
				}
			}
		}
	}
}