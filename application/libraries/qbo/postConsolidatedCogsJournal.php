<?php
$this->reInitialize();
$enableCOGSJournals	= $this->ci->globalConfig['enableCOGSJournals'];
$exchangeRates		= $this->getExchangeRate();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableCOGSJournals){continue;}
	if($clientcode == 'bbhugmeqbo'){if(!$exchangeRates){continue;}}
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$datas	= $this->ci->db->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if((!is_array($datas)) OR (empty($datas))){continue;}
	
	$allCogsOrderIds		= array();
	$allCogsOrderIds		= array_column($datas,'orderId');
	$allCogsOrderIds		= array_filter($allCogsOrderIds);
	$allCogsOrderIds		= array_unique($allCogsOrderIds);
	
	$allSaveSalesCredits	= array();
	$allSaveSalesCreditRef	= array();
    $this->ci->db->reset_query();
	if((is_array($allCogsOrderIds)) AND (!empty($allCogsOrderIds))){
		$this->ci->db->where_in('orderId',$allCogsOrderIds);
	}
	$allSaveSalesCreditsDatas	= $this->ci->db->select('account1Id,orderId,createOrderId,rowData,sendInAggregation,aggregationId,invoiceRef,qboContactID')->get_where('sales_credit_order',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation' => 1))->result_array();
	if((is_array($allSaveSalesCreditsDatas)) AND  (!empty($allSaveSalesCreditsDatas))){
		foreach($allSaveSalesCreditsDatas as $allSaveSalesCreditsData){
			if(!$allSaveSalesCreditsData['sendInAggregation']){continue;}
			if($allSaveSalesCreditsData['createOrderId']){
				$rowData	= json_decode($allSaveSalesCreditsData['rowData'],true);
				$allSaveSalesCredits[$allSaveSalesCreditsData['createOrderId']][]					= $allSaveSalesCreditsData['orderId'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['invoiceRef']		= $allSaveSalesCreditsData['invoiceRef'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['taxDate']		= $rowData['invoices'][0]['taxDate'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['qboContactID']	= $allSaveSalesCreditsData['qboContactID'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']][$allSaveSalesCreditsData['orderId']]['shippingStatus'] = $rowData['stockStatusCode'];
			}
		}
	}
	
	$allSaveRefundReceiptRef	= array();
	$allSaveRefundReceipt		= array();
	$this->ci->db->reset_query();
	if((is_array($allCogsOrderIds)) AND (!empty($allCogsOrderIds))){
		$this->ci->db->where_in('orderId',$allCogsOrderIds);
	}
	$allSaveRefundReceiptDatas	= $this->ci->db->select('account1Id,orderId,createOrderId,rowData,sendInAggregation,aggregationId,invoiceRef')->get_where('refund_receipt',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation' => 1))->result_array();
	if((is_array($allSaveRefundReceiptDatas)) AND  (!empty($allSaveRefundReceiptDatas))){
		foreach($allSaveRefundReceiptDatas as $allSaveRefundReceiptData){
			if(!$allSaveRefundReceiptData['sendInAggregation']){continue;}
			if($allSaveRefundReceiptData['createOrderId']){
				$rowData	= json_decode($allSaveRefundReceiptData['rowData'],true);
				$allSaveRefundReceipt[$allSaveRefundReceiptData['createOrderId']][]						= $allSaveRefundReceiptData['orderId'];
				$allSaveRefundReceiptRef[$allSaveRefundReceiptData['createOrderId']]['invoiceRef']		= $allSaveRefundReceiptData['invoiceRef'];
				$allSaveRefundReceiptRef[$allSaveRefundReceiptData['createOrderId']]['taxDate']			= $rowData['invoices'][0]['taxDate'];
				$allSaveRefundReceiptRef[$allSaveRefundReceiptData['createOrderId']][$allSaveRefundReceiptData['orderId']]['shippingStatus']	= $rowData['stockStatusCode'];
			}
		}
	}
	
	
	$allSaveSalesRef			= array();
	$allSaveSalesOrders			= array();
	$this->ci->db->reset_query();
	if((is_array($allCogsOrderIds)) AND (!empty($allCogsOrderIds))){
		$this->ci->db->where_in('orderId',$allCogsOrderIds);
	}
	$allSaveSalesOrdersDatas	= $this->ci->db->select('account1Id,orderId,createOrderId,rowData,sendInAggregation,aggregationId,invoiceRef, qboContactID')->get_where('sales_order',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation' => 1))->result_array();
	if((is_array($allSaveSalesOrdersDatas)) AND  (!empty($allSaveSalesOrdersDatas))){
		foreach($allSaveSalesOrdersDatas as $allSaveSalesOrdersData){
			if(!$allSaveSalesOrdersData['sendInAggregation']){continue;}
			if($allSaveSalesOrdersData['createOrderId']){
				$rowData	= json_decode($allSaveSalesOrdersData['rowData'],true);
				$allSaveSalesOrders[$allSaveSalesOrdersData['createOrderId']][]				= $allSaveSalesOrdersData['orderId'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['invoiceRef']	= $allSaveSalesOrdersData['invoiceRef'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['taxDate']		= $rowData['invoices'][0]['taxDate'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['qboContactID']	= $allSaveSalesOrdersData['qboContactID'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']][$allSaveSalesOrdersData['orderId']]['shippingStatus'] = $rowData['shippingStatusCode'];
			}
		}
	}
	
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if((is_array($nominalMappingTemps)) AND (!empty($nominalMappingTemps))){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$channelMappings		= array();
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if((is_array($channelMappingsTemps)) AND (!empty($channelMappingsTemps))){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	$postDatasTmpSO	= array();
	$postDatasTmpSC	= array();
	$postDatasTmpRR	= array();
	$jounralDataSO	= array();
	$jounralDataSC	= array();
	$jounralDataRR	= array();
	if((is_array($datas)) AND (!empty($datas))){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['createdJournalsId']){continue;}
			if($orderDatas['OrderType'] == 'SO'){
				$jounralDataSO[$orderDatas['orderId']][]	= $orderDatas;
			}
			if($orderDatas['OrderType'] == 'SC'){
				$jounralDataSC[$orderDatas['orderId']][]	= $orderDatas;
			}
			if($orderDatas['OrderType'] == 'RR'){
				$jounralDataRR[$orderDatas['orderId']][]	= $orderDatas;
			}
		}
		if((is_array($allSaveSalesOrders)) AND (!empty($allSaveSalesOrders))){
			foreach($allSaveSalesOrders as $createOrderId => $bpOrderIds){
				foreach($bpOrderIds as $bpOrderId){
					if((is_array($jounralDataSO)) AND (!empty($jounralDataSO)) AND (isset($jounralDataSO[$bpOrderId]))){
						foreach($jounralDataSO[$bpOrderId] as $key22 => $individualJournalData){
							$newJournalTaxDate	= date('Ymd',strtotime($individualJournalData['taxDate']));
							$postDatasTmpSO[$createOrderId][$newJournalTaxDate][][0] = $jounralDataSO[$bpOrderId][$key22];
						}
					}
				}
			}
		}
		if((is_array($allSaveSalesCredits)) AND (!empty($allSaveSalesCredits))){
			foreach($allSaveSalesCredits as $createOrderId => $bpOrderIds){
				foreach($bpOrderIds as $bpOrderId){
					if((is_array($jounralDataSC)) AND (!empty($jounralDataSC)) AND (isset($jounralDataSC[$bpOrderId]))){
						foreach($jounralDataSC[$bpOrderId] as $key22 => $individualJournalData){
							$newJournalTaxDate	= date('Ymd',strtotime($individualJournalData['taxDate']));
							$postDatasTmpSC[$createOrderId][$newJournalTaxDate][][0] = $jounralDataSC[$bpOrderId][$key22];
						}
					}
				}
			}
		}
		if((is_array($allSaveRefundReceipt)) AND (!empty($allSaveRefundReceipt))){
			foreach($allSaveRefundReceipt as $createOrderId => $bpOrderIds){
				foreach($bpOrderIds as $bpOrderId){
					if((is_array($jounralDataRR)) AND (!empty($jounralDataRR)) AND (isset($jounralDataRR[$bpOrderId]))){
						foreach($jounralDataRR[$bpOrderId] as $key22 => $individualJournalData){
							$newJournalTaxDate	= date('Ymd',strtotime($individualJournalData['taxDate']));
							$postDatasTmpRR[$createOrderId][$newJournalTaxDate][][0] = $jounralDataRR[$bpOrderId][$key22];
						}
					}
				}
			}
		}
		
		if((is_array($postDatasTmpSO)) AND (!empty($postDatasTmpSO))){
			foreach($postDatasTmpSO as $createOrderId => $journalDataTmpNew){
				foreach($journalDataTmpNew as $JournalTaxDateConsol => $journalDataTmp){
					$taxDate			    = date('Y-m-d',strtotime($JournalTaxDateConsol));
					$ChannelMappingId	    = 0;		$ItemSequence			= 0;		$exchangeRate		    = 1;
					$DocNumber			    = '';		$currency			    = '';		$channelId			    = '';
					$JournalRequest		    = array();	$JournalLines		    = array();	$AllDebits			    = array();	$AllCredits				= array();
					$DebitByNominalArray	= array();	$CreditByNominalArray	= array();	$allProcessedJournalsId	= array();	$allProcessedOrdersId	= array();
					
					$SalesOrderAllInfo		= $this->ci->db->order_by('orderId','desc')->get_where('sales_order',array('createOrderId' => $createOrderId))->row_array();
					if((!is_array($SalesOrderAllInfo)) OR (empty($SalesOrderAllInfo))){continue;}
					
					if($SalesOrderAllInfo['consolPostOptions'] == 2){
						$this->ci->db->where_in('orderId',$allSaveSalesOrders[$createOrderId])->update('cogs_journal',array('status' => 4, 'createdJournalsId' => '', 'message' => 'Posting Is Disable', 'isConsolidated' => 1));
						$this->ci->db->where_in('orderId',$allSaveSalesOrders[$createOrderId])->update('sales_credit_order',array('COGSPosted' => 1));
						continue;
					}
					
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							$config1		= $this->ci->account1Config[$journalDatas['account1Id']];
							$JournalType	= $journalDatas['journalTypeCode'];
							$params			= json_decode($journalDatas['params'],true);
							if(!$channelId){
								$channelId	= $journalDatas['channelId'];
							}
							
							$AllCredits	= $params['credits'];
							foreach($AllCredits as $key => $CreditsData){
								$CreditByNominalArray[$JournalType][$CreditsData['nominalCode']] += $CreditsData['transactionAmount'];
							}
							$AllDebits	= $params['debits'];
							foreach($AllDebits as $key => $DebitsData){
								$DebitByNominalArray[$JournalType][$DebitsData['nominalCode']] += $DebitsData['transactionAmount'];
							}
							$exchangeRate		= $params['exchangeRate'];
							$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
							$currency 			= $journalDatas['currencyCode'];
							if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
								$exRate = $this->getQboExchangeRateByDb($account2Id,$currency,$config['defaultCurrrency'],$taxDate);
								if($exRate){
									$exchangeRate		= $exRate;
								}
								else{
									$exRate = $exchangeRatesAll[strtolower($currency)][strtolower($config['defaultCurrrency'])]['Rate'];
									if($exRate){
										$exchangeRate		= $exRate;
									}
									else{
										$exchangeRate		= 0;
									}
								}
							}
							
							$orderShippingStatus		= '';
							$orderShippingStatus		= $allSaveSalesRef[$createOrderId][$journalDatas['orderId']]['shippingStatus'];
							if($orderShippingStatus == 'ASS'){
								$allProcessedOrdersId[]	= $journalDatas['orderId'];
							}
							$allProcessedJournalsId[]	= $journalDatas['journalsId'];
						}
					}
					if($DebitByNominalArray AND $CreditByNominalArray){
						if(isset($channelId)){
							if($channelMappings[$channelId]['account2ChannelId']){
								$ChannelMappingId	= $channelMappings[$channelId]['account2ChannelId'];
							}
						}
						$DocNumber			= $allSaveSalesRef[$createOrderId]['invoiceRef'];
						$qboContactID		= '';
						$qboContactID		= $allSaveSalesRef[$createOrderId]['qboContactID'];
						$entityType			= 'Customer';
						if($config['entityIdOnJournal']){
							if(!$qboContactID){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('message' => 'Contact ID is Missing'));
								continue;
							}
						}
						
						$ItemSequence	= 0;
						
						foreach($DebitByNominalArray as $TypeOfJournal => $DebitByNominalArrayData){
							foreach($DebitByNominalArrayData as $NominalOfJournal => $DebitAmt){
								if(!$DebitAmt){
									continue;
								}
								$debitAccRef	= '';
								if($TypeOfJournal == 'GO'){
									$debitAccRef	= $config['COGSDebitNominalSO'];
									if($nominalMappings[$NominalOfJournal]['account2NominalId']){
										$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
										$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
									}
								}
								$JournalLines[$ItemSequence]	= array(
									'LineNum'						=> ($ItemSequence + 1),
									'Description'					=> $TypeOfJournal.' COGS Journal',
									'Amount'						=> sprintf("%.4f",($DebitAmt)),
									'DetailType'					=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'		=> array(
										'PostingType'					=> 'Debit',
										'AccountRef'					=> array('value' => $debitAccRef),
									),								
								);
								if($config['entityIdOnJournal']){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']				= $entityType;
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
								}
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$NominalOfJournal]))){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$NominalOfJournal]['account2ClassId']);
								}
								elseif($ChannelMappingId){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelMappingId);
								}
								$ItemSequence++;
							}
						}
						foreach($CreditByNominalArray as $TypeOfJournal => $CreditByNominalArrayData){
							foreach($CreditByNominalArrayData as $NominalOfJournal => $CreditAmt){
								if(!$CreditAmt){
									continue;
								}
								$creditAccRef	= '';
								if($TypeOfJournal == 'GO'){
									$creditAccRef	= $config['COGSCreditNominalSO'];
									if($nominalMappings[$NominalOfJournal]['account2NominalId']){
										$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
										$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
									}
								}
								$JournalLines[$ItemSequence]	= array(
									'LineNum'						=> ($ItemSequence + 1),
									'Description'					=> $TypeOfJournal.' COGS Journal',
									'Amount'						=> sprintf("%.4f",($CreditAmt)),
									'DetailType'					=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'		=> array(
										'PostingType'					=> 'Credit',
										'AccountRef'					=> array('value' => $creditAccRef),
									),
								);
								if($config['entityIdOnJournal']){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']				= $entityType;
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
								}
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$NominalOfJournal]))){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$NominalOfJournal]['account2ClassId']);
								}
								elseif($ChannelMappingId){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelMappingId);
								}
								$ItemSequence++;
							}
						}
						if($JournalLines){
							
							if($clientcode == 'unvqbom'){
								$taxDate			= $allSaveSalesRef[$createOrderId]['taxDate'];
								//taxdate chanages
								$BPDateOffset		= (int)substr($taxDate,23,3);
								$QBOoffset			= 0;
								$diff				= $BPDateOffset - $QBOoffset;
								$date				= new DateTime($taxDate);
								$BPTimeZone			= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff > 0){
									$diff			.= ' hour';
									$date->modify($diff);
								}
								$taxDate			= $date->format('Y-m-d');
							}
							
							
							$JournalRequest	= array(
								'DocNumber'		=> $DocNumber,
								'TxnDate'		=> $taxDate,					
								'ExchangeRate'	=> $exchangeRate,					
								'CurrencyRef'	=> array('value' => $currency),
								'Line'			=> $JournalLines,
							);
							if(!$JournalRequest['ExchangeRate']){
								echo 'ExchangeRate Not found Line no - 342';continue;
								unset($JournalRequest['ExchangeRate']);
							}
						}
						if($JournalRequest){
							$JournalUrl		= 'journalentry?minorversion=37';  
							$JournalResults	= $this->getCurl($JournalUrl, 'POST', json_encode($JournalRequest), 'json', $account2Id)[$account2Id];
							$createdParams['Request data	: ']	= $JournalRequest;
							$createdParams['Response data	: ']	= $JournalResults;
							$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('createdParams' => json_encode($createdParams)));
							if(@$JournalResults['JournalEntry']['Id']){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('status' => 1, 'createdJournalsId' => $JournalResults['JournalEntry']['Id'], 'message' => '', 'isConsolidated' => 1));
								
								if(!empty($allProcessedOrdersId)){
									$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('sales_order',array('COGSPosted' => 1));
								}
							}
						}
					}
				}
			}
		}
		
		if($postDatasTmpSC){
			foreach($postDatasTmpSC as $createOrderId => $journalDataTmpNew){
				foreach($journalDataTmpNew as $JournalTaxDateConsol => $journalDataTmp){
					//$taxDate			    = date('Y-m-d');
					$taxDate			    = date('Y-m-d',strtotime($JournalTaxDateConsol));
					$ChannelMappingId	    = 0;
					$ItemSequence			= 0;
					$exchangeRate		    = 1;
					$DocNumber			    = '';
					$currency			    = '';
					$channelId			    = '';
					$JournalRequest		    = array();
					$JournalLines		    = array();
					$AllDebits			    = array();
					$AllCredits				= array();
					$DebitByNominalArray	= array();
					$CreditByNominalArray	= array();
					$allProcessedJournalsId	= array();
					$allProcessedOrdersId	= array();
					
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							$config1			= $this->ci->account1Config[$journalDatas['account1Id']];
							$JournalType		= $journalDatas['journalTypeCode'];
							$params				= json_decode($journalDatas['params'],true);
							$channelId 			= $journalDatas['channelId'];
							
							$AllCredits	= $params['credits'];
							foreach($AllCredits as $key => $CreditsData){
								$CreditByNominalArray[$JournalType][$CreditsData['nominalCode']] += $CreditsData['transactionAmount'];
							}
							$AllDebits	= $params['debits'];
							foreach($AllDebits as $key => $DebitsData){
								$DebitByNominalArray[$JournalType][$DebitsData['nominalCode']] += $DebitsData['transactionAmount'];
							}
							
							$exchangeRate		= $params['exchangeRate'];
							$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
							$currency 			= $journalDatas['currencyCode'];
							if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
								$exRate = $this->getQboExchangeRateByDb($account2Id,$currency,$config['defaultCurrrency'],$taxDate);
								if($exRate){
									$exchangeRate		= $exRate;
								}
								else{
									$exRate = $exchangeRatesAll[strtolower($currency)][strtolower($config['defaultCurrrency'])]['Rate'];
									if($exRate){
										$exchangeRate		= $exRate;
									}
									else{
										$exchangeRate		= 0;
									}
								}
							}
							
							$orderShippingStatus		= '';
							$orderShippingStatus		= $allSaveSalesCreditRef[$createOrderId][$journalDatas['orderId']]['shippingStatus'];
							if($orderShippingStatus == 'SCA'){
								$allProcessedOrdersId[]	= $journalDatas['orderId'];
							}
							$allProcessedJournalsId[]	= $journalDatas['journalsId'];
						}
					}
					if($DebitByNominalArray AND $CreditByNominalArray){
						if(isset($channelId)){
							if($channelMappings[$channelId]['account2ChannelId']){
								$ChannelMappingId	= $channelMappings[$channelId]['account2ChannelId'];
							}
						}
						$DocNumber		= $allSaveSalesCreditRef[$createOrderId]['invoiceRef'];
						$qboContactID	= '';
						$qboContactID	= $allSaveSalesCreditRef[$createOrderId]['qboContactID'];
						$entityType		= 'Customer';
						if($config['entityIdOnJournal']){
							if(!$qboContactID){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('message' => 'Contact ID is Missing'));
								continue;
							}
						}
						
						$ItemSequence	= 0;
						foreach($DebitByNominalArray as $TypeOfJournal => $DebitByNominalArrayData){
							foreach($DebitByNominalArrayData as $NominalOfJournal => $DebitAmt){
								if(!$DebitAmt){
									continue;
								}
								$debitAccRef	= $config['COGSDebitNominalSC'];
								if($TypeOfJournal == 'PG'){
									$debitAccRef	= $config['COGSDebitNominalSCPG'];
								}
								if($nominalMappings[$NominalOfJournal]['account2NominalId']){
									$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
									$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
								}
								$JournalLines[$ItemSequence]	= array(
									'LineNum'						=> ($ItemSequence + 1),
									'Description'					=> $TypeOfJournal.' COGS Journal',
									'Amount'						=> sprintf("%.4f",($DebitAmt)),
									'DetailType'					=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'		=> array(
										'PostingType'					=> 'Debit',
										'AccountRef'					=> array('value' => $debitAccRef),
									),
								);
								if($config['entityIdOnJournal']){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']				= $entityType;
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
								}
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$NominalOfJournal]))){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$NominalOfJournal]['account2ClassId']);
								}
								elseif($ChannelMappingId){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelMappingId);
								}
								$ItemSequence++;
							}
						}
						foreach($CreditByNominalArray as $TypeOfJournal => $CreditByNominalArrayData){
							foreach($CreditByNominalArrayData as $NominalOfJournal => $CreditAmt){
								if(!$CreditAmt){
									continue;
								}
								$creditAccRef	= $config['COGSCreditNominalSCPG'];
								if($TypeOfJournal == 'SG'){
									$creditAccRef	= $config['COGSCreditNominalSC'];
								}
								if($nominalMappings[$NominalOfJournal]['account2NominalId']){
									$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
									$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
								}
								$JournalLines[$ItemSequence]	= array(
									'LineNum'						=> ($ItemSequence + 1),
									'Description'					=> $TypeOfJournal.' COGS Journal',
									'Amount'						=> sprintf("%.4f",($CreditAmt)),
									'DetailType'					=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'		=> array(
										'PostingType'					=> 'Credit',
										'AccountRef'					=> array('value' => $creditAccRef),
									),
								);
								if($config['entityIdOnJournal']){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['Type']				= $entityType;
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['Entity']['EntityRef']['value']	= $qboContactID;
								}
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$NominalOfJournal]))){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$NominalOfJournal]['account2ClassId']);
								}
								elseif($ChannelMappingId){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelMappingId);
								}
								$ItemSequence++;
							}
						}
						if($JournalLines){
							
							if($clientcode == 'unvqbom'){
								$taxDate			= $allSaveSalesCreditRef[$createOrderId]['taxDate'];
								//taxdate chanages
								$BPDateOffset		= (int)substr($taxDate,23,3);
								$QBOoffset			= 0;
								$diff				= $BPDateOffset - $QBOoffset;
								$date				= new DateTime($taxDate);
								$BPTimeZone			= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff > 0){
									$diff			.= ' hour';
									$date->modify($diff);
								}
								$taxDate			= $date->format('Y-m-d');
							}
							
							
							$JournalRequest	= array(
								'DocNumber'		=> $DocNumber,
								'TxnDate'		=> $taxDate,					
								'ExchangeRate'	=> $exchangeRate,					
								'CurrencyRef'	=> array('value' => $currency),
								'Line'			=> $JournalLines,
							);
							if(!$JournalRequest['ExchangeRate']){
								echo 'ExchangeRate Not found Line no - 509';continue;
								unset($JournalRequest['ExchangeRate']);
							}
						}
						if($JournalRequest){
							$JournalUrl		= 'journalentry?minorversion=37';  
							$JournalResults	= $this->getCurl($JournalUrl, 'POST', json_encode($JournalRequest), 'json', $account2Id)[$account2Id];
							$createdParams	= array(
								'Request data	: '	=> $JournalRequest,
								'Response data	: '	=> $JournalResults,
							);
							$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('createdParams' => json_encode($createdParams)));
							if(@$JournalResults['JournalEntry']['Id']){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('status' => 1, 'createdJournalsId' => $JournalResults['JournalEntry']['Id'], 'message' => '', 'isConsolidated' => 1));
								
								if(!empty($allProcessedOrdersId)){
									$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('sales_credit_order',array('COGSPosted' => 1));
								}
							}
						}
					}
				}
			}
		}
		
		if($postDatasTmpRR){
			foreach($postDatasTmpRR as $createOrderId => $journalDataTmpNew){
				foreach($journalDataTmpNew as $JournalTaxDateConsol => $journalDataTmp){
					//$taxDate			    = date('Y-m-d');
					$taxDate			    = date('Y-m-d',strtotime($JournalTaxDateConsol));
					$ChannelMappingId	    = 0;
					$ItemSequence			= 0;
					$exchangeRate		    = 1;
					$DocNumber			    = '';
					$currency			    = '';
					$channelId			    = '';
					$JournalRequest		    = array();
					$JournalLines		    = array();
					$AllDebits			    = array();
					$AllCredits				= array();
					$DebitByNominalArray	= array();
					$CreditByNominalArray	= array();
					$allProcessedJournalsId	= array();
					$allProcessedOrdersId	= array();
					
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							$config1			= $this->ci->account1Config[$journalDatas['account1Id']];
							$JournalType		= $journalDatas['journalTypeCode'];
							$params				= json_decode($journalDatas['params'],true);
							$channelId 			= $journalDatas['channelId'];
							
							$AllCredits	= $params['credits'];
							foreach($AllCredits as $key => $CreditsData){
								$CreditByNominalArray[$JournalType][$CreditsData['nominalCode']] += $CreditsData['transactionAmount'];
							}
							$AllDebits	= $params['debits'];
							foreach($AllDebits as $key => $DebitsData){
								$DebitByNominalArray[$JournalType][$DebitsData['nominalCode']] += $DebitsData['transactionAmount'];
							}
							
							$exchangeRate		= $params['exchangeRate'];
							$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
							$currency 			= $journalDatas['currencyCode'];
							if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
								$exRate = $this->getQboExchangeRateByDb($account2Id,$currency,$config['defaultCurrrency'],$taxDate);
								if($exRate){
									$exchangeRate		= $exRate;
								}
								else{
									$exRate = $exchangeRatesAll[strtolower($currency)][strtolower($config['defaultCurrrency'])]['Rate'];
									if($exRate){
										$exchangeRate		= $exRate;
									}
									else{
										$exchangeRate		= 0;
									}
								}

							}
							
							$orderShippingStatus		= '';
							$orderShippingStatus		= $allSaveRefundReceiptRef[$createOrderId][$journalDatas['orderId']]['shippingStatus'];
							if($orderShippingStatus == 'SCA'){
								$allProcessedOrdersId[]	= $journalDatas['orderId'];
							}
							$allProcessedJournalsId[]	= $journalDatas['journalsId'];
						}
					}
					if($DebitByNominalArray AND $CreditByNominalArray){
						if(isset($channelId)){
							if($channelMappings[$channelId]['account2ChannelId']){
								$ChannelMappingId	= $channelMappings[$channelId]['account2ChannelId'];
							}
						}
						$DocNumber		= $allSaveRefundReceiptRef[$createOrderId]['invoiceRef'];
						$ItemSequence	= 0;
						foreach($DebitByNominalArray as $TypeOfJournal => $DebitByNominalArrayData){
							foreach($DebitByNominalArrayData as $NominalOfJournal => $DebitAmt){
								if(!$DebitAmt){
									continue;
								}
								$debitAccRef	= $config['COGSDebitNominalSC'];
								if($TypeOfJournal == 'PG'){
									$debitAccRef	= $config['COGSDebitNominalSCPG'];
								}
								if($nominalMappings[$NominalOfJournal]['account2NominalId']){
									$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
									$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
								}
								$JournalLines[$ItemSequence]	= array(
									'LineNum'						=> ($ItemSequence + 1),
									'Description'					=> $TypeOfJournal.' COGS Journal',
									'Amount'						=> sprintf("%.4f",($DebitAmt)),
									'DetailType'					=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'		=> array(
										'PostingType'					=> 'Debit',
										'AccountRef'					=> array('value' => $debitAccRef),
									),
								);
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$NominalOfJournal]))){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$NominalOfJournal]['account2ClassId']);
								}
								elseif($ChannelMappingId){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelMappingId);
								}
								$ItemSequence++;
							}
						}
						foreach($CreditByNominalArray as $TypeOfJournal => $CreditByNominalArrayData){
							foreach($CreditByNominalArrayData as $NominalOfJournal => $CreditAmt){
								if(!$CreditAmt){
									continue;
								}
								$creditAccRef	= $config['COGSCreditNominalSCPG'];
								if($TypeOfJournal == 'SG'){
									$creditAccRef	= $config['COGSCreditNominalSC'];
								}
								if($nominalMappings[$NominalOfJournal]['account2NominalId']){
									$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
									$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
								}
								$JournalLines[$ItemSequence]	= array(
									'LineNum'						=> ($ItemSequence + 1),
									'Description'					=> $TypeOfJournal.' COGS Journal',
									'Amount'						=> sprintf("%.4f",($CreditAmt)),
									'DetailType'					=> 'JournalEntryLineDetail',
									'JournalEntryLineDetail'		=> array(
										'PostingType'					=> 'Credit',
										'AccountRef'					=> array('value' => $creditAccRef),
									),
								);
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$NominalOfJournal]))){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$NominalOfJournal]['account2ClassId']);
								}
								elseif($ChannelMappingId){
									$JournalLines[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $ChannelMappingId);
								}
								$ItemSequence++;
							}
						}
						if($JournalLines){
							$JournalRequest	= array(
								'DocNumber'		=> $DocNumber,
								'TxnDate'		=> $taxDate,					
								'ExchangeRate'	=> $exchangeRate,					
								'CurrencyRef'	=> array('value' => $currency),
								'Line'			=> $JournalLines,
							);
							if(!$JournalRequest['ExchangeRate']){
								unset($JournalRequest['ExchangeRate']);
							}
						}
						if($JournalRequest){
							$JournalUrl		= 'journalentry?minorversion=37';  
							$JournalResults	= $this->getCurl($JournalUrl, 'POST', json_encode($JournalRequest), 'json', $account2Id)[$account2Id];
							$createdParams	= array(
								'Request data	: '	=> $JournalRequest,
								'Response data	: '	=> $JournalResults,
							);
							$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('createdParams' => json_encode($createdParams)));
							if(@$JournalResults['JournalEntry']['Id']){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('status' => 1, 'createdJournalsId' => $JournalResults['JournalEntry']['Id'], 'message' => '', 'isConsolidated' => 1));
								
								if(!empty($allProcessedOrdersId)){
									$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('refund_receipt',array('COGSPosted' => 1));
								}
							}
						}
					}
				}
			}
		}
	}
}