<?php
//QBO		:	post Consolidated Amazon fee by editing the QBO sales invoice
$this->reInitialize();
$excludedOrderIds			= array();
$excludedqboIds				= array();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$enableAmazonFees			= $this->ci->globalConfig['enableamazonfee'];
$disableConsolSOpayments	= $this->ci->globalConfig['disableConsolSOpayments'];
$exchangeRates				= $this->getExchangeRate();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableAmazonFees){
		continue;
	}
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){
			continue;
		}
	}
	$config	= $this->accountConfig[$account2Id];
	
	$allAggregatedSales	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id, 'status > ' => '0' , 'IsJournalposted' => '0' , 'sendInAggregation' => 1))->result_array();
	if(!$allAggregatedSales){
		continue;
	}
	
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
					}
				}
			}
		}
		if(!$AggregationMappings AND !$AggregationMappings2){
			continue;
		}
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if(!empty($channelMappingsTemps)){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	
	if($allAggregatedSales){
		$ConsolFeeItem				= array();
		$aggregatedJournalsData		= array();
		$SalesByAggregationIdData	= array();
		$AggregationGroup			= array();
		$ConsolForceCurrencyMapping	= array();
		foreach($allAggregatedSales as $allAggregatedSalesData){
			if($allAggregatedSalesData['status'] == 4){continue;}
			$AggregationGroup[$allAggregatedSalesData['aggregationId']][]	= $allAggregatedSalesData;
		}
		foreach($AggregationGroup as $aggregationId => $AggregationGroupSales){
			$allSearchOrderIds	= array();
			foreach($AggregationGroupSales as $AggregationGroupSalesData){
				$bpconfig				= $this->ci->account1Config[$AggregationGroupSalesData['account1Id']];
				$orderId				= $AggregationGroupSalesData['orderId'];
				$rowDatas				= json_decode($AggregationGroupSalesData['rowData'],true);
				$channelId				= $rowDatas['assignment']['current']['channelId'];
				$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$orderExchangeRate		= $rowDatas['currency']['exchangeRate'];
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				$paymentDetails			= json_decode($AggregationGroupSalesData['paymentDetails'],true);
				$ConsolAPIFieldValueID	= '';
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				if($ConsolAPIFieldValueID){
					$CustomFieldValueID	= $ConsolAPIFieldValueID;
				}
				
				if(!$paymentDetails){continue;}
				
				$allSearchOrderIds[]	= $orderId;
				
				if($enableAggregation){
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
							$ConsolForceCurrencyMapping[$aggregationId]	= $orderExchangeRate;
							$ConsolFeeItem[$aggregationId]	= $AggregationMappings[$channelId]['bpaccountingcurrency']['ConsolidationFeeItem'];
						}
						elseif($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsJournalAggregated']){
							$ConsolFeeItem[$aggregationId]	= $AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['ConsolidationFeeItem'];
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsJournalAggregated']){
							$ConsolForceCurrencyMapping[$aggregationId]	= $orderExchangeRate;
							$ConsolFeeItem[$aggregationId]	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ConsolidationFeeItem'];
						}
						elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
							$ConsolForceCurrencyMapping[$aggregationId]	= $orderExchangeRate;
							$ConsolFeeItem[$aggregationId]	= $AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ConsolidationFeeItem'];
						}
						elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsJournalAggregated']){
							$ConsolFeeItem[$aggregationId]	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['ConsolidationFeeItem'];
						}
						elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsJournalAggregated']){
							$ConsolFeeItem[$aggregationId]	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['ConsolidationFeeItem'];
						}
					}
				}
			}
			
			if(!empty($allSearchOrderIds)){
				$allSearchOrderIds	= array_filter($allSearchOrderIds);
				$allSearchOrderIds	= array_unique($allSearchOrderIds);
				$journalDatasTemps	= $this->ci->db->where_in('orderId',$allSearchOrderIds)->get_where('amazon_ledger',array('status ' => 0,'account2Id' => $account2Id))->result_array();
				if(!empty($journalDatasTemps)){
					$aggregatedJournalsData[$aggregationId][]	= $journalDatasTemps;
				}
			}
		}
		
		if($aggregatedJournalsData){
			foreach($aggregatedJournalsData as $aggregationId => $aggregatedJournalsDatas){
				$allProcessedOrdersId	= array();
				$allAmazonfeesIDs		= array();
				$EditRequest			= array();
				$EditResponse			= array();
				$totalFeeByNominals		= array();
				$totalFeeAmount			= 0;
				$feeSentSuccessFully	= 0;
				$newBpOrderId			= '';
				foreach($aggregatedJournalsDatas as $allJournalsTemp){
					foreach($allJournalsTemp as $allJournals){
						$allAmazonfeesIDs[]		= $allJournals['journalsId'];
						$allProcessedOrdersId[]	= $allJournals['orderId'];
						$newBpOrderId			= $allJournals['orderId'];
						$totalFeeAmount			+= $allJournals['amount'];
						
						//new code added after xoFetti req.
						$amazonFeeParams		= json_decode($allJournals['params'], true);
						$allDebits				= $amazonFeeParams['debits'];
						if(!empty($allDebits)){
							foreach($allDebits as $allDebitsTemps){
								if(isset($totalFeeByNominals[$allDebitsTemps['nominalCode']])){
									$totalFeeByNominals[$allDebitsTemps['nominalCode']]	= ($totalFeeByNominals[$allDebitsTemps['nominalCode']] + $allDebitsTemps['transactionAmount']);
								}
								else{
									$totalFeeByNominals[$allDebitsTemps['nominalCode']]	= ($allDebitsTemps['transactionAmount']);
								}
							}
						}
					}
				}
				
				if($totalFeeAmount AND (!empty($totalFeeByNominals))){
					if(!$ConsolFeeItem[$aggregationId]){
						continue;
					}
					if($ConsolForceCurrencyMapping[$aggregationId]){
						$newExchangeRate	= $ConsolForceCurrencyMapping[$aggregationId];
						$totalFeeAmount		= (($totalFeeAmount) * ((1) / ($newExchangeRate)));
						$totalFeeAmount		= sprintf("%.4f",$totalFeeAmount);
					}
					$totalFeeAmount	= 0;
					foreach($totalFeeByNominals as $amazonFeeNominal => $totalAmtByNominalCode){
						$SalesInfo	= $this->ci->db->get_where('sales_order',array('orderId' => $newBpOrderId, 'aggregationId' => $aggregationId, 'status >' => 0))->row_array();
						if($SalesInfo){
							$amazonConsolFeeItem	= $ConsolFeeItem[$aggregationId];
							$amazonChannelId		= $SalesInfo['channelId'];
							$QBOInvoiceID			= $SalesInfo['createOrderId'];
							$createdRowData			= json_decode($SalesInfo['createdRowData'],true);
							
							if(isset($defaultItemMapping[$amazonChannelId][$amazonFeeNominal])){
								$amazonConsolFeeItem	= $defaultItemMapping[$amazonChannelId][$amazonFeeNominal];
							}
							if($ConsolForceCurrencyMapping[$aggregationId]){
								$newExchangeRate			= $ConsolForceCurrencyMapping[$aggregationId];
								$totalAmtByNominalCode		= sprintf("%.4f",$totalAmtByNominalCode);
								$totalAmtByNominalCode		= (($totalAmtByNominalCode) * ((1) / ($newExchangeRate)));
								$totalAmtByNominalCode		= sprintf("%.4f",$totalAmtByNominalCode);
							}
							$totalFeeAmount		+= $totalAmtByNominalCode;
							
							$query				= "select * from invoice where id = '".$QBOInvoiceID."'";
							$suburl				= "query?minorversion=4&query=".rawurlencode($query);
							$qboOrderInfos		= @$this->getCurl($suburl, 'GET', '', 'json',$account2Id)[$account2Id]; 
							$qboLineNumber		= 1;
							foreach($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'] as $key => $qbLine){
								if($qbLine['DetailType'] == 'SubTotalLineDetail'){
									unset($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][$key]);
								}
								if(@$qbLine['LineNum'] > $qboLineNumber){
									$qboLineNumber	= $qbLine['LineNum'];
								}
							}
							
							$amazonFeeClassRef	= '';
							if(isset($channelMappings[$amazonChannelId])){
								$amazonFeeClassRef	= $channelMappings[$amazonChannelId]['account2ChannelId'];
							}
							
							$qboLineNumber++;
							$qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'])]	= array(
								'LineNum'						=> (count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line']) + 1),
								'Description'					=> 'Amazon Fees',
								'Amount'						=> sprintf("%.4f",((-1) * $totalAmtByNominalCode)),
								'DetailType'					=> 'SalesItemLineDetail',
								'SalesItemLineDetail'			=> array(
									'ItemRef' 						=> array('value'=> $amazonConsolFeeItem),
									'UnitPrice' 					=> sprintf("%.4f",((-1) * $totalAmtByNominalCode)),
									'Qty' 							=> 1,
									'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode']),
									'ClassRef'						=> array('value' => $amazonFeeClassRef),
								),
							);
							
							$rqInvoice		= $qboOrderInfos['QueryResponse']['Invoice']['0'];
							$EditRequest	= array(
								'Id'			=> $rqInvoice['Id'],
								'DocNumber'		=> $rqInvoice['DocNumber'],
								'CustomerRef'	=> array('value' => $rqInvoice['CustomerRef']['value']),
								'BillEmail'		=> $rqInvoice['BillEmail'],
								'SyncToken'		=> $rqInvoice['SyncToken'],
								'domain'		=> $rqInvoice['domain'],
								'TxnDate'		=> $rqInvoice['TxnDate'],
								'DueDate'		=> $rqInvoice['DueDate'],
								'BillAddr'		=> $rqInvoice['BillAddr'],
								'ShipAddr'		=> $rqInvoice['ShipAddr'],
								'ExchangeRate'	=> $rqInvoice['ExchangeRate'],
								'CurrencyRef'	=> $rqInvoice['CurrencyRef'],
								'ClassRef'		=> $rqInvoice['ClassRef'],
								'Line'			=> $rqInvoice['Line'],
							);
							if(!$EditRequest['ExchangeRate']){
								unset($EditRequest['ExchangeRate']);
							}
							if($EditRequest){
								$EditUrl		= 'invoice?minorversion=37';
								$EditResponse	= $this->getCurl($EditUrl, 'POST', json_encode($EditRequest), 'json', $account2Id)[$account2Id];
								$createdRowData['Edit Invoice Request data	:'.date('Ymd').'-'.$amazonFeeNominal]	= $EditRequest;
								$createdRowData['Edit Invoice Response data	:'.date('Ymd').'-'.$amazonFeeNominal]	= $EditResponse;
								$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('sales_order',array('createdRowData' => json_encode($createdRowData)));
								
								if($EditResponse['Invoice']['Id']){
									$feeSentSuccessFully	= 1;
								}
							}
						}
					}
					if($feeSentSuccessFully == 1){
						$amazonBatchID	= uniqid();
						$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('sales_order',array('amazonBatchID' => $amazonBatchID, 'IsJournalposted' => 1, 'totalFeeAmount' => $totalFeeAmount));
						$this->ci->db->where_in('journalsId',$allAmazonfeesIDs)->update('amazon_ledger',array('status' => '1', 'createdJournalsId' => 'Linked with Invoice', 'message' => 'JournalAttachedInCosolidationInvoice', 'paymentCreated' => 1));
					}
				}
			}
		}
	}
}

foreach($this->accountDetails as $account2Id => $accountDetails){
	if($disableConsolSOpayments){
		continue;
	}
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){
			continue;
		}
	}
	
	$config			= $this->accountConfig[$account2Id];
	$exchangeRate	= $exchangeRates[$account2Id];
	
	$allAggregatedSales	= $this->ci->db->order_by('orderId','desc')->get_where('sales_order',array('account2Id' => $account2Id, 'status > ' => '0' , 'IsJournalposted' => '1' , 'sendInAggregation' => 1, 'isPaymentCreated' => 0, 'paymentDetails <>' => '', 'paymentSynced' => 0))->result_array();
	if(!$allAggregatedSales){
		continue;
	}
	
	$this->ci->db->reset_query();
	$allPendingFees	= array();
	$allSentFees	= array();
	$journalDatasTemps	= $this->ci->db->select('id, orderId, status, journalsId')->get_where('amazon_ledger',array('account2Id' => $account2Id))->result_array();
	if($journalDatasTemps){
		foreach($journalDatasTemps as $journalDatasTemp){
			if($journalDatasTemp['status'] == 0){
				$allPendingFees[$journalDatasTemp['orderId']]	= $journalDatasTemp;
			}
			elseif($journalDatasTemp['status'] == 1){
				$allSentFees[$journalDatasTemp['orderId']]		= $journalDatasTemp;
			}
		}
	}
	
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
					}
				}
			}
		}
		if(!$AggregationMappings AND !$AggregationMappings2){
			continue;
		}
	}
	
	$this->ci->db->reset_query();
	/* $paymentMappingsTemps		= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
	$paymentMappings			= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$paymentMappings[$paymentMappingsTemp['account1PaymentId']]			= $paymentMappingsTemp;
		}
	} */
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	$journalIds = array();
	if($allAggregatedSales){
		foreach($allAggregatedSales as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['createOrderId']){continue;}
			
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'qbo'){
						if($paymentDetail['status'] == '0'){
							if($paymentKey){
								$journalIds[]	= $paymentDetail['journalId'];
							}
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);	
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		
		$ordersByAggregation		= array();
		$ConsolForceCurrencyMapping	= array();
		foreach($allAggregatedSales as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$orderExchangeRate		= $rowDatas['currency']['exchangeRate'];
			$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			$ConsolAPIFieldValueID	= '';
				
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			if($enableAggregation){
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
								$ConsolForceCurrencyMapping[$orderDatas['aggregationId']]		= $orderExchangeRate;
								$ordersByAggregation[$orderDatas['aggregationId']][$orderDatas['amazonBatchID']][$orderId]	= $orderDatas;
							}
						}
					}
					elseif($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsPaymentAggregated']){
						if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsJournalAggregated']){
								$ordersByAggregation[$orderDatas['aggregationId']][$orderDatas['amazonBatchID']][$orderId]	= $orderDatas;
							}
						}
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsJournalAggregated']){
								$ConsolForceCurrencyMapping[$orderDatas['aggregationId']]		= $orderExchangeRate;
								$ordersByAggregation[$orderDatas['aggregationId']][$orderDatas['amazonBatchID']][$orderId]	= $orderDatas;
							}
						}
					}
					elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
								$ConsolForceCurrencyMapping[$orderDatas['aggregationId']]		= $orderExchangeRate;
								$ordersByAggregation[$orderDatas['aggregationId']][$orderDatas['amazonBatchID']][$orderId]	= $orderDatas;
							}
						}
					}
					elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsPaymentAggregated']){
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsJournalAggregated']){
								$ordersByAggregation[$orderDatas['aggregationId']][$orderDatas['amazonBatchID']][$orderId]	= $orderDatas;
							}
						}
					}
					elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsJournalAggregated']){
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsJournalAggregated']){
								$ordersByAggregation[$orderDatas['aggregationId']][$orderDatas['amazonBatchID']][$orderId]	= $orderDatas;
							}
						}
					}
				}
			}
		}
		
		if($ordersByAggregation){
			foreach($ordersByAggregation as $aggregationId => $allorderDatasTemp){
				foreach($allorderDatasTemp as $amazonBatchID => $allorderDatas){
					$IsJournalPendings		= 0;
					$PaymentData			= array();
					$allSentIDS				= array();
					$request				= array();
					$Positiveamount			= 0;
					$Negativeamount			= 0;
					$sentableAmount			= 0;
					$QBOCustomerID			= '';
					$QBOTotalAmount			= 0;
					$aggregatedInvoiceTotal	= 0;
					$paymentDate			= date('Y-m-d');
					$CountExchangeRate		= 0;
					$ExchangeRateTotal		= 0;
					$CurrencyRate			= 1;
					$PaymentMethodRef		= $config['PayType'];
					$DepositToAccountRef	= $config['DepositToAccountRef'];
					$totalFeeAmount			= 0;
					$QBOorderId				= '';
					$paymentSetCurrency		= '';
					foreach($allorderDatas as $orderId => $orderDatas){
						$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
						$orderId				= $orderDatas['orderId'];
						$rowDatas				= json_decode($orderDatas['rowData'],true);
						$channelId				= $rowDatas['assignment']['current']['channelId'];
						$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
						$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
						
						$createOrderId			= $orderDatas['createOrderId'];
						$QBOorderId				= $createOrderId;
						$account1Id				= $orderDatas['account1Id'];
						$config1				= $this->ci->account1Config[$account1Id];
						$createdRowData			= json_decode($orderDatas['createdRowData'],true);
						$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
						if(!$paymentDetails){
							continue;
						}
						$QBOTotalAmount			+= $rowDatas['totalValue']['total'];
						$aggregatedInvoiceTotal	= $createdRowData['Response data	: ']['Invoice']['TotalAmt'];
						$totalFeeAmount			= $orderDatas['totalFeeAmount'];
						$aggregatedInvoiceTotal	= ($aggregatedInvoiceTotal - $totalFeeAmount);
						if(($allPendingFees[$orderId]) OR (!$allSentFees[$orderId])){
							if(!in_array($orderId, $excludedOrderIds)){
								$IsJournalPendings	= 1;
							}
						}
						$paymentSetCurrency		= '';
						foreach($paymentDetails as $key => $paymentDetail){
							if($paymentDetail['sendPaymentTo'] == 'qbo'){
								if(($paymentDetail['status'] == 0)){
									
									if($ConsolForceCurrencyMapping[$aggregationId]){
										$newForceExchangeRate		= $ConsolForceCurrencyMapping[$aggregationId];
										$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($newForceExchangeRate)));
										$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
									}
									
									if(($paymentDetail['paymentType'] == 'RECEIPT') OR ($paymentDetail['paymentType'] == 'CAPTURE')){
										if(@$paymentDetail['paymentDate']){
											$paymentDate	= $paymentDetail['paymentDate'];
											$BPDateOffset	= (int)substr($paymentDate,23,3);
											$xeroOffset		= 0;
											$diff			= 0;
											$diff			= $BPDateOffset - $xeroOffset;
											$date			= new DateTime($paymentDate);
											$BPTimeZone		= 'GMT';
											$date->setTimezone(new DateTimeZone($BPTimeZone));
											if($diff){
												$diff			.= ' hour';
												$date->modify($diff);
											}
											$paymentDate	= $date->format('Y-m-d');
										}
										/* if(isset($paymentMappings[$paymentDetail['paymentMethod']])){
											$DepositToAccountRef	= $paymentMappings[$paymentDetail['paymentMethod']]['account2PaymentId'];
											$PaymentMethodRef		= $paymentMappings[$paymentDetail['paymentMethod']]['paymentValue'];
										} */
										
										if(isset($paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])])){
											$DepositToAccountRef		= $paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
											$PaymentMethodRef			= $paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
										}
										
										else if(isset($paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])])){
											$DepositToAccountRef		= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
											$PaymentMethodRef			= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
										}
										
										else if(isset($paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])])){
											$DepositToAccountRef		= $paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
											$PaymentMethodRef			= $paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
										}
										
										else if(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
											$DepositToAccountRef	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
											$PaymentMethodRef		= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
										}
										
										
										
										$exRate	= '';
										if(isset($journalDatas[$paymentDetail['journalId']])){
											$CurrencyRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
										}
										if($paymentDetail['amount'] > 0){
											$paymentSetCurrency	= $paymentDetail['currency'];
											$allSentIDS[]		= $key;
											$Positiveamount		+= $paymentDetail['amount'];
										}
										else{
											continue;
										}
									}
									elseif($paymentDetail['paymentType'] == 'PAYMENT'){
										$allSentIDS[]	= $key;
										$Negativeamount	+= abs($paymentDetail['amount']);
									}
								}
							}
						}
					}
					
					if($IsJournalPendings){
						continue;
					}
					$aggregatedInvoiceNumber	= 'PMT-';
					if($QBOorderId){
						$readurl				= 'invoice/'.$QBOorderId.'?minorversion=41';
						$InvoiceResponse		= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
						if(isset($InvoiceResponse['Invoice'])){
							$QBOCustomerID				= $InvoiceResponse['Invoice']['CustomerRef']['value'];
							$QBOTotalAmount				= $InvoiceResponse['Invoice']['TotalAmt'];
							$aggregatedInvoiceTotal		= $InvoiceResponse['Invoice']['TotalAmt'];
							$aggregatedInvoiceNumber	.= $InvoiceResponse['Invoice']['DocNumber'];
						}
					}
					$AmazonFeeInfo	= $this->ci->db->get_where('sales_order',array('amazonBatchID' => $amazonBatchID))->row_array();
					$totalFeeAmount	= $AmazonFeeInfo['totalFeeAmount'];
					if($Positiveamount	> 0){
						$sentableAmount	= $Positiveamount;
						if($Negativeamount	> 0){
							$sentableAmount	= $sentableAmount - $Negativeamount;
						}
						if($totalFeeAmount	> 0){
							$sentableAmount	= $sentableAmount - $totalFeeAmount;
						}
						if($sentableAmount){
							if(sprintf("%.4f",($sentableAmount)) > sprintf("%.4f",($aggregatedInvoiceTotal))){
								continue;
							}
							if(!$QBOCustomerID){continue;};
							/* if(!$PaymentMethodRef){continue;}; */
							if(!$DepositToAccountRef){continue;};
							$request	= array(
								'TxnStatus' 			=> 'PAID',
								'CustomerRef' 			=> array('value' => $QBOCustomerID),
								'PaymentRefNum'			=> substr($aggregatedInvoiceNumber,0,21),	//this field have a max lenght (21)
								'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
								/* 'PaymentMethodRef' 		=> array('value' => $PaymentMethodRef), */
								'TxnDate' 				=> $paymentDate,
								'ExchangeRate'			=> sprintf("%.4f",(1 / $CurrencyRate)),
								'TotalAmt' 				=> $sentableAmount,
								'Line' 					=> array(
									array(
										'Amount'			=> $sentableAmount,
										'LinkedTxn'			=> array(
											array(
												'TxnId'			=> $QBOorderId,
												'TxnType'		=> "Invoice",
											)
										),
									),
								),
							);
							if($paymentSetCurrency){
								if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
									$exRate = $this->getQboExchangeRateByDb($account2Id,$paymentSetCurrency,$config['defaultCurrrency'],$paymentDate);
									if($exRate){
										$request['ExchangeRate'] = $exRate;
									}
									else{
										$exRate	= $exchangeRate[strtolower($paymentSetCurrency)][strtolower($config['defaultCurrrency'])]['Rate'];
										if($exRate){
											$request['ExchangeRate'] = $exRate;
										}
										else{
											echo 'ExchangeRate Not found Line no - 564';continue;
											unset($request['ExchangeRate']);
										}
									}
									
								}
							}
							if(!$request['ExchangeRate']){
								echo 'ExchangeRate Not found Line no - 572';continue;
								unset($request['ExchangeRate']);
							}
							if($ConsolForceCurrencyMapping[$aggregationId]){
								unset($request['ExchangeRate']);
							}
						}
						if($request){
							$url		= 'payment';
							$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
							$createdRowData['QBO Payment Request	: ']	= $request;
							$createdRowData['QBO Payment Response	: ']	= $results;
							$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('amazonBatchID' => $amazonBatchID));
							if($results['Payment']['Id']){
								$dueAmount			= 0;
								$readurl			= 'invoice/'.$QBOorderId.'?minorversion=41';
								$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
								if(isset($InvoiceResponse['Invoice'])){
									$dueAmount		= $InvoiceResponse['Invoice']['Balance'];
								}
								foreach($allorderDatas as $orderId => $orderDatas){
									$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
									foreach($paymentDetails as $key => $paymentDetail){
										if(in_array($key,$allSentIDS)){
											$paymentDetails[$key]['status']	= '1';
										}
									}
									$paymentDetails[$results['Payment']['Id']]	= array(
										"amount" 				=> $sentableAmount,
										'status'				=> '1',
										'AmountCreditedIn'		=> 'QBO',
										'PaymentMethodRef'		=> $DepositToAccountRef,
									);
									$updateArray		= array();
									$updateArray	= array(
										'isPaymentCreated'	=> '1',
										'status' 			=> '3',
										'paymentStatus'		=> '1',
									);
									$updateArray['paymentDetails']	= json_encode($paymentDetails);
									$updateArray['paymentSynced']	= 1;
									$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
								}
								if($dueAmount == 0){
									$this->ci->db->where_in('aggregationId',$aggregationId)->update('sales_order',array('isPaymentCreated' => 1, 'status' => 3, 'paymentStatus' => 1));
								}
							}
						}
					}
				}
			}
		}
	}
}
?>