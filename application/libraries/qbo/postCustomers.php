<?php
$this->reInitialize();
$coutryJson		= file_get_contents( FCPATH . 'application' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'country' . DIRECTORY_SEPARATOR . 'countriesall.json'); 
$coutryJsons	= json_decode($coutryJson,true);
$coutryLists	= array();
foreach($coutryJsons as $coutryJson){
	$coutryLists[strtolower($coutryJson['alpha-3'])]	= $coutryJson;
}
$this->currencyList	= $this->ci->{$this->ci->globalConfig['fetchCustomer']}->getAllCurrency();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config	= $this->accountConfig[$account2Id];	
	if(($postedAccount2Id) AND ($account2Id != $postedAccount2Id)){
		continue;
	}
	//QUERY TO GET ALL PENDING AND UPADETED CUSTOMERS AND VENDORES	-:
	if(!is_array($orgObjectId)){
		$orgObjectId	= array($orgObjectId);
	}
	if($orgObjectId){
		$this->ci->db->where_in('customerId',$orgObjectId);
	}
	$datas	= $this->ci->db->order_by('isPrimary','desc')->where_in('status',array('0','2'))->get_where('customers',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	$primaryCompany	= array();
	foreach($datas as $data){
		if(!$data['isPrimary']){
			if(@$data['company']){
				$primaryCompany[]	= $data['company'];
			}
		}
	}
	if($primaryCompany){
		$primaryCompany	= array_filter($primaryCompany);
		$primaryCompany	= array_unique($primaryCompany);
		$companyDatas	= $this->ci->db->where_in('company',$primaryCompany)->get_where('customers',array('isPrimary' => '1','account2Id' => $account2Id))->result_array();
		if($companyDatas){
			foreach($companyDatas as $companyData){
				if(!$companyData['createdCustomerId']){
					$orgObjectId[]		= $companyData['customerId'];
				}
			}
		}
		if($orgObjectId){
			$this->ci->db->where_in('customerId',$orgObjectId);
		}
		$datas	= $this->ci->db->order_by('isPrimary','desc')->where_in('status',array('0','2'))->get_where('customers',array('account2Id' => $account2Id))->result_array();
	}
	//CREATE ALL TAX CODES MAPPING	-:	
	$this->ci->db->reset_query();
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']]	= $taxMappingTemp;
	}
	
	//CREATE CUSTOMER'S TYPE MAPPING	-:	
	$this->ci->db->reset_query();
	$customerTypeMappings	= array();
	$customerMappingsTemps	= $this->ci->db->get_where('mapping_customertype',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerTypeMappings[strtolower($customerMappingsTemp['account1customertypeId'])]	= $customerMappingsTemp;
	}
	
	//CREATE TERMS MAPPING	-:	
	$this->ci->db->reset_query();
	$termsMappingTemps	= $this->ci->db->get_where('mapping_terms',array('account2Id' => $account2Id))->result_array();
	$termsMapping		= array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$termsMapping[$termsMappingTemp['account1TermsId']]	= $termsMappingTemp;
	}
	
	if($datas){
		foreach($datas as $customerDatas){
			if(($customerDatas['status'] == 4) OR ($customerDatas['status'] == 3)){
				//skip sending the archived and error contacts
				continue;
			}
			$config1		= $this->ci->account1Config[$customerDatas['account1Id']];
			$currencyList	= $this->currencyList[$customerDatas['account1Id']];
			$rowDatas		= json_decode($customerDatas['params'],true);
			$customerId		= $customerDatas['customerId'];
			$currencyId		= $rowDatas['financialDetails']['currencyId'];
			
			$rowDatas['organisation']['name']	= trim($rowDatas['organisation']['name']);
			$customerDatas['company']	= trim($customerDatas['company']);
			$customerDatas['fname']	= trim($customerDatas['fname']);
			$customerDatas['lname']	= trim($customerDatas['lname']);
						
			$rowDatas['organisation']['name']	= str_replace("(","",$rowDatas['organisation']['name']);
			$rowDatas['organisation']['name']	= str_replace(")","",$rowDatas['organisation']['name']);
			$rowDatas['organisation']['name']	= str_replace(":","",$rowDatas['organisation']['name']);
			$customerDatas['fname']				= str_replace("(","",$customerDatas['fname']);
			$customerDatas['fname']				= str_replace(")","",$customerDatas['fname']);
			$customerDatas['lname']				= str_replace("(","",$customerDatas['lname']);
			$customerDatas['lname']				= str_replace(")","",$customerDatas['lname']);
			$customerDatas['company']			= str_replace("(","",$customerDatas['company']);
			$customerDatas['company']			= str_replace(")","",$customerDatas['company']);
			$customerDatas['company']			= str_replace(":","",$customerDatas['company']);
			
			$BPCommpanyName	= $rowDatas['organisation']['name'];
			/* $BPCommpanyName	= str_replace("'","",$BPCommpanyName); */
			$BPCommpanyName	= str_replace(":","",$BPCommpanyName);
			$BPCommpanyName	= str_replace("::","",$BPCommpanyName);
			$BPEmailAddress	= $customerDatas['email'];
			$BPFirstName	= $customerDatas['fname'];
			/* $BPFirstName	= str_replace("'","",$BPFirstName); */
			$BPFirstName	= str_replace(":","",$BPFirstName);
			$BPFirstName	= str_replace("::","",$BPFirstName);
			$BPLastName		= $customerDatas['lname'];
			/* $BPLastName		= str_replace("'","",$BPLastName); */
			$BPLastName		= str_replace(":","",$BPLastName);
			$BPLastName		= str_replace("::","",$BPLastName);
			$BPFullName		= ($BPLastName) ? ($BPFirstName.' '.$BPLastName) : ($BPFirstName);
						
			$request				= array();
			$fetchResponse			= array();
			$ParentRef				= '';
			$PrimaryCustomerQBOID	= '';
			$createNewContact		= 0;
			
			if($customerDatas['isSupplier']){
				if(!$rowDatas['isPrimaryContact']){
					$BPCommpanyName	= $rowDatas['organisation']['name'];
					if($BPCommpanyName){
						$isPrimaryCompanySent	= 0;
						$bpCompanyDatas			= $this->ci->db->get_where('customers',array('createdCustomerId <> ' => '','company' => $BPCommpanyName,'account2Id' => $account2Id))->result_array();
						$bpCompanyDatasInfo		= array();
						foreach($bpCompanyDatas as $bpCompanyData){
							$bpCompanyDataRowData	= json_decode($bpCompanyData['params'],true);
							if($bpCompanyDataRowData['isPrimaryContact']){
								$isPrimaryCompanySent	= 1;
								$PrimaryCustomerQBOID	= $bpCompanyData['createdCustomerId'];
								break;
							}
						}
						if($isPrimaryCompanySent){
							$this->ci->db->update('customers',array('createdCustomerId' => $PrimaryCustomerQBOID, 'status' => 1,'message' => ''),array('id' => $customerDatas['id']));
							continue;
						}
						else{
							$this->ci->db->update('customers',array('message' => 'Primary Vendor is not sent yet'),array('id' => $customerDatas['id']));
							continue;
						}
					} 
				}
				$query		= '';
				if($customerDatas['createdCustomerId']){
					$query		= "select * from vendor where id = '".$customerDatas['createdCustomerId']."'";
					$url		= "query?minorversion=4&query=".rawurlencode($query);
					$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
					$request	= $serchRes['QueryResponse']['Vendor']['0'];
				}
				else{
					if($BPCommpanyName){
						$query			= "select * from vendor where CompanyName = '".addslashes($BPCommpanyName)."'";
						$url			= "query?minorversion=4&query=".rawurlencode($query);
						$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
						$fetchResponse	= $serchRes['QueryResponse']['Vendor'][0];
						$request		= $fetchResponse;
						if(!$fetchResponse){
							$query			= "select * from vendor where DisplayName = '".addslashes($BPCommpanyName)."'";
							$url			= "query?minorversion=4&query=".rawurlencode($query);
							$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
							$fetchResponse	= $serchRes['QueryResponse']['Vendor'][0];
							$request		= $fetchResponse;
						}
						if(!$fetchResponse){
							$createNewContact	= 1;
						}
					}
					else{
						if($BPFirstName AND $BPLastName){
							$query			= "select * from vendor where GivenName = '".addslashes($BPFirstName)."' and FamilyName = '".addslashes($BPLastName)."'";
						}
						if($BPFirstName AND !$BPLastName){
							$query			= "select * from vendor where GivenName = '".addslashes($BPFirstName)."'";							
						}
						if(!$BPFirstName AND $BPLastName){
							$query			= "select * from vendor where FamilyName = '".addslashes($BPLastName)."'";
						}
						$url			= "query?minorversion=4&query=".rawurlencode($query);
						$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
						$fetchResponse	= $serchRes['QueryResponse']['Vendor'];
						$VendorFound	= 0;
						foreach($fetchResponse as $fetchResponseTemp){
							if($fetchResponseTemp['PrimaryEmailAddr']['Address']){
								if($BPEmailAddress == $fetchResponseTemp['PrimaryEmailAddr']['Address']){
									$request		= $fetchResponseTemp;
									$VendorFound	= 1;
									break;
								}
							}
						}
						if(!$VendorFound){
							$query			= "select * from vendor where DisplayName = '".addslashes($BPFullName)."'";
							$url			= "query?minorversion=4&query=".rawurlencode($query);
							$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
							$fetchResponse	= $serchRes['QueryResponse']['Vendor'];
							foreach($fetchResponse as $fetchResponseTemp){
								if($fetchResponseTemp['PrimaryEmailAddr']['Address']){
									if($BPEmailAddress == $fetchResponseTemp['PrimaryEmailAddr']['Address']){
										$request		= $fetchResponseTemp;
										$VendorFound	= 1;
										break;
									}
								}
							}
						}
						if(!$VendorFound){
							$createNewContact	= 1;
						}
					}
				}
			}
			else{
				if(!$rowDatas['isPrimaryContact']){
					$BPCommpanyName	= $rowDatas['organisation']['name'];
					if($BPCommpanyName){
						$isPrimaryCompanySent	= 0;
						$bpCompanyDatas			= $this->ci->db->get_where('customers',array('createdCustomerId <> ' => '','company' => $BPCommpanyName,'account2Id' => $account2Id))->result_array();
						$bpCompanyDatasInfo		= array();
						foreach($bpCompanyDatas as $bpCompanyData){
							$bpCompanyDataRowData	= json_decode($bpCompanyData['params'],true);
							if($bpCompanyDataRowData['isPrimaryContact']){
								$isPrimaryCompanySent	= 1;
								$PrimaryCustomerQBOID	= $bpCompanyData['createdCustomerId'];
								break;
							}
						}
						if($isPrimaryCompanySent){
							$this->ci->db->update('customers',array('createdCustomerId' => $PrimaryCustomerQBOID, 'status' => 1,'message' => ''),array('id' => $customerDatas['id']));
							continue;
						}
						else{
							$this->ci->db->update('customers',array('message' => 'Primary Customer is not sent yet'),array('id' => $customerDatas['id']));
							continue;
						}
					} 
				}
				if($customerDatas['createdCustomerId']){
					$query		= "select * from customer where id = '".$customerDatas['createdCustomerId']."'";
					$url		= "query?minorversion=4&query=".rawurlencode($query);
					$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
					$request	= $serchRes['QueryResponse']['Customer']['0'];
				}
				else{
					if($BPCommpanyName){
						$query			= "select * from customer where CompanyName = '".addslashes($BPCommpanyName)."'";
						$url			= "query?minorversion=4&query=".rawurlencode($query);
						$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
						$fetchResponse	= $serchRes['QueryResponse']['Customer'][0];
						$request		= $fetchResponse;
						if(!$fetchResponse){
							$query			= "select * from customer where DisplayName = '".addslashes($BPCommpanyName)."'";
							$url			= "query?minorversion=4&query=".rawurlencode($query);
							$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
							$fetchResponse	= $serchRes['QueryResponse']['Customer'][0];
							$request		= $fetchResponse;
						}
						if(!$fetchResponse){
							$createNewContact	= 1;
						}
					}
					else{
						if($BPFirstName AND $BPLastName){
							$query			= "select * from customer where GivenName = '".addslashes($BPFirstName)."' and FamilyName = '".addslashes($BPLastName)."' and PrimaryEmailAddr = '".$BPEmailAddress."'";
						}
						if($BPFirstName AND !$BPLastName){
							$query			= "select * from customer where GivenName = '".addslashes($BPFirstName)."' and PrimaryEmailAddr = '".addslashes($BPEmailAddress)."'";
						}
						if(!$BPFirstName AND $BPLastName){
							$query			= "select * from customer where FamilyName = '".addslashes($BPLastName)."' and PrimaryEmailAddr = '".addslashes($BPEmailAddress)."'";
						}
						$query			= "select * from customer where GivenName = '".addslashes($BPFirstName)."' and FamilyName = '".addslashes($BPLastName)."' and PrimaryEmailAddr = '".$BPEmailAddress."'";
						$url			= "query?minorversion=4&query=".rawurlencode($query);
						$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
						$fetchResponse	= $serchRes['QueryResponse']['Customer'][0];
						$request		= $fetchResponse;
						if(!$fetchResponse){
							$query			= "select * from customer where DisplayName = '".addslashes($BPFullName)."' and PrimaryEmailAddr = '".addslashes($BPEmailAddress)."'";
							$url			= "query?minorversion=4&query=".rawurlencode($query);
							$serchRes		= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
							$fetchResponse	= $serchRes['QueryResponse']['Customer'][0];
							$request		= $fetchResponse;
							if(!$fetchResponse){
								$createNewContact	= 1;
							}
						}
					}
				}
			}
			$DisplayName	= $BPFullName;
			if(!$BPFullName){
				$BPFullName		= $customerDatas['company'];
				$DisplayName	= $BPFullName;
			}
			if($customerDatas['company']){
				if($customerDatas['fname']){
					$DisplayName	= $customerDatas['company'] .' - '. trim($BPFirstName.' '.$BPLastName);
				}
			}
			
			if(($clientcode == 'sandbqbo') OR ($clientcode == 'biscuiteersqbo')){
				if($customerDatas['company']){
					$DisplayName	= $customerDatas['company'];
				}
				else{
					$DisplayName	= $BPFullName;
				}
			}
			$billAddress					= $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['BIL']];
			$shipAddress					= $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['DEL']];
			$request['Title'] 				= $rowDatas['salutation'];
			$request['GivenName'] 			= $BPFirstName;
			$request['FamilyName']			= $BPLastName;
			$request['PrintOnCheckName']	= $BPFullName;
			if($customerDatas['company']){
				$request['CompanyName']		= $customerDatas['company'];
			}
			if($request['DisplayName']){
				$request['DisplayName']		= $request['DisplayName'];
			}
			else{
				$request['DisplayName']		= $DisplayName;
			}
			$request['CompanyName']			= $customerDatas['company'];
			$request['PrimaryPhone'] 		= array('FreeFormNumber' => $customerDatas['phone']);
			$request['PrimaryEmailAddr']	= array('Address' => $customerDatas['email']);
			$request['CurrencyRef']			= array('value' => isset($currencyList[$currencyId])?($currencyList[$currencyId]['code']):($config1['baseCurrency']));
			if(!$customerDatas['company']){
				if($request['DisplayName']){
					$request['DisplayName']	= $request['DisplayName'];
				}
				else{
					$request['DisplayName']	= $BPFullName;
				}
			}
			$request['BillAddr']		= array(
				'Line1' 					=> $billAddress['addressLine1'],
				'Line2' 					=> @$billAddress['addressLine2'],
				'City' 						=> @$billAddress['addressLine3'], 
				'CountrySubDivisionCode'	=> @$billAddress['addressLine4'],
				'Country' 					=> (@$coutryLists[strtolower($billAddress['countryIsoCode'])])?(@$coutryLists[strtolower($billAddress['countryIsoCode'])]['name']):($billAddress['countryIsoCode']),
				'PostalCode' 				=> $billAddress['postalCode'],
			);
			$request['ShipAddr'] 		= array(
				'Line1' 					=> @$shipAddress['addressLine1'],
				'Line2' 					=> @$shipAddress['addressLine2'],
				'City' 						=> @$shipAddress['addressLine3'],
				'CountrySubDivisionCode' 	=> @$shipAddress['addressLine4'], 
				'Country' 					=> (@$coutryLists[strtolower($shipAddress['countryIsoCode'])])?(@$coutryLists[strtolower($shipAddress['countryIsoCode'])]['name']):($shipAddress['countryIsoCode']),
				'PostalCode' 				=> $shipAddress['postalCode'], 
			);
			if(!$customerDatas['isSupplier']){
				$customerTypeValues	= @$rowDatas['customFields'][$config1['customerTypeCustomField']];
				if(isset($customerTypeMappings[strtolower($customerTypeValues)])){
					$customerType	= $customerTypeMappings[strtolower($customerTypeValues)]['account2customertypeId'];
					if($customerType){
						$request['CustomerTypeRef']	= array('value' => $customerType);
					}
				}
				if($rowDatas['financialDetails']['taxCodeId']){
					if($taxMapppings[$rowDatas['financialDetails']['taxCodeId']]){
						//
					}
				}
				else{
					$request['Taxable']	= false;
				}
			}
			if(strtolower($config['accountType']) == 'uk'){
				unset($request['DefaultTaxCodeRef']);
				unset($request['Taxable']);
			}
			$termsId	= '';
			if(isset($rowDatas['financialDetails']['creditTermDays'])){
				$termsId	= $termsMapping[$rowDatas['financialDetails']['creditTermDays']]['account2TermsId'];
			}
			if($termsId){
				if(!$customerDatas['isSupplier']){
					$request['SalesTermRef']['value']	= $termsId;
				}	
				else{
					$request['TermRef']['value']		= $termsId;
				}
			}
			if(@$rowDatas['communication']['telephones']['MOB']){
				$request['Mobile']['FreeFormNumber']	= $rowDatas['communication']['telephones']['MOB'];
			}
			if($customerDatas['isSupplier']){
				$url	= 'vendor';
			}
			else{
				$url	= 'customer';
			}
			
			$request['DisplayName']	= str_replace(":","",$request['DisplayName']);
			
			$res		= @$this->getCurl($url, 'POST', json_encode($request), 'json',$account2Id)[$account2Id]; 
			$createdId	= '';
			if(@isset($res['Vendor']['Id'])){
				$createdId	= $res['Vendor']['Id'];
			}
			else if(@$res['Customer']['Id']){
				$createdId	= $res['Customer']['Id'];
			}
			$isCustomerDuplicateError	= 0;
			if($res['Fault']){
				foreach($res['Fault']['Error'] as $Error){
					if(($Error['Message'] == 'Duplicate Name Exists Error') OR ($Error['Message'] == 'Error de existencia de nombre duplicado')){
						$isCustomerDuplicateError	= 1;
					}
				}
			}
			if($isCustomerDuplicateError){
				if(!$customerDatas['isUsedCustomerIdForName']){
					//
				}
				$BPFullName	.= '('.$customerDatas['customerId'].')';
				if(!$customerDatas['company']){
					if($request['DisplayName']){
						$request['DisplayName'] = $request['DisplayName'].' ('.$customerDatas['customerId'].')';
					}
					else{
						$request['DisplayName']	= $BPFullName;
					}
				}
				$res		= @$this->getCurl($url, 'POST', json_encode($request), 'json',$account2Id)[$account2Id]; 
				$createdId	= '';
				if(@isset($res['Vendor']['Id'])){
					$createdId	= $res['Vendor']['Id'];
				}
				else if(@$res['Customer']['Id']){
					$createdId	= $res['Customer']['Id'];
				}
			}
			$ceatedParams	= array(
				'Request data	: '	=> $request, 
				'Response data	: '	=> $res,
			);
			$this->ci->db->update('customers',array('ceatedParams' => json_encode($ceatedParams)),array('id' => $customerDatas['id']));
			if($createdId){
				$this->ci->db->update('customers',array('status' => '1','createdCustomerId' => $createdId,'message' => ''),array('id' => $customerDatas['id'])); 
			}
			
			//new logStoringFunctionality
			$path	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account2'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'customers'. DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path.$customerId.'.logs',"\n\n QBO Log Added On : ".date('c')." \n". json_encode($ceatedParams),FILE_APPEND);
		}	
	}
}
?>