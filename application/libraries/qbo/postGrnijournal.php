<?php
//qbo_demo		:	postCogs_journal to xero
$this->reInitialize();
$clientcode		= $this->ci->config->item('clientcode');
$exchangeRates	= $this->getExchangeRate();

foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){continue;}
	}
	
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('journalsId',$orgObjectId);
	}
	$allDatas	= $this->ci->db->get_where('grni_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(empty($allDatas)){continue;}
	
	$allPurchaseOrderData	= array();
	$allPurchaseOrderIds	= array();
	$allPurchaseOrderIds	= array_column($allDatas, 'orderId');
	$allPurchaseInfos		= $this->ci->db->where_in('orderId',$allPurchaseOrderIds)->get_where('purchase_order')->result_array();
	
	if(!empty($allPurchaseInfos)){
		foreach($allPurchaseInfos as $allPurchaseInfosTemp){
			$allPurchaseOrderData[$allPurchaseInfosTemp['orderId']]	= $allPurchaseInfosTemp;
		};
	}
	
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	foreach($allDatas as $orderDatas){
		if($orderDatas['status'] != 0){continue;}
		if($orderDatas['createdJournalsId']){continue;}
		
		
		if(isset($allPurchaseOrderData[$orderDatas['orderId']])){
			if(strlen($allPurchaseOrderData[$orderDatas['orderId']]['createOrderId']) > 0){
				$this->ci->db->update('grni_journal',array('message' => 'Purchase order already posted.', 'status' => 4),array('id' => $orderDatas['id']));
				continue;
			}
		}
		
		
		
		$request			= array();
		$results			= array();
		$InvoiceLineAdd		= array();
		$AllCredits			= array();
		$AllDebits			= array();
		$blockPosting		= 0;
		$ItemSequence		= 0;
		$debitAccRef		= '';
		$creditAccRef		= '';
		
		$journalsId			= $orderDatas['journalsId'];
		$config1			= $this->ci->account1Config[$orderDatas['account1Id']];
		$params				= json_decode($orderDatas['params'],true);
		$createdParams		= json_decode($orderDatas['createdParams'],true);
		$bpBaseCurrency		= strtolower($config1['currencyCode']);
		$channelId			= $orderDatas['channelId'];
		$AllCredits			= $params['credits'];
		$AllDebits			= $params['debits'];
		$taxDate			= $params['taxDate'];
		
		//taxdate chanages
		$BPDateOffset		= (int)substr($taxDate,23,3);
		$QBOoffset			= 0;
		$diff				= $BPDateOffset - $QBOoffset;
		$date				= new DateTime($taxDate);
		$BPTimeZone			= 'GMT';
		$date->setTimezone(new DateTimeZone($BPTimeZone));
		if($diff > 0){
			$diff			.= ' hour';
			$date->modify($diff);
		}
		$taxDate			= $date->format('Y-m-d');
		
		$exchangeRate		= $params['exchangeRate'];
		$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
		if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
			$exRate = $this->getQboExchangeRateByDb($account2Id,$orderDatas['currencyCode'],$config['defaultCurrrency'],$taxDate);
			if($exRate){
				$exchangeRate		= $exRate;
			}
			else{
				$exRate = $exchangeRatesAll[strtolower($orderDatas['currencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
				if($exRate){
					$exchangeRate		= $exRate;
				}
				else{
					$exchangeRate		= 0;
				}
			}
		}
		
		if(!empty($AllCredits)){
			foreach($AllCredits as $key => $CreditsData){
				if($CreditsData['transactionAmount'] == 0){continue;}
				$creditAccRef	= '';
				$journalNominal	= $CreditsData['nominalCode'];
				if($nominalMappings[$journalNominal]['account2NominalId']){
					$creditAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
				}
				if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
					$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
				}
				if(!$creditAccRef){
					$blockPosting	= 1;
				}
				$InvoiceLineAdd[$ItemSequence]	= array(
					'LineNum'					=> ($ItemSequence + 1),
					'Description'				=> 'GRNI Journal',
					'Amount'					=> sprintf("%.4f",($CreditsData['transactionAmount'])),
					'DetailType'				=> 'JournalEntryLineDetail',
					'JournalEntryLineDetail'	=> array(
						'PostingType'				=> 'Credit',
						'AccountRef'				=> array('value' => $creditAccRef),
					),
				);
				if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
				}
				elseif($channelMappings[$channelId]['account2ChannelId']){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
				}
				$ItemSequence++;
			}
		}
		if(!empty($AllDebits)){
			foreach($AllDebits as $key => $DebitsData){
				if($DebitsData['transactionAmount'] == 0){continue;}
				$debitAccRef	= '';
				$journalNominal	= $DebitsData['nominalCode'];
				if($nominalMappings[$journalNominal]['account2NominalId']){
					$debitAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
				}
				if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
					$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
				}
				if(!$debitAccRef){
					$blockPosting	= 1;
				}
				$InvoiceLineAdd[$ItemSequence]	= array(
					'LineNum'					=> ($ItemSequence + 1),
					'Description'				=> 'GRNI Journal',
					'Amount'					=> sprintf("%.4f",($DebitsData['transactionAmount'])),
					'DetailType'				=> 'JournalEntryLineDetail',
					'JournalEntryLineDetail'	=> array(
						'PostingType'				=> 'Debit',
						'AccountRef'				=> array('value' => $debitAccRef),
					),
				);
				if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
				}
				elseif($channelMappings[$channelId]['account2ChannelId']){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
				}
				$ItemSequence++;
			}
		}
		if($blockPosting){
			$this->ci->db->update('grni_journal',array('message' => 'NominalMapping is Missing'),array('id' => $orderDatas['id']));
			continue;
		}
		
		if(empty($InvoiceLineAdd)){continue;}
		
		$DocNumber	= 'PO#'.$orderDatas['orderId'];
		$DocNumber	= substr($DocNumber,0,21);
		$request	= array(
			'DocNumber'		=> $DocNumber,
			'TxnDate'		=> $taxDate,
			'ExchangeRate'	=> $exchangeRate,
			'CurrencyRef'	=> array('value' => $orderDatas['currencyCode']),
			'Line'			=> $InvoiceLineAdd,
		);
		if(!$request['ExchangeRate']){
			echo 'ExchangeRate Not found Line no - 180';continue;
			unset($request['ExchangeRate']);
		}
		if(!empty($request)){
			$url			= 'journalentry?minorversion=37';
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdParams['Request data	: ']	= $request;
			$createdParams['Response data	: ']	= $results;
			
			if(@$results['JournalEntry']['Id']){
				$this->ci->db->update('grni_journal',array('status' => '1','createdJournalsId' => $results['JournalEntry']['Id'],'message' => '','createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
			}
			else{
				$this->ci->db->update('grni_journal',array('createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
			}
		}
	}
}

foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){continue;}
	}
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('journalsId',$orgObjectId);
	}
	$allDatas	= $this->ci->db->get_where('grni_journal',array('account2Id' => $account2Id , 'status' => 1))->result_array();
	if(empty($allDatas)){continue;}
	
	$allPurchaseOrderData	= array();
	$allPurchaseOrderIds	= array();
	$allPurchaseOrderIds	= array_column($allDatas, 'orderId');
	$allPurchaseInfos		= $this->ci->db->where_in('orderId',$allPurchaseOrderIds)->get_where('purchase_order',array('status <>' => 0, 'createOrderId <>' => ''))->result_array();
	
	if(empty($allPurchaseInfos)){
		continue;
	}
	else{
		foreach($allPurchaseInfos as $allPurchaseInfosTemp){
			$allPurchaseOrderData[$allPurchaseInfosTemp['orderId']]	= $allPurchaseInfosTemp;
		};
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	foreach($allDatas as $orderDatas){
		if($orderDatas['status'] != 1){continue;}
		if($orderDatas['reversedJournalsId']){continue;}
		
		$journalsOrderInfo	= array();
		if(isset($allPurchaseOrderData[$orderDatas['orderId']])){
			if(strlen($allPurchaseOrderData[$orderDatas['orderId']]['createOrderId']) > 0){
				$journalsOrderInfo	= $allPurchaseOrderData[$orderDatas['orderId']];
			}
			else{
				continue;
			}
		}
		else{
			continue;
		}
		
		if(empty($journalsOrderInfo)){continue;}
		
		
		
		$request			= array();
		$results			= array();
		$InvoiceLineAdd		= array();
		$AllCredits			= array();
		$AllDebits			= array();
		$blockPosting		= 0;
		$ItemSequence		= 0;
		$debitAccRef		= '';
		$creditAccRef		= '';
		
		$journalsId			= $orderDatas['journalsId'];
		$config1			= $this->ci->account1Config[$orderDatas['account1Id']];
		$params				= json_decode($orderDatas['params'],true);
		$createdParams		= json_decode($orderDatas['createdParams'],true);
		$bpBaseCurrency		= strtolower($config1['currencyCode']);
		$channelId			= $orderDatas['channelId'];
		$AllCredits			= $params['credits'];
		$AllDebits			= $params['debits'];
		$taxDate			= $params['taxDate'];
		
		//overrideTaxDateInReverseJournal
		$taxDate			= $orderDatas['orderTaxdate'];
		$orderRawInfo		= json_decode($journalsOrderInfo['rowData'], true);
		$poOrderTaxDate		= $orderRawInfo['invoices'][0]['taxDate'];
		
		//overrideTaxDateInReverseJournal
		$taxDate			= $poOrderTaxDate;
		
	
		//taxdate chanages
		$BPDateOffset		= (int)substr($taxDate,23,3);
		$QBOoffset			= 0;
		$diff				= $BPDateOffset - $QBOoffset;
		$date				= new DateTime($taxDate);
		$BPTimeZone			= 'GMT';
		$date->setTimezone(new DateTimeZone($BPTimeZone));
		if($diff > 0){
			$diff			.= ' hour';
			$date->modify($diff);
		}
		$taxDate			= $date->format('Y-m-d');
		
		$exchangeRate		= $params['exchangeRate'];
		$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
		if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
			$exRate = $this->getQboExchangeRateByDb($account2Id,$orderDatas['currencyCode'],$config['defaultCurrrency'],$taxDate);
			if($exRate){
				$exchangeRate		= $exRate;
			}
			else{
				$exRate = $exchangeRatesAll[strtolower($orderDatas['currencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
				if($exRate){
					$exchangeRate		= $exRate;
				}
				else{
					$exchangeRate		= 0;
				}
			}
		}
		
		if(!empty($AllCredits)){
			foreach($AllCredits as $key => $CreditsData){
				if($CreditsData['transactionAmount'] == 0){continue;}
				$creditAccRef	= '';
				$journalNominal	= $CreditsData['nominalCode'];
				if($nominalMappings[$journalNominal]['account2NominalId']){
					$creditAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
				}
				if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
					$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
				}
				if(!$creditAccRef){
					$blockPosting	= 1;
				}
				$InvoiceLineAdd[$ItemSequence]	= array(
					'LineNum'					=> ($ItemSequence + 1),
					'Description'				=> 'GRNI Journal',
					'Amount'					=> sprintf("%.4f",($CreditsData['transactionAmount'])),
					'DetailType'				=> 'JournalEntryLineDetail',
					'JournalEntryLineDetail'	=> array(
						'PostingType'				=> 'Debit',
						'AccountRef'				=> array('value' => $creditAccRef),
					),
				);
				if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
				}
				elseif($channelMappings[$channelId]['account2ChannelId']){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
				}
				$ItemSequence++;
			}
		}
		if(!empty($AllDebits)){
			foreach($AllDebits as $key => $DebitsData){
				if($DebitsData['transactionAmount'] == 0){continue;}
				$debitAccRef	= '';
				$journalNominal	= $DebitsData['nominalCode'];
				if($nominalMappings[$journalNominal]['account2NominalId']){
					$debitAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
				}
				if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal])) AND ($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'])){
					$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
				}
				if(!$debitAccRef){
					$blockPosting	= 1;
				}
				$InvoiceLineAdd[$ItemSequence]	= array(
					'LineNum'					=> ($ItemSequence + 1),
					'Description'				=> 'GRNI Journal',
					'Amount'					=> sprintf("%.4f",($DebitsData['transactionAmount'])),
					'DetailType'				=> 'JournalEntryLineDetail',
					'JournalEntryLineDetail'	=> array(
						'PostingType'				=> 'Credit',
						'AccountRef'				=> array('value' => $debitAccRef),
					),
				);
				if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$journalNominal]))){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $nominalClassMapping[strtolower($channelId)][$journalNominal]['account2ClassId']);
				}
				elseif($channelMappings[$channelId]['account2ChannelId']){
					$InvoiceLineAdd[$ItemSequence]['JournalEntryLineDetail']['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
				}
				$ItemSequence++;
			}
		}
		if($blockPosting){
			$this->ci->db->update('grni_journal',array('message' => 'NominalMapping is Missing'),array('id' => $orderDatas['id']));
			continue;
		}
		
		if(empty($InvoiceLineAdd)){continue;}
		
		$DocNumber	= 'PO#'.$orderDatas['orderId'];
		$DocNumber	= substr($DocNumber,0,21);
		$request	= array(
			'DocNumber'		=> $DocNumber,
			'TxnDate'		=> $taxDate,
			'ExchangeRate'	=> $exchangeRate,
			'CurrencyRef'	=> array('value' => $orderDatas['currencyCode']),
			'Line'			=> $InvoiceLineAdd,
		);
		if(!$request['ExchangeRate']){
			echo 'ExchangeRate Not found Line no - 376';continue;
			unset($request['ExchangeRate']);
		}
		
		if(!empty($request)){
			$url			= 'journalentry?minorversion=37';
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdParams['Reverse Request data	: ']	= $request;
			$createdParams['Reverse Response data	: ']	= $results;
			
			if(@$results['JournalEntry']['Id']){
				$this->ci->db->update('grni_journal',array('status' => '2','reversedJournalsId' => $results['JournalEntry']['Id'],'message' => '','createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
			}
			else{
				$this->ci->db->update('grni_journal',array('createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
			}
		}
	}
}