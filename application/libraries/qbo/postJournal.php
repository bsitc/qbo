<?php
//qbo_demo		:	postAmazonfees by editing the SalesInvoices
$this->reInitialize();
$enableAggregation							= $this->ci->globalConfig['enableAggregation'];
$enableAmazonFees							= $this->ci->globalConfig['enableamazonfee'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableAmazonFees){
		continue;
	}
	
	$config	= $this->accountConfig[$account2Id];
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$journalDatasTemps	= $this->ci->db->get_where('amazon_ledger',array('account2Id' => $account2Id, 'status' => '0'))->result_array();
	
	if(!$journalDatasTemps){
		continue;
	}
	
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		$AggregationMappings		= array();
		$AggregationMappings2		= array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				if(!$ConsolMappingCustomField){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]								= $AggregationMappingsTemp;
				}
				else{
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if(!empty($channelMappingsTemps)){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$journalDatas	= array();
	$this->ci->db->reset_query();
	foreach($journalDatasTemps as $journalDatasTemp){
		if(isset($journalDatas[$journalDatasTemp['orderId']])){
			$journalDatas[$journalDatasTemp['orderId']]['amount']	+= $journalDatasTemp['amount'];
		}
		else{
			$journalDatas[$journalDatasTemp['orderId']]['amount']	= $journalDatasTemp['amount'];
		}
	}
	if($journalDatas){
		foreach($journalDatas as $BPOrderID => $journalData){
			$SalesInfo			= $this->ci->db->get_where('sales_order',array('orderId' => $BPOrderID, 'status >' => 0))->row_array();
			$totalFeeAmount		= 0;
			$totalFeeAmount		= $journalData['amount'];
			$EditRequest		= array();
			$AmazonAddedData	= array();
			if($SalesInfo){
				$channelId			= '';
				$orderCurrencyCode	= '';
				$orderId			= $SalesInfo['orderId'];
				$bpconfig			= $this->ci->account1Config[$SalesInfo['account1Id']];
				$rowDatas			= json_decode($SalesInfo['rowData'],true);
				$channelId			= $rowDatas['assignment']['current']['channelId'];
				$orderCurrencyCode	= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$CustomFieldValueID	= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				
				if($SalesInfo['sendInAggregation']){
					continue;
				}
				
				$QBOInvoiceID	= $SalesInfo['createOrderId'];
				if($QBOInvoiceID){
					$EditRequest		= array();
					$AmazonAddedData	= array();
					$rqInvoice			= array();
					$createdRowData		= json_decode($SalesInfo['createdRowData'],true);
					$query				= "select * from invoice where id = '".$QBOInvoiceID."'";
					$suburl				= "query?minorversion=4&query=".rawurlencode($query);
					$qboOrderInfos		= @$this->getCurl($suburl, 'GET', '', 'json',$account2Id)[$account2Id]; 
					$qboLineNumber		= 1;
					if($qboOrderInfos['QueryResponse']['Invoice']){
						foreach($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'] as $key => $qbLine){
							if($qbLine['DetailType'] == 'SubTotalLineDetail'){
								unset($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][$key]);
							}
							if(@$qbLine['LineNum'] > $qboLineNumber){
								$qboLineNumber	= $qbLine['LineNum'];
							}
						}
						
						
						$amazonFeeClassRef	= '';
						if(isset($channelMappings[$channelId])){
							$amazonFeeClassRef	= $channelMappings[$channelId]['account2ChannelId'];
						}
						
						$qboLineNumber++;
						$qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'])]	= array(
							'LineNum'						=> (count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line']) + 1),
							'Description'					=> 'Amazon Fees',
							'Amount'						=> sprintf("%.4f",((-1) * $totalFeeAmount)),
							'DetailType'					=> 'SalesItemLineDetail',
							'SalesItemLineDetail'			=> array(
								'ItemRef' 						=> array('value'=> $config['AmazonFeeItem']),
								'UnitPrice' 					=> sprintf("%.4f",((-1) * $totalFeeAmount)),
								'Qty' 							=> 1,
								'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode']),
								'ClassRef'						=> array('value' => $amazonFeeClassRef),
							),
						);
					
						$rqInvoice		= $qboOrderInfos['QueryResponse']['Invoice']['0'];
						$EditRequest	= array(
							'Id'			=> $rqInvoice['Id'],
							'DocNumber'		=> $rqInvoice['DocNumber'],
							'CustomerRef'	=> array('value' => $rqInvoice['CustomerRef']['value']),
							'BillEmail'		=> $rqInvoice['BillEmail'],
							'SyncToken'		=> $rqInvoice['SyncToken'],
							'domain'		=> $rqInvoice['domain'],
							'TxnDate'		=> $rqInvoice['TxnDate'],
							'DueDate'		=> $rqInvoice['DueDate'],
							'BillAddr'		=> $rqInvoice['BillAddr'],
							'ShipAddr'		=> $rqInvoice['ShipAddr'],
							'ExchangeRate'	=> $rqInvoice['ExchangeRate'],
							'CurrencyRef'	=> $rqInvoice['CurrencyRef'],
							//'TxnTaxDetail'	=> $rqInvoice['TxnTaxDetail'], 
							'ClassRef'		=> $rqInvoice['ClassRef'],
							'Line'			=> $rqInvoice['Line'],
						);
						if($clientcode == 'coloradokayak'){
							$EditRequest['AllowOnlineCreditCardPayment']	= 'true';
						}
						if(!$EditRequest['ExchangeRate']){
							unset($EditRequest['ExchangeRate']);
						}
					}
					if($EditRequest){
						$EditUrl		= 'invoice?minorversion=37';
						$EditResponse	= $this->getCurl($EditUrl, 'POST', json_encode($EditRequest), 'json', $account2Id)[$account2Id];
						$createdRowData['Edit Invoice Request Data	: ']	= $EditRequest;
						$createdRowData['Edit Invoice Response Data	: ']	= $EditResponse;
						$AmazonAddedData	= array(
							'Amazon Edit Request	:'	=>	$EditRequest,
							'Amazon Edit Response	:'	=>	$EditResponse,
						);
						$this->ci->db->where_in('orderId',$BPOrderID)->update('amazon_ledger',array('createdParams' => json_encode($AmazonAddedData)));
						if($EditResponse['Invoice']['Id']){
							$EditSuccess	= 1;
							
							$this->ci->db->where_in('orderId',$BPOrderID)->update('sales_order',array('createdRowData' => json_encode($createdRowData), 'status' => '1', 'message' => '', 'IsJournalposted' => 1, 'totalFeeAmount' => $totalFeeAmount));
							
							$this->ci->db->where_in('orderId',$BPOrderID)->update('amazon_ledger',array('status' => '1', 'createdJournalsId' => 'Linked with Invoice', 'message' => '', 'paymentCreated' => 1,'createdParams' => json_encode($AmazonAddedData)));
						}
						else{
							continue;
						}
					}
				}
			}
		}
	}
}
?>