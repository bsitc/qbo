<?php
$datasTemps				= $this->ci->db->get_where('product_load',array('status' => '0'))->result_array();
$productMappings		= array();
$productMappingsTemps	= $this->ci->db->get_where('products')->result_array();
foreach($productMappingsTemps as $productMappingsTemp){
	$productMappings[strtolower($productMappingsTemp['sku'])]	= $productMappingsTemp;
}
$this->extraProductParams	= array();
$proIds						= array();
foreach($datasTemps as $datasTemp){
	$sku		= strtolower($datasTemp['sku']);
	$productId	= $productMappings[$sku]['productId'];
	if($productId){
		$datasTemp['poststatus']	= 0;
		$proIds[]					= $productId;
		$this->extraProductParams[$datasTemp['account2Id']][$productId]	= $datasTemp; 
	}
} 
$productIds	= array_unique($proIds);
$this->postProducts($proIds);
?>