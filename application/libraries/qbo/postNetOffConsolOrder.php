<?php 
//QBO		: 	POST NETOFF CONSOL INVOICE OR CREDIT NOTE TO QBO
$dateResults	= array(1=> array('name'=> 'JAN','lastDate'=> '31',),2=> array('name'=> 'FEB','lastDate'=> '28','lastDate2'=> '29',),3=> array('name'=> 'MAR','lastDate'=> '31',),4=> array('name'=> 'APR','lastDate'=> '30',),5=> array('name'=> 'MAY','lastDate'=> '31',),6=> array('name'=> 'JUN','lastDate'=> '30',),7=> array('name'=> 'JUL','lastDate'=> '31',),8=> array('name'=> 'AUG','lastDate'=> '31',),9=> array('name'=> 'SEP','lastDate'=> '30',),10=> array('name'=> 'OCT','lastDate'=> '31',),11=> array('name'=> 'NOV','lastDate'=> '30',),12=> array('name'=> 'DEC','lastDate'=> '31',),);

$this->reInitialize();
$exchangeRates		= $this->getExchangeRate();
$enableAggregation	= $this->ci->globalConfig['enableAggregation'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	$exchangeRatetemp	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$salesOrderdatas	= $this->ci->db->order_by('orderId', 'asc')->get_where('sales_order',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	
	$this->ci->db->reset_query();
	$salesCreditdatas	= $this->ci->db->order_by('orderId', 'asc')->get_where('sales_credit_order',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	
	if(!$salesOrderdatas AND !$salesCreditdatas){
		continue;
	}
	
	
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]								= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
					}
				}
			}
		}
	}
	if(!$AggregationMappings AND !$AggregationMappings2){
		continue;
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$productMappings		= array();
	if($this->ci->globalConfig['enableAdvanceTaxMapping']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	if($this->ci->globalConfig['enableTaxMapping']){
		$this->ci->db->reset_query();
		$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
		$taxMappings		= array();
		if($taxMappingsTemps){
			foreach($taxMappingsTemps as $taxMappingsTemp){
				if($this->ci->globalConfig['enableAdvanceTaxMapping']){
					if($taxMappingsTemp['stateName']){
						$isStateEnabled	= 1;
					}
					if($taxMappingsTemp['countryName']){
						$isStateEnabled = 1;
					}
				}
			}
			foreach($taxMappingsTemps as $taxMappingsTemp){
				$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
				if($taxMappingsTemp['stateName']){
					foreach($stateTemp as $Statekey => $stateTemps){
						$stateName			= strtolower(trim($stateTemps));
						$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
						$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
						if($this->ci->globalConfig['enableAdvanceTaxMapping']){
							if($isStateEnabled){
								if($account1ChannelId){
									$isChannelEnabled		= 1;
									$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
									foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
										$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
										$taxMappings[strtolower($key)]	= $taxMappingsTemp;
									}
								}
								else{
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'];
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
				}
				else{
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($isStateEnabled){
						if($account1ChannelId){
							$isChannelEnabled	= 1;
							$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
							foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}			
				}
				if(!$isStateEnabled){
					$key							= $taxMappingsTemp['account1TaxId'];
					$taxMappings[strtolower($key)]	= $taxMappingsTemp;
				}
			}
		}
	}
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard	= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	
	if($salesOrderdatas OR $salesCreditdatas){
		$salesOrderBatch			= array();
		$salesCreditBatch			= array();
		$salesOrderProductDatas		= array();
		$salesCreditProductDatas	= array();
		if($salesOrderdatas){
			foreach($salesOrderdatas as $orderDatas){
				$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
				$taxDate				= '';
				$orderId				= $orderDatas['orderId'];
				$rowDatas				= json_decode($orderDatas['rowData'],true);
				$taxDate				= $rowDatas['invoices'][0]['taxDate'];
				$channelId				= $rowDatas['assignment']['current']['channelId'];
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
				$ConsolAPIFieldValueID	= '';
				
				if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
					continue;
				}
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				if($ConsolAPIFieldValueID){
					$CustomFieldValueID	= $ConsolAPIFieldValueID;
				}
				
				if($enableAggregation){
					if($AggregationMappings){
						if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($enableConsolidationMappingCustomazation){
								if($AggregationMappings[$channelId]['bpaccountingcurrency']){
									if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
								else{
									if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
					}
					elseif($AggregationMappings2){
						if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
								if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
							else{
								if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
						else{
							if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
					}
				}
				
				$AggregationMappingData	= array();
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
						$CustomFieldValueID		= 'NA';
					}
					else{
						$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
						$CustomFieldValueID		= 'NA';
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
					}
					else{
						$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
					}
				}
				
				$consolFrequency		= $AggregationMappingData['consolFrequency'];
				$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
				if(!$consolFrequency){
					$consolFrequency	= 1;
				}
				if(!$netOffConsolidation){
					continue;
				}
				
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$Acc2Offset		= 0;
				$diff			= $BPDateOffset - $Acc2Offset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff > 0){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
				
				//consolFrequency : 1 => Daily, 2 => Monthly
				if($consolFrequency == 1){
					$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
					
					if($clientcode != 'monicaandandy'){
						/* if($taxDate > $fetchTaxDate){
							continue;
						} */
					}
					else{
						$invoiceMonth	= date('m',strtotime($taxDate));
						$currentMonth	= date('m');
						$currentDate	= date('d');
						
						if($invoiceMonth == $currentMonth){
							continue;
						}
						else{
							if($currentDate < 3){
								continue;
							}
						}
					}
					
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesOrderBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesOrderBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
						}
					}
				}
				elseif($consolFrequency == 2){
					$invoiceMonth	= date('m',strtotime($taxDate));
					$currentMonth	= date('m');
					
					if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
						continue;
					}
					
					$batchKey	= date('y-m',strtotime($taxDate));
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesOrderBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesOrderBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
						}
					}
				}
			}
		}
		
		if($salesCreditdatas){
			foreach($salesCreditdatas as $orderDatas){
				$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
				$taxDate				= '';
				$orderId				= $orderDatas['orderId'];
				$rowDatas				= json_decode($orderDatas['rowData'],true);
				$taxDate				= $rowDatas['invoices'][0]['taxDate'];
				$channelId				= $rowDatas['assignment']['current']['channelId'];
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
				$ConsolAPIFieldValueID	= '';
				
				if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
					continue;
				}
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				if($ConsolAPIFieldValueID){
					$CustomFieldValueID	= $ConsolAPIFieldValueID;
				}
				
				if($enableAggregation){
					if($AggregationMappings){
						if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($enableConsolidationMappingCustomazation){
								if($AggregationMappings[$channelId]['bpaccountingcurrency']){
									if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
								else{
									if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
					}
					elseif($AggregationMappings2){
						if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
								if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
							else{
								if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											if(!$AggregatedCustomFieldData){
												$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
													$APIfieldValueTmps		= '';
													foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
														if(!$APIfieldValueTmps){
															$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
														}
														else{
															$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
														}
													}
													if($APIfieldValueTmps){
														$AggregatedCustomFieldData	= $APIfieldValueTmps;
													}
											}
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
						else{
							if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
					}
				}
				
				$AggregationMappingData	= array();
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
						$CustomFieldValueID		= 'NA';
					}
					else{
						$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
						$CustomFieldValueID		= 'NA';
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
					}
					else{
						$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
					}
				}
				
				$consolFrequency		= $AggregationMappingData['consolFrequency'];
				$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
				if(!$consolFrequency){
					$consolFrequency	= 1;
				}
				if(!$netOffConsolidation){
					continue;
				}
				
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$Acc2Offset		= 0;
				$diff			= $BPDateOffset - $Acc2Offset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff > 0){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
				
				//consolFrequency : 1 => Daily, 2 => Monthly
				if($consolFrequency == 1){
					$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
					
					if($clientcode != 'monicaandandy'){
						/* if($taxDate > $fetchTaxDate){
							continue;
						} */
					}
					else{
						$invoiceMonth	= date('m',strtotime($taxDate));
						$currentMonth	= date('m');
						$currentDate	= date('d');
						
						if($invoiceMonth == $currentMonth){
							continue;
						}
						else{
							if($currentDate < 3){
								continue;
							}
						}
					}
					
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesCreditBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesCreditBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
						}
					}
				}
				elseif($consolFrequency == 2){
					$invoiceMonth	= date('m',strtotime($taxDate));
					$currentMonth	= date('m');
					
					if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
						continue;
					}
					
					$batchKey	= date('y-m',strtotime($taxDate));
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesCreditBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
						}
						else{
							$salesCreditBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
						}
					}
				}
			}
		}
		
		
		//salesCreditCode/postaggregationsalesCredit
		if($salesCreditBatch){
			foreach($salesCreditBatch as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $CurrencyCode => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $CurrencyCode;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$results				= array();
							$AggregationMapping		= array();
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$consolFrequency		= 0;
								
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('sales_credit_order',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							
							$ChannelCustomer	= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef	= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount		= 0;
							$BPTotalAmountBase	= 0;
							$OrderCount			= 0;
							$invoiceLineCount	= 0;
							$linNumber			= 1;
							$request			= array();
							$InvoiceLineAdd		= array();
							$AllorderId			= array();
							$ProductArray		= array();
							$TotalTaxLines		= array();
							$ProductArrayCount	= 0;
							$totalItemDiscount	= 0;
							$exchangeRate		= 1;
							$invoiceFormat			= '';
							$taxDate				= '';
							$BPChannelName			= '';
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$bpconfig			= $this->ci->account1Config[$orderDatas['account1Id']];
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$exchangeRate		= 1;
									$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									$countryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
												}
											}
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bundleParentID	= '';
										$isBundleChild	= $rowdatass['composition']['bundleChild'];
										if($isBundleChild){
											$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
										}
										
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										if($isBundleChild AND $bundleParentID){
											$bpTaxID	= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
										}
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $bpconfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										$taxMapping				= array();
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowdatass['rowValue']['rowNet']['value'] == 0){
												continue;
											}
										}
										
										if($kidsTaxCustomField){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
											}
										}
										
										$taxMappingKey		= $rowdatass['rowValue']['taxClassId'];
										$taxMappingKey1		= $rowdatass['rowValue']['taxClassId'];
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode;
											}
										}
										
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
										if($taxMapping){
											if($taxMapping['account2LineTaxId']){
												$bpTaxID 	= $taxMapping['account2LineTaxId'];
												if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'])){
													$bpTaxID  = $taxMapping['account2KidsLineTaxId'];
												}
											}
											else{
												$bpTaxID 	= $config['salesNoTaxCode'];
											}
										}
										else{
											$bpTaxID 	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										if(!$AggregationMapping['SendTaxAsLine']){
											$invoiceFormat	= 'InlineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpTaxID])){
														$ProductArray['discount'][$bpTaxID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpTaxID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$bpTaxID])){
																	$ProductArray['discountCoupon'][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$bpTaxID][$customItemIdQbo])){
														$ProductArray['customItems'][$bpTaxID][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$bpTaxID][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
													}
												}
												else{
													if(isset($ProductArray[$bpTaxID])){
														$ProductArray[$bpTaxID]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray[$bpTaxID]['TotalNetAmt']	= $rowNet;
													}
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpTaxID])){
													$ProductArray['shipping'][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpTaxID])){
													$ProductArray['giftcard'][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpTaxID])){
													$ProductArray['couponitem'][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
										}
										else{
											$invoiceFormat	= 'SaparatelineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice = $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice = round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'])){
														$ProductArray['discount']['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount']['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'])){
																	$ProductArray['discountCoupon']['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon']['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$customItemIdQbo])){
														$ProductArray['customItems'][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
													}
												}
												else{
													if(isset($ProductArray['aggregationItem'])){
														$ProductArray['aggregationItem']['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['aggregationItem']['TotalNetAmt']	= $rowNet;
													}
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'])){
													$ProductArray['shipping']['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping']['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'])){
													$ProductArray['giftcard']['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard']['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'])){
													$ProductArray['couponitem']['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem']['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									$BPTotalAmount	+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$orderId		= $orderDatas['orderId'];
									$AllorderId[]	= $orderId;
									$BPChannelName	= $orderDatas['channelName'];
									$OrderCount++;
								}
							}
							else{
								continue;
							}
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['exchangeRate']				= $exchangeRate;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPChannelName']			= $BPChannelName;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlConfig']		= $brightpearlConfig;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['config']					= $config;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['productData']				= $ProductArray;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmountBase']		= $BPTotalAmountBase;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmount']			= $BPTotalAmount;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['OrderCount']				= $OrderCount;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['consolMappingSettings']	= $AggregationMapping;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['orderIds']['SC']			= $AllorderId;
						}
					}
				}
			}
		}
		
		//salesOrder Code/postaggregationsales
		if($salesOrderBatch){
			foreach($salesOrderBatch as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $CurrencyCode => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $CurrencyCode;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$results				= array();
							$AggregationMapping		= array();
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$consolFrequency		= 0;
							
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('sales_order',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							
							$ChannelCustomer	= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef	= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount		= 0;
							$BPTotalAmountBase	= 0;
							$OrderCount			= 0;
							$invoiceLineCount	= 0;
							$linNumber			= 1;
							$request			= array();
							$InvoiceLineAdd		= array();
							$AllorderId			= array();
							$ProductArray		= array();
							$TotalTaxLines		= array();
							$ProductArrayCount	= 0;
							$totalItemDiscount	= 0;
							$exchangeRate		= 1;
							$invoiceFormat		= '';
							$dueDate			= '';
							$taxDate			= '';
							$BPChannelName		= '';
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$bpconfig			= $this->ci->account1Config[$orderDatas['account1Id']];
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$exchangeRate		= 1;
									$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									$countryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									
									//////////////////////////////////////////////////////////////
									$dueDate		= $rowDatas['invoices']['0']['dueDate'];
									//dueDate chanages
									$BPDateOffset	= (int)substr($dueDate,23,3);
									$qboOffset		= 0;
									$diff			= $BPDateOffset - $qboOffset;
									$date			= new DateTime($dueDate);
									$BPTimeZone		= 'GMT';
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($diff > 0){
										$diff			.= ' hour';
										$date->modify($diff);
									}
									$dueDate		= $date->format('Y-m-d');
									//////////////////////////////////////////////////////////////
									
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
												}
											}
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bundleParentID	= '';
										$isBundleChild	= $rowdatass['composition']['bundleChild'];
										if($isBundleChild){
											$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
										}
										
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										if($isBundleChild AND $bundleParentID){
											$bpTaxID	= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
										}
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $bpconfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										$taxMapping				= array();
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowdatass['rowValue']['rowNet']['value'] == 0){
												continue;
											}
										}
										
										if($kidsTaxCustomField){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
											}
										}
										
										$taxMappingKey		= $rowdatass['rowValue']['taxClassId'];
										$taxMappingKey1		= $rowdatass['rowValue']['taxClassId'];
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode;
											}
										}
										
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
										if($taxMapping){
											if($taxMapping['account2LineTaxId']){
												$bpTaxID 	= $taxMapping['account2LineTaxId'];
												if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'])){
													$bpTaxID  = $taxMapping['account2KidsLineTaxId'];
												}
											}
											else{
												$bpTaxID 	= $config['salesNoTaxCode'];
											}
										}
										else{
											$bpTaxID 	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										if(!$AggregationMapping['SendTaxAsLine']){
											$invoiceFormat	= 'InlineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpTaxID])){
														$ProductArray['discount'][$bpTaxID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpTaxID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$bpTaxID])){
																	$ProductArray['discountCoupon'][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$bpTaxID][$customItemIdQbo])){
														$ProductArray['customItems'][$bpTaxID][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$bpTaxID][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
													}
												}
												else{
													if(isset($ProductArray[$bpTaxID])){
														$ProductArray[$bpTaxID]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray[$bpTaxID]['TotalNetAmt']	= $rowNet;
													}
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpTaxID])){
													$ProductArray['shipping'][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpTaxID])){
													$ProductArray['giftcard'][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpTaxID])){
													$ProductArray['couponitem'][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
										}
										else{
											$invoiceFormat	= 'SaparatelineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice = $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice = round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'])){
														$ProductArray['discount']['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount']['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'])){
																	$ProductArray['discountCoupon']['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon']['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$customItemIdQbo])){
														$ProductArray['customItems'][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
													}
												}
												else{
													if(isset($ProductArray['aggregationItem'])){
														$ProductArray['aggregationItem']['TotalNetAmt']	+= $rowNet;
													}
													else{
														$ProductArray['aggregationItem']['TotalNetAmt']	= $rowNet;
													}
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'])){
													$ProductArray['shipping']['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping']['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'])){
													$ProductArray['giftcard']['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard']['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'])){
													$ProductArray['couponitem']['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem']['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'])){
													$ProductArray['allTax']['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax']['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									$BPTotalAmount		+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$orderId			= $orderDatas['orderId'];
									$AllorderId[]		= $orderId;
									$BPChannelName		= $orderDatas['channelName'];
									$OrderCount++;
								}
							}
							else{
								continue;
							}
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['dueDate']				= $dueDate;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['exchangeRate']			= $exchangeRate;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPChannelName']			= $BPChannelName;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlConfig']		= $brightpearlConfig;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['config']				= $config;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['productData']			= $ProductArray;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmountBase']		= $BPTotalAmountBase;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmount']			= $BPTotalAmount;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['OrderCount']			= $OrderCount;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['consolMappingSettings']	= $AggregationMapping;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['orderIds']['SO']		= $AllorderId;
						}
					}
				}
			}
		}
		
		if($salesOrderProductDatas AND $salesCreditProductDatas){
			foreach($salesOrderProductDatas as $invoiceFormat => $salesOrderProductDatasTemp1){
				foreach($salesOrderProductDatasTemp1 as $taxDateBP => $salesOrderProductDatasTemp2){
					foreach($salesOrderProductDatasTemp2 as $SalesChannel => $salesOrderProductDatasTemp3){
						foreach($salesOrderProductDatasTemp3 as $CurrencyCode => $salesOrderProductDatasTemp4){
							foreach($salesOrderProductDatasTemp4 as $CustomFieldValueID => $salesOrderDatasArray){
								if($salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]){
									$creditDataforNetOff	= $salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['productData'];
									$creditIds				= $salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['orderIds']['SC'];
									$salesDataforNetOff		= $salesOrderDatasArray['productData'];
									$salesIds				= $salesOrderDatasArray['orderIds']['SO'];
									if($invoiceFormat == 'SaparatelineTax'){
										foreach($salesDataforNetOff as $ItmeType => $ProductArrayTemp){
											if($creditDataforNetOff[$ItmeType]){
												if($ProductArrayTemp['TotalNetAmt'] >= $creditDataforNetOff[$ItmeType]['TotalNetAmt']){
													$salesDataforNetOff[$ItmeType]['TotalNetAmt']	= ($ProductArrayTemp['TotalNetAmt'] - $creditDataforNetOff[$ItmeType]['TotalNetAmt']);
													unset($creditDataforNetOff[$ItmeType]);
												}
											}
										}
										if($creditDataforNetOff){
											foreach($creditDataforNetOff as $dataforNetOffKey1 => $dataforNetOffTemp1){
												if(empty($dataforNetOffTemp1)){
													unset($creditDataforNetOff[$dataforNetOffKey1]);
												}
											}
										}
										if($salesDataforNetOff){
											foreach($salesDataforNetOff as $dataforNetOffKey1 => $dataforNetOffTemp1){
												if(empty($dataforNetOffTemp1)){
													unset($salesDataforNetOff[$dataforNetOffKey1]);
												}
											}
										}
										$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['productData']		= $salesDataforNetOff;
										$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['orderIds']['SC']	= $creditIds;
										$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['productData']		= $creditDataforNetOff;
										$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['orderIds']['SO']	= $salesIds;
									}
								}
							}
						}
					}
				}
			}
			
			
			foreach($salesOrderProductDatas as $invoiceFormat => $salesOrderProductDatasTemp1){
				foreach($salesOrderProductDatasTemp1 as $taxDateBP => $salesOrderProductDatasTemp2){
					foreach($salesOrderProductDatasTemp2 as $SalesChannel => $salesOrderProductDatasTemp3){
						foreach($salesOrderProductDatasTemp3 as $CurrencyCode => $salesOrderProductDatasTemp4){
							foreach($salesOrderProductDatasTemp4 as $CustomFieldValueID => $salesOrderDatasArray){
								$netOffTotalInBase		= 0;
								$AllorderId				= $salesOrderDatasArray['orderIds']['SO'];
								$netOffTotalInBase		= $salesOrderDatasArray['BPTotalAmountBase'];
								$netOffTotalInOrder		= 0;
								$netOffTotalInOrder		= $salesOrderDatasArray['BPTotalAmount'];
								$BPChannelName			= $salesOrderDatasArray['BPChannelName'];
								$exchangeRate			= $salesOrderDatasArray['exchangeRate'];
								$dueDate				= $salesOrderDatasArray['dueDate'];
								$invoiceLineCount		= 0;
								$linNumber				= 1;
								$request				= array();
								$InvoiceLineAdd			= array();
								$brightpearlConfig		= $salesOrderDatasArray['brightpearlConfig'];
								$config					= $salesOrderDatasArray['config'];
								$OrderCount				= $salesOrderDatasArray['OrderCount'];
								$ProductArray			= $salesOrderDatasArray['productData'];
								$consolMappingSettings	= $salesOrderDatasArray['consolMappingSettings'];
								$uniqueInvoiceRef		= $consolMappingSettings['uniqueChannelName'];
								$orderForceCurrency		= $consolMappingSettings['orderForceCurrency'];
								$consolFrequency		= $consolMappingSettings['consolFrequency'];
								$ChannelCustomer		= $consolMappingSettings['account2ChannelId'];
								if(!$ChannelCustomer){continue;}
								$OrderPostCurrencyCode	= strtoupper($CurrencyCode);
								if(!$consolFrequency){
									$consolFrequency	= 1;
								}
								if($orderForceCurrency){
									$OrderPostCurrencyCode	= strtoupper($brightpearlConfig['currencyCode']);
									$exchangeRate			= 1;
								}
								
								$isInvoiceRequired			= 1;
								$isCreditNoteRequired		= 0;
								$totalInvoicePostAmt		= 0;
								$totalCreditNotePostAmt		= 0;
								if($ProductArray){
									if(!$consolMappingSettings['SendTaxAsLine']){
										//TBD
									}
									else{
										foreach($ProductArray as $tempKey1 => $tempArray1){
											$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray1['TotalNetAmt']);
										}
										
										foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
											if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
												$InvoiceLineAdd[$invoiceLineCount]	= array(
													'LineNum'				=> $linNumber++,
													'Description'			=> "Consolidation of ".$OrderCount.' invoices',
													'Amount'				=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
													'DetailType'			=> 'SalesItemLineDetail',
													'SalesItemLineDetail'	=> array(
														'ItemRef'				=> array('value' => $consolMappingSettings['AggregationItem']),
														'Qty'					=> 1,
														'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
														'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
													),
												);
												if($keyproduct == 'shipping'){
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
													$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
												}
												if($keyproduct == 'giftcard'){
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
													$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
												}
												if($keyproduct == 'couponitem'){
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
													$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon';
												}
												if($keyproduct == 'discount'){
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['discountItem']);
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatasTemp['TotalNetAmt']));
													$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
													$InvoiceLineAdd[$invoiceLineCount]['Amount']		= sprintf("%.4f",((-1) * $ProductArrayDatasTemp['TotalNetAmt']));
												}
												if($keyproduct == 'discountCoupon'){
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
													$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatasTemp['TotalNetAmt']));
													$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
													$InvoiceLineAdd[$invoiceLineCount]['Amount']		= sprintf("%.4f",((-1) * $ProductArrayDatasTemp['TotalNetAmt']));
												}
												$invoiceLineCount++;
											}
											elseif($keyproduct == 'customItems'){
												foreach($ProductArrayDatasTemp as $customItemId	=> $customItemIdTemp){
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> 'Special Item',
														'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $customItemId),
															'Qty'					=> 1,
															'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
															'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
														),
													);
													$invoiceLineCount++;
												}
											}
											elseif($keyproduct == 'allTax'){
												if($ProductArrayDatasTemp['TotalNetAmt'] <= 0){continue;}
												$mappedtaxItem		= '';
												$mappedtaxItem		= $consolMappingSettings['defaultTaxItem'];
												$InvoiceLineAdd[$invoiceLineCount]	= array(
													'LineNum'				=> $linNumber++,
													'Description'			=> 'Total Tax Amount',
													'Amount'				=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
													'DetailType'			=> 'SalesItemLineDetail',
													'SalesItemLineDetail'	=> array(
														'ItemRef'				=> array('value' => $mappedtaxItem),
														'Qty'					=> 1,
														'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
														'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
													),
												);
												$invoiceLineCount++;
											}
										}
									}
								}
								
								
								$allNetOffSalesCreditData	= array();
								$netOffSalesCreditIds		= array();
								$totalSOCount			 	= $OrderCount;
								$totalSCCount			 	= 0;
								if($salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]){
									$allNetOffSalesCreditData	= $salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID];
									$allNetOffCreditItemsData	= $allNetOffSalesCreditData['productData'];
									$netOffSalesCreditIds		= $allNetOffSalesCreditData['orderIds']['SC'];
									$OrderCount					= ($OrderCount + $allNetOffSalesCreditData['OrderCount']);
									$totalSCCount				= ($allNetOffSalesCreditData['OrderCount']);
									$netOffTotalInBase			= ($netOffTotalInBase - $allNetOffSalesCreditData['BPTotalAmountBase']);
									$netOffTotalInOrder			= ($netOffTotalInOrder - $allNetOffSalesCreditData['BPTotalAmount']);
									if(!empty($allNetOffCreditItemsData)){
										if(!$consolMappingSettings['SendTaxAsLine']){
											//To do
										}
										else{
											foreach($allNetOffCreditItemsData as $tempKey1 => $tempArray1){
												$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray1['TotalNetAmt']);
											}
											
											foreach($allNetOffCreditItemsData as $keyproduct => $allNetOffCreditItemsDataTempTemp){
												if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> "Consolidation of ".$OrderCount.' invoices',
														'Amount'				=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $consolMappingSettings['AggregationItem']),
															'Qty'					=> 1,
															'UnitPrice'				=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
															'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
														),
													);
													if($keyproduct == 'shipping'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
													}
													if($keyproduct == 'giftcard'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
													}
													if($keyproduct == 'couponitem'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon';
													}
													if($keyproduct == 'discount'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['discountItem']);
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= (-1) * (sprintf("%.4f",((-1) * $allNetOffCreditItemsDataTempTemp['TotalNetAmt'])));
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
														$InvoiceLineAdd[$invoiceLineCount]['Amount']		= (-1) * (sprintf("%.4f",((-1) * $allNetOffCreditItemsDataTempTemp['TotalNetAmt'])));
													}
													if($keyproduct == 'discountCoupon'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= (-1) * (sprintf("%.4f",((-1) * $allNetOffCreditItemsDataTempTemp['TotalNetAmt'])));
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
														$InvoiceLineAdd[$invoiceLineCount]['Amount']		= (-1) * (sprintf("%.4f",((-1) * $allNetOffCreditItemsDataTempTemp['TotalNetAmt'])));
													}
													
													$invoiceLineCount++;
												}
												elseif($keyproduct == 'customItems'){
													foreach($allNetOffCreditItemsDataTempTemp as $customItemId	=> $customItemIdTemp){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> 'Special Item',
															'Amount'				=> (-1) * (sprintf("%.4f",$customItemIdTemp['TotalNetAmt'])),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $customItemId),
																'Qty'					=> 1,
																'UnitPrice'				=> (-1) * (sprintf("%.4f",$customItemIdTemp['TotalNetAmt'])),
																'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
															),
														);
														$invoiceLineCount++;
													}
												}
												elseif($keyproduct == 'allTax'){
													if($allNetOffCreditItemsDataTempTemp['TotalNetAmt'] <= 0){continue;}
														$mappedtaxItem		= '';
														$mappedtaxItem		= $consolMappingSettings['defaultTaxItem'];
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> 'Total Tax Amount',
															'Amount'				=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $mappedtaxItem),
																'Qty'					=> 1,
																'UnitPrice'				=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
																'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
															),
														);
													
													$invoiceLineCount++;
												}
											}
										}
									}
								}
								
								if($totalCreditNotePostAmt > $totalInvoicePostAmt){
									$isCreditNoteRequired		= 1;
								}
								
								if($InvoiceLineAdd){
									if($consolFrequency == 1){
										$sendTaxDate		= $taxDateBP;
										$sendDueDate		= $dueDate;
										$RefForInvoice		= $taxDateBP;
										$ReferenceNumber	= date('Ymd',strtotime($RefForInvoice)).'-'.$uniqueInvoiceRef.'-'.$CurrencyCode;
									}
									else{
										$isLeap				= 0;
										$invMonth			= (int)substr($taxDateBP,3,2);
										$invYear			= (int)substr($taxDateBP,0,2);
										$invYearFull		= '20'.$invYear;
										
										if(((int)$invYearFull % 4) == 0){
											if(((int)$invYearFull % 100) == 0){
												if(((int)$invYearFull % 400) == 0){
													$isLeap	= 1;
												}
												else{
													$isLeap	= 0;
												}
											}
											else{
												$isLeap	= 1;
											}
										}
										else{
											$isLeap	= 0;
										}
										
										$invDate	= $dateResults[$invMonth]['lastDate'];
										if($invMonth == 2){
											if($isLeap){
												$invDate	= $dateResults[$invMonth]['lastDate2'];										
											}
										}
										$invMonthName	= $dateResults[$invMonth]['name'];
										if(strlen($invMonth) == 1){
											$invMonth	= (string)('0'.$invMonth);
										}
										$sendTaxDate		= $invYearFull.'-'.$invMonth.'-'.$invDate;
										$sendDueDate		= $sendTaxDate;
										$RefForInvoice		= $invMonthName.$invYear;
										$ReferenceNumber	= $RefForInvoice.'-'.$uniqueInvoiceRef.'-'.$CurrencyCode;
									}
								
									$aggreagationID			= uniqid((strtoupper(trim($BPChannelName))).'-');
									$ReferenceNumber		= substr($ReferenceNumber,0,21);
									
									$CheckReferenceNumber	= array();
									$CheckReferenceNumber	= $this->ci->db->get_where('sales_credit_order',array('invoiceRef' => $ReferenceNumber, 'account2Id' => $account2Id))->row_array();
									if($CheckReferenceNumber){
										$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('message' => 'DocNumber is already exists in QBO'));
										if($netOffSalesCreditIds){
											$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('message' => 'DocNumber is already exists in QBO'));
										}
										continue;
									}
									
									if($isCreditNoteRequired){
										foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
											$InvoiceLineAdd[$LineSeq]['Amount']								= ((-1) * ($InvoiceLineAdd[$LineSeq]['Amount']));
											$InvoiceLineAdd[$LineSeq]['SalesItemLineDetail']['UnitPrice']	= ((-1) * ($InvoiceLineAdd[$LineSeq]['Amount']));
										}
									}
									
									foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
										$InvoiceLineAdd[$LineSeq]['Description']	= "Consolidation of ".$totalSOCount." sales orders";
										if($totalSCCount){
											$InvoiceLineAdd[$LineSeq]['Description']	= "Consolidation of ".$totalSOCount." sales orders & ".$totalSCCount." sales credits";
										}
										if($InvoiceLineAddTemp['Description'] == 'Special Item'){
											unset($InvoiceLineAdd[$LineSeq]['Description']);
										}
									}
									
									$request			= array(
										'DocNumber'			=> $ReferenceNumber,
										'CustomerRef'		=> array('value' => $ChannelCustomer),
										'Line'				=> $InvoiceLineAdd,
										'ExchangeRate'		=> sprintf("%.4f",(1 / $exchangeRate)),
										'CurrencyRef'		=> array('value' => strtoupper($OrderPostCurrencyCode)),
										'TxnDate'			=> $sendTaxDate,
										'DueDate'			=> $sendTaxDate,
									);
									if($dueDate){
										$request['DueDate']	= $dueDate;
									}
									if(isset($channelMappings[$SalesChannel])){
										$request['ClassRef']	= array('value' => $channelMappings[$SalesChannel]['account2ChannelId']);
									}
									
									if(($config['defaultCurrrency']) AND ($brightpearlConfig['currencyCode'] != $config['defaultCurrrency'])){
										$exRate = $this->getQboExchangeRateByDb($account2Id,$OrderPostCurrencyCode,$config['defaultCurrrency'],$sendTaxDate);
										if($exRate){
											$request['ExchangeRate'] = $exRate;
										}
										else{
											$exRate = $exchangeRatetemp[strtolower($OrderPostCurrencyCode)][strtolower($config['defaultCurrrency'])]['Rate'];
											if($exRate){
												$request['ExchangeRate'] = $exRate;
											}
											else{
												echo 'ExchangeRate Not found Line no - 2289';continue;
												unset($request['ExchangeRate']);
											}
										}
									}
									echo "request<pre>";print_r($request); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
									if($request AND $InvoiceLineAdd){
										if($disableSalesPosting){
											$createdRowData	= array(
												'Request data	: '	=> $request,
												'Response data	: '	=> 'POSTING_IS_DISABLED',
											);
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											if($netOffSalesCreditIds){
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											}
											$fakecreateOrderId	= uniqid("QBO_ID_").strtotime("now");
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'IsJournalposted' => $journalAggregation, 'qboContactID' => $ChannelCustomer, 'consolPostOptions'=> 1));
											if($netOffSalesCreditIds){
												$fakecreateOrderId	= uniqid("QBO_ID_").strtotime("now");
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'qboContactID' => $ChannelCustomer, 'consolPostOptions'=> 1));
											}
										}
										else{
											if($isCreditNoteRequired){
												$url		= 'creditmemo?minorversion=37';
											}
											else{
												$url		= 'invoice?minorversion=37';
											}
											$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
											$createdRowData	= array(
												'Request data	: '	=> $request,
												'Response data	: '	=> $results,
											);
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											if($netOffSalesCreditIds){
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											}
											if((isset($results['Invoice']['Id'])) OR (isset($results['CreditMemo']['Id']))){
												$consolPostOptions	= 0;
												if($disableCOGSPosting){
													$consolPostOptions	= 2;
												}
												if($isCreditNoteRequired){
													$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('isNetOff' => 1, 'linkedWithCredit' => $results['CreditMemo']['Id'], 'uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['CreditMemo']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'IsJournalposted' => $journalAggregation, 'qboContactID' => $ChannelCustomer, 'consolPostOptions'=> $consolPostOptions));
													if($netOffSalesCreditIds){
														$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('isNetOff' => 1,'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['CreditMemo']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'createdRowData' => json_encode($createdRowData)));
													}
												}
												else{
													$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('isNetOff' => 1, 'uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoice']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'IsJournalposted' => $journalAggregation, 'qboContactID' => $ChannelCustomer, 'consolPostOptions'=> $consolPostOptions));
													if($netOffSalesCreditIds){
														$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('isNetOff' => 1, 'linkedWithInvoice' => $results['Invoice']['Id'], 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoice']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'createdRowData' => json_encode($createdRowData)));
													}
												}
												
												if($isCreditNoteRequired){
													$QBOTotalAmount		= $results['CreditMemo']['TotalAmt'];
												}
												else{
													$QBOTotalAmount		= $results['Invoice']['TotalAmt'];
												}
												
												if($orderForceCurrency){
													$NetRoundOff	= $netOffTotalInBase - $XeroTotalAmount;
												}
												else{
													$NetRoundOff	= $netOffTotalInOrder - $XeroTotalAmount;
												}
												
												$RoundOffCheck		= abs($NetRoundOff);
												$RoundOffCheck		= sprintf("%.4f",$RoundOffCheck);
												$RoundOffApplicable	= 0;
												if($RoundOffCheck != 0){
													if($RoundOffCheck < 0.99){
														$RoundOffApplicable = 1;
													}
													if($RoundOffApplicable){
														$InvoiceLineAdd[$invoiceLineCount] = array(
															'LineNum'				=> $linNumber++,
															'Description'			=> 'RoundOffItem',
															'Amount'				=> sprintf("%.4f",$NetRoundOff),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef' 				=> array(
																	'value'					=> $config['roundOffItem']
																),
																'Qty' 					=> 1,
																'UnitPrice' 			=> sprintf("%.4f",$NetRoundOff),
																'TaxCodeRef' 			=> array(
																	'value'					=> $config['salesNoTaxCode'] 
																),
															),
														);
														$request['Line']		= $InvoiceLineAdd;
														$request['SyncToken']	= 0;
														if($isCreditNoteRequired){
															$request['Id']		= $results['CreditMemo']['Id'];
															$url				= 'creditmemo?minorversion=37';  
														}
														else{
															$request['Id']		= $results['Invoice']['Id'];
															$url				= 'invoice?minorversion=37';  
														}
														
														$results2	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
														$createdRowData['Rounding Request data	: ']	= $request;
														$createdRowData['Rounding Response data	: ']	= $results2;
														
														if((isset($results['Invoice']['Id'])) OR (isset($results['CreditMemo']['Id']))){
															$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('status' => '1', 'createdRowData' => json_encode($createdRowData)));
															if($netOffSalesCreditIds){
																$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('status' => '1', 'createdRowData' => json_encode($createdRowData)));
															
															}
														}
														else{
															$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)));
															if($netOffSalesCreditIds){
																$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)));
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}