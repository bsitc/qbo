<?php
//qbo
$this->reInitialize();
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$enableCOGSJournals			= $this->ci->globalConfig['enableCOGSJournals'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	if(($postedAccount2Id) AND ($account2Id != $postedAccount2Id)){
		continue;
	}
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('productId',$orgObjectId);
	}
	$datas	= $this->ci->db->where_in('status',array('0','2'))->get_where('products',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappings		= array();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']]	= $taxMappingsTemp;
		}
	}
	
	if($datas){
		$pid				= array_column($datas, 'productId');
		$productPriceLists	= $this->ci->{$this->ci->globalConfig['fetchProduct']}->getProductPriceList($pid);	
		foreach($datas as $productDatas){
			if(!$productDatas['sku']){continue;}
			if(($productDatas['status'] == 4) OR ($productDatas['status'] == 3)){
				//skip the error and archived produtcs for sending
				continue;
			}
			$config1			= $this->ci->account1Config[$productDatas['account1Id']];
			$productId			= $productDatas['productId'];
			$productPriceList	= $productPriceLists[$productId];
			$SalesPrice			= 0.00;
			$PurchaseCost		= 0.00;
			
			if(@$productPriceList[$config['defaultPriceListForRetail']] > 0){
				$SalesPrice		= $productPriceList[$config['defaultPriceListForRetail']];
			}
			if(@$productPriceList[$config['defaultPriceListForProduct']] > 0){
				$PurchaseCost	= $productPriceList[$config['defaultPriceListForProduct']]; 
			}
			
			$description		= strip_tags($productDatas['description']);
			$params				= json_decode($productDatas['params'],true);
			$AssetAccountRef	= $config['AssetAccountRef'];
			$ExpenseAccountRef	= $config['ExpenseAccountRef'];
			$IncomeAccountRef	= $config['IncomeAccountRef'];
			
			
			if(isset($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
				$IncomeAccountRef	= $nominalMappings[$params['nominalCodeSales']]['account2NominalId'];
			}
			elseif(isset($nominalChannelMappings[$params['nominalCodeSales']]['account2NominalId'])){
				$IncomeAccountRef	= $nominalChannelMappings[$params['nominalCodeSales']]['account2NominalId'];
			}
			
			if(isset($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
				$ExpenseAccountRef	= $nominalMappings[$params['nominalCodePurchases']]['account2NominalId'];
			}
			elseif(isset($nominalChannelMappings[$params['nominalCodePurchases']]['account2NominalId'])){
				$ExpenseAccountRef	= $nominalChannelMappings[$params['nominalCodePurchases']]['account2NominalId'];
			}
			
			if(isset($nominalMappings[$params['nominalCodeStock']]['account2NominalId'])){
				$AssetAccountRef	= $nominalMappings[$params['nominalCodeStock']]['account2NominalId'];
			}
			elseif(isset($nominalChannelMappings[$params['nominalCodeStock']]['account2NominalId'])){
				$AssetAccountRef	= $nominalChannelMappings[$params['nominalCodeStock']]['account2NominalId'];
			}
			
			$request	= array();
			if($productDatas['createdProductId']){
				$query		= "select * from item where id = '".$productDatas['createdProductId']."'";
				$url		= "query?minorversion=4&query=".rawurlencode($query);
				$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id];
				$request	= $serchRes['QueryResponse']['Item']['0'];
			}
			else{
				$query		= "select * from item where sku = '".$productDatas['sku']."'";
				$url		= "query?minorversion=4&query=".rawurlencode($query);
				$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id];
				if(@$serchRes['QueryResponse']['Item']){
					foreach($serchRes['QueryResponse']['Item'] as $fetchPro){
						if((strtolower(trim($productDatas['sku']))) == (strtolower(trim($fetchPro['Name'])))){
							$request	= $fetchPro;
						}
					}
				}
				if(!$request){
					$query		= "select * from item where name = '".$productDatas['sku']."'";
					$url		= "query?minorversion=4&query=".rawurlencode($query);
					$serchRes	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id];
					if(@$serchRes['QueryResponse']['Item']){
						foreach($serchRes['QueryResponse']['Item'] as $fetchPro){
							if((strtolower(trim($productDatas['sku']))) == (strtolower(trim($fetchPro['Name'])))){
								$request	= $fetchPro;
							}
						}
					}
				}
			}
			
			$request['Name'] 				= $productDatas['sku']; 
			$request['FullyQualifiedName']	= $productDatas['sku']; 
			$request['Sku'] 				= $productDatas['sku']; 
			$request['Description'] 		= $productDatas['name'];
			$request['PurchaseDesc'] 		= $productDatas['name'];
			$request['IncomeAccountRef'] 	= array('value' => $IncomeAccountRef); 
			$request['ExpenseAccountRef'] 	= array('value' => $ExpenseAccountRef);  
			$request['AssetAccountRef'] 	= array('value' => $AssetAccountRef);
			$request['Taxable'] 			= $params['financialDetails']['taxable'];
			$request['Type'] 				= ($params['stock']['stockTracked'])?('Inventory'):('Service');
			$request['UnitPrice'] 			= $SalesPrice;
			$request['PurchaseCost'] 		= $PurchaseCost;
			
			if(!isset($request['TrackQtyOnHand'])){
				$request['TrackQtyOnHand'] = $params['stock']['stockTracked'];
			}
			
			if(!$request['InvStartDate']){
				if(date('Y',strtotime($productDatas['created'])) < 2019){
					$request['InvStartDate']	= '2019-01-01'; 
				}
				else{
					$request['InvStartDate']	= gmdate('Y-m-d',strtotime($productDatas['created']." -2 days")); 
				}
			} 
			if(!$request['SalesTaxIncluded']){
				$request['SalesTaxIncluded']	= false; 
			}
			if(!$request['QtyOnHand']){
				$request['QtyOnHand']			= 0;
			}
			if(strtolower($config['accountType']) != 'us'){
				if(isset($params['financialDetails']['taxCode'])){
					$bpTaxId	= @$params['financialDetails']['taxCode']['id'];
					if($bpTaxId){
						if($taxMappings[$bpTaxId]){
							$request['SalesTaxCodeRef']		= array('value' => $taxMappings[$bpTaxId]['account2LineTaxId']);
							$request['PurchaseTaxCodeRef']	= array('value' => $taxMappings[$bpTaxId]['account2LineTaxId']);
						}
					}
				}
			}
			
			//if $config['InventoryManagementEnabled'] == true that means connector is non-inventory managed	nominalChannelMappings
			if($config['InventoryManagementEnabled'] == true){
				$InventMgt_SellNominalCode		= '';
				$InventMgt_PurchaseNominalCode	= '';
				
				if((isset($nominalMappings[$params['nominalCodeSales']])) AND ($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
					$InventMgt_SellNominalCode		= $nominalMappings[$params['nominalCodeSales']]['account2NominalId'];
				}
				elseif((isset($nominalChannelMappings[$params['nominalCodeSales']])) AND ($nominalChannelMappings[$params['nominalCodeSales']]['account2NominalId'])){
					$InventMgt_SellNominalCode		= $nominalChannelMappings[$params['nominalCodeSales']]['account2NominalId'];
				}
				
				if((isset($nominalMappings[$params['nominalCodePurchases']])) AND ($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
					$InventMgt_PurchaseNominalCode	= $nominalMappings[$params['nominalCodePurchases']]['account2NominalId'];
				}
				elseif((isset($nominalChannelMappings[$params['nominalCodePurchases']])) AND ($nominalChannelMappings[$params['nominalCodePurchases']]['account2NominalId'])){
					$InventMgt_PurchaseNominalCode	= $nominalChannelMappings[$params['nominalCodePurchases']]['account2NominalId'];
				}
				
				if(!$InventMgt_SellNominalCode){
					$InventMgt_SellNominalCode 		= $config['InventoryManagementProductSellNominalCode'];
				}
				if(!$InventMgt_PurchaseNominalCode){
					$InventMgt_PurchaseNominalCode	= $config['InventoryManagementProductPurchaseNominalCode'];
				}
				if($params['stock']['stockTracked']){
					$request['Type']				= 'Service';
					$request['IncomeAccountRef'] 	= array('value' => $InventMgt_SellNominalCode); 
					$request['ExpenseAccountRef']	= array('value' => $InventMgt_PurchaseNominalCode);
					unset($request['AssetAccountRef']);
					unset($request['QtyOnHand']);
					unset($request['TrackQtyOnHand']);
				}
				if($enableCOGSJournals){
					if($params['stock']['stockTracked']){
						$request['ExpenseAccountRef']	= array('value' => $config['AssetAccountRef']);
						
						if((isset($nominalMappings[$params['nominalCodeStock']])) AND ($nominalMappings[$params['nominalCodeStock']]['account2NominalId'])){
							$request['ExpenseAccountRef']	= array('value' => $nominalMappings[$params['nominalCodeStock']]['account2NominalId'] );
						}
						elseif((isset($nominalChannelMappings[$params['nominalCodeStock']])) AND ($nominalChannelMappings[$params['nominalCodeStock']]['account2NominalId'])){
							$request['ExpenseAccountRef']	= array('value' => $nominalChannelMappings[$params['nominalCodeStock']]['account2NominalId'] );
						}
					}
				}
			}
			
			if($request['Type'] == 'Service'){
				$request['Type'] = 'NonInventory';
			}
			
			$url	= 'item?include=donotupdateaccountontxns&minorversion=37';
			if(isset($this->extraProductParams[$account2Id][$productDatas['productId']])){
				$extraProductParams	= $this->extraProductParams[$account2Id][$productDatas['productId']];
				if(!$this->extraProductParams[$account2Id][$productDatas['productId']]['poststatus']){
					$request['QtyOnHand']		= $extraProductParams['qty'];
					$request['PurchaseCost']	= $extraProductParams['unitPrice'];
					$request['InvStartDate']	= date('Y-m-d',strtotime($extraProductParams['availableDate']));
					$this->extraProductParams[$account2Id][$productDatas['productId']]['poststatus']	= 1;
				}
				else{
					$request['QtyOnHand']	= $extraProductParams['qty'];
				}
			}
			else{
				//continue;
			}
			$results		= @$this->getCurl($url, 'POST', json_encode($request), 'json',$account2Id)[$account2Id]; 
			$ceatedParams	= array(
				'Post Request URL	: '	=> $url,
				'Post Request Data	: '	=> $request,
				'Post Response Data : '	=> $results,
			);
			$this->ci->db->where(array('id' => $productDatas['id']))->update('products', array('ceatedParams' => json_encode($ceatedParams) )); 
			if(@$results['Item']['Id']){
				$this->ci->db->where(array('id' => $productDatas['id']))->update('products', array('status' => '1','createdProductId' => $results['Item']['Id'] ));
				if(isset($this->extraProductParams[$account2Id][$productDatas['productId']])){ 
					$this->ci->db->where(array('sku' => $productDatas['sku']))->update('product_load',array('status' => '1'));
				}
			}
			
			//new logStoringFunctionality
			$path	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account2'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'products'. DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path.$productId.'.logs',"\n\n QBO Log Added On : ".date('c')." \n". json_encode($ceatedParams),FILE_APPEND);
		}
	}
}
?>