<?php
//qbo
$this->reInitialize();
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$enableCOGSJournals			= $this->ci->globalConfig['enableCOGSJournals'];
$exchangeRates				= $this->getExchangeRate();
$UnInvoicingEnabled			= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('purchase_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$this->postProductAccount	= $account2Id;
	$exchangeRate				= $exchangeRates[$account2Id];
	$config						= $this->accountConfig[$account2Id];
	$ignoreLineDiscount			= $config['sendNetPriceExcludeDiscount'];
	
	$supplierInfos		= array();
	$batchInvoicePO		= array();
	$allBatchInvoiceIDs	= array();
	if($config['enablePurchaseConsol']){
		$allDistnictSuppliers	= array();
		if($config['purchaseConsolBasedON'] == 'suppplierAccountCode'){
			$allSupplierInfo		= array();
			$allDistnictSuppliers	= array_column($datas,'customerId');
			$allDistnictSuppliers	= array_unique($allDistnictSuppliers);
			$allDistnictSuppliers	= array_unique($allDistnictSuppliers);
			if($allDistnictSuppliers){
				$this->ci->db->reset_query();
				$allSupplierInfo	= $this->ci->db->where_in('customerId',$allDistnictSuppliers)->select('customerId, accountReference')->get_where('customers',array('account2Id' => $account2Id))->result_array();;
				if($allSupplierInfo){
					foreach($allSupplierInfo as $allSupplierInfoTemp){
						if($allSupplierInfoTemp['accountReference']){
							$supplierInfos[$allSupplierInfoTemp['customerId']]	= $allSupplierInfoTemp['accountReference'];
						}
					}
				}
			}
		}
		foreach($datas as $datasTemp){
			if((!$config['purchaseConsolBasedON']) OR ($config['purchaseConsolBasedON'] == 'supplierID')){
				if($datasTemp['customerId'] AND $datasTemp['bpInvoiceNumber']){
					$batchInvoicePO[$datasTemp['customerId'].'~'.$datasTemp['bpInvoiceNumber']][]	= $datasTemp;
				}
			}
			else{
				if($supplierInfos[$datasTemp['customerId']]){
					$uniqueSupplierRef	= $supplierInfos[$datasTemp['customerId']];
					if($datasTemp['customerId'] AND $datasTemp['bpInvoiceNumber']){
						$batchInvoicePO[$uniqueSupplierRef.'~'.$datasTemp['bpInvoiceNumber']][]	= $datasTemp;
					}
				}
			}
		}
		if($batchInvoicePO){
			foreach($batchInvoicePO as $invoiceKey => $batchInvoicePOTemp){
				if(count($batchInvoicePOTemp) > 1){
					foreach($batchInvoicePOTemp as $batchInvoicePOTempData){
						$allBatchInvoiceIDs[$batchInvoicePOTempData['orderId']]	= $batchInvoicePOTempData['orderId'];
					}
				}
			}
		}
		if($orgObjectId){
			if(count($datas) == 1){
				$singleInvoiceNumber	= $datas[0]['bpInvoiceNumber'];
				$singleSupplierID		= $datas[0]['customerId'];
				if((!$config['purchaseConsolBasedON']) OR ($config['purchaseConsolBasedON'] == 'supplierID')){
					if($singleInvoiceNumber AND $singleSupplierID){
						$this->ci->db->reset_query();
						$batchDatas	= $this->ci->db->get_where('purchase_order',array('status' => 0, 'bpInvoiceNumber' => $singleInvoiceNumber, 'customerId' => $singleSupplierID, 'account2Id' => $account2Id))->result_array();
						if(count($batchDatas) > 1){
							continue;
						}
					}
				}
				else{
					if($singleInvoiceNumber AND $singleSupplierID){
						if($supplierInfos[$singleSupplierID]){
							$batchDatas			= $this->ci->db->get_where('purchase_order',array('status' => 0, 'bpInvoiceNumber' => $singleInvoiceNumber, 'account2Id' => $account2Id))->result_array();
							if(count($batchDatas) > 1){
								$allSupplierIdsDB	= array_column($batchDatas,'customerId');
								$allSupplierIdsDB	= array_unique($allSupplierIdsDB);
								$allSupplierIdsDB	= array_unique($allSupplierIdsDB);
								if($allSupplierIdsDB){
									$this->ci->db->reset_query();
									$distinctSupplierInfo	= $this->ci->db->where_in('customerId',$allSupplierIdsDB)->select('DISTINCT accountReference',false)->get_where('customers',array('account2Id' => $account2Id))->result_array();
									if($distinctSupplierInfo){
										if(count($distinctSupplierInfo) == 1){
											continue;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(!$config['disableSkuDetails']){
		if($datas){
			$allPostItmeIds	= array();
			foreach($datas as $datasTemp){
				$rowDatas	= json_decode($datasTemp['rowData'], true);
				foreach($rowDatas['orderRows'] as $orderRowsTemp){
					if($orderRowsTemp['productId'] > 1001){
						$allPostItmeIds[]	= $orderRowsTemp['productId'];
					}
				}
			}
			$allPostItmeIds = array_filter($allPostItmeIds);
			$allPostItmeIds = array_unique($allPostItmeIds);
			if($allPostItmeIds){
				$this->postProducts($allPostItmeIds,$account2Id);
			}
		}
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}	
	$allSalesCustomerTemps	= $this->ci->db->select('customerId')->get_where('purchase_order',array('account2Id' => $account2Id, 'status' => '0', 'customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer	= array();
		$allSalesCustomer	= array_column($allSalesCustomerTemps,'customerId');
		$allSalesCustomer	= array_unique($allSalesCustomer);
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer,$account2Id);
		}
	}
	$this->ci->db->reset_query();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	$genericcustomerMappings		= array();
	if($genericcustomerMappingsTemps){
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('createdCustomerId <>' => NULL, 'account2Id' => $account2Id))->result_array();
	$customerMappings		= array();
	if($customerMappingsTemps){
		foreach($customerMappingsTemps as $customerMappingsTemp){
			if($customerMappingsTemp['createdCustomerId']){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	if(!$config['disableSkuDetails']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id, 'createdProductId <>' => NULL))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				if($productMappingsTemp['createdProductId']){
					$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
				}
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	if($this->ci->globalConfig['enablePaymenttermsMapping']){
		$this->ci->db->reset_query();
		$paymentTermsMappingsTemps	= $this->ci->db->get_where('mapping_paymentterms',array('type' => 'purchase','account2Id' => $account2Id))->result_array();
		$paymentTermsMappings		= array();
		if($paymentTermsMappingsTemps){
			foreach($paymentTermsMappingsTemps as $paymentTermsMappingsTemp){
				if($paymentTermsMappingsTemp['account1PurchaseTermId'] != 'NA'){
					$paymentTermsMappings[$paymentTermsMappingsTemp['account1PurchaseTermId']]	= $paymentTermsMappingsTemp;
				}
			}
		}
	}
	
	$CustomFieldMappings					= array();
	$getAllSalesrepData			=  array();
	$getAllProjectsData			=  array();
	$getAllChannelMethodData	=  array();
	$getAllLeadsourceData		=  array();
	$getAllTeamData				=  array();
	if($this->ci->globalConfig['enableCustomFieldMapping']){
		$this->ci->db->reset_query();
		$isAssignmentMapped						= 0;
		$isAssignmentstaffOwnerContactIdMapped	= 0;
		$isAssignmentprojectIdMapped			= 0;
		$isAssignmentchannelIdMapped			= 0;
		$isAssignmentleadSourceIdMapped			= 0;
		$isAssignmentteamIdMapped				= 0;
		$CustomFieldMappingsTemps				= $this->ci->db->get_where('mapping_customfield',array('type' => 'purchase', 'account2Id' => $account2Id))->result_array();
		if($CustomFieldMappingsTemps){
			foreach($CustomFieldMappingsTemps as $CustomFieldMappingsTemp){
				$CustomFieldMappings[$CustomFieldMappingsTemp['account1CustomField']]	= $CustomFieldMappingsTemp;
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'assignment')){
					$isAssignmentMapped	= 1;
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'staffownercontactid')){
						$isAssignmentstaffOwnerContactIdMapped	= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'projectid')){
						$isAssignmentprojectIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'channelid')){
						$isAssignmentchannelIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'leadsourceid')){
						$isAssignmentleadSourceIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'teamid')){
						$isAssignmentteamIdMapped				= 1;
					}
				}
			}
		}
		
		if($CustomFieldMappings AND $isAssignmentMapped){
			if($isAssignmentstaffOwnerContactIdMapped){
				$getAllSalesrepData			= $this->ci->brightpearl->getAllSalesrep();
			}
			if($isAssignmentprojectIdMapped){
				$getAllProjectsData			= $this->ci->brightpearl->getAllProjects();
			}
			if($isAssignmentchannelIdMapped){
				$getAllChannelMethodData	= $this->ci->brightpearl->getAllChannelMethod();
			}
			if($isAssignmentleadSourceIdMapped){
				$getAllLeadsourceData		= $this->ci->brightpearl->getAllLeadsource();
			}
			if($isAssignmentteamIdMapped){
				$getAllTeamData				= $this->ci->brightpearl->getAllTeam();
			}
		}
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	if($datas){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($allBatchInvoiceIDs[$orderDatas['orderId']]){continue;}
			if($orderDatas['sendInAggregation']){continue;}
			$config1	= $this->ci->account1Config[$orderDatas['account1Id']];
			$bpconfig	= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId	= $orderDatas['orderId'];
			
			/* UNINVOICING CODE STARTS */
			if($UnInvoicingEnabled){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						$QBOorderId		= $orderDatas['createOrderId'];
						$uninvoiceCount	= $orderDatas['uninvoiceCount'];
						$BillVoided		= 0;
						$TempAcc2ID		= '';
						if($orderDatas['TempAcc2ID']){
							$TempAcc2ID	= $orderDatas['TempAcc2ID'];
						}
						else{
							$TempAcc2ID	= $account2Id;
						}
						$createdRowData	= json_decode($orderDatas['createdRowData'],true);
						$IsPaid			= 0;
						if($orderDatas['paymentDetails']){
							$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
							foreach($paymentDetails as $pKey => $paymentDetail){
								if($paymentDetail['amount'] > 0){
									if($paymentDetail['status'] == 1){
										$IsPaid	= 1;
										break;
									}
								}
							}
						}
						else{
							$IsPaid	= 0;
						}
						$SyncToken		= '';
						if($IsPaid == 0){
							$readurl		= 'bill/'.$QBOorderId;
							$billResponse	= $this->getCurl($readurl, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
							if($billResponse['Bill']){
								$SyncToken	= $billResponse['Bill']['SyncToken'];
							}
							$request		= array(
								"SyncToken"		=>	$SyncToken,
								"Id"			=>	$QBOorderId,
							);
							$url			= 'bill?operation=delete';
							$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $TempAcc2ID)[$TempAcc2ID];
							$createdRowData['Void Request data	: ']	= $request;
							$createdRowData['Void Response data	: ']	= $results;
							$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
							if($results['Bill']['status'] == 'Deleted'){
								$uninvoiceCount++;
								$BillVoided		= 1;
								$PaymentState	= 1;
								
								if($orderDatas['PaymentState'] == 2){
									$PaymentState	= 2;
								}
								
								$this->ci->db->update('purchase_order',array('paymentDetails' => '','TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount,'PaymentState' => $PaymentState),array('orderId' => $orderId));
							}
							else{
								$this->ci->db->update('purchase_order',array('message' => 'Unable to Delete the Bill'),array('orderId' => $orderId));
								continue;
							}
						}
						elseif($IsPaid	== 1){
							$createdRowData	= json_decode($orderDatas['createdRowData'],true);
							$QBOorderId		= $orderDatas['createOrderId'];			
							$readurl		= 'bill/'.$QBOorderId;
							$billResponse	= $this->getCurl($readurl, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
							if($billResponse['Bill']){
								$SyncToken	= $billResponse['Bill']['SyncToken'];
							}
							$request		= array(
								"SyncToken"		=>	$SyncToken,
								"Id"			=>	$QBOorderId,
							);
							$url				= 'bill?operation=delete';
							$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $TempAcc2ID)[$TempAcc2ID];
							$createdRowData['Void Request data	: ']	= $request;
							$createdRowData['Void Response data	: ']	= $results;
							$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
							if($results['Bill']['status'] == 'Deleted'){
								$uninvoiceCount++;
								$BillVoided		= 1;
								$PaymentState	= 2;
								$this->ci->db->update('purchase_order',array('TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '','isPaymentCreated' => '0', 'sendPaymentTo' => '', 'paymentDetails' => '',  'uninvoiceCount' => $uninvoiceCount,'PaymentState' => $PaymentState),array('orderId' => $orderId));
							}
							else{
								$this->ci->db->update('purchase_order',array('message' => 'Unable to Delete the Bill'),array('orderId' => $orderId));
								continue;
							}
						}
						if($BillVoided){
							$PurchaseCogsData	= $this->ci->db->get_where('cogs_journal',array('orderId' => $orderId , 'status' => 1, 'createdJournalsId <>' => ''))->result_array();
							if($PurchaseCogsData){
								foreach($PurchaseCogsData as $PurchaseCogsDataTemp){
									$createdJournalsId		= $PurchaseCogsDataTemp['createdJournalsId'];
									$JournalAccout2Id		= $PurchaseCogsDataTemp['account2Id'];
									$Journalreadurl			= 'journalentry/'.$createdJournalsId;
									$journalReadResponse	= $this->getCurl($Journalreadurl, 'GET', '', 'json', $JournalAccout2Id)[$JournalAccout2Id];
									$JournalSyncToken		= 0;
									if($journalReadResponse['JournalEntry']){
										$JournalSyncToken	= $journalReadResponse['JournalEntry']['SyncToken'];
									}
									$JournalVoidRequest		= array(
										"SyncToken"		=>	$JournalSyncToken,
										"Id"			=>	$createdJournalsId,
									);
									$JournalVoidUrl			= 'journalentry?operation=delete';
									$JournalVoidResult		= $this->getCurl($JournalVoidUrl, 'POST', json_encode($JournalVoidRequest), 'json', $JournalAccout2Id)[$JournalAccout2Id];
									if($JournalVoidResult['JournalEntry']['status'] == 'Deleted'){
										$this->ci->db->where('createdJournalsId', $createdJournalsId)->delete('cogs_journal',array('account2Id' => $JournalAccout2Id));
									}
								}
							}
							
							$grniJournalData	= $this->ci->db->get_where('grni_journal',array('orderId' => $orderId , 'status' => 2, 'reversedJournalsId <>' => ''))->result_array();
							if($grniJournalData){
								foreach($grniJournalData as $grniJournalDataTemp){
									$reversedJournalsId		= $grniJournalDataTemp['reversedJournalsId'];
									$JournalAccout2Id		= $grniJournalDataTemp['account2Id'];
									$Journalreadurl			= 'journalentry/'.$reversedJournalsId;
									$journalReadResponse	= $this->getCurl($Journalreadurl, 'GET', '', 'json', $JournalAccout2Id)[$JournalAccout2Id];
									$JournalSyncToken		= 0;
									if($journalReadResponse['JournalEntry']){
										$JournalSyncToken	= $journalReadResponse['JournalEntry']['SyncToken'];
									}
									$JournalVoidRequest		= array(
										"SyncToken"		=>	$JournalSyncToken,
										"Id"			=>	$reversedJournalsId,
									);
									$JournalVoidUrl			= 'journalentry?operation=delete';
									$JournalVoidResult		= $this->getCurl($JournalVoidUrl, 'POST', json_encode($JournalVoidRequest), 'json', $JournalAccout2Id)[$JournalAccout2Id];
									if($JournalVoidResult['JournalEntry']['status'] == 'Deleted'){
										$this->ci->db->update('grni_journal',array('reversedJournalsId' => '', 'status' => 1, 'isReversed' => 0),array('reversedJournalsId' => $reversedJournalsId));
									}
								}
							}
						}
						continue;
					}
				}
			}
			/* UNINVOICING ENDS */
			
			if($orderDatas['createOrderId']){continue;}
			if(!$orderDatas['invoiced']){continue;}
			if($allBatchInvoiceIDs[$orderId]){continue;}
			
			/* CODE TO CHECK DROPSHIPS CASE */
			//	$config['InventoryManagementEnabled'] == 0 that means connector is inventory managed
			if($config['InventoryManagementEnabled'] == 0){
				if($orderDatas['LinkedWithSO']){
					$GetLiinkedSO	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id, 'orderId' => $orderDatas['LinkedWithSO']))->row_array();
					if(!$GetLiinkedSO){
						$this->ci->db->update('purchase_order',array('message' => 'Parent SO is Not fetched yet'),array('orderId' => $orderDatas['orderId'],'account2Id' => $account2Id));
						continue;
					}
				}
			}
			
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			if(!$channelId){
				$channelId	= 'blank';
			}
			
			$billAddress			= $rowDatas['parties']['billing'];
			$shipAddress			= $rowDatas['parties']['delivery'];
			$orderCustomer			= $rowDatas['parties']['supplier'];
			$BrightpearlTotalAmount	= $rowDatas['totalValue']['total'];
			$BrightpearlTotalTAX	= $rowDatas['totalValue']['taxAmount'];
			
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				$this->ci->db->update('purchase_order',array('message' => 'Supplier not sent yet.'),array('orderId' => $orderId));
				continue;
			}
			if(!$rowDatas['invoices']['0']['invoiceReference']){
				continue;
			}
			
			$missingSkus					= array();
			$InvoiceLineAdd					= array();
			$discountCouponAmt				= array();
			$totalItemDiscount				= array();
			$invoiceLineCount				= 0;
			$isDiscountCouponAdded			= 0;
			$orderTaxAmount					= 0;
			$taxAmount						= 0;
			$linNumber						= 1;
			$orderTaxId						= '';
			$couponItemLineID				= '';
			$shippingLineID					= '';
			$ItemRefValue					= '';
			$ItemRefName					= '';
			$countryIsoCode					= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState					= strtolower(trim($shipAddress['addressLine4']));
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$productId	= $orderRows['productId'];
				if($orderRows['productId'] <= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID			= $rowId;
					}
				}
			}
			$itemtaxAbleLine		= $config['orderLineTaxCode'];
			$discountTaxAbleLine	= $config['orderLineTaxCode'];
			
			if($config['disableSkuDetails'] AND !$orderDatas['LinkedWithSO']){
				$ProductArray		= array();
				foreach($rowDatas['orderRows'] as $rowId => $orderRows){
					if($rowId == $couponItemLineID){
						if($rowDatas['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					$taxMapping			= array();
					$params				= '';
					$ItemRefName		= '';
					$ItemRefValue		= '';
					$LineTaxId			= '';
					$orderTaxId			= '';
					$isBundleChild 		= '';
					
					$rowNet				= $orderRows['rowValue']['rowNet']['value'];
					$rowTax				= $orderRows['rowValue']['rowTax']['value'];
					$productId			= $orderRows['productId'];
					$BPNominal			= $orderRows['nominalCode'];
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
					
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$isTrackedItem		= 0;
					if($productMappings[$productId]){
						$productParams	= json_decode($productMappings[$productId]['params'], true);
						$isTrackedItem	= $productParams['stock']['stockTracked'];
					}
					else{
						if($productId > 1001){
							$missingSkus[]	= $orderRows['productSku'];
							continue;
						}
					}
					
					$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
					$taxMappingKey1	= $orderRows['rowValue']['taxClassId'];
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode;
						}
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId])){
						$taxMapping	= $taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId];
					}
					$LineTaxId  = $taxMapping['account2LineTaxId'];
					$orderTaxId = $taxMapping['account2TaxId'];
					if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'] OR $taxMapping['account2KidsTaxId'])){
						$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
						$orderTaxId = $taxMapping['account2KidsTaxId'];
					}
					if(!$config['SendTaxAsLineItem']){
						if(($orderRows['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $orderRows['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$BPNominal][$LineTaxId])){
									$ProductArray['discount'][$BPNominal][$LineTaxId]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$BPNominal][$LineTaxId]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								if($isBundleChild){
									
								}
								else{
									$originalPrice		= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
									if(!$originalPrice){
										$originalPrice	= $rowNet;
									}
									if($originalPrice > $rowNet){
										$discountCouponAmt	= $originalPrice - $rowNet;
										$rowNet				= $originalPrice;
										if($discountCouponAmt > 0){
											if(isset($ProductArray['discountCoupon'][$BPNominal][$LineTaxId])){
												$ProductArray['discountCoupon'][$BPNominal][$LineTaxId]['TotalNetAmt']	+= $discountCouponAmt;
											}
											else{
												$ProductArray['discountCoupon'][$BPNominal][$LineTaxId]['TotalNetAmt']	= $discountCouponAmt;
											}
										}
									}
								}
							}
						}
						if((!in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($orderRows['nominalCode'],$nominalCodeForShipping)) AND (!in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($orderRows['nominalCode'],$nominalCodeForDiscount))){
							if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$channelId][$orderRows['nominalCode']]))){
								$customItemIdQbo	= $defaultItemMapping[$channelId][$orderRows['nominalCode']];
								if(isset($ProductArray['customItems'][$BPNominal][$LineTaxId][$customItemIdQbo])){
									$ProductArray['customItems'][$BPNominal][$LineTaxId][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
									$ProductArray['customItems'][$BPNominal][$LineTaxId][$customItemIdQbo]['bpTaxTotal']	+= $rowTax;
								}
								else{
									$ProductArray['customItems'][$BPNominal][$LineTaxId][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
									$ProductArray['customItems'][$BPNominal][$LineTaxId][$customItemIdQbo]['bpTaxTotal']	= $rowTax;
								}
							}
							else{
								if($isTrackedItem){
									if(isset($ProductArray['aggregationItem2'][$BPNominal][$LineTaxId])){
										$ProductArray['aggregationItem2'][$BPNominal][$LineTaxId]['TotalNetAmt']	+= $rowNet;
										$ProductArray['aggregationItem2'][$BPNominal][$LineTaxId]['bpTaxTotal']	+= $rowTax;
									}
									else{
										$ProductArray['aggregationItem2'][$BPNominal][$LineTaxId]['TotalNetAmt']	= $rowNet;
										$ProductArray['aggregationItem2'][$BPNominal][$LineTaxId]['bpTaxTotal']	= $rowTax;
									}

								}
								else{
									if(isset($ProductArray['aggregationItem'][$BPNominal][$LineTaxId])){
										$ProductArray['aggregationItem'][$BPNominal][$LineTaxId]['TotalNetAmt']	+= $rowNet;
										$ProductArray['aggregationItem'][$BPNominal][$LineTaxId]['bpTaxTotal']	+= $rowTax;
									}
									else{
										$ProductArray['aggregationItem'][$BPNominal][$LineTaxId]['TotalNetAmt']	= $rowNet;
										$ProductArray['aggregationItem'][$BPNominal][$LineTaxId]['bpTaxTotal']	= $rowTax;
									}
								}
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$BPNominal][$LineTaxId])){
								$ProductArray['landedCost'][$BPNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['landedCost'][$BPNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['landedCost'][$BPNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['landedCost'][$BPNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$BPNominal][$LineTaxId])){
								$ProductArray['shipping'][$BPNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['shipping'][$BPNominal][$LineTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['shipping'][$BPNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['shipping'][$BPNominal][$LineTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$BPNominal][$LineTaxId])){
								$ProductArray['giftcard'][$BPNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['giftcard'][$BPNominal][$LineTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['giftcard'][$BPNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['giftcard'][$BPNominal][$LineTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$BPNominal][$LineTaxId])){
								$ProductArray['couponitem'][$BPNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['couponitem'][$BPNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['couponitem'][$BPNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['couponitem'][$BPNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
					}
					else{
						if(($orderRows['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $orderRows['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$BPNominal])){
									$ProductArray['discount'][$BPNominal]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$BPNominal]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								$originalPrice		= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice	= $rowNet;
								}
								if($originalPrice > $rowNet){
									$discountCouponAmt	= $originalPrice - $rowNet;
									$rowNet				= $originalPrice;
									if($discountCouponAmt > 0){
										if(isset($ProductArray['discountCoupon'][$BPNominal])){
											$ProductArray['discountCoupon'][$BPNominal]['TotalNetAmt']	+= $discountCouponAmt;
										}
										else{
											$ProductArray['discountCoupon'][$BPNominal]['TotalNetAmt']	= $discountCouponAmt;
										}
									}
								}
							}
						}
						if((!in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($orderRows['nominalCode'],$nominalCodeForShipping)) AND (!in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($orderRows['nominalCode'],$nominalCodeForDiscount))){
							if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$channelId][$orderRows['nominalCode']]))){
								$customItemIdQbo	= $defaultItemMapping[$channelId][$orderRows['nominalCode']];
								if(isset($ProductArray['customItems'][$BPNominal][$customItemIdQbo])){
									$ProductArray['customItems'][$BPNominal][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
								}
								else{
									$ProductArray['customItems'][$BPNominal][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
								}
								if(isset($ProductArray['allTax'])){
									$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
								}
								else{
									$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
								}
							}
							else{
								if($isTrackedItem){
									if(isset($ProductArray['aggregationItem2'][$BPNominal])){
										$ProductArray['aggregationItem2'][$BPNominal]['TotalNetAmt']	+= $rowNet;
									}
									else{
										$ProductArray['aggregationItem2'][$BPNominal]['TotalNetAmt']	= $rowNet;
									}
									if(isset($ProductArray['allTax'])){
										$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
									}
									else{
										$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
									}
								}
								else{
									if(isset($ProductArray['aggregationItem'][$BPNominal])){
										$ProductArray['aggregationItem'][$BPNominal]['TotalNetAmt']	+= $rowNet;
									}
									else{
										$ProductArray['aggregationItem'][$BPNominal]['TotalNetAmt']	= $rowNet;
									}
									if(isset($ProductArray['allTax'])){
										$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
									}
									else{
										$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
									}
								}
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$BPNominal])){
								$ProductArray['landedCost'][$BPNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['landedCost'][$BPNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$BPNominal])){
								$ProductArray['shipping'][$BPNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['shipping'][$BPNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$BPNominal])){
								$ProductArray['giftcard'][$BPNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['giftcard'][$BPNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$BPNominal])){
								$ProductArray['couponitem'][$BPNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['couponitem'][$BPNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
					}
				}
	
				if($ProductArray){
					if(!$config['SendTaxAsLineItem']){
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$ExpenseAccountRef	= $config['ExpenseAccountRef'];
									if(isset($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'Amount'							=> sprintf("%.4f",$datas['TotalNetAmt']),
												'Description'						=> 'Generic Item',
												'DetailType'						=> 'AccountBasedExpenseLineDetail',
												'AccountBasedExpenseLineDetail'		=> array(
													'AccountRef'						=> array('value' => $ExpenseAccountRef),
													'TaxCodeRef'						=> array('value' => $Taxkeyproduct1),
													'BillableStatus'					=> 'NotBillable',
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'LineNum'							=> $linNumber++,
												'Description'						=> 'Expense Item',
												'Amount'							=> sprintf("%.4f",$datas['TotalNetAmt']),
												'DetailType'						=> 'ItemBasedExpenseLineDetail',
												'ItemBasedExpenseLineDetail'		=> array(
													'BillableStatus'					=> 'NotBillable',
													'ItemRef'							=> array('value' => $config['genericSku']),
													'Qty'								=> 1,
													'TaxCodeRef'						=> array('value' => $Taxkeyproduct1),
													'UnitPrice'							=> sprintf("%.4f",$datas['TotalNetAmt']),
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
										}
										
										if($keyproduct == 'landedCost'){
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Landed Cost';
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['landedCostItem']);
											}
										}
										if($keyproduct == 'shipping'){
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
											}
										}
										if($keyproduct == 'giftcard'){
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
											}
										}
										if($keyproduct == 'couponitem'){
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon';
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
											}
										}
										if($keyproduct == 'discount'){
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
											$InvoiceLineAdd[$invoiceLineCount]['Amount']		= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']		= array('value' => $config['discountItem']);
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											}
										}
										if($keyproduct == 'discountCoupon'){
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
											$InvoiceLineAdd[$invoiceLineCount]['Amount']		= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']		= array('value' => $config['couponItem']);
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											}
										}
										$invoiceLineCount++;
									}
								}
							}
							elseif($keyproduct == 'customItems'){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$ExpenseAccountRef	= $config['ExpenseAccountRef'];
									if(isset($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $ProductArrayDatass){
										foreach($ProductArrayDatass as $customItemId	=> $customItemIdTemp){
											if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
												$InvoiceLineAdd[$invoiceLineCount]	= array(
													'Amount'							=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
													'Description'						=> 'Special Item',
													'DetailType'						=> 'AccountBasedExpenseLineDetail',
													'AccountBasedExpenseLineDetail'		=> array(
														'AccountRef'						=> array('value' => $ExpenseAccountRef),
														'TaxCodeRef'						=> array('value' => $Taxkeyproduct1),
														'BillableStatus'					=> 'NotBillable',
													),
												);
												if($nominalClassID){
													$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
												}
												$invoiceLineCount++;
											}
											else{
												$InvoiceLineAdd[$invoiceLineCount]	= array(
													'LineNum'							=> $linNumber++,
													'Description'						=> 'Special Item',
													'Amount'							=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
													'DetailType'						=> 'ItemBasedExpenseLineDetail',
													'ItemBasedExpenseLineDetail'		=> array(
														'BillableStatus'					=> 'NotBillable',
														'ItemRef'							=> array('value' => $customItemId),
														'Qty'								=> 1,
														'TaxCodeRef'						=> array('value' => $Taxkeyproduct1),
														'UnitPrice'							=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
													),
												);
												if($nominalClassID){
													$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
												}
												$invoiceLineCount++;
											}
										}
									}
								}
							}
							else{
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$ExpenseAccountRef	= $config['ExpenseAccountRef'];
									if(isset($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'Amount'							=> sprintf("%.4f",$datas['TotalNetAmt']),
												'Description'						=> 'Generic Item',
												'DetailType'						=> 'AccountBasedExpenseLineDetail',
												'AccountBasedExpenseLineDetail'		=> array(
													'AccountRef'						=> array('value' => $ExpenseAccountRef),
													'TaxCodeRef'						=> array('value' => $Taxkeyproduct1),
													'BillableStatus'					=> 'NotBillable',
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
											$invoiceLineCount++;
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'LineNum'							=> $linNumber++,
												'Description'						=> 'Expense Item',
												'Amount'							=> sprintf("%.4f",$datas['TotalNetAmt']),
												'DetailType'						=> 'ItemBasedExpenseLineDetail',
												'ItemBasedExpenseLineDetail'		=> array(
													'BillableStatus'					=> 'NotBillable',
													'ItemRef'							=> array('value' => $config['genericSku']),
													'Qty'								=> 1,
													'TaxCodeRef'						=> array('value' => $Taxkeyproduct1),
													'UnitPrice'							=> sprintf("%.4f",$datas['TotalNetAmt']),
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
											if($keyproduct == 'aggregationItem2'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Tracked Inventory';
											}
											$invoiceLineCount++;
										}
									}
								}
							}
						}
					}
					else{
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'aggregationItem2') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$ExpenseAccountRef	= $config['ExpenseAccountRef'];
									if(isset($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'Amount'							=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
											'Description'						=> 'Generic Item',
											'DetailType'						=> 'AccountBasedExpenseLineDetail',
											'AccountBasedExpenseLineDetail'		=> array(
												'AccountRef'						=> array('value' => $ExpenseAccountRef),
												'TaxCodeRef'						=> array('value' => $config['salesNoTaxCode']),
												'BillableStatus'					=> 'NotBillable',
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
									}
									else{
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'							=> $linNumber++,
											'Description'						=> 'Expense Item',
											'Amount'							=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
											'DetailType'						=> 'ItemBasedExpenseLineDetail',
											'ItemBasedExpenseLineDetail'		=> array(
												'BillableStatus'					=> 'NotBillable',
												'ItemRef'							=> array('value' => $config['genericSku']),
												'Qty'								=> 1,
												'TaxCodeRef'						=> array('value' => $config['salesNoTaxCode']),
												'UnitPrice'							=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
									}
									if($keyproduct == 'aggregationItem2'){
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Tracked Inventory';
										}
									}
									if($keyproduct == 'landedCost'){
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Landed Cost';
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['landedCostItem']);
										}
									}
									if($keyproduct == 'shipping'){
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
										}
									}
									if($keyproduct == 'giftcard'){
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
										}
									}
									if($keyproduct == 'couponitem'){
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon';
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
										}
									}
									$IncomeAccountRef = '';
									if($keyproduct == 'discount'){
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount'.'-'.$IncomeAccountRef;
										$InvoiceLineAdd[$invoiceLineCount]['Amount']		= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']		= array('value' => $config['discountItem']);
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										}
									}
									if($keyproduct == 'discountCoupon'){
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
										$InvoiceLineAdd[$invoiceLineCount]['Amount']		= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']		= array('value' => $config['couponItem']);
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										}
									}
									$invoiceLineCount++;
								}
							}
							elseif($keyproduct == 'customItems'){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$ExpenseAccountRef	= $config['ExpenseAccountRef'];
									if(isset($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $customItemId	=> $customItemIdTemp){
										if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'Amount'							=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												'Description'						=> 'Special Item',
												'DetailType'						=> 'AccountBasedExpenseLineDetail',
												'AccountBasedExpenseLineDetail'		=> array(
													'AccountRef'						=> array('value' => $ExpenseAccountRef),
													'TaxCodeRef'						=> array('value' => $config['salesNoTaxCode']),
													'BillableStatus'					=> 'NotBillable',
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
											$invoiceLineCount++;
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'LineNum'							=> $linNumber++,
												'Description'						=> 'Special Item',
												'Amount'							=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												'DetailType'						=> 'ItemBasedExpenseLineDetail',
												'ItemBasedExpenseLineDetail'		=> array(
													'BillableStatus'					=> 'NotBillable',
													'ItemRef'							=> array('value' => $customItemId),
													'Qty'								=> 1,
													'TaxCodeRef'						=> array('value' => $config['salesNoTaxCode']),
													'UnitPrice'							=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
											$invoiceLineCount++;
										}
									}
								}
							}
							elseif($keyproduct == 'allTax'){
								if($ProductArrayDatasTemp['TotalNetAmt'] <= 0){
									continue;
								}
								$InvoiceLineAdd[$invoiceLineCount] = array(
									'LineNum'				=> $linNumber++,
									'Description'			=> 'Total Tax Amount',
									'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
									'DetailType'			=> 'ItemBasedExpenseLineDetail',
									'ItemBasedExpenseLineDetail'	=> array(
										'ItemRef'						=> array('value' => $config['TaxLineItemCode']),
										'Qty' 							=> 1,
										'UnitPrice' 					=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
										'TaxCodeRef' 					=> array('value' => $config['salesNoTaxCode']),
									),	
								);
								$invoiceLineCount++;
							}
						}
					}
				}
			}
			else{
				ksort($rowDatas['orderRows']);
				foreach($rowDatas['orderRows'] as $rowId => $orderRows){
					if($rowId == $couponItemLineID){
						if($orderRows['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					
					$taxMapping			= array();
					$params				= '';
					$ItemRefName		= '';
					$ItemRefValue		= '';
					$LineTaxId			= '';
					$orderTaxId			= '';
					$productId			= $orderRows['productId'];
					
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
					$taxMappingKey1	= $orderRows['rowValue']['taxClassId'];
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode;
						}
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId])){
						$taxMapping	= $taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId];
					}
					$LineTaxId  = $taxMapping['account2LineTaxId'];
					$orderTaxId = $taxMapping['account2TaxId'];
					if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'] OR $taxMapping['account2KidsTaxId'])){
						$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
						$orderTaxId = $taxMapping['account2KidsTaxId'];
					}
					
					if($productId > 1001){
						if(!$productMappings[$productId]['createdProductId']){
							$missingSkus[]	= $orderRows['productSku'];
							continue;
						}
						$ItemRefValue	= $productMappings[$productId]['createdProductId'];
						$ItemRefName	= $productMappings[$productId]['sku'];
					}
					else{
						if($orderRows['rowValue']['rowNet']['value'] > 0){
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else if($orderRows['rowValue']['rowNet']['value'] < 0){
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else{
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
					}
					
					$BPNominal			=  $orderRows['nominalCode'];
					$price				=  $orderRows['rowValue']['rowNet']['value'];
					$orderTaxAmount		+= $orderRows['rowValue']['rowTax']['value'];
					$originalPrice		= $price;
					$discountPercentage	=  0;
					
					if(($orderRows['discountPercentage'] > 0) && (!$ignoreLineDiscount)){
						$discountPercentage	= 100 - $orderRows['discountPercentage'];
						if($discountPercentage == 0){
							$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
						}
						else{
							$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
						}
						$tempTaxAmt	= $originalPrice - $price;
						if($tempTaxAmt > 0){
							if(isset($totalItemDiscount[$LineTaxId][$BPNominal])){
								$totalItemDiscount[$LineTaxId][$BPNominal]['AMT']	+= $tempTaxAmt;
							}
							else{
								$totalItemDiscount[$LineTaxId][$BPNominal]['AMT']	= $tempTaxAmt;
							}
						}
					}
					else if($isDiscountCouponAdded){
						if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							if(!$originalPrice){
								$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
							}
							if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
								$discountCouponAmtTemp	= ($originalPrice - $price);
								if($discountCouponAmtTemp > 0){
									if(isset($discountCouponAmt[$LineTaxId][$BPNominal])){
										$discountCouponAmt[$LineTaxId][$BPNominal]['AMT']	+= $discountCouponAmtTemp;
									}
									else{
										$discountCouponAmt[$LineTaxId][$BPNominal]['AMT']	= $discountCouponAmtTemp;
									}
								}
							}
							else{
								$originalPrice	= $orderRows['rowValue']['rowNet']['value'];
							}
						}
					}
					$params	= json_decode($productMappings[$productId]['params'],true);
					
					if($ItemRefValue){
						$ExpenseAccountRef	= $config['ExpenseAccountRef'];
						if(isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
							$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']])) AND ($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'])){
							$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
						}
						$nominalClassID		= '';
						if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]))){
							$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]['account2ClassId'];
						}
						
						
						$UnitAmount	= 0.00;
						if($originalPrice != 0){
							$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
						}
						if($enableCOGSJournals AND $orderDatas['LinkedWithSO']){
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'Amount'							=> sprintf("%.4f",($UnitAmount * $orderRows['quantity']['magnitude'])),
								'Description'						=> $orderRows['productName'],
								'DetailType'						=> 'AccountBasedExpenseLineDetail',
								'AccountBasedExpenseLineDetail'		=> array(
									'AccountRef'						=> array('value' => $ExpenseAccountRef),
									'TaxCodeRef'						=> array('value' => $LineTaxId),
									'BillableStatus'					=> 'NotBillable',
								),
							);
							if($nominalClassID){
								$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
							}
							if(!$LineTaxId){
								unset($InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['TaxCodeRef']);
							}
							$invoiceLineCount++;
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'LineNum'							=> $linNumber++,
								'Description'						=> $orderRows['productName'],
								'Amount'							=> sprintf("%.4f",($UnitAmount * $orderRows['quantity']['magnitude'])),
								'DetailType'						=> 'ItemBasedExpenseLineDetail',
								'ItemBasedExpenseLineDetail'		=> array(
									'BillableStatus'					=> 'NotBillable',
									'ItemRef'							=> array('value' => $ItemRefValue,),
									'Qty'								=> $orderRows['quantity']['magnitude'],
									'TaxCodeRef'						=> array('value' => $LineTaxId),
									'UnitPrice'							=> sprintf("%.4f",($UnitAmount)),
								),
							);
							if($nominalClassID){
								$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
							}
							if(!empty($defaultItemMapping)){
								if(isset($defaultItemMapping[$channelId][$orderRows['nominalCode']])){
									$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $defaultItemMapping[$channelId][$orderRows['nominalCode']]);
								}
							}
							
							if(!$LineTaxId){
								unset($InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']);
							}
							$invoiceLineCount++;
						}
					}
				}
				if($totalItemDiscount){
					foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmountData){
						foreach($totalItemDiscountLineAmountData as $BPNominalCode => $totalItemDiscountLineAmount){
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'LineNum'							=> $linNumber++,
								'Description' 						=> 'Item Discount',
								'Amount'							=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount['AMT'])),
								'DetailType'						=> 'ItemBasedExpenseLineDetail',
								'ItemBasedExpenseLineDetail'		=> array(
									'ItemRef' 							=> array( 'value' => $config['discountItem']),
									'Qty' 								=> 1,
									'UnitPrice' 						=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount['AMT'])),
								),	
							);
							$nominalClassID		= '';
							if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
								$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
							}
							if($nominalClassID){
								$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']		= array('value' => $nominalClassID);
							}
							if($TaxID){
								$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
							}
							else{
								$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
							}
							$invoiceLineCount++;
						}
					}
				}
				if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					if($discountCouponAmt){
						foreach($discountCouponAmt as $TaxID => $discountCouponLineAmountData){
							foreach($discountCouponLineAmountData as $BPNominalCode => $discountCouponLineAmount){
								$InvoiceLineAdd[$invoiceLineCount] = array(
									'LineNum'				=> $linNumber++,
									'Description'			=> 'Coupon Discount',
									'Amount'				=> sprintf("%.4f",((-1) * $discountCouponLineAmount['AMT'])),
									'DetailType'			=> 'ItemBasedExpenseLineDetail',
									'ItemBasedExpenseLineDetail'	=> array('ItemRef' => array('value' => $config['couponItem']),
										'Qty'						=> 1,
										'UnitPrice'					=> sprintf("%.4f",((-1) * $discountCouponLineAmount['AMT'])),
									),	
								);
								$nominalClassID		= '';
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
									$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
								}
								if($nominalClassID){
									$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']		= array('value' => $nominalClassID);
								}
								if($TaxID){
									$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
								}
								else{
									$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
								}
								$invoiceLineCount++;
							}
						}
					}
				}
				if($config['SendTaxAsLineItem']){
					if($BrightpearlTotalTAX > 0){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Total Tax Amount',
							'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
							'DetailType'			=> 'ItemBasedExpenseLineDetail',
							'ItemBasedExpenseLineDetail'	=> array(
								'ItemRef'						=> array('value' => $config['TaxLineItemCode']),
								'Qty' 							=> 1,
								'UnitPrice' 					=> sprintf("%.4f",$BrightpearlTotalTAX),
								'TaxCodeRef' 					=> array('value' => $config['salesNoTaxCode']),
							),	
						);
						$invoiceLineCount++;
					}
				}
	 
			}
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			
			$dueDate	= date('c');
			$taxDate	= date('c');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= $rowDatas['invoices']['0']['dueDate'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
			}

			//taxdate chanages
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$QBOoffset		= 0;
			$diff			= $BPDateOffset - $QBOoffset;
			$date1			= new DateTime($dueDate);
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date1->setTimezone(new DateTimeZone($BPTimeZone));
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
				$date1->modify($diff);
			}
			$taxDate		= $date->format('Y-m-d');
			$dueDate		= $date1->format('Y-m-d');
			
			$invoiceRef		= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			
			if($config['UseAsDocNumberpopc']){
				$account1FieldIds	= explode(".",$config['UseAsDocNumberpopc']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$invoiceRef	= $fieldValueTmps;
				}
				else{
					$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
				}
			}
			if(!$invoiceRef){
				$invoiceRef	= $orderId;
			}
			$invoiceRef	= substr($invoiceRef,0,21);
			if($config['disableSkuDetails'] AND !$orderDatas['LinkedWithSO']){
				$invoiceLineAddNew		= array();
				$invoiceLineFormatted	= array();
				$newItemLineCount		= 0;
				foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['trackedGenericItem']){
						$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['genericSku']){
						$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['Description'] == 'Special Item'){
						$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['discountItem']){
						$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['couponItem']){
						$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['shippingItem']){
						$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['TaxLineItemCode']){
						$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['giftCardItem']){
						$invoiceLineAddNew[8][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemBasedExpenseLineDetail']['ItemRef']['value'] == $config['landedCostItem']){
						$invoiceLineAddNew[9][]	= $InvoiceLineAddTemp;
						continue;
					}
				}
				ksort($invoiceLineAddNew);
				foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
					foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
						$invoiceLineFormatted[$newItemLineCount]			= $invoiceLineAddNewTempTemp;
						$invoiceLineFormatted[$newItemLineCount]['LineNum']	= ($newItemLineCount + 1);
						$newItemLineCount++;
					}
				}
				if($invoiceLineFormatted){
					$InvoiceLineAdd		= $invoiceLineFormatted;
					$invoiceLineCount	= $newItemLineCount;
					$linNumber			= ($newItemLineCount + 1);
				}
			}

			//chnageReq added on 8th fab 2023 req by Tushar
			if($clientcode == 'aimeeqbo'){
				foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
					if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
						$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Shipping Charges';
					}
				}
			}
			if($clientcode == 'anatomicalwwqbo'){
				foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
					if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
						$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Products';
					}
				}
			}
			
			foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
				if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
					unset($InvoiceLineAdd[$lineItemIdNewTemp]['Description']);
				}
			}
			
			$lineLevelClassId	= '';
			if((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
				$lineLevelClassId	= $channelMappings[$channelId]['account2ChannelId'];
			}
			if($lineLevelClassId){
				foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
					if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
						continue;
					}
					else{
						$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
					}
				}
			}
			
			$QBOCustomerID	= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
			$request		= array(
				'DocNumber'		=> $invoiceRef,
				'VendorRef'		=> array('value' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId']),
				'TxnDate'		=> $taxDate,
				'DueDate'		=> $dueDate,
				'Line'			=> $InvoiceLineAdd,
				'ShipAddr'		=> array(
					'Line1'						=> @$shipAddress['addressLine1'],
					'Line2'						=> @$shipAddress['addressLine2'],
					'City'						=> @$shipAddress['addressLine3'],
					'CountrySubDivisionCode'	=> @$shipAddress['addressLine4'],
					'Country'					=> @$shipAddress['countryIsoCode'],
					'PostalCode'				=> @$shipAddress['postalCode'],
				),
				'ExchangeRate'	=> sprintf("%.4f",(1 / $rowDatas['currency']['exchangeRate'])),
				'CurrencyRef'	=> array('value' => $rowDatas['currency']['orderCurrencyCode']),
			);
			if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$rowDatas['currency']['orderCurrencyCode'],$config['defaultCurrrency'],$taxDate);
				if($exRate){
					$request['ExchangeRate'] = $exRate;
				}
				else{
					$exRate = $exchangeRate[strtolower($rowDatas['currency']['orderCurrencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$request['ExchangeRate'] = $exRate;
					}
					else{
						echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
						unset($request['ExchangeRate']);
					}
				}
			}
			
			
			if($clientcode == 'biscuiteersqbo'){
				if(!$config['SendTaxAsLineItem']){
					$TxnTaxDetailAdded	= 0;
					if($orderTaxId){
						if($orderTaxAmount > 0){
							$TxnTaxDetailAdded	= 1;
							$request['TxnTaxDetail']	= array(
								'TotalTax'					=> $orderTaxAmount,
								'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
							);
							$request['GlobalTaxCalculation']	= 'TaxExcluded';
						}
					}
					if(!$TxnTaxDetailAdded) {
						if(!$orderTaxId){
							$orderTaxId	= $config['TaxCode'];
						}
						$request['TxnTaxDetail']	= array(
							'TotalTax' 					=> $orderTaxAmount,
							'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
						);
						$request['GlobalTaxCalculation']	= 'TaxExcluded';		 				
					}
				}
			}
			else{
				$TxnTaxDetailAdded	= 0;
				if($orderTaxId){
					if($orderTaxAmount > 0){
						$TxnTaxDetailAdded	= 1;
						$request['TxnTaxDetail']	= array(
							'TotalTax'					=> $orderTaxAmount,
							'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
						);
						$request['GlobalTaxCalculation']	= 'TaxExcluded';
					}
				}
				if(!$TxnTaxDetailAdded){
					if(!$orderTaxId){
						$orderTaxId	= $config['TaxCode'];
					}
					$request['TxnTaxDetail']	= array(
						'TotalTax' 					=> $orderTaxAmount,
						'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
					);
					$request['GlobalTaxCalculation']	= 'TaxExcluded';		 				
				}
			}
			
			$qboTermsId	= '';
			if($this->ci->globalConfig['enablePaymenttermsMapping']){
				$PurchaseTermsFiledName	= $config1['PurchaseTermsFiledName'];
				$paymentTermsRowData	= $rowDatas['customFields'][$PurchaseTermsFiledName];
				if($paymentTermsRowData){
					if($paymentTermsMappings[$paymentTermsRowData['id']]){
						if($paymentTermsMappings[$paymentTermsRowData['id']]['account2TermId']){
							$qboTermsId	= $paymentTermsMappings[$paymentTermsRowData['id']]['account2TermId'];
						}
					}
				}
			}
			if($qboTermsId){
				unset($request['DueDate']);
				$request['SalesTermRef']	= array('value' => $qboTermsId);
			}
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				unset($request['TxnTaxDetail']);
			}
			if($this->ci->globalConfig['enableCustomFieldMapping']){
				if($CustomFieldMappings){
					foreach($CustomFieldMappings as $CustomFieldMappingsData){
						$CustomFields		= array();
						$account1FieldIds	= explode(".",$CustomFieldMappingsData['account1CustomFieldPO']);
						$fieldValue			= '';
						$fieldValueTmps		= '';
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.staffOwnerContactId'){
							$CustomstaffOwnerContactId	= $rowDatas['assignment']['current']['staffOwnerContactId'];
							$fieldValueTmps				= $getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.projectId'){
							$CustomprojectId			= $rowDatas['assignment']['current']['projectId'];
							$fieldValueTmps				= $getAllProjectsData[$orderDatas['account1Id']][$CustomprojectId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.channelId'){
							$CustomchannelId			= $rowDatas['assignment']['current']['channelId'];
							$fieldValueTmps				= $getAllChannelMethodData[$orderDatas['account1Id']][$CustomchannelId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.leadSourceId'){
							$CustomleadSourceId			= $rowDatas['assignment']['current']['leadSourceId'];
							$fieldValueTmps				= $getAllLeadsourceData[$orderDatas['account1Id']][$CustomleadSourceId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.teamId'){
							$CustomteamId				= $rowDatas['assignment']['current']['teamId'];
							$fieldValueTmps				= $getAllTeamData[$orderDatas['account1Id']][$CustomteamId]['name'];
						}
						
						/* if($fieldValueTmps){
							$allQBOCustomFieldIds = explode(",",$CustomFieldMappingsData['account2CustomField']);
							foreach($allQBOCustomFieldIds as $allQBOCustomFieldIdsTmp){
								$CustomFields = array();
								if(is_array($fieldValueTmps)){
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> substr($fieldValueTmps['value'],0,31),
										'Type'			=> 'StringType',
									);
								}
								else{
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> substr($fieldValueTmps,0,31),
										'Type'			=> 'StringType',
									);
								}
								if($CustomFields){
									$request['CustomField'][]	= $CustomFields;
								}
							}
						} */
						if($fieldValueTmps){
							$allQBOCustomFieldIds	= explode(",",trim($CustomFieldMappingsData['account2CustomField']));
							foreach($allQBOCustomFieldIds as $allQBOCustomFieldIdsTmp){
								if(strlen($allQBOCustomFieldIdsTmp) == 1){
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> (is_array($fieldValueTmps)) ? (substr($fieldValueTmps['value'],0,31)) : (substr($fieldValueTmps,0,31)),
										'Type'			=> 'StringType',
									);
									$request['CustomField'][]	= $CustomFields;
								}
								else{
									$arrayNumbers	= count(explode(".",$allQBOCustomFieldIdsTmp));
									if($arrayNumbers == 1){
										$request[$allQBOCustomFieldIdsTmp]	= (is_array($fieldValueTmps)) ? ($fieldValueTmps['value']) : ($fieldValueTmps); 
									}
									else{
										$firstKey					= (explode(".",$allQBOCustomFieldIdsTmp))[0];
										$allQBOCustomFieldIdsTmp	= str_replace($firstKey.'.','',$allQBOCustomFieldIdsTmp);
										$finalSentValue				= json_encode((is_array($fieldValueTmps)) ? ($fieldValueTmps['value']) : ($fieldValueTmps));
										$subClose					= '';
										$arrayNumbers			 	= $arrayNumbers - 1;
										for($i = 1; $i <= $arrayNumbers; $i++){
											$subClose	.= '}';
										}
										$request[$firstKey]			= json_decode(('{"'.str_replace(".",'":{"',$allQBOCustomFieldIdsTmp).'":'.$finalSentValue.$subClose), true);
									}
								}
							}
						}
					}
					if(!$request['CustomField']){
						unset($request['CustomField']);
					}
				}
			}
			if($clientcode == 'doubleqbo'){
				unset($request['TxnTaxDetail']);
			}
			
			$url		= 'bill?minorversion=37';
			$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData['Request data	: ']	= $request;
			$createdRowData['Response data	: ']	= $results;
			
			$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if($results['Bill']['Id']){
				$this->ci->db->update('purchase_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['Bill']['Id'],'message' => '', 'PostedTime' => date('c'), 'qboContactID' => $QBOCustomerID),array('id' => $orderDatas['id']));
				$QBOTotalAmount			= $results['Bill']['TotalAmt'];
				$NetRoundOff			= $BrightpearlTotalAmount - $QBOTotalAmount;
				$RoundOffCheck			= abs($NetRoundOff);
				$RoundOffApplicable		= 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.99){
						$RoundOffApplicable	= 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'LineNum'							=> $linNumber++,
							'Description'						=> 'RoundOffItem',
							'Amount'							=> sprintf("%.4f",$NetRoundOff),
							'DetailType'						=> 'ItemBasedExpenseLineDetail',
							'ItemBasedExpenseLineDetail'		=> array(
								'ItemRef'							=> array(
									'value'								=> $config['roundOffItem']
								),
								'Qty'							=> 1,
								'UnitPrice'						=> sprintf("%.4f",$NetRoundOff),
								'TaxCodeRef'					=> array(
									'value'							=> $config['salesNoTaxCode'] 
								),
							),	
						);
						$request['Line']		= $InvoiceLineAdd;
						$request['SyncToken']	= 0;
						$request['Id']			= $results['Bill']['Id'];
						$url					= 'bill?minorversion=37';  
						$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Rounding Request data	: ']	= $request;
						$createdRowData['Rounding Response data	: ']	= $results2;
						$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						
						if($results2['Bill']['Id']){
							$this->ci->db->update('purchase_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results2['Bill']['Id'],'message' => ''),array('id' => $orderDatas['id']));
						}
					}
				}
			}
		}
	}
}
$this->postPurchaseBatchInvoice($orgObjectId);
$this->fetchPurchasePayment();
$this->fetchPurchaseConsolPayment();
?>