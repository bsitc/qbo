<?php
/* QBO : BRIGHTPEARL INTEGRATION */
$this->reInitialize(); 
$clientcode		= $this->ci->config->item('clientcode');
$enableCOGSJournals	= $this->ci->globalConfig['enableCOGSJournals'];
$exchangeRates	= $this->getExchangeRate();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	$exchangeRate		= $exchangeRates[$account2Id];
	$config				= $this->accountConfig[$account2Id];
	$ignoreLineDiscount	= $config['sendNetPriceExcludeDiscount'];
	if(!$config['enablePurchaseConsol']){continue;}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('purchase_order',array('status' => 0, 'account2Id' => $account2Id))->result_array();
	if(!$datas){continue;}
	
	$allDistnictSuppliers	= array();
	$supplierByRefNo		= array();
	$supplierInfos		= array();
	if($config['purchaseConsolBasedON'] == 'suppplierAccountCode'){
		$allSupplierInfo	= array();
		$this->ci->db->reset_query();
		$allSupplierInfo	= $this->ci->db->select('customerId, accountReference')->get_where('customers',array('account2Id' => $account2Id))->result_array();;
		if($allSupplierInfo){
			foreach($allSupplierInfo as $allSupplierInfoTemp){
				if($allSupplierInfoTemp['accountReference']){
					$supplierInfos[$allSupplierInfoTemp['customerId']]				= $allSupplierInfoTemp['accountReference'];
					$supplierByRefNo[$allSupplierInfoTemp['accountReference']][]	= $allSupplierInfoTemp['customerId'];
				}
			}
		}
	}
	
	if($orgObjectId){
		if(count($datas) == 1){
			$singleInvoiceNumber	= $datas[0]['bpInvoiceNumber'];
			$singleSupplierID		= $datas[0]['customerId'];
			if((!$config['purchaseConsolBasedON']) OR ($config['purchaseConsolBasedON'] == 'supplierID')){
				if($singleInvoiceNumber AND $singleSupplierID){
					$batchDatas	= $this->ci->db->get_where('purchase_order',array('status' => 0, 'bpInvoiceNumber' => $singleInvoiceNumber, 'customerId' => $singleSupplierID, 'account2Id' => $account2Id))->result_array();
					if(count($batchDatas) > 1){
						$datas	= $batchDatas;
					}
					else{
						continue;
					}
				}
			}
			else{
				if($singleInvoiceNumber AND $singleSupplierID){
					if($supplierInfos[$singleSupplierID]){
						$accountRefForOrg	= $supplierInfos[$singleSupplierID];
						$batchDatas			= $this->ci->db->get_where('purchase_order',array('status' => 0, 'bpInvoiceNumber' => $singleInvoiceNumber, 'account2Id' => $account2Id))->result_array();
						if(count($batchDatas) > 1){
							$datas	= array();
							foreach($batchDatas as $batchDatasTemp){
								if($batchDatasTemp['customerId']){
									if(($supplierInfos[$batchDatasTemp['customerId']]) AND ($supplierInfos[$batchDatasTemp['customerId']] == $accountRefForOrg)){
										$datas[]	= $batchDatasTemp;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	$batchInvoiceDatas	= array();
	if($datas){
		foreach($datas as $orderDatas){
			$orderId	= $orderDatas['orderId'];
			$rowDatas	= json_decode($orderDatas['rowData'],true);
			$bpInvRef	= $orderDatas['bpInvoiceNumber'];
			$suplierId	= $orderDatas['customerId'];
			$accRefSupp	= '';
			if($supplierInfos[$suplierId]){
				$accRefSupp	= $supplierInfos[$suplierId];
			}
			else{
				if($config['purchaseConsolBasedON'] == 'suppplierAccountCode'){
					continue;
				}
			}
			
			if($orderDatas['uninvoiced']){continue;}
			if($orderDatas['createOrderId']){continue;}
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['invoiced']){continue;}
			if(!$rowDatas['invoices']['0']['invoiceReference']){continue;}
			if((!$config['purchaseConsolBasedON']) OR ($config['purchaseConsolBasedON'] == 'supplierID')){
				$batchInvoiceDatas[$suplierId][$rowDatas['invoices']['0']['invoiceReference']][]	= $orderDatas;
			}
			else{
				$batchInvoiceDatas[$accRefSupp][$rowDatas['invoices']['0']['invoiceReference']][]	= $orderDatas;
			}
		}
	}
	
	if(!$batchInvoiceDatas){continue;}
	
	if($datas){
		$allPostItmeIds	= array();
		foreach($datas as $datasTemp){
			$rowDatas	= json_decode($datasTemp['rowData'], true);
			foreach($rowDatas['orderRows'] as $orderRowsTemp){
				if($orderRowsTemp['productId'] > 1001){
					$allPostItmeIds[]	= $orderRowsTemp['productId'];
				}
			}
		}
		$allPostItmeIds = array_filter($allPostItmeIds);
		$allPostItmeIds = array_unique($allPostItmeIds);
		if($allPostItmeIds){
			$this->postProducts($allPostItmeIds,$account2Id);
		}
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}	
	$allSalesCustomerTemps	= $this->ci->db->select('customerId')->get_where('purchase_order',array('account2Id' => $account2Id, 'status' => '0', 'customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer	= array();
		$allSalesCustomer	= array_column($allSalesCustomerTemps,'customerId');
		$allSalesCustomer	= array_unique($allSalesCustomer);
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer,$account2Id);
		}
	}
	
	$this->ci->db->reset_query();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	$genericcustomerMappings		= array();
	if($genericcustomerMappingsTemps){
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('createdCustomerId <>' => NULL, 'account2Id' => $account2Id))->result_array();
	$customerMappings		= array();
	if($customerMappingsTemps){
		foreach($customerMappingsTemps as $customerMappingsTemp){
			if($customerMappingsTemp['createdCustomerId']){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id,'createdProductId <>' => NULL, ))->result_array();
	$productMappings		= array();
	if($productMappingsTemps){
		foreach($productMappingsTemps as $productMappingsTemp){
			if($productMappingsTemp['createdProductId']){
				$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	if($this->ci->globalConfig['enablePaymenttermsMapping']){
		$this->ci->db->reset_query();
		$paymentTermsMappingsTemps	= $this->ci->db->get_where('mapping_paymentterms',array('type' => 'purchase','account2Id' => $account2Id))->result_array();
		$paymentTermsMappings		= array();
		if($paymentTermsMappingsTemps){
			foreach($paymentTermsMappingsTemps as $paymentTermsMappingsTemp){
				if($paymentTermsMappingsTemp['account1PurchaseTermId'] != 'NA'){
					$paymentTermsMappings[$paymentTermsMappingsTemp['account1PurchaseTermId']]	= $paymentTermsMappingsTemp;
				}
			}
		}
	}
	
	if($this->ci->globalConfig['enableCustomFieldMapping']){
		$this->ci->db->reset_query();
		$isAssignmentMapped						= 0;
		$isAssignmentstaffOwnerContactIdMapped	= 0;
		$isAssignmentprojectIdMapped			= 0;
		$isAssignmentchannelIdMapped			= 0;
		$isAssignmentleadSourceIdMapped			= 0;
		$isAssignmentteamIdMapped				= 0;
		$CustomFieldMappingsTemps				= $this->ci->db->get_where('mapping_customfield',array('type' => 'purchase', 'account2Id' => $account2Id))->result_array();
		$CustomFieldMappings					= array();
		if($CustomFieldMappingsTemps){
			foreach($CustomFieldMappingsTemps as $CustomFieldMappingsTemp){
				$CustomFieldMappings[$CustomFieldMappingsTemp['account1CustomField']]	= $CustomFieldMappingsTemp;
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'assignment')){
					$isAssignmentMapped	= 1;
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'staffownercontactid')){
						$isAssignmentstaffOwnerContactIdMapped	= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'projectid')){
						$isAssignmentprojectIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'channelid')){
						$isAssignmentchannelIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'leadsourceid')){
						$isAssignmentleadSourceIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'teamid')){
						$isAssignmentteamIdMapped				= 1;
					}
				}
			}
		}
		if($CustomFieldMappings AND $isAssignmentMapped){
			if($isAssignmentstaffOwnerContactIdMapped){
				$getAllSalesrepData			= $this->ci->brightpearl->getAllSalesrep();
			}
			if($isAssignmentprojectIdMapped){
				$getAllProjectsData			= $this->ci->brightpearl->getAllProjects();
			}
			if($isAssignmentchannelIdMapped){
				$getAllChannelMethodData	= $this->ci->brightpearl->getAllChannelMethod();
			}
			if($isAssignmentleadSourceIdMapped){
				$getAllLeadsourceData		= $this->ci->brightpearl->getAllLeadsource();
			}
			if($isAssignmentteamIdMapped){
				$getAllTeamData				= $this->ci->brightpearl->getAllTeam();
			}
		}
	}
	
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	if($batchInvoiceDatas){
		foreach($batchInvoiceDatas as $bpSupplierIds => $batchInvoiceDatass){
			foreach($batchInvoiceDatass as $bpInvNumber => $batchInvoiceData){
				if(count($batchInvoiceData) > 1){
					$aggreagationID			= '';
					$CurrencyCode			= '';
					$CurrencyRate			= 0;
					$qboSupplierId			= '';
					$CustomerCompanyName	= '';
					$taxDate				= '';
					$dueDate				= '';
					$shipAddress			= array();
					$processedOrderIds		= array();
					$request				= array();
					$missingSkus			= array();
					$ProductArray			= array();
					$InvoiceLineAdd			= array();
					$invoiceLineCount		= 0;
					$linNumber				= 0;
					$allBPtotalAmount		= 0;
					$BrightpearlTotalAmount	= 0;
					$channelId				= '';
					$CountryIsoCode			= '';
					
					if($config['purchaseConsolBasedON'] == 'suppplierAccountCode'){
						foreach($supplierByRefNo[$bpSupplierIds] as $bpSupplierIdBYRef){
							if(!$qboSupplierId){
								if($customerMappings[$bpSupplierIdBYRef]['createdCustomerId']){
									$qboSupplierId			= $customerMappings[$bpSupplierIdBYRef]['createdCustomerId'];
								}
							}
							else{
								break;
							}
						}
					}
					else{
						if($customerMappings[$bpSupplierIds]['createdCustomerId']){
							$qboSupplierId	= $customerMappings[$bpSupplierIds]['createdCustomerId'];
						}
					}
					
					foreach($batchInvoiceData as $purchaseData){
						$rowDatas	= json_decode($purchaseData['rowData'],true);
						
						if(!$CurrencyCode){
							$CurrencyCode	= $rowDatas['currency']['orderCurrencyCode'];
						}
						if(!$CurrencyRate){
							$CurrencyRate	= $rowDatas['currency']['exchangeRate'];
						}
						if(!$taxDate){
							if($rowDatas['invoices']['0']['taxDate']){
								$taxDate	= $rowDatas['invoices']['0']['taxDate'];
							}
						}
						if(!$dueDate){
							if($rowDatas['invoices']['0']['dueDate']){
								$dueDate		= $rowDatas['invoices']['0']['dueDate'];
							}
						}
						$bpconfig				= $this->ci->account1Config[$purchaseData['account1Id']];
						$orderId				= $purchaseData['orderId'];
						$isDiscountCouponAdded	= 0;
						$couponItemLineID		= '';
						$DiscountItemAccountRef	= '';
						$totalItemDiscount		= array();
						$discountCouponAmt		= array();
						$BrightpearlTotalAmount	= $rowDatas['totalValue']['total'];
						$allBPtotalAmount		+= $rowDatas['totalValue']['total'];
						$BrightpearlTotalTAX	= $rowDatas['totalValue']['taxAmount'];
						$PostalCode				= $rowDatas['parties']['delivery']['postalCode'];
						$CountryIsoCode			= $rowDatas['parties']['delivery']['countryIsoCode'];
						$shipAddress			= $rowDatas['parties']['delivery'];
						$CountryIsoCode			= strtolower(trim($shipAddress['countryIsoCode3']));
						$countryState			= strtolower(trim($shipAddress['addressLine4']));
						$channelId				= $rowDatas['assignment']['current']['channelId'];
						if(!$channelId){
							$channelId	= 'blank';
						}
						
						$processedOrderIds[]	= $orderId;
						
						ksort($rowDatas['orderRows']);
						foreach($rowDatas['orderRows'] as $rowId => $orderRows){
							if(substr_count(strtolower($orderRows['productName']),'coupon')){
								if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
									$isDiscountCouponAdded	= 1;
									$couponItemLineID		= $rowId;
								}
							}
						}
						foreach($rowDatas['orderRows'] as $rowId => $orderRows){
							$taxMapping		= array();
							$params			= '';
							$ItemRefName	= '';
							$ItemRefValue	= '';
							$LineTaxId		= '';
							$orderTaxId		= '';
							$productId		= $orderRows['productId'];
							
							if($rowId == $couponItemLineID){
								if($orderRows['rowValue']['rowNet']['value'] == 0){
									continue;
								}
							}
							
							
							$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
							$isKidsTaxEnabled	= 0;
							if($kidsTaxCustomField){
								if($productMappings[$productId]){
									$productParams	= json_decode($productMappings[$productId]['params'], true);
									if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
										$isKidsTaxEnabled	= 1;
									}
								}
							}
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'];
							if($isStateEnabled){
								if($isChannelEnabled){
									$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode.'-'.$countryState.'-'.$channelId;
									$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode.'-'.$channelId;
								}
								else{
									$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode.'-'.$countryState;
									$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode;
								}
							}
							$taxMappingKey	= strtolower($taxMappingKey);
							$taxMappingKey1	= strtolower($taxMappingKey1);
							if(isset($taxMappings[$taxMappingKey])){
								$taxMapping	= $taxMappings[$taxMappingKey];
							}
							elseif(isset($taxMappings[$taxMappingKey1])){
								$taxMapping	= $taxMappings[$taxMappingKey1];
							}
							elseif(isset($taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId])){
								$taxMapping	= $taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId];
							}
							$LineTaxId  = $taxMapping['account2LineTaxId'];
							$orderTaxId = $taxMapping['account2TaxId'];
							if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'] OR $taxMapping['account2KidsTaxId'])){
								$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
								$orderTaxId = $taxMapping['account2KidsTaxId'];
							}
							
							if($productId > 1001){
								if(!$productMappings[$productId]['createdProductId']){
									$missingSkus[]	= $orderRows['productSku'];
									continue;
								}
								$ItemRefValue	= $productMappings[$productId]['createdProductId'];
								$ItemRefName	= $productMappings[$productId]['sku'];
							}
							else{
								if($orderRows['rowValue']['rowNet']['value'] > 0){
									$ItemRefValue		= $config['genericSku'];
									$ItemRefName		= $orderRows['productName'];
									if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
										$ItemRefValue	= $config['shippingItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
										$ItemRefValue	= $config['giftCardItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
										$ItemRefValue	= $config['landedCostItem'];
										$ItemRefName	= $orderRows['productName'];
									}
								}
								else if($orderRows['rowValue']['rowNet']['value'] < 0){
									$ItemRefValue		= $config['genericSku'];
									$ItemRefName		= $orderRows['productName'];
									if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
										$ItemRefValue	= $config['couponItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
										$ItemRefValue	= $config['giftCardItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
										$ItemRefValue	= $config['shippingItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
										$ItemRefValue	= $config['landedCostItem'];
										$ItemRefName	= $orderRows['productName'];
									}
								}
								else{
									$ItemRefValue		= $config['genericSku'];
									$ItemRefName		= $orderRows['productName'];
									if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
										$ItemRefValue	= $config['couponItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
										$ItemRefValue	= $config['giftCardItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
										$ItemRefValue	= $config['shippingItem'];
										$ItemRefName	= $orderRows['productName'];
									}
									if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
										$ItemRefValue	= $config['landedCostItem'];
										$ItemRefName	= $orderRows['productName'];
									}
								}
							}
							
							$BPNominal			=  $orderRows['nominalCode'];
							$price				=  $orderRows['rowValue']['rowNet']['value'];
							//$orderTaxAmount		+= $orderRows['rowValue']['rowTax']['value'];
							$originalPrice		= $price;
							$discountPercentage	=  0;
							
							if(($orderRows['discountPercentage'] > 0) && (!$ignoreLineDiscount)){
								$discountPercentage	= 100 - $orderRows['discountPercentage'];
								if($discountPercentage == 0){
									$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
								}
								else{
									$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
								}
								$tempTaxAmt	= $originalPrice - $price;
								if($tempTaxAmt > 0){
									if(isset($totalItemDiscount[$LineTaxId][$BPNominal])){
										$totalItemDiscount[$LineTaxId][$BPNominal]['AMT']	+= $tempTaxAmt;
									}
									else{
										$totalItemDiscount[$LineTaxId][$BPNominal]['AMT']	= $tempTaxAmt;
									}
								}
							}
							else if($isDiscountCouponAdded){
								if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
									$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
									if(!$originalPrice){
										$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
									}
									if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
										$discountCouponAmtTemp	= ($originalPrice - $price);
										if($discountCouponAmtTemp > 0){
											if(isset($discountCouponAmt[$LineTaxId][$BPNominal])){
												$discountCouponAmt[$LineTaxId][$BPNominal]['AMT']	+= $discountCouponAmtTemp;
											}
											else{
												$discountCouponAmt[$LineTaxId][$BPNominal]['AMT']	= $discountCouponAmtTemp;
											}
										}
									}
									else{
										$originalPrice	= $orderRows['rowValue']['rowNet']['value'];
									}
								}
							}
							$params	= json_decode($productMappings[$productId]['params'],true);
							if($ItemRefValue){
								$ExpenseAccountRef	= $config['ExpenseAccountRef'];
								if(isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
									$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']])) AND ($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'])){
									$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
								}
								$nominalClassID		= '';
								if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]))){
									$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]['account2ClassId'];
								}
								$UnitAmount	= 0.00;
								if($originalPrice != 0){
									$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
								}
								if($enableCOGSJournals AND $purchaseData['LinkedWithSO']){
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'Amount'							=> sprintf("%.4f",($UnitAmount*($orderRows['quantity']['magnitude']))),
										'Description'						=> $orderRows['productName'],
										'DetailType'						=> 'AccountBasedExpenseLineDetail',
										'AccountBasedExpenseLineDetail'		=> array(
											'AccountRef'						=> array('value' => $ExpenseAccountRef),
											'TaxCodeRef'						=> array('value' => $LineTaxId),
											'BillableStatus'					=> 'NotBillable',
										),
									);
									if($nominalClassID){
										$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
									}
									if(!$LineTaxId){
										unset($InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['TaxCodeRef']);
									}
									$invoiceLineCount++;
								}
								else{
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'LineNum'							=> $linNumber++,
										'Description'						=> $orderRows['productName'],
										'Amount'							=> sprintf("%.4f",($UnitAmount*($orderRows['quantity']['magnitude']))),
										'DetailType'						=> 'ItemBasedExpenseLineDetail',
										'ItemBasedExpenseLineDetail'		=> array(
											'BillableStatus'					=> 'NotBillable',
											'ItemRef'							=> array('value' => $ItemRefValue,),
											'Qty'								=> $orderRows['quantity']['magnitude'],
											'TaxCodeRef'						=> array('value' => $LineTaxId),
											'UnitPrice'							=> sprintf("%.4f",($UnitAmount)),
										),
									);
									if($nominalClassID){
										$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
									}
									if(!empty($defaultItemMapping)){
										if(isset($defaultItemMapping[$channelId][$BPNominal])){
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $defaultItemMapping[$channelId][$BPNominal]);
										}
									}
									if(!$LineTaxId){
										unset($InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']);
									}
									$invoiceLineCount++;
								}
							}
						}
						if($totalItemDiscount){
							foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmountData){
								foreach($totalItemDiscountLineAmountData as $BPNominalCode => $totalItemDiscountLineAmount){
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'LineNum'							=> $linNumber++,
										'Description' 						=> 'Item Discount',
										'Amount'							=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount['AMT'])),
										'DetailType'						=> 'ItemBasedExpenseLineDetail',
										'ItemBasedExpenseLineDetail'		=> array(
											'ItemRef' 							=> array( 'value' => $config['discountItem']),
											'Qty' 								=> 1,
											'UnitPrice' 						=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount['AMT'])),
										),	
									);
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									if($nominalClassID){
										$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']		= array('value' => $nominalClassID);
									}
									if($TaxID){
										$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
									}
									else{
										$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
									}
									$invoiceLineCount++;
								}
							}
						}
						if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							if($discountCouponAmt){
								foreach($discountCouponAmt as $TaxID => $discountCouponLineAmountData){
									foreach($discountCouponLineAmountData as $BPNominalCode => $discountCouponLineAmount){
										$InvoiceLineAdd[$invoiceLineCount] = array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Coupon Discount',
											'Amount'				=> sprintf("%.4f",((-1) * $discountCouponLineAmount['AMT'])),
											'DetailType'			=> 'ItemBasedExpenseLineDetail',
											'ItemBasedExpenseLineDetail'	=> array('ItemRef' => array('value' => $config['couponItem']),
												'Qty'						=> 1,
												'UnitPrice'					=> sprintf("%.4f",((-1) * $discountCouponLineAmount['AMT'])),
											),	
										);
										$nominalClassID		= '';
										if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
											$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
										}
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']		= array('value' => $nominalClassID);
										}
										if($TaxID){
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
										}
										else{
											$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
										}
										$invoiceLineCount++;
									}
								}
							}
						}
						if($config['SendTaxAsLineItem']){
							if($BrightpearlTotalTAX > 0){
								$InvoiceLineAdd[$invoiceLineCount] = array(
									'LineNum'				=> $linNumber++,
									'Description'			=> 'Total Tax Amount',
									'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
									'DetailType'			=> 'ItemBasedExpenseLineDetail',
									'ItemBasedExpenseLineDetail'	=> array(
										'ItemRef'						=> array('value' => $config['TaxLineItemCode']),
										'Qty' 							=> 1,
										'UnitPrice' 					=> sprintf("%.4f",$BrightpearlTotalTAX),
										'TaxCodeRef' 					=> array('value' => $config['salesNoTaxCode']),
									),	
								);
								$invoiceLineCount++;
							}
						}
					}
					if($missingSkus){
						$missingSkus	= array_unique($missingSkus);
						$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('account2Id' => $account2Id));
						continue;
					}
					if(!$qboSupplierId){
						$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('message' => 'Supplier Not Posted'),array('account2Id' => $account2Id));
						continue;
					}
					if(!$taxDate OR !$dueDate){
						$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('message' => 'TaxDate Not Found'),array('account2Id' => $account2Id));
						continue;
					}
					else{
						//taxdate chanages
						$BPDateOffset	= (int)substr($taxDate,23,3);
						$QBOoffset		= 0;
						$diff			= $BPDateOffset - $QBOoffset;
						$date1			= new DateTime($dueDate);
						$date			= new DateTime($taxDate);
						$BPTimeZone		= 'GMT';
						$date1->setTimezone(new DateTimeZone($BPTimeZone));
						$date->setTimezone(new DateTimeZone($BPTimeZone));
						if($diff > 0){
							$diff			.= ' hour';
							$date->modify($diff);
							$date1->modify($diff);
						}
						$taxDate		= $date->format('Y-m-d');
						$dueDate		= $date1->format('Y-m-d');
					}
					if($InvoiceLineAdd){
						
						$lineLevelClassId	= '';
						if((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
							$lineLevelClassId	= $channelMappings[$channelId]['account2ChannelId'];
						}
						if($lineLevelClassId){
							foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
								if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
									continue;
								}
								else{
									$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
								}
							}
						}
						$bpInvNumber	= substr($bpInvNumber,0,21);
						$request	= array(
							'DocNumber'		=> $bpInvNumber,
							'VendorRef'		=> array('value' => $qboSupplierId),
							'TxnDate'		=> $taxDate,
							'DueDate'		=> $dueDate,
							'Line'			=> $InvoiceLineAdd,
							'ShipAddr'		=> array(
								'Line1'						=> @$shipAddress['addressLine1'],
								'Line2'						=> @$shipAddress['addressLine2'],
								'City'						=> @$shipAddress['addressLine3'],
								'CountrySubDivisionCode'	=> @$shipAddress['addressLine4'],
								'Country'					=> @$shipAddress['countryIsoCode'],
								'PostalCode'				=> @$shipAddress['postalCode'],
							),
							'ExchangeRate'	=> sprintf("%.4f",(1 / $rowDatas['currency']['exchangeRate'])),
							'CurrencyRef'	=> array('value' => $CurrencyCode),
						);
						$request['GlobalTaxCalculation']	= 'TaxExcluded';
						if($CurrencyRate){
							$request['ExchangeRate']	= sprintf("%.4f",(1 / $CurrencyRate));
						}
						else{
							$request['ExchangeRate']	= 1;
						}
						if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
							$exRate = $this->getQboExchangeRateByDb($account2Id,$rowDatas['currency']['orderCurrencyCode'],$config['defaultCurrrency'],$taxDate);
							if($exRate){
								$request['ExchangeRate'] = $exRate;
							}
							else{
								$exRate = $exchangeRate[strtolower($rowDatas['currency']['orderCurrencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
								if($exRate){
									$request['ExchangeRate'] = $exRate;
								}
								else{
									echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
									if(isset($request['ExchangeRate'])){
										unset($request['ExchangeRate']);
									}
								}
							}
						}
						if($this->ci->globalConfig['enableAdvanceTaxMapping']){
							if(isset($request['TxnTaxDetail'])){
								unset($request['TxnTaxDetail']);
							}
						}
						
						$aggreagationID	= uniqid();
						$url			= 'bill?minorversion=37';
						$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData	= array(
							'Request data	: '	=> $request,
							'Response data	: '	=> $results,
						);
					
						if($results['Bill']['Id']){
							$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('sendInAggregation' => 1, 'aggregationId' => $aggreagationID, 'createdRowData' => json_encode($createdRowData), 'status' => '1','invoiced' => '0','invoiceRef' => $bpInvNumber,'createOrderId' => $results['Bill']['Id'],'message'=>'','PostedTime' => date('c'), 'qboContactID' => $qboSupplierId),array('account2Id' => $account2Id));
							
							$QBOTotalAmount			= $results['Bill']['TotalAmt'];
							$NetRoundOff			= $allBPtotalAmount - $QBOTotalAmount;
							$RoundOffCheck			= abs($NetRoundOff);
							$RoundOffApplicable		= 0;
							if($RoundOffCheck != 0){
								if($RoundOffCheck < 0.99){
									$RoundOffApplicable	= 1;
								}
								if($RoundOffApplicable){
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'LineNum'							=> $linNumber++,
										'Description'						=> 'RoundOffItem',
										'Amount'							=> sprintf("%.4f",$NetRoundOff),
										'DetailType'						=> 'ItemBasedExpenseLineDetail',
										'ItemBasedExpenseLineDetail'		=> array(
											'ItemRef'							=> array('value' => $config['roundOffItem']),
											'Qty'							=> 1,
											'UnitPrice'						=> sprintf("%.4f",$NetRoundOff),
											'TaxCodeRef'					=> array(
												'value'							=> $config['salesNoTaxCode'] 
											),
										),	
									);
									$request['Line']		= $InvoiceLineAdd;
									$request['SyncToken']	= 0;
									$request['Id']			= $results['Bill']['Id'];
									$url					= 'bill?minorversion=37';  
									$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
									$createdRowData['Rounding Request data	: ']	= $request;
									$createdRowData['Rounding Response data	: ']	= $results2;
									if($results2['Bill']['Id']){
										$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('createOrderId' => $results2['Bill']['Id'], 'createdRowData' => json_encode($createdRowData)),array('account2Id' => $account2Id));
									}
									else{
										$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)),array('account2Id' => $account2Id));
									}
								}
							}
						}
						else{
							$this->ci->db->where_in('orderId', $processedOrderIds)->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('account2Id' => $account2Id));
						}
					}
				}
			}
		}
	}
}