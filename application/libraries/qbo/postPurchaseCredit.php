<?php
//qbo
$this->reInitialize();
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$enableCOGSJournals			= $this->ci->globalConfig['enableCOGSJournals'];
$exchangeRates				= $this->getExchangeRate();
$UnInvoicingEnabled			= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('purchase_credit_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$this->postProductAccount	= $account2Id;
	$exchangeRate				= $exchangeRates[$account2Id];
	$config						= $this->accountConfig[$account2Id];
	$ignoreLineDiscount			= $config['sendNetPriceExcludeDiscount'];
	
	if($datas){
		$allPostItmeIds	= array();
		foreach($datas as $datasTemp){
			$rowDatas	= json_decode($datasTemp['rowData'], true);
			foreach($rowDatas['orderRows'] as $orderRowsTemp){
				if($orderRowsTemp['productId'] > 1001){
					$allPostItmeIds[]	= $orderRowsTemp['productId'];
				}
			}
		}
		$allPostItmeIds = array_filter($allPostItmeIds);
		$allPostItmeIds = array_unique($allPostItmeIds);
		if($allPostItmeIds){
			$this->postProducts($allPostItmeIds,$account2Id);
		}
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}	
	$allSalesCustomerTemps	= $this->ci->db->select('customerId')->get_where('purchase_credit_order',array('account2Id' => $account2Id, 'status' => '0', 'customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer	= array();
		$allSalesCustomer	= array_column($allSalesCustomerTemps,'customerId');
		$allSalesCustomer	= array_unique($allSalesCustomer);
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer,$account2Id);
		}
	}
	$this->ci->db->reset_query();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	$genericcustomerMappings		= array();
	if($genericcustomerMappingsTemps){
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('createdCustomerId <>' => NULL, 'account2Id' => $account2Id))->result_array();
	$customerMappings		= array();
	if($customerMappingsTemps){
		foreach($customerMappingsTemps as $customerMappingsTemp){
			if($customerMappingsTemp['createdCustomerId']){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id, 'createdProductId <>' => NULL))->result_array();
	$productMappings		= array();
	if($productMappingsTemps){
		foreach($productMappingsTemps as $productMappingsTemp){
			if($productMappingsTemp['createdProductId']){
				$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$CustomFieldMappings					= array();
	$getAllSalesrepData			= array();
	$getAllProjectsData			= array();
	$getAllChannelMethodData	= array();
	$getAllLeadsourceData		= array();
	$getAllTeamData				= array();
	if($this->ci->globalConfig['enableCustomFieldMapping']){
		$this->ci->db->reset_query();
		$isAssignmentMapped						= 0;
		$isAssignmentstaffOwnerContactIdMapped	= 0;
		$isAssignmentprojectIdMapped			= 0;
		$isAssignmentchannelIdMapped			= 0;
		$isAssignmentleadSourceIdMapped			= 0;
		$isAssignmentteamIdMapped				= 0;
		$CustomFieldMappingsTemps				= $this->ci->db->get_where('mapping_customfield',array('type' => 'purchase', 'account2Id' => $account2Id))->result_array();
		if($CustomFieldMappingsTemps){
			foreach($CustomFieldMappingsTemps as $CustomFieldMappingsTemp){
				$CustomFieldMappings[$CustomFieldMappingsTemp['account1CustomField']]	= $CustomFieldMappingsTemp;
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'assignment')){
					$isAssignmentMapped	= 1;
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'staffownercontactid')){
						$isAssignmentstaffOwnerContactIdMapped	= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'projectid')){
						$isAssignmentprojectIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'channelid')){
						$isAssignmentchannelIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'leadsourceid')){
						$isAssignmentleadSourceIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'teamid')){
						$isAssignmentteamIdMapped				= 1;
					}
				}
			}
		}
		if($CustomFieldMappings AND $isAssignmentMapped){
			if($isAssignmentstaffOwnerContactIdMapped){
				$getAllSalesrepData			= $this->ci->brightpearl->getAllSalesrep();
			}
			if($isAssignmentprojectIdMapped){
				$getAllProjectsData			= $this->ci->brightpearl->getAllProjects();
			}
			if($isAssignmentchannelIdMapped){
				$getAllChannelMethodData	= $this->ci->brightpearl->getAllChannelMethod();
			}
			if($isAssignmentleadSourceIdMapped){
				$getAllLeadsourceData		= $this->ci->brightpearl->getAllLeadsource();
			}
			if($isAssignmentteamIdMapped){
				$getAllTeamData				= $this->ci->brightpearl->getAllTeam();
			}
		}
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	if($datas){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			
			$config1	= $this->ci->account1Config[$orderDatas['account1Id']];
			$bpconfig	= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId	= $orderDatas['orderId'];
			
			/* UNINVOICING CODE STARTS */
			if($UnInvoicingEnabled){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						$TempAcc2ID		= '';
						if($orderDatas['TempAcc2ID']){
							$TempAcc2ID	= $orderDatas['TempAcc2ID'];
						}
						else{
							$TempAcc2ID	= $account2Id;
						}
						$IsPaid			= 0;
						if($orderDatas['paymentDetails']){
							$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
							foreach($paymentDetails as $pKey => $paymentDetail){
								if($paymentDetail['amount'] > 0){
									if($paymentDetail['status'] == 1){
										$IsPaid	= 1;
										break;
									}
								}
							}
						}
						else{
							$IsPaid	= 0;
						}
						if($IsPaid == 0){
							$createdRowData	= json_decode($orderDatas['createdRowData'],true);
							$uninvoiceCount	= $orderDatas['uninvoiceCount'];
							$SyncToken		= '';
							$QBOorderId		= $orderDatas['createOrderId'];
							$readurl		= 'vendorcredit/'.$QBOorderId;
							$billResponse	= $this->getCurl($readurl, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
							if($billResponse['VendorCredit']){
								$SyncToken	= $billResponse['VendorCredit']['SyncToken'];
							}
							$request		= array(
								"SyncToken"		=>	$SyncToken,
								"Id"			=>	$QBOorderId,
							);
							$url			= 'vendorcredit?operation=delete';
							$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $TempAcc2ID)[$TempAcc2ID];
							$createdRowData['Void Request data	: ']	= $request;
							$createdRowData['Void Response data	: ']	= $results;
							$this->ci->db->update('purchase_credit_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
							if($results['VendorCredit']['status'] == 'Deleted'){
								$uninvoiceCount++;
								$PaymentState	= 1;
								
								if($orderDatas['PaymentState'] == 2){
									$PaymentState	= 2;
								}
								
								$this->ci->db->update('purchase_credit_order',array('paymentDetails' => '', 'TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount, 'PaymentState' => $PaymentState),array('orderId' => $orderId));
								continue;
							}
							else{
								$this->ci->db->update('purchase_credit_order',array('message' => 'Unable to Delete the VendorCredit'),array('orderId' => $orderId));
								continue;
							}
						}
						else{
							continue;
						}
						continue;
					}
				}
			}
			/* UNINVOICING ENDS */
			
			if($orderDatas['createOrderId']){continue;}
			if(!$orderDatas['invoiced']){continue;}
			
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			if(!$channelId){
				$channelId	= 'blank';
			}
			
			//Dropship changes in PC added in 15th march 2023
			$isDropshipPC			= 0;
			if(isset($rowDatas['parentOrderId'])){
				$parentOrderId			= $rowDatas['parentOrderId'];
				if(strlen($parentOrderId) > 0){
					$parentOrderUrl		= '/order-service/order/'.$parentOrderId;
					$parentOrderInfo	= $this->ci->brightpearl->getCurl($parentOrderUrl, "GET", '', 'json', $orderDatas['account1Id'])[$orderDatas['account1Id']];
					if(isset($parentOrderInfo[0])){
						if(($parentOrderInfo[0]['orderTypeCode'] == 'PO') AND ($parentOrderInfo[0]['isDropship'] == 1)){
							$isDropshipPC	= 1;
						}
					}
				}
			}
			
			$billAddress			= $rowDatas['parties']['billing'];
			$shipAddress			= $rowDatas['parties']['delivery'];
			$orderCustomer			= $rowDatas['parties']['supplier'];
			$BrightpearlTotalAmount	= $rowDatas['totalValue']['total'];
			$BrightpearlTotalTAX	= $rowDatas['totalValue']['taxAmount'];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				$this->ci->db->update('purchase_credit_order',array('message' => 'Supplier not sent yet.'),array('orderId' => $orderId));
				continue;
			}
			if(!$rowDatas['invoices']['0']['invoiceReference']){continue;}
			
			$missingSkus					= array();
			$InvoiceLineAdd					= array();
			$discountCouponAmt				= array();
			$totalItemDiscount				= array();
			$invoiceLineCount				= 0;
			$isDiscountCouponAdded			= 0;
			$orderTaxAmount					= 0;
			$taxAmount						= 0;
			$linNumber						= 1;
			$orderTaxId						= '';
			$couponItemLineID				= '';
			$shippingLineID					= '';
			$ItemRefValue					= '';
			$ItemRefName					= '';
			$countryIsoCode					= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState					= strtolower(trim($shipAddress['addressLine4']));
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$productId	= $orderRows['productId'];
				if($orderRows['productId'] <= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID			= $rowId;
					}
				}
			}
			$itemtaxAbleLine		= $config['orderLineTaxCode'];
			$discountTaxAbleLine	= $config['orderLineTaxCode'];
			
			ksort($rowDatas['orderRows']);
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($rowId == $couponItemLineID){
					if($orderRows['rowValue']['rowNet']['value'] == 0){
						continue;
					}
				}
				
				
				$taxMapping			= array();
				$params				= '';
				$ItemRefName		= '';
				$ItemRefValue		= '';
				$LineTaxId			= '';
				$orderTaxId			= '';
				$productId			= $orderRows['productId'];
				
				$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
				$isKidsTaxEnabled	= 0;
				if($kidsTaxCustomField){
					if($productMappings[$productId]){
						$productParams	= json_decode($productMappings[$productId]['params'], true);
						if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
							$isKidsTaxEnabled	= 1;
						}
					}
				}
				
				$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
				$taxMappingKey1	= $orderRows['rowValue']['taxClassId'];
				if($isStateEnabled){
					if($isChannelEnabled){
						$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
						$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$channelId;
					}
					else{
						$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState;
						$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode;
					}
				}
				$taxMappingKey	= strtolower($taxMappingKey);
				$taxMappingKey1	= strtolower($taxMappingKey1);
				if(isset($taxMappings[$taxMappingKey])){
					$taxMapping	= $taxMappings[$taxMappingKey];
				}
				elseif(isset($taxMappings[$taxMappingKey1])){
					$taxMapping	= $taxMappings[$taxMappingKey1];
				}
				elseif(isset($taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId])){
					$taxMapping	= $taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId];
				}
				$LineTaxId  = $taxMapping['account2LineTaxId'];
				$orderTaxId = $taxMapping['account2TaxId'];
				if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'] OR $taxMapping['account2KidsTaxId'])){
					$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
					$orderTaxId = $taxMapping['account2KidsTaxId'];
				}
				
				if($productId > 1001){
					if(!$productMappings[$productId]['createdProductId']){
						$missingSkus[]	= $orderRows['productSku'];
						continue;
					}
					$ItemRefValue	= $productMappings[$productId]['createdProductId'];
					$ItemRefName	= $productMappings[$productId]['sku'];
				}
				else{
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue		= $config['genericSku'];
						$ItemRefName		= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
							$ItemRefValue	= $config['landedCostItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue		= $config['genericSku'];
						$ItemRefName		= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
							$ItemRefValue	= $config['landedCostItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
					else{
						$ItemRefValue		= $config['genericSku'];
						$ItemRefName		= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
							$ItemRefValue	= $config['landedCostItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
				}
				$price				=  $orderRows['rowValue']['rowNet']['value']; 
				$orderTaxAmount		+= $orderRows['rowValue']['rowTax']['value'];
				$originalPrice		= $price;
				$discountPercentage	=  0;
				
				if(($orderRows['discountPercentage'] > 0) && (!$ignoreLineDiscount)){
					$discountPercentage	= 100 - $orderRows['discountPercentage'];
					if($discountPercentage == 0){
						$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					}
					else{
						$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
					}
					$tempTaxAmt	= $originalPrice - $price;
					if($tempTaxAmt > 0){
						if(isset($totalItemDiscount[$LineTaxId])){
							$totalItemDiscount[$LineTaxId]	+= $tempTaxAmt;
						}
						else{
							$totalItemDiscount[$LineTaxId]	= $tempTaxAmt;
						}
					}
				}
				else if($isDiscountCouponAdded){
					if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
						$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
						if(!$originalPrice){
							$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
						}
						if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
							$discountCouponAmtTemp	= ($originalPrice - $price);
							if($discountCouponAmtTemp > 0){
								if(isset($discountCouponAmt[$LineTaxId])){
									$discountCouponAmt[$LineTaxId]	+= $discountCouponAmtTemp;
								}
								else{
									$discountCouponAmt[$LineTaxId]	= $discountCouponAmtTemp;
								}
							}
						}
						else{
							$originalPrice	= $orderRows['rowValue']['rowNet']['value'];
						}
					}
				}
				$IncomeAccountRef	= $config['IncomeAccountRef'];
				$params				= json_decode($productMappings[$productId]['params'],true);
				
				// if $config['InventoryManagementEnabled'] == false	 that means connector is InventoryManaged
				if($config['InventoryManagementEnabled'] == false){
					if($productId > 1001){
						if($params){
							if($params['stock']['stockTracked']){
								$ItemRefValue	= $config['PurchaseCreditItem'];
							}
						}	
					}		
				}
				
				// if $config['InventoryManagementEnabled'] == true	 that means connector is Non-InventoryManaged
				if($config['InventoryManagementEnabled'] == true){
					if($enableCOGSJournals){
						if($productId > 1001){
							if($params){
								if($params['stock']['stockTracked']){
									$ItemRefValue	= $config['PurchaseCreditItem'];
								}
								else{
									if($config['disableSkuDetails']){
										$ItemRefValue	= $config['genericSku'];
									}
								}
							}	
						}
					}
				}
				
				if($ItemRefValue){
					$dropShipPCnominal	= $config['COGSDebitNominalSO'];
					$ExpenseAccountRef	= $config['ExpenseAccountRef'];
					$AssetAccountRef	= $config['AssetAccountRef'];
					if(isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
						$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
						$dropShipPCnominal	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']])) AND ($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'])){
						$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
						$dropShipPCnominal	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
					}
					$UnitAmount	= 0.00;
					if($originalPrice != 0){
						$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
					}
					if(($isDropshipPC == 1) AND (!in_array($orderRows['nominalCode'],$nominalCodeForShipping))){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'Amount'							=> sprintf("%.4f",($UnitAmount*($orderRows['quantity']['magnitude']))),
							'Description'						=> $orderRows['productName'],
							'DetailType'						=> 'AccountBasedExpenseLineDetail',
							'AccountBasedExpenseLineDetail'		=> array(
								'AccountRef'						=> array('value' => $dropShipPCnominal),
								'TaxCodeRef'						=> array('value' => $LineTaxId),
								'BillableStatus'					=> 'NotBillable',
							),
						);
						
						$nominalClassID		= '';
						if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]))){
							$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]['account2ClassId'];
						}
						if($nominalClassID){
							$InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
						}
						
						if(!$LineTaxId){
							unset($InvoiceLineAdd[$invoiceLineCount]['AccountBasedExpenseLineDetail']['TaxCodeRef']);
						}
						$invoiceLineCount++;
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'LineNum'							=> $linNumber++,
							'Description'						=> $orderRows['productName'],
							'Amount'							=> sprintf("%.4f",($UnitAmount*($orderRows['quantity']['magnitude']))),
							'DetailType'						=> 'ItemBasedExpenseLineDetail',
							'ItemBasedExpenseLineDetail'		=> array(
								'BillableStatus'					=> 'NotBillable',
								'ItemRef'							=> array('value' => $ItemRefValue,),
								'Qty'								=> $orderRows['quantity']['magnitude'],
								'TaxCodeRef'						=> array('value' => $LineTaxId),
								'UnitPrice'							=> sprintf("%.4f",($UnitAmount)),
							),
						);
						$nominalClassID		= '';
						if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]))){
							$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]['account2ClassId'];
						}
						if($nominalClassID){
							$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ClassRef']	= array('value' => $nominalClassID);
						}
						
						if(!empty($defaultItemMapping)){
							if(isset($defaultItemMapping[$channelId][$orderRows['nominalCode']])){
								$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $defaultItemMapping[$channelId][$orderRows['nominalCode']]);
							}
						}
						if(!$LineTaxId){
							unset($InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']);
						}
						$invoiceLineCount++;
					}
				}
			}
			if($totalItemDiscount){
				foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmount){
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'LineNum'							=> $linNumber++,
						'Description' 						=> 'Item Discount',
						'Amount'							=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount)),
						'DetailType'						=> 'ItemBasedExpenseLineDetail',
						'ItemBasedExpenseLineDetail'		=> array(
							'ItemRef' 							=> array( 'value' => $config['discountItem']),
							'Qty' 								=> 1,
							'UnitPrice' 						=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount)),
						),	
					);
					if($TaxID){
						$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
					}
					$invoiceLineCount++;
				}
			}
			if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
				if($discountCouponAmt){
					foreach($discountCouponAmt as $TaxID => $discountCouponLineAmount){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Coupon Discount',
							'Amount'				=> sprintf("%.4f",((-1) * $discountCouponLineAmount)),
							'DetailType'			=> 'ItemBasedExpenseLineDetail',
							'ItemBasedExpenseLineDetail'	=> array('ItemRef' => array('value' => $config['couponItem']),
								'Qty'						=> 1,
								'UnitPrice'					=> sprintf("%.4f",((-1) * $discountCouponLineAmount)),
							),	
						);
						if($TaxID){
							$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
						}
						$invoiceLineCount++;
					}
				}
			}
			if($config['SendTaxAsLineItem']){
				if($BrightpearlTotalTAX > 0){
					$InvoiceLineAdd[$invoiceLineCount] = array(
						'LineNum'				=> $linNumber++,
						'Description'			=> 'Total Tax Amount',
						'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
						'DetailType'			=> 'ItemBasedExpenseLineDetail',
						'ItemBasedExpenseLineDetail'	=> array(
							'ItemRef'						=> array('value' => $config['TaxLineItemCode']),
							'Qty' 							=> 1,
							'UnitPrice' 					=> sprintf("%.4f",$BrightpearlTotalTAX),
							'TaxCodeRef' 					=> array('value' => $config['salesNoTaxCode']),
						),	
					);
					$invoiceLineCount++;
				}
			}
			
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('purchase_credit_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			
			$dueDate	= date('c');
			$taxDate	= date('c');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= $rowDatas['invoices']['0']['dueDate'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
			}

			//taxdate chanages
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$QBOoffset		= 0;
			$diff			= $BPDateOffset - $QBOoffset;
			$date1			= new DateTime($dueDate);
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date1->setTimezone(new DateTimeZone($BPTimeZone));
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
				$date1->modify($diff);
			}
			$taxDate		= $date->format('Y-m-d');
			$dueDate		= $date1->format('Y-m-d');
			
			$invoiceRef		= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			
			if($config['UseAsDocNumberpopc']){
				$account1FieldIds	= explode(".",$config['UseAsDocNumberpopc']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$invoiceRef	= $fieldValueTmps;
				}
				else{
					$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
				}
			}
			if(!$invoiceRef){
				$invoiceRef	= $orderId;
			}
			$invoiceRef		= substr($invoiceRef,0,21);
			
			$lineLevelClassId	= '';
			if((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
				$lineLevelClassId	= $channelMappings[$channelId]['account2ChannelId'];
			}
			if($lineLevelClassId){
				foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
					if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
						continue;
					}
					else{
						$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
					}
				}
			}
			
			$QBOCustomerID	= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
			$request		= array(
				'DocNumber'		=> $invoiceRef,
				'VendorRef'		=> array('value' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId']),
				'TxnDate'		=> $taxDate,
				'Line'			=> $InvoiceLineAdd,
				'ExchangeRate'	=> sprintf("%.4f",(1 / $rowDatas['currency']['exchangeRate'])),
				'CurrencyRef'	=> array('value' => $rowDatas['currency']['orderCurrencyCode']),
			);
			if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$rowDatas['currency']['orderCurrencyCode'],$config['defaultCurrrency'],$taxDate);
				if($exRate){
					$request['ExchangeRate'] = $exRate;
				}
				else{
					$exRate = $exchangeRate[strtolower($rowDatas['currency']['orderCurrencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$request['ExchangeRate'] = $exRate;
					}
					else{
						echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
						unset($request['ExchangeRate']);
					}
				}
			}
			
			if($clientcode == 'biscuiteersqbo'){
				if(!$config['SendTaxAsLineItem']){
					$TxnTaxDetailAdded	= 0;
					if($orderTaxId){
						if($orderTaxAmount > 0){
							$TxnTaxDetailAdded	= 1;
							$request['TxnTaxDetail']	= array(
								'TotalTax'					=> $orderTaxAmount,
								'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
							);
							$request['GlobalTaxCalculation']	= 'TaxExcluded';
						}
					}
				}
			}
			else{
				$TxnTaxDetailAdded	= 0;
				if($orderTaxId){
					if($orderTaxAmount > 0){
						$TxnTaxDetailAdded	= 1;
						$request['TxnTaxDetail']	= array(
							'TotalTax'					=> $orderTaxAmount,
							'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
						);
						$request['GlobalTaxCalculation']	= 'TaxExcluded';
					}
				}
			}
			
			
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				unset($request['TxnTaxDetail']);
			}
			if($this->ci->globalConfig['enableCustomFieldMapping']){
				if($CustomFieldMappings){
					foreach($CustomFieldMappings as $CustomFieldMappingsData){
						$CustomFields		= array();
						$account1FieldIds	= explode(".",$CustomFieldMappingsData['account1CustomFieldPO']);
						$fieldValue			= '';
						$fieldValueTmps		= '';
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.staffOwnerContactId'){
							$CustomstaffOwnerContactId	= $rowDatas['assignment']['current']['staffOwnerContactId'];
							$fieldValueTmps				= $getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.projectId'){
							$CustomprojectId			= $rowDatas['assignment']['current']['projectId'];
							$fieldValueTmps				= $getAllProjectsData[$orderDatas['account1Id']][$CustomprojectId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.channelId'){
							$CustomchannelId			= $rowDatas['assignment']['current']['channelId'];
							$fieldValueTmps				= $getAllChannelMethodData[$orderDatas['account1Id']][$CustomchannelId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.leadSourceId'){
							$CustomleadSourceId			= $rowDatas['assignment']['current']['leadSourceId'];
							$fieldValueTmps				= $getAllLeadsourceData[$orderDatas['account1Id']][$CustomleadSourceId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomFieldPO'] == 'assignment.current.teamId'){
							$CustomteamId				= $rowDatas['assignment']['current']['teamId'];
							$fieldValueTmps				= $getAllTeamData[$orderDatas['account1Id']][$CustomteamId]['name'];
						}
						
						/* if($fieldValueTmps){
							$allQBOCustomFieldIds = explode(",",$CustomFieldMappingsData['account2CustomField']);
							foreach($allQBOCustomFieldIds as $allQBOCustomFieldIdsTmp){
								$CustomFields = array();
								if(is_array($fieldValueTmps)){
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> substr($fieldValueTmps['value'],0,31),
										'Type'			=> 'StringType',
									);
								}
								else{
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> substr($fieldValueTmps,0,31),
										'Type'			=> 'StringType',
									);
								}
								if($CustomFields){
									$request['CustomField'][]	= $CustomFields;
								}
							}
						} */
						if($fieldValueTmps){
							$allQBOCustomFieldIds	= explode(",",trim($CustomFieldMappingsData['account2CustomField']));
							foreach($allQBOCustomFieldIds as $allQBOCustomFieldIdsTmp){
								if(strlen($allQBOCustomFieldIdsTmp) == 1){
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> (is_array($fieldValueTmps)) ? (substr($fieldValueTmps['value'],0,31)) : (substr($fieldValueTmps,0,31)),
										'Type'			=> 'StringType',
									);
									$request['CustomField'][]	= $CustomFields;
								}
								else{
									$arrayNumbers	= count(explode(".",$allQBOCustomFieldIdsTmp));
									if($arrayNumbers == 1){
										$request[$allQBOCustomFieldIdsTmp]	= (is_array($fieldValueTmps)) ? ($fieldValueTmps['value']) : ($fieldValueTmps); 
									}
									else{
										$firstKey					= (explode(".",$allQBOCustomFieldIdsTmp))[0];
										$allQBOCustomFieldIdsTmp	= str_replace($firstKey.'.','',$allQBOCustomFieldIdsTmp);
										$finalSentValue				= json_encode((is_array($fieldValueTmps)) ? ($fieldValueTmps['value']) : ($fieldValueTmps));
										$subClose					= '';
										$arrayNumbers			 	= $arrayNumbers - 1;
										for($i = 1; $i <= $arrayNumbers; $i++){
											$subClose	.= '}';
										}
										$request[$firstKey]			= json_decode(('{"'.str_replace(".",'":{"',$allQBOCustomFieldIdsTmp).'":'.$finalSentValue.$subClose), true);
									}
								}
							}
						}
					}
					if(!$request['CustomField']){
						unset($request['CustomField']);
					}
				}
			}
			
			if($clientcode == 'doubleqbo'){
				unset($request['TxnTaxDetail']);
			}
			
			$url		= 'vendorcredit?minorversion=37';
			$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData['Request data	: ']	= $request;
			$createdRowData['Response data	: ']	= $results;
			
			$this->ci->db->update('purchase_credit_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if($results['VendorCredit']['Id']){
				$this->ci->db->update('purchase_credit_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['VendorCredit']['Id'],'message' => '', 'PostedTime' => date('c'), 'qboContactID' => $QBOCustomerID),array('id' => $orderDatas['id']));
				$QBOTotalAmount			= $results['VendorCredit']['TotalAmt'];
				$NetRoundOff			= $BrightpearlTotalAmount - $QBOTotalAmount;
				$RoundOffCheck			= abs($NetRoundOff);
				$RoundOffApplicable		= 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.99){
						$RoundOffApplicable	= 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'LineNum'							=> $linNumber++,
							'Description'						=> 'RoundOffItem',
							'Amount'							=> sprintf("%.4f",$NetRoundOff),
							'DetailType'						=> 'ItemBasedExpenseLineDetail',
							'ItemBasedExpenseLineDetail'		=> array(
								'ItemRef'							=> array(
									'value'								=> $config['roundOffItem']
								),
								'Qty'							=> 1,
								'UnitPrice'						=> sprintf("%.4f",$NetRoundOff),
								'TaxCodeRef'					=> array(
									'value'							=> $config['salesNoTaxCode'] 
								),
							),	
						);
						$request['Line']		= $InvoiceLineAdd;
						$request['SyncToken']	= 0;
						$request['Id']			= $results['VendorCredit']['Id'];
						$url					= 'vendorcredit?minorversion=37';  
						$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Rounding Request data	: ']	= $request;
						$createdRowData['Rounding Response data	: ']	= $results2;
						$this->ci->db->update('purchase_credit_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						
						if($results2['VendorCredit']['Id']){
							$this->ci->db->update('purchase_credit_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results2['VendorCredit']['Id'],'message' => ''),array('id' => $orderDatas['id']));
						}
					}
				}
			}
		}
	}
}
?>