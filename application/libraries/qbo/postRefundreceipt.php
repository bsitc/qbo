<?php
$this->reInitialize();
$exchangeRates				= $this->getExchangeRate();
$enableCustomFieldMappings	= $this->ci->globalConfig['enableCustomFieldMapping'];
$enableRefundReceiptConsol	= $this->ci->globalConfig['enableRefundReceiptConsol'];
$UnInvoicingEnabled			= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config			= $this->accountConfig[$account2Id];
	$exchangeRate	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('refund_receipt',array('account2Id' => $account2Id))->result_array();
	if(empty($datas)){continue;}
	
	$this->ci->db->reset_query();
	$genericcustomerMappings			= array();
	$GenericCustomerCurrencyMappings	= array();
	$allCustomFieldsForGenericMapping	= array();
	$genericCustomerMappingAdvanced		= 0;
	$genericcustomerMappingsTemps		= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	if(!empty($genericcustomerMappingsTemps)){
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$mappedCurrency		= ($genericcustomerMappingsTemp['account1CurrencylId']) ? (strtolower($genericcustomerMappingsTemp['account1CurrencylId'])) : '';
			$mappedChannel		= ($genericcustomerMappingsTemp['account1ChannelId']) ? (strtolower($genericcustomerMappingsTemp['account1ChannelId'])) : '';
			
			if($this->ci->globalConfig['enableGenericCustomerMappingCustomazation']){
				$genericCustomerMappingAdvanced	= 1;
				$brightpearlCustomFieldName		= trim(strtolower($genericcustomerMappingsTemp['brightpearlCustomFieldName']));
				$includedStringInCustomField	= explode("||",trim($genericcustomerMappingsTemp['IncludedStringInCustomField']));
				$allCustomFieldsForGenericMapping[$brightpearlCustomFieldName]	= $brightpearlCustomFieldName; 
				if($mappedCurrency){
					if((is_array($includedStringInCustomField)) AND (!empty($includedStringInCustomField)) AND ($brightpearlCustomFieldName)){
						foreach($includedStringInCustomField as $includedStringInCustomFieldss){
							$includedDataString	= trim(strtolower($includedStringInCustomFieldss));
							$GenericCustomerCurrencyMappings[$mappedChannel][$mappedCurrency][$brightpearlCustomFieldName][$includedDataString]	= $genericcustomerMappingsTemp;
						}
					}
					else{
						$GenericCustomerCurrencyMappings[$mappedChannel][$mappedCurrency]['Blank']	= $genericcustomerMappingsTemp;
					}
				}
				else{
					if((is_array($includedStringInCustomField)) AND (!empty($includedStringInCustomField)) AND ($brightpearlCustomFieldName)){
						foreach($includedStringInCustomField as $includedStringInCustomFieldss){
							$includedDataString	= trim(strtolower($includedStringInCustomFieldss));
							$genericcustomerMappings[$mappedChannel][$brightpearlCustomFieldName][$includedDataString]	= $genericcustomerMappingsTemp;
						}
					}
					else{
						$genericcustomerMappings[$mappedChannel]['Blank']	= $genericcustomerMappingsTemp;
					}
				}
			}
			else{
				if($mappedCurrency){
					$GenericCustomerCurrencyMappings[$mappedChannel][$mappedCurrency]	= $genericcustomerMappingsTemp;
				}
				else{
					$genericcustomerMappings[$mappedChannel]	= $genericcustomerMappingsTemp;
				}
			}
			
		}
	}
	
	$this->ci->db->reset_query();
	$AggregationMappings				= array();
	$AggregationMappingsTemps			= $this->ci->db->get_where('mapping_receiptConsol',array('account2Id' => $account2Id))->result_array();
	if(!empty($AggregationMappingsTemps)){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$AggregationMappings[$AggregationMappingsTemp['account1ChannelId']][strtolower($AggregationMappingsTemp['account1CurrencyId'])][$AggregationMappingsTemp['account1PaymentId']]	= $AggregationMappingsTemp;
			
		}
	}
	
	$this->ci->db->reset_query();
	$paymentMappings		= array();
	$paymentMappings1		= array();
	$paymentMappings2		= array();
	$paymentMappings3		= array();
	$GlobalPaymnetMehod		= '';
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	if(!empty($paymentMappingsTemps)){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			if(!$GlobalPaymnetMehod){
				$GlobalPaymnetMehod	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			}
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}
			elseif((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$channelMappings		= array();
	if($this->ci->globalConfig['enableChannelMapping']){
		$this->ci->db->reset_query();
		$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
		if($channelMappingsTemps){
			foreach($channelMappingsTemps as $channelMappingsTemp){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$ChannelLocationMappings		= array();
	if($this->ci->globalConfig['enableChannelLocationMapping']){
		$this->ci->db->reset_query();
		$ChannelLocationMappingsTemps	= $this->ci->db->get_where('mapping_channelLocation',array('account2Id' => $account2Id))->result_array();
		if($ChannelLocationMappingsTemps){
			foreach($ChannelLocationMappingsTemps as $ChannelLocationMappingsTemp){
				$ChannelLocationMappings[$ChannelLocationMappingsTemp['account1LocationId']][$ChannelLocationMappingsTemp['account1ChannelId']]	= $ChannelLocationMappingsTemp;
			}
		}
	}
	
	$CustomFieldMappings		= array();
	$getAllSalesrepData			= array();
	$getAllProjectsData			= array();
	$getAllChannelMethodData	= array();
	$getAllLeadsourceData		= array();
	$getAllTeamData				= array();
	if($this->ci->globalConfig['enableCustomFieldMapping']){
		$this->ci->db->reset_query();
		$isAssignmentMapped						= 0;
		$isAssignmentstaffOwnerContactIdMapped	= 0;
		$isAssignmentprojectIdMapped			= 0;
		$isAssignmentchannelIdMapped			= 0;
		$isAssignmentleadSourceIdMapped			= 0;
		$isAssignmentteamIdMapped				= 0;
		$CustomFieldMappingsTemps	= $this->ci->db->get_where('mapping_customfield',array('type' => 'sales', 'account2Id' => $account2Id))->result_array();
		if($CustomFieldMappingsTemps){
			foreach($CustomFieldMappingsTemps as $CustomFieldMappingsTemp){
				$CustomFieldMappings[$CustomFieldMappingsTemp['account1CustomField']]	= $CustomFieldMappingsTemp;
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'assignment')){
					$isAssignmentMapped	= 1;
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'staffownercontactid')){
						$isAssignmentstaffOwnerContactIdMapped	= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'projectid')){
						$isAssignmentprojectIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'channelid')){
						$isAssignmentchannelIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'leadsourceid')){
						$isAssignmentleadSourceIdMapped			= 1;
					}
					if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'teamid')){
						$isAssignmentteamIdMapped				= 1;
					}
				}
			}
		}
		if($CustomFieldMappings AND $isAssignmentMapped){
			if($isAssignmentstaffOwnerContactIdMapped){
				$getAllSalesrepData			= $this->ci->brightpearl->getAllSalesrep();
			}
			if($isAssignmentprojectIdMapped){
				$getAllProjectsData			= $this->ci->brightpearl->getAllProjects();
			}
			if($isAssignmentchannelIdMapped){
				$getAllChannelMethodData	= $this->ci->brightpearl->getAllChannelMethod();
			}
			if($isAssignmentleadSourceIdMapped){
				$getAllLeadsourceData		= $this->ci->brightpearl->getAllLeadsource();
			}
			if($isAssignmentteamIdMapped){
				$getAllTeamData				= $this->ci->brightpearl->getAllTeam();
			}
		}
	}
	
	if($this->ci->globalConfig['enablePaymenttermsMapping']){
		$this->ci->db->reset_query();
		$paymentTermsMappingsTemps	= $this->ci->db->get_where('mapping_paymentterms',array('type' => 'sales','account2Id' => $account2Id))->result_array();
		$paymentTermsMappings	= array();
		if($paymentTermsMappingsTemps){
			foreach($paymentTermsMappingsTemps as $paymentTermsMappingsTemp){
				if($paymentTermsMappingsTemp['account1SalesTermId'] != 'NA'){
					$paymentTermsMappings[$paymentTermsMappingsTemp['account1SalesTermId']]	= $paymentTermsMappingsTemp;
				}
			}
		}
	}
	
	if(!empty($datas)){
		$allPostContactIds	= array();
		foreach($datas as $datasTemp){
			$tempRowData		= json_decode($datasTemp['rowData'],true);
			$channelId			= $tempRowData['assignment']['current']['channelId'];
			$orderCurrencyCode	= strtolower($tempRowData['currency']['orderCurrencyCode']);
			if($channelId){
				if(isset($genericcustomerMappings[$channelId])){continue;}
				elseif(isset($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode])){continue;}
				elseif(isset($AggregationMappings[$channelId][$orderCurrencyCode])){continue;}
				else{
					$allPostContactIds[]	= $datasTemp['customerId'];
				}
			}
			else{
				$allPostContactIds[]	= $datasTemp['customerId'];
			}
		}
		$allPostContactIds = array_filter($allPostContactIds);
		$allPostContactIds = array_unique($allPostContactIds);
		if(!empty($allPostContactIds)){
			$this->postCustomers($allPostContactIds,$account2Id);
		}
	}
	
	if(!$config['disableSkuDetails']){
		if(!empty($datas)){
			$allPostItmeIds	= array();
			foreach($datas as $datasTemp){
				$rowDatas			= json_decode($datasTemp['rowData'], true);
				$channelId			= $rowDatas['assignment']['current']['channelId'];
				$orderCurrencyCode	= strtolower($rowDatas['currency']['orderCurrencyCode']);
				
				if(isset($AggregationMappings[$channelId][$orderCurrencyCode])){continue;}
				else{
					foreach($rowDatas['orderRows'] as $orderRowsTemp){
						if($orderRowsTemp['productId'] > 1001){
							$allPostItmeIds[]	= $orderRowsTemp['productId'];
						}
					}
				}
			}
			$allPostItmeIds = array_filter($allPostItmeIds);
			$allPostItmeIds = array_unique($allPostItmeIds);
			if(!empty($allPostItmeIds)){
				$this->postProducts($allPostItmeIds,$account2Id);
			}
		}
	}
	
	$this->ci->db->reset_query();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('createdCustomerId <>' => NULL, 'account2Id' => $account2Id))->result_array();
	$customerMappings		= array();
	if($customerMappingsTemps){
		foreach($customerMappingsTemps as $customerMappingsTemp){
			if($customerMappingsTemp['createdCustomerId']){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	if(!$config['disableSkuDetails']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id,'createdProductId <>' => NULL, ))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				if($productMappingsTemp['createdProductId']){
					$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
				}
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			}
		}
	}
	
	if($this->ci->globalConfig['enablePaymenttermsMapping']){
		$this->ci->db->reset_query();
		$paymentTermsMappingsTemps	= $this->ci->db->get_where('mapping_paymentterms',array('type' => 'sales','account2Id' => $account2Id))->result_array();
		$paymentTermsMappings		= array();
		if($paymentTermsMappingsTemps){
			foreach($paymentTermsMappingsTemps as $paymentTermsMappingsTemp){
				if($paymentTermsMappingsTemp['account1SalesTermId'] != 'NA'){
					$paymentTermsMappings[$paymentTermsMappingsTemp['account1SalesTermId']]	= $paymentTermsMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	if($datas){
		$journalIds		= array();
		foreach($datas as $orderDatas){
			if($orderDatas['createOrderId']){continue;}
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'qbo'){
						if($paymentKey){
							$journalIds[]	= $paymentDetail['journalId'];
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['paymentDetails']){continue;}
			
			$orderId			= $orderDatas['orderId'];
			$bpconfig			= $this->ci->account1Config[$orderDatas['account1Id']];
			$giftCardPayment	= $bpconfig['giftCardPayment'];
			$rowDatas			= json_decode($orderDatas['rowData'],true);
			$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
			$channelId			= $rowDatas['assignment']['current']['channelId'];
			$orderCurrencyCode	= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$dueDate			= date('c');
			$taxDate			= date('c');
			if(!$channelId){
				$channelId	= 'blank';
			}
			if(strtolower($rowDatas['orderPaymentStatus']) != 'paid'){continue;}
			if(!$rowDatas['invoices']['0']['invoiceReference']){continue;}
			if($orderDatas['createOrderId']){continue;}
			
			$currentPaidAmount	= 0;
			foreach($paymentDetails as $key => $paymentDetail){
				if($paymentDetail['amount'] != 0){
					$currentPaidAmount += $paymentDetail['amount'];
				}
			}
			
			if(sprintf("%.4f",$currentPaidAmount) != sprintf("%.4f",$rowDatas['totalValue']['total'])){
				$this->ci->db->update('refund_receipt',array('message' => 'Order is not paid.'),array('orderId' => $orderId));
				continue;
			}
			
			if($rowDatas['invoices']['0']['dueDate']){
				$dueDate		= $rowDatas['invoices']['0']['dueDate'];
			}
			if($rowDatas['invoices']['0']['taxDate']){
				$taxDate		= $rowDatas['invoices']['0']['taxDate'];
			}
			
			$DepositToAccountRef	= '';
			$PaymentMethodRef		= '';
			$paymentMethod			= '';
			$paymentCurrency		= '';
			$CurrencyRate			= $rowDatas['currency']['exchangeRate'];
			$paymentDate			= $taxDate;
			foreach($paymentDetails as $key => $paymentDetail){
				if($paymentDetail['paymentType'] == 'PAYMENT'){
					if($paymentDetail['amount'] > 0){
						if($paymentDetail['paymentMethod'] != $giftCardPayment){
							$paymentMethod		= $paymentDetail['paymentMethod'];
							$paymentCurrency	= $paymentDetail['currency'];
							$paymentDate		= $paymentDetail['paymentDate'];
							if(isset($journalDatas[$paymentDetail['journalId']])){
								$CurrencyRate	= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
							}
							break;
						}
					}
				}
			}
			
			if(!$paymentMethod){
				$totalGiftPaymnet	= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if($paymentDetail['paymentMethod'] == $giftCardPayment){
						$totalGiftPaymnet	+= $paymentDetail['amount'];
						$paymentCurrency	= $paymentDetail['currency'];
					}
				}
				if($totalGiftPaymnet >= $rowDatas['totalValue']['total']){
					$paymentMethod	= $GlobalPaymnetMehod;
					if(!$paymentMethod){
						$this->ci->db->update('refund_receipt',array('message' => 'Payment Method Not Found'),array('orderId' => $orderId));
						continue;
					}
				}
				else{
					$this->ci->db->update('refund_receipt',array('message' => 'Payment Method Not Found'),array('orderId' => $orderId));
					continue;
				}
			}
			if(!$paymentMethod){
				$this->ci->db->update('refund_receipt',array('message' => 'Payment Method Not Found'),array('orderId' => $orderId));
				continue;
			}
			else{
				if(isset($paymentMappings1[$channelId][strtolower($paymentCurrency)][strtolower($paymentMethod)])){
					$DepositToAccountRef	= $paymentMappings1[$channelId][strtolower($paymentCurrency)][strtolower($paymentMethod)]['account2PaymentId'];
					$PaymentMethodRef	= $paymentMappings1[$channelId][strtolower($paymentCurrency)][strtolower($paymentMethod)]['paymentValue'];
				}
				else if(isset($paymentMappings2[$channelId][strtolower($paymentMethod)])){
					$DepositToAccountRef	= $paymentMappings2[$channelId][strtolower($paymentMethod)]['account2PaymentId'];
					$PaymentMethodRef	= $paymentMappings2[$channelId][strtolower($paymentMethod)]['paymentValue'];
				}
				else if(isset($paymentMappings3[strtolower($paymentCurrency)][strtolower($paymentMethod)])){
					$DepositToAccountRef	= $paymentMappings3[strtolower($paymentCurrency)][strtolower($paymentMethod)]['account2PaymentId'];
					$PaymentMethodRef	= $paymentMappings3[strtolower($paymentCurrency)][strtolower($paymentMethod)]['paymentValue'];
				}
				else if(isset($paymentMappings[strtolower($paymentMethod)])){
					$DepositToAccountRef	= $paymentMappings[strtolower($paymentMethod)]['account2PaymentId'];
					$PaymentMethodRef	= $paymentMappings[strtolower($paymentMethod)]['paymentValue'];
				}
				else{
					$this->ci->db->update('refund_receipt',array('message' => 'Payment Mapping is Missing'),array('orderId' => $orderId));
					continue;
				}
			}
			
			if($enableRefundReceiptConsol){
				if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)][$paymentMethod]){
					continue;
				}
			}
			
			//UNINVOICING CODE STARTS//
			if($UnInvoicingEnabled){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						$TempAcc2ID		= '';
						if($orderDatas['TempAcc2ID']){
							$TempAcc2ID	= $orderDatas['TempAcc2ID'];
						}
						else{
							$TempAcc2ID	= $account2Id;
						}
						$createdRowData		= json_decode($orderDatas['createdRowData'],true);
						$uninvoiceCount		= $orderDatas['uninvoiceCount'];
						$SyncToken			= '';
						$QBOorderId			= $orderDatas['createOrderId'];			
						$readurl			= 'refundreceipt/'.$QBOorderId.'?minorversion=41';
						$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
						if($InvoiceResponse['RefundReceipt']){
							$SyncToken	= $InvoiceResponse['RefundReceipt']['SyncToken'];
						}
						$request		= array(
							"SyncToken"		=>	$SyncToken,
							"Id"			=>	$QBOorderId,
						);
						$url				= 'refundreceipt?operation=delete&minorversion=41';
						$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $TempAcc2ID)[$TempAcc2ID];
						$createdRowData['Void Request data	: ']	= $request;
						$createdRowData['Void Response data	: ']	= $results;
						$this->ci->db->update('refund_receipt',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
						if($results['RefundReceipt']['status'] == 'Deleted'){
							$uninvoiceCount++;
							$this->ci->db->update('refund_receipt',array('paymentDetails' => '', 'TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount),array('orderId' => $orderId));
							continue;
						}
						else{
							$this->ci->db->update('refund_receipt',array('message' => 'Unable to Delete the RefundReceipt On QBO'),array('orderId' => $orderId));
							continue;
						}
						continue;
					}
				}
			}
			//UNINVOICING ENDS
			
			
			
			if(!$orderDatas['invoiced']){continue;}
			
			
			$QBOCustomerID			= '';
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$billAddress			= $rowDatas['parties']['billing'];
			$shipAddress 	        = $rowDatas['parties']['delivery'];
			$orderCustomer 	        = $rowDatas['parties']['customer'];
			$BrightpearlTotalAmount	= $rowDatas['totalValue']['total'];
			$BrightpearlTotalTAX	= $rowDatas['totalValue']['taxAmount'];
			$QBOCustomerID			= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
			$CustomLocationID		= $rowDatas['customFields'][$bpconfig['SOLocationCustomField']]['id'];
			$genericcustomerMapping	= array();
			
			if($genericCustomerMappingAdvanced){
				foreach($rowDatas['customFields'] as $fieldNameKey => $AllCustFieldDataTemp){
					$fieldNameKey	= strtolower($fieldNameKey);
					if(in_array($fieldNameKey,$allCustomFieldsForGenericMapping)){
						if($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode][$fieldNameKey]){
							foreach($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode][$fieldNameKey] as $IncludedString => $GenericMappingTemp){
								if(is_array($AllCustFieldDataTemp)){
									if(substr_count(strtolower($AllCustFieldDataTemp['value']),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
								else{
									if(substr_count(strtolower($AllCustFieldDataTemp),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
							}
						}
						elseif($genericcustomerMappings[$channelId][$fieldNameKey]){
							foreach($genericcustomerMappings[$channelId][$fieldNameKey] as $IncludedString => $GenericMappingTemp){
								if(is_array($AllCustFieldDataTemp)){
									if(substr_count(strtolower($AllCustFieldDataTemp['value']),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
								else{
									if(substr_count(strtolower($AllCustFieldDataTemp),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
							}
						}
					}
				}
				if(!$genericcustomerMapping){
					if($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode]['Blank']){
						$genericcustomerMapping	= $GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode]['Blank'];
					}
					elseif($genericcustomerMappings[$channelId]['Blank']){
						$genericcustomerMapping	= $genericcustomerMappings[$channelId]['Blank'];
					}
				}
			}
			else{
				if($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode]){
					$genericcustomerMapping	= $GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode];
				}
				elseif($genericcustomerMappings[$channelId]){
					$genericcustomerMapping	= $genericcustomerMappings[$channelId];
				}
			}
			
			if($genericcustomerMapping){
				if($genericcustomerMapping['account2ChannelId']){
					$QBOCustomerID	= $genericcustomerMapping['account2ChannelId'];
				}
			}
			if(!$QBOCustomerID){
				$this->ci->db->update('refund_receipt',array('message' => 'Customer not sent yet.'),array('orderId' => $orderId));
				continue;
			}
			
			$discountCouponAmt				= array();
			$totalItemDiscount				= array();
			$missingSkus					= array();
			$InvoiceLineAdd					= array();
			$invoiceLineCount				= 0;
			$isDiscountCouponAdded			= 0;
			$isDiscountCouponTax			= 0;
			$itemDiscountTax				= 0;
			$orderTaxAmount					= 0;
			$taxAmount						= 0;
			$linNumber						= 1;
			$orderTaxId						= '';
			$couponItemLineID				= '';
			$shippingLineID					= '';
			$countryIsoCode					= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState					= strtolower(trim($shipAddress['addressLine4']));
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$productId	= $orderRows['productId'];
				if($productId <= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
				}
			}
			
			$itemtaxAbleLine		= $config['orderLineTaxCode'];
			$discountTaxAbleLine	= $config['orderLineTaxCode'];
			$LineItemTax			= array();
			
			if($config['disableSkuDetails']){
				$ProductArray		= array();
				foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
					if($rowId == $couponItemLineID){
						if($rowdatass['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					
					$bpNominal			= $rowdatass['nominalCode'];
					$bpTaxID			= $rowdatass['rowValue']['taxClassId'];
					$rowNet				= $rowdatass['rowValue']['rowNet']['value'];
					$rowTax				= $rowdatass['rowValue']['rowTax']['value'];
					$discountCouponAmt	= 0;
					$discountAmt		= 0;
					$productId			= $rowdatass['productId'];
					
					$bundleParentID	= '';
					$isBundleChild	= $rowdatass['composition']['bundleChild'];
					if($isBundleChild){
						$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
					}
					if($isBundleChild AND $bundleParentID){
						$bpTaxID		= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
					}
					
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$isTrackedItem		= 0;
					if($productMappings[$productId]){
						$productParams	= json_decode($productMappings[$productId]['params'], true);
						$isTrackedItem	= $productParams['stock']['stockTracked'];
					}
					else{
						if($productId > 1001){
							$missingSkus[]	= $rowdatass['productSku'];
							continue;
						}
					}
					
					$taxMappingKey	= $bpTaxID;
					$taxMappingKey1	= $bpTaxID;
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode;
						}
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					$taxMapping	= array();
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$bpTaxID.'--'.$channelId])){
						$taxMapping	= $taxMappings[$bpTaxID.'--'.$channelId];
					}
					$LineTaxId  		= $taxMapping['account2LineTaxId'];
					
					if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'])){
						$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
					}
					
					if(!$config['SendTaxAsLineItem']){
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$bpNominal][$LineTaxId])){
									$ProductArray['discount'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								if($isBundleChild){
									
								}
								else{
									$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
									if(!$originalPrice){
										$originalPrice	= $rowNet;
									}
									if($originalPrice > $rowNet){
										$discountCouponAmt	= $originalPrice - $rowNet;
										$rowNet				= $originalPrice;
										if($discountCouponAmt > 0){
											if(isset($ProductArray['discountCoupon'][$bpNominal][$LineTaxId])){
												$ProductArray['discountCoupon'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $discountCouponAmt;
											}
											else{
												$ProductArray['discountCoupon'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $discountCouponAmt;
											}
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$channelId][$rowdatass['nominalCode']]))){
								$customItemIdQbo	= $defaultItemMapping[$channelId][$rowdatass['nominalCode']];
								if(isset($ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo])){
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['bpTaxTotal']	+= $rowTax;
								}
								else{
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['bpTaxTotal']	= $rowTax;
								}
							}
							else{
								if($isTrackedItem){
									if(isset($ProductArray['aggregationItem2'][$bpNominal][$LineTaxId])){
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $rowNet;
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
									}
									else{
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $rowNet;
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
									}
								}
								else{
									if(isset($ProductArray['aggregationItem'][$bpNominal][$LineTaxId])){
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $rowNet;
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['bpTaxTotal']	+= $rowTax;
									}
									else{
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $rowNet;
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['bpTaxTotal']	= $rowTax;
									}
								}
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$bpNominal][$LineTaxId])){
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$bpNominal][$LineTaxId])){
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$bpNominal][$LineTaxId])){
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$bpNominal][$LineTaxId])){
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
					}
					else{
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$bpNominal])){
									$ProductArray['discount'][$bpNominal]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$bpNominal]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice	= $rowNet;
								}
								if($originalPrice > $rowNet){
									$discountCouponAmt	= $originalPrice - $rowNet;
									$rowNet				= $originalPrice;
									if($discountCouponAmt > 0){
										if(isset($ProductArray['discountCoupon'][$bpNominal])){
											$ProductArray['discountCoupon'][$bpNominal]['TotalNetAmt']	+= $discountCouponAmt;
										}
										else{
											$ProductArray['discountCoupon'][$bpNominal]['TotalNetAmt']	= $discountCouponAmt;
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$channelId][$rowdatass['nominalCode']]))){
								$customItemIdQbo	= $defaultItemMapping[$channelId][$rowdatass['nominalCode']];
								if(isset($ProductArray['customItems'][$bpNominal][$customItemIdQbo])){
									$ProductArray['customItems'][$bpNominal][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
								}
								else{
									$ProductArray['customItems'][$bpNominal][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
								}
								if(isset($ProductArray['allTax'])){
									$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
								}
								else{
									$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
								}
							}
							else{
								if($isTrackedItem){
									if(isset($ProductArray['aggregationItem2'][$bpNominal])){
										$ProductArray['aggregationItem2'][$bpNominal]['TotalNetAmt']	+= $rowNet;
									}
									else{
										$ProductArray['aggregationItem2'][$bpNominal]['TotalNetAmt']	= $rowNet;
									}
									if(isset($ProductArray['allTax'])){
										$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
									}
									else{
										$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
									}
								}
								else{
									if(isset($ProductArray['aggregationItem'][$bpNominal])){
										$ProductArray['aggregationItem'][$bpNominal]['TotalNetAmt']	+= $rowNet;
									}
									else{
										$ProductArray['aggregationItem'][$bpNominal]['TotalNetAmt']	= $rowNet;
									}
									if(isset($ProductArray['allTax'])){
										$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
									}
									else{
										$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
									}
								}
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$bpNominal])){
								$ProductArray['landedCost'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['landedCost'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$bpNominal])){
								$ProductArray['shipping'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['shipping'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$bpNominal])){
								$ProductArray['giftcard'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['giftcard'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$bpNominal])){
								$ProductArray['couponitem'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['couponitem'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
					}
				}
				
				if($ProductArray){
					if(!$config['SendTaxAsLineItem']){
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Expense Item',
											'Amount'				=> sprintf("%.4f",$datas['TotalNetAmt']),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $config['genericSku']),
												'Qty'					=> 1,
												'UnitPrice'				=> sprintf("%.4f",$datas['TotalNetAmt']),
												'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
												'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
										if($keyproduct == 'landedCost'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['landedCostItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Landed Cost';
										}
										if($keyproduct == 'shipping'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Shipping';
										}
										if($keyproduct == 'giftcard'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'GiftCard';
										}
										if($keyproduct == 'couponitem'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon';
										}
										if($keyproduct == 'discount'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['discountItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Item Discount';
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
										}
										if($keyproduct == 'discountCoupon'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon Discount';
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
										}
										$invoiceLineCount++;
									}
								}
							}
							elseif($keyproduct == 'customItems'){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $ProductArrayDatass){
										foreach($ProductArrayDatass as $customItemId	=> $customItemIdTemp){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'LineNum'				=> $linNumber++,
												'Description'			=> 'Special Item',
												'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												'DetailType'			=> 'SalesItemLineDetail',
												'SalesItemLineDetail'	=> array(
													'ItemRef'				=> array('value' => $customItemId),
													'Qty'					=> 1,
													'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
													'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
													'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
											$invoiceLineCount++;
										}
									}
								}
							}
							else{
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Expense Item',
											'Amount'				=> sprintf("%.4f",$datas['TotalNetAmt']),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $config['genericSku']),
												'Qty'					=> 1,
												'UnitPrice'				=> sprintf("%.4f",$datas['TotalNetAmt']),
												'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
												'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
										if($keyproduct == 'aggregationItem2'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Tracked Inventory';
										}
										$invoiceLineCount++;
									}
								}
							}
						}
					}
					else{
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'aggregationItem2') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'LineNum'				=> $linNumber++,
										'Description'			=> 'Expense Item',
										'Amount'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
										'DetailType'			=> 'SalesItemLineDetail',
										'SalesItemLineDetail'	=> array(
											'ItemRef'				=> array('value' => $config['genericSku']),
											'Qty'					=> 1,
											'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
											'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
											'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
										),
									);
									if($nominalClassID){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
									}
									if($keyproduct == 'aggregationItem2'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Tracked Inventory';
									}
									if($keyproduct == 'landedCost'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['landedCostItem']);
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= $config['landedCostItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Landed Cost';
									}
									if($keyproduct == 'shipping'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Shipping';
									}
									if($keyproduct == 'giftcard'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'GiftCard';
									}
									if($keyproduct == 'couponitem'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon';
									}
									if($keyproduct == 'discount'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['discountItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Item Discount'.'-'.$IncomeAccountRef;
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
									}
									if($keyproduct == 'discountCoupon'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon Discount';
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
									}
									$invoiceLineCount++;
								}
							}
							elseif($keyproduct == 'customItems'){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $customItemId	=> $customItemIdTemp){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Special Item',
											'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $customItemId),
												'Qty'					=> 1,
												'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
												'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
										$invoiceLineCount++;
									}
								}
							}
							elseif($keyproduct == 'allTax'){
								if($ProductArrayDatasTemp['TotalNetAmt'] <= 0){
									continue;
								}
								$InvoiceLineAdd[$invoiceLineCount] = array(
									'LineNum'				=> $linNumber++,
									'Description'			=> 'Total Tax Amount',
									'Amount'				=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
									'DetailType'			=> 'SalesItemLineDetail',
									'SalesItemLineDetail'	=> array(
										'ItemRef'				=> array('value' => $config['TaxLineItemCode']),
										'Qty' 					=> 1,
										'UnitPrice' 			=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
										'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
									),	
								);
								$invoiceLineCount++;
							}
						}
					}
				}
			}
			else{
				ksort($rowDatas['orderRows']);
				foreach($rowDatas['orderRows'] as $rowId => $orderRows){
					if($rowId == $couponItemLineID){
						if($orderRows['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					
					$bundleParentID	= '';
					$isBundleChild	= $orderRows['composition']['bundleChild'];
					if($isBundleChild){
						$bundleParentID	= $orderRows['composition']['parentOrderRowId'];
					}
					
					$taxMapping			= array();
					$ItemRefValue		= '';
					$ItemRefName		= '';
					$LineTaxId			= '';
					$isDDPOrder			= '';
					$orderTaxId			= '';
					$productId			= $orderRows['productId'];
					$taxClassId			= $orderRows['rowValue']['taxClassId'];
					if($isBundleChild AND $bundleParentID){
						$taxClassId		= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
					}
					
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$taxMappingKey	= $taxClassId;
					$taxMappingKey1	= $taxClassId;
					
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $taxClassId.'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $taxClassId.'-'.$countryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $taxClassId.'-'.$countryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $taxClassId.'-'.$countryIsoCode;
						}
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					$taxMapping	= array();
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$taxClassId.'--'.$channelId])){
						$taxMapping	= $taxMappings[$taxClassId.'--'.$channelId];
					}
					
					$LineTaxId  = $taxMapping['account2LineTaxId'];
					$orderTaxId = $taxMapping['account2TaxId'];
					if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'] OR $taxMapping['account2KidsTaxId'])){
						$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
						$orderTaxId = $taxMapping['account2KidsTaxId'];
					}
					
					if($productId > 1001){
						if(!$productMappings[$productId]['createdProductId']){
							$missingSkus[]	= $orderRows['productSku'];
							continue;
						}
						$ItemRefValue	= $productMappings[$productId]['createdProductId'];
						$ItemRefName	= $productMappings[$productId]['sku'];
					}
					else{
						if($orderRows['rowValue']['rowNet']['value'] > 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else if($orderRows['rowValue']['rowNet']['value'] < 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else{
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
					}
					
					$itemtaxAbleLine 	= $LineTaxId;
					$price				= $orderRows['rowValue']['rowNet']['value'];
					$orderTaxAmount		+= $orderRows['rowValue']['rowTax']['value'];
					$originalPrice		= $price;
					$discountPercentage	=  0;
					
					if(($orderRows['discountPercentage'] > 0) && (!$config['sendNetPriceExcludeDiscount'])){
						$discountPercentage	= 100 - $orderRows['discountPercentage'];
						if($discountPercentage == 0){
							$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
						}
						else{
							$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
						}
						$tempTaxAmt	= $originalPrice - $price;
						if($tempTaxAmt > 0){
							if(isset($totalItemDiscount[$LineTaxId])){
								$totalItemDiscount[$LineTaxId]	+= $tempTaxAmt;
							}
							else{
								$totalItemDiscount[$LineTaxId]	= $tempTaxAmt;
							}
						}
					}
					else if($isDiscountCouponAdded){
						if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							if($isBundleChild){
								//
							}
							else{
								$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
								}
								if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
									$discountCouponAmtTemp	= ($originalPrice - $price);
									if($discountCouponAmtTemp > 0){
										if(isset($discountCouponAmt[$LineTaxId])){
											$discountCouponAmt[$LineTaxId]	+= $discountCouponAmtTemp;
										}
										else{
											$discountCouponAmt[$LineTaxId]	= $discountCouponAmtTemp;
										}
									}
								}
								else{
									$originalPrice	= $orderRows['rowValue']['rowNet']['value'];
								}
							}
						}
					}
					$IncomeAccountRef	= $config['IncomeAccountRef'];
					
					if((isset($nominalMappings[$orderRows['nominalCode']])) AND ($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
						$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']])) AND ($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'])){
						$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
					}
					
					$UnitAmount	= 0.00;
					if($originalPrice != 0){
						$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'LineNum'				=> $linNumber++,
						'Description'			=> $orderRows['productName'],
						'Amount'				=> sprintf("%.4f",($UnitAmount*($orderRows['quantity']['magnitude']))),
						'DetailType'			=> 'SalesItemLineDetail',
						'SalesItemLineDetail'	=> array(
							'ItemRef'				=> array('value' => $ItemRefValue),
							'Qty'					=> $orderRows['quantity']['magnitude'],
							'UnitPrice'				=> sprintf("%.4f",($UnitAmount)),
							'TaxCodeRef'			=> array('value' => $LineTaxId),
							'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
						),
					);
					$nominalClassID		= '';
					if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]))){
						$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]['account2ClassId'];
					}
					if($nominalClassID){
						$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']		= array('value' => $nominalClassID);
					}
					if(!empty($defaultItemMapping)){
						if(isset($defaultItemMapping[$channelId][$orderRows['nominalCode']])){
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $defaultItemMapping[$channelId][$orderRows['nominalCode']]);
						}
					}
					
					if($LineTaxId){
						if(isset($LineItemTax[$LineTaxId])){
							$LineItemTax[$LineTaxId]['tax']	+= $orderRows['rowValue']['rowTax']['value'];
							$LineItemTax[$LineTaxId]['net']	+= $orderRows['rowValue']['rowNet']['value'];
						}
						else{
							$LineItemTax[$LineTaxId]['tax']	= $orderRows['rowValue']['rowTax']['value'];
							$LineItemTax[$LineTaxId]['net']	= $orderRows['rowValue']['rowNet']['value'];
						}
					}
					else{
						if($orderRows['rowValue']['rowTax']['value'] > 0){
							if(isset($LineItemTax[$config['orderLineTaxCode']])){
								$LineItemTax[$config['orderLineTaxCode']]['tax']	+= $orderRows['rowValue']['rowTax']['value'];
								$LineItemTax[$config['orderLineTaxCode']]['net']	+= $orderRows['rowValue']['rowNet']['value'];
							}
							else{
								$LineItemTax[$config['orderLineTaxCode']]['tax']	= $orderRows['rowValue']['rowTax']['value'];
								$LineItemTax[$config['orderLineTaxCode']]['net']	= $orderRows['rowValue']['rowNet']['value'];
							}
						}
					}
					
					if(!$LineTaxId){
						if($orderRows['rowValue']['rowTax']['value'] > 0){
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef'] = array('value' => $config['orderLineTaxCode']);
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']	= array('value' => $config['salesNoTaxCode']);
						}
					}
					if($isDDPOrder){
						$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']		= array( 'value' => $config['salesNoTaxCode'] );
					}
					$invoiceLineCount++;
				}
				
				if($totalItemDiscount){
					foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmount){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Item Discount',
							'Amount'				=> - sprintf("%.4f",($totalItemDiscountLineAmount*1)),
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['discountItem']),
								'Qty'					=> 1,
								'UnitPrice'				=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount)),
							),	
						);
						if($TaxID){
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']		= array( 'value' => $TaxID);
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']		= array( 'value' => $config['salesNoTaxCode']);
						}
						$invoiceLineCount++;
					}
				}
				
				if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					if($discountCouponAmt){
						foreach($discountCouponAmt as $TaxID => $discountCouponLineAmount){
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'LineNum'				=> $linNumber++,
								'Description'			=> 'Coupon Discount',
								'Amount'				=> - sprintf("%.4f",($discountCouponLineAmount*1)),
								'DetailType'			=> 'SalesItemLineDetail',
								'SalesItemLineDetail'	=> array(
									'ItemRef'				=> array('value' => $config['couponItem']),
									'Qty'					=> 1,
									'UnitPrice'				=> sprintf("%.4f",((-1) * $discountCouponLineAmount)),
									'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
								),
							);
							if($TaxID){
								$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef'] = array( 'value' => $TaxID); 
							}
							else{
								$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef'] = array( 'value' => $config['salesNoTaxCode']);
							}
							$invoiceLineCount++;
						}
					}
				}
				
				if($config['SendTaxAsLineItem']){
					if($BrightpearlTotalTAX > 0){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Total Tax Amount',
							'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['TaxLineItemCode']),
								'Qty' 					=> 1,
								'UnitPrice' 			=> sprintf("%.4f",$BrightpearlTotalTAX),
								'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
							),	
						);
						$invoiceLineCount++;
					}
				}
			}
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('refund_receipt',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			foreach($paymentDetails as $key => $paymentDetail){
				if($paymentDetail['paymentType'] == 'PAYMENT'){
					if($paymentDetail['amount'] > 0){
						if($paymentDetail['paymentMethod'] == $giftCardPayment){
							$InvoiceLineAdd[$invoiceLineCount] = array(
								'LineNum'				=> $linNumber++,
								'Description'			=> 'Gift Card Payment',
								'Amount'				=> ((-1) * (sprintf("%.4f",$paymentDetail['amount']))),
								'DetailType'			=> 'SalesItemLineDetail',
								'SalesItemLineDetail'	=> array(
									'ItemRef'				=> array('value' => $config['giftCardItem']),
									'Qty' 					=> 1,
									'UnitPrice' 			=> ((-1) * (sprintf("%.4f",$paymentDetail['amount']))),
									'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
								),	
							);
							$invoiceLineCount++;
						}
					}
				}
			}
			
			//taxdate chanages
			$BPDateOffset	= (int)substr($paymentDate,23,3);
			$QBOoffset		= 0;
			$diff			= $BPDateOffset - $QBOoffset;
			$date			= new DateTime($paymentDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
			}
			$paymentDate	= $date->format('Y-m-d');
			
			
			
			$invoiceRef		= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			if($config['UseAsDocNumbersosc']){
				$account1FieldIds	= explode(".",$config['UseAsDocNumbersosc']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$invoiceRef	= $fieldValueTmps;
				}
				else{
					$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
				}
			}
			if(!$invoiceRef){
				$invoiceRef	= $orderId;
			}
			
			if($config['disableSkuDetails']){
				$invoiceLineAddNew		= array();
				$invoiceLineFormatted	= array();
				$newItemLineCount		= 0;
				foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['trackedGenericItem']){
						$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['genericSku']){
						$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['Description'] == 'Special Item'){
						$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['discountItem']){
						$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['couponItem']){
						$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['shippingItem']){
						$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['TaxLineItemCode']){
						$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['giftCardItem']){
						$invoiceLineAddNew[8][]	= $InvoiceLineAddTemp;
						continue;
					}
				}
				ksort($invoiceLineAddNew);
				foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
					foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
						$invoiceLineFormatted[$newItemLineCount]			= $invoiceLineAddNewTempTemp;
						$invoiceLineFormatted[$newItemLineCount]['LineNum']	= ($newItemLineCount + 1);
						$newItemLineCount++;
					}
				}
				if($invoiceLineFormatted){
					$InvoiceLineAdd		= $invoiceLineFormatted;
					$invoiceLineCount	= $newItemLineCount;
					$linNumber			= ($newItemLineCount + 1);
				}
			}

			//chnageReq added on 8th fab 2023 req by Tushar
			if($clientcode == 'aimeeqbo'){
				foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
					if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
						$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Shipping Charges';
					}
				}
			}
			
			foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
				if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
					unset($InvoiceLineAdd[$lineItemIdNewTemp]['Description']);
				}
			}
			
			$invoiceRef	= substr($invoiceRef,0,21);
			$request	= array(
				'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
				'Line'					=> $InvoiceLineAdd,
				'CurrencyRef'			=> array('value' => $rowDatas['currency']['orderCurrencyCode']),
				'TxnDate'				=> $paymentDate,
				'DocNumber'				=> $invoiceRef,
				'CustomerRef'			=> array('value' => $QBOCustomerID),
				'PaymentMethodRef'		=> array('value' => $PaymentMethodRef),
				'PrintStatus'			=> 'NotSet',
				'ExchangeRate'			=> sprintf("%.4f",(1 / $CurrencyRate)),
				'BillAddr'				=> array(
					'Line1'						=> $billAddress['addressLine1'],
					'Line2'						=> (string)$billAddress['addressLine2'],
					'City'						=> $billAddress['addressLine3'],
					'CountrySubDivisionCode'	=> $billAddress['addressLine4'],
					'Country'					=> $billAddress['countryIsoCode'],
					'PostalCode'				=> $billAddress['postalCode'],
				),
				'ShipAddr'				=> array(
					'Line1'						=> $shipAddress['addressLine1'],
					'Line2'						=> (string)$shipAddress['addressLine2'],
					'City'						=> $shipAddress['addressLine3'],
					'CountrySubDivisionCode'	=> $shipAddress['addressLine4'],
					'Country'					=> $shipAddress['countryIsoCode'],
					'PostalCode'				=> $shipAddress['postalCode'],
				),
			);
			if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$rowDatas['currency']['orderCurrencyCode'],$config['defaultCurrrency'],$paymentDate);
				if($exRate){
					$request['ExchangeRate'] = $exRate;
				}
				else{
					$exRate = $exchangeRate[strtolower($rowDatas['currency']['orderCurrencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$request['ExchangeRate'] = $exRate;
					}
					else{
						echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
						unset($request['ExchangeRate']);
					}
				}
			}
			
			if((is_array($ChannelLocationMappings)) AND (!empty($ChannelLocationMappings)) AND ($CustomLocationID) AND ($channelId) AND (isset($ChannelLocationMappings[$CustomLocationID][$channelId]))){
				$request['ClassRef']	= array('value' => $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId']);
			}
			elseif((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
				$request['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
			}
			
			$lineLevelClassId	= '';
			if((is_array($ChannelLocationMappings)) AND (!empty($ChannelLocationMappings)) AND ($CustomLocationID) AND ($channelId) AND (isset($ChannelLocationMappings[$CustomLocationID][$channelId]))){
				$lineLevelClassId	= $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId'];
			}
			elseif((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
				$lineLevelClassId	= $channelMappings[$channelId]['account2ChannelId'];
			}
			if($lineLevelClassId){
				foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
					if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
						continue;
					}
					else{
						$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
					}
				}
			}
			
			if($enableCustomFieldMappings){
				if($CustomFieldMappings){
					foreach($CustomFieldMappings as $CustomFieldMappingsData){
						$CustomFields		= array();
						$account1FieldIds	= explode(".",$CustomFieldMappingsData['account1CustomField']);
						$fieldValue			= '';
						$fieldValueTmps		= '';
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.staffOwnerContactId'){
							$CustomstaffOwnerContactId	= $rowDatas['assignment']['current']['staffOwnerContactId'];
							$fieldValueTmps				= $getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.projectId'){
							$CustomprojectId			= $rowDatas['assignment']['current']['projectId'];
							$fieldValueTmps				= $getAllProjectsData[$orderDatas['account1Id']][$CustomprojectId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.channelId'){
							$CustomchannelId			= $rowDatas['assignment']['current']['channelId'];
							$fieldValueTmps				= $getAllChannelMethodData[$orderDatas['account1Id']][$CustomchannelId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.leadSourceId'){
							$CustomleadSourceId			= $rowDatas['assignment']['current']['leadSourceId'];
							$fieldValueTmps				= $getAllLeadsourceData[$orderDatas['account1Id']][$CustomleadSourceId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.teamId'){
							$CustomteamId				= $rowDatas['assignment']['current']['teamId'];
							$fieldValueTmps				= $getAllTeamData[$orderDatas['account1Id']][$CustomteamId]['name'];
						}
						
						if($fieldValueTmps){
							$allQBOCustomFieldIds = explode(",",$CustomFieldMappingsData['account2CustomField']);
							foreach($allQBOCustomFieldIds as $allQBOCustomFieldIdsTmp){
								$CustomFields = array();
								if(is_array($fieldValueTmps)){
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> substr($fieldValueTmps['value'],0,31),
										'Type'			=> 'StringType',
									);
								}
								else{
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> substr($fieldValueTmps,0,31),
										'Type'			=> 'StringType',
									);
								}
								if($CustomFields){
									$request['CustomField'][]	= $CustomFields;
								}
							}
						}
					}
					if(!$request['CustomField']){
						unset($request['CustomField']);
					}
				}
			}
			
			
			$url				= 'refundreceipt?minorversion=37';
			$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData['Request data	: ']	= $request;
			$createdRowData['Response data	: ']	= $results;
			
			$this->ci->db->update('refund_receipt',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
			if((isset($results['RefundReceipt']['Id']))){
				$this->ci->db->update('refund_receipt',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['RefundReceipt']['Id'],'message' => '', 'PostedTime' => date('c')),array('orderId' => $orderId));
				
				$QBOTotalAmount			= $results['RefundReceipt']['TotalAmt'];
				$NetRoundOff			= $BrightpearlTotalAmount - $QBOTotalAmount;
				$RoundOffCheck			= abs($NetRoundOff);
				$RoundOffApplicable		= 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.99){
						$RoundOffApplicable = 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'RoundOffItem',
							'Amount'				=> sprintf("%.4f",$NetRoundOff),
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['roundOffItem']),
								'Qty'					=> 1,
								'UnitPrice'				=> sprintf("%.4f",$NetRoundOff),
								'TaxCodeRef'			=> array(
									'value'					=> $config['salesNoTaxCode'] 
								),
							),
						);
						$request['Line']		= $InvoiceLineAdd;
						$request['SyncToken']	= 0;
						$request['Id']			= $results['RefundReceipt']['Id'];
						$url					= 'refundreceipt?minorversion=37';  
						$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Rounding Request data	: ']	= $request;
						$createdRowData['Rounding Response data	: ']	= $results2;
						$this->ci->db->update('refund_receipt',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
						if($results2['RefundReceipt']['Id']){
							$this->ci->db->update('refund_receipt',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results2['RefundReceipt']['Id'],'message' => ''),array('orderId' => $orderId));
						}
						else{
							$this->ci->db->update('refund_receipt',array('message' => 'Unable to add roundOff Item'),array('orderId' => $orderId));
						}
					}
				}
			}
		}
	}
}
?>