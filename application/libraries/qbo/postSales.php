<?php
$this->reInitialize();
$AllTaxInfo									= $this->getAllTax();
$exchangeRates								= $this->getExchangeRate();
$enableSendSCasPO							= $this->ci->globalConfig['enableSendSCasPO'];
$enableCustomFieldMappings					= $this->ci->globalConfig['enableCustomFieldMapping'];
$enableInventoryManagement					= $this->ci->globalConfig['enableInventoryManagement'];
$enableAggregation							= $this->ci->globalConfig['enableAggregation'];
$UnInvoicingEnabled							= 1;
$clientcode									= $this->ci->config->item('clientcode');
$enableGenericCustomerMappingCustomazation	= $this->ci->globalConfig['enableGenericCustomerMappingCustomazation'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$allBPShippingMethods						= array();
if(($clientcode == 'officesigncoqbo') OR ($clientcode == 'dunejewelryqbo') OR ($clientcode == 'qbodemo')){
	$allBPShippingMethods	= $this->ci->brightpearl->getAllShippingMethod();
}
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config			= $this->accountConfig[$account2Id];
	$Acc2AllTaxInfo	= $AllTaxInfo[$account2Id];
	$exchangeRate	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){continue;}
	
	
	//CREATE TERMS MAPPING	-:	
	$this->ci->db->reset_query();
	$termsMappingTemps	= $this->ci->db->get_where('mapping_terms',array('account2Id' => $account2Id))->result_array();
	$termsMapping		= array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$termsMapping[$termsMappingTemp['account1TermsId']]	= $termsMappingTemp;
	}
	
	$genericcustomerMappings			= array();
	$GenericCustomerCurrencyMappings	= array();
	$AllCustomFieldsForGenericMapping	= array();
	$GenericCustomerMappingAdvanced		= 0;
	if($this->ci->globalConfig['enableGenericcustomerMapping']){
		$this->ci->db->reset_query();
		$genericcustomerMappingsTemps		= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
		if($genericcustomerMappingsTemps){
			foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
				$MappedCurrency		= '';
				$MappedChannel		= '';
				$MappedCurrency		= strtolower($genericcustomerMappingsTemp['account1CurrencylId']);
				$MappedChannel		= $genericcustomerMappingsTemp['account1ChannelId'];
				
				
				if($enableGenericCustomerMappingCustomazation){
					$GenericCustomerMappingAdvanced		= 1;
					$brightpearlCustomFieldName			= trim(strtolower($genericcustomerMappingsTemp['brightpearlCustomFieldName']));
					$IncludedStringInCustomField		= explode("||",trim($genericcustomerMappingsTemp['IncludedStringInCustomField']));
					$AllCustomFieldsForGenericMapping[$brightpearlCustomFieldName]	= $brightpearlCustomFieldName; 
					if($MappedCurrency){
						if($IncludedStringInCustomField AND $brightpearlCustomFieldName){
							foreach($IncludedStringInCustomField as $IncludedStringInCustomFieldTemp){
								$IncludedDataString		= trim(strtolower($IncludedStringInCustomFieldTemp));
								$GenericCustomerCurrencyMappings[$MappedChannel][$MappedCurrency][$brightpearlCustomFieldName][$IncludedDataString]	= $genericcustomerMappingsTemp;
							}
						}
						else{
							$GenericCustomerCurrencyMappings[$MappedChannel][$MappedCurrency]['Blank']	= $genericcustomerMappingsTemp;
						}
					}
					else{
						if($IncludedStringInCustomField AND $brightpearlCustomFieldName){
							foreach($IncludedStringInCustomField as $IncludedStringInCustomFieldTemp){
								$IncludedDataString		= trim(strtolower($IncludedStringInCustomFieldTemp));
								$genericcustomerMappings[$MappedChannel][$brightpearlCustomFieldName][$IncludedDataString]	= $genericcustomerMappingsTemp;
							}
						}
						else{
							$genericcustomerMappings[$MappedChannel]['Blank']	= $genericcustomerMappingsTemp;
						}
					}
				}
				else{
					if($MappedCurrency){
						$GenericCustomerCurrencyMappings[$MappedChannel][$MappedCurrency]	= $genericcustomerMappingsTemp;
					}
					else{
						$genericcustomerMappings[$MappedChannel]							= $genericcustomerMappingsTemp;
					}
				}
				
			}
		}
	}
	
	$AggregationMappings	= array();
	$AggregationMappings2	= array();
	if($this->ci->globalConfig['enableAggregation']){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]								= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
					}
				}
			}
		}
	}
	
	if($this->ci->globalConfig['enableNominalMapping']){
		$this->ci->db->reset_query();
		$nominalMappings		= array();
		$nominalChannelMappings	= array();
		$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
		if(!empty($nominalMappingTemps)){
			foreach($nominalMappingTemps as $nominalMappingTemp){
				if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
					$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
				}
				else{
					$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$channelMappings		= array();
	if($this->ci->globalConfig['enableChannelMapping']){
		$this->ci->db->reset_query();
		$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
		if($channelMappingsTemps){
			foreach($channelMappingsTemps as $channelMappingsTemp){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$ChannelLocationMappings		= array();
	if($this->ci->globalConfig['enableChannelLocationMapping']){
		$this->ci->db->reset_query();
		$ChannelLocationMappingsTemps	= $this->ci->db->get_where('mapping_channelLocation',array('account2Id' => $account2Id))->result_array();
		if($ChannelLocationMappingsTemps){
			foreach($ChannelLocationMappingsTemps as $ChannelLocationMappingsTemp){
				$ChannelLocationMappings[$ChannelLocationMappingsTemp['account1LocationId']][$ChannelLocationMappingsTemp['account1ChannelId']]	= $ChannelLocationMappingsTemp;
			}
		}
	}
	
	$CustomFieldMappings		= array();
	$getAllSalesrepData			= array();
	$getAllProjectsData			= array();
	$getAllChannelMethodData	= array();
	$getAllLeadsourceData		= array();
	$getAllTeamData				= array();
	$getAllWarehouseData		= array();
	$getAllShippingMethodData	= array();
	if($this->ci->globalConfig['enableCustomFieldMapping']){
		$this->ci->db->reset_query();
		$isAssignmentMapped						= 0;
		$isAssignmentstaffOwnerContactIdMapped	= 0;
		$isAssignmentprojectIdMapped			= 0;
		$isAssignmentchannelIdMapped			= 0;
		$isAssignmentleadSourceIdMapped			= 0;
		$isAssignmentteamIdMapped				= 0;
		$isAssignmentWarehouseIdMapped			= 0;
		$isAssignmentshippingIdMapped			= 0;
		$CustomFieldMappingsTemps	= $this->ci->db->get_where('mapping_customfield',array('type' => 'sales', 'account2Id' => $account2Id))->result_array();
		if($CustomFieldMappingsTemps){
			foreach($CustomFieldMappingsTemps as $CustomFieldMappingsTemp){
				$CustomFieldMappings[$CustomFieldMappingsTemp['account1CustomField']]	= $CustomFieldMappingsTemp;
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'staffownercontactid')){
					$isAssignmentMapped	= 1;
					$isAssignmentstaffOwnerContactIdMapped	= 1;
				}
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'projectid')){
					$isAssignmentMapped	= 1;
					$isAssignmentprojectIdMapped			= 1;
				}
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'channelid')){
					$isAssignmentMapped	= 1;
					$isAssignmentchannelIdMapped			= 1;
				}
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'leadsourceid')){
					$isAssignmentMapped	= 1;
					$isAssignmentleadSourceIdMapped			= 1;
				}
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'teamid')){
					$isAssignmentMapped	= 1;
					$isAssignmentteamIdMapped				= 1;
				}
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'warehouseid')){
					$isAssignmentMapped	= 1;
					$isAssignmentWarehouseIdMapped			= 1;
				}
				if(substr_count(strtolower($CustomFieldMappingsTemp['account1CustomField']),'shippingmethodid')){
					$isAssignmentMapped	= 1;
					$isAssignmentshippingIdMapped			= 1;
				}
			}
		}
		if($CustomFieldMappings AND $isAssignmentMapped){
			if($isAssignmentstaffOwnerContactIdMapped){
				$getAllSalesrepData			= $this->ci->brightpearl->getAllSalesrep();
			}
			if($isAssignmentprojectIdMapped){
				$getAllProjectsData			= $this->ci->brightpearl->getAllProjects();
			}
			if($isAssignmentchannelIdMapped){
				$getAllChannelMethodData	= $this->ci->brightpearl->getAllChannelMethod();
			}
			if($isAssignmentleadSourceIdMapped){
				$getAllLeadsourceData		= $this->ci->brightpearl->getAllLeadsource();
			}
			if($isAssignmentteamIdMapped){
				$getAllTeamData				= $this->ci->brightpearl->getAllTeam();
			}
			if($isAssignmentWarehouseIdMapped){
				$getAllWarehouseData		= $this->ci->brightpearl->getAllLocation();
			}
			if($isAssignmentshippingIdMapped){
				$getAllShippingMethodData	= $this->ci->brightpearl->getAllShippingMethod();
			}
		}
	}
	
	$paymentTermsMappings	= array();
	if($this->ci->globalConfig['enablePaymenttermsMapping']){
		$this->ci->db->reset_query();
		$paymentTermsMappingsTemps	= $this->ci->db->get_where('mapping_paymentterms',array('type' => 'sales','account2Id' => $account2Id))->result_array();
		if($paymentTermsMappingsTemps){
			foreach($paymentTermsMappingsTemps as $paymentTermsMappingsTemp){
				if($paymentTermsMappingsTemp['account1SalesTermId'] != 'NA'){
					$paymentTermsMappings[$paymentTermsMappingsTemp['account1SalesTermId']]	= $paymentTermsMappingsTemp;
				}
			}
		}
	}
	
	if(!empty($datas)){
		$allPostContactIds	= array();
		foreach($datas as $datasTemp){
			$tempRowData		= json_decode($datasTemp['rowData'],true);
			$channelId			= $tempRowData['assignment']['current']['channelId'];
			$orderCurrencyCode	= strtolower($tempRowData['currency']['orderCurrencyCode']);
			if($channelId){
				if(isset($genericcustomerMappings[$channelId])){continue;}
				if(isset($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings[$channelId]['bpaccountingcurrency'])){continue;}
				if(isset($AggregationMappings2[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings2[$channelId]['bpaccountingcurrency'])){continue;}
				$allPostContactIds[]	= $datasTemp['customerId'];
			}
			else{
				$allPostContactIds[]	= $datasTemp['customerId'];
			}
		}
		$allPostContactIds = array_filter($allPostContactIds);
		$allPostContactIds = array_unique($allPostContactIds);
		if(!empty($allPostContactIds)){
			$this->postCustomers($allPostContactIds,$account2Id);
		}
	}
	
	if(!$config['disableSkuDetails']){
		if(!empty($datas)){
			$allPostItmeIds	= array();
			foreach($datas as $datasTemp){
				$rowDatas			= json_decode($datasTemp['rowData'], true);
				$channelId			= $rowDatas['assignment']['current']['channelId'];
				$orderCurrencyCode	= strtolower($rowDatas['currency']['orderCurrencyCode']);
				
				if(isset($AggregationMappings[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings[$channelId]['bpaccountingcurrency'])){continue;}
				if(isset($AggregationMappings2[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings2[$channelId]['bpaccountingcurrency'])){continue;}
				foreach($rowDatas['orderRows'] as $orderRowsTemp){
					if($orderRowsTemp['productId'] > 1001){
						$allPostItmeIds[]	= $orderRowsTemp['productId'];
					}
				}
			}
			$allPostItmeIds = array_filter($allPostItmeIds);
			$allPostItmeIds = array_unique($allPostItmeIds);
			if(!empty($allPostItmeIds)){
				$this->postProducts($allPostItmeIds,$account2Id);
			}
		}
	}
	
	$this->ci->db->reset_query();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('createdCustomerId <>' => NULL, 'account2Id' => $account2Id))->result_array();
	$customerMappings		= array();
	if($customerMappingsTemps){
		foreach($customerMappingsTemps as $customerMappingsTemp){
			if($customerMappingsTemp['createdCustomerId']){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	if(!$config['disableSkuDetails']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id,'createdProductId <>' => NULL))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				if($productMappingsTemp['createdProductId']){
					$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
				}
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			}
		}
	}
	
	if($this->ci->globalConfig['enablePaymenttermsMapping']){
		$this->ci->db->reset_query();
		$paymentTermsMappingsTemps	= $this->ci->db->get_where('mapping_paymentterms',array('type' => 'sales','account2Id' => $account2Id))->result_array();
		$paymentTermsMappings		= array();
		if($paymentTermsMappingsTemps){
			foreach($paymentTermsMappingsTemps as $paymentTermsMappingsTemp){
				if($paymentTermsMappingsTemp['account1SalesTermId'] != 'NA'){
					$paymentTermsMappings[$paymentTermsMappingsTemp['account1SalesTermId']]	= $paymentTermsMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if($defaultItemMappingTemps){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	if($datas){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['sendInAggregation']){continue;}
			
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			if(!$channelId){$channelId	= 'blank';}
			$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			$ConsolAPIFieldValueID	= '';
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			
			$IsConsolApplicable		= 0;
			$IsNonConsolApplicable	= 0;
			
			if($this->ci->globalConfig['enableAggregation']){
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						if($enableConsolidationMappingCustomazation){
							if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
								$IsConsolApplicable					= 0;
								$IsNonConsolApplicable				= 0;
								$ExcludedStringInCustomField		= array();
								$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
								$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
								if(!$AggregatedCustomFieldData){
									$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
									$APIfieldValueTmps		= '';
									foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
										if(!$APIfieldValueTmps){
											$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
										}
										else{
											$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
										}
									}
									if($APIfieldValueTmps){
										$AggregatedCustomFieldData	= $APIfieldValueTmps;
									}
								}
								$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
								$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
								if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
									foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
										$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
										if(is_array($AggregatedCustomFieldData)){
											if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
										else{
											if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
									}
								}
								else{
									continue;
								}
								if($IsConsolApplicable){
									continue;
								}
							}
							else{
								continue;
							}
						}
						else{
							continue;
						}
					}
					if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]){
						if($enableConsolidationMappingCustomazation){
							if(($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['ExcludedStringInCustomField'])){
								$IsConsolApplicable					= 0;
								$IsNonConsolApplicable				= 0;
								$ExcludedStringInCustomField		= array();
								$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['brightpearlCustomFieldName']);
								$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
								if(!$AggregatedCustomFieldData){
									$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
									$APIfieldValueTmps		= '';
									foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
										if(!$APIfieldValueTmps){
											$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
										}
										else{
											$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
										}
									}
									if($APIfieldValueTmps){
										$AggregatedCustomFieldData	= $APIfieldValueTmps;
									}
								}
								$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['ExcludedStringInCustomField']));
								$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
								if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
									foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
										$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
										if(is_array($AggregatedCustomFieldData)){
											if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
										else{
											if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
									}
								}
								else{
									continue;
								}
								if($IsConsolApplicable){
									continue;
								}
							}
							else{
								continue;
							}
						}
						else{
							continue;
						}
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
							if($enableConsolidationMappingCustomazation){
								if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
										$APIfieldValueTmps		= '';
										foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
											if(!$APIfieldValueTmps){
												$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
											}
											else{
												$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
											}
										}
										if($APIfieldValueTmps){
											$AggregatedCustomFieldData	= $APIfieldValueTmps;
										}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
						elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']){
							if($enableConsolidationMappingCustomazation){
								if(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
									$IsNonConsolApplicable				= 0;
									$IsConsolApplicable					= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
										$APIfieldValueTmps		= '';
										foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
											if(!$APIfieldValueTmps){
												$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
											}
											else{
												$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
											}
										}
										if($APIfieldValueTmps){
											$AggregatedCustomFieldData	= $APIfieldValueTmps;
										}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
					if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]){
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]){
							if($enableConsolidationMappingCustomazation){
								if(($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
										$APIfieldValueTmps		= '';
										foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
											if(!$APIfieldValueTmps){
												$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
											}
											else{
												$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
											}
										}
										if($APIfieldValueTmps){
											$AggregatedCustomFieldData	= $APIfieldValueTmps;
										}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
						elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']){
							if($enableConsolidationMappingCustomazation){
								if(($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['ExcludedStringInCustomField'])){
									$IsNonConsolApplicable				= 0;
									$IsConsolApplicable					= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
										$APIfieldValueTmps		= '';
										foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
											if(!$APIfieldValueTmps){
												$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
											}
											else{
												$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
											}
										}
										if($APIfieldValueTmps){
											$AggregatedCustomFieldData	= $APIfieldValueTmps;
										}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
				}
			}
			
			//		UNINVOICING STARTS		//
			if($UnInvoicingEnabled){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						$TempAcc2ID		= '';
						if($orderDatas['TempAcc2ID']){
							$TempAcc2ID	= $orderDatas['TempAcc2ID'];
						}
						else{
							$TempAcc2ID	= $account2Id;
						}
						$IsPaid			= 0;
						if($orderDatas['paymentDetails']){
							$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
							foreach($paymentDetails as $pKey => $paymentDetail){
								if($paymentDetail['amount'] > 0){
									if($paymentDetail['status'] == 1){
										$IsPaid	= 1;
										break;
									}
								}
							}
						}
						else{
							$IsPaid	= 0;
						}
						if($IsPaid == 0){
							$createdRowData		= json_decode($orderDatas['createdRowData'],true);
							$uninvoiceCount		= $orderDatas['uninvoiceCount'];
							$SyncToken			= '';
							$QBOorderId			= $orderDatas['createOrderId'];			
							$readurl			= 'invoice/'.$QBOorderId.'?minorversion=41';
							$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
							if((substr_count(strtolower($InvoiceResponse['Invoice']['PrivateNote']),'voided')) OR (substr_count(strtolower($InvoiceResponse['Invoice']['PrivateNote']),'anulado'))){
								$uninvoiceCount++;
								$this->ci->db->update('sales_order',array('paymentDetails' => '', 'TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount),array('orderId' => $orderId));
								continue;
							}
							else{
								if($InvoiceResponse['Invoice']){
									$SyncToken	= $InvoiceResponse['Invoice']['SyncToken'];
								}
								$request		= array(
									"SyncToken"		=>	$SyncToken,
									"Id"			=>	$QBOorderId,
								);
								$url				= 'invoice?operation=void&minorversion=41';
								$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $TempAcc2ID)[$TempAcc2ID];
								$createdRowData['Void Request data	: ']	= $request;
								$createdRowData['Void Response data	: ']	= $results;
								$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
								if((substr_count(strtolower($results['Invoice']['PrivateNote']),'voided')) OR (substr_count(strtolower($results['Invoice']['PrivateNote']),'anulado'))){
									$uninvoiceCount++;
									$this->ci->db->update('sales_order',array('paymentDetails' => '', 'TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount),array('orderId' => $orderId));
									continue;
								}
								else{
									$this->ci->db->update('sales_order',array('message' => 'Unable to Void the Invoice On QBO'),array('orderId' => $orderId));
									continue;
								}
							}
						}
						elseif($IsPaid	== 1){
							$QBOpaymentIds		= array();
							$createdRowData		= json_decode($orderDatas['createdRowData'],true);
							$allVoidResponse	= array();
							$uninvoiceCount		= $orderDatas['uninvoiceCount'];
							$payment_datas		= json_decode($orderDatas['paymentDetails'],true);
							foreach($payment_datas as $paymemt_key => $payment_data){
								if((strtolower($payment_data['paymentInitiatedIn']) ==  'qbo') OR (strtolower($payment_data['AmountCreditedIn']) == 'qbo')){
									$QBOpaymentIds[]	= $paymemt_key;
									unset($payment_datas[$paymemt_key]);
								}
							}
							foreach($QBOpaymentIds as $QBOpaymentId){
								$SyncToken			= '';
								$GetPaymentDetail	= 'payment/'.$QBOpaymentId.'?minorversion=41';
								$Paymentresponse	= $this->getCurl($GetPaymentDetail, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
								if($Paymentresponse['Payment']){
									if((substr_count(strtolower($Paymentresponse['Payment']['PrivateNote']),'voided')) OR (substr_count(strtolower($Paymentresponse['Payment']['PrivateNote']),'anulado'))){
										continue;
									}
									else{
										$SyncToken	= $Paymentresponse['Payment']['SyncToken'];
										$VoidPaymentRequest		= array(
											"SyncToken"				=>	$SyncToken,
											"Id"					=>	$QBOpaymentId,
											"sparse"				=>	true,
										);
										$VoidPaymentURL			= 'payment?operation=update&include=void&minorversion=41';
										$VoidPaymentResponse	= $this->getCurl($VoidPaymentURL, 'POST', json_encode($VoidPaymentRequest), 'json', $TempAcc2ID)[$TempAcc2ID];
										$allVoidResponse[]		= $VoidPaymentResponse;
									}
								}
							}
							$checkvoidflag	= 0;
							if($allVoidResponse){
								$checkvoidflag	= 0;
								foreach($allVoidResponse as $allVoidResponses){
									if((substr_count(strtolower($allVoidResponses['Payment']['PrivateNote']),'voided')) OR (substr_count(strtolower($allVoidResponses['Payment']['PrivateNote']),'anulado'))){
										continue;
									}
									else{
										$checkvoidflag	= 1;
									}
								}
							}
							if($checkvoidflag	== 1){
								$this->ci->db->update('sales_order',array('message' => 'Unable to Void the Payment On QBO'),array('orderId' => $orderId));
								continue;
							}
							else{
								$SyncToken			= '';
								$QBOorderId			= $orderDatas['createOrderId'];			
								$ReadInvoiceURL		= 'invoice/'.$QBOorderId;
								$InvoiceResponse	= $this->getCurl($ReadInvoiceURL, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
								if((substr_count(strtolower($InvoiceResponse['Invoice']['PrivateNote']),'voided')) OR (substr_count(strtolower($InvoiceResponse['Invoice']['PrivateNote']),'anulado'))){
									$uninvoiceCount++;
									$this->ci->db->update('sales_order',array('paymentDetails' => '', 'TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '','isPaymentCreated' => '0', 'sendPaymentTo' => '', 'uninvoiceCount' => $uninvoiceCount),array('orderId' => $orderId));
									continue;
								}
								else{
									if($InvoiceResponse['Invoice']){
										$SyncToken	= $InvoiceResponse['Invoice']['SyncToken'];
									}
									if($SyncToken){
										$VoidInvoiceRequest		= array(
											"SyncToken"		=>	$SyncToken,
											"Id"			=>	$QBOorderId,
										);
										$VoidInvoiceURL			= 'invoice?operation=void&minorversion=41';
										$VoidInvoiceResponse	= $this->getCurl($VoidInvoiceURL, 'POST', json_encode($VoidInvoiceRequest), 'json', $TempAcc2ID)[$TempAcc2ID];
										$createdRowData['VoidPaymentResponse']	= $allVoidResponse;
										$createdRowData['Void Request data	:']	= $VoidInvoiceRequest;
										$createdRowData['Void Response data	:']	= $VoidInvoiceResponse;
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
										if((substr_count(strtolower($VoidInvoiceResponse['Invoice']['PrivateNote']),'voided')) OR (substr_count(strtolower($VoidInvoiceResponse['Invoice']['PrivateNote']),'anulado'))){
											$uninvoiceCount++;
											$this->ci->db->update('sales_order',array('TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '','isPaymentCreated' => '0', 'sendPaymentTo' => '', 'paymentDetails' => '',  'uninvoiceCount' => $uninvoiceCount),array('orderId' => $orderId));
											continue;
										}
										else{
											$this->ci->db->update('sales_order',array('message' => 'Unable to Void the Invoice On QBO'),array('orderId' => $orderId));
											continue;
										}
									}
								}
							}
						}
						else{
							continue;
						}
						continue;
					}
				}
			}
			//		UNINVOICING ENDS		//
			
			if($orderDatas['createOrderId']){continue;}
			if(!$orderDatas['invoiced']){continue;}
			
			/* CODE TO CHECK DROPSHIP PO'S ARE SENT OR NOT, IF NOT THEN SKIP THAT SALES ORDER		*/
			/* $CONFIG['INVENTORYMANAGEMENTENABLED'] == 0 THAT MEANS CONNECTOR IS INVENTORY MANAGED	*/
			if($config['InventoryManagementEnabled'] == 0){
				if($orderDatas['IsDropShip']){
					$DropShipPODetail	= $this->ci->db->get_where('purchase_order',array('LinkedWithSO' => $orderId,'createOrderId' => NULL))->result_array();
					if($DropShipPODetail){
						$this->ci->db->update('sales_order',array('message' => 'DropShipPO is not Sent Yet'),array('orderId' => $orderId));
						continue;
					}
				}
			}
			
			$isGenericContact		= 0;
			$QBOCustomerID			= '';
			$rowDatas		        = json_decode($orderDatas['rowData'],true);
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$channelId		        = $rowDatas['assignment']['current']['channelId'];
			if(!$channelId){$channelId	= 'blank';}
			
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$billAddress			= $rowDatas['parties']['billing'];
			$shipAddress 	        = $rowDatas['parties']['delivery'];
			$orderCustomer 	        = $rowDatas['parties']['customer'];
			$BrightpearlTotalAmount	= $rowDatas['totalValue']['total'];
			$BrightpearlTotalTAX	= $rowDatas['totalValue']['taxAmount'];
			$QBOCustomerID			= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
			$CustomLocationID		= $rowDatas['customFields'][$bpconfig['SOLocationCustomField']]['id'];
			$genericcustomerMapping	= array();
			
			if($GenericCustomerMappingAdvanced){
				foreach($rowDatas['customFields'] as $fieldNameKey => $AllCustFieldDataTemp){
					$fieldNameKey	= strtolower($fieldNameKey);
					if(in_array($fieldNameKey,$AllCustomFieldsForGenericMapping)){
						if($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode][$fieldNameKey]){
							foreach($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode][$fieldNameKey] as $IncludedString => $GenericMappingTemp){
								if(is_array($AllCustFieldDataTemp)){
									if(substr_count(strtolower($AllCustFieldDataTemp['value']),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
								else{
									if(substr_count(strtolower($AllCustFieldDataTemp),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
							}
						}
						elseif($genericcustomerMappings[$channelId][$fieldNameKey]){
							foreach($genericcustomerMappings[$channelId][$fieldNameKey] as $IncludedString => $GenericMappingTemp){
								if(is_array($AllCustFieldDataTemp)){
									if(substr_count(strtolower($AllCustFieldDataTemp['value']),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
								else{
									if(substr_count(strtolower($AllCustFieldDataTemp),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
							}
						}
					}
				}
				if(!$genericcustomerMapping){
					if($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode]['Blank']){
						$genericcustomerMapping	= $GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode]['Blank'];
					}
					elseif($genericcustomerMappings[$channelId]['Blank']){
						$genericcustomerMapping	= $genericcustomerMappings[$channelId]['Blank'];
					}
				}
			}
			else{
				if($GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode]){
					$genericcustomerMapping	= $GenericCustomerCurrencyMappings[$channelId][$orderCurrencyCode];
				}
				elseif($genericcustomerMappings[$channelId]){
					$genericcustomerMapping	= $genericcustomerMappings[$channelId];
				}
			}
			
			if($genericcustomerMapping){
				if($genericcustomerMapping['account2ChannelId']){
					$QBOCustomerID		= $genericcustomerMapping['account2ChannelId'];
					$isGenericContact	= 1;
				}
			}
			if(!$QBOCustomerID){
				$this->ci->db->update('sales_order',array('message' => 'Customer not sent yet.'),array('orderId' => $orderId));
				continue;
			}
			
			if(!$rowDatas['invoices']['0']['invoiceReference']){continue;}
			
			$productUpdateNominalCodeInfos	= array();
			$discountCouponAmt				= array();
			$totalItemDiscount				= array();
			$missingSkus					= array();
			$InvoiceLineAdd					= array();
			$invoiceLineCount				= 0;
			$isDiscountCouponAdded			= 0;
			$isDiscountCouponTax			= 0;
			$itemDiscountTax				= 0;
			$orderTaxAmount					= 0;
			$taxAmount						= 0;
			$linNumber						= 1;
			$orderTaxId						= '';
			$couponItemLineID				= '';
			$countryIsoCode					= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState					= strtolower(trim($shipAddress['addressLine4']));
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$productId	= $orderRows['productId'];
				if($productId <= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
				}
			}
			
			$itemtaxAbleLine		= $config['orderLineTaxCode'];
			$discountTaxAbleLine	= $config['orderLineTaxCode'];
			$LineItemTax			= array();
			
			if($config['disableSkuDetails']){
				$ProductArray		= array();
				foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
					if($rowId == $couponItemLineID){
						if($rowdatass['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					
					$bpNominal			= $rowdatass['nominalCode'];
					$bpTaxID			= $rowdatass['rowValue']['taxClassId'];
					$rowNet				= $rowdatass['rowValue']['rowNet']['value'];
					$rowTax				= $rowdatass['rowValue']['rowTax']['value'];
					$discountCouponAmt	= 0;
					$discountAmt		= 0;
					$productId			= $rowdatass['productId'];
					
					$bundleParentID	= '';
					$isBundleChild	= $rowdatass['composition']['bundleChild'];
					if($isBundleChild){
						$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
					}
					if($isBundleChild AND $bundleParentID){
						$bpTaxID		= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
					}
					
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$isTrackedItem		= 0;
					if($productMappings[$productId]){
						$productParams	= json_decode($productMappings[$productId]['params'], true);
						$isTrackedItem	= $productParams['stock']['stockTracked'];
					}
					else{
						if($productId > 1001){
							$missingSkus[]	= $rowdatass['productSku'];
							continue;
						}
					}
					
					$taxMappingKey	= $bpTaxID;
					$taxMappingKey1	= $bpTaxID;
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode;
						}
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					$taxMapping	= array();
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$bpTaxID.'--'.$channelId])){
						$taxMapping	= $taxMappings[$bpTaxID.'--'.$channelId];
					}
					$LineTaxId  		= $taxMapping['account2LineTaxId'];
					
					if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'])){
						$LineTaxId  		= $taxMapping['account2KidsLineTaxId'];
					}
					
					if(!$config['SendTaxAsLineItem']){
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$bpNominal][$LineTaxId])){
									$ProductArray['discount'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								if($isBundleChild){
									
								}
								else{
									$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
									if(!$originalPrice){
										$originalPrice	= $rowNet;
									}
									if($originalPrice > $rowNet){
										$discountCouponAmt	= $originalPrice - $rowNet;
										$rowNet				= $originalPrice;
										if($discountCouponAmt > 0){
											if(isset($ProductArray['discountCoupon'][$bpNominal][$LineTaxId])){
												$ProductArray['discountCoupon'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $discountCouponAmt;
											}
											else{
												$ProductArray['discountCoupon'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $discountCouponAmt;
											}
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$channelId][$rowdatass['nominalCode']]))){
								$customItemIdQbo	= $defaultItemMapping[$channelId][$rowdatass['nominalCode']];
								if(isset($ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo])){
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['bpTaxTotal']	+= $rowTax;
								}
								else{
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
									$ProductArray['customItems'][$bpNominal][$LineTaxId][$customItemIdQbo]['bpTaxTotal']	= $rowTax;
								}
							}
							else{
								if($isTrackedItem){
									if(isset($ProductArray['aggregationItem2'][$bpNominal][$LineTaxId])){
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $rowNet;
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
									}
									else{
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $rowNet;
										$ProductArray['aggregationItem2'][$bpNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
									}
								}
								else{
									if(isset($ProductArray['aggregationItem'][$bpNominal][$LineTaxId])){
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['TotalNetAmt']	+= $rowNet;
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['bpTaxTotal']	+= $rowTax;
									}
									else{
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['TotalNetAmt']	= $rowNet;
										$ProductArray['aggregationItem'][$bpNominal][$LineTaxId]['bpTaxTotal']	= $rowTax;
									}
								}
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$bpNominal][$LineTaxId])){
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['landedCost'][$bpNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$bpNominal][$LineTaxId])){
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['shipping'][$bpNominal][$LineTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$bpNominal][$LineTaxId])){
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['giftcard'][$bpNominal][$LineTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$bpNominal][$LineTaxId])){
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['couponitem'][$bpNominal][$LineTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
					}
					else{
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$bpNominal])){
									$ProductArray['discount'][$bpNominal]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$bpNominal]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice	= $rowNet;
								}
								if($originalPrice > $rowNet){
									$discountCouponAmt	= $originalPrice - $rowNet;
									$rowNet				= $originalPrice;
									if($discountCouponAmt > 0){
										if(isset($ProductArray['discountCoupon'][$bpNominal])){
											$ProductArray['discountCoupon'][$bpNominal]['TotalNetAmt']	+= $discountCouponAmt;
										}
										else{
											$ProductArray['discountCoupon'][$bpNominal]['TotalNetAmt']	= $discountCouponAmt;
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$channelId][$rowdatass['nominalCode']]))){
								$customItemIdQbo	= $defaultItemMapping[$channelId][$rowdatass['nominalCode']];
								if(isset($ProductArray['customItems'][$bpNominal][$customItemIdQbo])){
									$ProductArray['customItems'][$bpNominal][$customItemIdQbo]['TotalNetAmt']	+= $rowNet;
								}
								else{
									$ProductArray['customItems'][$bpNominal][$customItemIdQbo]['TotalNetAmt']	= $rowNet;
								}
								if(isset($ProductArray['allTax'])){
									$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
								}
								else{
									$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
								}
							}
							else{
								if($isTrackedItem){
									if(isset($ProductArray['aggregationItem2'][$bpNominal])){
										$ProductArray['aggregationItem2'][$bpNominal]['TotalNetAmt']	+= $rowNet;
									}
									else{
										$ProductArray['aggregationItem2'][$bpNominal]['TotalNetAmt']	= $rowNet;
									}
									if(isset($ProductArray['allTax'])){
										$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
									}
									else{
										$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
									}
								}
								else{
									if(isset($ProductArray['aggregationItem'][$bpNominal])){
										$ProductArray['aggregationItem'][$bpNominal]['TotalNetAmt']	+= $rowNet;
									}
									else{
										$ProductArray['aggregationItem'][$bpNominal]['TotalNetAmt']	= $rowNet;
									}
									if(isset($ProductArray['allTax'])){
										$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
									}
									else{
										$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
									}
								}
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$bpNominal])){
								$ProductArray['landedCost'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['landedCost'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$bpNominal])){
								$ProductArray['shipping'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['shipping'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$bpNominal])){
								$ProductArray['giftcard'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['giftcard'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$bpNominal])){
								$ProductArray['couponitem'][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['couponitem'][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']	= $rowTax;
							}
						}
					}
				}
				
				if($ProductArray){
					if(!$config['SendTaxAsLineItem']){
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Expense Item',
											'Amount'				=> sprintf("%.4f",$datas['TotalNetAmt']),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $config['genericSku']),
												'Qty'					=> 1,
												'UnitPrice'				=> sprintf("%.4f",$datas['TotalNetAmt']),
												'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
										if($keyproduct == 'landedCost'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['landedCostItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Landed Cost';
										}
										if($keyproduct == 'shipping'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Shipping';
										}
										if($keyproduct == 'giftcard'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'GiftCard';
										}
										if($keyproduct == 'couponitem'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon';
										}
										if($keyproduct == 'discount'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['discountItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Item Discount';
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
										}
										if($keyproduct == 'discountCoupon'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon Discount';
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
										}
										$invoiceLineCount++;
									}
								}
							}
							elseif($keyproduct == 'customItems'){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $ProductArrayDatass){
										foreach($ProductArrayDatass as $customItemId	=> $customItemIdTemp){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'LineNum'				=> $linNumber++,
												'Description'			=> 'Special Item',
												'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												'DetailType'			=> 'SalesItemLineDetail',
												'SalesItemLineDetail'	=> array(
													'ItemRef'				=> array('value' => $customItemId),
													'Qty'					=> 1,
													'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
													'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
												),
											);
											if($nominalClassID){
												$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
											}
											$invoiceLineCount++;
										}
									}
								}
							}
							else{
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Expense Item',
											'Amount'				=> sprintf("%.4f",$datas['TotalNetAmt']),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $config['genericSku']),
												'Qty'					=> 1,
												'UnitPrice'				=> sprintf("%.4f",$datas['TotalNetAmt']),
												'TaxCodeRef'			=> array('value' => $Taxkeyproduct1),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
										if($keyproduct == 'aggregationItem2'){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Tracked Inventory';
										}
										$invoiceLineCount++;
									}
								}
							}
						}
					}
					else{
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'aggregationItem2') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$IncomeAccountRef	= $config['IncomeAccountRef'];
									if((isset($nominalMappings[$BPNominalCode])) AND ($nominalMappings[$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode])) AND ($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'])){
										$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
									}
									
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'LineNum'				=> $linNumber++,
										'Description'			=> 'Expense Item',
										'Amount'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
										'DetailType'			=> 'SalesItemLineDetail',
										'SalesItemLineDetail'	=> array(
											'ItemRef'				=> array('value' => $config['genericSku']),
											'Qty'					=> 1,
											'UnitPrice'				=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
											'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
										),
									);
									if($nominalClassID){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
									}
									if($keyproduct == 'aggregationItem2'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Tracked Inventory';
									}
									if($keyproduct == 'landedCost'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['landedCostItem']);
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= $config['landedCostItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Landed Cost';
									}
									if($keyproduct == 'shipping'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['shippingItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Shipping';
									}
									if($keyproduct == 'giftcard'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['giftCardItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'GiftCard';
									}
									if($keyproduct == 'couponitem'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon';
									}
									if($keyproduct == 'discount'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['discountItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Item Discount'.'-'.$IncomeAccountRef;
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
									}
									if($keyproduct == 'discountCoupon'){
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $config['couponItem']);
										$InvoiceLineAdd[$invoiceLineCount]['Description']						= 'Coupon Discount';
										$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										$InvoiceLineAdd[$invoiceLineCount]['Amount']							= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
									}
									$invoiceLineCount++;
								}
							}
							elseif($keyproduct == 'customItems'){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$nominalClassID		= '';
									if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$BPNominalCode]))){
										$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$BPNominalCode]['account2ClassId'];
									}
									foreach($ProductArrayDatas as $customItemId	=> $customItemIdTemp){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'Special Item',
											'Amount'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $customItemId),
												'Qty'					=> 1,
												'UnitPrice'				=> sprintf("%.4f",$customItemIdTemp['TotalNetAmt']),
												'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
											),
										);
										if($nominalClassID){
											$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
										}
										$invoiceLineCount++;
									}
								}
							}
							elseif($keyproduct == 'allTax'){
								if($ProductArrayDatasTemp['TotalNetAmt'] <= 0){continue;}
								$InvoiceLineAdd[$invoiceLineCount] = array(
									'LineNum'				=> $linNumber++,
									'Description'			=> 'Total Tax Amount',
									'Amount'				=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
									'DetailType'			=> 'SalesItemLineDetail',
									'SalesItemLineDetail'	=> array(
										'ItemRef'				=> array('value' => $config['TaxLineItemCode']),
										'Qty' 					=> 1,
										'UnitPrice' 			=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
										'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
									),	
								);
								$invoiceLineCount++;
							}
						}
					}
				}
			}
			else{
				ksort($rowDatas['orderRows']);
				foreach($rowDatas['orderRows'] as $rowId => $orderRows){
					if($rowId == $couponItemLineID){
						if($orderRows['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					if($config['BundleSuppression']){
						/*** Bundle component suppression****/
						if(($orderRows['composition']['bundleChild']) && ($orderRows['rowValue']['rowNet']['value'] == 0)){
							continue;
						}
						/***end***/
					}
					$bundleParentID	= '';
					$isBundleChild	= $orderRows['composition']['bundleChild'];
					
					
					
					if($isBundleChild){
						$bundleParentID	= $orderRows['composition']['parentOrderRowId'];
					}
					
					$taxMapping			= array();
					$ItemRefValue		= '';
					$ItemRefName		= '';
					$LineTaxId			= '';
					$isDDPOrder			= '';
					$orderTaxId			= '';
					$productId			= $orderRows['productId'];
					$taxClassId			= $orderRows['rowValue']['taxClassId'];
					if($isBundleChild AND $bundleParentID){
						$taxClassId		= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
					}
					
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax']; 
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$taxMappingKey	= $taxClassId;
					$taxMappingKey1	= $taxClassId;
					
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $taxClassId.'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $taxClassId.'-'.$countryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $taxClassId.'-'.$countryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $taxClassId.'-'.$countryIsoCode;
						}
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					$taxMapping	= array();
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$taxClassId.'--'.$channelId])){
						$taxMapping	= $taxMappings[$taxClassId.'--'.$channelId];
					}
					
					$LineTaxId  = $taxMapping['account2LineTaxId'];
					$orderTaxId = $taxMapping['account2TaxId'];
					if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'] OR $taxMapping['account2KidsTaxId'])){
						$LineTaxId  = $taxMapping['account2KidsLineTaxId'];
						$orderTaxId = $taxMapping['account2KidsTaxId'];
					}
					
					if($productId > 1001){
						if(!$productMappings[$productId]['createdProductId']){
							$missingSkus[]	= $orderRows['productSku'];
							continue;
						}
						$ItemRefValue	= $productMappings[$productId]['createdProductId'];
						$ItemRefName	= $productMappings[$productId]['sku'];
					}
					else{
						if($orderRows['rowValue']['rowNet']['value'] > 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else if($orderRows['rowValue']['rowNet']['value'] < 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else{
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
					}
					
					$itemtaxAbleLine 	= $LineTaxId;
					$price				= $orderRows['rowValue']['rowNet']['value'];
					$orderTaxAmount		+= $orderRows['rowValue']['rowTax']['value'];
					$originalPrice		= $price;
					$discountPercentage	=  0;
					
					if(($orderRows['discountPercentage'] > 0) && (!$config['sendNetPriceExcludeDiscount'])){
						$discountPercentage	= 100 - $orderRows['discountPercentage'];
						if($discountPercentage == 0){
							$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
						}
						else{
							$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
						}
						$tempTaxAmt	= $originalPrice - $price;
						if($tempTaxAmt > 0){
							if(isset($totalItemDiscount[$LineTaxId])){
								$totalItemDiscount[$LineTaxId]	+= $tempTaxAmt;
							}
							else{
								$totalItemDiscount[$LineTaxId]	= $tempTaxAmt;
							}
						}
					}
					else if($isDiscountCouponAdded){
						if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							if($isBundleChild){
								//
							}
							else{
								$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
								}
								if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
									$discountCouponAmtTemp	= ($originalPrice - $price);
									if($discountCouponAmtTemp > 0){
										if(isset($discountCouponAmt[$LineTaxId])){
											$discountCouponAmt[$LineTaxId]	+= $discountCouponAmtTemp;
										}
										else{
											$discountCouponAmt[$LineTaxId]	= $discountCouponAmtTemp;
										}
									}
								}
								else{
									$originalPrice	= $orderRows['rowValue']['rowNet']['value'];
								}
							}
						}
					}
					$IncomeAccountRef	= $config['IncomeAccountRef'];
					
					if((isset($nominalMappings[$orderRows['nominalCode']])) AND ($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
						$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']])) AND ($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'])){
						$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
					}
					
					$UnitAmount	= 0.00;
					if($originalPrice != 0){
						$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'LineNum'				=> $linNumber++,
						'Description'			=> $orderRows['productName'],
						'Amount'				=> sprintf("%.4f",($UnitAmount*($orderRows['quantity']['magnitude']))),
						'DetailType'			=> 'SalesItemLineDetail',
						'SalesItemLineDetail'	=> array(
							'ItemRef'				=> array('value' => $ItemRefValue),
							'Qty'					=> $orderRows['quantity']['magnitude'],
							'UnitPrice'				=> sprintf("%.4f",($UnitAmount)),
							'TaxCodeRef'			=> array('value' => $LineTaxId),
							'ItemAccountRef'		=> array('value'=> $IncomeAccountRef),
						),
					);
					
					$nominalClassID		= '';
					if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]))){
						$nominalClassID	= $nominalClassMapping[strtolower($channelId)][$orderRows['nominalCode']]['account2ClassId'];
					}
					if($nominalClassID){
						$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $nominalClassID);
					}
					
					if(!empty($defaultItemMapping)){
						if(isset($defaultItemMapping[$channelId][$orderRows['nominalCode']])){
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']	= array('value' => $defaultItemMapping[$channelId][$orderRows['nominalCode']]);
						}
					}
					
					
					if($LineTaxId){
						if(isset($LineItemTax[$LineTaxId])){
							$LineItemTax[$LineTaxId]['tax']	+= $orderRows['rowValue']['rowTax']['value'];
							$LineItemTax[$LineTaxId]['net']	+= $orderRows['rowValue']['rowNet']['value'];
						}
						else{
							$LineItemTax[$LineTaxId]['tax']	= $orderRows['rowValue']['rowTax']['value'];
							$LineItemTax[$LineTaxId]['net']	= $orderRows['rowValue']['rowNet']['value'];
						}
					}
					else{
						if($orderRows['rowValue']['rowTax']['value'] > 0){
							if(isset($LineItemTax[$config['orderLineTaxCode']])){
								$LineItemTax[$config['orderLineTaxCode']]['tax']	+= $orderRows['rowValue']['rowTax']['value'];
								$LineItemTax[$config['orderLineTaxCode']]['net']	+= $orderRows['rowValue']['rowNet']['value'];
							}
							else{
								$LineItemTax[$config['orderLineTaxCode']]['tax']	= $orderRows['rowValue']['rowTax']['value'];
								$LineItemTax[$config['orderLineTaxCode']]['net']	= $orderRows['rowValue']['rowNet']['value'];
							}
						}
					}
					
					if(!$LineTaxId){
						if($orderRows['rowValue']['rowTax']['value'] > 0){
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef'] = array('value' => $config['orderLineTaxCode']);
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']	= array('value' => $config['salesNoTaxCode']);
						}
					}
					if($isDDPOrder){
						$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']		= array( 'value' => $config['salesNoTaxCode'] );
					}
					$invoiceLineCount++;
				}
				
				if($totalItemDiscount){
					foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmount){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Item Discount',
							'Amount'				=> - sprintf("%.4f",($totalItemDiscountLineAmount*1)),
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['discountItem']),
								'Qty'					=> 1,
								'UnitPrice'				=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount)),
							),	
						);
						if($TaxID){
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']		= array( 'value' => $TaxID);
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef']		= array( 'value' => $config['salesNoTaxCode']);
						}
						$invoiceLineCount++;
					}
				}
				
				if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					if($discountCouponAmt){
						foreach($discountCouponAmt as $TaxID => $discountCouponLineAmount){
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'LineNum'				=> $linNumber++,
								'Description'			=> 'Coupon Discount',
								'Amount'				=> - sprintf("%.4f",($discountCouponLineAmount*1)),
								'DetailType'			=> 'SalesItemLineDetail',
								'SalesItemLineDetail'	=> array(
									'ItemRef'				=> array('value' => $config['couponItem']),
									'Qty'					=> 1,
									'UnitPrice'				=> sprintf("%.4f",((-1) * $discountCouponLineAmount)),
									'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
								),
							);
							if($TaxID){
								$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef'] = array( 'value' => $TaxID); 
							}
							else{
								$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['TaxCodeRef'] = array( 'value' => $config['salesNoTaxCode']);
							}
							$invoiceLineCount++;
						}
					}
				}
				
				if($config['SendTaxAsLineItem']){
					if($BrightpearlTotalTAX > 0){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Total Tax Amount',
							'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['TaxLineItemCode']),
								'Qty' 					=> 1,
								'UnitPrice' 			=> sprintf("%.4f",$BrightpearlTotalTAX),
								'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
							),	
						);
						$invoiceLineCount++;
					}
				}
			}
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			
			$dueDate	= date('c');
			$taxDate	= date('c');
			if($rowDatas['invoices']['0']['dueDate']){
				$dueDate	= $rowDatas['invoices']['0']['dueDate'];
			}
			if($rowDatas['invoices']['0']['taxDate']){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
			}

			//taxdate chanages
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$QBOoffset		= 0;
			$diff			= $BPDateOffset - $QBOoffset;
			$date1			= new DateTime($dueDate);
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date1->setTimezone(new DateTimeZone($BPTimeZone));
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
				$date1->modify($diff);
			}
			$taxDate		= $date->format('Y-m-d');
			$dueDate		= $date1->format('Y-m-d');
			
			$invoiceRef		= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			if($config['UseAsDocNumbersosc']){
				$account1FieldIds	= explode(".",$config['UseAsDocNumbersosc']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$invoiceRef	= $fieldValueTmps;
				}
				else{
					$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
				}
			}
			if(!$invoiceRef){
				$invoiceRef	= $orderId;
			}
			
			if($orderDatas['uninvoiceCount'] > 0){
				if($clientcode == 'scalesplusqbo'){
					$invoiceRef	= $invoiceRef.'_REV_0'.$orderDatas['uninvoiceCount'];
				}
			}
			
			if($config['disableSkuDetails']){
				$invoiceLineAddNew		= array();
				$invoiceLineFormatted	= array();
				$newItemLineCount		= 0;
				foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['trackedGenericItem']){
						$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['genericSku']){
						$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['Description'] == 'Special Item'){
						$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['discountItem']){
						$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['couponItem']){
						$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['shippingItem']){
						$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['TaxLineItemCode']){
						$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['giftCardItem']){
						$invoiceLineAddNew[8][]	= $InvoiceLineAddTemp;
						continue;
					}
				}
				ksort($invoiceLineAddNew);
				foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
					foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
						$invoiceLineFormatted[$newItemLineCount]			= $invoiceLineAddNewTempTemp;
						$invoiceLineFormatted[$newItemLineCount]['LineNum']	= ($newItemLineCount + 1);
						$newItemLineCount++;
					}
				}
				if($invoiceLineFormatted){
					$InvoiceLineAdd		= $invoiceLineFormatted;
					$invoiceLineCount	= $newItemLineCount;
					$linNumber			= ($newItemLineCount + 1);
				}
			}
			
			//chnageReq added on 8th fab 2023 req by Tushar
			if($clientcode == 'aimeeqbo'){
				foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
					if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
						$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Shipping Charges';
					}
				}
			}
			if($clientcode == 'anatomicalwwqbo'){
				foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
					if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
						$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Products';
					}
				}
			}
			
			foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
				if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
					unset($InvoiceLineAdd[$lineItemIdNewTemp]['Description']);
				}
			}
			
			$lineLevelClassId	= '';
			if((is_array($ChannelLocationMappings)) AND (!empty($ChannelLocationMappings)) AND ($CustomLocationID) AND ($channelId) AND (isset($ChannelLocationMappings[$CustomLocationID][$channelId]))){
				$lineLevelClassId	= $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId'];
			}
			elseif((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
				$lineLevelClassId	= $channelMappings[$channelId]['account2ChannelId'];
			}
			if($lineLevelClassId){
				foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
					if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
						continue;
					}
					else{
						$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
					}
				}
			}
			
			$invoiceRef	= substr($invoiceRef,0,21);
			$request	= array(
				'DocNumber'			=> $invoiceRef,
				'CustomerRef'		=> array('value' => $QBOCustomerID),
				'Line'				=> $InvoiceLineAdd,
				'BillAddr'			=> array(
					'Line1'						=> $billAddress['addressLine1'],
					'Line2'						=> (string)$billAddress['addressLine2'],
					'City'						=> $billAddress['addressLine3'],
					'CountrySubDivisionCode'	=> $billAddress['addressLine4'],
					'Country'					=> $billAddress['countryIsoCode'],
					'PostalCode'				=> $billAddress['postalCode'],
				),
				'ShipAddr'			=> array(
					'Line1'						=> $shipAddress['addressLine1'],
					'Line2'						=> (string)$shipAddress['addressLine2'],
					'City'						=> $shipAddress['addressLine3'],
					'CountrySubDivisionCode'	=> $shipAddress['addressLine4'],
					'Country'					=> $shipAddress['countryIsoCode'],
					'PostalCode'				=> $shipAddress['postalCode'],
				),
				'ExchangeRate'		=> sprintf("%.4f",(1 / $rowDatas['currency']['exchangeRate'])),
				'CurrencyRef'		=> array('value' => $rowDatas['currency']['orderCurrencyCode']),
				'TxnDate'			=> $taxDate,
				'DueDate'			=> $dueDate,
			);
			
			if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$rowDatas['currency']['orderCurrencyCode'],$config['defaultCurrrency'],$taxDate);
				if($exRate){
					$request['ExchangeRate'] = $exRate;
				}
				else{
					$exRate = $exchangeRate[strtolower($rowDatas['currency']['orderCurrencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$request['ExchangeRate'] = $exRate;
					}
					else{
						echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
						unset($request['ExchangeRate']);
					}
				}
			}
			
			if((is_array($ChannelLocationMappings)) AND (!empty($ChannelLocationMappings)) AND ($CustomLocationID) AND ($channelId) AND (isset($ChannelLocationMappings[$CustomLocationID][$channelId]))){
				$request['ClassRef']	= array('value' => $ChannelLocationMappings[$CustomLocationID][$channelId]['account2ChannelId']);
			}
			elseif((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($channelId) AND (isset($channelMappings[$channelId]))){
				$request['ClassRef']	= array('value' => $channelMappings[$channelId]['account2ChannelId']);
			}
			
			
			
			if($enableCustomFieldMappings){
				if($CustomFieldMappings){
					foreach($CustomFieldMappings as $CustomFieldMappingsData){
						$CustomFields		= array();
						$account1FieldIds	= explode(".",$CustomFieldMappingsData['account1CustomField']);
						$fieldValue			= '';
						$fieldValueTmps		= '';
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.staffOwnerContactId'){
							$CustomstaffOwnerContactId	= $rowDatas['assignment']['current']['staffOwnerContactId'];
							$fieldValueTmps				= $getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.projectId'){
							$CustomprojectId			= $rowDatas['assignment']['current']['projectId'];
							$fieldValueTmps				= $getAllProjectsData[$orderDatas['account1Id']][$CustomprojectId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.channelId'){
							$CustomchannelId			= $rowDatas['assignment']['current']['channelId'];
							$fieldValueTmps				= $getAllChannelMethodData[$orderDatas['account1Id']][$CustomchannelId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.leadSourceId'){
							$CustomleadSourceId			= $rowDatas['assignment']['current']['leadSourceId'];
							$fieldValueTmps				= $getAllLeadsourceData[$orderDatas['account1Id']][$CustomleadSourceId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'assignment.current.teamId'){
							$CustomteamId				= $rowDatas['assignment']['current']['teamId'];
							$fieldValueTmps				= $getAllTeamData[$orderDatas['account1Id']][$CustomteamId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'delivery.shippingMethodId'){
							$CustomshippingMethodId		= $rowDatas['delivery']['shippingMethodId'];
							$fieldValueTmps				= $getAllShippingMethodData[$orderDatas['account1Id']][$CustomshippingMethodId]['name'];
						}
						if($CustomFieldMappingsData['account1CustomField'] == 'warehouseId'){
							$CustomwarehouseId			= $rowDatas['warehouseId'];
							$fieldValueTmps				= $getAllWarehouseData[$orderDatas['account1Id']][$CustomwarehouseId]['name'];
						}
						 
						if($fieldValueTmps){
							$allQBOCustomFieldIds	= explode(",",trim($CustomFieldMappingsData['account2CustomField']));
							foreach($allQBOCustomFieldIds as $allQBOCustomFieldIdsTmp){
								if(strlen($allQBOCustomFieldIdsTmp) == 1){
									$CustomFields	= array(
										'DefinitionId'	=> $allQBOCustomFieldIdsTmp,
										'StringValue'	=> (is_array($fieldValueTmps)) ? (substr($fieldValueTmps['value'],0,31)) : (substr($fieldValueTmps,0,31)),
										'Type'			=> 'StringType',
									);
									$request['CustomField'][]	= $CustomFields;
								}
								else{
									if((($clientcode == 'dunejewelryqbo') OR ($clientcode == 'qbodemo')) AND (substr_count($allQBOCustomFieldIdsTmp, '--'))){
										$SalesrepDataEmailArr = array();
										$SalesrepDataEmailArr = explode('--', $allQBOCustomFieldIdsTmp);
										if($SalesrepDataEmailArr[0]){
											$fieldValueTmps				= $getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]['email'];
											
											$CustomFields	= array(
												'DefinitionId'	=> $SalesrepDataEmailArr[0],
												'StringValue'	=> (is_array($fieldValueTmps)) ? (substr($fieldValueTmps['value'],0,31)) : (substr($fieldValueTmps,0,31)),
												'Type'			=> 'StringType',
											);
											$request['CustomField'][]	= $CustomFields;
										}
									}
									else{
										$arrayNumbers	= count(explode(".",$allQBOCustomFieldIdsTmp));
										if($arrayNumbers == 1){
											$request[$allQBOCustomFieldIdsTmp]	= (is_array($fieldValueTmps)) ? ($fieldValueTmps['value']) : ($fieldValueTmps); 
										}
										else{
											$firstKey					= (explode(".",$allQBOCustomFieldIdsTmp))[0];
											$allQBOCustomFieldIdsTmp	= str_replace($firstKey.'.','',$allQBOCustomFieldIdsTmp);
											$finalSentValue				= json_encode((is_array($fieldValueTmps)) ? ($fieldValueTmps['value']) : ($fieldValueTmps));
											$subClose					= '';
											$arrayNumbers			 	= $arrayNumbers - 1;
											for($i = 1; $i <= $arrayNumbers; $i++){
												$subClose	.= '}';
											}
											$request[$firstKey]			= json_decode(('{"'.str_replace(".",'":{"',$allQBOCustomFieldIdsTmp).'":'.$finalSentValue.$subClose), true);
										}
									}
								}
							}
						}
					}
					if(!$request['CustomField']){
						unset($request['CustomField']);
					}
				}
			}
			
			$OrderTaxDetails	= array();
			$orderTotalTax		= 0;
			$TotalTaxLines		= 0;
			
			if($clientcode == 'biscuiteersqbo'){
				if(!$config['SendTaxAsLineItem']){
					if($this->ci->globalConfig['enableAdvanceTaxMapping'] == 0){
						if($LineItemTax){
							foreach($LineItemTax as $QBOTaxID => $LineItemTaxAmt){
								$orderTotalTax	+= $LineItemTaxAmt['tax'];
								$TaxRateID		= '';
								$TaxInfo		= $Acc2AllTaxInfo[$QBOTaxID]['SalesTaxRateList'];
								if($TaxInfo['TaxRateDetail'][0]){
									$TaxRateID	= $TaxInfo['TaxRateDetail'][0]['TaxRateRef']['value'];
								}
								if($TaxRateID){
									$OrderTaxDetails[$TotalTaxLines]	= array(
										"Amount"							=> $LineItemTaxAmt['tax'],
										"DetailType"						=> "TaxLineDetail",
										"TaxLineDetail"						=> array(
											"TaxRateRef"						=> array(
												"value" 							=> $TaxRateID
											),
											"NetAmountTaxable"					=> $LineItemTaxAmt['net'],
										),
									);
									$TotalTaxLines++;
								}
							}
						}
						if($OrderTaxDetails){
							$request['TxnTaxDetail']['TaxLine']			= $OrderTaxDetails;
							if($orderTotalTax){
								$request['TxnTaxDetail']['TotalTax']	= $orderTotalTax;
							}
						}
					}
				}
			}
			else{
				if($this->ci->globalConfig['enableAdvanceTaxMapping'] == 0){
					if($LineItemTax){
						foreach($LineItemTax as $QBOTaxID => $LineItemTaxAmt){
							$orderTotalTax	+= $LineItemTaxAmt['tax'];
							$TaxRateID		= '';
							$TaxInfo		= $Acc2AllTaxInfo[$QBOTaxID]['SalesTaxRateList'];
							if($TaxInfo['TaxRateDetail'][0]){
								$TaxRateID	= $TaxInfo['TaxRateDetail'][0]['TaxRateRef']['value'];
							}
							if($TaxRateID){
								$OrderTaxDetails[$TotalTaxLines]	= array(
									"Amount"							=> $LineItemTaxAmt['tax'],
									"DetailType"						=> "TaxLineDetail",
									"TaxLineDetail"						=> array(
										"TaxRateRef"						=> array(
											"value" 							=> $TaxRateID
										),
										"NetAmountTaxable"					=> $LineItemTaxAmt['net'],
									),
								);
								$TotalTaxLines++;
							}
						}
					}
					if($OrderTaxDetails){
						$request['TxnTaxDetail']['TaxLine']			= $OrderTaxDetails;
						if($orderTotalTax){
							$request['TxnTaxDetail']['TotalTax']	= $orderTotalTax;
						}
					}
				}
			}
			
			$qboTermsId	= '';
			if($this->ci->globalConfig['enablePaymenttermsMapping']){
				$SalesTermsFiledName	= $bpconfig['SalesTermsFiledName'];
				$paymentTermsRowData	= $rowDatas['customFields'][$SalesTermsFiledName];
				if($paymentTermsRowData){
					if($paymentTermsMappings[$paymentTermsRowData['id']]){
						if($paymentTermsMappings[$paymentTermsRowData['id']]['account2TermId']){
							$qboTermsId	= $paymentTermsMappings[$paymentTermsRowData['id']]['account2TermId'];
						}
					}
				}
			}
			if($qboTermsId){
				unset($request['DueDate']);
				$request['SalesTermRef']	= array('value' => $qboTermsId);
			}
			
			if(($clientcode == 'nitrossnowbqbm') OR ($clientcode == 'vitalbodyqbo') OR ($clientcode == 'anatomicalwwqbo') OR ($clientcode == 'pillowpetsqbo')){
				if($isGenericContact == 0){
					$request['BillEmail']	= array('Address' => $rowDatas['parties']['customer']['email']);
				}
			} 
			
			if(($clientcode == 'growershouseqbo') OR ($clientcode == 'qbodemo') OR ($clientcode == 'dorwestqbo')  OR ($clientcode == 'dunejewelryqbo')){
				$request['BillEmail']	= array('Address' => $rowDatas['parties']['billing']['email']); 
			}
			
			if($clientcode == 'doubleqbo'){
				unset($request['TxnTaxDetail']);
			}
			
			if(($clientcode == 'pillowpetsqbo')){
				$allGoodsOutNotesurl	= '/warehouse-service/order/'.$orderId.'/goods-note/goods-out/'; 
				$allGoodsOutNotesData	= $this->ci->brightpearl->getCurl($allGoodsOutNotesurl, 'GET', '', 'json', $orderDatas['account1Id'])[$orderDatas['account1Id']];
				if(!empty($allGoodsOutNotesData)){
					foreach($allGoodsOutNotesData as $allGoodsOutNotesDataTemp){
						if(isset($allGoodsOutNotesDataTemp['status']['shippedOn'])){
							$request['ShipDate']				= date('Y-m-d', strtotime($allGoodsOutNotesDataTemp['status']['shippedOn']));
						}
						if(isset($allGoodsOutNotesDataTemp['shipping']['reference'])){
							$request['TrackingNum']				= $allGoodsOutNotesDataTemp['shipping']['reference'];
							$request['TrackingNum']				= substr($request['TrackingNum'],0,31);
						}
						break;
					};
				}
			}
			
			if(($clientcode == 'officesigncoqbo')){
				if($isGenericContact == 0){
					$request['BillEmail']	= array('Address' => $rowDatas['parties']['billing']['email']);
				}
				$allGoodsOutNotesurl	= '/warehouse-service/order/'.$orderId.'/goods-note/goods-out/'; 
				$allGoodsOutNotesData	= $this->ci->brightpearl->getCurl($allGoodsOutNotesurl, 'GET', '', 'json', $orderDatas['account1Id'])[$orderDatas['account1Id']];
				if(!empty($allGoodsOutNotesData)){
					foreach($allGoodsOutNotesData as $allGoodsOutNotesDataTemp){
						if(isset($allGoodsOutNotesDataTemp['shipping']['shippingMethodId'])){
							$request['ShipMethodRef']['value']	= $allGoodsOutNotesDataTemp['shipping']['shippingMethodId'];
						}
						if(isset($allGoodsOutNotesDataTemp['status']['shippedOn'])){
							$request['ShipDate']				= date('Y-m-d', strtotime($allGoodsOutNotesDataTemp['status']['shippedOn']));
						}
						if(isset($allGoodsOutNotesDataTemp['shipping']['reference'])){
							$request['TrackingNum']				= $allGoodsOutNotesDataTemp['shipping']['reference'];
						}
						break;
					};
				}
				if(isset($request['ShipMethodRef']['value']) AND (strlen($request['ShipMethodRef']['value']) > 0)){
					if(isset($allBPShippingMethods[$orderDatas['account1Id']][$request['ShipMethodRef']['value']])){
						$request['ShipMethodRef']['value']	= $allBPShippingMethods[$orderDatas['account1Id']][$request['ShipMethodRef']['value']]['name'];
					}
					else{
						unset($request['ShipMethodRef']);
					}
				}
				if(isset($request['ShipMethodRef']['value']) AND (strlen($request['ShipMethodRef']['value']) > 0)){
					$request['ShipMethodRef']['value']	= substr($request['ShipMethodRef']['value'],0,31);
				}
				
				$shipLine1	= '';
				$isset		= 0;
				if(strlen($rowDatas['parties']['delivery']['addressFullName']) > 0){
					if($isset){
						$shipLine1	= $shipLine1.', '.$rowDatas['parties']['delivery']['addressFullName'];
					}
					else{
						$shipLine1	= $shipLine1.' '.$rowDatas['parties']['delivery']['addressFullName'];
					}
					$shipLine1	= trim($shipLine1);
					$isset		= 1;
				}
				if(strlen($rowDatas['parties']['delivery']['companyName']) > 0){
					if($isset){
						$shipLine1	= $shipLine1.', '.$rowDatas['parties']['delivery']['companyName'];
					}
					else{
						$shipLine1	= $shipLine1.' '.$rowDatas['parties']['delivery']['companyName'];
					}
					$shipLine1	= trim($shipLine1);
					$isset		= 1;
				}
				if(strlen($rowDatas['parties']['delivery']['addressLine1']) > 0){
					if($isset){
						$shipLine1	= $shipLine1.', '.$rowDatas['parties']['delivery']['addressLine1'];
					}
					else{
						$shipLine1	= $shipLine1.' '.$rowDatas['parties']['delivery']['addressLine1'];
					}
					$shipLine1	= trim($shipLine1);
					$isset		= 1;
				}
				if(strlen($shipLine1) > 0){
					$request['ShipAddr']['Line1']	= $shipLine1;
				}
				
				$billLine1	= '';
				$isset		= 0;
				if(strlen($rowDatas['parties']['billing']['addressFullName']) > 0){
					if($isset){
						$billLine1	= $billLine1.', '.$rowDatas['parties']['billing']['addressFullName'];
					}
					else{
						$billLine1	= $billLine1.' '.$rowDatas['parties']['billing']['addressFullName'];
					}
					$billLine1	= trim($billLine1);
					$isset		= 1;
				}
				if(strlen($rowDatas['parties']['billing']['companyName']) > 0){
					if($isset){
						$billLine1	= $billLine1.', '.$rowDatas['parties']['billing']['companyName'];
					}
					else{
						$billLine1	= $billLine1.' '.$rowDatas['parties']['billing']['companyName'];
					}
					$billLine1	= trim($billLine1);
					$isset		= 1;
				}
				if(strlen($rowDatas['parties']['billing']['addressLine1']) > 0){
					if($isset){
						$billLine1	= $billLine1.', '.$rowDatas['parties']['billing']['addressLine1'];
					}
					else{
						$billLine1	= $billLine1.' '.$rowDatas['parties']['billing']['addressLine1'];
					}
					$billLine1	= trim($billLine1);
					$isset		= 1;
				}
				if(strlen($billLine1) > 0){
					$request['BillAddr']['Line1']	= $billLine1;
				}
			}
			
			
			if(($clientcode == 'dunejewelryqbo') OR ($clientcode == 'qbodemo')){
				$allGoodsOutNotesurl	= '/warehouse-service/order/'.$orderId.'/goods-note/goods-out/'; 
				$allGoodsOutNotesData	= $this->ci->brightpearl->getCurl($allGoodsOutNotesurl, 'GET', '', 'json', $orderDatas['account1Id'])[$orderDatas['account1Id']];
				if(!empty($allGoodsOutNotesData)){
					foreach($allGoodsOutNotesData as $allGoodsOutNotesDataTemp){
						if(isset($allGoodsOutNotesDataTemp['shipping']['shippingMethodId'])){
							$request['ShipMethodRef']['value']	= $allGoodsOutNotesDataTemp['shipping']['shippingMethodId'];
						}
						if(isset($allGoodsOutNotesDataTemp['status']['shippedOn'])){
							$request['ShipDate']				= date('Y-m-d', strtotime($allGoodsOutNotesDataTemp['status']['shippedOn']));
						}
						if(isset($allGoodsOutNotesDataTemp['shipping']['reference'])){
							$request['TrackingNum']				= $allGoodsOutNotesDataTemp['shipping']['reference'];
						}
						break;
					};
				}
				if(isset($request['ShipMethodRef']['value']) AND (strlen($request['ShipMethodRef']['value']) > 0)){
					if(isset($allBPShippingMethods[$orderDatas['account1Id']][$request['ShipMethodRef']['value']])){
						$request['ShipMethodRef']['value']	= $allBPShippingMethods[$orderDatas['account1Id']][$request['ShipMethodRef']['value']]['name'];
					}
					else{
						unset($request['ShipMethodRef']);
					}
				}
				if(isset($request['ShipMethodRef']['value']) AND (strlen($request['ShipMethodRef']['value']) > 0)){
					$request['ShipMethodRef']['value']	= substr($request['ShipMethodRef']['value'],0,31);
				}
				$termcustomerId = '';
				$termsId	= '';
				if($termsMapping){
					$termcustomerId = $rowDatas['parties']['customer']['contactId'];
					if($termcustomerId){
						$customerRowDatasTemps	= $this->ci->db->select('params')->get_where('customers',array('customerId'=>$termcustomerId,'account2Id' => $account2Id))->row_array();
						$customerRowDatas = json_decode($customerRowDatasTemps['params'], true);
						if(isset($customerRowDatas['financialDetails']['creditTermDays']) AND ($customerRowDatas['financialDetails']['creditTermDays'] !='')){
							if(isset($termsMapping[$customerRowDatas['financialDetails']['creditTermDays']])){
								$termsId	= $termsMapping[$customerRowDatas['financialDetails']['creditTermDays']]['account2TermsId'];
							}
						}
					}
					if($termsId){
						$request['SalesTermRef']['value']	= $termsId;
					}
				}
			}
			
			// added by dean to send reference in ship via field in qbo
			if($clientcode == 'pillowpetsqbo'){
				$request['ShipMethodRef']['value']	= $rowDatas['reference'];
			}
			// end of adding dean
			
			if($clientcode == 'dunejewelryqbo'){
				$CustomstaffOwnerContactId		= $rowDatas['assignment']['current']['staffOwnerContactId'];
				if($CustomstaffOwnerContactId AND (isset($getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]))){
					$CustomstaffOwnerContactEmail	= $getAllSalesrepData[$orderDatas['account1Id']][$CustomstaffOwnerContactId]['email'];
					if($CustomstaffOwnerContactEmail){
						$request['CustomerMemo']['value']	= $CustomstaffOwnerContactEmail;
					}
				}
			}
			
			$url				= 'invoice?minorversion=37';
			$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData['Request data	: ']	= $request;
			$createdRowData['Response data	: ']	= $results;
			
			$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((isset($results['Invoice']['Id']))){
				$this->ci->db->update('sales_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['Invoice']['Id'],'message' => '', 'PostedTime' => date('c'), 'qboContactID' => $QBOCustomerID),array('id' => $orderDatas['id']));
				
				$QBOTotalAmount			= $results['Invoice']['TotalAmt'];
				$NetRoundOff			= $BrightpearlTotalAmount - $QBOTotalAmount;
				$RoundOffCheck			= abs($NetRoundOff);
				$RoundOffApplicable		= 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.99){
						$RoundOffApplicable = 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'RoundOffItem',
							'Amount'				=> sprintf("%.4f",$NetRoundOff),
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['roundOffItem']),
								'Qty'					=> 1,
								'UnitPrice'				=> sprintf("%.4f",$NetRoundOff),
								'TaxCodeRef'			=> array(
									'value'					=> $config['salesNoTaxCode'] 
								),
							),
						);
						$request['Line']		= $InvoiceLineAdd;
						$request['SyncToken']	= 0;
						$request['Id']			= $results['Invoice']['Id']; 
						$url					= 'invoice?minorversion=37';  
						$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Rounding Request data	: ']	= $request;
						$createdRowData['Rounding Response data	: ']	= $results2;
						$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						if($results2['Invoice']['Id']){
							$this->ci->db->update('sales_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results2['Invoice']['Id'],'message' => ''),array('id' => $orderDatas['id']));
						}
					}
				}
			}
		}
	}
}
$this->postJournal($orgObjectId);
$this->postSalesPayment($orgObjectId);	
$this->fetchSalesPayment();
?>