<?php
//qbo_demo
//qbo : post salesCreditPayment from BP to qbo
$this->reInitialize();
$exchangeRates				= $this->getExchangeRate();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$disableSCpaymentbptoqbo	= $this->ci->globalConfig['disableSCpaymentbptoqbo'];
$PaymentReversalEnabled		= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($disableSCpaymentbptoqbo){continue;}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->order_by('id', 'desc')->get_where('sales_credit_order',array('isNetOff' => 0, 'createOrderId <>' => '','paymentDetails <> ' => '', 'status < ' => '4', 'account2Id' => $account2Id))->result_array();
	if(!$datas){continue;}
	
	$config			= $this->accountConfig[$account2Id];
	$exchangeRate	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	/* $paymentMappingsTemps	= $this->ci->db->get_where('mapping_paymentpurchase',array('account2Id' => $account2Id))->result_array();
	$paymentMappings		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
		}
	} */
	
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	$currencyMappingsTemps	= $this->ci->db->get_where('mapping_currency',array('account2Id' => $account2Id))->result_array();
	$currencyMappings		= array();
	if($currencyMappingsTemps){
		foreach($currencyMappingsTemps as $currencyMappingsTemp){
			$currencyMappings[strtolower($currencyMappingsTemp['account1CurrencyId'])]	= $currencyMappingsTemp;
		}
	}
	
	$AggregationMappings	= array();
	$AggregationMappings2	= array();
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
					}
				}
			}
		}
	}
	
	if($datas){
		$journalIds	= array();
		foreach($datas as $orderDatas){
			if(!$orderDatas['createOrderId']){
				continue;
			}
			else{
				$paymentDetails	= array();
				$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
				if($paymentDetails){
					foreach($paymentDetails as $paymentKey => $paymentDetail){
						if($paymentDetail['sendPaymentTo'] == 'qbo'){
							if($paymentDetail['status'] == '0'){
								if($paymentKey){
									$journalIds[]	= $paymentDetail['journalId'];
								}
							}
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);
		if($journalIds){
			$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		}
		
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['createOrderId']){continue;}
			
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
			$sendInAggregation		= $orderDatas['sendInAggregation'];
			$ConsolAPIFieldValueID	= '';
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			if(!$paymentDetails){
				continue;
			}
			
			/* if($clientcode == 'coloradokayak'){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
				$taxDate	= date('Ymd',strtotime($taxDate));
				if($taxDate < '20210401'){
					continue;
				}
			} */
			
			$invoiceDate	= $rowDatas['invoices']['0']['taxDate'];
			$BPDateOffset	= (int)substr($invoiceDate,23,3);
			$QBOOffset		= 0;
			$diff			= 0;
			$diff			= $BPDateOffset - $QBOOffset;
			$date			= new DateTime($invoiceDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff			.= ' hour';
				$date->modify($diff);
			}
			$invoiceDate	= $date->format('Y-m-d');
			
			if($enableAggregation){
				if($sendInAggregation){
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
							continue;
						}
						if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['disablePayments']){
							continue;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['disablePayments']){
							continue;
						}
					}
				}
			}
			
			if(($orderDatas['uninvoiced'] == 1) AND ($orderDatas['paymentDetails'])){
				continue;
			}
			
			
			$account1Id					= $orderDatas['account1Id'];
			$config1					= $this->ci->account1Config[$account1Id];
			$giftCardPayment			= $config1['giftCardPayment'];
			$QBOorderId					= $orderDatas['createOrderId'];
			$createdRowData				= json_decode($orderDatas['createdRowData'],true);
			$paymentDetails				= json_decode($orderDatas['paymentDetails'],true);
			$CurrencyRate				= 1;
			$DepositToAccountRef		= $config['DepositToAccountRef'];
			$amount						= 0;
			$paidAmount					= 0;
			$giftAmount					= 0;
			$totalReceivedPaidAmount	= 0;
			$paymentMethod				= $orderDatas['paymentMethod'];
			$qboCustomerId				= $createdRowData['Response data	: ']['CreditMemo']['CustomerRef']['value'];
			$QBOTotalAmount				= $createdRowData['Response data	: ']['CreditMemo']['TotalAmt'];
			$sentPayments				= array();
			$ReversePayments			= array();
			$adjustmentspayments		= array();
			$negativepayments			= array();
			$applicablepayment			= array();
			$adjustmentarray			= array();
			
			if(!$qboCustomerId){
				$readurl				= 'creditmemo/'.$QBOorderId.'?minorversion=41';
				$InvoiceResponse		= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
				if(isset($InvoiceResponse['CreditMemo'])){
					$QBOTotalAmount		= $InvoiceResponse['CreditMemo']['TotalAmt'];
					$qboCustomerId		= $InvoiceResponse['CreditMemo']['CustomerRef']['value'];
				}
			}
			if(!$qboCustomerId){
				continue;
			}
			
			if($currencyMappings[strtolower($orderCurrencyCode)]['account2DepositAccId']){
				$DepositToAccountRef	= $currencyMappings[strtolower($orderCurrencyCode)]['account2DepositAccId'];
			}
			
			
			/* Payment Reversal code starts (Code to void the expense document in qbo)*/
			if($PaymentReversalEnabled){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['status'] == '1'){
						if(strtolower($paymentDetail['paymentInitiatedIn']) == 'bp'){
							if(strtolower($paymentDetail['paymentType']) == 'payment'){
								$sentPayments[$paymentKey]		= $paymentDetail;
							}
						}
					}
					if($paymentDetail['status'] == '0'){
						if(strtolower($paymentDetail['paymentInitiatedIn']) == 'bp'){
							if(strtolower($paymentDetail['paymentType']) == 'receipt'){
								if(strtolower($paymentDetail['paymentMethod']) == 'adjustment'){
									$adjustmentspayments[$paymentKey]	= $paymentDetail;
								}
								else{
									$ReversePayments[$paymentKey]		= $paymentDetail;
								}
							}
							elseif((strtolower($paymentDetail['paymentType']) == 'payment') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
								$applicablepayment[$paymentKey]			= $paymentDetail;
							}
						}
					}
				}
				if($ReversePayments){
					$updateArray	= array();
					$error_reported	= 0;
					foreach($ReversePayments as $rKey => $ReversePaymentss){
						if($sendInAggregation){
							continue;
						}
						foreach($sentPayments as $sKey => $sentPaymentss){
							if(($sentPaymentss['sendPaymentTo'] == 'qbo') AND ($sentPaymentss['status'] == '1')){
								if(($sentPaymentss['paymentMethod'] == $ReversePaymentss['paymentMethod']) AND ((float)($sentPaymentss['amount']+$ReversePaymentss['amount']) == 0)){
									if($sentPaymentss['qboexpenseid']){
										$qboVoidExpenseID	= $sentPaymentss['qboexpenseid'];
										$SyncToken			= '';
										$readurl			= 'purchase/'.$qboVoidExpenseID.'?minorversion=41';
										$purchaseResponse	= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
										if($purchaseResponse['Purchase']){
											$SyncToken	= $purchaseResponse['Purchase']['SyncToken'];
										}
										$VoidExpenserequest	= array(
											"SyncToken"		=>	$SyncToken,
											"Id"			=>	$qboVoidExpenseID,
										);
										$voidurl			= 'purchase?operation=delete&minorversion=41';
										$voidresults		= $this->getCurl($voidurl, 'POST', json_encode($VoidExpenserequest), 'json', $account2Id)[$account2Id];
										if($voidresults['Purchase']['status']){
											$paymentDetails[$rKey]['status']		= '1';
											$paymentDetails[$rKey]['PaymentDelete']	= 'yes';
											$paymentDetails[$rKey]['reverseby']		= 'bp';
											unset($sentPaymentss[$sKey]);
											unset($ReversePayments[$rKey]);
										}
										else{
											$error_reported	= 1;
											continue;
										}
										break;
									}
								}
							}
						}
					}
					if($error_reported == 0){
						$updateArray['paymentDetails']	= json_encode($paymentDetails);
						$updateArray['status']			= 1;
						$this->ci->db->where(array('orderId' => $orderId))->update('sales_credit_order',$updateArray);
					}
					$paymentDetails		= array();
					$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
					$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true);
				}
				/* Payment Reversal code ends (Code to void the expense document in qbo)*/
			}
			
			foreach($paymentDetails as $paymentKey => $paymentDetail){
				if($paymentDetail['sendPaymentTo'] == 'qbo'){
					if($paymentDetail['status'] == '0'){
						if($paymentDetail['amount'] > 0){
							if($paymentDetail['paymentMethod'] == $giftCardPayment){
								$giftAmount					+= $paymentDetail['amount'];
								$totalReceivedPaidAmount	+= $paymentDetail['amount'];
								continue;
							}
						}
						else{
							$totalReceivedPaidAmount	+= $paymentDetail['amount'];
						}
					}
				}
			}
			if($giftAmount){
				if($sendInAggregation){
					continue;
				}
				$query			= "select * from creditmemo where id = '".$orderDatas['createOrderId']."'";
				$url			= "query?minorversion=4&query=".rawurlencode($query);
				$qboOrderInfos	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
				$qboLineNumber	= 1;
				foreach($qboOrderInfos['QueryResponse']['CreditMemo']['0']['Line'] as $key => $qbLine){
					if($qbLine['DetailType'] == 'SubTotalLineDetail'){
						unset($qboOrderInfos['QueryResponse']['CreditMemo']['0']['Line'][$key]);
					}
					if(@$qbLine['LineNum'] > $qboLineNumber){
						$qboLineNumber	= $qbLine['LineNum'];
					}
				}
				$qboLineNumber++;
				$qboOrderInfos['QueryResponse']['CreditMemo']['0']['Line'][count($qboOrderInfos['QueryResponse']['CreditMemo']['0']['Line'])]	= array(
					'LineNum'				=> count($qboOrderInfos['QueryResponse']['CreditMemo']['0']['Line']) + 1,
					'Amount'				=> (-1) * $giftAmount,
					'DetailType'			=> 'SalesItemLineDetail',
					'SalesItemLineDetail'	=> array(
						'ItemRef'				=> array('value' => $config['giftCardItem']),
						'UnitPrice'				=> (-1) * $giftAmount,
						'Qty'					=> '1',
						'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
					),
				);
				$rqInvoice	= $qboOrderInfos['QueryResponse']['CreditMemo']['0'];
				$request	= array(
					'Id'			=> $rqInvoice['Id'],
					'DocNumber'		=> $rqInvoice['DocNumber'],
					'SyncToken'		=> $rqInvoice['SyncToken'],
					'domain'		=> $rqInvoice['domain'],
					'TxnDate'		=> $rqInvoice['TxnDate'],
					'DueDate'		=> $rqInvoice['DueDate'],
					'CustomerRef'	=> array(
						'value'			=> $rqInvoice['CustomerRef']['value']
					),
					'BillAddr'		=> $rqInvoice['BillAddr'],
					'ShipAddr'		=> $rqInvoice['ShipAddr'],
					'Line'			=> $rqInvoice['Line'],
					'ExchangeRate'	=> $rqInvoice['ExchangeRate'],
					'CurrencyRef'	=> $rqInvoice['CurrencyRef'],
					'ClassRef'		=> $rqInvoice['ClassRef'],
					/* 'TxnTaxDetail'	=> $rqInvoice['TxnTaxDetail'],  */
				);
				if($config1['currencyCode'] != $config['defaultCurrrency']){
					unset($request['ExchangeRate']);
				}
				$url			= 'creditmemo';
				$updateResults	= @$this->getCurl($url, 'POST', json_encode($request), 'json',$account2Id)[$account2Id]; 
				$createdRowData['QBO Gift Card Request Data		: ']	= $request;
				$createdRowData['QBO Gift Card Response Data	: ']	= $updateResults;
				$this->ci->db->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
				if((isset($updateResults['CreditMemo']['Id']))){
					$updateArray	= array();
					foreach($paymentDetails as $key => $paymentDetail){
						if($paymentDetail['sendPaymentTo'] == 'qbo'){
							if($paymentDetail['paymentMethod'] == $giftCardPayment){
								$paymentDetails[$key]['status']	= '1';
							}
						}
					}
					if(((int)$updateResults['CreditMemo']['RemainingCredit']) == 0){
						$updateArray	= array(
							'isPaymentCreated'	=> '1',
							'status'			=> '3',
						);
					}
					$updateArray['paymentDetails']	= json_encode($paymentDetails);
					$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_credit_order',$updateArray); 
					$orderDatas	= $this->ci->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
				}
			}
			$orderId		= $orderDatas['orderId'];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$BPcustomerRef	= $rowDatas['reference'];
			$BPInvoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			$amount			= 0;
			$paidAmount		= 0;
			$giftAmount		= 0;
			$paymentMethod	= $orderDatas['paymentMethod'];
			$EntityRefType	= 'Customer';
			$totalReceivedPaidAmount	= 0;
			
			/* new sc payment functionality */
			foreach($paymentDetails as $paymentKey => $paymentDetail){
				if($paymentDetail['sendPaymentTo'] == 'qbo'){
					if($paymentDetail['paymentMethod'] != $giftCardPayment){
						$totalReceivedPaidAmount	+= $paymentDetail['amount'];
					}
					if($paymentDetail['status'] == '0'){
						if($paymentDetail['amount'] > 0){
							$OrderPayemntCurrency		= $paymentDetail['currency'];
							if($paymentDetail['paymentMethod'] != $giftCardPayment){
								$purchaseRequest	= array();
								$DocNumber			= $paymentDetail['Desc'];
								$amount				= $paymentDetail['amount'];
								$accountref			= $paymentDetail['paymentMethod'];
								/* if($paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId']){
									$accountref	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
								} */
								
								if(isset($paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
									$accountref	= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
								}
								
								else if(isset($paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])])){
									$accountref	= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
								}
								
								else if(isset($paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
									$accountref	= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
								}
								
								else if(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
									$accountref	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
								}
								
								$TxnDate		= $paymentDetail['paymentDate'];
								if($TxnDate){
									$BPDateOffset	= (int)substr($TxnDate,23,3);
									$QBOoffset		= 0;
									$diff			= $BPDateOffset - $QBOoffset;
									$date			= new DateTime($TxnDate);
									$BPTimeZone		= 'GMT';
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($diff){
										$diff			.= ' hour';
										$date->modify($diff);
									}
									$TxnDate	= $date->format('Y-m-d');
								}
								else{
									$TxnDate	= date('Y-m-d');
								}
								
								if($clientcode == 'coloradokayak'){
									$CheckPaymentDate	= date('Ymd',strtotime($TxnDate));
									if($CheckPaymentDate < '20230201'){
										continue;
									}
								}
								if(($clientcode == 'aspectledqbo') OR ($clientcode == 'teeturtleqbo')){
									$TxnDate	= $invoiceDate;
								}
								
								$foundJournalData	= 0;
								if($paymentDetail['journalId']){
									if(isset($journalDatas[$paymentDetail['journalId']])){
										$CurrencyRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
										$foundJournalData	= 1;
									}
								}
								if(!$foundJournalData){
									if(isset($journalDatas[$paymentKey])){
										$CurrencyRate	= $journalDatas[$paymentKey]['exchangeRate'];
									}
								}
								
								$memo	= '';
								if($config['memoOnSOSCPayments']){
									if($config['memoOnSOSCPayments'] == 'Transactionref'){
										$memo	= $paymentDetail['Reference'];
									}
									if($config['memoOnSOSCPayments'] == 'Reference'){
										if(isset($journalDatas[$paymentDetail['journalId']])){
											$memo	= $journalDatas[$paymentDetail['journalId']]['description'];
										}
									}
									if(($config['memoOnSOSCPayments'] != 'Transactionref') AND ($config['memoOnSOSCPayments'] != 'Reference')){
										$account1FieldIdsTemp	= explode(".",$config['memoOnSOSCPayments']);
										$fieldValueTmpsTemp		= '';
										foreach($account1FieldIdsTemp as $account1FieldIdTemp){
											if(!$fieldValueTmpsTemp){
												$fieldValueTmpsTemp	= $rowDatas[$account1FieldIdTemp];
											}
											else{
												$fieldValueTmpsTemp = $fieldValueTmpsTemp[$account1FieldIdTemp];
											}
										}
										if($fieldValueTmpsTemp){
											$memo	= $fieldValueTmpsTemp;
										}
									}
								}
								
								$CurrencyRef	= $paymentDetail['currency'];
								$PaymentType	= $config['DefaultSCPaymentType'];
								$PurachaseLine	= array(
									array(
										"Id"							=> "1",
										"Description"					=> 'Credit Note '.$orderDatas['invoiceRef'].' Payment',
										"Amount"						=> $amount,
										"DetailType"					=> "AccountBasedExpenseLineDetail",
										"AccountBasedExpenseLineDetail"	=> array(
											"CustomerRef"					=> array("value" => $qboCustomerId),
											"AccountRef"					=> array("value" => $DepositToAccountRef),
											"TaxCodeRef"					=> array("value" => $config['salesNoTaxCode']),
										),
									),
								);
								$purchaseRequest	= array(
									"DocNumber"			=> $orderId,
									"AccountRef"		=> array("value" => $accountref),
									"PaymentType"		=> $PaymentType,
									"EntityRef"			=> array("value" => $qboCustomerId,"type" => $EntityRefType),
									"TotalAmt"			=> $amount,
									"TxnDate"			=> $TxnDate,
									"CurrencyRef"		=> array("value" => $CurrencyRef),
									'ExchangeRate'		=> sprintf("%.4f",(1 / $CurrencyRate)),
									"Line"				=> $PurachaseLine,
								);
								$SCpaymentDocnum = $rowDatas['invoices']['0']['invoiceReference'];
								if($config['UseAsDocNumbersosc']){
									$account1FieldIds	= explode(".",$config['UseAsDocNumbersosc']);
									$fieldValueTmps		= '';
									foreach($account1FieldIds as $account1FieldId){
										if(!$fieldValueTmps){
											$fieldValueTmps	= $rowDatas[$account1FieldId];
										}
										else{
											$fieldValueTmps = $fieldValueTmps[$account1FieldId];
										}
									}
									if($fieldValueTmps){
										$SCpaymentDocnum	= $fieldValueTmps;
									}
									else{
										$SCpaymentDocnum	= $rowDatas['invoices']['0']['invoiceReference'];
									}
								}
								if($SCpaymentDocnum){
									$purchaseRequest['DocNumber']	= $SCpaymentDocnum;
								}
								if($clientcode == 'manitobahmuklukscadqbo' || $clientcode == 'manitobahmukluksusqbo'){
									$purchaseRequest['DocNumber']	= $BPInvoiceRef;
								}
								if($clientcode == 'rivetandhideqbo'){
									$purchaseRequest['DocNumber']	= $BPcustomerRef;
									if(!$purchaseRequest['DocNumber']){
										$purchaseRequest['DocNumber']	= $orderId;
									}
								}
								if($memo){
									$purchaseRequest['PrivateNote']	= substr($memo,0,4000);
								}
								if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
									$exRate = $this->getQboExchangeRateByDb($account2Id,$paymentDetail['currency'],$config['defaultCurrrency'],$TxnDate);
									if($exRate){
										$purchaseRequest['ExchangeRate'] = $exRate;
									}
									else{
										$exRate	= $exchangeRate[strtolower($paymentDetail['currency'])][strtolower($config['defaultCurrrency'])]['Rate'];
										if($exRate){
											$purchaseRequest['ExchangeRate'] = $exRate;
										}
										else{
											echo 'ExchangeRate Not found Line no - 511';continue;
											unset($purchaseRequest['ExchangeRate']);
										}
									}
								}
								if(!$purchaseRequest['ExchangeRate']){
									unset($purchaseRequest['ExchangeRate']);
								}
								if($purchaseRequest['PaymentType'] == 'Cheque'){
									$purchaseRequest['PaymentType']	= 'Check';
								}
								if($purchaseRequest){
									$url				= 'purchase?minorversion=45';
									$Purchaseresults	= $this->getCurl($url, 'POST', json_encode($purchaseRequest), 'json', $account2Id)[$account2Id];
									$createdRowData['QBO Payment Request Data	: '.$paymentKey]	= $purchaseRequest;
									$createdRowData['QBO Payment Response Data	: '.$paymentKey]	= $Purchaseresults;
									$this->ci->db->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
									if((isset($Purchaseresults['Purchase']['Id']))){
										$updateArray	= array();
										$paymentDetails[$paymentKey]['status']				= '1';
										$paymentDetails[$paymentKey]['qboexpenseid']		= $Purchaseresults['Purchase']['Id'];
										$paymentDetails[$Purchaseresults['Purchase']['Id']]	= $Purchaseresults;
										$CreditMemoQuery		= "select * from creditmemo where id = '".$orderDatas['createOrderId']."'";
										$CreditMemoQueryUrl		= "query?minorversion=4&query=".rawurlencode($CreditMemoQuery);
										$qboCreditMemoQueryRes	= @$this->getCurl($CreditMemoQueryUrl, 'GET', '', 'json',$account2Id)[$account2Id];
										if($totalReceivedPaidAmount >= $qboCreditMemoQueryRes['QueryResponse']['CreditMemo'][0]['TotalAmt']){
											$updateArray	= array(
												'isPaymentCreated'	=> '1',
												'status'			=> '3',
											);
										}
										$updateArray['paymentDetails']	= json_encode($paymentDetails);
										$this->ci->db->where(array('orderId' => $orderId))->update('sales_credit_order',$updateArray); 
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
?>