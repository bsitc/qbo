<?php
//qbo_demo
//qbo : post salesPayment from BP to qbo
$this->reInitialize();
$exchangeRates				= $this->getExchangeRate();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$disableSOpaymentbptoqbo	= $this->ci->globalConfig['disableSOpaymentbptoqbo'];
$PaymentReversalEnabled		= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($disableSOpaymentbptoqbo){continue;}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->order_by('id', 'desc')->get_where('sales_order',array('isNetOff' => 0, 'createOrderId <> ' => '','status < ' => '4' , 'paymentDetails <>' => '', 'account2Id' => $account2Id))->result_array();
	if(!$datas){continue;}
	
	$config			= $this->accountConfig[$account2Id];
	$exchangeRate	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$allPendingFees		= array();
	$allSentFees		= array();
	$journalDatasTemps	= $this->ci->db->select('orderId,journalsId,status')->get_where('amazon_ledger',array('account2Id' => $account2Id))->result_array();
	if($journalDatasTemps){
		foreach($journalDatasTemps as $journalDatasTemp){
			if($journalDatasTemp['status'] == 0){
				$allPendingFees[$journalDatasTemp['orderId']]	= $journalDatasTemp;
			}
			elseif($journalDatasTemp['status'] == 1){
				$allSentFees[$journalDatasTemp['orderId']]		= $journalDatasTemp;
			}
		}
	}
	$AmazonFeesChannels	= array();
	if($config['AmazonFeesChannels']){
		$AmazonFeesChannels	= explode(",",$config['AmazonFeesChannels']);
	}
	
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	$AggregationMappings	= array();
	$AggregationMappings2	= array();
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
					}
				}
			}
		}
	}
	
	if($datas){
		$journalIds		= array();
		foreach($datas as $orderDatas){
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			else{
				$paymentDetails	= array();
				$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
				if($paymentDetails){
					foreach($paymentDetails as $paymentKey => $paymentDetail){
						if($paymentDetail['sendPaymentTo'] == 'qbo'){
							if($paymentDetail['status'] == '0'){
								if($paymentKey){
									$journalIds[]	= $paymentDetail['journalId'];
								}
							}
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);
		if($journalIds){
			$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		}
		
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['createOrderId']){continue;}
			
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
			$sendInAggregation		= $orderDatas['sendInAggregation'];
			$ConsolAPIFieldValueID	= '';
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			if(!$paymentDetails){continue;}
			
			if($clientcode == 'coloradokayak'){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
				$taxDate	= date('Ymd',strtotime($taxDate));
				if($taxDate < '20210401'){
					continue;
				}
			}
			
			$invoiceDate	= $rowDatas['invoices']['0']['taxDate'];
			$BPDateOffset	= (int)substr($invoiceDate,23,3);
			$QBOOffset		= 0;
			$diff			= 0;
			$diff			= $BPDateOffset - $QBOOffset;
			$date			= new DateTime($invoiceDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff			.= ' hour';
				$date->modify($diff);
			}
			$invoiceDate	= $date->format('Y-m-d');
			
			if($enableAggregation){
				if($sendInAggregation){
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
							continue;
						}
						if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['disablePayments']){
							continue;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsPaymentAggregated']){
							continue;
						}
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['disablePayments']){
							continue;
						}
					}
				}
			}
			
			if($allPendingFees[$orderId]){
				continue;
			}
			if($AmazonFeesChannels){
				if(in_array($channelId,$AmazonFeesChannels)){
					if(!$allSentFees[$orderId]){
						continue;
					}
				}
			}
			
			if(($orderDatas['uninvoiced'] == 1) AND ($orderDatas['paymentDetails'])){
				continue;
			}
			
			$account1Id					= $orderDatas['account1Id'];
			$config1					= $this->ci->account1Config[$account1Id];
			$StoreCreditPayment			= $config1['StoreCreditPayment'];
			$giftCardPayment			= $config1['giftCardPayment'];
			$QBOorderId					= $orderDatas['createOrderId'];
			$createdRowData				= json_decode($orderDatas['createdRowData'],true);
			$paymentDetails				= json_decode($orderDatas['paymentDetails'],true);
			$negativepayments			= array();
			$sentpayments				= array();
			$applicablepayment			= array();
			$adjustmentarray			= array();
			$adjustmentspayments		= array();
			$allPkey					= array();
			$allRkey					= array();
			$reference					= array();
			$billAddress				= $rowDatas['parties']['billing'];
			$shipAddress				= $rowDatas['parties']['delivery'];
			$orderCustomer				= $rowDatas['parties']['customer'];
			$paymentMethod				= $orderDatas['paymentMethod'];
			$amount						= 0;
			$paidAmount					= 0;
			$StoreCreditAmount			= 0;
			$giftAmount					= 0;
			$totalReceivedPaidAmount	= 0;
			$totaladjustmentpayment		= 0;
			$totalapplicablepayment		= 0;
			$paymentDate				= '';
			$BrightpearlTotalAmount		= $rowDatas['totalValue']['total'];
			$QBOCustomerID				= $createdRowData['Response data	: ']['Invoice']['CustomerRef']['value'];
			$QBOTotalAmount				= $createdRowData['Response data	: ']['Invoice']['TotalAmt'];
			
			
			if(!$QBOCustomerID){
				$readurl			= 'invoice/'.$QBOorderId.'?minorversion=41';
				$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
				if(isset($InvoiceResponse['Invoice'])){
					$QBOCustomerID	= $InvoiceResponse['Invoice']['CustomerRef']['value'];
					$QBOTotalAmount	= $InvoiceResponse['Invoice']['TotalAmt'];
				}
			}
			if(!$QBOCustomerID){
				continue;
			}
			
			
			
			//AmazonFeesOrder Payment Code
			$paymentDate				= date('Y-m-d');
			$totalAmazonOrderAmount		= 0;
			$Negativeamount				= 0;
			$sentableAmount				= 0;
			$CurrencyRate				= 1;
			$reference					= '';
			$memo						= '';
			$paymentDetailcurrency		= '';
			$AmazonOrderrequest			= array();
			$parentOrderPaymentDatas	= array();
			$allAmazonPaymentIds[]		= array();
			$PaymentMethodRef			= $config['PayType'];
			$DepositToAccountRef		= $config['DepositToAccountRef'];
			$accountCode				= $config['IncomeAccountRef'];
			if($orderDatas['totalFeeAmount']){
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['sendPaymentTo'] == 'qbo')){
						$OrderPayemntCurrency		= $paymentDetail['currency'];
						if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
							if($paymentDetail['paymentDate']){
								$paymentDate	= $paymentDetail['paymentDate'];
								$BPDateOffset	= (int)substr($paymentDate,23,3);
								$QBOOffset		= 0;
								$diff			= 0;
								$diff			= $BPDateOffset - $QBOOffset;
								$date			= new DateTime($paymentDate);
								$BPTimeZone		= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff){
									$diff			.= ' hour';
									$date->modify($diff);
								}
								$paymentDate	= $date->format('Y-m-d');
							}
							
							if($clientcode == 'coloradokayak'){
								$CheckPaymentDate	= date('Ymd',strtotime($paymentDate));
								if($CheckPaymentDate < '20230201'){
									continue;
								}
							}
							if(($clientcode == 'aspectledqbo') OR ($clientcode == 'teeturtleqbo')){
								$paymentDate	= $invoiceDate;
							}
							
							if($paymentDetail['paymentMethod']){
								
								if($clientcode != 'anatomicalwwqbo'){
									if(strtolower($paymentDetail['paymentMethod']) == 'other'){
										$parentOrderId	= $rowDatas['parentOrderId'];
										if($parentOrderId){
											$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
											if($parentOrderPaymentDataTemp){
												$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
											}
											if($parentOrderPaymentDatas){
												foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
													if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
														if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
															$paymentDetail['paymentMethod']	= $parentOrderPaymentData['paymentMethod'];
														}
													}
												}
											}
										}
									}
								}
								
								if(isset($paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
									$DepositToAccountRef		= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									$PaymentMethodRef			= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
								}
								
								else if(isset($paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])])){
									$DepositToAccountRef		= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									$PaymentMethodRef			= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
								}
								
								else if(isset($paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
									$DepositToAccountRef		= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									$PaymentMethodRef			= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
								}
								
								else if(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
									$DepositToAccountRef	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									$PaymentMethodRef		= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
								}
								
								
								
							}
							$reference	= $paymentDetail['Reference'];
							if(!$reference){
								$reference	= $paymentDetail['reference'];
							}
							if($paymentDetail['journalId']){
								if(isset($journalDatas[$paymentDetail['journalId']])){
									$CurrencyRate	= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
									$reference		= $journalDatas[$paymentDetail['journalId']]['description'];
									if($config['useRefOnPayments']){
										if($config['useRefOnPayments'] == 'Transactionref'){
											$reference	= $paymentDetail['Reference'];
											if(!$reference){
												$reference	= $journalDatas[$paymentDetail['journalId']]['description'];
											}
										}
										if(($config['useRefOnPayments'] != 'Transactionref') AND ($config['useRefOnPayments'] != 'Reference')){
											$account1FieldIds	= explode(".",$config['useRefOnPayments']);
											$fieldValueTmps		= '';
											foreach($account1FieldIds as $account1FieldId){
												if(!$fieldValueTmps){
													$fieldValueTmps	= $rowDatas[$account1FieldId];
												}
												else{
													$fieldValueTmps = $fieldValueTmps[$account1FieldId];
												}
											}
											if($fieldValueTmps){
												$reference	= $fieldValueTmps;
											}
											else{
												$reference	= $journalDatas[$paymentDetail['journalId']]['description'];
											}
										}
									}
								}
							}
							$memo	= '';
							if($config['memoOnSOSCPayments']){
								if($config['memoOnSOSCPayments'] == 'Transactionref'){
									$memo	= $paymentDetail['Reference'];
								}
								if($config['memoOnSOSCPayments'] == 'Reference'){
									if(isset($journalDatas[$paymentDetail['journalId']])){
										$memo	= $journalDatas[$paymentDetail['journalId']]['description'];
									}
								}
								if(($config['memoOnSOSCPayments'] != 'Transactionref') AND ($config['memoOnSOSCPayments'] != 'Reference')){
									$account1FieldIdsTemp	= explode(".",$config['memoOnSOSCPayments']);
									$fieldValueTmpsTemp		= '';
									foreach($account1FieldIdsTemp as $account1FieldIdTemp){
										if(!$fieldValueTmpsTemp){
											$fieldValueTmpsTemp	= $rowDatas[$account1FieldIdTemp];
										}
										else{
											$fieldValueTmpsTemp = $fieldValueTmpsTemp[$account1FieldIdTemp];
										}
									}
									if($fieldValueTmpsTemp){
										$memo	= $fieldValueTmpsTemp;
									}
								}
							}
							$paymentDetailcurrency	= $paymentDetail['currency'];
							$allAmazonPaymentIds[]	= $key;
							$totalAmazonOrderAmount	+= $paymentDetail['amount'];
						}
						elseif(strtolower($paymentDetail['paymentType']) == 'payment'){
							if(strtolower($paymentDetail['paymentMethod']) == 'adjustment'){
								$allAmazonPaymentIds[]	= $key;
								$Negativeamount			+= abs($paymentDetail['amount']);
							}
						}
					}
				}
				if($totalAmazonOrderAmount){
					$sentableAmount	= $totalAmazonOrderAmount - ($Negativeamount + $orderDatas['totalFeeAmount']);
					if($sentableAmount){
						$AmazonOrderrequest	= array(
							'TxnStatus'				=> 'PAID',
							'CustomerRef'			=> array('value' => $QBOCustomerID),
							'PaymentRefNum'			=> substr($reference,0,21),
							'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
							'TxnDate'				=> $paymentDate,
							'ExchangeRate'			=> sprintf("%.4f",(1 / $CurrencyRate)),
							'TotalAmt'				=> $sentableAmount,
							'Line'					=> array(
								array(
									'Amount'			=> $sentableAmount,
									'LinkedTxn'			=> array(
										array(
											'TxnId'			=> $orderDatas['createOrderId'],
											'TxnType'		=> "Invoice",
										)
									),
								),
							),
						);
						
						if($clientcode == 'ecfqbo'){
							$AmazonOrderrequest['PaymentMethodRef']	= array('value' => $PaymentMethodRef);
						}
						
						//added on 14th Feb 2023 req by Veena
						if($clientcode == 'biscuiteersqbo'){
							if(($channelId == 2) OR ($channelId == 16)){
								$AmazonOrderrequest['TxnDate']	= $invoiceDate;
							}
						}
						
						
						if($memo){
							$AmazonOrderrequest['PrivateNote']	= substr($memo,0,4000);
						}
						if(!$AmazonOrderrequest['PaymentRefNum']){
							unset($AmazonOrderrequest['PaymentRefNum']);
						}
						if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
							$exRate = $this->getQboExchangeRateByDb($account2Id,$paymentDetailcurrency,$config['defaultCurrrency'],$paymentDate);
							if($exRate){
								$AmazonOrderrequest['ExchangeRate'] = $exRate;
							}
							else{
								$exRate	= $exchangeRate[strtolower($paymentDetailcurrency)][strtolower($config['defaultCurrrency'])]['Rate'];
								if($exRate){
									$AmazonOrderrequest['ExchangeRate'] = $exRate;
								}
								else{
									echo 'ExchangeRate Not found Line no - 407';continue;
									unset($AmazonOrderrequest['ExchangeRate']);
								}
							}
						}
						if(!$AmazonOrderrequest['ExchangeRate']){
							echo 'ExchangeRate Not found Line no - 413';continue;
							unset($AmazonOrderrequest['ExchangeRate']);
						}
					}
					if($AmazonOrderrequest){
						$QBOPaymenturl	= 'payment';
						$Paymentresults	= $this->getCurl($QBOPaymenturl, 'POST', json_encode($AmazonOrderrequest), 'json', $account2Id)[$account2Id];
						$createdRowData['QBO Payment Request	: ']	= $AmazonOrderrequest;
						$createdRowData['QBO Payment Response	: ']	= $Paymentresults;
						$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
						
						if($clientcode == 'anatomicalwwqbo'){
							if((is_array($Paymentresults)) AND (!empty($Paymentresults)) AND (isset($Paymentresults['Fault']['Error'][0]['Message']))){
								if(($invoiceDate) AND (isset($Paymentresults['Fault']['Error'][0]['Message'])) AND (substr_count(strtolower($Paymentresults['Fault']['Error'][0]['Message']),"account period closed, cannot update through services api"))){
									$AmazonOrderrequest['TxnDate']	= $invoiceDate;
									$QBOPaymenturl					= 'payment';
									$Paymentresults					= $this->getCurl($QBOPaymenturl, 'POST', json_encode($AmazonOrderrequest), 'json', $account2Id)[$account2Id];
									$createdRowData['QBO Payment Request	: ']	= $AmazonOrderrequest;
									$createdRowData['QBO Payment Response	: ']	= $Paymentresults;
									$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
								}
							}
						}
						
						if($Paymentresults['Payment']['Id']){
							$isPaymentPosted	= 1;
							$updateArray		= array();
							foreach($paymentDetails as $key => $paymentDetail){
								if(in_array($key,$allAmazonPaymentIds)){
									$paymentDetails[$key]['status']				= '1';
									$paymentDetails[$key]['amazonSettlement']	= '1';
								}
							}
							$paymentDetails['amazonFeeinfo']	= array(
								'amount'	=> $orderDatas['totalFeeAmount'],
							);
							$paymentDetails[$Paymentresults['Payment']['Id']]	= array(
								"amount" 				=> $sentableAmount,
								'status'				=> '1',
								'AmountCreditedIn'		=> 'QBO',
								'PaymentMethodRef'		=> $PaymentMethodRef,
							);
							if($sentableAmount >= $BrightpearlTotalAmount){
								$updateArray	= array(
									'isPaymentCreated'	=> '1',
									'status' 			=> '3',
								);
							}
							if($sentableAmount >= $BrightpearlTotalAmount){
								$updateArray['paymentStatus']	= 1;
							}
							else{
								$updateArray['paymentStatus']	= 2;
							}
							$updateArray['paymentDetails']	= json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
						}
					}
				}
				continue;
			}
			
			if($PaymentReversalEnabled){
				foreach($paymentDetails as $sentkey => $paymentDetail){
					if($paymentDetail['status'] == 1){
						if(strtolower($paymentDetail['AmountCreditedIn']) == 'qbo'){
							$sentpayments[$sentkey]	= $paymentDetail;
						}
					}
				}
				
				foreach($paymentDetails as $NegativePaymentKey => $paymentDetail){
					if(strtolower($paymentDetail['sendPaymentTo']) == 'qbo'){
						if($paymentDetail['status'] == 0){
							if(strtolower($paymentDetail['paymentType']) == 'payment'){
								if(strtolower($paymentDetail['paymentMethod']) == 'adjustment'){
									$adjustmentspayments[$NegativePaymentKey]	= $paymentDetail;
								}
								else{
									$negativepayments[$NegativePaymentKey]		= $paymentDetail;
								}
							}
							elseif((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
								$applicablepayment[$NegativePaymentKey]	= $paymentDetail;
							}
						}
					}
				}
			}
			if($negativepayments){
				foreach($negativepayments as $negativekey => $negativepayment){
					if($sendInAggregation){
						continue;
					}
					foreach($sentpayments as $sentkey => $sentpayment){
						if((float)($negativepayment['amount'] + $sentpayment['amount']) == 0){
							$QBOPaymentDeleteKey	= $sentkey;
							$SyncToken				= '';
							$GetPaymentDetail		= 'payment/'.$QBOPaymentDeleteKey.'?minorversion=41';
							$Paymentresponse		= $this->getCurl($GetPaymentDetail, 'GET', '', 'json', $account2Id)[$account2Id];
							if($Paymentresponse['Payment']){
								$SyncToken			= $Paymentresponse['Payment']['SyncToken'];
								$VoidPaymentRequest		= array(
									"SyncToken"				=> $SyncToken,
									"Id"					=> $QBOPaymentDeleteKey,
									"sparse"				=> true,
								);
								$VoidPaymentResponse	= array();
								$VoidPaymentURL			= 'payment?operation=update&include=void&minorversion=41';
								$VoidPaymentResponse	= $this->getCurl($VoidPaymentURL, 'POST', json_encode($VoidPaymentRequest), 'json', $account2Id)[$account2Id];
								$createdRowData['QBO Void Payment Request	: '.$QBOPaymentDeleteKey]	= $VoidPaymentRequest;
								$createdRowData['QBO Void Payment Response	: '.$QBOPaymentDeleteKey]	= $VoidPaymentResponse;
								$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
								if((substr_count(strtolower($VoidPaymentResponse['Payment']['PrivateNote']),'voided')) OR (substr_count(strtolower($VoidPaymentResponse['Payment']['PrivateNote']),'anulado'))){
									$updateArray	= array();
									foreach($paymentDetails as $key => $paymentDetail){
										if($key == $negativekey){
											$paymentDetails[$key]['status']	= '1';
										}
									}
									$paymentDetails['QBODeleteResponse'][]	= $VoidPaymentResponse;
									$updateArray	= array(
											'isPaymentCreated'	=> '0',
											'status' 			=> '1',
										);
									$updateArray['paymentDetails']	= json_encode($paymentDetails);
									$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
								}
							}
							unset($sentpayments[$sentkey]);
							unset($negativepayments[$negativekey]);
							break;
						}
					}
				}
				$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $orderDatas['orderId']))->row_array();
				$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true);
			}
			//PAYMENT REVERSAL CODE ENDS
			
			if($adjustmentspayments AND $applicablepayment){
				$adjustmentarray		= $adjustmentspayments;
				$totaladjustmentpayment	= array_sum(array_column($adjustmentspayments,'amount'));
				$totalapplicablepayment	= array_sum(array_column($applicablepayment,'amount'));
				foreach($adjustmentspayments as $key11 => $negativepaymentss){
					$allRkey[]	= $key11;
				}
				foreach($applicablepayment as $key22 => $applicablepaymentsss){
					$allPkey[]	= $key22;
				}
			}
			if($adjustmentarray){
				if($sendInAggregation){
					if($totalapplicablepayment > $BrightpearlTotalAmount){
						$totalapplicablepayment	= $totalapplicablepayment + $totaladjustmentpayment;
					}
				}
				else{
					if($totalapplicablepayment > $QBOTotalAmount){
						$totalapplicablepayment	= $totalapplicablepayment + $totaladjustmentpayment;
					}
				}
				$applicablepaymentcurr = '';
				foreach($applicablepayment as $samplekey =>  $applicablepaymentss){
					$applicablepaymentcurr = $applicablepaymentss['currency'];
					$reference		= $applicablepaymentss['Reference'];
					if(!$reference){
						$reference	= $applicablepaymentss['reference'];
					}
					$paymentMethod	= $applicablepaymentss['paymentMethod'];
					
					$paymentDate	= $applicablepaymentss['paymentDate'];
					if($paymentDate){
						$BPDateOffset	= (int)substr($paymentDate,23,3);
						$QBOoffset		= 0;
						$diff			= $BPDateOffset - $QBOoffset;
						$date			= new DateTime($paymentDate);
						$BPTimeZone		= 'GMT';
						$date->setTimezone(new DateTimeZone($BPTimeZone));
						if($diff > 0){
							$diff			.= ' hour';
							$date->modify($diff);
						}
						$paymentDate	= $date->format('Y-m-d');
					}
					else{
						$paymentDate	= date('Y-m-d');
					}
					
					if($clientcode == 'coloradokayak'){
						$CheckPaymentDate	= date('Ymd',strtotime($paymentDate));
						if($CheckPaymentDate < '20230201'){
							continue;
						}
					}
					if(($clientcode == 'aspectledqbo') OR ($clientcode == 'teeturtleqbo')){
						$paymentDate	= $invoiceDate;
					}
					
					$PaymentMethodRef		= $config['PayType'];
					$DepositToAccountRef	= $config['DepositToAccountRef'];
					if($paymentMethod){
						if($clientcode != 'anatomicalwwqbo'){
							if(strtolower($paymentMethod) == 'other'){
								$parentOrderId	= $rowDatas['parentOrderId'];
								if($parentOrderId){
									$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
									if($parentOrderPaymentDataTemp){
										$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
									}
									if($parentOrderPaymentDatas){
										foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
											if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
												if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
													$paymentMethod	= $parentOrderPaymentData['paymentMethod'];
												}
											}
										}
									}
								}
							}
						}
						
						if(isset($paymentMappings1[$channelId][strtolower($applicablepaymentss['currency'])][strtolower($paymentMethod)])){
							$DepositToAccountRef		= $paymentMappings1[$channelId][strtolower($applicablepaymentss['currency'])][strtolower($paymentMethod)]['account2PaymentId'];
							$PaymentMethodRef			= $paymentMappings1[$channelId][strtolower($applicablepaymentss['currency'])][strtolower($paymentMethod)]['paymentValue'];
						}
						elseif(isset($paymentMappings2[$channelId][strtolower($paymentMethod)])){
							$DepositToAccountRef		= $paymentMappings2[$channelId][strtolower($paymentMethod)]['account2PaymentId'];
							$PaymentMethodRef			= $paymentMappings2[$channelId][strtolower($paymentMethod)]['paymentValue'];
						}
						elseif(isset($paymentMappings3[strtolower($applicablepaymentss['currency'])][strtolower($paymentMethod)])){
							$DepositToAccountRef		= $paymentMappings3[strtolower($applicablepaymentss['currency'])][strtolower($paymentMethod)]['account2PaymentId'];
							$PaymentMethodRef			= $paymentMappings3[strtolower($applicablepaymentss['currency'])][strtolower($paymentMethod)]['paymentValue'];
						}
						elseif(isset($paymentMappings[strtolower($paymentMethod)])){
							$DepositToAccountRef	= $paymentMappings[strtolower($paymentMethod)]['account2PaymentId'];
							$PaymentMethodRef		= $paymentMappings[strtolower($paymentMethod)]['paymentValue'];
						}
					}
					if($applicablepaymentss['journalId']){
						if(isset($journalDatas[$applicablepaymentss['journalId']])){
							$CurrencyRate	= $journalDatas[$applicablepaymentss['journalId']]['exchangeRate'];
							$reference		= $journalDatas[$applicablepaymentss['journalId']]['description'];
							if($config['useRefOnPayments']){
								if($config['useRefOnPayments'] == 'Transactionref'){
									$reference	= $applicablepaymentss['Reference'];
									if(!$reference){
										$reference	= $journalDatas[$applicablepaymentss['journalId']]['description'];
									}
								}
								if(($config['useRefOnPayments'] != 'Transactionref') AND ($config['useRefOnPayments'] != 'Reference')){
									$account1FieldIds	= explode(".",$config['useRefOnPayments']);
									$fieldValueTmps		= '';
									foreach($account1FieldIds as $account1FieldId){
										if(!$fieldValueTmps){
											$fieldValueTmps	= $rowDatas[$account1FieldId];
										}
										else{
											$fieldValueTmps = $fieldValueTmps[$account1FieldId];
										}
									}
									if($fieldValueTmps){
										$reference	= $fieldValueTmps;
									}
									else{
										$reference	= $journalDatas[$applicablepaymentss['journalId']]['description'];
									}
								}
							}
						}
					}
					$memo	= '';
					if($config['memoOnSOSCPayments']){
						if($config['memoOnSOSCPayments'] == 'Transactionref'){
							$memo	= $applicablepaymentss['Reference'];
						}
						if($config['memoOnSOSCPayments'] == 'Reference'){
							if(isset($journalDatas[$applicablepaymentss['journalId']])){
								$memo	= $journalDatas[$applicablepaymentss['journalId']]['description'];
							}
						}
						if(($config['memoOnSOSCPayments'] != 'Transactionref') AND ($config['memoOnSOSCPayments'] != 'Reference')){
							$account1FieldIdsTemp	= explode(".",$config['memoOnSOSCPayments']);
							$fieldValueTmpsTemp		= '';
							foreach($account1FieldIdsTemp as $account1FieldIdTemp){
								if(!$fieldValueTmpsTemp){
									$fieldValueTmpsTemp	= $rowDatas[$account1FieldIdTemp];
								}
								else{
									$fieldValueTmpsTemp = $fieldValueTmpsTemp[$account1FieldIdTemp];
								}
							}
							if($fieldValueTmpsTemp){
								$memo	= $fieldValueTmpsTemp;
							}
						}
					}
				}
				$amount		= $totalapplicablepayment;
				$request	= array(
					'TxnStatus'				=> 'PAID',
					'CustomerRef'			=> array('value' => $QBOCustomerID),
					'PaymentRefNum'			=> substr($reference,0,21),
					'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
					'TxnDate'				=> $paymentDate,
					'ExchangeRate'			=> sprintf("%.4f",(1 / $CurrencyRate)),
					'TotalAmt'				=> $amount,
					'Line'					=> array(
						array(
							'Amount'			=> $amount,
							'LinkedTxn'			=> array(
								array(
									'TxnId'			=> $orderDatas['createOrderId'],
									'TxnType'		=> "Invoice",
								)
							),
						),
					),
				);
				
				if($clientcode == 'ecfqbo'){
					$request['PaymentMethodRef']	= array('value' => $PaymentMethodRef);
				}
				//added on 14th Feb 2023 req by Veena
				if($clientcode == 'biscuiteersqbo'){
					if(($channelId == 2) OR ($channelId == 16)){
						$request['TxnDate']	= $invoiceDate;
					}
				}
				
				if($memo){
					$request['PrivateNote']	= substr($memo,0,4000);
				}
				if(!$request['PaymentRefNum']){
					unset($request['PaymentRefNum']);
				}
				if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
					$exRate = $this->getQboExchangeRateByDb($account2Id,$applicablepaymentcurr,$config['defaultCurrrency'],$paymentDate);
					if($exRate){
						$request['ExchangeRate'] = $exRate;
					}
					else{
						$exRate	= $exchangeRate[strtolower($applicablepaymentcurr)][strtolower($config['defaultCurrrency'])]['Rate'];
						if($exRate){
							$request['ExchangeRate'] = $exRate;
						}
						else{
							echo 'ExchangeRate Not found Line no - 711';continue;
							unset($request['ExchangeRate']);
						}
					}
				}
				if(!$request['ExchangeRate']){
					echo 'ExchangeRate Not found Line no - 717';continue;
					unset($request['ExchangeRate']);
				}
				$url		= 'payment';
				$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
				$createdRowData['QBO Payment Request	: (adjusted)']	= $request;
				$createdRowData['QBO Payment Response	: (adjusted)']	= $results;
				$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
				
				if($clientcode == 'anatomicalwwqbo'){
					if((is_array($results)) AND (!empty($results)) AND (isset($results['Fault']['Error'][0]['Message']))){
						if(($invoiceDate) AND (isset($results['Fault']['Error'][0]['Message'])) AND (substr_count(strtolower($results['Fault']['Error'][0]['Message']),"account period closed, cannot update through services api"))){
							$request['TxnDate']	= $invoiceDate;
							$url		= 'payment';
							$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
							$createdRowData['QBO Payment Request	: (adjusted)']	= $request;
							$createdRowData['QBO Payment Response	: (adjusted)']	= $results;
							$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						}
					}
				}
				
				if($results['Payment']['Id']){
					$updateArray	= array();
					foreach($paymentDetails as $updatekey => $paymentDetailsdata){
						if(in_array($updatekey, $allRkey)){
							$paymentDetails[$updatekey]['status']			= 1; 
							$paymentDetails[$updatekey]['notapplicable']	= 1; 
						}
						if(in_array($updatekey, $allPkey)){
							$paymentDetails[$updatekey]['status']			= 1; 
						}
					}
					$paymentDetails[$results['Payment']['Id']]	= array(
						"amount" 				=> $amount,
						"ParentPaymentId"		=> $allPkey,
						'status'				=> '1',
						'AmountCreditedIn'		=> 'QBO',
						'PaymentMethodRef'		=> $PaymentMethodRef,
					);
					$updateArray['paymentDetails']	= json_encode($paymentDetails);
					$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);   
				}
				$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $orderDatas['orderId']))->row_array();
				$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true);
			}
			
			foreach($paymentDetails as $paymentKey => $paymentDetail){
				$giftAmount		= 0;
				$StoreCreditAmount	= 0;
				if((!$paymentDetail['AmountCreditedIn']) AND (!$paymentDetail['notapplicable']) AND (!$paymentDetail['DeletedOnBrightpearl']) AND (!$paymentDetail['isvoided'])){
					$totalReceivedPaidAmount	+= $paymentDetail['amount'];
				}
				if($paymentDetail['sendPaymentTo'] == 'qbo'){
					if($paymentDetail['status'] == '0'){
						if($paymentDetail['amount'] > 0){
							if(($paymentDetail['paymentMethod'] == $giftCardPayment) OR ($paymentDetail['paymentMethod'] == $StoreCreditPayment)){
								if($paymentDetail['paymentMethod'] == $giftCardPayment){
									$giftAmount			= $paymentDetail['amount'];
								}
								else{
									$StoreCreditAmount	= $paymentDetail['amount'];
								}
								if($giftAmount OR $StoreCreditAmount){
									$query			= "select * from invoice where id = '".$orderDatas['createOrderId']."'";
									$url			= "query?minorversion=4&query=".rawurlencode($query);
									$qboOrderInfos	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
									$qboLineNumber	= 1;
									foreach($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'] as $key => $qbLine){
										if($qbLine['DetailType'] == 'SubTotalLineDetail'){
											unset($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][$key]);
										}
										if(@$qbLine['LineNum'] > $qboLineNumber){
											$qboLineNumber	= $qbLine['LineNum'];
										}
									}
									$qboLineNumber++;
									if($paymentDetail['paymentMethod'] == $giftCardPayment){
										$qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'])]	= array(
											'LineNum'				=> count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line']) + 1,
											'Amount'				=> (-1) * $giftAmount,		
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $config['giftCardItem']),
												'UnitPrice'				=> (-1) * $giftAmount,
												'Qty'					=> '1',
												'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
											),
										);
									}
									elseif($paymentDetail['paymentMethod'] == $StoreCreditPayment){
										$qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'])]	= array(
											'LineNum'				=> count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line']) + 1,
											'Amount'				=> (-1) * $StoreCreditAmount,		
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef'				=> array('value' => $config['StoreCreditItem']),
												'UnitPrice'				=> (-1) * $StoreCreditAmount,
												'Qty'					=> '1',
												'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
											),
										);
									}
									
									$rqInvoice	= $qboOrderInfos['QueryResponse']['Invoice']['0'];
									$request	= array(
										'Id'			=> $rqInvoice['Id'],
										'DocNumber'		=> $rqInvoice['DocNumber'],
										'CustomerRef'	=> array('value' => $rqInvoice['CustomerRef']['value']),
										'BillEmail'		=> $rqInvoice['BillEmail'],
										'SyncToken'		=> $rqInvoice['SyncToken'],
										'domain'		=> $rqInvoice['domain'],
										'TxnDate'		=> $rqInvoice['TxnDate'],
										'DueDate'		=> $rqInvoice['DueDate'],
										'BillAddr'		=> $rqInvoice['BillAddr'],
										'ShipAddr'		=> $rqInvoice['ShipAddr'],
										'ExchangeRate'	=> $rqInvoice['ExchangeRate'],
										'CurrencyRef'	=> $rqInvoice['CurrencyRef'],
										/* 'TxnTaxDetail'	=> $rqInvoice['TxnTaxDetail'],  */
										'ClassRef'		=> $rqInvoice['ClassRef'],
										'Line'			=> $rqInvoice['Line'],
									);
									if($clientcode == 'coloradokayak'){
										$request['AllowOnlineCreditCardPayment']	= 'true';
									}
									if($config1['currencyCode'] != $config['defaultCurrrency']){
										unset($request['ExchangeRate']);
									}
									if(!$request['ExchangeRate']){
										unset($request['ExchangeRate']);
									}
									$url			= 'invoice?minorversion=37'; 
									$updateResults	= @$this->getCurl($url, 'POST', json_encode($request), 'json',$account2Id)[$account2Id];
									if($paymentDetail['paymentMethod'] == $giftCardPayment){
										$createdRowData['QBO Gift Card Request Data		: '.$paymentKey]	= $request;
										$createdRowData['QBO Gift Card Response Data	: '.$paymentKey]	= $updateResults;
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
										if($updateResults['Invoice']['Id']){
											$updateArray	= array();
											if($paymentDetail['sendPaymentTo'] == 'qbo'){
												if($paymentDetail['paymentMethod'] == $giftCardPayment){
													$paymentDetails[$paymentKey]['status']	= '1';
												}
											}
											$paymentDetails['GiftCardAdded'.$paymentKey]['status']	= '1';
											if($totalReceivedPaidAmount >= $QBOTotalAmount){
												$updateArray	= array(
													'isPaymentCreated'	=> '1',
													'status'			=> '3',
												);
											}
											$updateArray['paymentDetails'] = json_encode($paymentDetails);
											$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);   
										}
									}
									elseif($paymentDetail['paymentMethod'] == $StoreCreditPayment){
										$createdRowData['QBO StoreCredit Request Data	: '.$paymentKey]	= $request;
										$createdRowData['QBO StoreCredit Response Data	: '.$paymentKey]	= $updateResults;
										if($updateResults['Invoice']['Id']){
											$updateArray	= array();
											if($paymentDetail['sendPaymentTo'] == 'qbo'){
												if($paymentDetail['paymentMethod'] == $StoreCreditPayment){
													$paymentDetails[$paymentKey]['status']	= '1';
												}
											}
											$paymentDetails['StoreCreditAdded'.$paymentKey]['status']	= '1';
											if($totalReceivedPaidAmount >= $QBOTotalAmount){
												$updateArray	= array(
													'isPaymentCreated'	=> '1',
													'status'			=> '3',
												);
											}
											$updateArray['paymentDetails'] = json_encode($paymentDetails);
											$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);   
										}
									}
								}
							}
							else{
								$amount					= $paymentDetail['amount'];
								$reference				= $paymentDetail['Reference'];
								if(!$reference){
									$reference	= $paymentDetail['reference'];
								}
								$paymentMethod			= $paymentDetail['paymentMethod'];
								$paymentDate			= $paymentDetail['paymentDate'];
								if($paymentDate){
									$BPDateOffset	= (int)substr($paymentDate,23,3);
									$QBOoffset		= 0;
									$diff			= $BPDateOffset - $QBOoffset;
									$date			= new DateTime($paymentDate);
									$BPTimeZone		= 'GMT';
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($diff){
										$diff			.= ' hour';
										$date->modify($diff);
									}
									$paymentDate	= $date->format('Y-m-d');
								}
								else{
									$paymentDate	= date('Y-m-d');
								}
								
								if($clientcode == 'coloradokayak'){
									$CheckPaymentDate	= date('Ymd',strtotime($paymentDate));
									if($CheckPaymentDate < '20230201'){
										continue;
									}
								}
								
								if(($clientcode == 'aspectledqbo') OR ($clientcode == 'teeturtleqbo')){
									$paymentDate	= $invoiceDate;
								}
								
								if($paymentDetail['journalId']){
									if(isset($journalDatas[$paymentDetail['journalId']])){
										$CurrencyRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
										$reference			= $journalDatas[$paymentDetail['journalId']]['description'];
										if($config['useRefOnPayments']){
											if($config['useRefOnPayments'] == 'Transactionref'){
												$reference	= $paymentDetail['Reference'];
												if(!$reference){
													$reference	= $journalDatas[$paymentDetail['journalId']]['description'];
												}
											}
											if(($config['useRefOnPayments'] != 'Transactionref') AND ($config['useRefOnPayments'] != 'Reference')){
												$account1FieldIds	= explode(".",$config['useRefOnPayments']);
												$fieldValueTmps		= '';
												foreach($account1FieldIds as $account1FieldId){
													if(!$fieldValueTmps){
														$fieldValueTmps	= $rowDatas[$account1FieldId];
													}
													else{
														$fieldValueTmps = $fieldValueTmps[$account1FieldId];
													}
												}
												if($fieldValueTmps){
													$reference	= $fieldValueTmps;
												}
												else{
													$reference	= $journalDatas[$paymentDetail['journalId']]['description'];
												}
											}
										}
									}
								}
								$memo	= '';
								if($config['memoOnSOSCPayments']){
									if($config['memoOnSOSCPayments'] == 'Transactionref'){
										$memo	= $paymentDetail['Reference'];
									}
									if($config['memoOnSOSCPayments'] == 'Reference'){
										if(isset($journalDatas[$paymentDetail['journalId']])){
											$memo	= $journalDatas[$paymentDetail['journalId']]['description'];
										}
									}
									if(($config['memoOnSOSCPayments'] != 'Transactionref') AND ($config['memoOnSOSCPayments'] != 'Reference')){
										$account1FieldIdsTemp	= explode(".",$config['memoOnSOSCPayments']);
										$fieldValueTmpsTemp		= '';
										foreach($account1FieldIdsTemp as $account1FieldIdTemp){
											if(!$fieldValueTmpsTemp){
												$fieldValueTmpsTemp	= $rowDatas[$account1FieldIdTemp];
											}
											else{
												$fieldValueTmpsTemp = $fieldValueTmpsTemp[$account1FieldIdTemp];
											}
										}
										if($fieldValueTmpsTemp){
											$memo	= $fieldValueTmpsTemp;
										}
									}
								}
								
								$PaymentMethodRef		= $config['PayType'];
								$DepositToAccountRef	= $config['DepositToAccountRef'];
								if($paymentMethod){
									if($clientcode != 'anatomicalwwqbo'){
										if(strtolower($paymentMethod) == 'other'){
											$parentOrderId	= $rowDatas['parentOrderId'];
											if($parentOrderId){
												$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
												if($parentOrderPaymentDataTemp){
													$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
												}
												if($parentOrderPaymentDatas){
													foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
														if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
															if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
																$paymentMethod	= $parentOrderPaymentData['paymentMethod'];
															}
														}
													}
												}
											}
										}
									}
									
									if(isset($paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentMethod)])){
										$DepositToAccountRef		= $paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentMethod)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentMethod)]['paymentValue'];
									}
									else if(isset($paymentMappings2[$channelId][strtolower($paymentMethod)])){
										$DepositToAccountRef		= $paymentMappings2[$channelId][strtolower($paymentMethod)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings2[$channelId][strtolower($paymentMethod)]['paymentValue'];
									}
									else if(isset($paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentMethod)])){
										$DepositToAccountRef		= $paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentMethod)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentMethod)]['paymentValue'];
									}
									else if(isset($paymentMappings[strtolower($paymentMethod)])){
										$DepositToAccountRef	= $paymentMappings[strtolower($paymentMethod)]['account2PaymentId'];
										$PaymentMethodRef		= $paymentMappings[strtolower($paymentMethod)]['paymentValue'];
									}
								}
								$request	= array(
									'TxnStatus'				=> 'PAID',
									'CustomerRef'			=> array('value' => $QBOCustomerID),
									'PaymentRefNum'			=> substr($reference,0,21),
									'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
									'TxnDate'				=> $paymentDate,
									'TotalAmt'				=> $amount,
									'ExchangeRate'			=> sprintf("%.4f",(1 / $CurrencyRate)),
									'Line'					=> array(
										array(
											'Amount'			=> $amount,
											'LinkedTxn'			=> array(
												array(
													'TxnId'			=> $orderDatas['createOrderId'],
													'TxnType'		=> "Invoice",
												)
											),
										),
									),
								);
								
								if($clientcode == 'ecfqbo'){
									$request['PaymentMethodRef']	= array('value' => $PaymentMethodRef);
								}
								
								//added on 14th Feb 2023 req by Veena
								if($clientcode == 'biscuiteersqbo'){
									if(($channelId == 2) OR ($channelId == 16)){
										$request['TxnDate']	= $invoiceDate;
									}
								}
								
								if($memo){
									$request['PrivateNote']	= substr($memo,0,4000);
								}
								if(!$request['PaymentRefNum']){
									unset($request['PaymentRefNum']);
								}
								if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
									$exRate = $this->getQboExchangeRateByDb($account2Id,$paymentDetail['currency'],$config['defaultCurrrency'],$paymentDate);
									if($exRate){
										$request['ExchangeRate'] = $exRate;
									}
									else{
										$exRate	= $exchangeRate[strtolower($paymentDetail['currency'])][strtolower($config['defaultCurrrency'])]['Rate'];
										if($exRate){
											$request['ExchangeRate'] = $exRate;
										}
										else{
											echo 'ExchangeRate Not found Line no - 1029';continue;
											unset($request['ExchangeRate']);
										}
									}
									
								}
								if(!$request['ExchangeRate']){
									echo 'ExchangeRate Not found Line no - 1036';continue;
									unset($request['ExchangeRate']);
								}
								$url		= 'payment';
								$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
								$createdRowData['QBO Payment Request Data	: '.$paymentKey]	= $request;
								$createdRowData['QBO Payment Response Data	: '.$paymentKey]	= $results;
								$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
								
								if($clientcode == 'anatomicalwwqbo'){
									if((is_array($results)) AND (!empty($results)) AND (isset($results['Fault']['Error'][0]['Message']))){
										if(($invoiceDate) AND (isset($results['Fault']['Error'][0]['Message'])) AND (substr_count(strtolower($results['Fault']['Error'][0]['Message']),"account period closed, cannot update through services api"))){
											$request['TxnDate']	= $invoiceDate;
											$url		= 'payment';
											$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
											$createdRowData['QBO Payment Request Data	: '.$paymentKey]	= $request;
											$createdRowData['QBO Payment Response Data	: '.$paymentKey]	= $results;
											$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
										}
									}
								}
								
								if($results['Payment']['Id']){
									$updateArray	= array();
									if($paymentDetail['sendPaymentTo'] == 'qbo'){
										$paymentDetails[$paymentKey]['status']	= '1';
									}
									$paymentDetails[$results['Payment']['Id']]	= array(
										"amount" 				=> $paymentDetail['amount'],
										"ParentPaymentId"		=> $paymentKey,
										'status'				=> '1',
										'AmountCreditedIn'		=> 'QBO',
										'PaymentMethodRef'		=> $PaymentMethodRef,
									);
									if($totalReceivedPaidAmount >= $BrightpearlTotalAmount){
										$updateArray	= array(
											'isPaymentCreated'	=> '1',
											'status'			=> '3',
										);
									}
									if($totalReceivedPaidAmount >= $BrightpearlTotalAmount){
										$updateArray['paymentStatus']	= 1;
									}
									else{
										$updateArray['paymentStatus']	= 2;
									}
									$updateArray['paymentDetails']	= json_encode($paymentDetails);
									$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);   
								}
							}
						}
					}
				}
			}
		}
	}
}
?>