<?php
//qbo
$this->reInitialize();
$exchangeRates	= $this->getExchangeRate();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){
			continue;
		}
	}
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('stockTransferId',$orgObjectId);
	}
	$datas	= $this->ci->db->order_by('goodsMovementId', 'desc')->get_where('single_company_stock_transfers',array('status' => 0, 'account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$this->ci->db->reset_query();
	$warehouseClassMappingsTemps	= $this->ci->db->get_where('mapping_warehouseClass',array('account2Id' => $account2Id))->result_array();
	$warehouseClassMappings			= array();
	if($warehouseClassMappingsTemps){
		foreach($warehouseClassMappingsTemps as $warehouseClassMappingsTemp){
			$warehouseClassMappings[$warehouseClassMappingsTemp['account1WarehouseId']]	= $warehouseClassMappingsTemp;
		}
	}
	
	$Alldatas			= array();
	$AllNegativeData	= array();
	$AllPostiveData		= array();
	$infoAbouttx		= array();
	foreach($datas as $datasTemp){
		$Alldatas[$datasTemp['stockTransferId']][]		= $datasTemp;
		$infoAbouttx[$datasTemp['stockTransferId']]		= $datasTemp;
		if($datasTemp['qty'] < 0){
			$AllNegativeData[$datasTemp['stockTransferId']][$datasTemp['productId']]	= $datasTemp;
		}
		else{
			$AllPostiveData[$datasTemp['stockTransferId']][$datasTemp['productId']]	= $datasTemp;
		}
	}
	
	foreach($Alldatas as $stockTransferId => $AlldatasTemp){
		$journalRequest				= array();
		$journalLineAdd				= array();
		$LineSequence				= 0;
		$shippedOn					= date('Y-m-d');
		$skiptx						= 0;
		if($infoAbouttx[$stockTransferId]){
			$rowDataTx			= json_decode($infoAbouttx[$stockTransferId]['rowDataTx'],true);
			$ProductInfoInTx	= $rowDataTx['transferRows'];
			if($ProductInfoInTx){
				foreach($ProductInfoInTx as $ProductInfoInTxTemp){
					$productIdinTx	= $ProductInfoInTxTemp['productId'];
					if(($AllNegativeData[$stockTransferId][$productIdinTx]) AND ($AllPostiveData[$stockTransferId][$productIdinTx])){
						//
					}
					else{
						$skiptx	= 1;
						break;
					}
				}
			}
			else{
				continue;
			}
		}
		else{
			continue;
		}
		if($skiptx){
			$this->ci->db->update('single_company_stock_transfers',array('message' => 'Missing GoodsMovement'),array('stockTransferId' => $stockTransferId));
			continue;
		}
		$exchangeRate	= 1;
		$currencyCode	= '';
		foreach($AlldatasTemp as $data){
			if($skiptx){continue;}
			
			$qty	= $data['qty'];
			if($qty < 0){
				continue;
			}
			if($AllNegativeData[$stockTransferId][$data['productId']]){
				//
			}
			else{
				continue;
			}
			
			$goodsMovementId	= $data['goodsMovementId'];
			$sourceWarehouseId	= $data['sourceWarehouseId'];
			$targetWarehouseId	= $data['targetWarehouseId'];
			$currencyCode		= $data['currencyCode'];
			$shippedOn			= date('Y-m-d',strtotime($data['shippedOn']));
			$Acc1config			= $this->ci->account1Config[$data['account1Id']];
			$CreditClassId		= $warehouseClassMappings[$sourceWarehouseId]['account2ChannelId'];
			$DebitClassId		= $warehouseClassMappings[$targetWarehouseId]['account2ChannelId'];
			
			$journalLineAdd[$LineSequence]	= array(
				'LineNum'					=> ($LineSequence + 1),
				'Description'				=> 'Stock Transfer',
				'Amount'					=> sprintf("%.4f",($data['totalValue'])),
				'DetailType'				=> 'JournalEntryLineDetail',
				'JournalEntryLineDetail'	=> array(
					'PostingType'				=> 'Credit',
					'AccountRef'				=> array('value' => $config['AssetAccountRef']),
					'ClassRef'					=> array('value' => $CreditClassId),
				),
			);
			$LineSequence++;
			
			$journalLineAdd[$LineSequence]	= array(
				'LineNum'					=> ($LineSequence + 1),
				'Description'				=> 'Stock Transfer',
				'Amount'					=> sprintf("%.4f",($data['totalValue'])),
				'DetailType'				=> 'JournalEntryLineDetail',
				'JournalEntryLineDetail'	=> array(
					'PostingType'				=> 'Debit',
					'AccountRef'				=> array('value' => $config['AssetAccountRef']),
					'ClassRef'					=> array('value' => $DebitClassId),
				),
			);
			$LineSequence++;
			
			$exchangeRate	= 1;
			if(($config['defaultCurrrency']) AND ($Acc1config['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$currencyCode,$config['defaultCurrrency'],$shippedOn);
				if($exRate){
					$exchangeRate	= $exRate;
				}
				else{
					$exRate = $exchangeRatesAll[strtolower($currencyCode)][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$exchangeRate	= $exRate;
					}
					else{
						$exchangeRate	= 0;
					}
				}
				
			}
		}
		if($journalLineAdd){
			$DocNumber		= 'ST-'.$stockTransferId;
			$DocNumber		= substr($DocNumber,0,21);
			$journalRequest	= array(
				'DocNumber'		=> $DocNumber,
				'TxnDate'		=> $shippedOn,
				'ExchangeRate'	=> sprintf("%.6f",($exchangeRate)),
				'CurrencyRef'	=> array('value' => strtoupper($currencyCode)),
				'Line'			=> $journalLineAdd,
			);
			if(!$journalRequest['ExchangeRate']){
				echo 'ExchangeRate Not found Line no - 167';continue;
				unset($journalRequest['ExchangeRate']);
			}
		}
		
		if($journalRequest){
			
			$url		= 'journalentry?minorversion=37';  
			$results	= $this->getCurl($url, 'POST', json_encode($journalRequest), 'json', $account2Id)[$account2Id];
			$createdRowData['Request data	: ']	= $journalRequest;
			$createdRowData['Response data	: ']	= $results;
			$this->ci->db->update('single_company_stock_transfers',array('createdRowData' => json_encode($createdRowData)),array('stockTransferId' => $stockTransferId));
			if(@$results['JournalEntry']['Id']){
				$this->ci->db->update('single_company_stock_transfers',array('status' => '1','createdOrderId' => $results['JournalEntry']['Id'],'message' => ''),array('stockTransferId' => $stockTransferId));
			}
		}
	}
}
?>