<?php
//qbo
$this->reInitialize();
$exchangeRates					= $this->getExchangeRate();
$enableStockAdjustment			= $this->ci->globalConfig['enableStockAdjustment'];
$enableConsolStockAdjustment	= $this->ci->globalConfig['enableConsolStockAdjustment'];
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableStockAdjustment){continue;}
	if($enableConsolStockAdjustment){continue;}
	if($clientcode == 'bbhugmeqbo'){
		if(!$exchangeRates){
			continue;
		}
	}
	
	$config				= $this->accountConfig[$account2Id];
	$exchangeRatesAll	= $exchangeRates[$account2Id];
	$datasTemps	= array();
	$fetchType ='';
	if($fetchType){
		$this->ci->db->reset_query();
		if($fetchType == 'SC'){
			$datasTemps	= $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('GoodNotetype' => 'SC', 'account2Id' => $account2Id))->result_array();
		}
		elseif($fetchType == 'GO'){
			$datasTemps	= $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('GoodNotetype' => 'GO', 'account2Id' => $account2Id))->result_array();
		}
	}
	else{
		$this->ci->db->reset_query();
		if($orgObjectId){
			$this->ci->db->where_in('orderId',$orgObjectId);
		}
		$datasTemps	= $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array();
		if(!$datasTemps){
			$this->ci->db->reset_query();
			$this->ci->db->where_in('ActivityId',$orgObjectId);
			$datasTemps	= $this->ci->db->where_in('status',array('0'))->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array(); 
		}
	}
	if(!$datasTemps){
		continue;
	}
	$productIds	= array_column($datasTemps,'productId');
	if($productIds){
		if(!$config['disableSkuDetails']){
			$this->postProducts($productIds,$account2Id);
		}
	}
	$this->ci->db->reset_query();
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
	}
	
	$this->ci->db->reset_query();
	$warehouseClassMappingsTemps	= $this->ci->db->get_where('mapping_warehouseClass',array('account2Id' => $account2Id))->result_array();
	$warehouseClassMappings			= array();
	if($warehouseClassMappingsTemps){
		foreach($warehouseClassMappingsTemps as $warehouseClassMappingsTemp){
			$warehouseClassMappings[$warehouseClassMappingsTemp['account1WarehouseId']]	= $warehouseClassMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$datass		= array();
	foreach($datasTemps as $datasTemp){
		if($datasTemp['GoodNotetype'] == 'GO'){
			$datass[$datasTemp['ActivityId']][]	= $datasTemp;
		}
		if($datasTemp['GoodNotetype'] == 'SC'){
			$datass[$datasTemp['orderId']][]	= $datasTemp;
		}
	}
	
	foreach($datass as $datas){
		$positiveStockRequest	= array();
		$negativeStockRequest	= array();
		$AdjustmentStockRequest	= array();
		$positiveOrderIds		= array();
		$negativeOrderIds		= array();
		$AdjustmentOrderIds		= array();
		foreach($datas as $data){
			$goodsMovementId	= $data['orderId'];
			$ActivityId			= $data['ActivityId'];
			$shippedDate		= $data['shippedDate'];
			$warehouseId		= $data['warehouseId'];
			$config1			= $this->ci->account1Config[$data['account1Id']];
			$exchangeRate		= 1;
			$exchangeRate		= sprintf("%.4f",(1 / $exchangeRate));
			if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$config1['currencyCode'],$config['defaultCurrrency'],$shippedDate);
				if($exRate){
					$exchangeRate		= $exRate;
				}
				else{
					$exRate = $exchangeRatesAll[strtolower($config1['currencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$exchangeRate		= $exRate;
					}
					else{
						$exchangeRate		= 0;
					}
				}
			}
			if($data['GoodNotetype'] ==  'GO'){
				if($data['qty'] > 0){
					$LineItem	= array(
						'Description'					=> $productMappings[$data['productId']]['name'],
						'Amount'						=> abs(((int)$data['qty']) * (sprintf("%.4f",($data['price'])))),
						'DetailType'					=> 'ItemBasedExpenseLineDetail',
						'ItemBasedExpenseLineDetail'	=> array(
							'BillableStatus'				=> 'NotBillable',
							'ItemRef'						=> array('value' => $config['InventoryTransferSKU'] ),
							'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode']),
							'Qty'							=> abs((int)($data['qty'])),
							'UnitPrice'						=> sprintf("%.4f",($data['price'])),
						),
					);
					$positiveStockRequest[]				= $LineItem;
					$positiveOrderIds[$data['orderId']]	= $data['orderId'];
				}
				else{
					$LineItem	= array(
						'Description'					=> $productMappings[$data['productId']]['name'],
						'Amount'						=> abs(((int)$data['qty']) * (sprintf("%.4f",($data['price'])))),
						'DetailType'					=> 'SalesItemLineDetail',
						'SalesItemLineDetail'			=> array(
							'ItemRef'						=> array('value' => $config['InventoryTransferSKU'] ),
							'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode'] ),
							'Qty'							=> abs((int)($data['qty'])),
							'UnitPrice'						=> sprintf("%.4f",($data['price'])),
						),
					);
					$negativeStockRequest[]				= $LineItem;
					$negativeOrderIds[$data['orderId']]	= $data['orderId'];
				}
			}
			if($data['GoodNotetype'] ==  'SC'){
				$LineItem	= array(
					'Description'					=> $productMappings[$data['productId']]['name'],
					'Amount'						=> ((int)($data['qty']) * (sprintf("%.4f",($data['price'])))),
					'DetailType'					=> 'ItemBasedExpenseLineDetail',
					'ItemBasedExpenseLineDetail'	=> array(
						'BillableStatus'				=> 'NotBillable',
						'ItemRef'						=> array('value' => $productMappings[$data['productId']]['createdProductId'] ),
						'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode'] ),
						'Qty'							=> (int)($data['qty']),
						'UnitPrice'						=> sprintf("%.4f",($data['price'])),
						'ClassRef'						=> array('value' => $warehouseClassMappings[$warehouseId]['account2ChannelId']),
					),
				);
				
				if($config['disableSkuDetails']){
					if($config['trackedGenericItem'] != ''){
						$LineItem['Description']	= 'Tracked Generic SKU';
						$LineItem['ItemBasedExpenseLineDetail']['ItemRef']	= array('value' => $config['trackedGenericItem']);
					}
				}
				
				$AdjustmentStockRequest[]				= $LineItem;
				$AdjustmentOrderIds[$data['orderId']]	= $data['orderId'];
				$stockLine	= array(
					'Description'					=> 'Inventory Adjustment',
					'Amount'						=> (-1) * ((int)($data['qty']) * (sprintf("%.4f",($data['price'])))),
					'DetailType'					=> 'AccountBasedExpenseLineDetail',
					'AccountBasedExpenseLineDetail'	=> array(
						'BillableStatus'				=> 'NotBillable',
						'AccountRef'					=> array('value' => $config['accRefForStockAdjustment']),
						'ClassRef'						=> array('value' => $warehouseClassMappings[$warehouseId]['account2ChannelId']),
					),
				);
				$AdjustmentStockRequest[]	= $stockLine;
			}
		}
		if($positiveStockRequest){
			$request	= array();
			$DocNumber	= $ActivityId;
			$customer	= array('value' => $config['InterCoSupplier']);
			$request	= array(
				'DocNumber'		=> $DocNumber,
				'VendorRef'		=> $customer,
				'TxnDate'		=> date('Y-m-d',strtotime($shippedDate)),
				'DueDate'		=> date('Y-m-d',strtotime($shippedDate)),
				'Line'			=> $positiveStockRequest,
				'CurrencyRef'	=> array('value' => strtoupper($config1['currencyCode'])),
			);
			if($exchangeRate){
				$request['ExchangeRate']	= $exchangeRate;
			}
			$url		= 'bill';
			$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'stockAdjustment'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'GO'. DIRECTORY_SEPARATOR;
			if(!is_dir($rowDataFilePath)){
				mkdir($rowDataFilePath,0777,true);
				chmod(dirname($rowDataFilePath), 0777);
			}
			file_put_contents($rowDataFilePath.$ActivityId.'.json',json_encode($createdRowData));
			if($results['Bill']['Id']){
				$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('status' => '1', 'createdOrderId' => $results['Bill']['Id']));
			}
		}
		if($negativeStockRequest){
			$request	= array();
			$DocNumber	= $ActivityId;
			$customer	= array('value' => $config['InterCoCustomer']);
			$request	= array(
				'DocNumber'		=> $DocNumber,
				'CustomerRef'	=> $customer,
				'TxnDate'		=> date('Y-m-d',strtotime($shippedDate)),
				'DueDate'		=> date('Y-m-d',strtotime($shippedDate)),
				'Line'			=> $negativeStockRequest,
				'CurrencyRef'	=> array('value' => strtoupper($config1['currencyCode'])),
			);
			if($exchangeRate){
				$request['ExchangeRate']	= $exchangeRate;
			}
			$url			= 'invoice?minorversion=37'; 
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'stockAdjustment'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'GO'. DIRECTORY_SEPARATOR;
			if(!is_dir($rowDataFilePath)){
				mkdir($rowDataFilePath,0777,true);
				chmod(dirname($rowDataFilePath), 0777);
			}
			file_put_contents($rowDataFilePath.$ActivityId.'.json',json_encode($createdRowData));
			if((isset($results['Invoice']['Id']))){
				$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('status' => '1', 'createdOrderId' => $results['Invoice']['Id']));
			}
		}
		if($AdjustmentStockRequest){
			$request	= array();
			$DocNumber	= $goodsMovementId;
			$customer	= array('value' => $config['supplierIdForBill']);
			$request	= array(
				'DocNumber'		=> $DocNumber,
				'VendorRef'		=> $customer,
				'TxnDate'		=> date('Y-m-d',strtotime($data['created'])),
				'DueDate'		=> date('Y-m-d',strtotime($data['created'])),
				'Line'			=> $AdjustmentStockRequest,
				'CurrencyRef'	=> array('value' => strtoupper($config1['currencyCode'])),
			);
			if($exchangeRate){
				$request['ExchangeRate']	= $exchangeRate;
			}
			$url		= 'bill';
			$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'stockAdjustment'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'SA'. DIRECTORY_SEPARATOR;
			if(!is_dir($rowDataFilePath)){
				mkdir($rowDataFilePath,0777,true);
				chmod(dirname($rowDataFilePath), 0777);
			}
			$updateIds	= array_keys($AdjustmentOrderIds);
			file_put_contents($rowDataFilePath.$updateIds[0].'.json',json_encode($createdRowData));
			if($results['Bill']['Id']){
				$this->ci->db->where_in('orderId',array_values($AdjustmentOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('status' => '1', 'createdOrderId' => $results['Bill']['Id']));
			}
		}
	}
}
?>