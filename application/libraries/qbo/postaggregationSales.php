<?php	//demo
$dateResults	= array(1=> array('name'=> 'JAN','lastDate'=> '31',),2=> array('name'=> 'FEB','lastDate'=> '28','lastDate2'=> '29',),3=> array('name'=> 'MAR','lastDate'=> '31',),4=> array('name'=> 'APR','lastDate'=> '30',),5=> array('name'=> 'MAY','lastDate'=> '31',),6=> array('name'=> 'JUN','lastDate'=> '30',),7=> array('name'=> 'JUL','lastDate'=> '31',),8=> array('name'=> 'AUG','lastDate'=> '31',),9=> array('name'=> 'SEP','lastDate'=> '30',),10=> array('name'=> 'OCT','lastDate'=> '31',),11=> array('name'=> 'NOV','lastDate'=> '30',),12=> array('name'=> 'DEC','lastDate'=> '31',),);

$this->reInitialize();
$exchangeRates		= $this->getExchangeRate();
$enableAggregation	= $this->ci->globalConfig['enableAggregation'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	$exchangeRatetemp	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$datas	= $this->ci->db->get_where('sales_order',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	if(empty($datas)){continue;}
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if(!empty($AggregationMappingsTemps)){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]	= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$allAPIFieldsValues	= explode("||", trim($account1APIFieldId));
					$allAPIFieldsValues	= array_filter($allAPIFieldsValues);
					$allAPIFieldsValues	= array_unique($allAPIFieldsValues);
					if((is_array($allAPIFieldsValues)) AND (!empty($allAPIFieldsValues))){
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
				}
				else{
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
				}
			}
		}
	}
	if((empty($AggregationMappings)) AND (empty($AggregationMappings2))){
		continue;
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if(!empty($channelMappingsTemps)){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$productMappings		= array();
	if($this->ci->globalConfig['enableAdvanceTaxMapping']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		if(!empty($productMappingsTemps)){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($this->ci->globalConfig['enableTaxMapping']){
		$this->ci->db->reset_query();
		$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
		if(!empty($taxMappingsTemps)){
			foreach($taxMappingsTemps as $taxMappingsTemp){
				if($this->ci->globalConfig['enableAdvanceTaxMapping']){
					if($taxMappingsTemp['stateName']){
						$isStateEnabled	= 1;
					}
					if($taxMappingsTemp['countryName']){
						$isStateEnabled = 1;
					}
				}
			}
			foreach($taxMappingsTemps as $taxMappingsTemp){
				$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
				if($taxMappingsTemp['stateName']){
					foreach($stateTemp as $Statekey => $stateTemps){
						$stateName			= strtolower(trim($stateTemps));
						$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
						$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
						if($this->ci->globalConfig['enableAdvanceTaxMapping']){
							if($isStateEnabled){
								if($account1ChannelId){
									$isChannelEnabled		= 1;
									$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
									foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
										$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
										$taxMappings[strtolower($key)]	= $taxMappingsTemp;
									}
								}
								else{
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'];
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
				}
				else{
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($isStateEnabled){
						if($account1ChannelId){
							$isChannelEnabled	= 1;
							$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
							foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}			
				}
				if(!$isStateEnabled){
					$key							= $taxMappingsTemp['account1TaxId'];
					$taxMappings[strtolower($key)]	= $taxMappingsTemp;
				}
			}
		}
	}
	if($clientcode == 'biscuiteersqbo'){
		$taxMappings	= array();
		//tax is not in scope for biscuiteersqbo
	}
	
	$this->ci->db->reset_query();
	$defaultItemMappingTemps	= $this->ci->db->get_where('mapping_defaultitem',array('account2Id' => $account2Id))->result_array();
	$defaultItemMapping			= array();
	if(!empty($defaultItemMappingTemps)){
		foreach($defaultItemMappingTemps as $defaultItemMappingTemp){
			if($defaultItemMappingTemp['itemIdentifyNominal'] AND $defaultItemMappingTemp['account2ProductID'] AND $defaultItemMappingTemp['account1ChannelId']){
				$allItemNominalChannels	= explode(",",$defaultItemMappingTemp['account1ChannelId']);
				$allIdentifyNominals	= explode(",",$defaultItemMappingTemp['itemIdentifyNominal']);
				if(is_array($allItemNominalChannels)){
					foreach($allItemNominalChannels as $allItemNominalChannelsTemp){
						if(is_array($allIdentifyNominals)){
							foreach($allIdentifyNominals as $allIdentifyNominalTemp){
								$defaultItemMapping[$allItemNominalChannelsTemp][$allIdentifyNominalTemp]	= $defaultItemMappingTemp['account2ProductID'];
							}
						}
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalClassMapping	= array();
	$nominalClassMappings	= $this->ci->db->get_where('mapping_nominalclass',array('account2Id' => $account2Id))->result_array();
	if(!empty($nominalClassMappings)){
		foreach($nominalClassMappings as $nominalClassMappings){
			$account1ChannelIds	= explode(",",trim($nominalClassMappings['account1ChannelId']));
			$account1ChannelIds	= array_filter($account1ChannelIds);
			$account1ChannelIds	= array_unique($account1ChannelIds);
			
			$account1NominalIds	= explode(",",trim($nominalClassMappings['account1NominalId']));
			$account1NominalIds	= array_filter($account1NominalIds);
			$account1NominalIds	= array_unique($account1NominalIds);
			
			if((!empty($account1ChannelIds)) AND (!empty($account1NominalIds))){
				foreach($account1ChannelIds as $account1ChannelIdsClass){
					foreach($account1NominalIds as $account1NominalIdsClass){
						$nominalClassMapping[$account1ChannelIdsClass][$account1NominalIdsClass]	= $nominalClassMappings;
					}
				}
			}
		}
	}
	
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard	= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	
	if(!empty($datas)){
		$OrdersByChannel				= array();
		$OrdersByChannelCurrency		= array();
		$OrdersByChannelCurrencyTaxDate	= array();
		foreach($datas as $orderDatas){
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$taxDate				= '';
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$taxDate				= $rowDatas['invoices'][0]['taxDate'];
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
			$ConsolAPIFieldValueID	= '';
			
			if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
				continue;
			}
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			if($enableAggregation){
				if($AggregationMappings){
					if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
						continue;
					}
					else{
						if($enableConsolidationMappingCustomazation){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']){
								if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
									$ExcludedStringInCustomField		= array();
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
							else{
								if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
									$ExcludedStringInCustomField		= array();
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
					}
				}
				elseif($AggregationMappings2){
					if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
						continue;
					}
					else{
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
								continue;
							}
							else{
								if($enableConsolidationMappingCustomazation){
									if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
									elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
						else{
							if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
								continue;
							}
							else{
								if($enableConsolidationMappingCustomazation){
									if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
									elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
					}
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
							$CustomFieldValueID	= 'NA';
						}
					}
					else{
						if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
							$CustomFieldValueID	= 'NA';
						}
					}
				}
			}
			
			$AggregationMappingData	= array();
			if($AggregationMappings){
				if($AggregationMappings[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
					$CustomFieldValueID		= 'NA';
				}
				else{
					$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
					$CustomFieldValueID		= 'NA';
				}
			}
			elseif($AggregationMappings2){
				if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
				}
				else{
					$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
				}
			}
			
			$consolFrequency		= $AggregationMappingData['consolFrequency'];
			$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
			if(!$consolFrequency){
				$consolFrequency	= 1;
			}
			if($netOffConsolidation){
				continue;
			}
			
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$Acc2Offset		= 0;
			$diff			= $BPDateOffset - $Acc2Offset;
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff	.= ' hour';
				$date->modify($diff);
			}
			$taxDate		= $date->format('Ymd');
			
			//consolFrequency : 1 => Daily, 2 => Monthly
			if($consolFrequency == 1){
				$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
				
				if($clientcode != 'monicaandandy'){
					if($taxDate > $fetchTaxDate){
						continue;
					}
				}
				else{
					$invoiceMonth	= date('m',strtotime($taxDate));
					$currentMonth	= date('m');
					$currentDate	= date('d');
					
					if($invoiceMonth == $currentMonth){
						continue;
					}
					else{
						if($currentDate < 3){
							continue;
						}
					}
				}
				
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
					}
				}
			}
			elseif($consolFrequency == 2){
				$invoiceMonth	= date('m',strtotime($taxDate));
				$currentMonth	= date('m');
				
				if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
					continue;
				}
				
				$batchKey	= date('y-m',strtotime($taxDate));
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
					}
				}
			}
		}
		
		if($OrdersByChannelCurrencyTaxDate){
			foreach($OrdersByChannelCurrencyTaxDate as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $rootCurrency => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$customOrderDescription	= '';
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $rootCurrency;
							$CurrencyCode			= $rootCurrency;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$results				= array();
							$AggregationMapping		= array();
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$consolFrequency		= 0;
							
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('sales_order',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							
							$ChannelCustomer	= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef	= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount		= 0;
							$BPTotalAmountBase	= 0;
							$OrderCount			= 0;
							$invoiceLineCount	= 0;
							$linNumber			= 1;
							$request			= array();
							$InvoiceLineAdd		= array();
							$AllorderId			= array();
							$ProductArray		= array();
							$TotalTaxLines		= array();
							$ProductArrayCount	= 0;
							$totalItemDiscount	= 0;
							$exchangeRate		= 1;
							$taxDate				= '';
							$dueDate				= '';
							$BPChannelName			= '';
							$aggreagationID			= '';
							$ReferenceNumber		= '';
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$bpconfig			= $this->ci->account1Config[$orderDatas['account1Id']];
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$exchangeRate		= 1;
									$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									$countryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(($clientcode == 'teeturtleqbo') AND ($SalesChannel == 10)){
										if((isset($rowDatas['customFields']['PCF_CONSOLID'])) AND (strlen(trim($rowDatas['customFields']['PCF_CONSOLID'])) > 0)){
											$customOrderDescription	= trim($rowDatas['customFields']['PCF_CONSOLID']);
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
												}
											}
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bundleParentID	= '';
										$isBundleChild	= $rowdatass['composition']['bundleChild'];
										if($isBundleChild){
											$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
										}
										
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										if($isBundleChild AND $bundleParentID){
											$bpTaxID	= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
										}
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $bpconfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										$taxMapping				= array();
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowdatass['rowValue']['rowNet']['value'] == 0){
												continue;
											}
										}
										
										if($kidsTaxCustomField){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
											}
										}
										
										$taxMappingKey		= $rowdatass['rowValue']['taxClassId'];
										$taxMappingKey1		= $rowdatass['rowValue']['taxClassId'];
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$countryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$countryIsoCode;
											}
										}
										
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
										if($taxMapping){
											if($taxMapping['account2LineTaxId']){
												$bpTaxID 	= $taxMapping['account2LineTaxId'];
												if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsLineTaxId'])){
													$bpTaxID  = $taxMapping['account2KidsLineTaxId'];
												}
											}
											else{
												$bpTaxID 	= $config['salesNoTaxCode'];
											}
										}
										else{
											$bpTaxID 	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										$nominalClassID		= 'NA';
										if((is_array($nominalClassMapping)) AND (!empty($nominalClassMapping)) AND (isset($nominalClassMapping[strtolower($SalesChannel)][$bpItemNonimalCode]))){
											$nominalClassID	= $nominalClassMapping[strtolower($SalesChannel)][$bpItemNonimalCode]['account2ClassId'];
										}
										
										if(!$AggregationMapping['SendTaxAsLine']){
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$nominalClassID][$bpTaxID])){
														$ProductArray['discount'][$nominalClassID][$bpTaxID]['TotalNetAmt']		+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$nominalClassID][$bpTaxID]['TotalNetAmt']		= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$nominalClassID][$bpTaxID])){
																	$ProductArray['discountCoupon'][$nominalClassID][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$nominalClassID][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$nominalClassID][$bpTaxID][$customItemIdQbo])){
														$ProductArray['customItems'][$nominalClassID][$bpTaxID][$customItemIdQbo]['TotalNetAmt']		+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$nominalClassID][$bpTaxID][$customItemIdQbo]['TotalNetAmt']		= $rowNet;
													}
												}
												else{
													if(isset($ProductArray['consolItems'][$nominalClassID][$bpTaxID])){
														$ProductArray['consolItems'][$nominalClassID][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													}
													else{
														$ProductArray['consolItems'][$nominalClassID][$bpTaxID]['TotalNetAmt']		= $rowNet;
													}
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$nominalClassID][$bpTaxID])){
													$ProductArray['shipping'][$nominalClassID][$bpTaxID]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$nominalClassID][$bpTaxID]['TotalNetAmt']		= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$nominalClassID][$bpTaxID])){
													$ProductArray['giftcard'][$nominalClassID][$bpTaxID]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$nominalClassID][$bpTaxID]['TotalNetAmt']		= $rowNet;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$nominalClassID][$bpTaxID])){
													$ProductArray['couponitem'][$nominalClassID][$bpTaxID]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$nominalClassID][$bpTaxID]['TotalNetAmt']	= $rowNet;
												}
											}
										}
										else{
											if($bpdiscountPercentage > 0){
												$discountPercentage = 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice = $productPrice * $quantitymagnitude;
												}
												else{
													$originalPrice = round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$nominalClassID])){
														$ProductArray['discount'][$nominalClassID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$nominalClassID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= $productPrice * $quantitymagnitude;
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$nominalClassID])){
																	$ProductArray['discountCoupon'][$nominalClassID]['TotalNetAmt']		+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$nominalClassID]['TotalNetAmt']		= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if((!empty($defaultItemMapping)) AND (isset($defaultItemMapping[$SalesChannel][$bpItemNonimalCode]))){
													$customItemIdQbo	= $defaultItemMapping[$SalesChannel][$bpItemNonimalCode];
													if(isset($ProductArray['customItems'][$nominalClassID][$customItemIdQbo])){
														$ProductArray['customItems'][$nominalClassID][$customItemIdQbo]['TotalNetAmt']		+= $rowNet;
													}
													else{
														$ProductArray['customItems'][$nominalClassID][$customItemIdQbo]['TotalNetAmt']		= $rowNet;
													}
												}
												else{
													if(isset($ProductArray['aggregationItem'][$nominalClassID])){
														$ProductArray['aggregationItem'][$nominalClassID]['TotalNetAmt']		+= $rowNet;
													}
													else{
														$ProductArray['aggregationItem'][$nominalClassID]['TotalNetAmt']		= $rowNet;
													}
												}
												if(isset($ProductArray['allTax'][$nominalClassID])){
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$nominalClassID])){
													$ProductArray['shipping'][$nominalClassID]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$nominalClassID]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$nominalClassID])){
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$nominalClassID])){
													$ProductArray['giftcard'][$nominalClassID]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$nominalClassID]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$nominalClassID])){
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$nominalClassID])){
													$ProductArray['couponitem'][$nominalClassID]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$nominalClassID]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$nominalClassID])){
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$nominalClassID]['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									$dueDate		= $rowDatas['invoices']['0']['dueDate'];
									$taxDate		= $rowDatas['invoices']['0']['taxDate'];
									$orderId		= $orderDatas['orderId'];
									$AllorderId[]	= $orderId;
									$BPChannelName	= $orderDatas['channelName'];
									$CurrencyCode	= $rowDatas['currency']['orderCurrencyCode'];
									$exchangeRate	= $rowDatas['currency']['exchangeRate'];
									
									if($orderForceCurrency){
										$CurrencyCode	= strtoupper($brightpearlConfig['currencyCode']);
										$exchangeRate	= 1;
									}
									
									$BPTotalAmount	+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$OrderCount++;
								}
								if($ProductArray){
									if(!$AggregationMapping['SendTaxAsLine']){
										foreach($ProductArray as $keyproduct11 => $ProductArrayTemp){
											foreach($ProductArrayTemp as $keyproduct22 => $ProductArrayTempTemp){
												foreach($ProductArrayTempTemp as $keyproduct33 => $ProductArrayTempTempTemp){
													if(($keyproduct11 == 'shipping') OR ($keyproduct11 == 'giftcard') OR ($keyproduct11 == 'couponitem') OR ($keyproduct11 == 'discount') OR ($keyproduct11 == 'discountCoupon')){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> "Consolidation of ".$OrderCount." invoices",
															'Amount'				=> sprintf("%.4f",$ProductArrayTempTempTemp['TotalNetAmt']),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $AggregationMapping['AggregationItem']),
																'Qty'					=> 1,
																'UnitPrice'				=> sprintf("%.4f",$ProductArrayTempTempTemp['TotalNetAmt']),
																'TaxCodeRef'			=> array('value'=> $keyproduct33),
															),
														);
														if($keyproduct22 != 'NA'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $keyproduct22);
														}
														if($keyproduct11 == 'shipping'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['shippingItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Shipping';
														}
														if($keyproduct11 == 'giftcard'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['giftCardItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'GiftCard';
														}
														if($keyproduct11 == 'couponitem'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon';
														}
														if($keyproduct11 == 'discount'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['discountItem'];
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $ProductArrayTempTempTemp['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $ProductArrayTempTempTemp['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Item Discount';
														}
														if($keyproduct11 == 'discountCoupon'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $ProductArrayTempTempTemp['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $ProductArrayTempTempTemp['TotalNetAmt']));
															$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon Discount';
														}
														$invoiceLineCount++;
													}
													elseif($keyproduct11 == 'consolItems'){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> "Consolidation of ".$OrderCount." invoices",
															'Amount'				=> sprintf("%.4f",$ProductArrayTempTempTemp['TotalNetAmt']),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $AggregationMapping['AggregationItem']),
																'Qty'					=> 1,
																'UnitPrice'				=> sprintf("%.4f",$ProductArrayTempTempTemp['TotalNetAmt']),
																'TaxCodeRef'			=> array('value'=> $keyproduct33),
																'ItemAccountRef'		=> array('value'=> $config['IncomeAccountRef']),
															),
														);
														if($keyproduct22 != 'NA'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']	= array('value' => $keyproduct22);
														}
														$invoiceLineCount++;
													}
													elseif($keyproduct11 == 'customItems'){
														foreach($ProductArrayTempTempTemp as $keyproduct44 => $ProductArrayTempTempTempTemp){
															$InvoiceLineAdd[$invoiceLineCount]	= array(
																'LineNum'				=> $linNumber++,
																'Description'			=> 'Special Item',
																'Amount'				=> sprintf("%.4f",$ProductArrayTempTempTempTemp['TotalNetAmt']),
																'DetailType'			=> 'SalesItemLineDetail',
																'SalesItemLineDetail'	=> array(
																	'ItemRef'				=> array('value' => $keyproduct44),
																	'Qty'					=> 1,
																	'UnitPrice'				=> sprintf("%.4f",$ProductArrayTempTempTempTemp['TotalNetAmt']),
																	'TaxCodeRef'			=> array('value' => $keyproduct33),
																),
															);
															if($keyproduct22 != 'NA'){
																$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']			= array('value' => $keyproduct22);
															}
															$invoiceLineCount++;
														}
													}
												}
											}
										}
									}
									else{
										foreach($ProductArray as $keyproduct11 => $ProductArrayTemp){
											foreach($ProductArrayTemp as $keyproduct22 => $ProductArrayTempTemp){
												if(($keyproduct11 == 'shipping') OR ($keyproduct11 == 'giftcard') OR ($keyproduct11 == 'couponitem') OR ($keyproduct11 == 'aggregationItem') OR ($keyproduct11 == 'discount') OR ($keyproduct11 == 'discountCoupon')){
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> "Consolidation of ".$OrderCount." invoices",
														'Amount'				=> sprintf("%.4f",$ProductArrayTempTemp['TotalNetAmt']),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $AggregationMapping['AggregationItem']),
															'Qty'					=> 1,
															'UnitPrice'				=> sprintf("%.4f",$ProductArrayTempTemp['TotalNetAmt']),
															'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
														),
													);
													if($keyproduct22 != 'NA'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']			= array('value' => $keyproduct22);
													}
													if($keyproduct11 == 'shipping'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['shippingItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Shipping';
													}
													if($keyproduct11 == 'giftcard'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['giftCardItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'GiftCard';
													}
													if($keyproduct11 == 'couponitem'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon';
													}
													if($keyproduct11 == 'discount'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['discountItem'];
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $ProductArrayTempTemp['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $ProductArrayTempTemp['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Item Discount';
													}
													if($keyproduct11 == 'discountCoupon'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ItemRef']['value']	= $config['couponItem'];
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['UnitPrice']			= sprintf("%.4f",((-1) * $ProductArrayTempTemp['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Amount']									= sprintf("%.4f",((-1) * $ProductArrayTempTemp['TotalNetAmt']));
														$InvoiceLineAdd[$invoiceLineCount]['Description']								= 'Coupon Discount';
													}
													$invoiceLineCount++;
												}
												elseif($keyproduct11 == 'allTax'){
													if($ProductArrayTempTemp['TotalNetAmt'] <= 0){continue;}
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'LineNum'				=> $linNumber++,
														'Description'			=> 'Total Tax Amount',
														'Amount'				=> sprintf("%.4f",$ProductArrayTempTemp['TotalNetAmt']),
														'DetailType'			=> 'SalesItemLineDetail',
														'SalesItemLineDetail'	=> array(
															'ItemRef'				=> array('value' => $AggregationMapping['defaultTaxItem']),
															'Qty'					=> 1,
															'UnitPrice'				=> sprintf("%.4f",$ProductArrayTempTemp['TotalNetAmt']),
															'TaxCodeRef'			=> array('value'=> $config['salesNoTaxCode']),
														),
													);
													if($keyproduct22 != 'NA'){
														$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']			= array('value' => $keyproduct22);
													}
													$invoiceLineCount++;
												}
												elseif($keyproduct11 == 'customItems'){
													foreach($ProductArrayTempTemp as $keyproduct33	=> $ProductArrayTempTempTemp){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'LineNum'				=> $linNumber++,
															'Description'			=> 'Special Item',
															'Amount'				=> sprintf("%.4f",$ProductArrayTempTempTemp['TotalNetAmt']),
															'DetailType'			=> 'SalesItemLineDetail',
															'SalesItemLineDetail'	=> array(
																'ItemRef'				=> array('value' => $keyproduct33),
																'Qty'					=> 1,
																'UnitPrice'				=> sprintf("%.4f",$ProductArrayTempTempTemp['TotalNetAmt']),
																'TaxCodeRef'			=> array('value' => $config['salesNoTaxCode']),
															),
														);
														if($keyproduct22 != 'NA'){
															$InvoiceLineAdd[$invoiceLineCount]['SalesItemLineDetail']['ClassRef']			= array('value' => $keyproduct22);
														}
														$invoiceLineCount++;
													}
												}
											}
										}
									}
								}
								//taxdate chanages
								$BPDateOffset	= (int)substr($taxDate,23,3);
								$Acc2Offset		= 0;
								$diff			= $BPDateOffset - $Acc2Offset;
								$date1			= new DateTime($dueDate);
								$date			= new DateTime($taxDate);
								$BPTimeZone		= 'GMT';
								$date1->setTimezone(new DateTimeZone($BPTimeZone));
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff > 0){
									$diff			.= ' hour';
									$date->modify($diff);
									$date1->modify($diff);
								}
								$taxDate			= $date->format('Y-m-d');
								$dueDate			= $date1->format('Y-m-d');
							}
							else{
								continue;
							}
							if($InvoiceLineAdd){
								if($consolFrequency == 1){
									$sendTaxDate		= $taxDate;
									$sendDueDate		= $dueDate;
									$RefForInvoice		= $taxDate;
									$ReferenceNumber	= date('Ymd',strtotime($RefForInvoice)).'-'.$uniqueInvoiceRef.'-'.$CurrencyCode;
								}
								else{
									$isLeap				= 0;
									$invMonth			= (int)substr($taxDateBP,3,2);
									$invYear			= (int)substr($taxDateBP,0,2);
									$invYearFull		= '20'.$invYear;
									
									if(((int)$invYearFull % 4) == 0){
										if(((int)$invYearFull % 100) == 0){
											if(((int)$invYearFull % 400) == 0){
												$isLeap	= 1;
											}
											else{
												$isLeap	= 0;
											}
										}
										else{
											$isLeap	= 1;
										}
									}
									else{
										$isLeap	= 0;
									}
									
									$invDate	= $dateResults[$invMonth]['lastDate'];
									if($invMonth == 2){
										if($isLeap){
											$invDate	= $dateResults[$invMonth]['lastDate2'];										
										}
									}
									$invMonthName	= $dateResults[$invMonth]['name'];
									if(strlen($invMonth) == 1){
										$invMonth	= (string)('0'.$invMonth);
									}
									$sendTaxDate		= $invYearFull.'-'.$invMonth.'-'.$invDate;
									$sendDueDate		= $sendTaxDate;
									$RefForInvoice		= $invMonthName.$invYear;
									$ReferenceNumber	= $RefForInvoice.'-'.$uniqueInvoiceRef.'-'.$CurrencyCode;
								}
								
								$aggreagationID			= uniqid((strtoupper(trim($BPChannelName))).'-');
								
								//newCode for add suffix in invoice Number::
								$invoiceNumberSuffix	= '';
								$allSameRefConsols		= $this->ci->db->like('invoiceRef', $ReferenceNumber, 'both')->select('DISTINCT createOrderId',false)->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
								
								if(count($allSameRefConsols) > 0){
									$invoiceNumberSuffix	= '_'.(count($allSameRefConsols));
								}
								
								if(strlen($invoiceNumberSuffix) > 0){
									$ReferenceNumber	= $ReferenceNumber.$invoiceNumberSuffix;
								}
								//newCode for add suffix in invoice Number::
								
								$ReferenceNumber		= substr($ReferenceNumber,0,21);
								$CheckReferenceNumber	= $this->ci->db->get_where('sales_order',array('invoiceRef' => $ReferenceNumber, 'account2Id' => $account2Id))->row_array();
								if($CheckReferenceNumber){
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('message' => 'DocNumber is already exists in QBO'));
									continue;
								}
								
								$invoiceLineAddNew		= array();
								$invoiceLineFormatted	= array();
								$newItemLineCount		= 0;
								foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $AggregationMapping['AggregationItem']){
										$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['Description'] == 'Special Item'){
										$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['discountItem']){
										$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['couponItem']){
										$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['shippingItem']){
										$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $AggregationMapping['defaultTaxItem']){
										$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['SalesItemLineDetail']['ItemRef']['value'] == $config['giftCardItem']){
										$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
										continue;
									}
								}
								
								ksort($invoiceLineAddNew);
								foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
									foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
										$invoiceLineFormatted[$newItemLineCount]			= $invoiceLineAddNewTempTemp;
										$invoiceLineFormatted[$newItemLineCount]['LineNum']	= ($newItemLineCount + 1);
										$newItemLineCount++;
									}
								}
								if($invoiceLineFormatted){
									$InvoiceLineAdd		= $invoiceLineFormatted;
									$invoiceLineCount	= $newItemLineCount;
									$linNumber			= ($newItemLineCount + 1);
								}
								
								//chnageReq added on 8th fab 2023 req by Tushar
								if($clientcode == 'aimeeqbo'){
									foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
										if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
											$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= 'Shipping Charges';
										}
									}
								}
								
								foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
									if($invoiceLineAddNewTemps['Description'] == 'Special Item'){
										$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= "Consolidation of ".$OrderCount." invoices";
										/* unset($InvoiceLineAdd[$lineItemIdNewTemp]['Description']); */
									}
								}
								
								$lineLevelClassId	= '';
								if((is_array($channelMappings)) AND (!empty($channelMappings)) AND ($SalesChannel) AND (isset($channelMappings[$SalesChannel]))){
									$lineLevelClassId	= $channelMappings[$SalesChannel]['account2ChannelId'];
								}
								if($lineLevelClassId){
									foreach($InvoiceLineAdd as $InvoiceLineAddKey => $InvoiceLineAddTempss){
										if((isset($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) AND (strlen(trim($InvoiceLineAddTempss[$InvoiceLineAddTempss['DetailType']]['ClassRef']['value'])) > 1)){
											continue;
										}
										else{
											$InvoiceLineAdd[$InvoiceLineAddKey][$InvoiceLineAddTempss['DetailType']]['ClassRef']['value']	= $lineLevelClassId;
										}
									}
								}
								
								if(($clientcode == 'teeturtleqbo') AND (strlen(trim($customOrderDescription)) > 0)){
									foreach($InvoiceLineAdd as $lineItemIdNewTemp => $invoiceLineAddNewTemps){
										if(substr_count(strtolower($invoiceLineAddNewTemps['Description']),'consolidation of')){
											$InvoiceLineAdd[$lineItemIdNewTemp]['Description']	= trim($customOrderDescription);
										}
									}
								}
								
								
								$request			= array(
									'DocNumber'			=> $ReferenceNumber,
									'CustomerRef'		=> array('value' => $ChannelCustomer),
									'Line'				=> $InvoiceLineAdd,
									'ExchangeRate'		=> sprintf("%.4f",(1 / $exchangeRate)),
									'CurrencyRef'		=> array('value' => $CurrencyCode),
									'TxnDate'			=> $sendTaxDate,
									'DueDate'			=> $sendTaxDate,
								);
								if(isset($channelMappings[$SalesChannel])){
									$request['ClassRef']	= array('value' => $channelMappings[$SalesChannel]['account2ChannelId']);
								}
							}
							
							if(($config['defaultCurrrency']) AND ($bpconfig['currencyCode'] != $config['defaultCurrrency'])){
								$exRate = $this->getQboExchangeRateByDb($account2Id,$CurrencyCode,$config['defaultCurrrency'],$taxDate);
								if($exRate){
									$request['ExchangeRate'] = $exRate;
								}
								else{
									$exRate = $exchangeRatetemp[strtolower($CurrencyCode)][strtolower($config['defaultCurrrency'])]['Rate'];
									if($exRate){
										$request['ExchangeRate'] = $exRate;
									}
									else{
										echo 'ExchangeRate Not found Line no - ';echo __LINE__; continue;
										unset($request['ExchangeRate']);
									}
								}
							}
							if($request AND $InvoiceLineAdd){
								if($disableSalesPosting){
									$createdRowData	= array(
										'Request data	: '	=> $request,
										'Response data	: '	=> 'POSTING_IS_DISABLED',
									);
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
								}
								else{
									$url		= 'invoice?minorversion=37';
									$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
									$createdRowData	= array(
										'Request data	: '	=> $request,
										'Response data	: '	=> $results,
									);
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
								}
							}
							else{
								continue;
							}
							
							if((isset($results['Invoice']['Id']))){
								$consolPostOptions	= 0;
								if($disableCOGSPosting){
									$consolPostOptions	= 2;
								}
								
								$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoice']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'IsJournalposted' => $journalAggregation, 'qboContactID' => $ChannelCustomer, 'consolPostOptions'=> $consolPostOptions));
								
								$QBOTotalAmount		= $results['Invoice']['TotalAmt'];
								if($orderForceCurrency){
									$NetRoundOff	= $BPTotalAmountBase - $QBOTotalAmount;
								}
								else{
									$NetRoundOff	= $BPTotalAmount - $QBOTotalAmount;
								}
								$RoundOffCheck		= abs($NetRoundOff);
								$RoundOffCheck		= sprintf("%.4f",$RoundOffCheck);
								$RoundOffApplicable = 0;
								if($RoundOffCheck != 0){
									if($RoundOffCheck < 0.99){
										$RoundOffApplicable = 1;
									}
									if($RoundOffApplicable){
										$InvoiceLineAdd[$invoiceLineCount] = array(
											'LineNum'				=> $linNumber++,
											'Description'			=> 'RoundOffItem',
											'Amount'				=> sprintf("%.4f",$NetRoundOff),
											'DetailType'			=> 'SalesItemLineDetail',
											'SalesItemLineDetail'	=> array(
												'ItemRef' 				=> array(
													'value'					=> $config['roundOffItem']
												),
												'Qty' 					=> 1,
												'UnitPrice' 			=> sprintf("%.4f",$NetRoundOff),
												'TaxCodeRef' 			=> array(
													'value'					=> $config['salesNoTaxCode'] 
												),
											),
										);
										$request['Line']		= $InvoiceLineAdd;
										$request['SyncToken']	= 0;
										$request['Id']			= $results['Invoice']['Id'];
										$url					= 'invoice?minorversion=37';  
										$results2				= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
										$createdRowData['Rounding Request data	: ']	= $request;
										$createdRowData['Rounding Response data	: ']	= $results2;
										if((isset($results['Invoice']['Id']))){
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoice']['Id'], 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'createdRowData' => json_encode($createdRowData)));
										}
										else{
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('message' => 'Unablle to add Rounding Item' , 'createdRowData' => json_encode($createdRowData)));
										}
									}
								}
							}
							else{
								if($disableSalesPosting){
									$fakecreateOrderId	= uniqid("QBO_ID_").strtotime("now");
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'IsJournalposted' => $journalAggregation, 'qboContactID' => $ChannelCustomer, 'consolPostOptions'=> 1));
								}
							}
						}
					}
				}
			}
		}
	}
}
$this->postConsolidatedJournal();
$this->postaggregationSalesPayment();