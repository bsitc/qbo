<?php
//qbo		:	PostConsolidationSales Payment on QBO
$this->reInitialize();
$exchangeRates				= $this->getExchangeRate();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$disableConsolSOpayments	= $this->ci->globalConfig['disableConsolSOpayments'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($disableConsolSOpayments){continue;}
	
	$config			= $this->accountConfig[$account2Id];
	$exchangeRate	= $exchangeRates[$account2Id];
	
	$this->ci->db->reset_query();
	$datas			= $this->ci->db->order_by('orderId', 'desc')->get_where('sales_order',array('isNetOff' => 0, 'paymentDetails<>' => '', 'isPaymentCreated' => 0, 'status > ' => '0','account2Id' => $account2Id,'sendInAggregation' => 1))->result_array();
	
	if(!$datas){continue;}
	
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if($enableAggregation){
		$this->ci->db->reset_query();
		$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
		if($AggregationMappingsTemps){
			foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
				$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
				$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
				$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
				$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
				
				if(!$ConsolMappingCustomField AND !$account1APIFieldId){
					$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]								= $AggregationMappingsTemp;
				}
				else{
					if($account1APIFieldId){
						$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
						foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
						}
					}
					else{
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
					}
				}
			}
		}
		if(!$AggregationMappings AND !$AggregationMappings2){
			continue;
		}
	}
	
	$this->ci->db->reset_query();
	$currencyMappingsTemps	= $this->ci->db->get_where('mapping_currency',array('account2Id' => $account2Id))->result_array();
	$currencyMappings		= array();
	if($currencyMappingsTemps){
		foreach($currencyMappingsTemps as $currencyMappingsTemp){
			$currencyMappings[strtolower($currencyMappingsTemp['account1CurrencyId'])]	= $currencyMappingsTemp;
		}
	}
	
	if($datas){
		$journalIds		= array();
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['createOrderId']){continue;}
			
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'qbo'){
						if($paymentDetail['status'] == '0'){
							if($paymentKey){
								$journalIds[]	= $paymentDetail['journalId'];
							}
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		
		$ordersByAggregation	= array();
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['createOrderId']){continue;}
			
			$account1Id				= $orderDatas['account1Id'];
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$sendInAggregation		= $orderDatas['sendInAggregation'];
			$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			$ConsolAPIFieldValueID	= '';
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			if($enableAggregation){
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
						continue;
					}
					if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
						continue;
					}
					if($AggregationMappings[$channelId][$orderCurrencyCode]['disablePayments']){
						continue;
					}
					if($AggregationMappings[$channelId][$orderCurrencyCode]['IsJournalAggregated']){
						continue;
					}
					if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
						if($sendInAggregation){
							$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
						}
						else{
							continue;
						}
					}
					elseif($AggregationMappings[$channelId][$orderCurrencyCode]['IsPaymentAggregated']){
						if($sendInAggregation){
							$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
						}
						else{
							continue;
						}
					}
					else{
						continue;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
						continue;
					}
					if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsJournalAggregated']){
						continue;
					}
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
						continue;
					}
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
						continue;
					}
					if($AggregationMappings2[$channelId][$orderCurrencyCode][$CustomFieldValueID]['disablePayments']){
						continue;
					}
					if($AggregationMappings2[$channelId][$orderCurrencyCode][$CustomFieldValueID]['IsJournalAggregated']){
						continue;
					}
					if($AggregationMappings2[$channelId][$orderCurrencyCode]['NA']['disablePayments']){
						continue;
					}
					if($AggregationMappings2[$channelId][$orderCurrencyCode]['NA']['IsJournalAggregated']){
						continue;
					}
					if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']) OR ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsPaymentAggregated'])){
						if($sendInAggregation){
							$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
						}
						else{
							continue;
						}
					}
					elseif(($AggregationMappings2[$channelId][$orderCurrencyCode][$CustomFieldValueID]['IsPaymentAggregated']) OR ($AggregationMappings2[$channelId][$orderCurrencyCode]['NA']['IsPaymentAggregated'])){
						if($sendInAggregation){
							$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
						}
						else{
							continue;
						}
					}
					else{
						continue;
					}
				}
			}
		}
		
		if($ordersByAggregation){
			foreach($ordersByAggregation as $aggregationId => $allorderDatas){
				$PaymentData			= array();
				$allSentIDS				= array();
				$request				= array();
				$PurachaseLine			= array();
				$allGiftSentIDS			= array();
				$GiftCardAdded			= 0;
				$GiftAmount				= 0;
				$NegativeGiftAmount		= 0;
				$sentableAmount			= 0;
				$QBOCustomerID			= '';
				$QBOTotalAmount			= 0;
				$aggregatedInvoiceTotal	= 0;
				$paymentDate			= date('Y-m-d');
				$CountExchangeRate		= 0;
				$ExchangeRateTotal		= 0;
				$CurrencyRate			= 1;
				$orderForceCurrency		= 0;
				$PaymentMethodRef		= $config['PayType'];
				$DepositToAccountRef	= $config['DepositToAccountRef'];
				$breakLoop1				= 0;
				$allPaidOrdersBPIds		= array();
				$allOrdersPaymentInDB	= array();
				$allOrdersTotalAmtInBP	= array();
				$allOrdersSkippedIds	= array();
				$ConsolMappingData		= array();
				
				$consolInvoiceDate		= '';
				
				foreach($allorderDatas as $orderId => $orderDatas){
					$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
					$QBOorderId				= '';
					$orderId				= $orderDatas['orderId'];
					$createOrderId			= $orderDatas['createOrderId'];
					$QBOorderId				= $createOrderId;
					$account1Id				= $orderDatas['account1Id'];
					$config1				= $this->ci->account1Config[$account1Id];
					$GiftCardPaymentMethod	= $config1['giftCardPayment'];
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					$createdRowData			= json_decode($orderDatas['createdRowData'],true);
					$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
					$channelId				= $rowDatas['assignment']['current']['channelId'];
					$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
					$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
					$totalOrderAmtInBase	= $rowDatas['totalValue']['baseTotal'];
					$ConsolAPIFieldValueID	= '';
					$OrderTotalAdjustmentPayment	= array();
					
					$invoiceDate	= $rowDatas['invoices'][0]['taxDate'];
					$BPDateOffset	= (int)substr($invoiceDate,23,3);
					$QBOoffset		= 0;
					$diff			= 0;
					$diff			= $BPDateOffset - $QBOoffset;
					$date			= new DateTime($invoiceDate);
					$BPTimeZone		= 'GMT';
					$date->setTimezone(new DateTimeZone($BPTimeZone));
					if($diff){
						$diff	.= ' hour';
						$date->modify($diff);
					}
					$invoiceDate		= $date->format('Y-m-d');
					$consolInvoiceDate	= $invoiceDate;
					
					if($this->ci->globalConfig['enableAggregationOnAPIfields']){
						$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
						$APIfieldValueTmps		= '';
						foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
							if(!$APIfieldValueTmps){
								$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
							}
							else{
								$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
							}
						}
						if($APIfieldValueTmps){
							$ConsolAPIFieldValueID	= $APIfieldValueTmps;
						}
					}
					if($ConsolAPIFieldValueID){
						$CustomFieldValueID	= $ConsolAPIFieldValueID;
					}
					
					if($orderDatas['isPaymentCreated']){
						$allOrdersSkippedIds[]	= $orderId;
						continue;
					}
					if($orderDatas['status'] == 4){
						$allOrdersSkippedIds[]	= $orderId;
						continue;
					}
					
					if(!$ConsolMappingData){
						if($AggregationMappings){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']){
								$ConsolMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
							}
							elseif($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]){
								$ConsolMappingData	= $AggregationMappings[$channelId][strtolower($orderCurrencyCode)];
							}
						}
						elseif($AggregationMappings2){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
									$ConsolMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
								}
								elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']){
									$ConsolMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'];
								}
							}
							elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]){
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]){
									$ConsolMappingData	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID];
								}
								elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']){
									$ConsolMappingData	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA'];
								}
							}
						}
					}
					
					if(!$ConsolMappingData){
						break;
					}
					
					$orderForceCurrency		= $ConsolMappingData['orderForceCurrency'];
					$QBOCustomerID			= $ConsolMappingData['account2ChannelId'];
					$QBOTotalAmount			+= $rowDatas['totalValue']['total'];
					$aggregatedInvoiceTotal	= $createdRowData['Response data	: ']['Invoice']['Balance'];
					if(!$paymentDetails){
						$allOrdersSkippedIds[]	= $orderId;
						continue;
					}
					
					foreach($paymentDetails as $key => $paymentDetail){
						if(($paymentDetail['sendPaymentTo'] == 'qbo') AND ($paymentDetail['amount'] != 0)){
							if($allOrdersPaymentInDB[$orderId]){
								$allOrdersPaymentInDB[$orderId]['receiveAmt']	+= $paymentDetail['amount'];
							}
							else{
								$allOrdersPaymentInDB[$orderId]['receiveAmt']	= $paymentDetail['amount'];
							}
						}
					}
					$allOrdersTotalAmtInBP[$orderId]['totalAmt']	= $rowDatas['totalValue']['total'];
					
					$OrderTotalPositivePayment		= array();
					$OrderTotalNegativePayment		= array();
					$OrderTotalAdjustmentPayment	= array();
					$OrderPaymentMethod				= '';
					$OrderPaymentDate				= date('Y-m-d');
					$OrderPayemntCurrencyRate		= 1;
					$OrderPayemntCurrency			= '';
					$paymentSetCurrency				= '';
					$totalAmtInBaseInDB				= 0;
					
					foreach($paymentDetails as $key => $paymentDetail){
						if($orderForceCurrency AND (strtolower($paymentDetail['currency']) != strtolower($rowDatas['currency']['accountingCurrencyCode']))){
							$differenceInNetAmt			= 0;
							$absdifferenceInNetAmt		= 0;
							$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
							$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
							if($paymentDetail['sendPaymentTo'] == 'qbo'){
								$totalAmtInBaseInDB		= ($totalAmtInBaseInDB + $paymentDetail['amount']);
							}
							if($totalOrderAmtInBase AND $totalAmtInBaseInDB){
								if($totalOrderAmtInBase != $totalAmtInBaseInDB){
									$differenceInNetAmt		= ($totalOrderAmtInBase - $totalAmtInBaseInDB);
									$absdifferenceInNetAmt	= abs($differenceInNetAmt);
									if(($absdifferenceInNetAmt) AND ($absdifferenceInNetAmt < 1)){
										if($totalOrderAmtInBase > $totalAmtInBaseInDB){
											$paymentDetail['amount']	= ($paymentDetail['amount'] + $differenceInNetAmt);
										}
										else{
											$paymentDetail['amount']	= ($paymentDetail['amount'] - $absdifferenceInNetAmt);
										}
									}
								}
							}
						}
						
						if($paymentDetail['sendPaymentTo'] == 'qbo'){
							if(($paymentDetail['status'] == 0)){
								$OrderPayemntCurrency		= $paymentDetail['currency'];
								
								if(($clientcode == 'aspectledqbo') OR ($clientcode == 'teeturtleqbo')){
									$paymentDate	= $invoiceDate;
								}
								else{
									if($paymentDetail['paymentDate']){
										$paymentDate	= $paymentDetail['paymentDate'];
										$BPDateOffset	= (int)substr($paymentDate,23,3);
										$QBOoffset		= 0;
										$diff			= 0;
										$diff			= $BPDateOffset - $QBOoffset;
										$date			= new DateTime($paymentDate);
										$BPTimeZone		= 'GMT';
										$date->setTimezone(new DateTimeZone($BPTimeZone));
										if($diff){
											$diff			.= ' hour';
											$date->modify($diff);
										}
										$paymentDate	= $date->format('Y-m-d');
									}
								}
								if(($paymentDetail['paymentType'] == 'RECEIPT') OR ($paymentDetail['paymentType'] == 'CAPTURE')){
									$exRate	= '';
									if(isset($journalDatas[$paymentDetail['journalId']])){
										if($paymentDetail['paymentMethod'] != $GiftCardPaymentMethod){
											$OrderPayemntCurrencyRate	= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
										}
									}
									if($paymentDetail['paymentMethod']){
										if($paymentDetail['paymentMethod'] == $GiftCardPaymentMethod){
											if($paymentDetail['amount'] > 0){
												$GiftAmount			+=	$paymentDetail['amount'];
												$allGiftSentIDS[]	= $key;
											}
										}
										else{
											if($paymentDetail['amount'] > 0){
												$paymentSetCurrency			= $paymentDetail['currency'];
												$OrderPayemntCurrency		= $paymentDetail['currency'];
												$OrderPaymentMethod			= $paymentDetail['paymentMethod'];
												$OrderPaymentDate			= $paymentDate;
												
												if($clientcode != 'anatomicalwwqbo'){
													if(strtolower($OrderPaymentMethod) == 'other'){
														$parentOrderId	= $rowDatas['parentOrderId'];
														if($parentOrderId){
															$parentOrderPaymentDatas		= array();
															$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
															if($parentOrderPaymentDataTemp){
																$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
															}
															if($parentOrderPaymentDatas){
																foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
																	if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
																		if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
																			$OrderPaymentMethod	= $parentOrderPaymentData['paymentMethod'];
																		}
																	}
																}
															}
														}
													}
												}
												
												if(isset($OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT'])){
													$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	+= $paymentDetail['amount'];
												}
												else{
													$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	= $paymentDetail['amount'];
												}
												$allSentIDS[]	= $key;
												$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['Ids'][]		= $key;
												$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['Currency']	= $OrderPayemntCurrency;
												$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['Erate']		= $OrderPayemntCurrencyRate;
											}
											
											if(isset($paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
												$DepositToAccountRef		= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
												$PaymentMethodRef			= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
											}
											
											else if(isset($paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])])){
												$DepositToAccountRef		= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
												$PaymentMethodRef			= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
											}
											
											else if(isset($paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
												$DepositToAccountRef		= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
												$PaymentMethodRef			= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
											}
											
											else if(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
												$DepositToAccountRef		= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
												$PaymentMethodRef			= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['paymentValue'];
											}
										}
									}
								}
								elseif($paymentDetail['paymentType'] == 'PAYMENT'){
									$OrderPaymentMethod	= $paymentDetail['paymentMethod'];
									$OrderPaymentDate	= $paymentDate;
									if($OrderPaymentMethod == $GiftCardPaymentMethod){
										$NegativeGiftAmount						+= abs($paymentDetail['amount']);
										$allGiftSentIDS[]						= $key;
									}
									elseif($OrderPaymentMethod == 'ADJUSTMENT'){
										$allSentIDS[]	= $key;
										if(isset($OrderTotalAdjustmentPayment['AMT'])){
											$OrderTotalAdjustmentPayment['AMT']		+= abs($paymentDetail['amount']);
										}
										else{
											$OrderTotalAdjustmentPayment['AMT']		= abs($paymentDetail['amount']);
										}
										$OrderTotalAdjustmentPayment['Ids'][]	= $key;
									}
									else{
										$allSentIDS[]	= $key;
										
										if($clientcode != 'anatomicalwwqbo'){
											if(strtolower($OrderPaymentMethod) == 'other'){
												$parentOrderId	= $rowDatas['parentOrderId'];
												if($parentOrderId){
													$parentOrderPaymentDatas		= array();
													$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
													if($parentOrderPaymentDataTemp){
														$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
													}
													if($parentOrderPaymentDatas){
														foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
															if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
																if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
																	$OrderPaymentMethod	= $parentOrderPaymentData['paymentMethod'];
																}
															}
														}
													}
												}
											}
										}
										
										if(isset($OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT'])){
											$OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	+= abs($paymentDetail['amount']);
										}
										else{
											$OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	= abs($paymentDetail['amount']);
										}
										$OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['Ids'][]		= $key;
									}
								}
								else{
									continue;
								}
							}
						}
					}
					if($OrderTotalAdjustmentPayment AND $OrderTotalPositivePayment AND $OrderTotalNegativePayment){
						$allPostiveAmt	= 0;
						$allNegativeAmt	= 0;
						$OneRate		= 1;
						$OneCurrency	= '';
						$OneMethod		= '';
						$OneDate		= date('Y-m-d');
						$AllPIds		= array();
						$AllNIds		= array();
						$AllAIds		= array();
						
						foreach($OrderTotalNegativePayment as $Method => $DataWithMethod){
							foreach($DataWithMethod as $Date => $DataWithMethodDate){
								$NewAmt	= 0;
								if($OrderTotalPositivePayment[$Method][$Date] AND ($OrderTotalPositivePayment[$Method][$Date]['AMT'] >= $DataWithMethodDate['AMT'])){
									$NewAmt	= ($OrderTotalPositivePayment[$Method][$Date]['AMT'] - $DataWithMethodDate['AMT']);
									$OrderTotalPositivePayment[$Method][$Date]['AMT']	= $NewAmt;
									$OrderTotalPositivePayment[$Method][$Date]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$Method][$Date]['Ids']);
									unset($OrderTotalNegativePayment[$Method][$Date]);
								}
								else{
									foreach($OrderTotalPositivePayment as $pMethod => $OrderTotalPositivePaymentData){
										$isFound	= 0;
										if($Method  == $pMethod){
											foreach($OrderTotalPositivePaymentData as $pDate => $OrderTotalPositivePaymentDataTemps){
												if($OrderTotalPositivePaymentDataTemps['AMT'] >= $DataWithMethodDate['AMT']){
													$isFound	= 1;
													$NewAmt		= ($OrderTotalPositivePaymentDataTemps['AMT'] - $DataWithMethodDate['AMT']);
													$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= $NewAmt;
													$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
													unset($OrderTotalNegativePayment[$Method][$Date]);
													break;
												}
											}
											if($isFound){
												break;
											}
										}
									}
								}
							}
						}
						
						if($OrderTotalNegativePayment){
							foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
								if(!$dataWithMethod){
									unset($OrderTotalNegativePayment[$Method]);
								}
							}
							if($OrderTotalNegativePayment){
								foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
									foreach($dataWithMethod as $Date => $dataWithMethodDate){
										$isFound2	= 0;
										foreach($OrderTotalPositivePayment as $pMethod => $MethodData){
											foreach($MethodData as $pDate => $MethodDateData){
												if($MethodDateData['AMT'] >= $dataWithMethodDate['AMT']){
													$isFound2	= 1;
													$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= ($MethodDateData['AMT'] - $dataWithMethodDate['AMT']);
													$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
													unset($OrderTotalNegativePayment[$Method][$Date]);
													break;
												}
											}
											if($isFound2){
												break;
											}
										}
									}
								}
							}
						}
						
						foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
							foreach($DataWithMethod as $Date => $DataWithMethodDate){
								$NewAmt	= 0;
								if((isset($OrderTotalAdjustmentPayment['AMT'])) AND ($DataWithMethodDate['AMT'] >= $OrderTotalAdjustmentPayment['AMT'])){
									$NewAmt	= $DataWithMethodDate['AMT'] - $OrderTotalAdjustmentPayment['AMT'];
									if(isset($PaymentData[$Method][$Date]['AMT'])){
										$PaymentData[$Method][$Date]['AMT']				+= $NewAmt;
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
										$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
										unset($OrderTotalAdjustmentPayment);
									}
									else{
										$PaymentData[$Method][$Date]['AMT']				= $NewAmt;
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
										$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
										unset($OrderTotalAdjustmentPayment);
									}
								}
								else{
									if(isset($PaymentData[$Method][$Date]['AMT'])){
										$PaymentData[$Method][$Date]['AMT']				+= $DataWithMethodDate['AMT'];
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
									}
									else{
										$PaymentData[$Method][$Date]['AMT']				= $DataWithMethodDate['AMT'];
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
									}
								}
							}
						}
					}
					elseif($OrderTotalAdjustmentPayment AND $OrderTotalPositivePayment AND !$OrderTotalNegativePayment){
						$allPostiveAmt	= 0;
						$allNegativeAmt	= 0;
						$OneRate		= 1;
						$OneCurrency	= '';
						$OneMethod		= '';
						$OneDate		= date('Y-m-d');
						$AllPIds		= array();
						$AllNIds		= array();
						$AllAIds		= array();
						foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
							foreach($DataWithMethod as $Date => $DataWithMethodDate){
								$NewAmt	= 0;
								if((isset($OrderTotalAdjustmentPayment['AMT'])) AND ($DataWithMethodDate['AMT'] >= $OrderTotalAdjustmentPayment['AMT'])){
									$NewAmt	= $DataWithMethodDate['AMT'] - $OrderTotalAdjustmentPayment['AMT'];
									if(isset($PaymentData[$Method][$Date]['AMT'])){
										$PaymentData[$Method][$Date]['AMT']				+= $NewAmt;
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
										$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
										unset($OrderTotalAdjustmentPayment);
									}
									else{
										$PaymentData[$Method][$Date]['AMT']				= $NewAmt;
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
										$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
										unset($OrderTotalAdjustmentPayment);
									}
								}
								else{
									if(isset($PaymentData[$Method][$Date]['AMT'])){
										$PaymentData[$Method][$Date]['AMT']				+= $DataWithMethodDate['AMT'];
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
									}
									else{
										$PaymentData[$Method][$Date]['AMT']				= $DataWithMethodDate['AMT'];
										$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
										$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
										$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
									}
								}
							}
						}
					}
					elseif($OrderTotalNegativePayment AND $OrderTotalPositivePayment AND !$OrderTotalAdjustmentPayment){
						$allPostiveAmt	= array();
						$allNegativeAmt	= array();
						$OneRate		= 1;
						$OneCurrency	= '';
						$OneMethod		= '';
						$OneDate		= date('Y-m-d');
						$AllPIds		= array();
						$AllNIds		= array();
						$AllAIds		= array();
						
						foreach($OrderTotalNegativePayment as $Method => $DataWithMethod){
							foreach($DataWithMethod as $Date => $DataWithMethodDate){
								$NewAmt	= 0;
								if($OrderTotalPositivePayment[$Method][$Date] AND ($OrderTotalPositivePayment[$Method][$Date]['AMT'] >= $DataWithMethodDate['AMT'])){
									$NewAmt	= ($OrderTotalPositivePayment[$Method][$Date]['AMT'] - $DataWithMethodDate['AMT']);
									$OrderTotalPositivePayment[$Method][$Date]['AMT']	= $NewAmt;
									$OrderTotalPositivePayment[$Method][$Date]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$Method][$Date]['Ids']);
									unset($OrderTotalNegativePayment[$Method][$Date]);
								}
								else{
									foreach($OrderTotalPositivePayment as $pMethod => $OrderTotalPositivePaymentData){
										$isFound	= 0;
										if($Method  == $pMethod){
											foreach($OrderTotalPositivePaymentData as $pDate => $OrderTotalPositivePaymentDataTemps){
												if($OrderTotalPositivePaymentDataTemps['AMT'] >= $DataWithMethodDate['AMT']){
													$isFound	= 1;
													$NewAmt		= ($OrderTotalPositivePaymentDataTemps['AMT'] - $DataWithMethodDate['AMT']);
													$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= $NewAmt;
													$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
													unset($OrderTotalNegativePayment[$Method][$Date]);
													break;
												}
											}
											if($isFound){
												break;
											}
										}
									}
								}
							}
						}
						
						if($OrderTotalNegativePayment){
							foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
								if(!$dataWithMethod){
									unset($OrderTotalNegativePayment[$Method]);
								}
							}
							if($OrderTotalNegativePayment){
								foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
									foreach($dataWithMethod as $Date => $dataWithMethodDate){
										$isFound2	= 0;
										foreach($OrderTotalPositivePayment as $pMethod => $MethodData){
											foreach($MethodData as $pDate => $MethodDateData){
												if($MethodDateData['AMT'] >= $dataWithMethodDate['AMT']){
													$isFound2	= 1;
													$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= ($MethodDateData['AMT'] - $dataWithMethodDate['AMT']);
													$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
													unset($OrderTotalNegativePayment[$Method][$Date]);
													break;
												}
											}
											if($isFound2){
												break;
											}
										}
									}
								}
							}
						}
						
						
						/////////
						if($clientcode == 'anatomicalwwqbo'){
							if($OrderTotalNegativePayment){
								foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
									if(!$dataWithMethod){
										unset($OrderTotalNegativePayment[$Method]);
									}
								}
								if($OrderTotalNegativePayment){
									foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
										foreach($dataWithMethod as $Date => $dataWithMethodDate){
											$isFound2	= 0;
											foreach($OrderTotalPositivePayment as $pMethod => $MethodData){
												foreach($MethodData as $pDate => $MethodDateData){
													if($MethodDateData['AMT'] >= $dataWithMethodDate['AMT']){
														$isFound2	= 1;
														$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= ($MethodDateData['AMT'] - $dataWithMethodDate['AMT']);
														$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
														unset($OrderTotalNegativePayment[$Method][$Date]);
														break;
													}
													elseif($dataWithMethodDate['AMT'] >= $MethodDateData['AMT']){
														$isFound2	= 1;
														$OrderTotalNegativePayment[$Method][$Date]['AMT']	= ($dataWithMethodDate['AMT'] - $MethodDateData['AMT']);
														$OrderTotalNegativePayment[$Method][$Date]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
														unset($OrderTotalPositivePayment[$pMethod][$pDate]);
														break;
													}
												}
												if($isFound2){
													break;
												}
											}
										}
									}
								}
								foreach($OrderTotalPositivePayment as $Method => $dataWithMethod){
									if(!$dataWithMethod){
										unset($OrderTotalPositivePayment[$Method]);
									}
								}
							}
							if($OrderTotalNegativePayment){
								foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
									if(!$dataWithMethod){
										unset($OrderTotalNegativePayment[$Method]);
									}
								}
								if($OrderTotalNegativePayment){
									foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
										foreach($dataWithMethod as $Date => $dataWithMethodDate){
											$isFound2	= 0;
											foreach($OrderTotalPositivePayment as $pMethod => $MethodData){
												foreach($MethodData as $pDate => $MethodDateData){
													if($MethodDateData['AMT'] >= $dataWithMethodDate['AMT']){
														$isFound2	= 1;
														$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= ($MethodDateData['AMT'] - $dataWithMethodDate['AMT']);
														$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
														unset($OrderTotalNegativePayment[$Method][$Date]);
														break;
													}
												}
												if($isFound2){
													break;
												}
											}
										}
									}
								}
							}
						}
						////////
						
						foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
							foreach($DataWithMethod as $Date => $DataWithMethodDate){
								if(isset($PaymentData[$Method][$Date]['AMT'])){
									$PaymentData[$Method][$Date]['AMT']			+= $DataWithMethodDate['AMT'];
									$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
									$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
									$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
								}
								else{
									$PaymentData[$Method][$Date]['AMT']			= $DataWithMethodDate['AMT'];
									$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
									$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
									$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
								}
							}
						}
					}
					elseif(!$OrderTotalNegativePayment AND $OrderTotalPositivePayment AND !$OrderTotalAdjustmentPayment){
						$AllPIds		= array();
						foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
							foreach($DataWithMethod as $Date => $DataWithMethodDate){
								if(isset($PaymentData[$Method][$Date]['AMT'])){
									$PaymentData[$Method][$Date]['AMT']			+= $DataWithMethodDate['AMT'];
									$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
									$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
									$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
								}
								else{
									$PaymentData[$Method][$Date]['AMT']			= $DataWithMethodDate['AMT'];
									$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
									$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
									$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
								}
							}
						}
					}
					else{
						if($GiftAmount OR ($rowDatas['totalValue']['total'] == 0)){
							//noAction
						}
						else{
							$allOrdersSkippedIds[]	= $orderId;
							continue;
							break;
						}
					}
					$allPaidOrdersBPIds[]	= $orderId;
				}
				if($breakLoop1){
					continue;
				}
				
				if($PaymentData OR $GiftAmount){
					$isGiftCardApplicable	= 0;
					$isGiftCardAdded		= 0;
					if($GiftAmount){
						$GiftCardApplicable	= 1;
					}
					
					$aggregatedInvoiceNumber	= 'PMT';
					if($QBOorderId){
						$readurl			= 'invoice/'.$QBOorderId.'?minorversion=41';
						$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
						if(isset($InvoiceResponse['Invoice'])){
							$QBOCustomerID				= $InvoiceResponse['Invoice']['CustomerRef']['value'];
							$QBOTotalAmount				= $InvoiceResponse['Invoice']['TotalAmt'];
							$aggregatedInvoiceTotal		= $InvoiceResponse['Invoice']['Balance'];
							$aggregatedInvoiceNumber	.= $InvoiceResponse['Invoice']['DocNumber'];
						}
					}
					else{
						continue;
					}
					$sentableAmount			= 0;
					$AllSentableAmount		= 0;
					if($PaymentData){
						foreach($PaymentData as $PMethod => $PositiveamountData){
							foreach($PositiveamountData as $PDate => $PositiveamountDataTemp){
								$AllSentableAmount	+= $PositiveamountDataTemp['AMT'];
							}
						}
					}
					$sentableAmount	= $AllSentableAmount;
					if($GiftAmount){
						$sentableAmount	= ($sentableAmount + $GiftAmount);
					}
					if($NegativeGiftAmount){
						$sentableAmount	= ($sentableAmount - $NegativeGiftAmount);
					}
					if(sprintf("%.4f",$sentableAmount) > sprintf("%.4f",$aggregatedInvoiceTotal)){
						continue;
					}
					$GiftOrderIds	= array();
					if($GiftAmount){
						$EditRequest	= array();
						
						$query			= "select * from invoice where id = '".$QBOorderId."'";
						$url			= "query?minorversion=4&query=".rawurlencode($query);
						$qboOrderInfos	= @$this->getCurl($url, 'GET', '', 'json',$account2Id)[$account2Id]; 
						$qboLineNumber	= 1;
						foreach($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'] as $key => $qbLine){
							if($qbLine['DetailType'] == 'SubTotalLineDetail'){
								unset($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][$key]);
							}
							if(@$qbLine['LineNum'] > $qboLineNumber){
								$qboLineNumber	= $qbLine['LineNum'];
							}
						}
						$SentableGiftAmt	= $GiftAmount;
						if($NegativeGiftAmount){
							$SentableGiftAmt	= ($SentableGiftAmt - $NegativeGiftAmount);
						}
						$qboLineNumber++;
						$qboOrderInfos['QueryResponse']['Invoice']['0']['Line'][count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line'])]	= array(
							'LineNum'				=> count($qboOrderInfos['QueryResponse']['Invoice']['0']['Line']) + 1,
							'Amount'				=> (-1) * $SentableGiftAmt,		
							'DetailType'			=> 'SalesItemLineDetail',
							'SalesItemLineDetail'	=> array(
								'ItemRef'				=> array('value' => $config['giftCardItem']),
								'UnitPrice'				=> (-1) * $SentableGiftAmt,
								'Qty'					=> '1',
								'TaxCodeRef' 			=> array('value' => $config['salesNoTaxCode']),
							),
						);
						$rqInvoice		= $qboOrderInfos['QueryResponse']['Invoice']['0'];
						$EditRequest	= array(
							'Id'			=> $rqInvoice['Id'],
							'DocNumber'		=> $rqInvoice['DocNumber'],
							'CustomerRef'	=> array('value' => $rqInvoice['CustomerRef']['value']),
							'BillEmail'		=> $rqInvoice['BillEmail'],
							'SyncToken'		=> $rqInvoice['SyncToken'],
							'domain'		=> $rqInvoice['domain'],
							'TxnDate'		=> $rqInvoice['TxnDate'],
							'DueDate'		=> $rqInvoice['DueDate'],
							'BillAddr'		=> $rqInvoice['BillAddr'],
							'ShipAddr'		=> $rqInvoice['ShipAddr'],
							'ExchangeRate'	=> $rqInvoice['ExchangeRate'],
							'CurrencyRef'	=> $rqInvoice['CurrencyRef'],
							'ClassRef'		=> $rqInvoice['ClassRef'],
							'Line'			=> $rqInvoice['Line'],
						);
						if($config1['currencyCode'] != $config['defaultCurrrency']){
							if(isset($EditRequest['ExchangeRate'])){
								unset($EditRequest['ExchangeRate']);
							}
						}
						if(isset($EditRequest['ExchangeRate'])){
							if(!$EditRequest['ExchangeRate']){
								unset($EditRequest['ExchangeRate']);
							}
						}
						$url			= 'invoice?minorversion=37'; 
						$EditResponse	= @$this->getCurl($url, 'POST', json_encode($EditRequest), 'json',$account2Id)[$account2Id]; 
						$createdRowData['Gift Card Request  : '.$aggregationId]	= $EditRequest;
						$createdRowData['Gift Card Response : '.$aggregationId]	= $EditResponse;
						$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $aggregationId));
						if($EditResponse['Invoice']['Id']){
							$isGiftCardAdded	= 1;
							$dueAmount			= '';
							$readurl			= 'invoice/'.$QBOorderId.'?minorversion=41';
							$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
							if(isset($InvoiceResponse['Invoice'])){
								$dueAmount		= $InvoiceResponse['Invoice']['Balance'];
							}
							$GiftCardAdded		= 1;
							foreach($allorderDatas as $orderId => $orderDatas){
								if(in_array($orderId,$allOrdersSkippedIds)){
									continue;
								}
								$paymentDetails	= array();
								$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
								foreach($paymentDetails as $key => $paymentDetail){
									if(in_array($key,$allGiftSentIDS)){
										$paymentDetails[$key]['status']	= '1';
									}
								}
								$paymentDetails[uniqid()]	= array(
									"giftamount"			=> $SentableGiftAmt,
									'status'				=> '1',
									'AmountCreditedIn'		=> 'QBO',
								);
								$updateArray		= array();
								$GiftOrderIds[$orderId]['paymentDetails']	= json_encode($paymentDetails);
								$allorderDatas[$orderId]['paymentDetails']	= json_encode($paymentDetails);
								$updateArray['paymentDetails']				= json_encode($paymentDetails);
								$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
							}
						}
					}
					if($sentableAmount	> 0){
						if(!$isGiftCardAdded AND $isGiftCardApplicable){
							continue;
						}
						$paymentCounter	= 1;
						foreach($PaymentData as $Method => $PaymentDatasTemp){
							foreach($PaymentDatasTemp as $Date => $PaymentDatasTempData){
								$RequestAmt		= $PaymentDatasTempData['AMT'];
								$request		= array();
								$PurachaseLine	= array();
								if($RequestAmt > 0){
									$allUpdatedIds	= array();
									foreach($PaymentDatasTempData['Ids'] as $IdsData){
										foreach($IdsData as $IdsDatas){
											$allUpdatedIds[]	= $IdsDatas;
										}
									}
									
									$allUpdatedIds	= array_filter($allUpdatedIds);
									$allUpdatedIds	= array_unique($allUpdatedIds);
									
									if(isset($paymentMappings1[$channelId][strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)])){
										$DepositToAccountRef		= $paymentMappings1[$channelId][strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings1[$channelId][strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)]['paymentValue'];
									}
									
									else if(isset($paymentMappings2[$channelId][strtolower($Method)])){
										$DepositToAccountRef		= $paymentMappings2[$channelId][strtolower($Method)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings2[$channelId][strtolower($Method)]['paymentValue'];
									}
									
									else if(isset($paymentMappings3[strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)])){
										$DepositToAccountRef		= $paymentMappings3[strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings3[strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)]['paymentValue'];
									}
									
									else if(isset($paymentMappings[strtolower($Method)])){
										$DepositToAccountRef		= $paymentMappings[strtolower($Method)]['account2PaymentId'];
										$PaymentMethodRef			= $paymentMappings[strtolower($Method)]['paymentValue'];
									}
									
									if($clientcode == 'ecfqbo'){
										if(!$PaymentMethodRef){continue;};
									}
									if(!$QBOCustomerID){continue;};
									if(!$DepositToAccountRef){continue;};
									if(!$RequestAmt){continue;};
									$request	= array(
										'TxnStatus' 			=> 'PAID',
										'CustomerRef' 			=> array('value' => $QBOCustomerID),
										'PaymentRefNum'			=> substr($aggregatedInvoiceNumber,0,21),	//this field have a max lenght (21)
										'DepositToAccountRef'	=> array('value' => $DepositToAccountRef),
										'TxnDate' 				=> $Date,
										'ExchangeRate'			=> sprintf("%.4f",(1 / $PaymentDatasTempData['Erate'])),
										'TotalAmt' 				=> $RequestAmt,
										'Line' 					=> array(
											array(
												'Amount'			=> $RequestAmt,
												'LinkedTxn'			=> array(
													array(
														'TxnId'			=> $QBOorderId,
														'TxnType'		=> "Invoice",
													)
												),
											),
										),
									);
									if($clientcode == 'ecfqbo'){
										$request['PaymentMethodRef']	= array('value' => $PaymentMethodRef);
									}
									
									if($orderForceCurrency){
										unset($request['ExchangeRate']);
									}
									else{
										$NewCurrencyRate	= 0;
										if($PaymentDatasTempData['Currency']){
											if(($config['defaultCurrrency']) AND ($config1['currencyCode'] != $config['defaultCurrrency'])){
												$exRate = $this->getQboExchangeRateByDb($account2Id,$PaymentDatasTempData['Currency'],$config['defaultCurrrency'],$Date);
												if($exRate){
													$request['ExchangeRate'] = $exRate;
												}
												else{
													$exRate	= $exchangeRate[strtolower($PaymentDatasTempData['Currency'])][strtolower($config['defaultCurrrency'])]['Rate'];
													if($exRate){
														$request['ExchangeRate'] = $exRate;
													}
													else{
														echo 'ExchangeRate Not found Line no - 924';continue;
														unset($request['ExchangeRate']);
													}
												}
											}
										}
										if(!$request['ExchangeRate']){
											echo 'ExchangeRate Not found Line no - 933';continue;
											unset($request['ExchangeRate']);
										}
									}
									
									if($request){
										$url		= 'payment';
										$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
										$createdRowData['QBO Payment Request	: '.$paymentCounter.'_'.date('Ymd')]	= $request;
										$createdRowData['QBO Payment Response	: '.$paymentCounter.'_'.date('Ymd')]	= $results;
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $aggregationId));
										
										if($clientcode == 'anatomicalwwqbo'){
											if((is_array($results)) AND (!empty($results)) AND (isset($results['Fault']['Error'][0]['Message']))){
												if(($consolInvoiceDate) AND (isset($results['Fault']['Error'][0]['Message'])) AND (substr_count(strtolower($results['Fault']['Error'][0]['Message']),"account period closed, cannot update through services api"))){
													$request['TxnDate'] = $consolInvoiceDate;
													$url				= 'payment';
													$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
													$createdRowData['QBO Payment Request	: '.$paymentCounter.'_'.date('Ymd')]	= $request;
													$createdRowData['QBO Payment Response	: '.$paymentCounter.'_'.date('Ymd')]	= $results;
													$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $aggregationId));
												}
											}
										}
										$paymentCounter++;
										
										if($results['Payment']['Id']){
											$dueAmount			= '';
											$readurl			= 'invoice/'.$QBOorderId.'?minorversion=41';
											$InvoiceResponse	= $this->getCurl($readurl, 'GET', '', 'json', $account2Id)[$account2Id];
											if(isset($InvoiceResponse['Invoice'])){
												$dueAmount		= $InvoiceResponse['Invoice']['Balance'];
											}
											foreach($allorderDatas as $orderId => $orderDatas){
												if(in_array($orderId,$allOrdersSkippedIds)){
													continue;
												}
												$paymentDetails		= array();
												if($GiftCardAdded){
													$paymentDetails	= json_decode($GiftOrderIds[$orderId]['paymentDetails'],true);
												}
												else{
													$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
												}
												foreach($paymentDetails as $key => $paymentDetail){
													if(in_array($key,$allUpdatedIds)){
														$paymentDetails[$key]['status']	= '1';
														$paymentDetails[$key]['qboPaymentId']	= $results['Payment']['Id'];
													}
												}
												$paymentDetails[$results['Payment']['Id']]	= array(
													"amount" 				=> $RequestAmt,
													'status'				=> '1',
													'AmountCreditedIn'		=> 'QBO',
													'PaymentMethodRef'		=> $PaymentMethodRef,
												);
												$updateArray		= array();
												if($GiftCardAdded){
													$GiftOrderIds[$orderId]['paymentDetails']	= json_encode($paymentDetails);
												}
												$allorderDatas[$orderId]['paymentDetails']	= json_encode($paymentDetails);
												$updateArray['paymentDetails']				= json_encode($paymentDetails);
												$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
											}
										}
									}
								}
								else{
									$allUpdatedIds	= array();
									foreach($PaymentDatasTempData['Ids'] as $IdsData){
										foreach($IdsData as $IdsDatas){
											$allUpdatedIds[]	= $IdsDatas;
										}
									}
									$allUpdatedIds	= array_filter($allUpdatedIds);
									$allUpdatedIds	= array_unique($allUpdatedIds);
									foreach($allorderDatas as $orderId => $orderDatas){
										if(in_array($orderId,$allOrdersSkippedIds)){continue;}
										$paymentDetails		= array();
										if($GiftCardAdded){
											$paymentDetails	= json_decode($GiftOrderIds[$orderId]['paymentDetails'],true);
										}
										else{
											$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
										}
										foreach($paymentDetails as $key => $paymentDetail){
											if(in_array($key,$allUpdatedIds)){
												$paymentDetails[$key]['status']	= '1';
											}
										}
										$updateArray		= array();
										if($GiftCardAdded){
											$GiftOrderIds[$orderId]['paymentDetails']	= json_encode($paymentDetails);
										}
										$allorderDatas[$orderId]['paymentDetails']	= json_encode($paymentDetails);
										$updateArray['paymentDetails']				= json_encode($paymentDetails);
										$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
									}
								}
							}
						}
					}
					
					$NewUpdateArray	= array();
					$batchUpdates	= array();
					foreach($allorderDatas as $orderId => $orderDatas){
						$paymentDetails		= array();
						$totalPaidAmt		= 0;
						if($GiftCardAdded){
							$paymentDetails	= json_decode($GiftOrderIds[$orderId]['paymentDetails'],true);
						}
						else{
							$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
						}
						foreach($paymentDetails as $key => $paymentDetail){
							if(($paymentDetail['sendPaymentTo'] == 'qbo') AND ($paymentDetail['amount'] != 0) AND ($paymentDetail['status'] == 1)){
								$totalPaidAmt		= ($totalPaidAmt + $paymentDetail['amount']);
							}
						}
						if($totalPaidAmt AND $allOrdersTotalAmtInBP[$orderId]['totalAmt']){
							if($totalPaidAmt >= $allOrdersTotalAmtInBP[$orderId]['totalAmt']){
								$NewUpdateArray[$orderId]	= array(
									'orderId'			=> $orderId,
									'isPaymentCreated'	=> '1',
									'status' 			=> '3',
									'paymentStatus'		=> '1',
								);
							}
						}
					}
					if($NewUpdateArray){
						$batchUpdates	= array_chunk($NewUpdateArray,200);
						foreach($batchUpdates as $batchUpdate){
							if($batchUpdate){
								$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('sales_order',$batchUpdate,'orderId');
							}
						}
					}
				}
			}
		}
	}
}