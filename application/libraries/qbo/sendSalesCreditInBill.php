<?php
$this->reInitialize();
$exchangeRates	= $this->getExchangeRate();
if(!$orgObjectId){
	return false;
}
foreach($this->accountDetails as $account2Id => $accountDetails){
	continue;
	
	
	
	
	$exchangeRate		= $exchangeRates[$account2Id];
	$config				= $this->accountConfig[$account2Id];
	$ignoreLineDiscount	= $config['sendNetPriceExcludeDiscount'];
	
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('sales_credit_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$this->ci->db->reset_query();
	$customerMappingsTemps			= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	$customerMappings	= array();
	if($customerMappingsTemps){
		foreach($customerMappingsTemps as $customerMappingsTemp){
			$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$productMappingsTemps			= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
	$productMappings	= array();
	if($productMappingsTemps){
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings	= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps			= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings	= array();
	if($nominalMappingTemps){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if($nominalMappingTemp['account1ChannelId']){
				$nominalMappings[$nominalMappingTemp['account1NominalId']]['channel'][strtolower($nominalMappingTemp['account1ChannelId'])]	= $nominalMappingTemp;
			}
			else{
				if(@!$nominalMappings[$nominalMappingTemp['account1NominalId']])
					$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps			= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings	= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	$genericcustomerMappings	= array();
	if($genericcustomerMappingsTemps){
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
		}
	}
	
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForGiftCard	= explode(",",$config['nominalCodeForGiftCard']);
	
	if($datas){
		foreach($datas as $orderDatas){
			$config1	= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId	= $orderDatas['orderId'];
			if($orderDatas['createOrderId']){
				continue;
			}
			if(!$orderDatas['invoiced']){
				continue;
			}
			$rowDatas						= json_decode($orderDatas['rowData'],true);
			$createdRowData					= json_decode($orderDatas['createdRowData'],true);
			$channelId						= $rowDatas['assignment']['current']['channelId'];
			$billAddress					= $rowDatas['parties']['billing'];
			$shipAddress					= $rowDatas['parties']['delivery'];
			$orderCustomer					= $rowDatas['parties']['customer'];
			$BrightpearlTotalAmount			= $rowDatas['totalValue']['total'];
			$BrightpearlTotalTAX			= $rowDatas['totalValue']['taxAmount'];
			$invoiceLineCount				= 0;
			$isDiscountCouponAdded			= 0;
			$isDiscountCouponTax			= 0;
			$itemDiscountTax				= 0;
			$orderTaxAmount					= 0;
			$taxAmount						= 0;
			$linNumber						= 1;
			$orderTaxId						= '';
			$couponItemLineID				= '';
			$shippingLineID					= '';
			$productUpdateNominalCodeInfos	= array();
			$missingSkus					= array();
			$InvoiceLineAdd					= array();
			$productCreateIds				= array();
			$discountCouponAmt				= array();
			$totalItemDiscount				= array();
			$countryIsoCode					= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState					= strtolower(trim($shipAddress['addressLine4']));
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$productId	= $orderRows['productId'];
				if($orderRows['productId'] <= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID			= $rowId;
					}
				}
			}
			$itemtaxAbleLine		= $config['orderLineTaxCode'];
			$discountTaxAbleLine	= $config['orderLineTaxCode'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($rowId == $couponItemLineID){
					if($orderRows['rowValue']['rowNet']['value'] == 0){
						continue;
					}
				}
				$ItemRefValue	= '';
				$ItemRefName	= '';
				$productId		= $orderRows['productId'];
				$LineTaxId		= '';
				$orderTaxId		= '';
				$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
				$taxMappingKey1	= $orderRows['rowValue']['taxClassId'];
				if($isStateEnabled){
					if($isChannelEnabled){
						$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
						$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$channelId;
					}
					else{
						$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState;
						$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode;
					}
				}
				$taxMappingKey	= strtolower($taxMappingKey);
				$taxMappingKey1	= strtolower($taxMappingKey1);
				if(isset($taxMappings[$taxMappingKey])){
					$taxMapping	= $taxMappings[$taxMappingKey];
				}
				elseif(isset($taxMappings[$taxMappingKey1])){
					$taxMapping	= $taxMappings[$taxMappingKey1];
				}
				elseif(isset($taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId])){
					$taxMapping	= $taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId];
				}
				$LineTaxId  = $taxMapping['account2LineTaxId'];
				$orderTaxId = $taxMapping['account2TaxId'];
				
				if($productId > 1001){
					if(!$productMappings[$productId]['createdProductId']){
						$missingSkus[]	= $orderRows['productSku'];
						continue;
					}
					$ItemRefValue	= $productMappings[$productId]['createdProductId'];
					$ItemRefName	= $productMappings[$productId]['sku'];
				}
				else{
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue		= $config['genericSku'];
						$ItemRefName		= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue		= $config['discountItem'];
						$ItemRefName		= 'Discount Item';
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= 'Coupon Item';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
					}
					else{
						$ItemRefValue		= $config['genericSku'];
						$ItemRefName		= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= 'Coupon Item';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
					}
				}
				$price				=  $orderRows['rowValue']['rowNet']['value']; 
				$orderTaxAmount		+= $orderRows['rowValue']['rowTax']['value'];
				$originalPrice		= $price;
				$discountPercentage	= 0;
				
				if(($orderRows['discountPercentage'] > 0) && (!$ignoreLineDiscount)){
					$discountPercentage	= 100 - $orderRows['discountPercentage'];
					if($discountPercentage == 0){
						$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					}
					else{
						$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
					}
					$tempTaxAmt	= $originalPrice - $price;
					if($tempTaxAmt > 0){
						if(isset($totalItemDiscount[$LineTaxId])){
							$totalItemDiscount[$LineTaxId]	+= $tempTaxAmt;
						}
						else{
							$totalItemDiscount[$LineTaxId]	= $tempTaxAmt;
						}
					}
				}
				else if($isDiscountCouponAdded){
					if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
						$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
						if(!$originalPrice){
							$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
						}
						$discountCouponAmtTemp	= ($originalPrice - $price);
						if($discountCouponAmtTemp > 0){
							if(isset($discountCouponAmt[$LineTaxId])){
								$discountCouponAmt[$LineTaxId]	+= $discountCouponAmtTemp;
							}
							else{
								$discountCouponAmt[$LineTaxId]	= $discountCouponAmtTemp;
							}
						}
					}
				}
				$IncomeAccountRef	= $config['IncomeAccountRef'];	
				if($ItemRefValue){
					$ExpenseAccountRef	= $config['ExpenseAccountRef'];
					if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
						$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
						$nominalMapped		= $nominalMappings[$orderRows['nominalCode']];
						if($channelId){
							if(isset($nominalMapped['channel'])){
								if(isset($nominalMapped['channel'][strtolower($channelId)])){
									$ExpenseAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								}
							}
						}
					}
					$UnitAmount	= 0.00;
					if($originalPrice != 0){
						$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'LineNum'							=> $linNumber++,
						'Description'						=> $orderRows['productName'],
						'Amount'							=> sprintf("%.4f",($UnitAmount*((int)$orderRows['quantity']['magnitude']))),
						'DetailType'						=> 'ItemBasedExpenseLineDetail',
						'ItemBasedExpenseLineDetail'		=> array(
							'BillableStatus'					=> 'NotBillable',
							'ItemRef'							=> array('value' => $ItemRefValue),
							'Qty'								=> (int)$orderRows['quantity']['magnitude'],
							'ClassRef'							=> array('value' => $channelMappings[$channelId]['account2ChannelId']),
							'TaxCodeRef'						=> array('value' => $LineTaxId ),
							'UnitPrice'							=> sprintf("%.4f",($UnitAmount)),
						),
					);
					if(!$LineTaxId){
						unset($InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']);
					}
					$invoiceLineCount++;
				} 
			}
			if($totalItemDiscount){
				foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmount){
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'LineNum'							=> $linNumber++,
						'Description'						=> 'Item Discount',
						'Amount'							=> - sprintf("%.4f",$totalItemDiscountLineAmount),
						'DetailType'						=> 'ItemBasedExpenseLineDetail',
						'ItemBasedExpenseLineDetail'		=> array(
							'ItemRef'							=> array( 'value' => $config['discountItem']),
							'Qty'								=> 1,
							'ClassRef'							=> array('value' => $channelMappings[$channelId]['account2ChannelId']),
							'UnitPrice'							=> sprintf("%.4f",((-1) * $totalItemDiscountLineAmount)),
						),	
					);
					if($TaxID){
						$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
					}
					$invoiceLineCount++;
				}
			}
			if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
				if($discountCouponAmt){
					foreach($discountCouponAmt as $TaxID => $discountCouponLineAmount){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'LineNum'				=> $linNumber++,
							'Description'			=> 'Coupon Discount',
							'Amount'				=> - sprintf("%.4f",($discountCouponLineAmount*1)),
							'DetailType'			=> 'ItemBasedExpenseLineDetail',
							'ItemBasedExpenseLineDetail'	=> array('ItemRef' => array('value'=> $config['couponItem']),
								'Qty'						=> 1,
								'ClassRef'					=> array('value' => $channelMappings[$channelId]['account2ChannelId']),
								'UnitPrice'					=> sprintf("%.4f",((-1) * $discountCouponLineAmount)),
							),
						);
						if($TaxID){
							$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $TaxID);
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['ItemBasedExpenseLineDetail']['TaxCodeRef']		= array('value' => $config['salesNoTaxCode']);
						}
						$invoiceLineCount++;
					}
				}
			}
			if($config['SendTaxAsLineItem']){
				if($BrightpearlTotalTAX > 0){
					$InvoiceLineAdd[$invoiceLineCount] = array(
						'LineNum'				=> $linNumber++,
						'Description'			=> 'Total Tax Amount',
						'Amount'				=> sprintf("%.4f",$BrightpearlTotalTAX),
						'DetailType'			=> 'ItemBasedExpenseLineDetail',
						'ItemBasedExpenseLineDetail'	=> array(
							'ItemRef'						=> array('value' => $config['TaxLineItemCode']),
							'Qty'							=> 1,
							'UnitPrice'						=> sprintf("%.4f",$BrightpearlTotalTAX),
							'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode']),
						),
					);
					$invoiceLineCount++;
				}
			}
			$InvoiceLineAdd[$invoiceLineCount] = array(
				'LineNum'						=> $linNumber++,
				'Description'					=> $rowDatas['invoices']['0']['invoiceReference'],
				'Amount'						=> sprintf("%.4f",((-1) * $BrightpearlTotalAmount)),
				'DetailType'					=> 'AccountBasedExpenseLineDetail',
				'AccountBasedExpenseLineDetail'	=> array(
					'BillableStatus'				=> 'NotBillable',
					'AccountRef'					=> array('value' => $config['SalesCreditBillAccountRef']),
					'TaxCodeRef'					=> array('value' => $config['salesNoTaxCode']),
				),
			);
			$invoiceLineCount++;
			
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('sales_credit_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			
			$dueDate	= date('c');
			$taxDate	= date('c');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= $rowDatas['invoices']['0']['dueDate'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
			}

			//taxdate chanages
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$QBOoffset		= 0;
			$diff			= $BPDateOffset - $QBOoffset;
			$date1			= new DateTime($dueDate);
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date1->setTimezone(new DateTimeZone($BPTimeZone));
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
				$date1->modify($diff);
			}
			$taxDate		= $date->format('Y-m-d');
			$dueDate		= $date1->format('Y-m-d');
			
			$invoiceRef		= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			if($config['UseAsDocNumbersosc']){
				$account1FieldIds	= explode(".",$config['UseAsDocNumbersosc']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$invoiceRef	= $fieldValueTmps;
				}
				else{
					$invoiceRef	= $rowDatas['invoices']['0']['invoiceReference'];
				}
			}
			if(!$invoiceRef){
				$invoiceRef	= $orderId;
			}
			
			foreach($InvoiceLineAdd as $key => $invoiceVal){
				if(!$invoiceVal['ClassRef']['value']){
					unset($InvoiceLineAdd[$key]['ClassRef']['value']);
				}
			}
			$request	= array(
				'DocNumber'						=> $invoiceRef,
				'VendorRef'						=> array('value' => $config['SalesCreditBillVendorRef']),
				'TxnDate'						=> $taxDate,
				'DueDate'						=> $dueDate,
				'Line'							=> $InvoiceLineAdd,
				'ShipAddr'						=> array(
					'Line1'							=> @$shipAddress['addressLine1'],
					'Line2'							=> @$shipAddress['addressLine2'],
					'City'							=> @$shipAddress['addressLine3'],
					'CountrySubDivisionCode'		=> @$shipAddress['addressLine4'],
					'Country'						=> @$shipAddress['countryIsoCode'],
					'PostalCode'					=> @$shipAddress['postalCode'],
				),
				'ExchangeRate'					=> sprintf("%.4f",(1 / $rowDatas['currency']['exchangeRate'])),
				'CurrencyRef'					=> array('value' => $rowDatas['currency']['orderCurrencyCode']),
			);
			if(($config['defaultCurrrency']) && ($config1['currencyCode'] != $config['defaultCurrrency'])){
				$exRate = $this->getQboExchangeRateByDb($account2Id,$rowDatas['currency']['orderCurrencyCode'],$config['defaultCurrrency'],$taxDate);
				if($exRate){
					$request['ExchangeRate'] = $exRate;
				}
				else{
					$exRate = $exchangeRate[strtolower($rowDatas['currency']['orderCurrencyCode'])][strtolower($config['defaultCurrrency'])]['Rate'];
					if($exRate){
						$request['ExchangeRate'] = $exRate;
					}
					else{
						echo 'ExchangeRate Not found Line no - 548';continue;
						unset($request['ExchangeRate']);
					}
				}
				
			}
			$TxnTaxDetailAdded	= 0;
			if($orderTaxId){
				if($orderTaxAmount > 0){
					$TxnTaxDetailAdded	= 1;
					$request['TxnTaxDetail']	= array(
						'TotalTax'					=> $orderTaxAmount,
						'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
					);
					$request['GlobalTaxCalculation']	= 'TaxExcluded';
				}
			}
			if(!$TxnTaxDetailAdded) {
				if(!$orderTaxId){
					$orderTaxId	= $config['TaxCode'];
				}
				$request['TxnTaxDetail']	= array(
					'TotalTax'					=> $orderTaxAmount,
					'TxnTaxCodeRef'				=> array('value' => $orderTaxId),
				);	
				$request['GlobalTaxCalculation']	= 'TaxExcluded';
			}
			$url				= 'bill?minorversion=37';  
			$results			= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData		= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$this->ci->db->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if(@$results['Bill']['Id']){
				$this->ci->db->update('sales_credit_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['Bill']['Id'],'message' => ''),array('id' => $orderDatas['id']));
			}
		}
	}
}
?>