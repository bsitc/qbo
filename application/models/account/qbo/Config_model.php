<?php
#[\AllowDynamicProperties]
class Config_model extends CI_Model{
	public function get($type = ''){
		$data				= array();
		$data['data']		= $this->db->get('account_qbo_config')->result_array();
		$saveAccountTemps	= $this->db->get('account_qbo_account')->result_array();
		$saveAccount		= array();
		foreach($saveAccountTemps as $saveAccountTemp){
			$saveAccount[$saveAccountTemp['id']]	= $saveAccountTemp;
		}
		$data['saveAccount']	= $saveAccount;
		if($type == 'account1'){
			//$data['pricelist']			= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
		}
		else{
			$data['IncomeAccountRef']		= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails(); 
			$data['PaymentMethodRef']		= $this->{$this->globalConfig['account2Liberary']}->getAllPaymentMethod(); 
			$data['getAllTax']				= $this->{$this->globalConfig['account2Liberary']}->getAllTax(); 
			$data['channel']				= $this->{$this->globalConfig['account1Liberary']}->getAllChannel();
			$data['warehouse']				= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
			$data['pricelist']				= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
			$data['prepaymentAccount']		= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
			$data['account1Fieldconfigso']	= $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();
			$data['account1Fieldconfigpo']	= $this->{$this->globalConfig['account1Liberary']}->purchaseOrderFieldConfig();
			$data['Preferences']			= $this->{$this->globalConfig['account2Liberary']}->getPreferences();
		}
		return $data;
	}
	public function delete($id){
		$this->db->where(array('id' => $id))->delete('account_qbo_config');
	}
	public function save($data){
		$qboAccount		= $this->db->get_where('account_qbo_account', array('id' => $data['qboAccountId']))->row_array();
		$data['name']	= $qboAccount['name'];
		
		if(!empty($data)){
			foreach($data as $dataKey => $dataValue){
				if(is_array($data[$dataKey])){
					$dataValue		= array_filter($dataValue);
					$dataValue		= array_unique($dataValue);
					$data[$dataKey]	= implode(",",$dataValue);
				}
			}
		}
		
		if($data['id']){
			$status	= $this->db->where(array('id' => $data['id']))->update('account_qbo_config', $data);
		}
		else{
			$saveConfig	= $this->db->get_where('account_qbo_config', array('qboAccountId' => $data['qboAccountId']))->row_array();
			if($saveConfig){
				$data['id']	= $saveConfig['id'];
				$status		= $this->db->where(array('id' => $data['id']))->update('account_qbo_config', $data);
			}
			else{
				$status		= $this->db->insert('account_qbo_config', $data);
				$data['id']	= $this->db->insert_id();
			}
		}
		$data	= $this->db->get_where('account_qbo_config', array('id' => $data['id']))->row_array();
		if($data['id']){
			$data['status']	= '1';
		}
		return $data;
	}
}