<?php
#[\AllowDynamicProperties]
class Config_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->ci   = get_instance();
    }
    public function get(){
        $data				= array();
        $data['data']		= $this->db->get('aggregation_config')->result_array();
		$saveAccountTemps	= $this->db->get('account_xero_account')->result_array();
		$saveAccount		= array();
		foreach($saveAccountTemps as $saveAccountTemp){
			$saveAccount[$saveAccountTemp['id']]	= $saveAccountTemp;
		}
        $data['saveAccount']	= $saveAccount;
		return $data;
    }
    public function delete($id){
        $this->db->where(array('id' => $id))->delete('aggregation_config');
    }
    public function save($data){
        if($data['id']){
            $status		= $this->db->where(array('id' => $data['id']))->update('aggregation_config', $data);
        } 
        else{
            $saveConfig	= $this->db->get_where('aggregation_config', array('id' => $data['id']))->row_array();
            if ($saveConfig) {
                $data['id']	= $saveConfig['id'];
                $status		= $this->db->where(array('id' => $data['id']))->update('aggregation_config', $data);
            }
            else {
                $status		= $this->db->insert('aggregation_config', $data);
                $data['id']	= $this->db->insert_id();
            }
        }
        $data	= $this->db->get_where('aggregation_config', array('id' => $data['id']))->row_array();
        if ($data['id']) {
            $data['status'] = '1';
        }
        return $data;
    }
}