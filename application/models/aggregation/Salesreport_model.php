<?php
//qbo
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Salesreport_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getSales(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array("order"=> $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status != ''){
					$this->db->where_in('id', $ids)->update('sales_order', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('orderId'))){
				$where['orderId']			= trim($this->input->post('orderId'));
			}
			if(trim($this->input->post('createOrderId'))){
				$where['createOrderId']		= trim($this->input->post('createOrderId'));
			}
			if(trim($this->input->post('totalAmount'))){
				$where['totalAmount']		= trim($this->input->post('totalAmount'));
			}
			if(trim($this->input->post('channelName'))){
				$where['channelName']		= trim($this->input->post('channelName'));
			}
			if(trim($this->input->post('invoiceRef'))){
				$where['invoiceRef']		= trim($this->input->post('invoiceRef'));
			}
			if(trim($this->input->post('bpInvoiceNumber'))){
				$where['bpInvoiceNumber']	= trim($this->input->post('bpInvoiceNumber'));
			}
			if(trim($this->input->post('orderNo'))){
				$where['orderNo']			= trim($this->input->post('orderNo'));
			}
			if (trim($this->input->post('paymentStatus')) >= '0') {
                $where['paymentStatus']		= trim($this->input->post('paymentStatus'));
            }
		}
		if(trim($this->input->post('created_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if(trim($this->input->post('created_to'))){
			$query->where('date(created) < ', "date('" . $this->input->post('created_to') . "')", false);
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get_where('sales_order',array('sendInAggregation' => '1','createOrderId <>' => '','status <>' => ''))->row_array()['countsales'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('created_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if(trim($this->input->post('created_to'))){
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$displayProRowHeader	= array('id', 'channelName', 'orderId', 'bpInvoiceNumber', 'orderNo', 'createOrderId', 'invoiceRef', 'totalAmount', '', '', '', 'paymentStatus', 'taxDate', 'created');
		
		if($this->session->userdata('order')){
			foreach($this->session->userdata('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$status			= array('0' => 'Un-Paid','1' => 'Paid','2' => 'Partially Paid');
		$statusColor	= array('0' => 'default','1' => 'success','2' => 'info');
		$datas			= $query->limit($limit, $start)->select('id,channelName,orderId,createOrderId,bpInvoiceNumber,invoiceRef,totalAmount,paymentStatus,taxDate,created,rowData,orderNo,paymentDetails')->get_where('sales_order',array('sendInAggregation' => '1','createOrderId <>' => '','status <>' => '0'))->result_array();
		
		$AllConsolOrdersID	= array();
		$allCOGSAmountInfo		= array();
		$AllConsolPaymentInfo	= array();
		$allAmazonFeesInfo		= array();
		if($datas){
			foreach($datas as $SODatas){
				$AllConsolOrdersID[]	= $SODatas['orderId'];
			}
			if($AllConsolOrdersID){
				$allAmazonOrdersData	= array();
				$allAmazonOrdersData	= $this->db->where_in('orderId',$AllConsolOrdersID)->select('orderId,amount')->get_where('amazon_ledger')->result_array();
				if($allAmazonOrdersData){
					foreach($allAmazonOrdersData as $allAmazonOrdersDatas){
						if(isset($allAmazonFeesInfo[$allAmazonOrdersDatas['orderId']])){
							$allAmazonFeesInfo[$allAmazonOrdersDatas['orderId']]	+= $allAmazonOrdersDatas['amount'];
						}
						else{
							$allAmazonFeesInfo[$allAmazonOrdersDatas['orderId']]	= $allAmazonOrdersDatas['amount'];
						}
					}
				}
				
				$allCOGSOrderData		= array();
				$allCOGSOrderData		= $this->db->where_in('orderId',$AllConsolOrdersID)->select('orderId,creditAmount')->get_where('cogs_journal')->result_array();
				if($allCOGSOrderData){
					foreach($allCOGSOrderData as $allCOGSOrderDatas){
						if(isset($allCOGSAmountInfo[$allCOGSOrderDatas['orderId']])){
							$allCOGSAmountInfo[$allCOGSOrderDatas['orderId']]	+= $allCOGSOrderDatas['creditAmount'];
						}
						else{
							$allCOGSAmountInfo[$allCOGSOrderDatas['orderId']]	= $allCOGSOrderDatas['creditAmount'];
						}
					}
				}
				
				
				foreach($datas as $AllPaymentdata){
					if($AllPaymentdata['paymentDetails']){
						$TotalOrderPayment	= 0;
						$PaymentParams		= json_decode($AllPaymentdata['paymentDetails'],true);
						if($PaymentParams){
							foreach($PaymentParams as $PaymentParam){
								if($PaymentParam['sendPaymentTo'] == 'qbo'){
									if(isset($AllConsolPaymentInfo[$AllPaymentdata['orderId']])){
										$AllConsolPaymentInfo[$AllPaymentdata['orderId']]	+= $PaymentParam['amount'];
									}
									else{
										$AllConsolPaymentInfo[$AllPaymentdata['orderId']]	= $PaymentParam['amount'];
									}
								}
							}
						}
					}
				}
			}
		}
		
		foreach($datas as $data){
			$params					= json_decode($data['rowData'],true);
			$FeesAmount				= 0;
			$COGSAmount				= 0;
			$FetchedAmount			= 0;
			$bpInvoiceNumber		= '';
			$bpInvoiceNumber		= @$data['bpInvoiceNumber'];
			if(!$bpInvoiceNumber){
				$bpInvoiceNumber	= $params['invoices'][0]['invoiceReference'];
			}
			if($allAmazonFeesInfo[$data['orderId']]){
				$FeesAmount			= $allAmazonFeesInfo[$data['orderId']];
			}
			if($allCOGSAmountInfo[$data['orderId']]){
				$COGSAmount			= $allCOGSAmountInfo[$data['orderId']];
			}
			if($AllConsolPaymentInfo[$data['orderId']]){
				$FetchedAmount		= $AllConsolPaymentInfo[$data['orderId']];
			}
			$records["data"][]		= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$data['channelName'],
				@$data['orderId'],
				@$bpInvoiceNumber,
				@$data['orderNo'],
				@$data['createOrderId'],
				@$data['invoiceRef'],
				@sprintf("%.2f",$data['totalAmount']),
				@sprintf("%.2f",$FeesAmount),
				@sprintf("%.2f",$COGSAmount),
				@sprintf("%.2f",$FetchedAmount),
				'<span class="label label-sm label-' . $statusColor[$data['paymentStatus']] . '">' . $status[$data['paymentStatus']] . '</span>',
				@$data['taxDate'],
				@$data['created'],
				'',
				
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}