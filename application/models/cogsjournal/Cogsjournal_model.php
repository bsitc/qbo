<?php
//qbo
#[\AllowDynamicProperties]
class Cogsjournal_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
    public function fetchCogsjournal($journalsId = '', $accountId = ''){
		if(php_sapi_name() === 'cli'){
			//
		}
		else{
			$this->{$this->globalConfig['account1Liberary']}->fetchBpJournals();
		}
		
		$fetchby			= $journalsId;
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
        $cogsJournalDatass	= $this->{$this->globalConfig['fetchSalesOrder']}->fetchCogsjournal($journalsId, $accountId);
		foreach($cogsJournalDatass as $fetchAccount1Id => $cogsJournalDatassTemps){
			if(isset($cogsJournalDatassTemps['saveTime'])){
				$saveTime	= $cogsJournalDatassTemps['saveTime'] - (60*10);
			}
			
			$inserted			= 0;
			$insertChunk		= 100;
			$batchInsert		= array();
			$cogsJournalDatas	= $cogsJournalDatassTemps['return'];
			foreach($cogsJournalDatas as $account1Id => $cogsJournalData){
				$savedCogsIds		= array();
				$savedCogsDatas		= $this->db->select('journalsId')->get_where('cogs_journal')->result_array();
				if(!empty($savedCogsDatas)){
					$savedCogsIds	= array_column($savedCogsDatas,'journalsId');
					$savedCogsIds	= array_filter($savedCogsIds);
				}
				
				$archivedCogsIds	= array();
				$archivedCogsDatas	= $this->db->select('journalsId')->get_where('cogs_journal_archived')->result_array();
				if(!empty($archivedCogsDatas)){
					$archivedCogsIds	= array_column($archivedCogsDatas,'journalsId');
					$archivedCogsIds	= array_filter($archivedCogsIds);
				}
				
				foreach($cogsJournalData as $journalsId => $row){
					if(!$journalsId){continue;}
					if(in_array($journalsId,$savedCogsIds)){continue;}
					if(in_array($journalsId,$archivedCogsIds)){continue;}
					
					$batchInsert[]	= $row;
				}
			}	
			
			if(!empty($batchInsert)){
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true);
				foreach($batchInserts as $batchInsertTemp){
					$inserted	= $this->db->insert_batch('cogs_journal', $batchInsertTemp);
				}
			}
			if($inserted == 1){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'cogsjournal'.$fetchAccount1Id, 'saveTime' => $saveTime)); 
				}
			}
		}
	}
	public function postCogsjournal($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postCogsjournal($orderId);
	}
	public function postConsolCogsjournal($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postConsolCogsjournal($orderId);
	}
    public function getCogsjournal(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if($this->input->post('order')){
 			$orderData	= array("order"=> $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('cogs_journal', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']		= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']		= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('journalsId'))){
				$where['journalsId']		= trim($this->input->post('journalsId'));
			}
			if(trim($this->input->post('createdJournalsId'))){
				$where['createdJournalsId']	= trim($this->input->post('createdJournalsId'));
			}
			if (trim($this->input->post('orderId'))) {
                $where['orderId']			= trim($this->input->post('orderId'));                
            }
			if(trim($this->input->post('journalTypeCode'))){
				$where['journalTypeCode']	= strtoupper(trim($this->input->post('journalTypeCode')));
			}
			if(trim($this->input->post('OrderType'))){
				$where['OrderType']			= strtoupper(trim($this->input->post('OrderType')));
			}
			if (trim($this->input->post('status')) >= '0') {
                $where['status']			= trim($this->input->post('status'));
            }
			if (trim($this->input->post('isConsolidated')) >= '0') {
                $where['isConsolidated']	= trim($this->input->post('isConsolidated'));
            }
        }
        if (trim($this->input->post('taxDate_from'))) {
            $query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
        }
        if (trim($this->input->post('taxDate_to'))) {
            $query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
        }
		if (trim($this->input->post('created_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
        }
        if (trim($this->input->post('created_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
        }
        if ($where){
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countsales')->get('cogs_journal')->row_array()['countsales'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('taxDate_from'))) {
            $query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
        }
        if (trim($this->input->post('taxDate_to'))) {
            $query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
        }
		if (trim($this->input->post('created_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
        }
        if (trim($this->input->post('created_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
		$status              	= array('0'	=> 'Pending','1' => 'Sent','4' => 'Archive');
        $statusColor         	= array('0'	=> 'default','1' => 'success','4' => 'danger');
        $displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId', 'orderId','journalTypeCode','OrderType','taxDate', 'created','status');
        if ($this->session->userdata('order')) {
            foreach ($this->session->userdata('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas					= $query->select('id,account1Id,account2Id,journalsId,createdJournalsId,createdParams,orderId,journalTypeCode,OrderType,taxDate,created,status,isConsolidated')->limit($limit, $start)->get('cogs_journal')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
        foreach($datas as $data) {
			$invRef			= '';
			$createdParams	= json_decode($data['createdParams'],true);
			$invRef			= $createdParams['Request data	: ']['DocNumber'];
			$viewOrder		= '';
			if($data['createdJournalsId']){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/journal?txnId='.$data['createdJournalsId'].'">View Journal in QBO</a>
								</li>';
			}
			$records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                @$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
                $data['journalsId'],
                @$data['createdJournalsId'],
				$invRef,
                @$data['orderId'], 
                $data['journalTypeCode'],
                $data['OrderType'],
                $data['taxDate'],
                $data['created'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['isConsolidated'],
				$data['message'],
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('/cogsjournal/cogsjournal/postCogsjournal/'.$data['journalsId']).'">Post Journal</a>
						</li>
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/cogsjournal/cogsjournal/cogsjournalInfo/'.$data['journalsId']).'">Journal Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View SO in Brightpearl</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
	}
}