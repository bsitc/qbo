<?php
#[\AllowDynamicProperties]
class Grnijournal_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}	
	public function fetchGrnijournal($journalsId = '', $accountId = ''){
		$fetchby			= $journalsId;
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$grniJournalDatass	= $this->{$this->globalConfig['fetchSalesOrder']}->fetchGrnijournal($journalsId, $accountId);
		foreach($grniJournalDatass as $fetchAccount1Id => $grniJournalDatassTemps){
			if($grniJournalDatassTemps['saveTime']){
				$saveTime	= $grniJournalDatassTemps['saveTime'] - (60*10);
			}
			$grniJournalDatas	= $grniJournalDatassTemps['return'];
			$batchInsert		= array();
			foreach($grniJournalDatas as $account1Id => $grniJournalData){
				$journalInfo			= array();
				$tempJournalDatas		= $this->db->select('id,journalsId')->get_where('grni_journal')->result_array();
				foreach($tempJournalDatas as $tempJournalData){
					$journalInfo[$tempJournalData['journalsId']]	= $tempJournalData;
				}
				
				foreach($grniJournalData as $journalsId => $row){
					if(!$journalsId){continue;}
					if($journalInfo[$journalsId]){continue;}
					else{
						$batchInsert[]	= $row; 
					}
				}
			}
			
			$inserted		= 0;
			$insertChunk	= 100;
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true);
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('grni_journal', $batchInsert); 
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'grnijournal'.$fetchAccount1Id, 'saveTime' => $saveTime)); 
				}
			}
		}
	}
	public function postGrnijournal($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postGrnijournal($orderId);
	}
	public function getGrnijournal(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array("order"=> $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 9){
					$dbTable	= 'grni_journal';
					$this->{$this->globalConfig['fetchSalesOrder']}->removeErrorFlag($ids,$dbTable);
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Error Removed Successfully.";
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('grni_journal', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']			= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']			= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('journalsId'))){
				$where['journalsId']			= trim($this->input->post('journalsId'));
			}
			if(trim($this->input->post('createdJournalsId'))){
				$where['createdJournalsId']		= trim($this->input->post('createdJournalsId'));
			}
			if(trim($this->input->post('reversedJournalsId'))){
				$where['reversedJournalsId']	= trim($this->input->post('reversedJournalsId'));
			}
			if(trim($this->input->post('orderId'))){
				$where['orderId']				= trim($this->input->post('orderId'));                
			}
			if(trim($this->input->post('journalTypeCode'))){
				$where['journalTypeCode']		= strtoupper(trim($this->input->post('journalTypeCode')));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']				= trim($this->input->post('status'));
			}
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('orderTaxdate_from'))){
			$query->where('date(orderTaxdate) >= ', "date('" . $this->input->post('orderTaxdate_from') . "')", false);
        }
        if(trim($this->input->post('orderTaxdate_to'))){
			$query->where('date(orderTaxdate) <= ', "date('" . $this->input->post('orderTaxdate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get('grni_journal')->row_array()['countsales'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('orderTaxdate_from'))){
			$query->where('date(orderTaxdate) >= ', "date('" . $this->input->post('orderTaxdate_from') . "')", false);
        }
        if(trim($this->input->post('orderTaxdate_to'))){
			$query->where('date(orderTaxdate) <= ', "date('" . $this->input->post('orderTaxdate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0'	=> 'Pending','1' => 'Sent','2' => 'Reversed','4' => 'Archive');
		$statusColor			= array('0'	=> 'default','1' => 'success','2' => 'success','4' => 'danger');
		$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId', 'reversedJournalsId', 'orderId', 'journalTypeCode', 'taxDate', 'orderTaxdate', 'status');
		if($this->session->userdata('order')){
			foreach($this->session->userdata('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas					= $query->select('id,account1Id,account2Id,journalsId,createdJournalsId,reversedJournalsId,orderId,invoiceReference,creditAmount,debitAmount,journalTypeCode,taxDate,creditNominalCode,debitNominalCode,taxCode,status,message,channelId,currencyCode,created,isConsolidated,OrderType,IsVoided,fetchdate,orderTaxdate,orderInvoiceRef,isOrderInvoiced,isReversed')->limit($limit, $start)->get('grni_journal')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$viewJournal		= '';
			$viewreverseJournal	= '';
			if($data['createdJournalsId']){
				$viewJournal		=	'<li>
											<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/journal?txnId='.$data['createdJournalsId'].'">View Journal in QBO</a>
										</li>';
			}
			if($data['reversedJournalsId']){
				$viewreverseJournal	=	'<li>
											<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/journal?txnId='.$data['reversedJournalsId'].'">View Reverse Journal in QBO</a>
										</li>';
			}
			$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId', 'orderId','taxDate','journalTypeCode','created','status');
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				$account1Mappings[$data['account1Id']]['name'],
				$account2Mappings[$data['account2Id']]['name'],
				$data['journalsId'],
				$data['createdJournalsId'],
				$data['reversedJournalsId'],
				$data['orderId'], 
				$data['journalTypeCode'],
				$data['taxDate'],
				$data['orderTaxdate'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['message'],
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('/cogsjournal/grnijournal/postGrnijournal/'.$data['journalsId']).'">Post Journal</a>
						</li>
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/cogsjournal/grnijournal/grnijournalInfo/'.$data['journalsId']).'">Journal Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=accounts-correction&journal_id='.$data['journalsId'].'">View Journal in Brightpearl</a>
						</li>
						'.$viewJournal.'
						'.$viewreverseJournal.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}