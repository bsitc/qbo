<?php
//QBO Customer MOdel
#[\AllowDynamicProperties]
class Customers_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
	public function fetchCustomers($customerId = '',$checkDelete = '1'){
		$this->{$this->globalConfig['fetchCustomer']}->fetchCustomers($customerId);
	}
	public function postCustomers($customerId = '',$postedAccount2Id = ''){
		$this->{$this->globalConfig['postCustomer']}->postCustomers($customerId,$postedAccount2Id);
	}
	public function getCustomers(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
 			$orderData		= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 10){
					$customerData = $this->db->select('customerId', 'account2Id')->where_in('id', $ids)->get('customers')->result_array();
					$customerIds = array();
					if($customerData){
						foreach($customerData as $customerDatas){
							$customerIds[$customerDatas['account2Id']][] = $customerDatas['customerId'];
						}
						if($customerIds){
							foreach($customerIds as $account2Ids => $customerId ){
								$this->{$this->globalConfig['postCustomer']}->postCustomers($customerId,$account2Ids);
							}
						}
					}
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('customers', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']	= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']	= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('customerId'))){
				$where['customerId']	= trim($this->input->post('customerId'));
			}
			if(trim($this->input->post('email'))){
				$where['email']			= trim($this->input->post('email'));
			}
			if(trim($this->input->post('createdCustomerId'))){
				$where['createdCustomerId'] = trim($this->input->post('createdCustomerId'));
			}
			if(trim($this->input->post('fname'))){
				$where['fname']			= trim($this->input->post('fname'));
			}
			if(trim($this->input->post('lname'))){
				$where['lname']			= trim($this->input->post('lname'));
			}
			if(trim($this->input->post('phone'))){
				$where['phone']			= trim($this->input->post('phone'));
			}
			if(trim($this->input->post('address1'))){
				$where['address1']		= trim($this->input->post('address1'));
			}
			if(trim($this->input->post('address2'))){
				$where['address2']		= trim($this->input->post('address2'));
			}
			if(trim($this->input->post('city'))){
				$where['city']			= trim($this->input->post('city'));
			}
			if(trim($this->input->post('state'))){
				$where['state']			= trim($this->input->post('state'));
			}
			if(trim($this->input->post('country'))){
				$where['country']		= trim($this->input->post('country'));
			}
			if(trim($this->input->post('zip'))){
				$where['zip']			= trim($this->input->post('zip'));
			}
			if(trim($this->input->post('company'))){
				$where['company']		= trim($this->input->post('company'));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']		= trim($this->input->post('status'));
			}
			if(trim($this->input->post('isSupplier')) >= '0'){
				$where['isSupplier']	= trim($this->input->post('isSupplier'));
			}
		}
		if(trim($this->input->post('updated_from'))){
			$query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(updated) < ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countCust')->get('customers')->row_array()['countCust'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('updated_from'))){
			$query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(updated) < ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
		$displayCustRowHeader	= array('id','account1Id','account2Id', 'customerId', 'createdCustomerId', 'email', 'fname', 'lname', 'phone', 'isSupplier', 'city', 'state', 'country', 'zip', 'updated', 'status', 'message');
		if($this->input->post('order')){
			foreach($this->input->post('order') as $ordering){
				if(@$displayCustRowHeader[$ordering['column']]){
					$query->order_by($displayCustRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas	= $query->select('id,customerId,createdCustomerId,email,isSupplier,fname,lname,phone,address1,address2,city,state,country,zip,status,,company,updated,account1Id,account2Id,message')->limit($limit, $start)->get('customers')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchCustomer'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postCustomer'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		foreach($datas as $data){
			$viewLink	= '';
			if($data['createdCustomerId']){
				$viewLink	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/customerdetail?nameId='.$data['createdCustomerId'].'">View Contact in QBO</a>
								</li>';
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'], 
				@$account2Mappings[$data['account2Id']]['name'], 
				$data['customerId'],
				$data['createdCustomerId'],
				$data['email'],
				$data['fname'],
				$data['lname'],
				$data['phone'],
				$data['isSupplier'],
				$data['city'],
				$data['state'],
				$data['country'],
				$data['zip'],
				$data['updated'],
				'<span class="label label-sm label-' . @$statusColor[$data['status']] . '">' . @$status[$data['status']] . '</span>',
				$data['message'],
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="' . base_url('customers/customers/fetchcustomers/' . $data['customerId']) . '">Fetch Contacts</a>
						</li>
						<li>
							<a class="btnactionsubmit" href="' . base_url('customers/customers/postCustomers/' . $data['customerId'].'/'.$data['account2Id']) . '">Post Contacts</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="' . base_url('customers/customers/customerInfo/' . $data['id']) . '">Contact Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=contact&cID='.$data['customerId'].'">View Contact in Brightpearl</a>
						</li>
						'.$viewLink.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}