<?php
//qbo
#[\AllowDynamicProperties]
class Journal_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
	public function fetchJournal($journalId = ''){
		if(php_sapi_name() === 'cli'){
			//
		}
		else{
			$this->{$this->globalConfig['account1Liberary']}->fetchBpJournals();
		}
		
		$fetchby		= $journalId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$cronTime		= '';
		$journalDatasss	= $this->{$this->globalConfig['account1Liberary']}->fetchJournal($journalId);
		foreach($journalDatasss as $fetchAccount1Id => $journalDatass){
			if(isset($journalDatass['saveTime'])){
				$saveTime	= $journalDatass['saveTime'] - (60*10);
			}
			
			$inserted		= 0;
			$insertChunk	= 100;
			$batchInsert	= array();
			$journalDatas	= $journalDatass['return'];
			foreach($journalDatas as $account1Id => $journalData){
				$savedJournalIds		= array();
				$savedJournalDatas		= $this->db->select('journalsId')->get_where('amazon_ledger')->result_array();
				if(!empty($savedJournalDatas)){
					$savedJournalIds	= array_column($savedJournalDatas,'journalsId');
					$savedJournalIds	= array_filter($savedJournalIds);
				}
				
				$archivedJournalIds		= array();
				$archivedJournalDatas	= $this->db->select('journalsId')->get_where('amazon_ledger_archived')->result_array();
				if(!empty($archivedJournalDatas)){
					$archivedJournalIds	= array_column($archivedJournalDatas,'journalsId');
					$archivedJournalIds	= array_filter($archivedJournalIds);
				}
				
				foreach($journalData as $row){
					$journalsId	= $row['journalsId'];
					if(!$journalsId){continue;}
					if(in_array($journalsId,$savedJournalIds)){continue;}
					if(in_array($journalsId,$archivedJournalIds)){continue;}
					
					$batchInsert[]	= $row;
				}
			}
			
			if(!empty($batchInsert)){
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true);
				foreach($batchInserts as $batchInsertTemp){
					$inserted	= $this->db->insert_batch('amazon_ledger', $batchInsertTemp);
				}
			}
			if($inserted == 1){
				if(!$fetchby){
					$this->ci->db->insert('cron_management', array('type' => 'journal'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime));
				}
			}
		}
	}
	public function postJournal($journalId = ''){
		$this->{$this->globalConfig['postProduct']}->postJournal($journalId);
	}
	public function getJournal(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status != ''){
					$this->db->where_in('id', $ids)->update('amazon_ledger', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']		= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']		= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('journalsId'))){
				$where['journalsId']		= trim($this->input->post('journalsId'));
			}
			if(trim($this->input->post('createdJournalsId'))){
				$where['createdJournalsId']	= trim($this->input->post('createdJournalsId'));
			}
			if(trim($this->input->post('orderId'))){
				$where['orderId']			= trim($this->input->post('orderId'));
			}
			if(trim($this->input->post('journalTypeCode'))){
				$where['journalTypeCode']	= trim($this->input->post('journalTypeCode'));
			}
			if(strlen($this->input->post('status')) > 0){
				$where['status']			= trim($this->input->post('status'));
			}
			if(strlen($this->input->post('amount')) > 0){
				$where['amount']			= trim($this->input->post('amount'));
			}
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countpro')->get('amazon_ledger')->row_array()['countpro'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
		$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId','journalTypeCode','orderId', 'amount', 'taxDate','status', 'message');
		if(@$this->session->userdata($this->router->directory.$this->router->class)){
			foreach(@$this->session->userdata($this->router->directory.$this->router->class) as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas	= $query->select('id,account1Id,account2Id,journalsId,createdJournalsId,journalTypeCode,orderId,amount,taxDate,status,message')->limit($limit, $start)->get('amazon_ledger')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				$data['journalsId'],
				$data['createdJournalsId'],
				$data['journalTypeCode'],
				$data['orderId'],
				sprintf("%.2f",$data['amount']),
				$data['taxDate'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['message'],
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="' . base_url('journal/journal/postJournal/' . $data['orderId']) . '">Post Amazon Fee</a>
						</li>
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/journal/journal/journalinfo/'.$data['orderId']).'">Amazon Fee Info</a>
						</li>
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}