<?php
//qbo
#[\AllowDynamicProperties]
class Otheramazonfee_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
		
		$this->AllBPchannelsData	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		if($this->AllBPchannelsData){
			$this->channelIdMaped	= array();
			$this->channelNameMaped	= array();
			foreach($this->AllBPchannelsData as $acc1 => $AllBPchannelsDatas){
				foreach($AllBPchannelsDatas as $cID	=> $AllBPchannelsDatass){
					$this->channelIdMaped[$acc1][$AllBPchannelsDatass['id']]		= $AllBPchannelsDatass['name'];
					$this->channelNameMaped[$acc1][$AllBPchannelsDatass['name']]	= $AllBPchannelsDatass['id'];
				}
			}
		}
	}
	public function fetchAmazonFeeOther($journalId = ''){
		$fetchby		= $journalId;
		$cronTime 		= '';
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$journalDatasss	= $this->{$this->globalConfig['account1Liberary']}->fetchAmazonFeeOther($journalId);
		foreach($journalDatasss as $fetchAccount1Id => $journalDatass){
			if(isset($journalDatass['saveTime'])){
				$saveTime	= $journalDatass['saveTime'] - (60*10);
			}
			
			$inserted		= 0;
			$insertChunk	= 100;
			$batchInsert	= array();
			$journalDatas	= $journalDatass['return'];
			foreach($journalDatas as $account1Id => $journalData){
				$savedJournalIds		= array();
				$savedJournalDatas		= $this->db->select('journalId')->get_where('amazonFeesOther')->result_array();
				if(!empty($savedJournalDatas)){
					$savedJournalIds	= array_column($savedJournalDatas,'journalId');
					$savedJournalIds	= array_filter($savedJournalIds);
				}
				
				$archivedJournalIds		= array();
				$archivedJournalDatas	= $this->db->select('journalId')->get_where('amazonFeesOther_archived')->result_array();
				if(!empty($archivedJournalDatas)){
					$archivedJournalIds	= array_column($archivedJournalDatas,'journalId');
					$archivedJournalIds	= array_filter($archivedJournalIds);
				}
				
				foreach($journalData as $row){
					$journalsId	= $row['journalId'];
					if(!$journalsId){continue;}
					if(in_array($journalsId,$savedJournalIds)){continue;}
					if(in_array($journalsId,$archivedJournalIds)){continue;}
					
					$batchInsert[]	= $row;
				}
			}
			
			if(!empty($batchInsert)){
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true);
				foreach($batchInserts as $batchInsertTemp){
					$inserted	= $this->db->insert_batch('amazonFeesOther', $batchInsertTemp);
				}
			}
			if($inserted == 1){
				if(!$fetchby){
					$this->ci->db->insert('cron_management', array('type' => 'amazonfeeother'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime));
				}
			}
		}
	}
	public function postAmazonFeeOther($journalId = ''){
		$this->{$this->globalConfig['account2Liberary']}->postAmazonFeeOther($journalId);
	}
	public function getAmazonFeeOther(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status != ''){
					$this->db->where_in('id', $ids)->update('amazonFeesOther', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']		= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('journalId'))){
				$where['journalId']			= trim($this->input->post('journalId'));
			}
			if(trim($this->input->post('qboTxnId'))){
				$where['qboTxnId']			= trim($this->input->post('qboTxnId'));
			}
			if(trim($this->input->post('qboRefNo'))){
				$where['qboRefNo']			= trim($this->input->post('qboRefNo'));
			}
			if(trim($this->input->post('channelid'))){
				$searchChannelId			= $this->channelNameMaped[1][trim($this->input->post('channelid'))];
				$where['channelid']			= $searchChannelId;
			}
			if(strlen($this->input->post('status')) > 0){
				$where['status']			= trim($this->input->post('status'));
			}
			if(strlen($this->input->post('totalAmt')) > 0){
				$where['totalAmt']			= trim($this->input->post('totalAmt'));
			}
			if(trim($this->input->post('journalTypeCode'))){
				$where['journalTypeCode']	= trim($this->input->post('journalTypeCode'));
			}
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countpro')->get('amazonFeesOther')->row_array()['countpro'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
		$displayProRowHeader	= array('id', 'account2Id', 'journalId', 'qboTxnId','qboRefNo','channelid','totalAmt', 'journalTypeCode', 'taxDate', 'status', 'message');
		if(@$this->session->userdata($this->router->directory.$this->router->class)){
			foreach(@$this->session->userdata($this->router->directory.$this->router->class) as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas	= $query->select('id,account1Id,account2Id,journalId,qboTxnId,journalTypeCode,qboRefNo,taxDate,createdOn,updatedOn,currencyId,channelid,exchangeRate,totalAmt,status,fetchDate,message')->limit($limit, $start)->get('amazonFeesOther')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['account2Liberary'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$viewOrder		= '';
			if($data['qboTxnId']){
				/* $viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href='.$account2Mappings[$data['account2Id']]['viewUrl'].'/check?txnId='.$data['qboTxnId'].'">View Txn in qbo</a>
								</li>'; */
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account2Mappings[$data['account2Id']]['name'],
				$data['journalId'],
				$data['qboTxnId'],
				$data['qboRefNo'],
				$this->channelIdMaped[$data['account1Id']][$data['channelid']],
				sprintf("%.2f",$data['totalAmt']),
				$data['journalTypeCode'], 
				$data['taxDate'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['message'],
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="newInfoBtn" target="_blank" href="'.base_url('/journal/otheramazonfee/amazonFeeOtherInfo/'.$data['journalId']).'">Amazon Fee (Other) Info</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}