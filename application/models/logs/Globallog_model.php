<?php
#[\AllowDynamicProperties]
class Globallog_Model extends CI_Model{
	public $typearray	= array(
		'0'		=> '',
		'1' 	=> 'Fetch product',
		'2'		=> 'Post product',
		'3'		=> 'Fetch sales order',			
		'4'		=> 'Post sales order',
		'5'		=> 'Cancel sales order',
		'6'		=> 'Fetch dispatch confirmation',
		'7'		=> 'Post dispatch confirmation',
		'8'		=> 'Fetch stock adjustment',
		'9'		=> 'Post stock adjustment',
		'10'	=> 'Fetch customer',			
		'11'	=> 'Post customer',			
	);
	public function insertLog($logdata){
		$this->db->insert('global_log', $logdata);	
		return $this->db->insert_id();		
	}
	public function record_count() {
		$s		= @trim($this->input->get('s'));
		$s		= trim($s);
		$where	= '';
		if($s){
			$where		= " where log.id  LIKE '%".$s."%' OR log.type_id  LIKE '%".$s."%' OR log.type LIKE '%".$s."%'";
			$searchword	= $s;
			$matches	= array();
			foreach($this->typearray as $k=>$v) {
				if(preg_match("/\b$searchword\b/i", $v)) {
					$matches[$k]	= $v;
				}
			}
			if(@$matches){
				$type_array	= array_keys($matches);
				$type		= implode(",",$type_array);
				if($type){
					$where	.= " OR type IN ($type) ";
				}
			}
		}		
		$sql	= "SELECT log.* FROM global_log as log $where";	
		$query	= $this->db->query($sql);
		return $query->num_rows();
	}
	public function updateLog($id,$data){
		$sql	= 'UPDATE global_log SET data = concat(data,"\r\n'.$this->db->escape($data).'") WHERE id ='. $id;
		$this->db->query($sql);
	}
	public function autoUpdateLog($id,$data){
	    $this->db->where('id', $id);
	    $this->db->update('global_log', $data);		
	    return $this->db->affected_rows();
	}
	public function getLogs(){
		$query	= $this->db->query("SELECT * FROM global_log order by id desc");
		$data	= $query->result_array();
		foreach($data as $key=>$value){
			switch ($value['type']) {
				case 1:
					$value['type']	= 'Fetch product';
					break;
				case 2:
					$value['type']	= 'Post product';
					break;
				case 3:
					$value['type']	= 'Fetch sales order';
					break;
				case 4:
					$value['type']	= 'Post sales order';
					break;
				case 5:
					$value['type']	= 'Cancel sales order';
					break;
				case 12:
					$value['type']	= 'Fetch dispatch confirmation';
					break;
				case 13:
					$value['type']	= 'Post dispatch confirmation';
					break;
			}
			$data[$key]['type']	= $value['type'];
		}
		return $data;
	}
	public function getBpLogDetails($limit="", $start = ""){			
		$s		= (@$this->input->get('s')!="") ? @$this->input->get('s') : "" ;
		$s		= trim($s);
		$start	= $start * $limit;
		$where	= "";
		if($s !=""){						
			$where		= " where log.id  LIKE '%".$s."%' OR log.type_id  LIKE '%".$s."%' OR log.type LIKE '%".$s."%'";			
			$searchword	= $s;
			$matches	= array();
			foreach($this->typearray as $k=>$v) {
				if(preg_match("/\b$searchword\b/i", $v)) {
					$matches[$k]	= $v;
				}
			}
			if(@$matches){
				$type_array	= array_keys($matches);
				$type		= implode(",",$type_array);
				if($type){
					$where	.= " OR type IN ($type) ";
				}
			}
		}
		$orderby	= " order by log.id desc ";
		if(@$this->input->get('order')){
			$orderby	= " order by log.".@$this->input->get('order')." ".$this->input->get('direction','DESC');
		}
		$limit_condition	= "";
		if($limit!=''){
			$limit_condition	= " LIMIT ".$start.",".$limit;
		}		
		$sql	= "SELECT log.* FROM global_log as log $where $orderby $limit_condition";
		$query	= $this->db->query($sql);		
		$data	= $query->result_array();		
		foreach($data as $key=>$value){
			$value['type']		= $this->typearray[$value['type']];
			$data[$key]['type']	= $value['type'];
		}
		return $data;
	}
	public function getLogDetails($logId){
		$query	= $this->db->query("SELECT * FROM global_log where id ='".$logId."'");
		$data	= $query->result_array();
		return $data[0]['data'];
	}	
}
?>