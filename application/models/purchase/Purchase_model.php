<?php
//qbo
#[\AllowDynamicProperties]
class Purchase_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchPurchase($orderId = '', $accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$purchaseDatass	= $this->{$this->globalConfig['fetchPurchaseOrder']}->fetchPurchase($orderId, $accountId);
		foreach($purchaseDatass as $fetchAccount1Id => $purchaseDatassTemps){
			if(isset($purchaseDatassTemps['saveTime'])){
				$saveTime	= $purchaseDatassTemps['saveTime'] - (60*10);
			}
			$cronTime = '';
			//changesAddedAfterGRNIJournal
			$grniJournalsOrderIDs	= array();
			
			$purchaseDatas	= $purchaseDatassTemps['return'];
			$batchInsert	= array();
			$batchUpdate	= array();
			$inserted		= 0;
			$updateOrder	= 100;
			foreach($purchaseDatas as $account1Id => $purchaseData){
				$savedOrderData		= array();
				$savedOrderDatas	= $this->db->select('id,orderId,createOrderId,status,isPaymentCreated')->get_where('purchase_order')->result_array();
				if(!empty($savedOrderDatas)){
					foreach($savedOrderDatas as $savedOrderDatasTemp){
						$savedOrderData[$savedOrderDatasTemp['orderId']]		= $savedOrderDatasTemp;
					}
				}
				
				$archivedOrderData	= array();
				$archivedOrderDatas	= $this->db->select('orderId')->get_where('purchase_order_archived')->result_array();
				if(!empty($archivedOrderDatas)){
					foreach($archivedOrderDatas as $archivedOrderDatasTemp){
						$archivedOrderData[$archivedOrderDatasTemp['orderId']]	= $archivedOrderDatasTemp;
					}
				}
				
				foreach($purchaseData as $orderId => $row){
					if(!$orderId){continue;}
					if($archivedOrderData[$orderId]){continue;}
					if(isset($savedOrderData[$orderId])){
						$row['orders']['id']				= $savedOrderData[$orderId]['id'];
						$row['orders']['status']			= $savedOrderData[$orderId]['status'];
						$row['orders']['isPaymentCreated']	= $savedOrderData[$orderId]['isPaymentCreated'];
						if($row['orders']['status'] == 4){continue;}
						$batchUpdate[]	= $row['orders'];
						
						//changesAddedAfterGRNIJournal
						if(strlen($row['orders']['bpInvoiceNumber']) > 0){
							$grniJournalsOrderIDs[$orderId]	= array(
								'orderId'			=> $orderId,
								'orderInvoiceRef'	=> $row['orders']['bpInvoiceNumber'],
								'orderTaxdate'		=> $row['orders']['taxDate'],
								'isOrderInvoiced'	=> 1,
							);
						}
					}
					else{
						$batchInsert[]	= $row['orders'];
						
						//changesAddedAfterGRNIJournal
						if(strlen($row['orders']['bpInvoiceNumber']) > 0){
							$grniJournalsOrderIDs[$orderId]	= array(
								'orderId'			=> $orderId,
								'orderInvoiceRef'	=> $row['orders']['bpInvoiceNumber'],
								'orderTaxdate'		=> $row['orders']['taxDate'],
								'isOrderInvoiced'	=> 1,
							);
						}
					}
				}
			}
			
			if($batchUpdate){
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$inserted	= $this->db->update_batch('purchase_order', $batchUpdate,'id');
					}
				}
			}
			if($batchInsert){
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$inserted	= $this->db->insert_batch('purchase_order', $batchInsert); 
				}
			}
			//changesAddedAfterGRNIJournal
			if(!empty($grniJournalsOrderIDs)){
				$grniJournalsOrderIDss	= array_chunk($grniJournalsOrderIDs,$updateOrder,true);
				foreach($grniJournalsOrderIDss as $grniJournalsOrderIDsss){
					if($grniJournalsOrderIDsss){
						$this->db->update_batch('grni_journal', $grniJournalsOrderIDsss,'orderId');
					}
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'purchase'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime));
				}
			}
		}
	}
	public function postPurchase($orderId = ''){
		$this->{$this->globalConfig['postPurchaseOrder']}->postPurchase($orderId);
	}
}