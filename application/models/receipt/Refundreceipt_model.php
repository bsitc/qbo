<?php
//qbo
#[\AllowDynamicProperties]
class Refundreceipt_model extends CI_Model{	
	public function __construct(){
		parent::__construct();
	}
	public function fetchRefundreceipt($orderId = '', $accountId = '', $isWebhook = '0'){
		$fetchby			= $orderId;
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
        $salesDatass		= $this->brightpearl->fetchRefundreceipt($orderId, $accountId);
		$fatchedOrderIds	= array();
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(isset($salesDatassTemps['saveTime'])){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$cronTime = '';
			$salesDatas		= $salesDatassTemps['return'];
			$batchInsert	= array();
			$batchUpdate	= array();
			$inserted		= 0;
			$updateOrder	= 100;
			foreach($salesDatas as $account1Id => $salesData){
				$savedOrderData		= array();
				$savedOrderDatas	= $this->db->select('id,orderId,createOrderId,status')->get_where('refund_receipt')->result_array();
				if(!empty($savedOrderDatas)){
					foreach($savedOrderDatas as $savedOrderDatasTemp){
						$savedOrderData[$savedOrderDatasTemp['orderId']]		= $savedOrderDatasTemp;
					}
				}
				
				$archivedOrderData	= array();
				$archivedOrderDatas	= $this->db->select('orderId')->get_where('refund_receipt_archived')->result_array();
				if(!empty($archivedOrderDatas)){
					foreach($archivedOrderDatas as $archivedOrderDatasTemp){
						$archivedOrderData[$archivedOrderDatasTemp['orderId']]	= $archivedOrderDatasTemp;
					}
				}
				
				$savedSalesCredits		= array();
				$savedSalesCreditsDatas	= $this->db->select('orderId')->get_where('sales_credit_order')->result_array();
				if(!empty($savedSalesCreditsDatas)){
					foreach($savedSalesCreditsDatas as $savedSalesCreditsDatasTemp){
						$savedSalesCredits[$savedSalesCreditsDatasTemp['orderId']]		= $savedSalesCreditsDatasTemp;
					}
				}
				
				foreach($salesData as $orderId => $row){
					if(!$orderId){continue;}
					if($archivedOrderData[$orderId]){continue;}
					if($savedSalesCredits[$orderId]){continue;}
					$fatchedOrderIds[]	= $orderId;
					
					if(isset($savedOrderData[$orderId])){
						$row['orders']['id']				= $savedOrderData[$orderId]['id'];
						$row['orders']['status']			= $savedOrderData[$orderId]['status'];
						if($row['orders']['status'] == 4){continue;}
						$batchUpdate[]	= $row['orders'];
					}
					else{
						$batchInsert[]	= $row['orders'];
					}
				}
			}
			
			if($batchUpdate){
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$inserted	= $this->db->update_batch('refund_receipt', $batchUpdate,'id');   
					}
				}
			}
			if($batchInsert){
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$inserted	= $this->db->insert_batch('refund_receipt', $batchInsert); 
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'rufundreceipt'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
		if(php_sapi_name() === 'cli'){
			//
		}
		else{
			$this->{$this->globalConfig['account1Liberary']}->fetchPayment();
		}
		$this->brightpearl->fetchRefundreceiptpayment($fatchedOrderIds);
    }
	public function postRefundreceipt($orderId = ''){ 
		$this->qbo->postRefundreceipt($orderId);
	}
	public function postConsolRefundReceipt($orderId = ''){
		$this->qbo->postConsolRefundReceipt($orderId);
	}
	public function getRefundreceipt(){
	}
	public function refundreceiptItem($orderId){
		$datas = $this->db->get_where('refund_receipt_item',array('orderId' => $orderId))->result_array();
		return $datas;
	}
}