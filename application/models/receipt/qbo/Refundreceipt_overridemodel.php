<?php
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Refundreceipt_model.php');
#[\AllowDynamicProperties]
class Refundreceipt_overridemodel extends Refundreceipt_model{
	public function __construct(){
		parent::__construct();
		$this->ci = get_instance();
	}
	public function getRefundreceipt(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if ($groupAction == 'group_action') {
			$ids	= $this->input->post('id');
			if ($ids) {
				$status	= $this->input->post('customActionName');
				if($status == 5){
					$orderIds	= array_column($this->db->select('orderId')->where_in('id', $ids)->get('refund_receipt')->result_array(),'orderId');
					if($orderIds){
						$this->qbo->postRefundreceipt($orderIds);
					}
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('refund_receipt', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if ($this->input->post('action') == 'filter') {
			if (trim($this->input->post('account1Id'))) {
				$where['account1Id']		= trim($this->input->post('account1Id'));
			}
			if (trim($this->input->post('account2Id'))) {
				$where['account2Id']		= trim($this->input->post('account2Id'));
			}
			if (trim($this->input->post('orderId'))) {
				$where['orderId']			= trim($this->input->post('orderId'));
			}
			if (trim($this->input->post('bpInvoiceNumber'))) {
				$where['bpInvoiceNumber']	= trim($this->input->post('bpInvoiceNumber'));
			}
			if (trim($this->input->post('createOrderId'))) {
				$where['createOrderId']		= trim($this->input->post('createOrderId'));
			}
			if (trim($this->input->post('customerEmail'))) {
				$where['customerEmail']		= trim($this->input->post('customerEmail'));
			}
			if (trim($this->input->post('status')) >= '0') {
				$where['status']			= trim($this->input->post('status'));
			}
			if (trim($this->input->post('sendInAggregation')) >= '0') {
				$where['sendInAggregation']			= trim($this->input->post('sendInAggregation'));
			}
			if(trim($this->input->post('message'))){
                $where['message']			= trim($this->input->post('message'));
            }
			if(trim($this->input->post('channelName'))){
                $where['channelName']		= trim($this->input->post('channelName'));
            }
			
			if(trim($this->input->post('OrderInfo')) > '0'){
				if((trim($this->input->post('OrderInfo'))) == 2){
					$where['sendPaymentTo']	= 'qbo';
				}
				if((trim($this->input->post('OrderInfo'))) == 3){
					$where['uninvoiced']	= 1;
				}
				if((trim($this->input->post('OrderInfo'))) == 4){
					$where['uninvoiced']	= 2;
				}
            }
			
		}
		if (trim($this->input->post('taxDate_from'))) {
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if (trim($this->input->post('taxDate_to'))) {
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if (trim($this->input->post('created_from'))) {
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if (trim($this->input->post('created_to'))) {
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		if ($where) {
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get('refund_receipt')->row_array()['countsales'];
		$limit          = intval($this->input->post('length'));
		$limit          = $limit < 0 ? $totalRecord : $limit;
		$start          = intval($this->input->post('start'));
		$query          = $this->db;
		if (trim($this->input->post('taxDate_from'))) {
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if (trim($this->input->post('taxDate_to'))) {
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if (trim($this->input->post('created_from'))) {
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if (trim($this->input->post('created_to'))) {
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		if ($where) {
			$query->like($where);
		}
		$status					= array('0' => 'Pending', '1' => 'Sent', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '4' => 'danger');
		$displayProRowHeader	= array('id','account1Id','account2Id', 'orderId', 'bpInvoiceNumber', 'createOrderId', 'channelName', 'sendInAggregation', 'taxDate', 'updated', 'status', '',  'message');
		if ($this->session->userdata($this->router->directory.$this->router->class)) {
			foreach ($this->input->post('order') as $ordering) {
				if (@$displayProRowHeader[$ordering['column']]) {
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas	= $query->select('id, account1Id, account2Id, orderId, bpInvoiceNumber, createOrderId, customerEmail, taxDate, sendInAggregation, updated, status, uninvoiced, rowData, message, sendPaymentTo,channelName')->limit($limit, $start)->get('refund_receipt')->result_array();
		
		$account1Mappings =  array();$account2Mappings =  array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchRefundReceipt'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postRefundReceipt'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$user_login_data	= $this->session->userdata('login_user_data');
		$IsAdmin			= 0;
		if($user_login_data['role'] == 'admin'){
			$IsAdmin	= 1;
		}
		foreach($datas as $data){
			$OrderParam		= json_decode($data['rowData'],true);
			$invoiceRefBP	= $data['bpInvoiceNumber'];
			if(!$invoiceRefBP){
				$invoiceRefBP	= $OrderParam['invoices'][0]['invoiceReference'];
			}
			$viewOrder		= '';
			$postOrder		= '';
			$OrderInfo		= '';
			$message		= $data['message'];
			if($data['sendPaymentTo'] == 'qbo'){
				$OrderInfo	=	'<span class="label label-sm label-info">Payment Initiated In Brightpearl</span>'; 
			}
			if($data['uninvoiced'] == 1){
				$OrderInfo	=	'<span class="label label-sm label-info">Order Uninvoiced on Brightpearl</span>'; 
			}
			if($data['uninvoiced'] == 2){
				$OrderInfo	=	'<span class="label label-sm label-info">Order Void On QBO</span>'; 
			}
			$postOrder	= 	'<li>
								<a class="btnactionsubmit" href="'.base_url('receipt/refundreceipt/postRefundreceipt/'.$data['orderId']).'">Post Refund Receipt</a>
							</li>';
			if($data['createOrderId']){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/refundreceipt?txnId='.$data['createOrderId'].'">View Refund Receipt in QBO</a>
								</li>';
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				'<a href="'.base_url('receipt/refundreceipt/refundreceiptItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
				@$invoiceRefBP,
				'<a href="'.base_url('receipt/refundreceipt/refundreceiptItem/'.$data['orderId']).'">'.$data['createOrderId'].'</a>',
				@$data['channelName'],
				@$data['sendInAggregation'],
				@$data['taxDate'],
				$data['updated'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$OrderInfo,
				$message,
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('receipt/refundreceipt/fetchRefundreceipt/'.$data['orderId']).'">Fetch Refund Receipt</a>
						</li>
						'.$postOrder.'
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('receipt/refundreceipt/refundreceiptInfo/'.$data['orderId']).'">Refund Receipt Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View SalesCredit in Brightpearl</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}
?>