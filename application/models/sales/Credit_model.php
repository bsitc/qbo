<?php
//qbo
#[\AllowDynamicProperties]
class Credit_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchSalesCredit($orderId = '', $accountId = ''){
		$fetchby			= $orderId;
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$salesDatass		= $this->{$this->globalConfig['fetchSalesCredit']}->fetchSalesCredit($orderId, $accountId);
		$fatchedOrderIds	= array();
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(isset($salesDatassTemps['saveTime'])){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$cronTime = '';
			$salesDatas		= $salesDatassTemps['return'];
			$batchInsert	= array();
			$batchUpdate	= array();
			$inserted		= 0;
			$updateOrder	= 100;
			foreach($salesDatas as $account1Id => $salesData){
				$savedOrderData		= array();
				$savedOrderDatas	= $this->db->select('id,orderId,createOrderId,status,isPaymentCreated')->get_where('sales_credit_order')->result_array();
				if(!empty($savedOrderDatas)){
					foreach($savedOrderDatas as $savedOrderDatasTemp){
						$savedOrderData[$savedOrderDatasTemp['orderId']]		= $savedOrderDatasTemp;
					}
				}
				
				$archivedOrderData	= array();
				$archivedOrderDatas	= $this->db->select('orderId')->get_where('sales_credit_order_archived')->result_array();
				if(!empty($archivedOrderDatas)){
					foreach($archivedOrderDatas as $archivedOrderDatasTemp){
						$archivedOrderData[$archivedOrderDatasTemp['orderId']]	= $archivedOrderDatasTemp;
					}
				}
				
				$savedRefundReceipts		= array();
				$savedRefundReceiptsDatas	= $this->db->select('orderId')->get_where('refund_receipt')->result_array();
				if(!empty($savedRefundReceiptsDatas)){
					foreach($savedRefundReceiptsDatas as $savedRefundReceiptsDatasTemp){
						$savedRefundReceipts[$savedRefundReceiptsDatasTemp['orderId']]	= $savedRefundReceiptsDatasTemp;
					}
				}
				
				foreach($salesData as $orderId => $row){
					if(!$orderId){continue;}
					if($archivedOrderData[$orderId]){continue;}
					if($savedRefundReceipts[$orderId]){continue;}
					$fatchedOrderIds[]	= $orderId;
					
					if(isset($savedOrderData[$orderId])){
						$row['orders']['id']				= $savedOrderData[$orderId]['id'];
						$row['orders']['status']			= $savedOrderData[$orderId]['status'];
						$row['orders']['isPaymentCreated']	= $savedOrderData[$orderId]['isPaymentCreated'];
						if($row['orders']['status'] == 4){continue;}
						$batchUpdate[]	= $row['orders'];
					}
					else{
						$batchInsert[]	= $row['orders'];
					}
				}
			}
			
			if($batchUpdate){
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$inserted	= $this->db->update_batch('sales_credit_order', $batchUpdate,'id');
					}
				}
			}
			if($batchInsert){
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$inserted	= $this->db->insert_batch('sales_credit_order', $batchInsert); 
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'salesCredit'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime));
				}
			}
		}
		if(php_sapi_name() === 'cli'){
			//
		}
		else{
			$this->{$this->globalConfig['account1Liberary']}->fetchPayment();
		}
		$this->{$this->globalConfig['fetchSalesCredit']}->fetchSalesCreditPayment($fatchedOrderIds);
	}
	public function postSalesCredit($orderId = ''){ 
		$this->{$this->globalConfig['postSalesCredit']}->postSalesCredit($orderId);
		$this->{$this->globalConfig['fetchSalesCredit']}->postSalesCreditPayment($orderId);
	}
}