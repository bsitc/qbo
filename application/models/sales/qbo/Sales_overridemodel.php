<?php
//qbo_sales_model
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Sales_model.php');
#[\AllowDynamicProperties]
class Sales_overridemodel extends Sales_model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
	public function getSales(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 5){
					$orderIds	= array_column($this->db->select('orderId')->where_in('id', $ids)->get('sales_order')->result_array(),'orderId');
					if($orderIds){
						$this->{$this->globalConfig['postSalesOrder']}->postSales($orderIds);
						$this->{$this->globalConfig['fetchSalesOrder']}->postSalesPayment($orderIds);
					}
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('sales_order', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']		= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']		= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('orderId'))){
				$where['orderId']			= trim($this->input->post('orderId'));
			}
			if(trim($this->input->post('bpInvoiceNumber'))){
				$where['bpInvoiceNumber']	= trim($this->input->post('bpInvoiceNumber'));
			}
			if(trim($this->input->post('createOrderId'))){
				$where['createOrderId']		= trim($this->input->post('createOrderId'));
			}
			if(trim($this->input->post('channelName'))){
				$where['channelName']		= trim($this->input->post('channelName'));
			}
			if(trim($this->input->post('delAddressName'))){
				$where['delAddressName']	= trim($this->input->post('delAddressName'));
			}
			if(trim($this->input->post('customerEmail'))){
				$where['customerEmail']		= trim($this->input->post('customerEmail'));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']			= trim($this->input->post('status'));
			}
			if(trim($this->input->post('message'))){
                $where['message']			= trim($this->input->post('message'));
            }
			if(trim($this->input->post('sendInAggregation')) >= '0'){
                $where['sendInAggregation']	= trim($this->input->post('sendInAggregation'));
            }
			if(trim($this->input->post('OrderInfo')) > '0'){
				if((trim($this->input->post('OrderInfo'))) == 1){
					$where['sendPaymentTo']	= 'qbo';
				}
				if((trim($this->input->post('OrderInfo'))) == 2){
					$where['sendPaymentTo']	= 'brightpearl';
				}
				if((trim($this->input->post('OrderInfo'))) == 3){
					$where['uninvoiced']	= 1;
				}
				if((trim($this->input->post('OrderInfo'))) == 4){
					$where['uninvoiced']	= 2;
				}
            }
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('updated_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(created) <= ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get('sales_order')->row_array()['countsales'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('updated_from'))){
			$query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array(
			'0'	=> 'Pending',
			'1'	=> 'Sent',
			'2' => 'Invoiced',
			'3' => 'Payment Created',
			'4' => 'Archive'
		);
		$statusColor			= array(
			'0'	=> 'default',
			'1' => 'success',
			'2' => 'info',
			'3' => 'success',
			'4' => 'danger',
		);
		
		$displayProRowHeader	= array('id','account1Id','account2Id','orderId','bpInvoiceNumber','createOrderId', 'channelName', 'delAddressName', 'customerEmail', 'taxDate','updated', 'sendInAggregation', 'status','', 'message');
		if($this->session->userdata($this->router->directory.$this->router->class)){
			foreach ($this->session->userdata($this->router->directory.$this->router->class) as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas	= $query->select('id, account1Id, account2Id, orderId, bpInvoiceNumber, createOrderId, channelName, delAddressName, customerEmail, updated, status, sendPaymentTo, uninvoiced, rowData, message, taxDate, sendInAggregation')->limit($limit, $start)->get('sales_order')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$OrderParam		= json_decode($data['rowData'],true);
			$customerEmail	= $OrderParam['parties']['customer']['email'];
			$invoiceRefBP	= $data['bpInvoiceNumber'];
			if(!$invoiceRefBP){
				$invoiceRefBP	= $OrderParam['invoices'][0]['invoiceReference'];
			}
			$viewOrder		= '';
			$orderInfo		= '';
			$message		= $data['message'];
			if($data['sendPaymentTo'] == 'brightpearl'){
				$orderInfo	=	'<span class="label label-sm label-info">Payment initiated in Qbo</span>'; 
			}
			if($data['sendPaymentTo'] == 'qbo'){
				$orderInfo	=	'<span class="label label-sm label-info">Payment initiated in Brightpearl</span>'; 
			}
			if($data['uninvoiced'] == 1){
				$orderInfo	=	'<span class="label label-sm label-info">Order Uninvoiced on Brightpearl</span>'; 
			}
			if($data['uninvoiced'] == 2){
				$orderInfo	=	'<span class="label label-sm label-info">Invoice Void On QBO</span>'; 
			}
			if($data['createOrderId']){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/invoice?txnId='.$data['createOrderId'].'">View Invoice in QBO</a>
								</li>';
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				'<a href="'.base_url('/sales/sales/salesItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
				@$invoiceRefBP,
				'<a href="'.base_url('/sales/sales/salesItem/'.$data['orderId']).'">'.$data['createOrderId'].'</a>',
				@$data['channelName'],
				@$data['delAddressName'],
				@$customerEmail,
				date('Y-m-d',strtotime($data['taxDate'])),
				$data['updated'],
				$data['sendInAggregation'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$orderInfo,
				$message,
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('/sales/sales/fetchSales/'.$data['orderId']).'">Fetch Sales Order</a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('/sales/sales/postSales/'.$data['orderId']).'">Post Sales Order</a>
						</li>
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/sales/sales/salesInfo/'.$data['orderId']).'">Sales Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View SalesOrder in Brightpearl</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
	public function postSales($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postSales($orderId);
		$this->{$this->globalConfig['fetchSalesOrder']}->postSalesPayment($orderId);
	}
	public function postaggregationSales($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postaggregationSales($orderId);
	}
	public function postNetOffConsolOrder($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postNetOffConsolOrder($orderId);
	}

	public function getSalesItem($orderId){
		$datas	= $this->db->get_where('sales_item',array('orderId' => $orderId))->result_array();
		return $datas;
	}
}
?>