<?php
//qbo
#[\AllowDynamicProperties]
class Singlestocktx_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchSinglestocktx($orderId = '', $accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$salesDatass	= $this->{$this->globalConfig['fetchSingleCompanyStocktx']}->fetchSinglestocktx($orderId, $accountId);
		$cronTime		= '';
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(@$salesDatassTemps['saveTime']){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$salesDatas			= $salesDatassTemps['return'];
			$batchInsert		= array();
			$batchGoodsInserts	= array();
			$batchGoodsInsert	= array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds		= array_keys($salesData);
				$orderInfo		= array();
				$tempItemDatas	= $this->db->select('id,goodsMovementId')->get_where('single_company_stock_transfers',array('account1Id' => $account1Id))->result_array();
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['goodsMovementId']]	= $tempItemData;
				}
				foreach($salesData as $goodsMovementId => $row){
					if(!$goodsMovementId){
						continue;
					}
					if($orderInfo[$goodsMovementId]){
						continue;
					}
					$batchGoodsInsert[]	= $row;
				}
			}
			$inserted	= 0;
			$batchChunk	= 500;
			
			if($batchGoodsInsert){ 
				$inserted			= '1';
				$batchGoodsInserts	= array_chunk($batchGoodsInsert,$batchChunk,true);
				foreach($batchGoodsInserts as $batchGoodsInsert){
					$this->db->insert_batch('single_company_stock_transfers', $batchGoodsInsert);
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'singlestocktx'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
	}
	
	public function postSinglestocktx($id = ''){
		$this->{$this->globalConfig['postSingleCompanyStocktx']}->postSinglestocktx($id);
	}
	
	public function getSinglestocktx(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
 			$orderData		= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status != ''){
					$this->db->where_in('id', $ids)->update('single_company_stock_transfers', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		
		$this->db->reset_query();
		$allUniqueProducts		= $this->db->distinct('productId')->select('productId')->get_where('single_company_stock_transfers',array('productId <>' => ''))->result_array();
		$allUniqueProductsIds	= array();
		$allUniqueProductsIds	= array_column($allUniqueProducts,'productId');
		
		$this->db->reset_query();
		$productMappings		= array();
		$productMappingsSKU		= array();
		if($allUniqueProductsIds){
			$this->db->where_in('productId',$allUniqueProductsIds);
		}
		$productMappingsTemps	= $this->db->select('productId,sku')->get_where('products',array('productId <>' => ''))->result_array();
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			$productMappingsSKU[$productMappingsTemp['sku']]	= $productMappingsTemp;
		}
		
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']			= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']			= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('goodsMovementId'))){
				$where['goodsMovementId']				= trim($this->input->post('goodsMovementId'));
			}
			if(trim($this->input->post('goodsNoteId'))){
				$where['goodsNoteId']			= trim($this->input->post('goodsNoteId'));
			}
			if(trim($this->input->post('stockTransferId'))){
				$where['stockTransferId']			= trim($this->input->post('stockTransferId'));
			}
			if(trim($this->input->post('createdOrderId'))){
				$where['createdOrderId']		= trim($this->input->post('createdOrderId'));
			}
			if(trim($this->input->post('sourceWarehouseId'))){
				$where['sourceWarehouseId']					= trim($this->input->post('sourceWarehouseId'));
			}
			if(trim($this->input->post('targetWarehouseId'))){
				$where['targetWarehouseId']					= trim($this->input->post('targetWarehouseId'));
			}
			if(trim($this->input->post('productId'))){
				$where['productId']				= trim($this->input->post('productId'));
			}
			if(trim($this->input->post('qty'))){
				$where['qty']					= trim($this->input->post('qty'));
			}
			if(trim($this->input->post('price'))){
				$where['price']					= trim($this->input->post('price'));
			}
			if(trim($this->input->post('totalValue'))){
				$where['totalValue']			= trim($this->input->post('totalValue'));
			}
			if(trim($this->input->post('message'))){
				$where['message']				= trim($this->input->post('message'));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']				= trim($this->input->post('status'));
			}
			if(trim($this->input->post('sku'))){
				if($productMappingsSKU[trim($this->input->post('sku'))]){
					$checkProductID		= $productMappingsSKU[trim($this->input->post('sku'))]['productId'];
					$where['productId']	= $checkProductID;
					
				}
			}
		}
		
		if(trim($this->input->post('shippedOn_from'))){
			$query->where('date(shippedOn) >= ', "date('" . $this->input->post('shippedOn_from') . "')", false);
		}
		if(trim($this->input->post('shippedOn_to'))){
			$query->where('date(shippedOn) <= ', "date('" . $this->input->post('shippedOn_to') . "')", false);
		}
		if(trim($this->input->post('created_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if(trim($this->input->post('created_to'))){
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countpro')->get('single_company_stock_transfers')->row_array()['countpro'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		
		if(trim($this->input->post('shippedOn_from'))){
			$query->where('date(shippedOn) >= ', "date('" . $this->input->post('shippedOn_from') . "')", false);
		}
		if(trim($this->input->post('shippedOn_to'))){
			$query->where('date(shippedOn) <= ', "date('" . $this->input->post('shippedOn_to') . "')", false);
		}
		if(trim($this->input->post('created_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if(trim($this->input->post('created_to'))){
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		
		if($where){
			$query->like($where);
		}
		
		$status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
		$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'goodsMovementId', 'goodsNoteId', 'stockTransferId', 'createdOrderId', 'sourceWarehouseId', 'targetWarehouseId', 'productId', 'sku', 'qty', 'price', 'totalValue', 'shippedOn','created', 'status', 'message');
		
		if($this->input->post('order')){
			foreach($this->input->post('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		
		$datas	= $query->select('id, account1Id, account2Id, goodsMovementId, goodsNoteId, stockTransferId, createdOrderId, sourceWarehouseId, targetWarehouseId, productId, sku, qty, price, totalValue, shippedOn, created, status, message')->limit($limit, $start)->get('single_company_stock_transfers')->result_array();
		
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSingleCompanyStocktx'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSingleCompanyStocktx'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		$allMappedWarehouseName = array();
		$allBPWarehouse	= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		$user_login_data	= $this->session->userdata('login_user_data');
		$IsAdmin			= 0;
		if(($user_login_data['role'] == 'admin') OR ($user_login_data['role'] == '1')){
			$IsAdmin	= 1;
		}
		
		foreach($datas as $data){
			$viewOrder	= '';
			if($data['createdOrderId']){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/journal?txnId='.$data['createdOrderId'].'">View Journal in QBO</a>
								</li>';
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				$account1Mappings[$data['account1Id']]['name'],
				$account2Mappings[$data['account2Id']]['name'],
				$data['goodsMovementId'],
				$data['goodsNoteId'],
				$data['stockTransferId'],
				$data['createdOrderId'],
				$allMappedWarehouseName[$data['sourceWarehouseId']],
				$allMappedWarehouseName[$data['targetWarehouseId']],
				$data['productId'],
				$productMappings[$data['productId']]['sku'],
				$data['qty'],
				$data['price'],
				$data['totalValue'],
				$data['shippedOn'],
				$data['created'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['message'],
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('singlestocktx/singlestocktx/postSinglestocktx/'.$data['stockTransferId']).'">Post On QBO</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blnak" href="'.base_url('singlestocktx/singlestocktx/singlestocktxInfo/'.$data['id']) . '">Stock Transfer Info</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}