<?php
//qbo
#[\AllowDynamicProperties]
class Adjustment_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
	public function fetchStockAdjustment($orderId = '', $accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$salesDatass	= $this->{$this->globalConfig['fetchStockAdjustment']}->fetchStockAdjustment($orderId, $accountId);
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(isset($salesDatassTemps['saveTime'])){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$cronTime		= '';
			$inserted		= 0;
			$insertChunk	= 100;
			$batchInsert	= array();
			$salesDatas		= $salesDatassTemps['return'];
			foreach($salesDatas as $account1Id => $salesData){
				$saveAdjustmentIds		= array();
				$saveActivityIds		= array();
				$savedStockDatas		= $this->db->select('orderId,ActivityId')->get_where('stock_adjustment')->result_array();
				if(!empty($savedStockDatas)){
					$saveAdjustmentIds		= array_column($savedStockDatas,'orderId');
					$saveAdjustmentIds		= array_filter($saveAdjustmentIds);
					
					$saveActivityIds		= array_column($savedStockDatas,'ActivityId');
					$saveActivityIds		= array_filter($saveActivityIds);
				}
				
				$archivedAdjustmentIds	= array();
				$archivedActivityIds	= array();
				$archivedStockDatas		= $this->db->select('orderId,ActivityId')->get_where('stock_adjustment_archive')->result_array();
				if(!empty($archivedStockDatas)){
					$archivedAdjustmentIds	= array_column($archivedStockDatas,'orderId');
					$archivedAdjustmentIds	= array_filter($archivedAdjustmentIds);
					
					$archivedActivityIds	= array_column($archivedStockDatas,'ActivityId');
					$archivedActivityIds	= array_filter($archivedActivityIds);
				}
				
				foreach($salesData as $orderId => $row){
					$duplicateEntry	= 0;
					if(!$orderId){continue;}
					if(in_array($orderId,$saveAdjustmentIds)){continue;}
					if(in_array($orderId,$archivedAdjustmentIds)){continue;}
					
					foreach($row as $UniqueID => $TransferData){
						if($TransferData['ActivityId']){
							if(in_array($TransferData['ActivityId'],$saveActivityIds)){
								$duplicateEntry	= 1;
								break;
							}
							if(in_array($TransferData['ActivityId'],$archivedActivityIds)){
								$duplicateEntry	= 1;
								break;
							}
						}
					}
					if($duplicateEntry){continue;}
					foreach($row as $productId => $gitems){
						$batchInsert[]	= $gitems;
					}
				}
			}
			
			if(!empty($batchInsert)){
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true);
				foreach($batchInserts as $batchInsertTemp){
					$inserted	= $this->db->insert_batch('stock_adjustment', $batchInsertTemp);
				}
			}
			if($inserted == 1){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'stockadjustment'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
	}
	public function postStockAdjustment($id = ''){
		$inventoryDatas	= $this->{$this->globalConfig['postStockAdjustment']}->postStockAdjustment($id);
	}
	public function getAdjustment(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
 			$orderData		= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status != ''){
					$this->db->where_in('id', $ids)->update('stock_adjustment', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		
		$this->db->reset_query();
		$productMappings		= array();
		$productMappingsSKU		= array();
		$productMappingsTemps	= $this->db->select('productId,sku')->get_where('products',array('productId <>' => ''))->result_array();
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			$productMappingsSKU[$productMappingsTemp['sku']]	= $productMappingsTemp;
		}
		
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']			= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']			= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('orderId'))){
				$where['orderId']				= trim($this->input->post('orderId'));
			}
			if(trim($this->input->post('ActivityId'))){
				$where['ActivityId']			= trim($this->input->post('ActivityId'));
			}
			if(trim($this->input->post('GoodNotetype'))){
				$where['GoodNotetype']			= trim($this->input->post('GoodNotetype'));
			}
			if(trim($this->input->post('qty'))){
				$where['qty']					= trim($this->input->post('qty'));
			}
			if(trim($this->input->post('productId'))){
				$where['productId']				= trim($this->input->post('productId'));
			}
			if(trim($this->input->post('goodsNoteId'))){
				$where['goodsNoteId']			= trim($this->input->post('goodsNoteId'));
			}
			if(trim($this->input->post('price'))){
				$where['price']					= trim($this->input->post('price'));
			}
			if(trim($this->input->post('createdOrderId'))){
				$where['createdOrderId']		= trim($this->input->post('createdOrderId'));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']				= trim($this->input->post('status'));
			}
			if(trim($this->input->post('sku'))){
				if($productMappingsSKU[trim($this->input->post('sku'))]){
					$checkProductID		= $productMappingsSKU[trim($this->input->post('sku'))]['productId'];
					$where['productId']	= $checkProductID;
					
				}
			}
		}
		if(trim($this->input->post('updated_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if(trim($this->input->post('warehouseId'))){
			$query->where('warehouseId', $this->input->post('warehouseId'));
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countpro')->get('stock_adjustment')->row_array()['countpro'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('updated_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if(trim($this->input->post('warehouseId'))){
			$query->where('warehouseId', $this->input->post('warehouseId'));
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
		$displayProRowHeader	= array('id','account1Id','account2Id', 'orderId','goodsNoteId','ActivityId', 'createdOrderId', 'warehouseId', 'productId', 'sku', 'qty', 'price', 'value', 'GoodNotetype','created', 'status');
		if($this->input->post('order')){
			foreach($this->input->post('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas	= $query->select('id,account1Id,account2Id,orderId,createdOrderId,goodsOoutId,warehouseId,targetWarehouseId,locationId,reference,productId,sku,rowId,currentItemQty,qty,price,size,tax,status,created,updated,message,GoodNotetype,ActivityId,goodsNoteId,shippedDate ')->limit($limit, $start)->get('stock_adjustment')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		$allMappedWarehouseName = array();
		$allBPWarehouse			= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		$user_login_data	= $this->session->userdata('login_user_data');
		$IsAdmin			= 0;
		if($user_login_data['role'] == 'admin'){
			$IsAdmin	= 1;
		}
		foreach($datas as $data){
			$transfertype	= 'Inventory Adjustment';
			$postID			= $data['orderId'];
			$InfoButton		= 'Adjustment';
			if($data['GoodNotetype'] == 'GO'){
				$transfertype	= 'Inventory Transfer';
				$postID			= $data['ActivityId'];
				$InfoButton		= 'Transfer';
			}
			if($data['GoodNotetype'] == 'SC'){
				$transfertype	= 'Inventory Adjustment';
				$postID			= $data['orderId'];
				$InfoButton		= $data['orderId'];
				$InfoButton		= 'Adjustment';
			}
			$postOrder	= '';
			$viewOrder	= '';
			if($transfertype == 'Inventory Adjustment'){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/bill?txnId='.$data['createdOrderId'].'">View Bill in QBO</a>
								</li>';
			}
			if($transfertype == 'Inventory Transfer'){
				if($data['qty'] > 0){
					$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/bill?txnId='.$data['createdOrderId'].'">View Bill in QBO</a>
								</li>';
				}
				if($data['qty'] < 0){
					$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account2Mappings[$data['account2Id']]['viewUrl'].'/invoice?txnId='.$data['createdOrderId'].'">View Invoice in QBO</a>
								</li>';
				}
			
			}
			$postOrder	=	'<li>
								<a class="btnactionsubmit" href="'.base_url('stock/adjustment/postStockAdjustment/'.$postID).'">Post Stock Adjustment</a>
							</li>';
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				$data['orderId'],
				$data['goodsNoteId'],
				$data['ActivityId'],
				$data['createdOrderId'],
				$allMappedWarehouseName[$data['warehouseId']],
				$data['productId'],
				$productMappings[$data['productId']]['sku'],
				$data['qty'],
				@sprintf("%.2f",$data['price']),
				@sprintf("%.2f",($data['qty'] * $data['price'])),
				$transfertype,
				$data['created'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						'.$postOrder.'
						<li>
							<a class="newInfoBtn" target="_blnak" href="' . base_url('stock/adjustment/adjustmentinfo/'.$data['id']) . '">Stock '.$InfoButton.' Info</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}