<?php
/**
 * @package     STOCK SYNC MODEL
 * @subpackage  FETCH STOCK SYNC, FETCH ON ORDER STOCK, POST SYNC, GET ALL SYNCD STOCK
 * @copyright   @HRD TECHNOLOGY PVT. LTD. 2018
 * @license     MIT License
 */
 #[\AllowDynamicProperties]
class Sync_model extends CI_Model{
	
    public function __construct(){
        parent::__construct();
    }
    public function fetchSync($sku = ''){
		$account2StockDatas = $this->{$this->globalConfig['fetchStockSync']}->getProductStock();
		if(!$account2StockDatas){return false;}  
		$query = $this->db;
		if($sku){
			$query->where(array('sku' => $sku));
		}
        $saveDatasTemps = $query->select('id,productId,account1Id,account2Id,sku,size,color,createdProductId,createdVariantId,params')->get('products')->result_array();
        $bpProduct = array();
        $matchProducts = array();
		foreach($saveDatasTemps as $saveDatasTemp){
			$params = json_decode($saveDatasTemp['params'],true);
			if(!$params['stock']['stockTracked']){continue;} 
			$bpProduct[strtolower($saveDatasTemp['sku'])]= $saveDatasTemp;
			if(isset($account2StockDatas[strtolower($saveDatasTemp['sku'])])){
				$matchProducts[strtolower($saveDatasTemp['sku'])] = $account2StockDatas[strtolower($saveDatasTemp['sku'])];
			}
			else{
				$matchProducts[strtolower($saveDatasTemp['sku'])] = array('onHand' => '0','sku' => $saveDatasTemp['sku'],'internalSku' => $saveDatasTemp['sku']);
			}		
		}
		//$matchProducts = array_intersect_key($bpProduct,$account2StockDatas);
		if(!$matchProducts){return false;}
		$account1StockDatass = $this->{$this->globalConfig['postStockSync']}->fetchInventoradvice();
		$accountIds = array_keys($account1StockDatass);
		foreach($accountIds as $accountId){
			foreach($account1StockDatass[$accountId] as $account2Id => $account1StockDatas){
				$config = $this->{$this->globalConfig['postStockSync']}->accountConfig[$accountId];
				foreach($matchProducts as $sku => $productData){
					$productId = @$productData['productId'];
					if($productId){ 
						$bpStock = @(int)$account1StockDatas[$productId]['warehouses'][$config['warehouse']]['onHand'];
						$felxeStock = @(int)$account2StockDatas[$sku]['onHand'];
						$adjustmentQty = $felxeStock - $bpStock;
						//if($adjustmentQty == 0){continue;}
						$stockInsert  = array(
							'account1Id'          => $accountId,
							'productId'           => $productId,
							'createdProductId'    => $productId,
							'sku'                 => $productData['sku'],
							'sendTo'              => 'brightpearl', 
							'account1WarehouseId' => $config['warehouse'],
							'account2WarehouseId' => $config['warehouse'],
							'account1StockQty'    => @$bpStock,
							'account2StockQty'    => @$felxeStock,
							'adjustmentQty'       => $adjustmentQty,
							'status'              => '0',
						);
						$this->db->replace('stock_sync', $stockInsert);
						$this->db->insert('stock_sync_log', $stockInsert);
					}
				}
			}
		}
    }
    
	public function postSync($sku = ''){
        $this->{$this->globalConfig['postStockSync']}->postSync($sku);
    }
    
	public function getSync(){
        $groupAction     = $this->input->post('customActionType');
        $records         = array();
        $records["data"] = array();
        //Set order value in session to show selected order on page load
		if($this->input->post('order')){
 			$orderData = array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids = $this->input->post('id');
            if ($ids) {
                $status = $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('stock_sync', array('status' => $status));
                    $records["customActionStatus"]  = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }
        $where = array();
        $query = $this->db;
        if ($this->input->post('action') == 'filter') {
            
            if (trim($this->input->post('account1WarehouseId'))) {
                $where['account1WarehouseId'] = trim($this->input->post('account1WarehouseId'));
            }
            if (trim($this->input->post('sku'))) {
                $where['sku'] = trim($this->input->post('sku'));
            }
            if (trim($this->input->post('account1StockQty'))) {
                $where['account1StockQty'] = trim($this->input->post('account1StockQty'));
            }
            if (trim($this->input->post('account2StockQty'))) {
                $where['account2StockQty'] = trim($this->input->post('account2StockQty'));
            }
            if (trim($this->input->post('sku'))) {
                $where['sku'] = trim($this->input->post('sku'));
            }
            if (trim($this->input->post('adjustmentQty'))) {
                $where['adjustmentQty'] = trim($this->input->post('adjustmentQty'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status'] = trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }

        $totalRecord = @$query->select('count("id") as countpro')->get('stock_sync')->row_array()['countpro'];
        $limit       = intval($this->input->post('length'));
        $limit       = $limit < 0 ? $totalRecord : $limit;
        $start       = intval($this->input->post('start'));

        $query = $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }

        $status              = array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor         = array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader = array('id', 'account1WarehouseId', 'sku', 'account1StockQty', 'account2StockQty','adjustmentQty', 'created', 'status', 'size');
        if ($this->input->post('order')) {
            foreach ($this->session->userdata('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas = $query->select('id,productId,createdProductId,name,sku,color,created,status,size,adjustmentQty,account2WarehouseId,orderNo,name,account1Id,account2Id,account1StockQty,account2StockQty,account1WarehouseId')->limit($limit, $start)->get('stock_sync')->result_array();
		
        foreach ($datas as $data) {
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                $data['account1WarehouseId'],
                $data['sku'],
                $data['account1StockQty'],
                $data['account2StockQty'],
                $data['adjustmentQty'],
                $data['created'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
                '<div class="btn-group">
                    <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu pull-right">
                        <li>
                            <a class="btnactionsubmit" href="' . base_url('stock/sync/fetchSync/' . $data['sku']) . '"> Fetch Stock Sync </a>
                        </li>
                        <li>
                            <a class="btnactionsubmit" href="' . base_url('stock/sync/postSync/' . $data['sku']) . '"> Post Stock Sync </a>
                        </li>
                    </div>
                </div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
}