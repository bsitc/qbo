<?php
#[\AllowDynamicProperties]
class Synclog_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getSynclog()
    {
        $groupAction     = $this->input->post('customActionType');
        $records         = array();
        $records["data"] = array();

        //Set order value in session to show selected order on page load
        if($this->input->post('order')){
 			$orderData = array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids = $this->input->post('id');
            if ($ids) {
                $status = $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('stock_sync_log', array('status' => $status));
                    $records["customActionStatus"]  = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }
        $where = array();
        $query = $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('account2WarehouseId'))) {
                $where['account2WarehouseId'] = trim($this->input->post('account2WarehouseId'));
            }
            if (trim($this->input->post('sku'))) {
                $where['sku'] = trim($this->input->post('sku'));
            }
            if (trim($this->input->post('color'))) {
                $where['color'] = trim($this->input->post('color'));
            }
            if (trim($this->input->post('size'))) {
                $where['size'] = trim($this->input->post('size'));
            }
            if (trim($this->input->post('createdProductId'))) {
                $where['createdProductId'] = trim($this->input->post('createdProductId'));
            }
            if (trim($this->input->post('adjustmentQty'))) {
                $where['adjustmentQty'] = trim($this->input->post('adjustmentQty'));
            }
            if (trim($this->input->post('name'))) {
                $where['name'] = trim($this->input->post('name'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status'] = trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }

        $totalRecord = @$query->select('count("id") as countpro')->get('stock_sync_log')->row_array()['countpro'];
        $limit       = intval($this->input->post('length'));
        $limit       = $limit < 0 ? $totalRecord : $limit;
        $start       = intval($this->input->post('start'));

        $query = $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }

        $status              = array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor         = array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader = array('id', 'productId', 'createdProductId', 'name', 'sku', 'color', 'created', 'status', 'size');
        if ($this->session->userdata('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas = $query->select('id,productId,createdProductId,name,sku,color,created,status,size,adjustmentQty,account2WarehouseId,orderNo,name')->limit($limit, $start)->get('stock_sync_log')->result_array();
        foreach ($datas as $data) {
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                $data['account2WarehouseId'],
                $data['name'],
                $data['sku'],
                $data['color'],
                $data['size'],
                $data['adjustmentQty'],
                $data['orderNo'],
                $data['created'],
                '',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
}
