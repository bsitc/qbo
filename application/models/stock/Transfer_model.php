<?php
#[\AllowDynamicProperties]
class Transfer_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    public function fetchStockTransfer($transferId = '',$goodsId = '', $fetchedFromBp = '1',$fetchedFromVend = '1'){
		if($fetchedFromBp){
			$stockTransferDatas	= $this->{$this->globalConfig['fetchStockTransfer']}->fetchStockTransfer($transferId, $goodsId);
			$saveDatasTemps		= $this->db->select('id,account1Id,account2Id,orderId')->get('stock_transfer')->result_array();
			$saveDatas			= array();
			$saveId2Datas		= array();
			foreach ($saveDatasTemps as $saveDatasTemp) {
				$key				= @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($saveDatasTemp['account1Id'].'_'.$saveDatasTemp['account2Id'].'_'.$saveDatasTemp['orderId'])));
				$saveDatas[$key]	= $saveDatasTemp;
			}
			$batchOrderInsert	= array();
			$batchItemInsert	= array();
			$batchAddressInsert	= array();
			$batchUpdate		= array();
			foreach ($stockTransferDatas as $account1Id => $stockTransferData) {
				foreach ($stockTransferData as $account2Id => $stockTransfer) {
					foreach($stockTransfer as $transfers){
						$key	=  @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($transfers['account1Id'].'_'.$transfers['account2Id'].'_'.$transfers['orderId'])));
						if(@$saveDatas[$key]){
							// add code for update
						}
						else{
							$batchOrderInsert[]		= $transfers['sales'];
							foreach($transfers['items'] as $items)
							$batchItemInsert[]		= $items;
							$batchAddressInsert[]	= $transfers['address'];
						}
					}
				}
			}		
			if($batchOrderInsert){
				$this->db->insert_batch('stock_transfer', $batchOrderInsert);
			}
			if($batchItemInsert){
				$this->db->insert_batch('stock_transfer_item', $batchItemInsert);
			}
			if($batchAddressInsert){
				$this->db->insert_batch('stock_transfer_address', $batchAddressInsert);
			}
			if($batchUpdate){
				//$this->db->update_batch('products',$batchUpdate, 'id'); 
			}
		}
		if($fetchedFromVend){
			$stockTransferDatas	= $this->{$this->globalConfig['postStockTransfer']}->fetchStockTransfer($transferId);	
			$saveDatasTemps		= $this->db->select('id,account1Id,account2Id,orderId')->get('stock_transfer')->result_array();
			$saveDatas			= array();
			$saveId2Datas		= array();
			foreach ($saveDatasTemps as $saveDatasTemp) {
				$key				= @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($saveDatasTemp['account1Id'].'_'.$saveDatasTemp['account2Id'].'_'.$saveDatasTemp['orderId'])));
				$saveDatas[$key]	= $saveDatasTemp;
			}
			$batchOrderInsert	= array();
			$batchItemInsert	= array();
			$batchAddressInsert	= array();
			$batchUpdate		= array();
			foreach ($stockTransferDatas as $account1Id => $stockTransferData) {
				foreach ($stockTransferData as $account2Id => $stockTransfer) {
					foreach($stockTransfer as $transfers){
						$key	=  @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($transfers['account1Id'].'_'.$transfers['account2Id'].'_'.$transfers['orderId'])));
						if(@$saveDatas[$key]){
							// add code for update
						}
						else{
							$batchOrderInsert[]	= $transfers['sales'];
							foreach($transfers['items'] as $items)
							$batchItemInsert[]	= $items;
						}
					}
				}
			}		
			if($batchOrderInsert){
				$this->db->insert_batch('stock_transfer', $batchOrderInsert);
			}
			if($batchItemInsert){
				$this->db->insert_batch('stock_transfer_item', $batchItemInsert);
			}
			if($batchAddressInsert){
				$this->db->insert_batch('stock_transfer_address', $batchAddressInsert);
			}
			if($batchUpdate){
				//$this->db->update_batch('products',$batchUpdate, 'id'); 
			}
		}
		$latestTransfer	= $this->db->order_by('goodsOutId','DESC')->get('stock_transfer')->row_array();
		$transferId		= $latestTransfer['orderId'];
		if($latestTransfer['sendTo'] == 'brightpearl'){
			$transferId	= $latestTransfer['createOrderId'];
		}
		$transferTrackData	= $this->db->order_by('transferId','DESC')->get_where('stock_transfer_tracking')->row_array();
		if($transferTrackData['transferId'] < $transferId){			
			$this->db->update('stock_transfer_tracking', array('transferId' => $latestTransfer['orderId'], 'goodsId' => $latestTransfer['goodsOutId']));
		}
    }
    public function postStockTransfer($newSku = ''){		
		$this->{$this->globalConfig['postStockTransfer']}->postStockTransfer();
		$this->{$this->globalConfig['fetchStockTransfer']}->postStockTransfer();
    }
    public function getTransfer(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
		if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('stock_transfer', array('status' => $status));
                    $records["customActionStatus"]  = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId']		= trim($this->input->post('orderId'));
                $where['orderNo']		= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId'] = trim($this->input->post('createOrderId'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo']		= trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId']	= trim($this->input->post('customerId'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail'] = trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod'] = trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']		= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countsales')->get('stock_transfer')->row_array()['countsales'];
        $limit       	= intval($this->input->post('length'));
        $limit       	= $limit < 0 ? $totalRecord : $limit;
        $start       	= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status              	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Partially Received', '3' => 'Fully Received', '4' => 'Archive');
        $statusColor         	= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'success', '4' => 'danger');
        $displayProRowHeader 	= array('id', 'account1Id', 'account2Id', 'orderId', 'createOrderId', 'goodsOutId', 'created', 'status');
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,goodsOutId,account1Id,account2Id')->limit($limit, $start)->get('stock_transfer')->result_array();
		$account1Mappings = array();$account2Mappings = array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchStockTransfer'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postStockTransfer'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
        foreach ($datas as $data) {
        	$params		= json_decode($data['rowData'],true);
			$message	= $data['message'];
			if($data['cancelRequest']){
				$message	= '<span class="label label-sm label-danger">Cancel request received</span>';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'], 
                @$account2Mappings[$data['account2Id']]['name'],  
                '<a href="'.base_url('stock/transfer/salesItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                $data['createOrderId'],
                $data['goodsOutId'], 
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$message,
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('stock/transfer/fetchstock/'.$data['orderId']).'"> Fetch Sales Order </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('stock/transfer/poststock/'.$data['orderId']).'"> Post Sales Order </a>
						</li>
						<li>
							<a target = "_blank" href="'.base_url('stock/transfer/salesInfo/'.$data['orderId']).'"> Sales Info </a>
						</li>			
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]			= $draw;
        $records["recordsTotal"]	= $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
	public function getStockItem($orderId){
		$datas = $this->db->get_where('stock/_item',array('orderId' => $orderId))->result_array();
		return $datas;
	}
}