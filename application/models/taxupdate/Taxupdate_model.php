<?php
#[\AllowDynamicProperties]
class Taxupdate_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
	public function get(){
		$data					= array();
		$data['data']			= $this->db->get('taxupdate_config')->result_array();
		$data['channel']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannel();
		$data['orderstatus']	= $this->{$this->globalConfig['account1Liberary']}->getAllOrderStatus();
		$saveAccountTemps		= $this->db->get('account_brightpearl_account')->result_array();
		$saveAccount			= array();
		foreach($saveAccountTemps as $saveAccountTemp){
			$saveAccount[$saveAccountTemp['id']]	= $saveAccountTemp;
		}
		$data['saveAccount']	= $saveAccount;
		return $data;
	}
	public function delete($id){
		$this->db->where(array('id' => $id))->delete('taxupdate_config');
	}
	public function save($data){
		$BPAccount		= $this->db->get_where('account_brightpearl_account', array('id' => $data['brightpearlAccountId']))->row_array();
		$data['name']	= $BPAccount['name'];
		if(is_array($data['channelIds'])){
			$data['channelIds']	= array_filter($data['channelIds']);
			$data['channelIds']	= array_unique($data['channelIds']);
			$data['channelIds']	= $data['channelIds'] ? implode(",",$data['channelIds']) : "";
		}
		if($data['id']){
			$status		= $this->db->where(array('id' => $data['id']))->update('taxupdate_config', $data);
		}
		else{
			$saveConfig	= $this->db->get_where('taxupdate_config', array('id' => $data['id']))->row_array();
			if($saveConfig){
				$data['id']	= $saveConfig['id'];
				$status		= $this->db->where(array('id' => $data['id']))->update('taxupdate_config', $data);
			}
			else{
				$status		= $this->db->insert('taxupdate_config', $data);
				$data['id']	= $this->db->insert_id();
			}
		}
		$data	= $this->db->get_where('taxupdate_config', array('id' => $data['id']))->row_array();
		if($data['id']){
			$data['status']	= '1';
		}
		return $data;
	}
}