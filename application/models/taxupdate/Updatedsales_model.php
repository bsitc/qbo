<?php
#[\AllowDynamicProperties]
class Updatedsales_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci	= get_instance();
	}
	public function fetchAllSalesForUpdate($orderId = '', $accountId = ''){
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$FetchSalesDatas	= $this->{$this->globalConfig['fetchSalesOrder']}->fetchAllSalesForUpdate($orderId, $accountId);
		$fatchedOrderIds	= array();
		foreach($FetchSalesDatas as $FetchAccount1Id => $FetchSalesData){
			if($FetchSalesData['saveTime']){
				$saveTime	= $FetchSalesData['saveTime'] - (60*10);
			}
			$SalesDatas				= $FetchSalesData['return'];
			$batchInsert			= array();
			$batchUpdate			= array();
			foreach($SalesDatas as $account1Id => $SalesData){
				$SaveSalesOrderInfo	= array();
				$SaveSalesOrderData	= $this->db->select('id,orderId,status')->get_where('sales_order_customisation',array('account1Id' => $account1Id))->result_array();
				foreach($SaveSalesOrderData as $SaveSalesOrderDatas){
					$SaveSalesOrderInfo[$SaveSalesOrderDatas['orderId']]	= $SaveSalesOrderDatas;
				}
				foreach($SalesData as $orderId => $row){
					if(!$orderId){
						continue;
					}
					if($SaveSalesOrderInfo[$orderId]){
						if($SaveSalesOrderInfo[$orderId]['status'] == 1){
							continue;
						}
						else{
							$batchUpdate[]	= $row['orders'];
						}
					}
					else{
						$batchInsert[]	= $row['orders'];
					}
				}
			}	
			$inserted		= 0;
			$updateOrder	= 100;
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$this->db->update_batch('sales_order_customisation', $batchUpdate,'orderId');
					}
				}
			}
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('sales_order_customisation', $batchInsert); 
				}
			}
			if($inserted){
				$this->db->insert('cron_management', array('type' => 'allupdatesales'.$FetchAccount1Id, 'saveTime' => $saveTime)); 
			}
		}
	}
	public function fetchAllCreditForUpdate($orderId = '', $accountId = ''){
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$FetchCreditDatas	= $this->{$this->globalConfig['fetchSalesOrder']}->fetchAllCreditForUpdate($orderId, $accountId);
		$fatchedOrderIds	= array();
		foreach($FetchCreditDatas as $FetchAccount1Id => $FetchCreditData){
			if($FetchCreditData['saveTime']){
				$saveTime	= $FetchCreditData['saveTime'] - (60*10);
			}
			$CreditDatas	= $FetchCreditData['return'];
			$batchInsert	= array();
			$batchUpdate	= array();
			foreach($CreditDatas as $account1Id => $CreditData){
				$SaveSalesCreditInfo	= array();
				$SaveSalesCreditData	= $this->db->select('id,orderId,status')->get_where('sales_order_customisation',array('account1Id' => $account1Id))->result_array();
				foreach($SaveSalesCreditData as $SaveSalesCreditDatas){
					$SaveSalesCreditInfo[$SaveSalesCreditDatas['orderId']]	= $SaveSalesCreditDatas;
				}
				foreach($CreditData as $orderId => $row){
					if(!$orderId){
						continue;
					}
					if($SaveSalesCreditInfo[$orderId]){
						if($SaveSalesCreditInfo[$orderId]['status'] == 1){
							continue;
						}
						else{
							$batchUpdate[]	= $row['orders'];
						}
					}
					else{
						$batchInsert[]	= $row['orders'];
					}
				}
			}	
			$inserted		= 0;
			$updateOrder	= 100;
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$this->db->update_batch('sales_order_customisation', $batchUpdate,'orderId');
					}
				}
			}
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('sales_order_customisation', $batchInsert); 
				}
			}
			if($inserted){
				$this->db->insert('cron_management', array('type' => 'allupdatesalescredit'.$FetchAccount1Id, 'saveTime' => $saveTime)); 
			}
		}
	}
	public function postAllUpdatedSales($orderId){
		$this->{$this->globalConfig['fetchSalesOrder']}->postAllUpdatedSales($orderId, $accountId = '');
	}
	public function postAllUpdatedCredit($orderId){
		$this->{$this->globalConfig['fetchSalesOrder']}->postAllUpdatedCredit($orderId, $accountId = '');
	}
	public function getupdatedsales(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
		if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if($groupAction == 'group_action'){
            $ids	= $this->input->post('id');
            if($ids){
                $status	= $this->input->post('customActionName');
				if($status != ''){
                    $this->db->where_in('id', $ids)->update('sales_order_customisation', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
                $where['account1Id']		= trim($this->input->post('account1Id'));
            }
            if(trim($this->input->post('orderId'))){
                $where['orderId']			= trim($this->input->post('orderId'));                
            }
			if(trim($this->input->post('orderType'))){
                $where['orderType']			= trim($this->input->post('orderType'));                
            }
            if(trim($this->input->post('reference'))){
                $where['reference']			= trim($this->input->post('reference'));
            }
			if(trim($this->input->post('channelName'))){
                $where['channelName']		= trim($this->input->post('channelName'));
            }
			if(trim($this->input->post('Country'))){
                $where['Country']			= trim($this->input->post('Country'));
            }
            if(trim($this->input->post('status')) >= '0'){
                $where['status']			= trim($this->input->post('status'));
            }
        }
        if(trim($this->input->post('created_from'))){
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if(trim($this->input->post('created_to'))){
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if($where){
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countsales')->get('sales_order_customisation')->row_array()['countsales'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
		$query			= $this->db;
        if($where){
            $query->like($where);
        }
        $status					= array('0' => 'Pending', '1' => 'Order Sent');
        $statusColor			= array('0' => 'default', '1' => 'success');
        $displayProRowHeader	= array('id', 'account1Id', 'orderId', 'reference', 'channelName', 'Country', 'created', 'status', 'message');
		if($this->session->userdata($this->router->directory.$this->router->class)){
            foreach ($this->session->userdata($this->router->directory.$this->router->class) as $ordering){
                if(@$displayProRowHeader[$ordering['column']]){
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('id, account1Id, orderId, orderType, reference, channelId, channelName, Country, status, created, updated, message, rowData, rowData, createdRowData')->limit($limit, $start)->get('sales_order_customisation')->result_array();
		$account1Mappings = array(); 
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$user_login_data	= $this->session->userdata('login_user_data');
		$IsAdmin			= 0;
		if($user_login_data['role'] == 'admin'){
			$IsAdmin	= 1;
		}
        foreach($datas as $data){
			$postOrder	= '';
			$infoOrder	= '';
			$viewOrder	= '';
			$orderType	= '';
			if($IsAdmin){
				//
			}
			if($data['status'] != 1){
				if($data['orderType'] == 'SO'){
					$postOrder	= 	'<li>
										<a class="btnactionsubmit" href="'.base_url('/taxupdate/updatedsales/postAllUpdatedSales1/'.$data['orderId']).'">Post Updated Sales Order</a>
									</li>';
				}
				elseif($data['orderType'] == 'SC'){
					$postOrder	= 	'<li>
										<a class="btnactionsubmit" href="'.base_url('/taxupdate/updatedsales/postAllUpdatedCredit/'.$data['orderId']).'">Post Updated Sales Credit</a>
									</li>';
				}
				else{
					$postOrder	= '';
				}
			}
			if($data['orderType'] == 'SO'){
				$orderType	=	'SalesOrder';
				$infoOrder	=	'<li>
									<a class="newInfoBtn" target = "_blank" href="'.base_url('/taxupdate/updatedsales/updatedsalesInfo/'.$data['orderId']).'">Sales Info</a>
								</li>';
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View Sales in Brightpearl</a>
								</li>';
			}
			elseif($data['orderType'] == 'SC'){
				$orderType	= 'SalesCredit';
				$infoOrder	=	'<li>
									<a class="newInfoBtn" target = "_blank" href="'.base_url('/taxupdate/updatedsales/updatedsalesInfo/'.$data['orderId']).'">Credit Info</a>
								</li>';
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View Credit in Brightpearl</a>
								</li>';
			}
			else{
				$orderType	= '';
				$infoOrder	= '';
				$viewOrder	= '';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				$account1Mappings[$data['account1Id']]['name'],
                $orderType,
                $data['orderId'],
				$data['reference'],
				$data['channelName'],
				$data['Country'],
                $data['created'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['message'],
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						'.$postOrder.$infoOrder.$viewOrder.'
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
}