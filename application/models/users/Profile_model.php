<?php
//qbo
#[\AllowDynamicProperties]
class Profile_model extends CI_Model{
	public function getUserDetails(){
		if(@!$this->session_data){
			return @$this->db->get_where('admin_user')->row_array();
		}
		else{
			return @$this->db->get_where('admin_user',array('user_id' => $this->session_data['user_session_data']['user_id']))->row_array();			
		}
	}
	public function getGlobalConfig(){
		return $this->db->get('global_config')->row_array();
	}
	public function saveBasic($data){
		if($this->session_data['user_session_data']['user_id']){
			$this->db->where(array('user_id' => $this->session_data['user_session_data']['user_id']))->update('admin_user',$data);
		}
	}
	public function updatePassword($data){
		$this->db->where(array('user_id' => $this->session_data['user_session_data']['user_id'] , 'password' => md5($data['password'])))->update('admin_user', array('password' => md5($data['newpassword'])));
		return $this->db->affected_rows();
	}
	public function update( $data = array() ){
		if(count($data)>0){
			$this->db->where('user_id', $data['user_id']);
			return $this->db->update(ADMIN_USER, $data);	
 		}
		else{
			return false;
		}
	}
	public function uploadedProfiePic($data){
		$this->db->where(array('user_id' => $this->session_data['user_session_data']['user_id']))->update('admin_user',$data);
		return $this->db->affected_rows();
	}
	public function executeMultipleSql($filePath = ''){
		if(file_exists($filePath)){
			$fileData	= file_get_contents($filePath);
			if($fileData){
				$sqls	= explode(';', $fileData);
				foreach($sqls as $statement){
					if(strlen($statement) > 5){
						$statment	= $statement . ";";
						$this->db->query($statement); 
					}
				}
			}
		}
	}
	public function saveGlobalConfig($data){
		if(@is_array($data['archiveType'])){
			$data['archiveType']	= array_filter($data['archiveType']);
			$data['archiveType']	= array_unique($data['archiveType']);
			$data['archiveType']	= $data['archiveType'] ? implode(",",$data['archiveType']) : ""; 
		}
		if($data['account1Liberary']){
			$sql	= "SHOW TABLES LIKE 'account_".$data['account1Liberary']."%'";
			$query	= $this->db->query($sql);
			$rows	= $query->row_array();
			if(!$rows){
				$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'account'.DIRECTORY_SEPARATOR.$data['account1Liberary'].'.sql';
				$this->executeMultipleSql($filePath);
			}
		}
		if($data['account2Liberary']){
			$sql	= "SHOW TABLES LIKE 'account_".$data['account2Liberary']."%'";
			$query	= $this->db->query($sql);
			$rows	= $query->row_array();
			if(!$rows){
				$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'account'.DIRECTORY_SEPARATOR.$data['account2Liberary'].'.sql';
				$this->executeMultipleSql($filePath);
			}
		}
		foreach($data as $key => $value){
			if($value){
				if(substr_count($key,'Mapping')){
					$mappingKey	= str_replace('enable','',$key);
					$mappingKey	= str_replace('Mapping','',$mappingKey);
					if($mappingKey){
						$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'mapping'.DIRECTORY_SEPARATOR.strtolower($mappingKey).'.sql';
						$this->executeMultipleSql($filePath);
					}
				}
			}
		}
		if(@$data['enableProduct']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'product.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableCustomer']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'customers.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableSalesOrder']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'sales'.DIRECTORY_SEPARATOR.'sales.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableSalesCredit']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'sales'.DIRECTORY_SEPARATOR.'credit.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enablePurchaseOrder']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'purchase'.DIRECTORY_SEPARATOR.'purchase.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enablePurchaseCredit']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'purchase'.DIRECTORY_SEPARATOR.'credit.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableStockTransfer']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'transfer.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableStockSync']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'sync.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableInventoryadvice']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'advice.sql';
			$this->executeMultipleSql($filePath);
		}
		$getGlobalConfig	= $this->getGlobalConfig();
		$this->db->where(array('id' => $getGlobalConfig['id']))->update('global_config',$data);
	}
	public function updateMetadata($data){
		if($data){
			$orderTypeArray	= array("sales_order","sales_credit_order","purchase_order","purchase_credit_order");
			$return			= 1;
			$allIdsSets		= $data['Ids'];
			$allIdsSets		= explode(",",$allIdsSets);
			$datatype		= $data['datatype'];
			$actionRequired	= $data['actionRequired'];
			$account2Name	= $data['account2Name'];
			if((!$allIdsSets) OR (!$datatype) OR (!$actionRequired)){
				$return	= 0;
			}
			else{
				if(in_array($datatype, $orderTypeArray)){
					if($actionRequired != '111'){
						$this->db->where_in('orderId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
					}
					elseif($actionRequired == '111'){
						$allpayments	= $this->db->where_in('orderId',$allIdsSets)->select('orderId,paymentDetails,status')->get_where($datatype)->result_array();
						if($allpayments){
							foreach($allpayments as $allpaymentsData){
								$updateArray	= array();
								$orderId		= $allpaymentsData['orderId'];
								$paymentDetails	= json_decode($allpaymentsData['paymentDetails'],true);
								if(!$paymentDetails){continue;}
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['IsReversal'] == 1){
										$paymentDetails[$pkey]['DeletedonBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								if($allpaymentsData['status'] != 4){
									$updateArray['status']	= 3;
								}
								$this->db->where(array('orderId' => $orderId))->update($datatype,$updateArray);
							}
						}
					}
				}
				else{
					if($datatype == 'cogs_journal'){
						$this->db->where_in('journalsId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
					}
					elseif($datatype == 'amazon_ledger'){
						$this->db->where_in('journalsId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
					}
					elseif($datatype == 'stock_journals'){
						$this->db->where_in('journalId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
					}
					elseif($datatype == 'stock_adjustment'){
						$this->db->where_in('orderId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
					}
					elseif($datatype == 'customers'){
						if($actionRequired == "222"){
							$this->db->where_in('customerId',$allIdsSets)->where(array('account2Id' => $account2Name))->update($datatype, array('createdCustomerId' => '', 'status' => 0));
						}
						else{
							$this->db->where_in('customerId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
						}
					}
					elseif($datatype == 'products'){
						if($actionRequired == "222"){
							$this->db->where_in('productId',$allIdsSets)->where(array('account2Id' => $account2Name))->update($datatype, array('createdProductId' => '', 'status' => 0));
						}
						else{
							$this->db->where_in('productId',$allIdsSets)->update($datatype,array('status' => $actionRequired));
						}
					}
				}
			}
			return $return;
		}
		
	}
	public function updateDataIds($data){
		if($data){
			$return			= 1;
			$brightpearlId	= $data['brightpearlId'];
			$newupdateId	= $data['newupdateId'];
			$datatype		= $data['datatype'];
			$account2Name	= $data['account2Name'];
			if((!$brightpearlId) OR (!$newupdateId) OR (!$datatype) OR (!$account2Name)){
				$return	= 0;
			}
			else{
				if($datatype == 'customers'){
					$this->db->where(array('customerId' => $brightpearlId, 'account2Id' => $account2Name))->update($datatype, array('createdCustomerId' => $newupdateId, 'status' => 1));
					$return	= $this->db->affected_rows();
				}
				elseif($datatype == 'products'){
					$this->db->where(array('productId' => $brightpearlId, 'account2Id' => $account2Name))->update($datatype, array('createdProductId' => $newupdateId, 'status' => 1));
					$return	= $this->db->affected_rows();
				}
			}
			return $return;
		}
		
	}
	public function getAutomationConfig(){
		return $this->db->get('automationSetup')->row_array();
	}
	public function saveAutomation($data){
		$AutomationConfig	= $this->getAutomationConfig();
		$this->db->where(array('id' => $AutomationConfig['id']))->update('automationSetup',$data);
	}
}
?>