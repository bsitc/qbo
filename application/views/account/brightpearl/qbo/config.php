<?php
	//qbo
	$allCurrencyList		= array('AFA','ALL','DZD','AOR','ARS','AMD','AWG','AUD','AZN','BSD','BHD','BDT','BBD','BYN','BZD','BMD','BTN','BOB','BWP','BRL','GBP','BND','BGN','BIF','KHR','CAD','CVE','KYD','XOF','XAF','XPF','CLP','CNY','COP','KMF','CDF','CRC','HRK','CUP','CZK','DKK','DJF','DOP','XCD','EGP','SVC','ERN','EEK','ETB','EUR','FKP','FJD','GMD','GEL','GHS','GIP','XAU','XFO','GTQ','GNF','GYD','HTG','HNL','HKD','HUF','ISK','XDR','INR','IDR','IRR','IQD','ILS','JMD','JPY','JOD','KZT','KES','KWD','KGS','LAK','LVL','LBP','LSL','LRD','LYD','LTL','MOP','MKD','MGA','MWK','MYR','MVR','MRO','MUR','MXN','MDL','MNT','MAD','MZN','MMK','NAD','NPR','ANG','NZD','NIO','NGN','KPW','NOK','OMR','PKR','XPD','PAB','PGK','PYG','PEN','PHP','XPT','PLN','QAR','RON','RUB','RWF','SHP','WST','STD','SAR','RSD','SCR','SLL','XAG','SGD','SBD','SOS','ZAR','KRW','LKR','SDG','SRD','SZL','SEK','CHF','SYP','TWD','TJS','TZS','THB','TOP','TTD','TND','TRY','TMT','AED','UGX','XFU','UAH','UYU','USD','UZS','VUV','VEF','VND','YER','ZMK','ZWL');
	$account1Name		= $this->globalConfig['account1Name'];	
	$account1APIField	= $data['account1APIField'];
	$userLoginData		= $this->session->userdata('login_user_data');
	$accessRoles		= array('admin', 'developer', '1');
	if(in_array($userLoginData['role'], $accessRoles)){
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url(); ?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Account Settings</span><i class="fa fa-circle"></i></li>
				<li><span><?php echo $account1Name ?> Settings</span><i class="fa fa-circle"></i></li>
				<li><span>Default Configuration</span></li>
            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo $account1Name ?> Configuration</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Configuration</span>
					</a>
				</div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%"><?php echo $account1Name ?> ID</th>
                                    <th width="25%">Account Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('/account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php	foreach($data['data'] as $key => $row){	?>
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"><?php echo $row['brightpearlAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('/account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php	}	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog  modal-xl">        
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><b><?php echo $account1Name ?> Configuration</b></h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('/account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>  
							<input type="hidden" name="data[id]" class="id" />
						</form>                         
					</div>				
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction sbold btnExtra">Save</button>
						<button type="button" class="btn yellow btn-outline sbold btnExtra" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>
<div class="confighml">
<?php   
    $data['data']	= ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {	
?>
	<div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide"><button class="close" data-close="alert"></button>You have some form errors. Please check below.</div>
		<ul class="nav nav-tabs tabCss">
			<li class="active"><a href="#defaults" data-toggle="tab">Default Settings</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="defaults">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle"><?php echo $account1Name ?> ID<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[brightpearlAccountId]" data-required="1" class="form-control brightpearlAccountId">
										<option value="">Select a <?php echo $account1Name ?> account</option>
										<?php
											foreach ($data['saveAccount'] as $saveAccount) {
												echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Currency</label>
								<div class="marginStyle">
									<input type="text" name="data[currencyCode]" value="<?php echo $data['accountinfo'][$row['brightpearlAccountId']]['configuration']['baseCurrencyCode']; ?>"  class="form-control" readonly="readonly"/>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Timezone</label>
								<div class="marginStyle">
									<input type="text" name="data[timezone]" value="<?php echo $data['accountinfo'][$row['brightpearlAccountId']]['configuration']['timeZone']; ?>"  class="form-control" readonly="readonly"/>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Payment Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Payment Method</label>
								<div class="marginStyle">
									<select name="data[defaultPaymentMethod]" class="form-control defaultPaymentMethod">
										<option value="">Select Default Payment Method</option>
										<?php
											foreach ($data['defaultPaymentMethod'][$row['brightpearlAccountId']] as $defaultPaymentMethod) {
												echo '<option value="'.$defaultPaymentMethod['id'].'">'.ucwords($defaultPaymentMethod['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Gift Card Payment Method</label>
								<div class="marginStyle">
									<select name="data[giftCardPayment]" class="form-control giftCardPayment">
										<option value="">Select Default Gift Card Payment Method</option>
										<?php
											foreach ($data['defaultPaymentMethod'][$row['brightpearlAccountId']] as $defaultPaymentMethod) {
												echo '<option value="'.$defaultPaymentMethod['id'].'">'.ucwords($defaultPaymentMethod['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Store Credit Payment Method</label>
								<div class="marginStyle">
									<select name="data[StoreCreditPayment]" class="form-control StoreCreditPayment">
										<option value="">Select Default Store Credit Payment Method</option>
										<?php
											foreach ($data['defaultPaymentMethod'][$row['brightpearlAccountId']] as $defaultPaymentMethod) {
												echo '<option value="'.$defaultPaymentMethod['id'].'">'.ucwords($defaultPaymentMethod['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<?php	if($this->globalConfig['enableamazonfee']){		?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Amazon Fees Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fees Journal Type</label>
								<div class="marginStyle">
									<input type="text" name="data[journalType]"  placeholder="Enter Amazon Fee Journal Type" class="form-control journalType" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fees Journal Account</label>
								<div class="marginStyle">
									<input type="text" name="data[journalAccount]"  class="form-control journalAccount" placeholder="Enter Amazon Fee Journal Nominal" />
									<!-- <span class="help-block">(Hint :- Use `,` for multiple Nominals. Space is not Allowed)</span>	-->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fees Journal Reference</label>
								<div class="marginStyle">
									<input type="text" name="data[details]" placeholder="Enter Amazon Fee Journal Reference" class="form-control details" />
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<?php	if($this->globalConfig['enableAmazonFeeOther']){	?>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fee (Other) Journal Type</label>
								<div class="marginStyle">
									<input type="text" name="data[amazonFeeOtherJournalType]" class="form-control amazonFeeOtherJournalType" placeholder="Enter Amazon Fee (Other) Journal Type" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fee (Other) Journal Account</label>
								<div class="marginStyle">
									<input type="text" name="data[amazonFeeOtherJournalAccount]"  class="form-control amazonFeeOtherJournalAccount" placeholder="Enter Amazon Fee (Other) Journal Nominal" />
									<!-- <span class="help-block">(Hint :- Use `,` for multiple Nominals. Space is not Allowed)</span>	-->
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					<?php	}	?>
				</fieldset>
				<?php	}	?>
				<?php	if($this->globalConfig['enableCOGSJournals']){	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">COGS Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">COGS Journal Type</label>
								<div class="marginStyle">
									<input type="text" name="data[FetchCOGSJournalType]"  style="text-transform: uppercase; "class="form-control FetchCOGSJournalType" placeholder="Enter COGS Journal Type" />
									<!-- <span class="help-block">(Hint :- Use `,` for multiple journal type. Space is not Allowed)</span>	-->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">COGS Journal Nominal</label>
								<div class="marginStyle">
									<input type="text" name="data[FetchCOGSJournalNominal]"  class="form-control FetchCOGSJournalNominal" placeholder="Enter COGS Journal Nominal" />
									<!--	<span class="help-block">(Hint :- Use `,` for multiple Nominals. Space is not Allowed)</span>	-->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">GRNI Journal Nominal</label>
								<div class="marginStyle">
									<input type="text" name="data[FetchGRNIJournalNominal]"  class="form-control FetchGRNIJournalNominal" placeholder="Enter GRNI Journal Nominal" />
									<!--	<span class="help-block">(Hint :- Use `,` for multiple Nominals. Space is not Allowed)</span>	-->
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<?php	}	?>
				<?php	if(($this->globalConfig['enableSalesOrder']) OR ($this->globalConfig['enableSalesCredit']) OR ($this->globalConfig['enablePurchaseOrder']) OR ($this->globalConfig['enablePurchaseCredit']) OR ($this->globalConfig['enableRefundReceipt'])){	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Order Fetch Settings</legend>
					</div>
					<div class="row">
						<?php	if($this->globalConfig['enableSalesOrder']){		?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Exclude Sales Order Status</label>
								<div class="marginStyle">
									<select name="data[fetchSalesOrderStatus][]" data-required="0" class="form-control fetchSalesOrderStatus" multiple="true">
										<option value="">Select Sales Order Status</option>
										<?php
											foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
												if($orderstatus['orderTypeCode'] == 'SO'){
													echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
												}
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<?php	}	?>
						<?php	if($this->globalConfig['enableSalesCredit']){		?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Exclude Sales Credit Status</label>
								<div class="marginStyle">
									<select name="data[fetchSalesCredit][]" data-required="0" class="form-control fetchSalesCredit"  multiple="true">
										<option value="">Select Sales Credit Status</option>
										<?php
											foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
												if($orderstatus['orderTypeCode'] == 'SC'){
													echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
												}
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<?php	}	?>
						<?php	if($this->globalConfig['enablePurchaseOrder']){		?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Exclude Purchase Order Status</label>
								<div class="marginStyle">
									<select name="data[fetchPurchaseStatus][]" data-required="0" class="form-control fetchPurchaseStatus" multiple="true">
										<option value="">Select Purchase Order Status</option>
										<?php
											foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
												if($orderstatus['orderTypeCode'] == 'PO'){
													echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
												}
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<?php	}	?>
						<?php	if($this->globalConfig['enablePurchaseCredit']){	?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Exclude Purchase Credit Status</label>
								<div class="marginStyle marginStyle2">
									<select name="data[fetchPurchaseCredit][]" data-required="0" class="form-control fetchPurchaseCredit" multiple="true">
										<option value="">Select Purchase Credit Status</option>
										<?php
											foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
												if($orderstatus['orderTypeCode'] == 'PC'){
													echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
												}
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<?php	}	?>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">StockAdjustments Exclude Warehouses</label>
								<div class="marginStyle">
									<select name="data[excludewarehouseStockadjustment][]" class="form-control excludewarehouseStockadjustment" multiple="true">
										<option value="">Select warehouse</option>
										<?php
											$warehouses	= reset($data['warehouse']);
											foreach ($warehouses as $warehouse) {
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<?php	if($this->globalConfig['enableRefundReceipt']){	?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Exclude Refund Receipt Status</label>
								<div class="marginStyle">
									<select name="data[fetchRefundReceiptStatus][]" data-required="0" class="form-control fetchRefundReceiptStatus" multiple="true">
										<option value="">Select Refund Receipt Status</option>
										<?php
											foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
												if($orderstatus['orderTypeCode'] == 'SC'){
													echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
												}
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<?php	}	?>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<?php	}	?>
				<?php	if(($this->globalConfig['enablePaymenttermsMapping'])){		?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Payment Terms Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Terms Custom Field</label>
								<div class="marginStyle">
									<input type="text" name="data[SalesTermsFiledName]" placeholder="Enter Sales Terms Custom Field" class="form-control SalesTermsFiledName" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Purchase Terms Custom Field</label>
								<div class="marginStyle">
									<input type="text" name="data[PurchaseTermsFiledName]" placeholder="Enter Purchase Terms Custom Field" class="form-control PurchaseTermsFiledName" />
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<?php	}	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Other Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">SO Location Custom Field</label>
								<div class="marginStyle">
									<input type="text" name="data[SOLocationCustomField]" placeholder="Enter SO Location Custom Field" class="form-control SOLocationCustomField" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Fetch Custom Field</label>
								<div class="marginStyle">
									<input type="text" name="data[SalesFetchCustomField]" placeholder="Enter Custom Field for SalesOrder Fetch" class="form-control SalesFetchCustomField" />
								</div>
							</div>
						</div>
						<?php	if(($this->globalConfig['enableAggregationAdvance'])){		?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">CustomFieldMapping Field Name</label>
								<div class="marginStyle">
									<input type="text" name="data[CustomFieldMappingFieldName]"  class="form-control CustomFieldMappingFieldName" placeholder="Enter CustomFieldMapping Field Name" />
								</div>
							</div>
						</div>
						<?php	}	?>
						<?php	if($this->globalConfig['enableAggregationOnAPIfields']){	?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">API Field For Consol</label>
								<div class="marginStyle marginStyle2">
									<select name="data[apiFieldForConsol]" class="form-control apiFieldForConsol">
										<option value="">Select Any Option</option>
										<?php
											foreach($account1APIField as $fields => $account1APIField){
												echo '<option value="'.$fields.'">'.ucwords($account1APIField['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<?php	}	?>
					</div>
					<div class="row">
						<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Custom Field For Alternate Tax On Products</label>
								<div class="marginStyle">
									<input type="text" name="data[customFieldForKidsTax]"  class="form-control customFieldForKidsTax" placeholder="Enter Custom Field Name" />
								</div>
							</div>
						</div>
						<?php	}	?>
					</div>
				</fieldset>
			</div>
		</div>
	</div> 
<?php
	}
?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
	$(".chosen-select").chosen({width: "100%"}); 
	jQuery(".actioneditbtn").on("click",function(){
		 $(".chosen-select").trigger("liszt:updated");
	});
</script>
<?php 
	}
	else{
?>
<div class="page-content-wrapper">
	<div class="page-content">
	<div class="portlet ">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-exclamation-triangle"></i>You Do not have permission to access this page.</div>
		</div>
	</div>
</div>
<?php
	}
?>