<?php	
	$account1Name	= $this->globalConfig['account1Name'];	
	$userLoginData	= $this->session->userdata('login_user_data');
	$accessRoles	= array('admin', 'developer', '1');
	if(in_array($userLoginData['role'], $accessRoles)){
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Account Settings</span><i class="fa fa-circle"></i></li>
				<li><span>QBO Settings</span><i class="fa fa-circle"></i></li>
				<li><span>Accounts</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-square-o"></i>QBO Accounts</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Account</span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="table-responsive">
						<table class="table table-hover text-centered actiontable">
							<thead>
								<tr>
									<th width="5%">#</th>  
									<?php
										if($data['type'] == 'account2'){
											echo '<th width="15%">'.$this->globalConfig['account1Name'].' Name</th>';
										}
									?>
									<th width="10%">Company Name</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<?php	if($data['type'] == 'account2'){	?>
									<td><span class="value" data-value="account1Id"></span></td>
									<?php	}	?>
									<td><span class="value" data-value="name"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/account/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	foreach($data['data'] as $key => $row){	?>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td> 
									<?php	if($data['type'] == 'account2'){	?>
									<td>
										<span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span>
									</td>
									<?php	}	?>
									<td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick= 'editAction(<?php echo json_encode($row);?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a class="btn btn-icon-only" target="_blank" href="<?php echo base_url('webhooks/refreshToken/'.$row['id']);?>"  title=" Generate/Refresh Token"><i class="fa fa-refresh" title=" Generate/Refresh Token" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/account/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><b>QBO Account Settings</b></h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('account/'.$data['type'].'/account/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>You have some form errors. Please check below.
								</div>
								<?php	if($data['type'] == 'account2'){	?>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> ID
										<span class="required" aria-required="true"><b> * </b></span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]"  data-required="1" class="form-control account1Id">
											<option value="">Select A BrightPearl Account ID</option>
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<?php	}	?>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Company Name<span class="required" aria-required="true"><b> * </b></span></label>
									<div class="col-md-7"><input name="data[name]" data-required="1" class="form-control name" type="text" placeholder="Enter QBO Company Name" /></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Account Type<span class="required" aria-required="true"><b> * </b></span></label>
									<div class="col-md-7">
										<select name="data[mode]"  data-required="1" class="form-control mode">
											<option value="">Select Account Type</option>
											<option value="sandbox">Sandbox</option>
											<option value="production">Production</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Company ID<span class="required" aria-required="true"><b> * </b></span></label>
									<div class="col-md-7"><input name="data[companyId]" data-required="1" class="form-control companyId" type="text" placeholder="Enter QBO Company ID" /> </div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Client ID<span class="required" aria-required="true"><b> * </b></span></label>
									<div class="col-md-7"><input name="data[clientId]" data-required="1" class="form-control clientId" type="password" placeholder="Enter Client ID" /></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Client Secret<span class="required" aria-required="true"><b> * </b></span></label>
									<div class="col-md-7"><input name="data[clientSecret]" data-required="1" class="form-control clientSecret" type="password" placeholder="Enter Client Secret" /></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Redirect URL<span class="required" aria-required="true"><b> * </b></span></label>
									<div class="col-md-7"><input name="data[redirectUrl]" class="form-control redirectUrl" type="text" data-required="1" placeholder="Enter Redirect URL For Refresh Token" ></div>
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	}
	else{
?>
<div class="page-content-wrapper">
	<div class="page-content">
	<div class="portlet ">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-exclamation-triangle"></i>You Do not have permission to access this page.</div>
		</div>
	</div>
</div>
<?php
	}
?>