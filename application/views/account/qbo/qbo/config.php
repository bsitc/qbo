<?php
//qbo
	$allCurrencyList		= array('AFA','ALL','DZD','AOR','ARS','AMD','AWG','AUD','AZN','BSD','BHD','BDT','BBD','BYN','BZD','BMD','BTN','BOB','BWP','BRL','GBP','BND','BGN','BIF','KHR','CAD','CVE','KYD','XOF','XAF','XPF','CLP','CNY','COP','KMF','CDF','CRC','HRK','CUP','CZK','DKK','DJF','DOP','XCD','EGP','SVC','ERN','EEK','ETB','EUR','FKP','FJD','GMD','GEL','GHS','GIP','XAU','XFO','GTQ','GNF','GYD','HTG','HNL','HKD','HUF','ISK','XDR','INR','IDR','IRR','IQD','ILS','JMD','JPY','JOD','KZT','KES','KWD','KGS','LAK','LVL','LBP','LSL','LRD','LYD','LTL','MOP','MKD','MGA','MWK','MYR','MVR','MRO','MUR','MXN','MDL','MNT','MAD','MZN','MMK','NAD','NPR','ANG','NZD','NIO','NGN','KPW','NOK','OMR','PKR','XPD','PAB','PGK','PYG','PEN','PHP','XPT','PLN','QAR','RON','RUB','RWF','SHP','WST','STD','SAR','RSD','SCR','SLL','XAG','SGD','SBD','SOS','ZAR','KRW','LKR','SDG','SRD','SZL','SEK','CHF','SYP','TWD','TJS','TZS','THB','TOP','TTD','TND','TRY','TMT','AED','UGX','XFU','UAH','UYU','USD','UZS','VUV','VEF','VND','YER','ZMK','ZWL');
	$account1Fieldconfigso	= $data['account1Fieldconfigso'];
	$account1Fieldconfigpo	= $data['account1Fieldconfigpo'];
	$userLoginData			= $this->session->userdata('login_user_data');
	$accessRoles			= array('admin', 'developer', '1');
	if(in_array($userLoginData['role'], $accessRoles)){
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
                <li><span>Account Settings</span><i class="fa fa-circle"></i></li>
                <li><span><?php echo $this->globalConfig['account2Name'];?> Settings</span><i class="fa fa-circle"></i></li>
                <li><span>Default Configuration</span></li>
            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo $this->globalConfig['account2Name'];?> Configuration</div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs">Add New Configuration</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="25%"><?php echo $this->globalConfig['account2Name'];?> ID</th>
                                    <th width="25%">Account ID</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="qboAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="qboAccountId"><?php echo $row['qboAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php	}	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-xl">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php echo $this->globalConfig['account2Name'];?> Configuration</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>
							<input type="hidden" name="data[id]" class="id" />
						</form>                         
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction sbold btnExtra">Save</button>
						<button type="button" class="btn yellow btn-outline sbold btnExtra" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>
<div class="confighml">
<?php   
    $data['data']	= ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {
		$account1Id	= @$data['saveAccount'][$row['qboAccountId']]['account1Id']; 
?>
	<div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button> You have some form errors. Please check below.
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#defaults" data-toggle="tab">Default Settings</a></li>
			<?php	if(($this->globalConfig['enableInventoryManagement']) OR ($this->globalConfig['enableInventoryTransfer']) OR ($this->globalConfig['enableSingleCompanyStocktx'])){	?>
			<li><a href="#inventorySettings" data-toggle="tab">Inventory Settings</a></li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableCOGSJournals']){	?>
			<li><a href="#defaultCOGSAccounts" data-toggle="tab">COGS Settings</a></li>
			<?php	}	?>
			<li><a href="#defaultsItemsNominals" data-toggle="tab">Items/Nominal Settings</a></li>
			<?php	if($this->globalConfig['enableamazonfee']){	?>
			<li><a href="#amazonFees" data-toggle="tab">Amazon Settings</a></li>
			<?php	}	?>
			<li><a href="#otherSettings" data-toggle="tab">Other Settings</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="defaults">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle"><?php echo $this->globalConfig['account2Name'];?> ID<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[qboAccountId]" data-required="1" class="form-control QBOAccountId">
										<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> account</option>
										<?php
											foreach ($data['saveAccount'] as $saveAccount) {
												echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Currency</label>
								<div class="marginStyle">
									<input type="text" name="data[defaultCurrrency]" value="<?php echo $data['Preferences'][$row['qboAccountId']]['CurrencyPrefs']; ?>" class="form-control" readonly="readonly" />
									
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Account Type</label>
								<div class="marginStyle">
									<select name="data[accountType]"  class="form-control accountType" >
										<option value="">Select Account Type</option>
										<option value="us">US</option>
										<option value="uk">UK</option>
										<option value="ca">CA</option>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Accounts</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Deposit To Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[DepositToAccountRef]" data-required="1" class="form-control DepositToAccountRef ">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Income Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[IncomeAccountRef]" data-required="1" class="form-control IncomeAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Asset Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[AssetAccountRef]" data-required="1" class="form-control AssetAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Expense Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle marginStyle2">
									<select name="data[ExpenseAccountRef]" data-required="1" class="form-control ExpenseAccountRef">
										<option value="">Select Account</option>
										<?php
										foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
											echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
										}
										?>
									</select> 
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Pay Type</label>
								<div class="marginStyle">
									<select name="data[PayType]" data-required="0" class="form-control PayType">
										<option value="">Select PayType</option>
										<?php
											foreach ($data['PaymentMethodRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['name'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Discount Account</label>
								<div class="marginStyle">
									<select name="data[DiscountAccountRef]" data-required="0" class="form-control DiscountAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default SC Payment Method</label>
								<div class="marginStyle">
									<select name="data[DefaultSCPaymentType]"  class="form-control DefaultSCPaymentType">
										<option value="">Select Default Payment Method For SC</option>
										<?php
											foreach ($data['PaymentMethodRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['name'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Stock Adjustment Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Stock Adjustment Account Ref</label>
								<div class="marginStyle">
									<select name="data[accRefForStockAdjustment]" class="form-control accRefForStockAdjustment">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?> 
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Supplier ID For Stock Adjustments Bill</label>
								<div class="marginStyle"><input name="data[supplierIdForBill]" class="form-control supplierIdForBill" type="text" placeholder="Enter QBO Supplier ID For Stock Adjustments" /></div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Tax Codes</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order Tax Code</label>
								<div class="marginStyle">
									<select name="data[TaxCode]" data-required="0" class="form-control TaxCode">
										<option value="">Select Tax Code</option>
										<?php
											foreach ($data['getAllTax'][$row['qboAccountId']] as $getAllTax) {
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order No Tax Code</label>
								<div class="marginStyle">
									<select name="data[salesNoTaxCode]" data-required="0" class="form-control salesNoTaxCode">
										<option value="">Select Taxcode</option>
										<?php
											foreach ($data['getAllTax'][$row['qboAccountId']] as $getAllTax) {
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Line Item Tax Code</label>
								<div class="marginStyle">
									<select name="data[orderLineTaxCode]" class="form-control orderLineTaxCode">
										<option value="">Select Taxcode</option>
										<?php
											foreach ($data['getAllTax'][$row['qboAccountId']] as $getAllTax) {
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Send Tax As Line Item</label>
								<div class="marginStyle">
									<select name="data[SendTaxAsLineItem]" data-required="0" class="form-control SendTaxAsLineItem">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Tax Item ID</label>
								<div class="marginStyle">
									<input name="data[TaxLineItemCode]"  class="form-control TaxLineItemCode" type="text" placeholder="Enter QBO Default Tax Item ID" />
								</div> 
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Price Lists</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Product Purchase Price List</label>
								<div class="marginStyle">
									<select name="data[defaultPriceListForProduct]" data-required="0" class="form-control defaultPriceListForProduct">
										<option value="">Select Price List</option>
										<?php
											foreach ($data['pricelist'][$account1Id] as $pricelist) {
												echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
											}
										?>
									</select>  
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Product Sell Price List</label>
								<div class="marginStyle">
									<select name="data[defaultPriceListForRetail]" data-required="0" class="form-control defaultPriceListForRetail">
										<option value="">Select Price List</option>
										<?php
											foreach ($data['pricelist'][$account1Id] as $pricelist) {
												echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
			</div>
			
			<div class="tab-pane" id="inventorySettings">
				<?php	if($this->globalConfig['enableInventoryManagement']){	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Inventory Management</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Inventory Management Enabled</label>
								<div class="marginStyle">
									<select name="data[InventoryManagementEnabled]" data-required="0" class="form-control InventoryManagementEnabled">
										<option value="1">No</option>
										<option value="0">Yes</option>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">IM Product Purchase Nominal Code</label>
								<div class="marginStyle">
									<select name="data[InventoryManagementProductPurchaseNominalCode]" data-required="0" class="form-control InventoryManagementProductPurchaseNominalCode">
										<option value="">Select Nominal</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">IM Product Sell Nominal Code</label>
								<div class="marginStyle">
									<select name="data[InventoryManagementProductSellNominalCode]" data-required="0" class="form-control InventoryManagementProductSellNominalCode">
										<option value="">Select Nominal</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Disable SKU Details On Documents</label>
								<div class="marginStyle marginStyle2">
									<select name="data[disableSkuDetails]" class="form-control disableSkuDetails">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Bundle Component Suppression</label>
								<div class="marginStyle">
									<select name="data[BundleSuppression]" data-required="0" class="form-control BundleSuppression">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select> 
								</div> 
							</div>
						</div>
					</div>
				</fieldset>
				<?php	}	?>
				<?php 	if($this->globalConfig['enableInventoryTransfer']){		?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Inventory Management</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Inter Company Supplier</label>
								<div class="marginStyle"><input name="data[InterCoSupplier]" class="form-control InterCoSupplier" type="text" placeholder="Enter QBO Inventory Transfer Supplier ID" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Inter Company Customer</label>
								<div class="marginStyle"><input name="data[InterCoCustomer]" class="form-control InterCoCustomer" type="text" placeholder="Enter QBO Inventory Transfer Customer ID" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Inventory Transfer SKU</label>
								<div class="marginStyle"><input name="data[InventoryTransferSKU]" class="form-control InventoryTransferSKU" type="text" placeholder="Enter QBO Inventory Transfer Item ID" /></div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<?php	}	?>
				<?php 	if($this->globalConfig['enableSingleCompanyStocktx']){		?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Stock Transfers as Journal</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Warehouse For StockTransfer as Journal</label>
								<div class="marginStyle">
									<select name="data[warehouseForStockTx][]" class="form-control warehouseForStockTx" multiple="true">
										<option value="">Select Warehouse</option>
										<?php
											$warehouses	= reset($data['warehouse']);
											foreach ($warehouses as $warehouse) {
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-9"></div>
					</div>
				</fieldset>
				<?php	}	?>
			</div>
		
			<div class="tab-pane" id="defaultCOGSAccounts">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default COGS Accounts</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order (GO) - Credit Account</label>
								<div class="marginStyle">
									<select name="data[COGSCreditNominalSO]"  class="form-control COGSCreditNominalSO">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order (GO) - Debit Account</label>
								<div class="marginStyle">
									<select name="data[COGSDebitNominalSO]"  class="form-control COGSDebitNominalSO">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Credit (SG) - Credit Account</label>
								<div class="marginStyle">
									<select name="data[COGSCreditNominalSC]"  class="form-control COGSCreditNominalSC">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Credit (SG) - Debit Account</label>
								<div class="marginStyle">
									<select name="data[COGSDebitNominalSC]"  class="form-control COGSDebitNominalSC">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Credit (PG) - Credit Account</label>
								<div class="marginStyle">
									<select name="data[COGSCreditNominalSCPG]"  class="form-control COGSCreditNominalSCPG">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Sales Credit (PG) - Debit Account</label>
								<div class="marginStyle marginStyle2">
									<select name="data[COGSDebitNominalSCPG]"  class="form-control COGSDebitNominalSCPG">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle"> Purchase Order (GO) - Default Account </label>
								<div class="marginStyle">
									<select name="data[DefaultCOGSforPurchase]"  class="form-control DefaultCOGSforPurchase">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-9"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">COGS Journal Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Send Customer/Vendor ID On Journal</label>
								<div class="marginStyle">
									<select name="data[entityIdOnJournal]" class="form-control entityIdOnJournal">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		
			<div class="tab-pane" id="amazonFees">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Amazon Fees</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fees Channel</label>
								<div class="marginStyle">
									<select name="data[AmazonFeesChannels][]"  class="form-control AmazonFeesChannels"  multiple="true">
										<option value="">Select Channel</option>
										<?php
											foreach($data['channel'][$account1Id] as $channel){
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select>  
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fees Item</label>
								<div class="marginStyle"><input name="data[AmazonFeeItem]"  class="form-control AmazonFeeItem" type="text" placeholder="Enter AmazonFees Item ID" /></div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
			</div>
		
			<div class="tab-pane" id="defaultsItemsNominals">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Items IDs/Identify Nominal Codes</legend>
					</div>
					<div class="row">
						<div class="col-md-2 itemHeading"><label class="control-label">Item</label></div>
						<div class="col-md-2 itemHeading"><label class="control-label">QBO Item ID</label></div>
						<div class="col-md-2 itemHeading"><label class="control-label">Brightpearl Nominal</label></div>
					</div>
					<hr />
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Shipping</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[shippingItem]" class="form-control shippingItem" type="text" placeholder="Enter Shipping Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForShipping]" class="form-control nominalCodeForShipping" type="text" placeholder="Enter Shipping Identify Nominal Code" /></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Gift Card</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[giftCardItem]" class="form-control giftCardItem" type="text" placeholder="Enter Gift Card Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForGiftCard]" class="form-control nominalCodeForGiftCard" type="text" placeholder="Enter Gift Card Identify Nominal Code" /></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Coupon</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[couponItem]" class="form-control couponItem" type="text" placeholder="Enter Coupon Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForDiscount]" class="form-control nominalCodeForDiscount" type="text" placeholder="Enter Coupon Identify Nominal Code" /></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Landed Cost</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[landedCostItem]" class="form-control landedCostItem" type="text" placeholder="Enter Landed Cost Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForLandedCost]" class="form-control nominalCodeForLandedCost" type="text" placeholder="Enter Landed Cost Identify Nominal Code" /></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Generic SKU</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[genericSku]" class="form-control genericSku" type="text" placeholder="Enter Generic SKU Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2"></div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Tracked Generic SKU</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[trackedGenericItem]" class="form-control trackedGenericItem" type="text" placeholder="Enter Tracked Generic SKU Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2"></div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Discount</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[discountItem]" class="form-control discountItem" type="text" placeholder="Enter Discount Item ID" /></div>
							</div>
						</div>
						<div class="col-md-2"></div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Round Off</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[roundOffItem]" class="form-control roundOffItem" type="text" placeholder="Enter Round Off Item ID" /></div>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Store Credit</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[StoreCreditItem]" class="form-control StoreCreditItem" type="text" placeholder="Enter Store Credit Item ID" /></div>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">PUR-CREDIT</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[PurchaseCreditItem]" class="form-control PurchaseCreditItem" type="text" placeholder="Enter PUR-CREDIT Item ID" /></div>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
				</fieldset>
			</div>
		
			<div class="tab-pane" id="otherSettings">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Reference Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Use As Doc Number On SO/SC</label>
								<div class="marginStyle">
									<select name="data[UseAsDocNumbersosc]" data-required="0" class="form-control UseAsDocNumbersosc">
										<option value="">Select</option>
										<?php
											foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Use As Doc Number On PO/PC</label>
								<div class="marginStyle">
									<select name="data[UseAsDocNumberpopc]" data-required="0" class="form-control UseAsDocNumberpopc">
										<option value="">Select</option>
										<?php
											foreach($account1Fieldconfigpo as $fields => $account1Fieldconfigpos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigpos['name']).'</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Use As Reference On SO Payments</label>
								<div class="marginStyle">
									<select name="data[useRefOnPayments]" data-required="0" class="form-control useRefOnPayments">
										<option value="">Select</option>
										<option value="Reference">Reference (On Payment)</option>
										<option value="Transactionref">Transaction Ref (On Payment)</option>
										<?php
											foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Use As Memo On SO/SC Payments</label>
								<div class="marginStyle marginStyle2">
									<select name="data[memoOnSOSCPayments]" data-required="0" class="form-control memoOnSOSCPayments">
										<option value="">Select</option>
										<option value="Reference">Reference (On Payment)</option>
										<option value="Transactionref">Transaction Ref (On Payment)</option>
										<?php
											foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Discount Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Send Net Price (After Discount Percentage)</label>
								<div class="marginStyle">
									<select name="data[sendNetPriceExcludeDiscount]" class="form-control sendNetPriceExcludeDiscount">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-9"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Purchase Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Enable Purchase Consol (Batch Invoice)</label>
								<div class="marginStyle">
									<select name="data[enablePurchaseConsol]"  class="form-control enablePurchaseConsol">
										<option value="0">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Purchase Consol On</label>
								<div class="marginStyle">
									<select name="data[purchaseConsolBasedON]" class="form-control purchaseConsolBasedON">
										<option value="">Select</option>
										<option value="supplierID">Supplier ID</option>
										<option value="suppplierAccountCode">Supplier Account Code</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<?php	if($this->globalConfig['enableSendSCasPO']){	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Sales Credit Send as Bill Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">SC Bill SKU</label>
								<div class="marginStyle"><input name="data[BillIdentifySKU]"  class="form-control BillIdentifySKU" type="text" placeholder="Enter SC Bill Identifier SKU" /> </div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">SC Bill Default Vendor ID</label>
								<div class="marginStyle"><input name="data[SalesCreditBillVendorRef]"  class="form-control SalesCreditBillVendorRef" type="text" placeholder="Enter SC Bill Default Vendor ID" /> </div> 
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">SC Bill Account Ref</label>
								<div class="marginStyle">
									<select name="data[SalesCreditBillAccountRef]" data-required="0" class="form-control SalesCreditBillAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
											}
										?>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<?php	}	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Channel & Warehouse</legend>
					</div>
					<?php	if($this->globalConfig['enableSalesOrder']){	?>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Channel For SO</label>
								<div class="marginStyle">
									<select name="data[channelIds][]"  class="form-control channelIds"  multiple="true">
										<option value="">Select Channel</option>
										<?php
											$channels	= reset($data['channel']);
											foreach ($channels as $channel) {
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Warehouse For SO</label>
								<div class="marginStyle">
									<select name="data[warehouses][]" class="form-control warehouses" multiple="true">
										<option value="">Select Warehouse</option>
										<?php
											$warehouses	= reset($data['warehouse']);
											foreach ($warehouses as $warehouse) {
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					<?php	}	?>
					<?php	if($this->globalConfig['enableSalesCredit']){	?>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Channel For SC</label>
								<div class="marginStyle">
									<select name="data[channelIdsSC][]"  class="form-control channelIdsSC"  multiple="true">
										<option value="">Select Channel</option>
										<?php
											$channels	= reset($data['channel']);
											foreach ($channels as $channel) {
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Warehouse For SC</label>
								<div class="marginStyle">
									<select name="data[warehousesSC][]" class="form-control warehousesSC" multiple="true">
										<option value="">Select Warehouse</option>
										<?php
											$warehouses	= reset($data['warehouse']);
											foreach ($warehouses as $warehouse) {
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					<?php	}	?>
					<?php	if(($this->globalConfig['enablePurchaseOrder']) OR ($this->globalConfig['enablePurchaseCredit'])){	?>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Channel For PO/PC</label>
								<div class="marginStyle">
									<select name="data[channelIdspopc][]"  class="form-control channelIdspopc"  multiple="true">
										<option value="">Select Channel</option>
										<?php
											$channels	= reset($data['channel']);
											foreach ($channels as $channel) {
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Warehouse For PO/PC</label>
								<div class="marginStyle">
									<select name="data[warehousespopc][]" class="form-control warehousespopc" multiple="true">
										<option value="">Select Warehouse</option>
										<?php
											$warehouses	= reset($data['warehouse']);
											foreach ($warehouses as $warehouse) {
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					<?php	}	?>
					<?php	if(($this->globalConfig['enableRefundReceipt'])){	?>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Channel For Refund Receipt</label>
								<div class="marginStyle">
									<select name="data[channelIds_refund_sales_receipt][]"  class="form-control channelIds_refund_sales_receipt"  multiple="true">
										<option value="">Select Channel</option>
										<?php
											$channels	= reset($data['channel']);
											foreach ($channels as $channel) {
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Warehouse For Refund Receipt</label>
								<div class="marginStyle">
									<select name="data[warehouses_refund_sales_receipt][]" class="form-control warehouses_refund_sales_receipt" multiple="true">
										<option value="">Select Warehouse</option>
										<?php
											$warehouses	= reset($data['warehouse']);
											foreach ($warehouses as $warehouse) {
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
					<?php	}	?>
				</fieldset>
			</div>
		</div>
		
	</div> 
<?php
	}
?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
	$(".chosen-select").chosen({width: "100%"}); 
</script>
<?php 
	}
	else{
?>
<div class="page-content-wrapper">
	<div class="page-content">
	<div class="portlet ">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-exclamation-triangle"></i>You Do not have permission to access this page.</div>
		</div>
	</div>
</div>
<?php
	}
?>