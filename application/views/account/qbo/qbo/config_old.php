<?php
//qbo
	$account1Fieldconfigso	= $data['account1Fieldconfigso'];
	$account1Fieldconfigpo	= $data['account1Fieldconfigpo'];
?> 
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
                <li><span>Account Settings</span><i class="fa fa-circle"></i></li>
                <li><span><?php echo $this->globalConfig['account2Name'];?> Settings</span><i class="fa fa-circle"></i></li>
                <li><span>Default Configuration</span></li>
            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo $this->globalConfig['account2Name'];?> Configuration</div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs">Add New Configuration</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="25%"><?php echo $this->globalConfig['account2Name'];?> ID</th>
                                    <th width="25%">Account ID</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="qboAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="qboAccountId"><?php echo $row['qboAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php	}	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php echo $this->globalConfig['account2Name'];?> Configuration</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>
							<input type="hidden" name="data[id]" class="id" />
						</form>                         
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>
<div class="confighml">
<?php   
    $data['data']	= ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {
		$account1Id	= @$data['saveAccount'][$row['qboAccountId']]['account1Id']; 
?>
	<div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button> You have some form errors. Please check below.
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> ID<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-7">
				<select name="data[qboAccountId]" data-required="1" class="form-control QBOAccountId">
					<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> account</option>
					<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Deposit To Account Ref<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-7">
				<select name="data[DepositToAccountRef]" data-required="1" class="form-control DepositToAccountRef ">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Income Account Ref<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-7">
				<select name="data[IncomeAccountRef]" data-required="1" class="form-control IncomeAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Asset Account Ref<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-7">
				<select name="data[AssetAccountRef]" data-required="1" class="form-control AssetAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Expense Account Ref<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-7">
				<select name="data[ExpenseAccountRef]" data-required="1" class="form-control ExpenseAccountRef">
					<option value="">Select Account</option>
					<?php
					foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
						echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
					}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> PayType</label>
			<div class="col-md-7">
				<select name="data[PayType]" data-required="0" class="form-control PayType">
					<option value="">Select PayType</option>
					<?php
						foreach ($data['PaymentMethodRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['name'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Discount Account Ref</label>
			<div class="col-md-7">
				<select name="data[DiscountAccountRef]" data-required="0" class="form-control DiscountAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Purchase Cost For Product</label>
			<div class="col-md-7">
				<select name="data[defaultPriceListForProduct]" data-required="0" class="form-control defaultPriceListForProduct">
					<option value="">Select Purchase Cost</option>
					<?php
						foreach ($data['pricelist'][$account1Id] as $pricelist) {
							echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
						}
					?>
				</select>  
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Sales Price For Products</label>
			<div class="col-md-7">
				<select name="data[defaultPriceListForRetail]" data-required="0" class="form-control defaultPriceListForRetail">
					<option value="">Select Sales Price</option>
					<?php
						foreach ($data['pricelist'][$account1Id] as $pricelist) {
							echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Order TaxCode</label>
			<div class="col-md-7">
				<select name="data[TaxCode]" data-required="0" class="form-control TaxCode">
					<option value="">Select TaxCode</option>
					<?php
						foreach ($data['getAllTax'][$row['qboAccountId']] as $getAllTax) {
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Order No TaxCode</label>
			<div class="col-md-7">
				<select name="data[salesNoTaxCode]" data-required="0" class="form-control salesNoTaxCode">
					<option value="">Select Taxcode</option>
					<?php
						foreach ($data['getAllTax'][$row['qboAccountId']] as $getAllTax) {
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Line Item TaxCode</label>
			<div class="col-md-7">
				<select name="data[orderLineTaxCode]" class="form-control orderLineTaxCode">
					<option value="">Select Taxcode</option>
					<?php
						foreach ($data['getAllTax'][$row['qboAccountId']] as $getAllTax) {
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Stock Adjustment Account Ref</label>
			<div class="col-md-7">
				<select name="data[accRefForStockAdjustment]" class="form-control accRefForStockAdjustment">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?> 
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Supplier ID For Stock Adjustments Bill</label>
			<div class="col-md-7"><input name="data[supplierIdForBill]" class="form-control supplierIdForBill" type="text" placeholder="Enter QBO Supplier ID For Stock Adjustments" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Generic Item ID</label>
			<div class="col-md-7"><input name="data[genericSku]" class="form-control genericSku" type="text" placeholder="Enter QBO Generic Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Shipping Item ID</label>
			<div class="col-md-7"><input name="data[shippingItem]" class="form-control shippingItem" type="text" placeholder="Enter QBO Shipping Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Discount Item ID</label>
			<div class="col-md-7"><input name="data[discountItem]" class="form-control discountItem" type="text" placeholder="Enter QBO Discount Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Gift Card Item ID</label>
			<div class="col-md-7"><input name="data[giftCardItem]"  class="form-control giftCardItem" type="text" placeholder="Enter QBO Giftcard Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Store Credit Item ID</label>
			<div class="col-md-7"><input name="data[StoreCreditItem]"  class="form-control StoreCreditItem" type="text" placeholder="Enter QBO StoreCredit Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Coupon Item ID</label>
			<div class="col-md-7"><input name="data[couponItem]"  class="form-control couponItem" type="text" placeholder="Enter QBO Coupon Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">PUR-CREDIT Item ID</label>
			<div class="col-md-7"><input name="data[PurchaseCreditItem]"  class="form-control PurchaseCreditItem" type="text" placeholder="Enter QBO Purchase Credit Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">RoundOff Item ID</label>
			<div class="col-md-7"><input name="data[roundOffItem]"  class="form-control roundOffItem" type="text" placeholder="Enter QBO RoundOff Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Sales Tax Item ID</label>
			<div class="col-md-7"><input name="data[TaxLineItemCode]"  class="form-control TaxLineItemCode" type="text" placeholder="Enter QBO Default Tax Item ID" /> </div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">LandedCost Item ID</label>
			<div class="col-md-7"><input name="data[landedCostItem]"  class="form-control landedCostItem" type="text" placeholder="Enter QBO LandedCost Item ID" /> </div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">LandedCost Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForLandedCost]"  class="form-control nominalCodeForLandedCost" type="text" placeholder="Enter LandedCost Idetify Nominal Code" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Shipping Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForShipping]"  class="form-control nominalCodeForShipping" type="text" placeholder="Enter Shipping Idetify Nominal Code" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Gift Card Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForGiftCard]"  class="form-control nominalCodeForGiftCard" type="text" placeholder="Enter Giftcard Idetify Nominal Code" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Coupon Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForDiscount]"  class="form-control nominalCodeForDiscount" type="text" placeholder="Enter Coupon Idetify Nominal Code" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Account Type</label>
			<div class="col-md-7">
				<select name="data[accountType]"  class="form-control accountType" >
					<option value="">Select Account Type</option>
					<option value="us">US</option>
					<option value="uk">UK</option>
					<option value="ca">CA</option>
				</select> 
			</div>
		</div>
		<?php	if($this->globalConfig['enableamazonfee']){		?>
		<div class="form-group">
			<label class="control-label col-md-4">Amazon Fee Channel</label>
			<div class="col-md-7">
				<select name="data[AmazonFeesChannels][]"  class="form-control AmazonFeesChannels"  multiple="true">
					<option value="">Select Channel</option>
					<?php
						foreach($data['channel'][$account1Id] as $channel){
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select>  
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">AmazonFees Item</label>
			<div class="col-md-7"><input name="data[AmazonFeeItem]"  class="form-control AmazonFeeItem" type="text" placeholder="Enter AmazonFees Item ID" /></div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Amazon Fee PaymentMethodRef</label>
			<div class="col-md-7">
				<select name="data[amazonfeePaymentMethodRef]"  class="form-control amazonfeePaymentMethodRef">
					<option value="">Select Payment Method</option>
					<?php
						foreach ($data['PaymentMethodRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Amazon Fee DepositToAccountRef</label>
			<div class="col-md-7">
				<select name="data[amazonfeeDepositToAccountRef]"  class="form-control amazonfeeDepositToAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Amazon Fee AccountRef</label>
			<div class="col-md-7">
				<select name="data[amazonfeeAccountRef]"  class="form-control amazonfeeAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Amazon Withheld Tax Fee</label>
			<div class="col-md-7">
				<select name="data[amazonWithhelfeeAccountRef]"  class="form-control amazonWithhelfeeAccountRef">
					<option value="">Select Account</option>
					<?php
					foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
						echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
					}
					?>
				</select> 
			</div>
		</div>
		<?php	}	?>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Supplier prepayment AccountRef</label>
			<div class="col-md-7">
				<select name="data[prepaymentAccount]" data-required="0" class="form-control prepaymentAccount">
					<option value="">Select Account</option>
					<?php
						foreach ($data['prepaymentAccount'][$account1Id] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Customer deposit AccountRef</label>
			<div class="col-md-7">
				<select name="data[depositAccRef]" data-required="0" class="form-control depositAccRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['prepaymentAccount'][$account1Id] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<?php	if($this->globalConfig['enableDDPCharges']){	?>
		<div class="form-group">
			<label class="control-label col-md-4">Shopify Channel</label>
			<div class="col-md-7">
				<select name="data[shopifyChannel]" class="form-control shopifyChannel">
					<option value="">Select Channel</option>
					<?php
						$channels	= reset($data['channel']);
						foreach ($channels as $channel) {
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">DDP charges AccountRef</label>
			<div class="col-md-7">
				<select name="data[ddpChargesAccountRef]"  class="form-control ddpChargesAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">US Country Code</label>
			<div class="col-md-7"><input class="form-control nonUSCountry" name="data[nonUSCountry]" type="text" placeholder="Enter Country Code" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">DDP Charge Item ID</label>
			<div class="col-md-7"><input name="data[ddpChargeItem]"  class="form-control ddpChargeItem" type="text" placeholder="Enter QBO DDP Charge Item ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Customer Deposit Item ID</label>
			<div class="col-md-7"><input name="data[deositItemId]"  class="form-control deositItemId" type="text" placeholder="Enter QBO Customer Deposit Item ID" /></div>
		</div>
		<?php	}	?>
		<?php	if($this->globalConfig['enableInventoryManagement']){	?>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Management Enabled</label>
			<div class="col-md-7">
				<select name="data[InventoryManagementEnabled]" data-required="0" class="form-control InventoryManagementEnabled">
					<option value="1">No</option>
					<option value="0">Yes</option>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">IM Product Purchase Nominal Code</label>
			<div class="col-md-7">
				<select name="data[InventoryManagementProductPurchaseNominalCode]" data-required="0" class="form-control InventoryManagementProductPurchaseNominalCode">
					<option value="">Select Nominal</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">IM Product Sell Nominal Code</label>
			<div class="col-md-7">
				<select name="data[InventoryManagementProductSellNominalCode]" data-required="0" class="form-control InventoryManagementProductSellNominalCode">
					<option value="">Select Nominal</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<?php	}	?>
		<div class="form-group">
			<label class="control-label col-md-4">Send Tax As Line Item</label>
			<div class="col-md-7">
				<select name="data[SendTaxAsLineItem]" data-required="0" class="form-control SendTaxAsLineItem">
					<option value="">Select</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Send NetPrice (After Discount Percentage)</label>
			<div class="col-md-7">
				<select name="data[sendNetPriceExcludeDiscount]" class="form-control sendNetPriceExcludeDiscount">
					<option value="">Select</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Currency</label>
			<div class="col-md-7"><input name="data[defaultCurrrency]"  class="form-control defaultCurrrency" type="text" placeholder="Enter QBO Default Currency" /></div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><span aria-required="true"></span> Use As DocNumber On SO/SC</label>
			<div class="col-md-7">
				<select name="data[UseAsDocNumbersosc]" data-required="0" class="form-control UseAsDocNumbersosc">
					<option value="">Select</option>
					<?php
						foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
							echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><span aria-required="true"></span> Use As DocNumber On PO/PC</label>
			<div class="col-md-7">
				<select name="data[UseAsDocNumberpopc]" data-required="0" class="form-control UseAsDocNumberpopc">
					<option value="">Select</option>
					<?php
						foreach($account1Fieldconfigpo as $fields => $account1Fieldconfigpos){
							echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigpos['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><span aria-required="true"></span> Use As Ref On SO Payments</label>
			<div class="col-md-7">
				<select name="data[useRefOnPayments]" data-required="0" class="form-control useRefOnPayments">
					<option value="">Select</option>
					<option value="Reference">Reference (On Payment)</option>
					<option value="Transactionref">Transaction Ref (On Payment)</option>
					<?php
						foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
							echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><span aria-required="true"></span> Default SC Payment Method</label>
			<div class="col-md-7">
				<select name="data[DefaultSCPaymentType]"  class="form-control DefaultSCPaymentType">
					<option value="">Select Default Payment Method For SC</option>
					<?php
						foreach ($data['PaymentMethodRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['name'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<?php	if($this->globalConfig['enableSendSCasPO']){	?>
		<div class="form-group">
			<label class="control-label col-md-4">SC Bill SKU</label>
			<div class="col-md-7"><input name="data[BillIdentifySKU]"  class="form-control BillIdentifySKU" type="text" placeholder="Enter SC Bill Identifier SKU" /> </div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">SC Bill Default Vendor ID</label>
			<div class="col-md-7"><input name="data[SalesCreditBillVendorRef]"  class="form-control SalesCreditBillVendorRef" type="text" placeholder="Enter SC Bill Default Vendor ID" /> </div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">SC Bill Account Ref</label>
			<div class="col-md-7">
				<select name="data[SalesCreditBillAccountRef]" data-required="0" class="form-control SalesCreditBillAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<?php	}	?>
		<?php	if($this->globalConfig['enableCOGSJournals']){		?>
		<div class="form-group">
			<label class="control-label col-md-4">Credit Account For COGS Journal SalesOrder (GO)</label>
			<div class="col-md-7">
				<select name="data[COGSCreditNominalSO]"  class="form-control COGSCreditNominalSO">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Debit Account For COGS Journal SalesOrder (GO)</label>
			<div class="col-md-7">
				<select name="data[COGSDebitNominalSO]"  class="form-control COGSDebitNominalSO">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Credit Account For COGS Journal SalesCredit (SG)</label>
			<div class="col-md-7">
				<select name="data[COGSCreditNominalSC]"  class="form-control COGSCreditNominalSC">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Debit Account For COGS Journal SalesCredit (SG)</label>
			<div class="col-md-7">
				<select name="data[COGSDebitNominalSC]"  class="form-control COGSDebitNominalSC">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Credit Account For COGS WriteOff Journal SalesCredit (PG)</label>
			<div class="col-md-7">
				<select name="data[COGSCreditNominalSCPG]"  class="form-control COGSCreditNominalSCPG">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Debit Account For COGS WriteOff Journal SalesCredit (PG)</label>
			<div class="col-md-7">
				<select name="data[COGSDebitNominalSCPG]"  class="form-control COGSDebitNominalSCPG">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Account For COGS Journal PurchaseOrder (GO)</label>
			<div class="col-md-7">
				<select name="data[DefaultCOGSforPurchase]"  class="form-control DefaultCOGSforPurchase">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<?php	}	?>
		<?php	if($this->globalConfig['enableSalesOrder']){	?>
		<div class="form-group">
			<label class="control-label col-md-4">Default Channel For SO</label>
			<div class="col-md-7">
				<select name="data[channelIds][]"  class="form-control channelIds"  multiple="true">
					<option value="">Select Channel</option>
					<?php
						$channels	= reset($data['channel']);
						foreach ($channels as $channel) {
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Warehouse For SO</label>
			<div class="col-md-7">
				<select name="data[warehouses][]" class="form-control warehouses" multiple="true">
					<option value="">Select Warehouse</option>
					<?php
						$warehouses	= reset($data['warehouse']);
						foreach ($warehouses as $warehouse) {
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<?php	}	?>
		<?php	if($this->globalConfig['enableSalesCredit']){	?>
		<div class="form-group">
			<label class="control-label col-md-4">Default Channel For SC</label>
			<div class="col-md-7">
				<select name="data[channelIdsSC][]"  class="form-control channelIdsSC"  multiple="true">
					<option value="">Select Channel</option>
					<?php
						$channels	= reset($data['channel']);
						foreach ($channels as $channel) {
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Warehouse For SC</label>
			<div class="col-md-7">
				<select name="data[warehousesSC][]" class="form-control warehousesSC" multiple="true">
					<option value="">Select Warehouse</option>
					<?php
						$warehouses	= reset($data['warehouse']);
						foreach ($warehouses as $warehouse) {
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<?php	}	?>
		<?php	if(($this->globalConfig['enablePurchaseOrder']) OR ($this->globalConfig['enablePurchaseCredit'])){	?>
		<div class="form-group">
			<label class="control-label col-md-4">Default Channel For PO/PC</label>
			<div class="col-md-7">
				<select name="data[channelIdspopc][]"  class="form-control channelIdspopc"  multiple="true">
					<option value="">Select Channel</option>
					<?php
						$channels	= reset($data['channel']);
						foreach ($channels as $channel) {
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Warehouse For PO/PC</label>
			<div class="col-md-7">
				<select name="data[warehousespopc][]" class="form-control warehousespopc" multiple="true">
					<option value="">Select Warehouse</option>
					<?php
						$warehouses	= reset($data['warehouse']);
						foreach ($warehouses as $warehouse) {
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<?php	}	?>
		<?php	if(($this->globalConfig['enableRefundReceipt'])){	?>
		<div class="form-group">
			<label class="control-label col-md-4">Default Channel For RefundReceipt</label>
			<div class="col-md-7">
				<select name="data[channelIds_refund_sales_receipt][]"  class="form-control channelIds_refund_sales_receipt"  multiple="true">
					<option value="">Select Channel</option>
					<?php
						$channels	= reset($data['channel']);
						foreach ($channels as $channel) {
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Warehouse For RefundReceipt</label>
			<div class="col-md-7">
				<select name="data[warehouses_refund_sales_receipt][]" class="form-control warehouses_refund_sales_receipt" multiple="true">
					<option value="">Select Warehouse</option>
					<?php
						$warehouses	= reset($data['warehouse']);
						foreach ($warehouses as $warehouse) {
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<?php	}	?>
		<div class="form-group hide"> 
			<label class="control-label col-md-4">QBO PayType Account Ref</label>
			<div class="col-md-7">
				<select name="data[PayTypeAccRef]" data-required="0" class="form-control PayTypeAccRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Is QBO Essential</label>
			<div class="col-md-7">
				<select name="data[isEssential]" class="form-control isEssential">
					<option value="">Select</option>
					<option value="0">No</option>
					<option value="1">Yes</option>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">QBO Anonymous Customer ID</label>
			<div class="col-md-7"><input name="data[QBOAnonymousCustomer]"  class="form-control QBOAnonymousCustomer" type="text" placeholder="Enter QBO Anonymous Customer ID" /></div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Brightpearl Anonymous Customer Name</label>
			<div class="col-md-7"><input name="data[BrightpearlAnonymousCustomer]"  class="form-control BrightpearlAnonymousCustomer" type="text" placeholder="Enter Brightpearl Anonymous Customer Name" /></div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Use As DocNumber</label>
			<div class="col-md-7">
				<select name="data[UseAsDocNumber]" data-required="0" class="form-control UseAsDocNumber">
					<option value="">Select</option>
					<option value="customer_reference">Customer/Supplier Ref. No.</option>
					<option value="invoice_reference">Brightpearl Invoice Ref. No.</option>
				</select> 
			</div> 
		</div>
		<?php 	if($this->globalConfig['enableInventoryTransfer']){		?>
		<div class="form-group hide">
			<label class="control-label col-md-4">Inventory Transfer Sell AccountRef</label>
			<div class="col-md-7">
				<select name="data[InventoryTransferSalesAccountRef]" class="form-control InventoryTransferSalesAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Inventory Transfer Purchase AccountRef</label>
			<div class="col-md-7">
				<select name="data[InventoryTransferPurchaseAccountRef]" class="form-control InventoryTransferPurchaseAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['qboAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
						}
					?>
				</select> 
			</div> 
		</div> 
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer Inter Co. Supplier</label>
			<div class="col-md-7"><input name="data[InterCoSupplier]" class="form-control InterCoSupplier" type="text" placeholder="Enter QBO Inventory Transfer Supplier ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer Inter Co. Customer</label>
			<div class="col-md-7"><input name="data[InterCoCustomer]" class="form-control InterCoCustomer" type="text" placeholder="Enter QBO Inventory Transfer Customer ID" /></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer SKU</label>
			<div class="col-md-7"><input name="data[InventoryTransferSKU]" class="form-control InventoryTransferSKU" type="text" placeholder="Enter QBO Inventory Transfer Item ID" /></div>
		</div>
		<?php	}	?>
		<?php 	if($this->globalConfig['enableSingleCompanyStocktx']){		?>
		<div class="form-group">
			<label class="control-label col-md-4">Warehouse For StockTransfer as Journal</label>
			<div class="col-md-7">
				<select name="data[warehouseForStockTx][]" class="form-control warehouseForStockTx" multiple="true">
					<option value="">Select Warehouse</option>
					<?php
						$warehouses	= reset($data['warehouse']);
						foreach ($warehouses as $warehouse) {
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<?php	}	?>
	</div> 
<?php
	}
?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
	$(".chosen-select").chosen({width: "100%"}); 
</script>