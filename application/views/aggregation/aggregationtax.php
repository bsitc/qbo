<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
                <li><span>Aggregation</span><i class="fa fa-circle"></i></li>
                <li><span>Aggregation TaxItem Mapping</span></li>
            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cube"></i>Aggregation TaxItem Mapping</div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs">Add New Mapping</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="15%">Brightpearl Tax</th>
                                    <th width="15%">QBO TaxItem</th>
                                    <th width="15%">Brightpearl Account</th>
                                    <th width="15%">QBO Account</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="account1TaxId"></span></td>
                                    <td><span class="value" data-value="account2TaxItem"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('aggregation/aggregationtax/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) {?> 
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    
									<td><span class="value" data-value="account1TaxId"><?php echo @($data['account1TaxId'][$row['account1Id']][$row['account1TaxId']])?($data['account1TaxId'][$row['account1Id']][$row['account1TaxId']]['name']):($row['account1TaxId']);?></span></td>
                                   
									<td><span class="value" data-value="account2TaxItem"><?php echo @($data['account2TaxItem'][$row['account2Id']][$row['account2TaxItem']])?($data['account2TaxItem'][$row['account2Id']][$row['account2TaxItem']]['name']):($row['account2TaxItem']);?></span></td>
                                    
									<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    
									<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    
									<td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick='editAction(data<?php echo $row['id'];?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('aggregation/aggregationtax/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr> 
                                <?php	}	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">TaxItem Mapping</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('aggregation/aggregationtax/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>You have some form errors. Please check below.
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<option value="" class="show">Select a Brightpearl Account</option>
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<select name="data[account2Id]"  data-required="1" class="form-control account2Id acc2list">
											<option value="" class="show">Select a QBO Account</option>
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl TaxCode<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account1TaxId']){	?> 
										<select name="data[account1TaxId]" data-required="1" class="form-control account1TaxId acc1listoption">
											<option value="" class="show">Select a Brightpearl TaxCode</option>
												<?php
													foreach ($data['account1TaxId'] as $accountId => $account1TaxIds) {
														foreach ($account1TaxIds as $account1TaxId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1TaxId['id'].'">'.ucwords($account1TaxId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account1TaxId]" data-required="1" class="form-control account1TaxId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO TaxItem ID<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<input type="text" name="data[account2TaxItem]" data-required="1" class="form-control account2TaxItem" />
									</div>
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>                         
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>