<?php	//qbo	?>

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Consolidation</span><i class="fa fa-circle"></i></li>
				<li><span>Consolidation Sales Credit Report</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-file-excel-o"></i>Consolidation Sales Credit Report</div>
						<div class="actions">
							<a href="<?php echo base_url('aggregation/salescreditreport/exportSalescredit?');?>" class="btn btn-circle btn-danger exportBtn">
								<i class="fa fa-file-excel-o"></i>
								<span class="hidden-xs">Export Report in CSV</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper"></div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="10%">Channel</th>
										<th width="5%">Order ID</th>
										<th width="5%">Invoice Reference</th>
										<th width="5%">Reference</th>
										<th width="10%">QBO ID</th>
										<th width="8%">QBO DocNumber</th>
										<th width="5%">Amount</th>
										<th width="5%">COGSAmount</th>
										<th width="5%">Payment Received</th>
										<th width="5%">Payment Status</th>
										<th width="6%">TaxDate</th>
										<th width="6%">Created</th>
										<th width="5%">Action</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td><input type="text" class="form-control form-filter input-sm channelName" name="channelName" /></td>
										<td><input type="text" class="form-control form-filter input-sm orderId" name="orderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm bpInvoiceNumber" name="bpInvoiceNumber" /></td>
										<td><input type="text" class="form-control form-filter input-sm reference" name="reference" /></td>
										<td><input type="text" class="form-control form-filter input-sm createOrderId" name="createOrderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm invoiceRef" name="invoiceRef" /></td>
										<td><input type="text" class="form-control form-filter input-sm totalAmount" name="totalAmount" /></td>
										<td><input type="text" class="form-control form-filter input-sm COGSAmount" name="COGSAmount" /></td>
										<td><input type="text" class="form-control form-filter input-sm FetchedAmount" name="FetchedAmount" /></td>
										<td>
											<select name="paymentStatus" class="form-control form-filter input-sm paymentStatus">
											<option value="">Select</option>
											<option value="0">UnPaid</option>
											<option value="1">Paid</option>
											<option value="2">Partially Paid</option>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm taxDate_from" readonly name="taxDate_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm taxDate_to" readonly name="taxDate_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>	
												</span>
											</div>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm created_from" readonly name="created_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm created_to" readonly name="created_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>	
												</span>
											</div>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	loadUrl	= '<?php echo base_url('aggregation/salescreditreport/getCredit');?>';
	jQuery(".exportBtn").on("click",function(e){
		e.preventDefault();
		url	= jQuery(this).attr('href')+'reference='+jQuery(".reference").val()+'&channelName='+jQuery(".channelName").val()+'&orderId='+jQuery(".orderId").val()+'&createOrderId='+jQuery(".createOrderId").val()+'&bpInvoiceNumber='+jQuery(".bpInvoiceNumber").val()+'&invoiceRef='+jQuery(".invoiceRef").val()+'&totalAmount='+jQuery(".totalAmount").val()+'&paymentStatus='+jQuery(".paymentStatus").val()+'&taxDate_from='+jQuery(".taxDate_from").val()+'&taxDate_to='+jQuery(".taxDate_to").val()+'&created_from='+jQuery(".created_from").val()+'&created_to='+jQuery(".created_to").val();
		window.location.href	= url;
	})
</script>
<style>
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
	top: 28px !important;
}
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
	top: 28px !important;
}
</style>