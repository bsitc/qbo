<?php
$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
$userLoginData			= $this->session->userdata('login_user_data');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Sales Order</span><i class="fa fa-circle"></i></li>
				<li><span>Sales Order</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-shopping-cart"></i>COGS Journal Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('cogsjournal/cogsjournal/fetchCogsjournal');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch Journals</span>
							</a>
							<?php	if($userLoginData['role'] == 'admin'){	?>
							<a href="" class="btn btn-circle green-meadow btnactionsubmit" onclick="confirmAction()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post Journals</span>
							</a>
							<?php		if($this->globalConfig['enableAggregation']){	?>
							<a href="" class="btn btn-circle green-meadow btnactionsubmit" onclick="confirmAction2()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post Consolidation COGS Journals</span>
							</a>
							<?php	}	?>
							<?php	}	?>
							<a href="<?php echo base_url('cogsjournal/cogsjournal/exportReport?');?>" class="btn btn-circle btn-danger exportBtn">
								<i class="fa fa-file-excel-o"></i>
								<span class="hidden-xs">Export Report in CSV</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<?php	if($userLoginData['role'] == 'admin'){	?>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="4">Archive</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
								<?php	}	?> 
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="10%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Account</th>
										<th width="10%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Account</th>
										<th width="10%">Brightpearl Journal ID</th>
										<th width="10%">QBO Journal ID</th>
										<th width="10%">Reference</th>
										<th width="10%">OrderID</th>
										<th width="10%">JournalType</th>
										<th width="10%">OrderType</th>
										<th width="15%">TaxDate</th>
										<th width="15%">Created</th>
										<th width="10%">Status</th>
										<th width="10%">IsConsol</th>
										<th width="10%">Message</th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td>
											<select name="account1Id" class="form-control form-filter input-sm account1Id">
												<option value="">Select an option</option>
												<?php
													foreach($account1MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td>
											<select name="account2Id" class="form-control form-filter input-sm account2Id">
												<option value="">Select an option</option>
												<?php
													foreach($account2MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm journalsId" name="journalsId" /></td>
										<td><input type="text" class="form-control form-filter input-sm createdJournalsId" name="createdJournalsId" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="DocNumber" /></td>
										<td><input type="text" class="form-control form-filter input-sm orderId" name="orderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm journalTypeCode" style="text-transform: uppercase;" name="journalTypeCode" /></td>
										<td><input type="text" class="form-control form-filter input-sm OrderType" style="text-transform: uppercase;" name="OrderType" /></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm taxDate_from" readonly name="taxDate_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm taxDate_to" readonly name="taxDate_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="created_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="created_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm status">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
												<option value="4">Archive</option>
											</select>
										</td>
										<td>
											<select name="isConsolidated" class="form-control form-filter input-sm isConsolidated">
												<option value="">Select...</option>
												<option value="1">YES</option>
												<option value="0">NO</option>
											</select>
										</td>
										<td></td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody> </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>		
<script type="text/javascript">
	function confirmAction(){
		var answer = confirm("Do you want to send all the Journal?");
		if(answer == true){
			$.ajax({
				url			:	"<?php echo base_url('cogsjournal/cogsjournal/postCogsjournal');?>",
				type		:	"POST",
				dataType	:	"json",
				success		:	function(){}
			})
		}
	}
	function confirmAction2(){
		var answer = confirm("Do you want to send CONSOL COGS Journal?");
		if(answer == true){
			$.ajax({
				url			:	"<?php echo base_url('cogsjournal/cogsjournal/postConsolCogsjournal');?>",
				type		:	"POST",
				dataType	:	"json",
				success		:	function(){}
			})
		}
	}
	loadUrl = '<?php echo base_url('cogsjournal/cogsjournal/getCogsjournal');?>';
	jQuery(".exportBtn").on("click",function(e){
		e.preventDefault();
		url	= jQuery(this).attr('href')+'account1Id='+jQuery(".account1Id").val()+'&account2Id='+jQuery(".account2Id").val()+'&orderId='+jQuery(".orderId").val()+'&journalsId='+jQuery(".journalsId").val()+'&createdJournalsId='+jQuery(".createdJournalsId").val()+'&journalTypeCode='+jQuery(".journalTypeCode").val()+'&OrderType='+jQuery(".OrderType").val()+'&taxDate_from='+jQuery(".taxDate_from").val()+'&taxDate_to='+jQuery(".taxDate_to").val()+'&status='+jQuery(".status").val()+'&isConsolidated='+jQuery(".isConsolidated").val();
		window.location.href	= url;
	})
</script>
<style>
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
		top: 28px !important;
	}
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
		top: 28px !important;
	}
</style>