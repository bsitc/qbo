<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>COGS Journal</span><i class="fa fa-circle"></i></li>
				<li><span>Journal Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> COGS Journal Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($cogsjournalInfo['params'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> COGS Journal Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($cogsjournalInfo['createdParams'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>