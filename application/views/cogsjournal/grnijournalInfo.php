<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>GRNI Journal</span><i class="fa fa-circle"></i></li>
				<li><span>Journal Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> GRNI Journal Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($grnijournalInfo['params'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> GRNI Journal Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($grnijournalInfo['createdParams'],true), JSON_PRETTY_PRINT)."</pre>";	?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>