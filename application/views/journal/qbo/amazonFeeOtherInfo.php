<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Amazon Fee</span><i class="fa fa-circle"></i></li>
				<li><span>Amazon FeesInfo</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-amazon"></i><?php echo ucwords($this->globalConfig['account1Liberary']);?> Amazon Fee (Other) Information</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($amazonFeeOtherInfo['params'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-amazon"></i><?php echo ucwords($this->globalConfig['account2Liberary']);?> Amazon Fee (Other) Information</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($amazonFeeOtherInfo['createdParams'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>