<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Amazon Fees</span><i class="fa fa-circle"></i></li>
				<li><span>Amazon Fees Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-amazon"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> Amazon Fees Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($journalinfo['params'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-amazon"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> Amazon Fees Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($journalinfo['createdParams'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>