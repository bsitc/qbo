<?php
	$userLoginData	= $this->session->userdata('login_user_data');
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler"></div>
			</li>
		  	<li class="nav-item dashboard">
				<a href="<?php echo base_url();?>dashboard" class="nav-link ">
					<span aria-hidden="true" class="fa fa-home"></span>
					<span class="title">Dashboardssssss</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	if($userLoginData['role'] == 'admin'){	?>
			<li class="nav-item account">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-cogs"></span>
					<span class="title">Account Settings</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title"><?php echo $this->globalConfig['account1Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">		
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account1/account');?>" class="nav-link ">
									<i class="fa fa-square-o"></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account1/config');?>" class="nav-link ">
									<i class="fa fa-cogs"></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title"><?php echo $this->globalConfig['account2Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">		
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account2/account');?>" class="nav-link ">
									<i class="fa fa-square-o"></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account2/config');?>" class="nav-link ">
									<i class="fa fa-cogs"></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableMapping']){	?>
			<li class="nav-item mapping">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-server"></span>
					<span class="title">Mapping</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php 
						$mappings		= $this->config->item('mapping');
						$mappingIcon	= array('brand' => 'map','category' => 'map', 'channel' => 'bar-chart','leadsource' => 'calculator','payment' => 'credit-card', 'pricelist' => 'calculator' , 'shipping' => 'truck','salesrep' => 'map', 'tax' => 'calculator', 'warehouse' => 'truck','box' => 'cube','packedmapping' => 'cube','inventoryadvice' => 'bar-chart','nominal' => 'bank','genericcustomer' => 'user','customField' => 'list','channelLocation' => 'map-marker','terms' => 'pencil','paymentpurchase' => 'credit-card','customertype' => 'user','taximacro' => 'calculator', 'currency' => 'usd');
						foreach($mappings as $mapKey => $mapping){
							if(@$this->globalConfig['enable'.ucwords($mapKey).'Mapping']){
					?>
					<li class="nav-item <?php echo $mapKey;?>">
						<a href="<?php echo base_url();?>mapping/<?php echo $mapKey;?>" class="nav-link ">
							<span aria-hidden="true" class="fa fa-<?php echo (@$mappingIcon[$mapKey])?($mappingIcon[$mapKey]):'share';?>"></span>
							<span class="title"><?php echo $mapping;?> Mapping</span>
						</a>
					</li>
					<?php
							}
						}
					?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableFieldMapping']){	?>
			<li class="nav-item mapping">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-handbag"></span>
					<span class="title">Field Configuration</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php
						if($this->globalConfig['enableFieldMapping']){
							$mappings		= $this->config->item('fieldmapping');
							$mappingIcon	= array('ack' => 'map', 'packing' => 'bag','invoice' => 'bar-chart');
							foreach($mappings as $mapKey => $mapping){ 
								if(@$this->globalConfig['enable'.ucwords($mapKey).'Mapping']){
					?>
					<li class="nav-item <?php echo $mapKey;?>">
						<a href="<?php echo base_url();?>fieldconfig/<?php echo $mapKey;?>" class="nav-link ">
							<span aria-hidden="true" class="icon-<?php echo (@$mappingIcon[$mapKey])?($mappingIcon[$mapKey]):'share';?>"></span>
							<span class="title"><?php echo $mapping;?></span>
						</a>
					</li>
					<?php
								}
							}
						}
					?> 
				</ul>
			</li>   
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableProduct']){	?>
			<li class="nav-item products">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-th"></span>
					<span class="title">Products</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item products">
						<a href="<?php echo base_url();?>products/products" class="nav-link ">
							<i class="fa fa-th"></i>
							<span class="title">Products</span>
						</a>
					</li>
					<?php	if($this->globalConfig['enablePrebook']){	?>
					<li class="nav-item preproducts ">
						<a href="<?php echo base_url();?>products/products/preproducts" class="nav-link ">
							<i class="fa fa-th"></i>
							<span class="title">Pre-Order</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableCustomer']){		?>
			<li class="nav-item customers">
				<a href="<?php echo base_url();?>customers/customers" class="nav-link ">
					<span aria-hidden="true" class="fa fa-user"></span>
					<span class="title">Customers/Vendors</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableSalesOrder']){	?>
			<li class="nav-item sales">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-shopping-cart"></span>
					<span class="title">Sales Order</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">					
					<li class="nav-item  sales">
						<a href="<?php echo base_url();?>sales/sales" class="nav-link ">
							<i class="fa fa-shopping-cart"></i>
							<span class="title">Sales Order</span>
						</a>
					</li>           
					<?php	if(@$this->globalConfig['enableSalesCredit']){	?>
					<li class="nav-item credit ">
						<a href="<?php echo base_url();?>sales/credit" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Sales Credit</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableRefundReceipt']){	?>
			<li class="nav-item refundRceipt">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-exchange"></span>
					<span class="title">Receipt</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php	if(@$this->globalConfig['enableRefundReceipt']){	?>
					<li class="nav-item  refundRceipt">
						<a href="<?php echo base_url();?>receipt/refundreceipt" class="nav-link">
							<i class="fa fa-exchange"></i>
							<span class="title">Refund Receipt</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enablePacking']){	?>
			<li class="nav-item packed">
				<a href="<?php echo base_url();?>packed/packed" class="nav-link ">
					<i class="icon-drawer"></i>
					<span class="title">Packing</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enablePurchaseOrder']){	?>
			<li class="nav-item purchase">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-cart-plus"></span>
					<span class="title">Purchase Order</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  purchase">
						<a href="<?php echo base_url();?>purchase/purchase" class="nav-link ">
							<i class="fa fa-cart-plus"></i>
							<span class="title">Purchase Order</span>
						</a>
					</li>           
					<?php	if($this->globalConfig['enableReceipt']){	?>
					<li class="nav-item  receipt">
						<a href="<?php echo base_url();?>purchase/receipt" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Receipt Confirmation</span>
						</a>
					</li>
					<?php	}	?>
					<?php	if($this->globalConfig['enablePurchaseCredit']){	?>
					<li class="nav-item  credit">
						<a href="<?php echo base_url();?>purchase/credit" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Purchase Credit</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if((@$this->globalConfig['enableStockSync']) OR (@$this->globalConfig['enableStockTransfer']) OR (@$this->globalConfig['enableInventoryadvice']) OR (@$this->globalConfig['enableStockAdjustment'])){	?>
			<li class="nav-item stock">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-recycle"></span>
					<span class="title">Stock Details</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">	
					<?php	if($this->globalConfig['enableStockTransfer']){		?>
					<li class="nav-item  transfer">
						<a href="<?php echo base_url();?>stock/transfer" class="nav-link ">
							<i class="icon-user"></i>
							<span class="title">Stock Transfer</span>
							<span class="selected"></span>
						</a>
					</li>
					<?php	}	?>
					<?php	if($this->globalConfig['enableInventoryadvice']){	?>
					<li class="nav-item  adjustment">
						<a href="<?php echo base_url();?>stock/inventoryadvice" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Inventory Advice</span>
						</a>
					</li>           
					<?php	}	?>  
					<?php	if($this->globalConfig['enableStockAdjustment']){	?>
					<li class="nav-item  adjustment">
						<a href="<?php echo base_url();?>stock/adjustment" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Stock Adjustment</span>
						</a>
					</li>           
					<?php 	}	?>
					<?php	if($this->globalConfig['enableStockSync']){	?>
					<li class="nav-item  sync">
						<a href="<?php echo base_url();?>stock/sync" class="nav-link ">
							<i class="fa fa-recycle"></i>
							<span class="title">Stock Sync</span>
						</a>
					</li>
					<li class="nav-item  synclog">
						<a href="<?php echo base_url();?>stock/synclog" class="nav-link ">
							<i class="fa fa-trash-o"></i>
							<span class="title">Stock Sync Log</span>
						</a>
					</li>
					<?php 	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableamazonfee']){	?>
			<li class="nav-item journal">
				<a href="<?php echo base_url();?>journal/journal" class="nav-link ">
					<span aria-hidden="true" class="fa fa-amazon"></span>
					<span class="title">Amazon Fees</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableSingleCompanyStocktx']){	?>
			<li class="nav-item enableSingleCompanyStocktx">
				<a href="<?php echo base_url();?>singlestocktx/singlestocktx" class="nav-link ">
					<span aria-hidden="true" class="fa fa-exchange"></span>
					<span class="title">Stock Transfer</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableAggregation']){	?>
			<li class="nav-item aggreagtion">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-database"></span>
					<span class="title">Consolidation</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  aggreagtionmap">
						<a href="<?php echo base_url('aggregation/aggregationmapping');?>" class="nav-link ">
							<i class="fa fa-link"></i>
							<span class="title">Consolidation Mapping</span>
						</a>
					</li>
					<li class="nav-item  aggreagtiontaxmap">
						<a href="<?php echo base_url('aggregation/salesreport');?>" class="nav-link ">
							<i class="fa fa-file-excel-o"></i>
							<span class="title">Consolidation SO Report</span>
						</a>
					</li>
					<li class="nav-item  aggreagtiontaxmap">
						<a href="<?php echo base_url('aggregation/salescreditreport');?>" class="nav-link ">
							<i class="fa fa-file-excel-o"></i>
							<span class="title">Consolidation SC Report</span>
						</a>
					</li>
					<li class="nav-item  aggreagtiontaxmap hide">
						<a href="<?php echo base_url('aggregation/aggregationtax');?>" class="nav-link ">
							<i class="fa fa-cube"></i>
							<span class="title">Consolidation TaxItem Mapping</span>
						</a>
					</li>
					<li class="nav-item  aggreagtionconfig hide">
						<a href="<?php echo base_url('aggregation/config');?>" class="nav-link ">
							<i class="fa fa-cogs"></i>
							<span class="title">Consolidation Configuration</span>
						</a>
					</li>
				</ul>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableCOGSJournals']){		?>
			<li class="nav-item cogsjournal">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-file"></span>
					<span class="title">COGS Journal</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item cogsjournal">
						<a href="<?php echo base_url();?>cogsjournal/cogsjournal" class="nav-link ">
							<i class="fa fa-file"></i>
							<span class="title">COGS Journal</span>
						</a>
					</li>
				</ul>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableTaxCustomisation']){	?>
			<li class="nav-item taxupdate">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-calculator"></span>
					<span class="title">Tax Customisation</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item taxupdate">
						<a href="<?php echo base_url();?>taxupdate/taxupdate" class="nav-link ">
							<i class="fa fa-calculator"></i>
							<span class="title">Tax Customisation Configuration</span>
						</a>
					</li>
					<li class="nav-item taxupdate">
						<a href="<?php echo base_url();?>taxupdate/updatedsales" class="nav-link ">
							<i class="fa fa-calculator"></i>
							<span class="title">Tax Customised Sales/Credit</span>
						</a>
					</li>
				</ul>
			</li>
			<?php	}	?>
		</ul>
	</div>
</div>
<?php	
	$controllerName	= @$this->router->class;
	$allDirectory	= @json_encode(array_filter(explode("/",$this->router->directory)));
	$method			= @$this->router->method;
?>
<script>
	var	directory		= <?php echo $allDirectory;?>;
	var	controllerName	= '<?php echo $controllerName;?>';
	var	method			= '<?php echo $method;?>';
	var	activeClass		= '';
	for	(index in directory) {
		value	= directory[index];
		if(jQuery(activeClass + " ."+value).length){
			activeClass	+= " ." + value;
		}
	}
	var activeClass1	= '';
	if(controllerName !=""){
		if(jQuery(activeClass + " ."+controllerName).length){
			activeClass	+= " ." + controllerName;
		}
	}
	if( (method !="") && (method != 'index')){
		if(jQuery(activeClass + " ."+method).length){
			activeClass	+= " ." + method;
		}
	}
	if(activeClass != ""){
		jQuery(activeClass).addClass('active open');
	}
</script>