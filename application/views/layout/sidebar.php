<?php
	//qbo
	$userLoginData	= $this->session->userdata('login_user_data');
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse sidebarnewcss">
		<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler"></div>
			</li>
			<li class="nav-item dashboard">
				<a href="<?php echo base_url();?>dashboard" class="nav-link">
					<span aria-hidden="true" class="fa fa-home"></span>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	if($userLoginData['role'] == 'admin'){	?>
			<li class="nav-item account">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-cogs"></span>
					<span class="title">Account Settings</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title"><?php echo $this->globalConfig['account1Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item">
								<a href="<?php echo base_url('account/account1/account');?>" class="nav-link ">
									<i class="fa fa-square-o"></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url('account/account1/config');?>" class="nav-link ">
									<i class="fa fa-cogs"></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-cog"></i>
							<span class="title"><?php echo $this->globalConfig['account2Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item">
								<a href="<?php echo base_url('account/account2/account');?>" class="nav-link ">
									<i class="fa fa-square-o"></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account2/config');?>" class="nav-link ">
									<i class="fa fa-cogs"></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableMapping']){	?>
			<li class="nav-item mapping">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-server"></span>
					<span class="title">Mapping</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php 
						$mappings		= $this->config->item('mapping');
						$mappingIcon	= array('brand' => 'map','category' => 'map', 'channel' => 'bar-chart','leadsource' => 'calculator','payment' => 'credit-card', 'pricelist' => 'calculator' , 'shipping' => 'truck','salesrep' => 'map', 'tax' => 'calculator', 'warehouse' => 'truck','box' => 'cube','packedmapping' => 'cube','inventoryadvice' => 'bar-chart','nominal' => 'bank','genericcustomer' => 'user','customField' => 'list','channelLocation' => 'map-marker','terms' => 'pencil','paymentpurchase' => 'credit-card','customertype' => 'user','taximacro' => 'calculator', 'currency' => 'usd','defaultitem' => 'sitemap');
						foreach($mappings as $mapKey => $mapping){
							if(@$this->globalConfig['enable'.ucwords($mapKey).'Mapping']){
					?>
					<li class="nav-item <?php echo $mapKey;?>">
						<a href="<?php echo base_url();?>mapping/<?php echo $mapKey;?>" class="nav-link ">
							<span aria-hidden="true" class="fa fa-<?php echo (@$mappingIcon[$mapKey])?($mappingIcon[$mapKey]):'share';?>"></span>
							<span class="title"><?php echo $mapping;?></span>
						</a>
					</li>
					<?php
							}
						}
					?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableProduct']){	?>
			<li class="nav-item products">
				<a href="<?php echo base_url();?>products/products" class="nav-link ">
					<span aria-hidden="true" class="fa fa-th"></span>
					<span class="title">Products</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableCustomer']){	?>
			<li class="nav-item customers">
				<a href="<?php echo base_url();?>customers/customers" class="nav-link ">
					<span aria-hidden="true" class="fa fa-user"></span>
					<span class="title">Customers/Vendors</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableSalesOrder']){	?>
			<li class="nav-item sales">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-shopping-cart"></span>
					<span class="title">Sales</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item sales">
						<a href="<?php echo base_url();?>sales/sales" class="nav-link ">
							<i class="fa fa-shopping-cart"></i>
							<span class="title">Sales Order</span>
						</a>
					</li>
					<?php	if(@$this->globalConfig['enableSalesCredit']){	?>
					<li class="nav-item credit">
						<a href="<?php echo base_url();?>sales/credit" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Sales Credit</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enablePurchaseOrder']){	?>
			<li class="nav-item purchase">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-cart-plus"></span>
					<span class="title">Purchase</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  purchase">
						<a href="<?php echo base_url();?>purchase/purchase" class="nav-link ">
							<i class="fa fa-cart-plus"></i>
							<span class="title">Purchase Order</span>
						</a>
					</li>
					<?php	if($this->globalConfig['enableReceipt']){	?>
					<li class="nav-item  receipt">
						<a href="<?php echo base_url();?>purchase/receipt" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Receipt Confirmation</span>
						</a>
					</li>
					<?php	}	?>
					<?php	if($this->globalConfig['enablePurchaseCredit']){	?>
					<li class="nav-item  credit">
						<a href="<?php echo base_url();?>purchase/credit" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Purchase Credit</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableRefundReceipt']){	?>
			<li class="nav-item refundRceipt">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-exchange"></span>
					<span class="title">Receipt</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  refundRceipt">
						<a href="<?php echo base_url();?>receipt/refundreceipt" class="nav-link">
							<i class="fa fa-exchange"></i>
							<span class="title">Refund Receipt</span>
						</a>
					</li>
					<?php	if(@$this->globalConfig['enableRefundReceiptConsol']){	?>
					<li class="nav-item  refundRceipt">
						<a href="<?php echo base_url();?>receipt/consolmapping" class="nav-link">
							<i class="fa fa-exchange"></i>
							<span class="title">Consolidation Mapping</span>
						</a>
					</li>
					<?php	}	?>
				</ul>
			</li>
			<?php	}	?>
			<?php	if(@$this->globalConfig['enableStockAdjustment']){	?>
			<li class="nav-item adjustment">
				<a href="<?php echo base_url();?>stock/adjustment" class="nav-link ">
					<span aria-hidden="true" class="fa fa-cubes"></span>
					<span class="title">Stock Adjustment</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableSingleCompanyStocktx']){	?>
			<li class="nav-item enableSingleCompanyStocktx">
				<a href="<?php echo base_url();?>singlestocktx/singlestocktx" class="nav-link ">
					<span aria-hidden="true" class="fa fa-exchange"></span>
					<span class="title">Stock Transfer</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableConsolStockAdjustment']){ ?>
			<li class="nav-item stockjournal">
				<a href="<?php echo base_url();?>stockjournal/stockjournal" class="nav-link ">
					<span aria-hidden="true" class="fa fa-cubes"></span>
					<span class="title">Stock Adjustment Journal</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableamazonfee'] AND @$this->globalConfig['enableAmazonFeeOther']){ ?>
			<li class="nav-item amazonFee">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-amazon"></span>
					<span class="title">Amazon Fee</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item amazonFeeOrder">
						<a href="<?php echo base_url();?>journal/journal" class="nav-link ">
							<i class="fa fa-amazon"></i>
							<span class="title">Amazon Fee</span>
						</a>
					</li>
					<li class="nav-item amazonFeeOther">
						<a href="<?php echo base_url();?>journal/otheramazonfee" class="nav-link ">
							<i class="fa fa-amazon"></i>
							<span class="title">Amazon Fee (Other)</span>
						</a>
					</li>
				</ul>
			</li>
			<?php	}
					elseif(@$this->globalConfig['enableamazonfee']){	?>
			<li class="nav-item journal">
				<a href="<?php echo base_url();?>journal/journal" class="nav-link ">
					<span aria-hidden="true" class="fa fa-amazon"></span>
					<span class="title">Amazon Fee</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableAggregation']){	?>
			<li class="nav-item aggreagtion">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-database"></span>
					<span class="title">Consolidation</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  aggreagtionmap">
						<a href="<?php echo base_url('aggregation/aggregationmapping');?>" class="nav-link ">
							<i class="fa fa-link"></i>
							<span class="title">Consolidation Mapping</span>
						</a>
					</li>
					<li class="nav-item  aggreagtiontaxmap">
						<a href="<?php echo base_url('aggregation/salesreport');?>" class="nav-link ">
							<i class="fa fa-file-excel-o"></i>
							<span class="title">Consolidation SO Report</span>
						</a>
					</li>
					<li class="nav-item  aggreagtiontaxmap">
						<a href="<?php echo base_url('aggregation/salescreditreport');?>" class="nav-link ">
							<i class="fa fa-file-excel-o"></i>
							<span class="title">Consolidation SC Report</span>
						</a>
					</li>
					<li class="nav-item  aggreagtiontaxmap hide">
						<a href="<?php echo base_url('aggregation/aggregationtax');?>" class="nav-link ">
							<i class="fa fa-cube"></i>
							<span class="title">Consolidation TaxItem Mapping</span>
						</a>
					</li>
					<li class="nav-item  aggreagtionconfig hide">
						<a href="<?php echo base_url('aggregation/config');?>" class="nav-link ">
							<i class="fa fa-cogs"></i>
							<span class="title">Consolidation Configuration</span>
						</a>
					</li>
				</ul>
			</li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableCOGSJournals']){		?>
			<?php		if($this->globalConfig['enableGRNIjournal']){	?>
			<li class="nav-item journals">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-file"></span>
					<span class="title">Journals</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item cogsjournal">
						<a href="<?php echo base_url();?>cogsjournal/cogsjournal" class="nav-link ">
							<i class="fa fa-file"></i>
							<span class="title">COGS</span>
						</a>
					</li>
					<li class="nav-item grnijournal">
						<a href="<?php echo base_url();?>cogsjournal/grnijournal" class="nav-link ">
							<i class="fa fa-file"></i>
							<span class="title">GRNI</span>
						</a>
					</li>
				</ul>
			</li>
			<?php		}
						else{	?>
			<li class="nav-item cogsjournal">
				<a href="<?php echo base_url();?>cogsjournal/cogsjournal" class="nav-link ">
					<span aria-hidden="true" class="fa fa-file"></span>
					<span class="title">COGS Journal</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php		}	?>
			<?php	}		?>
			<?php	if($this->globalConfig['enableTaxCustomisation']){	?>
			<li class="nav-item taxupdate">
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="fa fa-calculator"></span>
					<span class="title">Tax Customisation</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item taxupdate">
						<a href="<?php echo base_url();?>taxupdate/taxupdate" class="nav-link ">
							<i class="fa fa-calculator"></i>
							<span class="title">Tax Customisation Configuration</span>
						</a>
					</li>
					<li class="nav-item taxupdate">
						<a href="<?php echo base_url();?>taxupdate/updatedsales" class="nav-link ">
							<i class="fa fa-calculator"></i>
							<span class="title">Tax Customised Sales/Credit</span>
						</a>
					</li>
				</ul>
			</li>
			<?php	}	?>
		</ul>
	</div>
</div>
<?php	
	$controllerName	= @$this->router->class;
	$allDirectory	= @json_encode(array_filter(explode("/",$this->router->directory)));
	$method			= @$this->router->method;
?>
<script>
	var	directory		= <?php echo $allDirectory;?>;
	var	controllerName	= '<?php echo $controllerName;?>';
	var	method			= '<?php echo $method;?>';
	var	activeClass		= '';
	for	(index in directory) {
		value	= directory[index];
		if(jQuery(activeClass + " ."+value).length){
			activeClass	+= " ." + value;
		}
	}
	var activeClass1	= '';
	if(controllerName !=""){
		if(jQuery(activeClass + " ."+controllerName).length){
			activeClass	+= " ." + controllerName;
		}
	}
	if((method !="") && (method != 'index')){
		if(jQuery(activeClass + " ."+method).length){
			activeClass	+= " ." + method;
		}
	}
	if(activeClass != ""){
		jQuery(activeClass).addClass('active open');
	}
</script>