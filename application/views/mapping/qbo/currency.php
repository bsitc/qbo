<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Mapping</span><i class="fa fa-circle"></i></li>
				<li><span>Currency Mapping</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-usd"></i>Currency Mapping</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Mapping</span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="">
						<table class="table table-hover text-centered actiontable" id="sample_4">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th width="25%"><?php echo $this->globalConfig['account1Name'];?> Currency</th>
									<th width="25%"><?php echo $this->globalConfig['account2Name'];?> Deposit Account</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Account</th>
									<th width="15%"><?php echo $this->globalConfig['account2Name'];?> Account</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="account1CurrencyId"></span></td>
									<td><span class="value" data-value="account2DepositAccId"></span></td>
									<td><span class="value" data-value="account1Id"></span></td>
									<td><span class="value" data-value="account2Id"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('mapping/currency/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
									<?php	foreach ($data['data'] as $key =>  $row){	?>
									<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
									<tr class="tr<?php echo $row['id'];?>">
										<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									
										<td><span class="value" data-value="account1CurrencyId"><?php echo @($data['account1CurrencyId'][$row['account1Id']][$row['account1CurrencyId']])?($data['account1CurrencyId'][$row['account1Id']][$row['account1CurrencyId']]['name']):($row['account1CurrencyId']);?></span></td>
									
										<td><span class="value" data-value="account2DepositAccId"><?php echo @($data['account2DepositAccId'][$row['account2Id']][$row['account2DepositAccId']])?($data['account2DepositAccId'][$row['account2Id']][$row['account2DepositAccId']]['name']):($row['account2DepositAccId']);?></span></td>
									
										<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
									
										<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
								
										<td class="action">
											<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
											<a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/currency/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
										</td>
									</tr>
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade " id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Currency Mapping</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('mapping/currency/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>You have some form errors. Please check below.
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<option value="">Select a Brightpearl Account</option>
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<option value="">Select a QBO Account</option>
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Currency<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account1CurrencyId']){	?> 
										<select name="data[account1CurrencyId]" data-required="1" class="form-control acc1listoption account1CurrencyId">
											<option value="">Select a Brightpearl Currency</option>
											<?php
													foreach ($data['account1CurrencyId'] as $accountId => $account1CurrencyIds) {
														foreach ($account1CurrencyIds as $account1CurrencyId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1CurrencyId['code'].'">'.ucwords($account1CurrencyId['code']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account1CurrencyId]" data-required="1" class="form-control account1CurrencyId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Desposit Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account2DepositAccId']){	?> 
										<select name="data[account2DepositAccId]" data-required="1" class="form-control acc2listoption account2DepositAccId">
											<option value="">Select a QBO Desposit Account</option>
											<?php	
													foreach ($data['account2DepositAccId'] as $accountId => $account2DepositAccIds) {
														foreach ($account2DepositAccIds as $account2DepositAccId) {
															echo '<option value="'.$account2DepositAccId['id'].'" class="acc2listoption'.$accountId.'" >'.ucwords($account2DepositAccId['name']).' ('.$account2DepositAccId['Classification'].')</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account2DepositAccId]" data-required="1" class="form-control account2DepositAccId" />
										<?php	}	?>
									</div>
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>