<?php
$clientcode	= $this->config->item('clientcode');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Mapping</span><i class="fa fa-circle"></i></li>
				<li><span>Payment Mapping</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-credit-card"></i>Payment Mapping</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Mapping</span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="">
						<table class="table table-hover text-centered actiontable" id="sample_4">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Payment Method</th>
									<th width="15%"><?php echo $this->globalConfig['account2Name'];?> Bank Account</th>
									<?php	if($clientcode == 'ecfqbo'){	?>
									<th width="15%"><?php echo $this->globalConfig['account2Name'];?> Payment Method</th>
									<?php	}	?>
									<th width="15%"> Applicable On</th>
									<th width="15%"> Channel Id</th>
									<th width="15%"> Currency</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Account</th>
									<th width="15%"><?php echo $this->globalConfig['account2Name'];?> Account</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="account1PaymentId"></span></td>
									<td><span class="value" data-value="account2PaymentId"></span></td>
									<?php	if($clientcode == 'ecfqbo'){	?>
									<td><span class="value" data-value="paymentValue"></span></td>
									<?php	}	?>
									<td><span class="value" data-value="applicableOn"></span></td>
									<td><span class="value" data-value="channelIds"></span></td>
									<td><span class="value" data-value="currency"></span></td>
									<td><span class="value" data-value="account1Id"></span></td>
									<td><span class="value" data-value="account2Id"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('mapping/payment/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	foreach ($data['data'] as $key => $row){	?>
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									
									<td><span class="value" data-value="account1PaymentId"><?php echo @($data['account1PaymentId'][$row['account1Id']][$row['account1PaymentId']])?($data['account1PaymentId'][$row['account1Id']][$row['account1PaymentId']]['name']):($row['account1PaymentId']);?></span></td>
									
									<td><span class="value" data-value="account2PaymentId"><?php echo @($data['account2PaymentId'][$row['account2Id']][$row['account2PaymentId']])?($data['account2PaymentId'][$row['account2Id']][$row['account2PaymentId']]['name']):($row['account2PaymentId']);?></span></td>
									
									<?php	if($clientcode == 'ecfqbo'){	?>
									<td><span class="value" data-value="paymentValue"><?php echo @($data['paymentValue'][$row['account2Id']][$row['paymentValue']])?($data['paymentValue'][$row['account2Id']][$row['paymentValue']]['name']):($row['paymentValue']);?></span></td>
									<?php	}	?>
									
									<td><span class="value" data-value="applicableOn"><?php echo @ucfirst($data['applicableOn'][$row['account2Id']][$row['applicableOn']])?ucfirst($data['applicableOn'][$row['account2Id']][$row['applicableOn']]['name']):ucfirst($row['applicableOn']);?></span></td>
									
									<td><span class="value" data-value="channelIds"><?php 
									$account1ChannelId = explode(',',$row['channelIds']);
									$channelName = array();
									if($account1ChannelId){
										foreach($account1ChannelId as $key => $account1Channel){
											$channelName[] = ucfirst(@$data['account1ChannelId'][$row['account1Id']][$account1Channel]['name']);
										}
									}
									if($channelName){
										echo implode(',',$channelName);
									}else{
										echo @($data['account1ChannelId'][$row['account1Id']][$row['channelIds']])?($data['account1ChannelId'][$row['account1Id']][$row['channelIds']]['name']):($row['channelIds']);
									} ?></span></td>
									
									<td><span class="value" data-value="currency"><?php echo @($data['currency'][$row['account2Id']][$row['currency']])?($data['currency'][$row['account2Id']][$row['currency']]['name']):($row['currency']);?></span></td>
									
									<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
									
									<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
									
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick='editAction(data<?php echo $row['id'];?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/payment/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Sales Payment Mapping</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('mapping/payment/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>You have some form errors. Please check below.
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Payment Method
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['account1PaymentId']){	?> 
										<select name="data[account1PaymentId]" data-required="1" class="form-control account1PaymentId acc1listoption">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Payment Method</option>
											<?php
													foreach ($data['account1PaymentId'] as $accountId => $account1PaymentIds) {
														echo '<option class="acc1listoption'.$accountId.'"  value="OTHER">OTHER</option>';
														foreach ($account1PaymentIds as $account1PaymentId) {
															echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1PaymentId['id'].'">'.ucwords($account1PaymentId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account1PaymentId]" data-required="1" class="form-control account1PaymentId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Bank Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php 
												$bankType	= array('Other Current Asset','Bank');
												if(@$data['account2PaymentId']){
										?> 
										<select name="data[account2PaymentId]" data-required="1" class="form-control account2PaymentId acc2listoption">
											<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> Bank Account</option>
											<?php
													foreach ($data['account2PaymentId'] as $accountId => $account2PaymentIds) {
														foreach ($account2PaymentIds as $account2PaymentId) {
															echo '<option class="acc2listoption'.$accountId.'" value="'.$account2PaymentId['id'].'">'.ucwords($account2PaymentId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account2PaymentId]" data-required="1" class="form-control account2PaymentId" />
										<?php	}	?>
									</div>
								</div>
								<?php	if($clientcode == 'ecfqbo'){	?>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Payment Method
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['paymentValue']){	?> 
										<select name="data[paymentValue]" data-required="1" class="form-control paymentValue acc2listoption">
											<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> Payment Method</option>
											<?php
													foreach ($data['paymentValue'] as $accountId => $account2PaymentIds) {
														foreach ($account2PaymentIds as $account2PaymentId) {
																echo '<option class="acc2listoption'.$accountId.'" value="'.$account2PaymentId['id'].'">'.ucwords($account2PaymentId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[paymentValue]" data-required="1" class="form-control paymentValue" />
										<?php	}	?>
									</div>
								</div>
								<?php	}	?>
								<div class="form-group">
									<label class="control-label col-md-4"> Applicable On
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['applicableOn']){	?> 
										<select name="data[applicableOn]" data-required="1" class="form-control applicableOn">
											<?php
													echo '<option value="">Select a applicable on</option>';
													foreach ($data['applicableOn'] as $key => $applicableOn) {
														echo '<option value="'.$key.'">'.ucwords($applicableOn).'</option>';
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[applicableOn]" data-required="1" class="form-control applicableOn" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Channel</label>
									<div class="col-md-7">
										<?php	if(@$data['account1ChannelId']){	?>
										<select name="data[channelIds][]" class="form-control acc1listoption channelIds" multiple="true">
												<?php
													foreach ($data['account1ChannelId'] as $accountId => $account1ChannelIds) {
													echo '<option value="">Select a Brightpearl Channel</option>';
													echo '<option value="blank">Blank Channel</option>';
														foreach ($account1ChannelIds as $account1ChannelId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ChannelId['id'].'">'.ucwords($account1ChannelId['name']).'</option>';
														}
													}
												?>
										</select>
											<?php	
												}
												else{
											?>
										<input type="text" name="data[channelIds]" class="form-control account1ChannelId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Currency</label>
									<div class="col-md-7">
										<?php	if(@$data['account1CurrencyId']){	?>
										<select name="data[currency][]" class="form-control acc1listoption currency" multiple="true">
												<?php
													foreach ($data['account1CurrencyId'] as $accountId => $account1CurrencyIds) {
													echo '<option value="">Select a Brightpearl Currency</option>';
														foreach ($account1CurrencyIds as $account1CurrencyId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1CurrencyId['code'].'">'.ucwords($account1CurrencyId['code']).'</option>';
														}
													}
												?>
										</select>
											<?php
												}
												else{
											?>
										<input type="text" name="data[currency]" class="form-control currency" />
										<?php	}	?>
									</div>
								</div>
								
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>