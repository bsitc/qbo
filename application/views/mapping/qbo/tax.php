<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Mapping</span><i class="fa fa-circle"></i></li>
				<li><span>Tax Mapping</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-calculator"></i>Tax Mapping</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Mapping </span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="">
						<table class="table table-hover text-centered actiontable" id="sample_4">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th width="8%">Brightpearl Tax</th>
									<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
										<th width="10%">Brightpearl Country</th>
										<th width="10%">Brightpearl State</th>
										<th width="10%">Brightpearl Channel</th>
									<?php	}	?>
									<th width="10%">QBO Sales Tax Item</th>
									<th width="10%">QBO Sales Tax Code</th>
									<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
									<th width="10%">QBO Sales Alternate Tax Item</th>
									<th width="10%">QBO Sales Alternate Tax Code</th>
									<?php	}	?>
									<th width="10%">Brightpearl Account</th>
									<th width="10%">QBO Account</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="account1TaxId"></span></td>
									<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
									<td><span class="value" data-value="countryName"></span></td>
									<td><span class="value" data-value="stateName"></span></td>
									<td><span class="value" data-value="account1ChannelId"></span></td>
									<?php	}	?>
									<td><span class="value" data-value="account2LineTaxId"></span></td>
									<td><span class="value" data-value="account2TaxId"></span></td>
									<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
									<td><span class="value" data-value="account2KidsLineTaxId"></span></td>
									<td><span class="value" data-value="account2KidsTaxId"></span></td>
									<?php	}	?>
									<td><span class="value" data-value="account1Id"></span></td>
									<td><span class="value" data-value="account2Id"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('mapping/tax/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	foreach ($data['data'] as $key =>  $row){	?> 
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									
									<td><span class="value" data-value="account1TaxId"><?php echo @($data['account1TaxId'][$row['account1Id']][$row['account1TaxId']])?($data['account1TaxId'][$row['account1Id']][$row['account1TaxId']]['name']):($row['account1TaxId']);?></span></td>
									
									<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
									<td><span class="value" data-value="countryName"><?php echo @($data['countryName'][$row['account1Id']][$row['countryName']])?($data['countryName'][$row['account1Id']][$row['countryName']]['name']):($row['countryName']);?></span></td>
									
									<td><span class="value" data-value="stateName"><?php echo @($data['stateName'][$row['account1Id']][$row['stateName']])?($data['stateName'][$row['account1Id']][$row['stateName']]['name']):($row['stateName']);?></span></td>
									
									<td>
										<span class="value" data-value="account1ChannelId">
										<?php
											$account1ChannelIdsName	= explode(",",trim($row['account1ChannelId']));
											foreach($account1ChannelIdsName as $account1ChannelIdName){
												echo @($data['account1ChannelId'][$row['account1Id']][$account1ChannelIdName])?($data['account1ChannelId'][$row['account1Id']][$account1ChannelIdName]['name']):($account1ChannelIdName);
												echo " ~ ";
											}
										?>
										</span>
									</td>
									<?php	}	?>
									<td><span class="value" data-value="account2LineTaxId"><?php echo @($data['account2TaxId'][$row['account2Id']][$row['account2LineTaxId']])?($data['account2TaxId'][$row['account2Id']][$row['account2LineTaxId']]['name']):($row['account2LineTaxId']);?></span></td>
									
									<td><span class="value" data-value="account2TaxId"><?php echo @($data['account2TaxId'][$row['account2Id']][$row['account2TaxId']])?($data['account2TaxId'][$row['account2Id']][$row['account2TaxId']]['name']):($row['account2TaxId']);?></span></td>
									<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
									<td><span class="value" data-value="account2KidsLineTaxId"><?php echo @($data['account2TaxId'][$row['account2Id']][$row['account2KidsLineTaxId']])?($data['account2TaxId'][$row['account2Id']][$row['account2KidsLineTaxId']]['name']):($row['account2KidsLineTaxId']);?></span></td> 
								
									<td><span class="value" data-value="account2KidsTaxId"><?php echo @($data['account2TaxId'][$row['account2Id']][$row['account2KidsTaxId']])?($data['account2TaxId'][$row['account2Id']][$row['account2KidsTaxId']]['name']):($row['account2KidsTaxId']);?></span></td>
									<?php	}	?>
									<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
									
									<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
									
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick='editAction(data<?php echo $row['id'];?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/tax/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr> 
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Tax Mapping</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('mapping/tax/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button> You have some form errors. Please check below.
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account2Id]"  data-required="1" class="form-control account2Id acc2list">
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Tax Code
										<span class="" aria-required="false"></span>
									</label>
									<div class="col-md-7">
									<?php	if(@$data['account1TaxId']){	?> 
										<select name="data[account1TaxId]" data-required="0" class="form-control account1TaxId acc1listoption">
											<option value="" class="show">Select a Brightpearl Tax Code</option>
											<?php
													foreach ($data['account1TaxId'] as $accountId => $account1TaxIds) {
														foreach ($account1TaxIds as $account1TaxId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1TaxId['id'].'">'.ucwords($account1TaxId['name']).'</option>';
														}
													}
											?>
										</select>
									<?php	}
											else{
									?>
										<input type="text" name="data[account1TaxId]" data-required="1" class="form-control account1TaxId" />
									<?php	}	?>
									</div>
								</div>
								<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Country</label>
									<div class="col-md-7"><input name="data[countryName]" class="form-control countryName" type="text"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl State</label>
									<div class="col-md-7"><input name="data[stateName]" class="form-control stateName" type="text"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Channel</label>
									<div class="col-md-7">
										<?php	if(@$data['account1ChannelId']){	?> 
										<select name="data[account1ChannelId][]" class="form-control acc1listoption account1ChannelId" multiple="true">
											<?php
													foreach ($data['account1ChannelId'] as $accountId => $account1ChannelIds) {
														echo '<option  class="acc1listoption'.$accountId.'" value="">Select a '.$this->globalConfig['account1Name'].' Channel Name</option>';
														echo '<option  class="acc1listoption'.$accountId.'" value="blank">Blank Channel</option>';
														foreach ($account1ChannelIds as $account1ChannelId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ChannelId['id'].'">'.ucwords($account1ChannelId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account1ChannelId][]" data-required="1" class="form-control account1ChannelId" />
									<?php		}
									?>
									</div>
								</div> 
								<?php	}	?>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Order Tax Name  
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['account2TaxId']){	?> 
										<select name="data[account2TaxId]" data-required="1" class="form-control account2TaxId acc2listoption">
											<option value="" class="show">Select a <?php echo $this->globalConfig['account2Name'];?> Order Tax Name</option>
											<?php
													foreach ($data['account2TaxId'] as $accountId => $account1TaxIds) {
														foreach ($account1TaxIds as $account2TaxId) {
															echo '<option class="acc2listoption'.$accountId.'" value="'.$account2TaxId['id'].'">'.ucwords($account2TaxId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account2TaxId]" data-required="1" class="form-control account2TaxId" />
										<?php	} ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Order Line Tax Name 
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['account2TaxId']){	?> 
										<select name="data[account2LineTaxId]" data-required="1" class="form-control account2LineTaxId acc2listoption">
											<option value="" class="show">Select a <?php echo $this->globalConfig['account2Name'];?>Order Line Tax Name</option>
											<?php
													foreach ($data['account2TaxId'] as $accountId => $account1TaxIds) {
														foreach ($account1TaxIds as $account2TaxId) {
															echo '<option class="acc2listoption'.$accountId.'" value="'.$account2TaxId['id'].'">'.ucwords($account2TaxId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account2LineTaxId]" data-required="1" class="form-control account2LineTaxId">
										<?php	}	?>
									</div>
								</div>
								<?php	if($this->globalConfig['enableAdvanceTaxMapping']){	?>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Alternate Order Tax Name  
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['account2TaxId']){	?> 
										<select name="data[account2KidsTaxId]"  class="form-control account2KidsTaxId acc2listoption">
											<option value="" class="show">Select a <?php echo $this->globalConfig['account2Name'];?> Order Tax Name</option>
											<?php
													foreach ($data['account2TaxId'] as $accountId => $account1TaxIds) {
														foreach ($account1TaxIds as $account2TaxId) {
															echo '<option class="acc2listoption'.$accountId.'" value="'.$account2TaxId['id'].'">'.ucwords($account2TaxId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account2KidsTaxId]" class="form-control account2KidsTaxId" />
										<?php	} ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Alternate Order Line Tax Name 
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<?php	if(@$data['account2TaxId']){	?> 
										<select name="data[account2KidsLineTaxId]" class="form-control account2KidsLineTaxId acc2listoption">
											<option value="" class="show">Select a <?php echo $this->globalConfig['account2Name'];?>Order Line Tax Name</option>
											<?php
													foreach ($data['account2TaxId'] as $accountId => $account1TaxIds) {
														foreach ($account1TaxIds as $account2TaxId) {
															echo '<option class="acc2listoption'.$accountId.'" value="'.$account2TaxId['id'].'">'.ucwords($account2TaxId['name']).'</option>';
														}
													}
											?>
										</select>
										<?php	}
												else{
										?>
										<input type="text" name="data[account2KidsLineTaxId]"  class="form-control account2KidsLineTaxId">
										<?php	}	?>
									</div>
								</div>
								<?php	}	?>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>