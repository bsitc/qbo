<!--qbo-->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Purchase Order</span><i class="fa fa-circle"></i></li>
				<li><span>Purchase Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cart-plus"></i><?php echo ucwords($this->globalConfig['fetchPurchaseOrder']);?> Purchase Order Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($purchaseInfo['rowData'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cart-plus"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> Purchase Order Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($purchaseInfo['createdRowData'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-credit-card"></i>Payment Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($purchaseInfo['paymentDetails'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>