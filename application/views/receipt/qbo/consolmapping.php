<?php
$enableCOGSJournals	= $this->globalConfig['enableCOGSJournals'];
$userLoginData		= $this->session->userdata('login_user_data');
$accessRoles		= array('admin', 'developer', '1');
?>

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Consolidation</span><i class="fa fa-circle"></i></li>
				<li><span>Refund Receipt Consolidation Mappings</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-link"></i>Refund Receipt Consolidation Mappings</div>
				<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Mapping</span>
					</a>
				</div>
				<?php	}	?>
			</div>
			<div class="portlet-body">
				<div class="table-container">
				<div class="">
						<table class="table table-hover text-centered actiontable" id="sample_4">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th width="15%">Brightpearl Channel</th>
									<th width="15%">Brightpearl Payment Method</th>
									<th width="15%">Brightpearl Currency</th>
									<th width="15%">QBO Customer ID</th>
									<th width="15%">DocNumber Ref</th>
									<th width="15%">Consolidation Frequency</th>
									<th width="15%">Brightpearl Account</th>
									<th width="15%">QBO Account</th>
									<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
									<th width="5%">Action</th>
									<?php	}	?>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="account1ChannelId"></span></td>
									<td><span class="value" data-value="account1PaymentId"></span></td>
									<td><span class="value" data-value="account1CurrencyId"></span></td>
									<td><span class="value" data-value="account2ChannelId"></span></td>
									<td><span class="value" data-value="uniqueChannelName"></span></td>
									<td><span class="value" data-value="consolFrequency"></span></td>
									<td><span class="value" data-value="account1Id"></span></td>
									<td><span class="value" data-value="account2Id"></span></td>
									<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('receipt/consolmapping/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
									<?php	}	?>
								</tr>
								<?php	foreach($data['data'] as $key =>  $row){	?>
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									
									<td><span class="value" data-value="account1ChannelId"><?php echo @($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']])?($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']]['name']):($row['account1ChannelId']);?></span></td>
									
									<td><span class="value" data-value="account1PaymentId"><?php echo @($data['account1PaymentId'][$row['account1Id']][$row['account1PaymentId']])?($data['account1PaymentId'][$row['account1Id']][$row['account1PaymentId']]['name']):($row['account1PaymentId']);?></span></td>
									
									<?php	$currencyInView	= ($data['account1CurrencyId'][$row['account1Id']][$row['account1CurrencyId']])?($data['account1CurrencyId'][$row['account1Id']][$row['account1CurrencyId']]['name']):($row['account1CurrencyId']);
											if($currencyInView == 'bpAccountingCurrency'){
												$currencyInView	= 'BrightPearl Home Currency';
											}
									?>
									<td><span class="value" data-value="account1CurrencyId"><?php echo $currencyInView;?></span></td>
									
									<td><span class="value" data-value="account2ChannelId"><?php echo @($data['account2ChannelId'][$row['account2Id']][$row['account2ChannelId']])?($data['account2ChannelId'][$row['account2Id']][$row['account2ChannelId']]['name']):($row['account2ChannelId']);?></span></td>
									
									<td><span class="value" data-value="uniqueChannelName"><?php echo @($data['uniqueChannelName'][$row['account2Id']][$row['uniqueChannelName']])?($data['uniqueChannelName'][$row['account2Id']][$row['uniqueChannelName']]['name']):($row['uniqueChannelName']);?></span></td>
									
									<?php
										$consolFrequency	= ($data['consolFrequency'][$row['account2Id']][$row['consolFrequency']])?($data['consolFrequency'][$row['account2Id']][$row['consolFrequency']]['name']):($row['consolFrequency']);
										if($consolFrequency	== 1){
											$consolFrequency	= 'Daily';
										}
										else{
											$consolFrequency	= 'Monthly';
										}
									?>
									
									<td><span class="value" data-value="consolFrequency"><?php echo $consolFrequency; ?></span></td>
									
									<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
	
									<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
									
									<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick='editAction(data<?php echo $row['id'];?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('receipt/consolmapping/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
									<?php	}	?>
								</tr> 
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Consolidation Mapping</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('receipt/consolmapping/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>You have some form errors. Please check below
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<option value="">Select a Brightpearl Account</option>
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">QBO Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<select name="data[account2Id]"  data-required="1" class="form-control account2Id acc2list">
											<option value="">Select a QBO Account</option>
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Channel<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account1ChannelId']){	?>
										<select name="data[account1ChannelId]" data-required="1" class="form-control acc1listoption account1ChannelId">
											<option value="">Select a Brightpearl Channel</option>
												<?php
													foreach ($data['account1ChannelId'] as $accountId => $account1ChannelIds) {
														foreach ($account1ChannelIds as $account1ChannelId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ChannelId['id'].'">'.ucwords($account1ChannelId['name']).'</option>';
														}
													}
												?>
										</select>
											<?php	
												}
												else{
											?>
										<input type="text" name="data[account1ChannelId]" data-required="1" class="form-control account1ChannelId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Payment Method<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account1PaymentId']){	?>
										<select name="data[account1PaymentId]" data-required="1" class="form-control acc1listoption account1PaymentId">
											<option value="">Select a Brightpearl Payment Method</option>
												<?php
													foreach ($data['account1PaymentId'] as $accountId => $account1PaymentIds) {
														foreach ($account1PaymentIds as $account1PaymentId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1PaymentId['id'].'">'.ucwords($account1PaymentId['name']).'</option>';
														}
													}
												?>
										</select>
											<?php	
												}
												else{
											?>
										<input type="text" name="data[account1PaymentId]" data-required="1" class="form-control account1PaymentId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Brightpearl Order Currency<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account1CurrencyId']){	?>
										<select name="data[account1CurrencyId]" data-required="1" class="form-control acc1listoption account1CurrencyId">
												<?php
													foreach ($data['account1CurrencyId'] as $accountId => $account1ChannelIds) {
														echo '<option class="acc1listoption'.$accountId.'" value="">Select</option>';
														echo '<option class="acc1listoption'.$accountId.'" value="bpAccountingCurrency">Brightpearl Home Currency</option>';
														foreach ($account1ChannelIds as $account1CurrencyId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1CurrencyId['code'].'">'.ucwords($account1CurrencyId['code']).'</option>';
														}
													}
												?>
										</select>
											<?php
												}
												else{
											?>
										<input type="text" name="data[account1ChannelId]" data-required="1" class="form-control account1ChannelId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group ">
									<label class="control-label col-md-4">QBO Customer ID<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">                                    
										<input type="text" name="data[account2ChannelId]" data-required="1" class="form-control account2ChannelId" placeholder="Enter QBO Customer ID">
									</div>
								</div>
								<div class="form-group ">
									<label class="control-label col-md-4">DocNumber For Invoice<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<input type="text" name="data[uniqueChannelName]" data-required="1" class="form-control uniqueChannelName" maxlength="4" placeholder="Enter DocNumber for QBO Invoice">
										<span class="help-block"> (Maximum Length allowed is 4)</span>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Consolidation Frequency<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<select name="data[consolFrequency]" data-required="0" class="form-control consolFrequency">
											<option value="1">Daily</option>
											<option value="2">Monthly</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Tax as Line Item<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<select name="data[SendTaxAsLine]" data-required="1" class="form-control SendTaxAsLine">
											<option value="" id="none">Select</option>
											<option value="1" id="yes1">Yes</option>
											<option value="0" id="no1">No</option>
										</select>
									</div>
								</div>
								<div class="form-group ">
									<label class="control-label col-md-4">Default Tax Item<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<input type="text" name="data[defaultTaxItem]" data-required="1" class="form-control defaultTaxItem" placeholder="Enter QBO Tax Item ID">
									</div>
								</div>
								<div class="form-group ">
									<label class="control-label col-md-4">Default Consolidation Item<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<input type="text" name="data[AggregationItem]" data-required="1" class="form-control AggregationItem" placeholder="Enter QBO Consolidation Item ID">
									</div>
								</div>
								<?php 	if($enableCOGSJournals){	?>
								<div class="form-group ">
									<label class="control-label col-md-4">Post Options<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<select name="data[postOtions]" data-required="1" class="form-control postOtions">
											<option value="0">NA</option>
											<option value="1">COGS Only</option>
											<option value="2">Sales Only</option>
										</select>
									</div>
								</div>
								<?php	}								?>
								<div class="form-group ">
									<label class="control-label col-md-4">QBO Order Currency<span class="required" aria-required="false"> * </span></label>
									<div class="col-md-7">
										<select name="data[orderForceCurrency]" data-required="1" class="form-control orderForceCurrency">
											<option value="0">Order Currency</option>
											<option value="1">Brightpearl Home Currency</option>
										</select>
									</div>
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>