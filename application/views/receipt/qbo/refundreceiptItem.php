<?php
	$status			= array('0' => 'Pending', '1' => 'Sent', '4' => 'Archive');
	$statusColor	= array('0' => 'default', '1' => 'success', '4' => 'danger');
	$saveOrderInfo	= json_decode($orderInfo['rowData'],true);
	$addressInfo	= $saveOrderInfo['parties'];
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Receipt</span><i class="fa fa-circle"></i></li>
				<li><span>Refund Receipt</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light portlet-fit portlet-datatable bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-truck font-dark"></i>
							<span class="caption-subject font-dark sbold uppercase">Order ID - <?php echo ($orderInfo['orderId'])?($orderInfo['orderId']):($orderInfo['orderNo']);?>
								<span class="hidden-xs">| <?php echo ($orderInfo['created'])?(date('M d, Y H:i:s',strtotime($orderInfo['created']))):('');?></span>
							</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="tabbable-line">
							<ul class="nav nav-tabs nav-tabs-lg"><li class="active"><a href="#tab_1" data-toggle="tab">Details</a></li></ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption"><i class="fa fa-file-text-o"></i>Order Details</div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-5 name">Order ID :</div>
														<div class="col-md-7 value"> <?php echo ($orderInfo['orderId'])?($orderInfo['orderId']):($orderInfo['orderNo']);?></div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name">Order Date & Time :</div>
														<div class="col-md-7 value"> <?php echo ($orderInfo['created'])?(date('M d, Y H:i:s',strtotime($orderInfo['created']))):('');?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name">Order Status :</div>
														<div class="col-md-7 value">
															 <?php echo '<span class="label label-sm label-' . @$statusColor[$orderInfo['status']] . '">' . @$status[$orderInfo['status']] . '</span>';?></div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name">Total :</div>
														<div class="col-md-7 value"> <?php echo sprintf("%.2f",$orderInfo['totalAmount']);?></div>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption"><i class="fa fa-user"></i>Customer Information</div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-5 name">Customer Name :</div>
														<div class="col-md-7 value"><?php echo $saveOrderInfo['parties']['customer']['addressFullName'];?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name">Email :</div>
														<div class="col-md-7 value"><?php echo $saveOrderInfo['parties']['customer']['email'];?></div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name">State :</div>
														<div class="col-md-7 value"><?php echo $saveOrderInfo['parties']['customer']['addressLine4'];?></div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name">Phone Number :</div>
														<div class="col-md-7 value"><?php echo $saveOrderInfo['parties']['customer']['telephone'];?> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption"><i class="fa fa-money"></i>Billing Address</div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-12 value"><?php echo $addressInfo['billing']['addressFullName'];?>
															<br><?php echo $addressInfo['billing']['addressLine1'];?>
															<br><?php echo $addressInfo['billing']['addressLine2'];?>
															<br><?php echo $addressInfo['billing']['addressLine3'] .', '.$addressInfo['billing']['addressLine4'] .' ' .$addressInfo['billing']['postalCode'];?>
															<br><?php echo $addressInfo['billing']['country'];?>
															<br> 
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption"><i class="fa fa-truck"></i>Shipping Address</div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-12 value"> <?php echo $addressInfo['delivery']['addressFullName'];?>
															<br><?php echo $addressInfo['delivery']['addressLine1'];?>
															<br><?php echo $addressInfo['delivery']['addressLine2'];?>
															<br><?php echo $addressInfo['delivery']['addressLine3'] .', '.$addressInfo['delivery']['addressLine4'] .' ' .$addressInfo['delivery']['postalCode'];?>
															<br><?php echo $addressInfo['delivery']['country'];?>
															<br>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title"><div class="caption"><i class="fa fa-shopping-cart"></i>Shopping Cart</div></div>
												<div class="portlet-body">
												<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th>S.No</th>
																	<th>Order Row ID</th>
																	<th>Product ID</th>
																	<th>Product SKU</th>
																	<th>Size</th>
																	<th>Quantity</th>
																	<th>Price</th>
																</tr>
															</thead>
															<tbody>
																<?php
																	if($items){
																		foreach($items as $i => $item){
																?>
																<tr>
																	<td><?php echo $i + 1; ?></td>
																	<td><a href="javascript:;"><?php echo $item['rowId'];?></a></td>
																	<td><?php echo $item['productId'];?></td>
																	<td><?php echo $item['sku'];?></td>
																	<td><?php echo $item['size'];?></td>
																	<td><?php echo $item['qty'];?></td>
																	<td><?php echo sprintf("%.2f",$item['price']);?></td>
																</tr>
																<?php	}
																	}
																?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>