<?php
	//qbo
	$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
	$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
	$userLoginData			= $this->session->userdata('login_user_data');
	$accessRoles			= array('admin', 'developer', '1', 'testing');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Sales Order</span><i class="fa fa-circle"></i></li>
				<li><span>Sales Credit</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-truck"></i>Sales Credit Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('sales/credit/fetchSalesCredit');?>" class="btn btn-circle btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch Sales Credit</span>
							</a>
							<?php	if(in_array($userLoginData['role'], $accessRoles)){		?>
							<a href="" class="btn btn-circle btnactionsubmit" onclick="confirmAction()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post Sales Credit</span>
							</a>
							<?php		if($this->globalConfig['enableAggregation']){	?>
							<a href="" class="btn btn-circle btnactionsubmit" onclick="confirmAction2()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post Consolidation Sales Credit</span>
							</a>
							<?php			if($this->globalConfig['enableNetOffConsol']){	?>
							<a href="" class="btn btn-circle btnactionsubmit" onclick="confirmAction3()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post NetOff Consolidation</span>
							</a>
							<?php			}	?>
							<?php		}	?>
							<?php	}	?>
							<a href="<?php echo base_url('sales/credit/exportSalesCredit?');?>" class="btn btn-circle btn-danger exportBtn">
								<i class="fa fa-file-excel-o"></i>
								<span class="hidden-xs">Export Report in CSV</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<?php	if(in_array($userLoginData['role'], $accessRoles)){		?>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="3">Payment Created</option>
									<option value="4">Archive</option>
									<option value="5">Post</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
								<?php	}	?>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"/></th>
										<th width="10%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Account</th>
										<th width="10%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Account</th>
										<th width="5%"><?php echo ucwords($this->globalConfig['fetchSalesCredit']);?>&nbsp;ID</th>
										<th width="10%">Invoice Ref</th>
										<th width="10%"><?php echo ucwords($this->globalConfig['postSalesCredit']);?>&nbsp;ID</th>
										<th width="15%">Channel</th>
										<th width="10%">Customer</th>
										<th width="10%">Email</th>
										<th width="15%">TaxDate</th>
										<th width="15%">Updated</th>
										<th width="15%">IsConsol</th>
										<th width="10%">Status</th>
										<th width="10%">OrderInfo</th>
										<th width="10%">Message</th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td>
											<select name="account1Id" class="form-control form-filter input-sm account1Id">
												<option value="">Select an option</option>
												<?php
													foreach($account1MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td>
											<select name="account2Id" class="form-control form-filter input-sm account2Id">
												<option value="">Select an option</option>
												<?php
													foreach($account2MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm orderId" name="orderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm bpInvoiceNumber" name="bpInvoiceNumber" /></td>
										<td><input type="text" class="form-control form-filter input-sm createOrderId" name="createOrderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm channelName" name="channelName" /></td>
										<td><input type="text" class="form-control form-filter input-sm delAddressName" name="delAddressName" /></td>
										<td><input type="text" class="form-control form-filter input-sm customerEmail" name="customerEmail" /></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm taxDate_from" readonly name="taxDate_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm taxDate_to" readonly name="taxDate_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm updated_from" readonly name="updated_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm updated_to" readonly name="updated_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="sendInAggregation" class="form-control form-filter input-sm sendInAggregation">
												<option value="">Select...</option>
												<option value="1">Yes</option>
												<option value="0">No</option>
											</select>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm status">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
												<option value="3">Payment Created</option>
												<option value="4">Archive</option>
											</select>
										</td>
										<td>
											<select name="OrderInfo" class="form-control form-filter input-sm OrderInfo">
												<option value="">Select...</option>
												<option value="1">Payment initiated in Brightpearl</option>
												<option value="2">Payment initiated in QBO</option>
												<option value="3">Order Uninvoiced on Brightpearl</option>
												<option value="4">Invoice Void On QBO</option>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm message" name="message" /></td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-refresh"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
		echo '<script type="text/javascript"> jsOrder =  [ 1, "desc" ];</script>';
	}
?>
<script type="text/javascript">
	function confirmAction(){
		var answer = confirm("Do you want to send all the Orders?");
		if(answer == true){
			$.ajax({
				url			: "<?php echo base_url('sales/credit/postSalesCredit');?>",
				type		: "POST",
				dataType	: "json",
				success		: function(){}
			})
		}
	}
	function confirmAction2(){
		var answer = confirm("Do you want to send all the Orders?");
		if(answer == true){
			$.ajax({
				url			: "<?php echo base_url('sales/credit/postaggregationSalescredit');?>",
				type		: "POST",
				dataType	: "json",
				success		: function(){}
			})
		}
	}
	function confirmAction3(){
		var answer = confirm("Do you want to send NetOff Consolidation?");
		if(answer == true){
			$.ajax({
				url			: "<?php echo base_url('sales/credit/postNetOffConsolOrder');?>",
				type		: "POST",
				dataType	: "json",
				success		: function(){}
			})
		}
	}
	loadUrl	= '<?php echo base_url('sales/credit/getCredit');?>';
	jQuery(".exportBtn").on("click",function(e){
		e.preventDefault();
		url	= jQuery(this).attr('href')+'account1Id='+jQuery(".account1Id").val()+'&account2Id='+jQuery(".account2Id").val()+'&orderId='+jQuery(".orderId").val()+'&bpInvoiceNumber='+jQuery(".bpInvoiceNumber").val()+'&createOrderId='+jQuery(".createOrderId").val()+'&channelName='+jQuery(".channelName").val()+'&delAddressName='+jQuery(".delAddressName").val()+'&customerEmail='+jQuery(".customerEmail").val()+'&taxDate_from='+jQuery(".taxDate_from").val()+'&taxDate_to='+jQuery(".taxDate_to").val()+'&status='+jQuery(".status").val()+'&OrderInfo='+jQuery(".OrderInfo").val();
		window.location.href	= url;
	})
</script>
<style>
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
		top: 28px !important;
	}
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
		top: 28px !important;
	}
</style>