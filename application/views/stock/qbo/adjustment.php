<?php
//qbo
$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
$allBPWarehouse			= $this->brightpearl->getAllLocation();
if($allBPWarehouse){
	foreach($allBPWarehouse[1] as $allBPWarehouses){
		$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
	}
}
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Stock Details</span><i class="fa fa-circle"></i></li>
				<li><span>Stock Adjustment</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cubes"></i>Stock Adjustments Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('stock/adjustment/fetchStockAdjustment');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch Stock Adjustments</span>
							</a>
							<a href="<?php echo base_url('stock/adjustment/postStockAdjustment');?>" class="btn btn-circle green-meadow btnactionsubmit hide">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post Stock Adjustments</span>
							</a>
							<a href="<?php echo base_url('stock/adjustment/exportStocks?');?>" class="btn btn-circle btn-danger exportBtn">
								<i class="fa fa-file-excel-o"></i>
								<span class="hidden-xs">Export Report in CSV</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="3">Error</option>
									<option value="4">Archive</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"></th>
										<th width="10%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Account</th>
										<th width="5%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Account</th>
										<th width="8%">GoodsMovementId</th>
										<th width="8%">GoodsNoteId</th>
										<th width="8%">StockTransferId</th>
										<th width="8%">QBO ID</th>
										<th width="10%">Warehouse</th>
										<th width="8%">Product ID</th>
										<th width="12%">SKU</th>
										<th width="5%">Quantity updated</th>
										<th width="8%">Price</th>
										<th width="8%">Value</th>
										<th width="12%">Type</th>
										<th width="10%">Created</th>
										<th width="10%">Status</th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td>
											<select name="account1Id" class="form-control form-filter input-sm account1Id">
												<option value="">Select an option</option>
												<?php
													foreach($account1MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td>
											<select name="account2Id" class="form-control form-filter input-sm account2Id">
												<option value="">Select an option</option>
												<?php
													foreach($account2MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm orderId" name="orderId"></td>
										<td><input type="text" class="form-control form-filter input-sm goodsNoteId" name="goodsNoteId"></td>
										<td><input type="text" class="form-control form-filter input-sm ActivityId" name="ActivityId" /></td>
										<td><input type="text" class="form-control form-filter input-sm createdOrderId" name="createdOrderId" /></td>
										<td>
											<select name="warehouseId" class="form-control form-filter input-sm warehouseId">
												<option value="">Select Warehouse</option>
												<?php
													foreach($allMappedWarehouseName as $WarehouseId => $allBPWarehouses){
														echo '<option value="'.$WarehouseId.'">'.$allBPWarehouses.'</option>';
													}
												?>
											</select>										
										</td>
										<td><input type="text" class="form-control form-filter input-sm productId" name="productId"></td>
										<td><input type="text" class="form-control form-filter input-sm sku" name="sku"></td>
										<td><input type="text" class="form-control form-filter input-sm qty" name="qty"></td>
										<td><input type="text" class="form-control form-filter input-sm price" name="price"></td>
										<td><input type="text" class="form-control form-filter input-sm TotalValue" name="TotalValue"></td>
										<td>
											<select name="GoodNotetype" class="form-control form-filter input-sm GoodNotetype">
												<option value="">Select Type</option>
												<?php
													echo '<option value="GO">Inventory Transfer</option>';
													echo '<option value="SC">Inventory Adjustment</option>';
												?>
											</select>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm updated_from" readonly name="updated_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm updated_to" readonly name="updated_to " placeholder="To" />	
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm status">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
												<option value="3">Error</option>
												<option value="4">Archive</option>
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<?php
	if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
		echo '<script type="text/javascript"> jsOrder = [ 1, "desc" ];</script>';
	}
?>
<script>
	loadUrl	=  '<?php echo base_url('stock/adjustment/getAdjustment');?>';
	
	jQuery(".exportBtn").on("click",function(e){
		e.preventDefault();
		url	= jQuery(this).attr('href')+'createdOrderId='+jQuery(".createdOrderId").val()+'&account1Id='+jQuery(".account1Id").val()+'&account2Id='+jQuery(".account2Id").val()+'&orderId='+jQuery(".orderId").val()+'&ActivityId='+jQuery(".ActivityId").val()+'&warehouseId='+jQuery(".warehouseId").val()+'&productId='+jQuery(".productId").val()+'&qty='+jQuery(".qty").val()+'&price='+jQuery(".price").val()+'&GoodNotetype='+jQuery(".GoodNotetype").val()+'&updated_from='+jQuery(".updated_from").val()+'&updated_to='+jQuery(".updated_to").val()+'&status='+jQuery(".status").val()+'&sku='+jQuery(".sku").val()+'&goodsNoteId='+jQuery(".goodsNoteId").val();
		window.location.href	= url;
	})
</script>
<style>
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
		top: 28px !important;
	}
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
		top: 28px !important;
	}
</style>