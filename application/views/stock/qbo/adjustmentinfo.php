<?php
//qbo
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Stock Details</span><i class="fa fa-circle"></i></li>
				<li><span>Stock Adjustment Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cubes"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> Stock Adjustment Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($salesInfo['rowData'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cubes"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> Stock Adjustment Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								if($salesInfo['GoodNotetype'] == 'GO'){
									$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'stockAdjustment'. DIRECTORY_SEPARATOR . $salesInfo['account2Id']. DIRECTORY_SEPARATOR .'GO'. DIRECTORY_SEPARATOR;
									$rowDataFilePath	= $rowDataFilePath.$salesInfo['ActivityId'].'.json';
									$createdRowData		= file_get_contents($rowDataFilePath);
								}
								else{
									$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'stockAdjustment'. DIRECTORY_SEPARATOR . $salesInfo['account2Id']. DIRECTORY_SEPARATOR .'SA'. DIRECTORY_SEPARATOR;
									$rowDataFilePath	= $rowDataFilePath.$salesInfo['orderId'].'.json';
									$createdRowData		= file_get_contents($rowDataFilePath);
								}
								echo "<pre>";
								print_r(json_decode($createdRowData,true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>