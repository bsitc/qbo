<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Stock Sync Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-shopping-cart"></i>Stock Sync Listing </div>
						<div class="actions">
							<a href="<?php echo base_url('stock/sync/fetchSync');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs"> Fetch Stock </span>
							</a>
							<a href="<?php echo base_url('stock/sync/postSync');?>" class="btn btn-circle green-meadow btnactionsubmit">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs"> Post Stock </span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="2">Updated</option>
									<option value="3">Error</option>
									<option value="4">Archive</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead> 
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="15%"> Location Id </th>
										<th width="15%"> Product&nbsp;SKU </th>
										<th width="10%"> <?php echo ucwords($this->globalConfig['account1Name']);?> Qty </th>
										<th width="10%"> <?php echo ucwords($this->globalConfig['account2Name']);?> Qty </th>
										<th width="10%"> Adjusted Qty </th>
										<th width="15%"> Date&nbsp;Updated </th>
										<th width="10%"> Status </th>
										<th width="10%"> Actions </th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td><input type="text" class="form-control form-filter input-sm" name="account1WarehouseId"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="sku"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="account1StockQty"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="account2StockQty"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="adjustmentQty"></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
												<option value="2">Updated</option>
												<option value="3">Error</option>
												<option value="4">Archive</option>
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
		echo '<script type="text/javascript"> jsOrder =  [ 1, "desc" ];</script>';
	}
?>
<script type="text/javascript">
	loadUrl	= '<?php echo base_url('stock/sync/getSync'); ?>';
</script>

