<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Stock Sync Logs</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-shopping-cart"></i>Stock Sync Logs Listing </div>
						<div class="actions hide">
							<a href="<?php echo base_url('stock/synclog/fetchSynclog');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs"> Fetch Stock Sync </span>
							</a>
							<a href="<?php echo base_url('stock/synclog/postSynclog');?>" class="btn btn-circle green-meadow btnactionsubmit">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs"> Post Stock Sync </span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="15%">Location Id </th>
										<th width="10%">Name </th>
										<th width="10%">Product&nbsp;SKU </th>
										<th width="10%">Color </th>
										<th width="10%">size </th>
										<th width="10%">Adjusted Qty </th>
										<th width="10%">Order No </th>
										<th width="10%">Date&nbsp;Updated </th>
										<th width="10%">Actions </th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td><input type="text" class="form-control form-filter input-sm" name="account2WarehouseId"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="name"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="sku"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="color"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="size"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="adjustmentQty"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderNo"> </td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
		echo '<script type="text/javascript"> jsOrder = [ 1, "desc" ];</script>';
	}
?>
<script type="text/javascript">
	loadUrl	= '<?php echo base_url('stock/synclog/getSynclog'); ?>';
</script>