<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Stock Details</span><i class="fa fa-circle"></i></li>
				<li><span>Stock Adjustment Journal Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cubes"></i><?php echo ucwords($this->globalConfig['fetchStockJournal']);?> Stock Adjustment Journal Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>";print_r(json_decode($stockjournalinfo['params'],true));echo "</pre>";	?>
						</div>
					</div>
				</div>
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cubes"></i><?php echo ucwords($this->globalConfig['postStockJournal']);?> Stock Adjustment Journal Information
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>";print_r(json_decode($stockjournalinfo['createdParams'],true));echo "</pre>";	?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>