<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
                <li><span>Tax Customisation</span><i class="fa fa-circle"></i></li>
                <li><span>Tax Customisation Configuration</span></li>
            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Tax Customisation Configuration</div>
                <div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs">Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Brightpearl ID</th>
                                    <th width="25%">Brightpearl Account Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"></span></td>
                                    <td><span class="value" data-value="name"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('/taxupdate/taxupdate/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php	foreach($data['data'] as $key =>  $row){	?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"><?php echo $row['brightpearlAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('/taxupdate/taxupdate/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php	}	?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog  modal-lg">        
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Tax Customisation Settings</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('/taxupdate/taxupdate/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>  
							<input type="hidden" name="data[id]" class="id" />
						</form>                         
					</div>				
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>
<div class="confighml">
<?php   
    $data['data']	= ($data['data']) ? ($data['data']) : (array(''));
    foreach($data['data'] as $key =>  $row){	
?>
	<div class="htmlaccount<?php echo $row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button> You have some form errors. Please check below.
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Brightpearl ID<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-7">
				<select name="data[brightpearlAccountId]" data-required="1" class="form-control brightpearlAccountId">
					<option value="">Select A Brightpearl Account</option>
					<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Channel</label>
			<div class="col-md-7">
				<select name="data[channelIds][]"  class="form-control channelIds"  multiple="true">
					<option value="">Select Channel</option>
					<?php
						$channels	= reset($data['channel']);
						foreach ($channels as $channel) {
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Country</label>
			<div class="col-md-7">
				<input name="data[countries]" class="form-control countries" type="text" placeholder="Enter Alpha-3 Country Code" />
				<span class="help-block">(Hint :- Enter Multiple Country Code with `,` Saparation. Space Not Allowed.)</span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Update Sales Order Status</label>
			<div class="col-md-7">
				<select name="data[SetSalesOrderStatus]" class="form-control SetSalesOrderStatus">
					<option value="">Select Sales Order Status</option>
					<?php
						foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
							if($orderstatus['orderTypeCode'] == 'SO'){
								echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
							}
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Update Sales Credit Status</label>
			<div class="col-md-7">
				<select name="data[SetSalesCreditStatus]" class="form-control SetSalesCreditStatus">
					<option value="">Select Sales Credit Status</option>
					<?php
						foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
							if($orderstatus['orderTypeCode'] == 'SC'){
								echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
							}
						}
					?>
				</select> 
			</div>
		</div>
	</div> 
<?php
	}
?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
	$(".chosen-select").chosen({width: "100%"}); 
	jQuery(".actioneditbtn").on("click",function(){
		$(".chosen-select").trigger("liszt:updated");
	});
</script>