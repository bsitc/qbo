<?php
$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
$userLoginData			= $this->session->userdata('login_user_data');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Tax Customisation</span><i class="fa fa-circle"></i></li>
                <li><span>Tax Customised Sales/Credit</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-shopping-cart"></i>Sales/Credit Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('taxupdate/updatedsales/fetchAllSalesForUpdate');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch Sales/Credit For Update</span>
							</a>
							<?php	if($userLoginData['role'] == 'admin'){	?>
								<a href="<?php echo base_url('taxupdate/updatedsales/postAllUpdatedSales');?>" class="btn btn-circle btn-info btnactionsubmit">
									<i class="fa fa-download"></i>
									<span class="hidden-xs">Post Updated Sales/Credit</span>
								</a>
							<?php	}	?> 
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<?php	if($userLoginData['role'] == 'admin'){	?>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
								<?php	}	?> 
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="10%">Brightpearl Account</th>
										<th width="10%">Type</th>
										<th width="10%">Brightpearl ID</th>
										<th width="10%">Customer Ref</th>
										<th width="15%">Channel</th>
										<th width="10%">Country</th>
										<th width="15%">Created</th>
										<th width="10%">Status</th>
										<th width="10%">Message</th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td>
											<select name="account1Id" class="form-control form-filter input-sm">
												<option value="">Select an option</option>
												<?php
													foreach($account1MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td>
											<select name="orderType" class="form-control form-filter input-sm">
												<option value="SO">Sales Order</option>
												<option value="SC">Sales Credit</option>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="reference" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="channelName" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="Country" /></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="created_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="created_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
											</select>
										</td>
										<td></td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>		
<script type="text/javascript">
	loadUrl	= '<?php echo base_url('taxupdate/updatedsales/getupdatedsales');?>';
</script>
<style>
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
		top: 28px !important;
	}
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
		top: 28px !important;
	}
</style>