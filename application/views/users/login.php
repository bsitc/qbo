<?php
	$global_config = $this->session->userdata('global_config');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo $global_config['app_name'];?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        
		<!--
		<link href="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		-->
		
		
        <link href="<?php echo $this->config->item('script_url');?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo $this->config->item('script_url');?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->config->item('script_url');?>assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
		<link href="<?php echo $this->config->item('base_url').'extracss/2extracss.css' ?>" rel="stylesheet" type="text/css" />
	</head>
    <body class=" login">
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 login-container bs-reset">
					<div class="row">
						<div class="col-md-4"><img width="85%" class="login-logo login-6" src="<?php echo $this->config->item('base_url').'/logos/bsitc.png';?>" /></div>
						<div class="col-md-4"></div>
						<div class="col-md-4"></div>
					</div>
                    <div class="login-content">
                        <h1><?php echo $global_config['app_name'];?> Admin Login</h1>
                        <form action="javascript:;" class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Enter any username and password. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" id="inputEmail" type="text" autocomplete="off" placeholder="Username" name="username" required/>
								</div>
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" id="inputPassword" type="password" autocomplete="off" placeholder="Password" name="password" required/>
								</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox" name="remember" value="1" /> Remember me
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="forgot-password hide">
                                        <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                                    </div>
                                    <button class="btn blue signInbtn" type="submit">Sign In</button>
                                </div>
                            </div>
                        </form>
                        <form class="forget-form hide" action="javascript:;" method="post">
                            <h3>Forgot Password ?</h3>
                            <p> Enter your e-mail address below to reset your password. </p>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
							</div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="btn blue btn-outline">Back</button>
                                <button type="submit" class="btn blue uppercase pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                                <ul class="login-social">
                                    <li><a href="javascript:;"><i class="icon-social-facebook"></i></a></li>
                                    <li><a href="javascript:;"><i class="icon-social-twitter"></i></a></li>
                                    <li><a href="javascript:;"><i class="icon-social-dribbble"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right"><p>Copyright &copy; BSITC <?php echo date('Y');?></p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bs-reset"><div class="login-bg"> </div></div>
            </div>
        </div>
		
        <script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		
		<!--
        <script src="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		-->
        
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('script_url');?>assets/global/scripts/app.min.js" type="text/javascript"></script>
     
        <script>
			var baseurl = "<?php echo base_url();?>";
			var url = baseurl + "users/login/checkLogin";
			jQuery("body").addClass("login_body");
			jQuery(".login-form").on("submit",function(e){
				e.preventDefault();
				jQuery(".alert-danger").hide();	
				var username = jQuery("#inputEmail").val();
				var password = jQuery("#inputPassword").val();
				jQuery(".btn-signin").attr("disabled","1");
				jQuery.post(url,jQuery( ".login-form" ).serialize(),function(res){
					console.log(res)
					if(res == "1"){
						window.location= baseurl + "dashboard";
					}
					else{
						jQuery(".btn-signin").removeAttr("disabled","1");
						jQuery(".alert-danger").show();
					}
					
				})
				//return false;
			});

            $(document).ready(function()
            {
				 $('.login-bg').backstretch([
					 "<?php echo $this->config->item('script_url');?>/assets/pages/img/login/bg1.jpg",
					"<?php echo $this->config->item('script_url');?>/assets/pages/img/login/bg2.jpg",
					"<?php echo $this->config->item('script_url');?>/assets/pages/img/login/bg3.jpg"
					], {
					  fade: 1000,
					  duration: 8000
					}
				);
			
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
				
				$('form').each(function() {
					$(this).find('input').keypress(function(e) {
						// Enter pressed?
						if(e.which == 10 || e.which == 13) {
							console.log("test");
							$(this).submit();
						}
					});
				});
            })
        </script>
    </body>
</html>