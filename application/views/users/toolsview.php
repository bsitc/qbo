<style>
	.container{
		width: 55%;
		margin: 0 auto;
		border: 1px solid black;
		padding: 10px 0px;
	}
</style>
	

<div class="tab-pane" id="tab_1_6">
	<div class="row">
		<div class="col-md-12">
			<a class="newloadercss" href="javascript:;" onclick="runEmailAlert()">Email Alert<img id="loadergif" style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>"></a>
		</div>
	</div>
	
	<h2>Running Automation</h2>
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="taskTable">
				<thead>
					<tr>
						<th>Task ID</th>
						<th>Start Time</th>
						<th>Task Name</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a class="newloadercss" href="javascript:;" onclick="checkAutomations()">Refresh<img id="loadergif" style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>"></a>
		</div>
	</div>
	
	
	
	
	</br></br>
	<h2>Document Update</h2>
	<form role="form" action="<?php echo base_url('users/profile/updateMetadata');?>">
		<div class="alert alert-success hide">
			<strong>Info!</strong> Data Changed successfully.
		</div>
		<div class="form-group">
			<label class="control-label">IDs</label>
			<textarea rows="6" placeholder="Enter IDs" name="Ids" class="form-control"/></textarea>
		</div>
		<div class="form-group">
			<label class="control-label">DataType</label>
			<select name="datatype" class="form-control">
				<option value="">Select</option>
				<option value="sales_order">Sales</option>
				<option value="sales_credit_order">SalesCredit</option>
				<option value="purchase_order">Purchase</option>
				<option value="purchase_credit_order">PurchaseCredit</option>
				<option value="cogs_journal">COGS</option>
				<option value="amazon_ledger">AmazonFee</option>
				<option value="stock_journals">StockJournal</option>
				<option value="stock_adjustment">StockAdjustment</option>
				<option value="customers">Customer</option>
				<option value="products">Product</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Account 2 Name</label>
			<select name="account2Name" class="form-control" >
				<option value="">Select</option>
			<?php	
				foreach($allAccount2NameArr as $acc2Id	=> $allAccount2NameArrTemp){
					echo '<option value="'.$acc2Id.'">'.$allAccount2NameArrTemp.'</option>';
				}
			?>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Action</label>
			<select name="actionRequired" class="form-control">
				<option value="">Select</option>
				<option value="0">Pending</option>
				<option value="1">Sent</option>
				<option value="2">Updated</option>
				<option value="3">Payment Created</option>
				<option value="4">Archived</option>
				<option value="111">Close Payments</option>
				<option value="222">Remove Linking</option>
			</select>
		</div>
		<div class="margin-top-10">
			<a href="javascript:;" class="btn savebtnCss savefrombtn">Execute<img style="display:none;width: 30px" src="<?php echo base_url('assets/layouts/layout/img/ajax-loader-1.gif');?>"> </a>
			<a href="javascript:;" class="btn default">Cancel</a>
			
		</div>
	</form>
	</br></br>
	<h2>ID Update</h2>
	<form role="form" action="<?php echo base_url('users/profile/updateDataIds');?>">
		<div class="alert alert-success hide"><strong>Info!</strong> Data Changed successfully.</div>
		<div class="form-group">
			<label class="control-label">BrightpearlID</label>
			<input type="text" name="brightpearlId" class="form-control" placeholder="Enter BrightpearlID" />
		</div>
		<div class="form-group">
			<label class="control-label">New ID</label>
			<input type="text" name="newupdateId" class="form-control" placeholder="Enter New ID"/>
		</div>
		<div class="form-group">
			<label class="control-label">DataType</label>
			<select name="datatype" class="form-control">
				<option value="">Select</option>
				<option value="customers">Customer</option>
				<option value="products">Product</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Account 2 Name</label>
			<select name="account2Name" class="form-control" >
				<option value="">Select</option>
			<?php	
				foreach($allAccount2NameArr as $acc2Id	=> $allAccount2NameArrTemp){
					echo '<option value="'.$acc2Id.'">'.$allAccount2NameArrTemp.'</option>';
				}
			?>
			</select>
		</div>
		<div class="margin-top-10">
			<a href="javascript:;" class="btn savebtnCss savefrombtn">Execute<img style="display:none;width: 30px" src="<?php echo base_url('assets/layouts/layout/img/ajax-loader-1.gif');?>"> </a>
			<a href="javascript:;" class="btn default">Cancel</a>
			
		</div>
	</form>
</div>
<script>
	$(document).ready(function(){
		checkAutomations();
	});
	function checkAutomations(){
		let	page_no	= 1;
		$.ajax({
			url:'<?php echo base_url('getAutomations.php');?>',
			type: 'post',
			data: {page:page_no},
			success: function(response){
				$("#taskTable tbody").html(response);
			}
		});
	}
	function runEmailAlert(){
		let	page_no	= 1;
		$.ajax({
			url:'<?php echo base_url('emailAlerts/processEmailAlerts/');?>',
			type: 'post',
			data: {page:page_no},
			success: function(response){
			}
		});
	}
</script>