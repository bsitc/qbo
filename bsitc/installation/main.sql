SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE IF NOT EXISTS `admin_user` (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'User First Name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'User Last Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `phone` varchar(15) NOT NULL,
  `profileimage` varchar(255) NOT NULL,
  `username` varchar(40) DEFAULT NULL COMMENT 'User Login',
  `password` varchar(100) DEFAULT NULL COMMENT 'User Password',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'User Created Time',
  `modified` timestamp NULL DEFAULT NULL COMMENT 'User Modified Time',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `lognum` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'User Login Number',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'User Is Active',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNQ_ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin User Table';

INSERT INTO `admin_user` (`user_id`, `firstname`, `lastname`, `email`, `phone`, `profileimage`, `username`, `password`, `created`, `modified`, `logdate`, `lognum`, `is_active`) VALUES
(1, 'BSITC', '', 'aherve@businesssolutionsinthecloud.com', '', '', 'bsitc', '6c269b37c4221a490da23e958aa208f6', '0000-00-00 00:00:00', NULL, '2018-12-05 07:28:56', 714, 1);

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('05qeja7rolole6hpdjpe7u5mfve6mtpu', '192.168.1.100', 1543994940, '__ci_last_regenerate|i:1543994935;global_config|a:60:{s:2:\"id\";s:1:\"1\";s:8:\"app_name\";s:6:\"Radial\";s:12:\"account1Name\";s:11:\"Brightpearl\";s:16:\"account1Liberary\";s:11:\"brightpearl\";s:12:\"account2Name\";s:6:\"Radial\";s:16:\"account2Liberary\";s:6:\"radial\";s:13:\"enableProduct\";s:1:\"0\";s:12:\"fetchProduct\";s:11:\"brightpearl\";s:11:\"postProduct\";s:6:\"radial\";s:13:\"enablePrebook\";s:1:\"0\";s:14:\"enableCustomer\";s:1:\"0\";s:13:\"fetchCustomer\";s:11:\"brightpearl\";s:12:\"postCustomer\";s:3:\"qbo\";s:16:\"enableSalesOrder\";s:1:\"0\";s:15:\"fetchSalesOrder\";s:11:\"brightpearl\";s:14:\"postSalesOrder\";s:6:\"radial\";s:13:\"enableReceipt\";s:1:\"0\";s:12:\"fetchReceipt\";s:11:\"brightpearl\";s:11:\"postReceipt\";s:11:\"brightpearl\";s:26:\"enableDispatchConfirmation\";s:1:\"0\";s:25:\"fetchDispatchConfirmation\";s:6:\"radial\";s:24:\"postDispatchConfirmation\";s:11:\"brightpearl\";s:19:\"enablePurchaseOrder\";s:1:\"0\";s:18:\"fetchPurchaseOrder\";s:11:\"brightpearl\";s:17:\"postPurchaseOrder\";s:11:\"brightpearl\";s:19:\"enableStockTransfer\";N;s:18:\"fetchStockTransfer\";s:11:\"brightpearl\";s:17:\"postStockTransfer\";s:11:\"brightpearl\";s:21:\"enableStockAdjustment\";s:1:\"0\";s:20:\"fetchStockAdjustment\";s:11:\"brightpearl\";s:19:\"postStockAdjustment\";s:11:\"brightpearl\";s:15:\"enableStockSync\";s:1:\"0\";s:14:\"fetchStockSync\";s:6:\"radial\";s:13:\"postStockSync\";s:11:\"brightpearl\";s:13:\"enblePreorder\";s:1:\"0\";s:21:\"enableInventoryadvice\";s:1:\"0\";s:19:\"fetchInventoradvice\";s:11:\"brightpearl\";s:18:\"postInventoradvice\";s:7:\"wayfair\";s:17:\"enableSalesCredit\";s:1:\"0\";s:16:\"fetchSalesCredit\";s:11:\"brightpearl\";s:15:\"postSalesCredit\";s:11:\"brightpearl\";s:20:\"enablePurchaseCredit\";s:1:\"0\";s:19:\"fetchPurchaseCredit\";s:11:\"brightpearl\";s:18:\"postPurchaseCredit\";s:3:\"qbo\";s:13:\"enableMapping\";s:1:\"1\";s:21:\"enableCategoryMapping\";s:1:\"0\";s:20:\"enableChannelMapping\";s:1:\"0\";s:23:\"enableLeadsourceMapping\";s:1:\"0\";s:20:\"enablePaymentMapping\";s:1:\"0\";s:22:\"enablePricelistMapping\";s:1:\"0\";s:21:\"enableSalesrepMapping\";s:1:\"0\";s:16:\"enableTaxMapping\";s:1:\"0\";s:22:\"enableWarehouseMapping\";s:1:\"0\";s:18:\"enableBrandMapping\";s:1:\"0\";s:21:\"enableShippingMapping\";s:1:\"0\";s:28:\"enableInventoryadviceMapping\";s:1:\"0\";s:26:\"enablePackedmappingMapping\";s:1:\"0\";s:20:\"enableTradingMapping\";s:1:\"0\";s:22:\"enableAllowanceMapping\";s:1:\"0\";s:16:\"enableBoxMapping\";s:1:\"0\";}login_user_data|a:6:{s:7:\"user_id\";s:1:\"1\";s:9:\"firstname\";s:5:\"BSITC\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:38:\"aherve@businesssolutionsinthecloud.com\";s:8:\"username\";s:5:\"bsitc\";s:12:\"profileimage\";s:75:\"http://192.168.1.100/bsitc/metronics//assets/layouts/layout/img/profile.png\";}');

CREATE TABLE IF NOT EXISTS `cron_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `totalRecord` int(11) NOT NULL,
  `runTime` varchar(255) DEFAULT NULL,
  `saveTime` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `file_name_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_file_name` varchar(1000) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table to track varios file name for each day';

CREATE TABLE IF NOT EXISTS `global_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `account1Name` varchar(255) NOT NULL,
  `account1Liberary` varchar(255) NOT NULL,
  `account2Name` varchar(255) NOT NULL,
  `account2Liberary` varchar(255) NOT NULL,
  `enableProduct` int(11) NOT NULL,
  `fetchProduct` varchar(255) NOT NULL,
  `postProduct` varchar(255) NOT NULL,
  `enablePrebook` int(11) NOT NULL,
  `enableCustomer` int(11) NOT NULL,
  `fetchCustomer` varchar(255) NOT NULL,
  `postCustomer` varchar(255) NOT NULL,
  `enableSalesOrder` int(11) NOT NULL,
  `fetchSalesOrder` varchar(255) NOT NULL,
  `postSalesOrder` varchar(255) NOT NULL,
  `enableReceipt` int(11) NOT NULL,
  `fetchReceipt` varchar(255) NOT NULL,
  `postReceipt` varchar(255) NOT NULL,
  `enableDispatchConfirmation` int(11) NOT NULL,
  `fetchDispatchConfirmation` varchar(255) NOT NULL,
  `postDispatchConfirmation` varchar(255) NOT NULL,
  `enablePurchaseOrder` int(11) NOT NULL,
  `fetchPurchaseOrder` varchar(255) NOT NULL,
  `postPurchaseOrder` varchar(255) NOT NULL,
  `enableStockTransfer` varchar(255) DEFAULT NULL,
  `fetchStockTransfer` varchar(255) DEFAULT NULL,
  `postStockTransfer` varchar(255) DEFAULT NULL,
  `enableStockAdjustment` int(11) NOT NULL,
  `fetchStockAdjustment` varchar(255) NOT NULL,
  `postStockAdjustment` varchar(255) NOT NULL,
  `enableStockSync` int(11) NOT NULL,
  `fetchStockSync` varchar(255) NOT NULL,
  `postStockSync` varchar(255) NOT NULL,
  `enblePreorder` int(11) NOT NULL,
  `enableInventoryadvice` int(11) NOT NULL DEFAULT '0',
  `fetchInventoradvice` varchar(255) DEFAULT NULL,
  `postInventoradvice` varchar(255) DEFAULT NULL,
  `enableSalesCredit` int(11) NOT NULL,
  `fetchSalesCredit` varchar(255) DEFAULT NULL,
  `postSalesCredit` varchar(255) DEFAULT NULL,
  `enablePurchaseCredit` int(11) NOT NULL,
  `fetchPurchaseCredit` varchar(255) DEFAULT NULL,
  `postPurchaseCredit` varchar(255) DEFAULT NULL,
  `enableMapping` int(11) NOT NULL,
  `enableCategoryMapping` int(11) NOT NULL,
  `enableChannelMapping` int(11) NOT NULL,
  `enableLeadsourceMapping` int(11) NOT NULL,
  `enablePaymentMapping` int(11) NOT NULL,
  `enablePricelistMapping` int(11) NOT NULL,
  `enableSalesrepMapping` int(11) NOT NULL,
  `enableTaxMapping` int(11) NOT NULL,
  `enableWarehouseMapping` int(11) NOT NULL,
  `enableBrandMapping` int(11) NOT NULL,
  `enableShippingMapping` int(11) NOT NULL,
  `enableInventoryadviceMapping` int(11) NOT NULL,
  `enablePackedmappingMapping` int(11) NOT NULL,
  `enableTradingMapping` int(11) NOT NULL,
  `enableAllowanceMapping` int(11) NOT NULL,
  `enableBoxMapping` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `global_config` (`id`, `app_name`, `account1Name`, `account1Liberary`, `account2Name`, `account2Liberary`, `enableProduct`, `fetchProduct`, `postProduct`, `enablePrebook`, `enableCustomer`, `fetchCustomer`, `postCustomer`, `enableSalesOrder`, `fetchSalesOrder`, `postSalesOrder`, `enableReceipt`, `fetchReceipt`, `postReceipt`, `enableDispatchConfirmation`, `fetchDispatchConfirmation`, `postDispatchConfirmation`, `enablePurchaseOrder`, `fetchPurchaseOrder`, `postPurchaseOrder`, `enableStockTransfer`, `fetchStockTransfer`, `postStockTransfer`, `enableStockAdjustment`, `fetchStockAdjustment`, `postStockAdjustment`, `enableStockSync`, `fetchStockSync`, `postStockSync`, `enblePreorder`, `enableInventoryadvice`, `fetchInventoradvice`, `postInventoradvice`, `enableSalesCredit`, `fetchSalesCredit`, `postSalesCredit`, `enablePurchaseCredit`, `fetchPurchaseCredit`, `postPurchaseCredit`, `enableMapping`, `enableCategoryMapping`, `enableChannelMapping`, `enableLeadsourceMapping`, `enablePaymentMapping`, `enablePricelistMapping`, `enableSalesrepMapping`, `enableTaxMapping`, `enableWarehouseMapping`, `enableBrandMapping`, `enableShippingMapping`, `enableInventoryadviceMapping`, `enablePackedmappingMapping`, `enableTradingMapping`, `enableAllowanceMapping`, `enableBoxMapping`) VALUES
(1, 'Radial', 'Brightpearl', 'brightpearl', 'Radial', 'radial', 0, 'brightpearl', 'radial', 0, 0, 'brightpearl', 'qbo', 0, 'brightpearl', 'radial', 0, 'brightpearl', 'brightpearl', 0, 'radial', 'brightpearl', 0, 'brightpearl', 'brightpearl', NULL, 'brightpearl', 'brightpearl', 0, 'brightpearl', 'brightpearl', 0, 'radial', 'brightpearl', 0, 0, 'brightpearl', 'wayfair', 0, 'brightpearl', 'brightpearl', 0, 'brightpearl', 'qbo', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
