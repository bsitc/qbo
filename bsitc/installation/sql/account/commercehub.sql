CREATE TABLE IF NOT EXISTS `account_commercehub_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `sftp` int(11) NOT NULL DEFAULT '0',
  `passive` varchar(255) DEFAULT NULL,
  `debug` varchar(255) DEFAULT NULL,
  `sales` varchar(255) DEFAULT NULL,
  `acknowledge` varchar(255) DEFAULT NULL,
  `pack` varchar(255) DEFAULT NULL,
  `dispatch` varchar(255) DEFAULT NULL,
  `purchase` varchar(255) DEFAULT NULL,
  `poacknowledge` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `invoice` varchar(255) DEFAULT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `inventoryadvice` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_commercehub_account` (`id`, `name`, `account1Id`, `hostname`, `username`, `password`, `port`, `sftp`, `passive`, `debug`, `sales`, `acknowledge`, `pack`, `dispatch`, `purchase`, `poacknowledge`, `receipt`, `invoice`, `stock`, `inventoryadvice`, `status`, `params`) VALUES
(1, 'commercehub', 1, '77.68.88.57', 'commercehub', '3Nyv4*1z', '21', 0, 'true', 'false', '/out', '/acknowledgement', '/packed', NULL, 'purchase', 'acknowledgement', 'receipt', '/invoice', 'inventory', '/inventoryadvice', 0, '');
CREATE TABLE IF NOT EXISTS `account_commercehub_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `spsAccountId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;