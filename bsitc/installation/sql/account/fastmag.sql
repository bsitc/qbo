CREATE TABLE IF NOT EXISTS `account_fastmag_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `serveur` varchar(255) NOT NULL,
  `enseigne` varchar(255) NOT NULL,
  `magasin` varchar(255) NOT NULL,
  `compte` varchar(255) NOT NULL,
  `motpasse` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO `account_fastmag_account` (`id`, `account1Id`, `name`, `serveur`, `enseigne`, `magasin`, `compte`, `motpasse`, `status`, `params`) VALUES
(1, 0, 'OPERA', 'opc4.fastmag.fr', 'DEMO', 'OPERA', 'ANTHONY', '12345', 1, '');
CREATE TABLE IF NOT EXISTS `account_fastmag_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fastmagAccountId` int(11) NOT NULL,
  `compte` varchar(255) NOT NULL,
  `vendor` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO `account_fastmag_config` (`id`, `fastmagAccountId`, `compte`, `vendor`) VALUES
(1, 1, 'ANTHONY', 'SERGE');