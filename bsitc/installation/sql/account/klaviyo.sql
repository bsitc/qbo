CREATE TABLE IF NOT EXISTS `account_klaviyo_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `userId` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_klaviyo_account` (`id`, `name`, `account1Id`, `userId`, `password`, `status`, `params`) VALUES
(1, 'klaviyo', 1, NULL, 'pk_453f70191929a6385aa2fdfe31b6169929', 0, '');
CREATE TABLE IF NOT EXISTS `account_klaviyo_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `klaviyoAccountId` int(11) NOT NULL,
  `defaultListId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_klaviyo_config` (`id`, `name`, `klaviyoAccountId`, `defaultListId`) VALUES
(1, 'klaviyo', 1, 'LFaCjE');