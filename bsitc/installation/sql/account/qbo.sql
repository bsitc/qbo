CREATE TABLE IF NOT EXISTS `account_qbo_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `clientId` varchar(255) NOT NULL,
  `clientSecret` varchar(255) NOT NULL,
  `redirectUrl` varchar(255) NOT NULL,
  `accessToken` varchar(1000) NOT NULL,
  `tokenType` varchar(255) NOT NULL,
  `expires` varchar(255) NOT NULL,
  `expiresIn` int(11) NOT NULL,
  `tokenFetchTime` varchar(50) DEFAULT NULL,
  `refreshToken` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `params` text,
  `mode` varchar(50) DEFAULT NULL,
  `baseUrl` varchar(255) DEFAULT NULL,
  `companyId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_qbo_account` (`id`, `account1Id`, `name`, `clientId`, `clientSecret`, `redirectUrl`, `accessToken`, `tokenType`, `expires`, `expiresIn`, `tokenFetchTime`, `refreshToken`, `created`, `status`, `params`, `mode`, `baseUrl`, `companyId`) VALUES
(1, 1, '123146089065279', 'Q0VQOgs2U724wKqMHrQIart0gJOMYL5TtxVDUpM1A1jymbURir', 'u6Y7qvVecM3segfZwKowdpxfjemmpghYgqatu1kA', 'http://bsitc-bridge16.com/filtersource/webhooks/refreshToken', 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Fi8IXIF00XBCyQZ8ejp3_w.zziifLNbps6sVxT4qXcIjMb1y5Deai53Sp3RYxK2TOh8PlJsl3LTIduPY_HohgWE1wo87u7SKtgBWFc_hrieqLg7s9qWKh4bpL1uAL_p7un7OxRo4fx9R8H-lKMlhgw00_Hqch_cKbkaiBB7P59lWuSCtWCVUHnKY9hsJ5HkwL0ovz4OkKnREGg2bqruKcTaUF7Axq4XdV1eZwT7DgFqbhsSgc3C-naAzQ08nCis8UZcRX8OqXBF0pk8_lwuKQyGhar22sxI3UKGea4PKGpH_G4wzJ_flNggH2G3hBNfT2O86DXNMNAuRL7wR49NlhC80ydzsUkYBw1Bz9ojj2nXHiQcaaSCpRZevDDdky1IZe8uc0dSv43rZ9PymIglwTll3LzgS3KdcuBz97AYy4gs_AQ2Omgraaln2YboHDCUQTryOpJ2VxC04wW4O87cSBXJE51fAzE4aKTW9uMdLd1alemeoi-Vo7r3mP2Zrf1WFgtqHPimW6Dj-rtQnuzfeEWrri5yfO8lJnC5AcbQ-06LfCW8Dz84u6MQFzxoZVWYkRuAAubH4lnlajijwuKaGSLlcOs8UvLIrVsJ8XA4gb2xAiXlO8FYYQHDas9yOPfK1DiJe2wTtGL514M3alE0W9vAzsIOuLDxBIpOX0wTlvl4NKudkPcxWz3uNWjzw4ZuN14FZZRihiLtdw0Yp1tohZC6.j3vj8q8DULskt5f5ttedrw', 'bearer', '8726400', 3600, '1536728707', 'Q011544346539KcI52MNrfR1g66SHCzkgQDzPceTv5kmW6Q4aY', '2018-05-14 13:32:41', 0, NULL, 'sandbox', 'https://sandbox-quickbooks.api.intuit.com', '123146089065279');
CREATE TABLE IF NOT EXISTS `account_qbo_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qboAccountId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `DepositToAccountRef` varchar(255) DEFAULT NULL,
  `IncomeAccountRef` varchar(255) NOT NULL,
  `AssetAccountRef` varchar(55) DEFAULT NULL,
  `ExpenseAccountRef` varchar(255) DEFAULT NULL,
  `PayTypeAccRef` varchar(255) DEFAULT NULL,
  `PayType` varchar(255) DEFAULT NULL,
  `TaxCode` varchar(255) DEFAULT NULL,
  `orderLineTaxCode` varchar(100) DEFAULT NULL,
  `DiscountAccountRef` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_qbo_config` (`id`, `qboAccountId`, `name`, `DepositToAccountRef`, `IncomeAccountRef`, `AssetAccountRef`, `ExpenseAccountRef`, `PayTypeAccRef`, `PayType`, `TaxCode`, `orderLineTaxCode`, `DiscountAccountRef`, `created`) VALUES
(1, 1, '123146089065279', '96', '79', '81', '80', '35', 'Check', '5', 'TAX', '86', '2018-08-30 09:39:06');