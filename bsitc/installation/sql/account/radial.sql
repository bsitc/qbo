CREATE TABLE IF NOT EXISTS `account_radial_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passive` varchar(255) DEFAULT NULL,
  `debug` varchar(255) DEFAULT NULL,
  `products` varchar(255) DEFAULT NULL,
  `sales` varchar(255) DEFAULT NULL,
  `acknowledge` varchar(255) DEFAULT NULL,
  `dispatch` varchar(255) DEFAULT NULL,
  `purchase` varchar(255) DEFAULT NULL,
  `poacknowledge` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `invoice` varchar(255) DEFAULT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO `account_radial_account` (`id`, `name`, `account1Id`, `hostname`, `username`, `password`, `passive`, `debug`, `products`, `sales`, `acknowledge`, `dispatch`, `purchase`, `poacknowledge`, `receipt`, `invoice`, `stock`, `status`, `params`) VALUES
(1, '77.68.88.57', 1, '77.68.88.57', 'radial', 'Xd$g3f84', 'true', 'true', '/products', '/sales', NULL, '/dispatch', '/purchase', '', '/receipt', '', '/stock', 0, '');
CREATE TABLE IF NOT EXISTS `account_radial_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `radialAccountId` int(11) NOT NULL,
  `accountCode` varchar(100) DEFAULT NULL,
  `BPCustomNotradial` varchar(255) DEFAULT NULL,
  `defaultProductType` varchar(255) NOT NULL,
  `defaultCountryOfOrigin` varchar(255) NOT NULL,
  `SupplierID` varchar(255) DEFAULT NULL,
  `orderType` varchar(255) DEFAULT NULL,
  `orderTypeInventorySales` varchar(255) DEFAULT NULL,
  `orderTypeInventoryPurchase` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_radial_config` (`id`, `name`, `radialAccountId`, `accountCode`, `BPCustomNotradial`, `defaultProductType`, `defaultCountryOfOrigin`, `SupplierID`, `orderType`, `orderTypeInventorySales`, `orderTypeInventoryPurchase`) VALUES
(1, '77.68.88.57', 1, 'RADIAL', 'TEST', 'P', 'GB', '37802', 'WEB', 'REPLEN', 'STDGI');