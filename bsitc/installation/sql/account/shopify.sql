CREATE TABLE IF NOT EXISTS `account_shopify_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `storeName` varchar(255) NOT NULL,
  `appKey` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO `account_shopify_account` (`id`, `account1Id`, `name`, `storeName`, `appKey`, `password`, `status`, `params`) VALUES
(1, 1, 'https://dean-teststore1.myshopify.com', 'https://dean-teststore1.myshopify.com', '93833412dd49b2d9358445cade9f96d9', 'b1383c8029b766df7f6012185050296d', 0, '');
CREATE TABLE IF NOT EXISTS `account_shopify_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopifyAccountId` varchar(255) NOT NULL,
  `location` varchar(20) NOT NULL,
  `storeName` varchar(255) NOT NULL,
  `defaultColor` varchar(255) NOT NULL,
  `defaultSize` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO `account_shopify_config` (`id`, `shopifyAccountId`, `location`, `storeName`, `defaultColor`, `defaultSize`) VALUES
(1, '1', '5123768378', 'https://dean-teststore1.myshopify.com', 'Unique', 'Unique'),
(2, '4', '14537483', 'https://bsitc-test.myshopify.com', 'Unique', 'Unique');