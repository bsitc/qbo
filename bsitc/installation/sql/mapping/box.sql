CREATE TABLE IF NOT EXISTS `mapping_box` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `boxName` varchar(255) DEFAULT NULL,
  `length` int(11) DEFAULT '0',
  `width` varchar(10) DEFAULT NULL,
  `height` int(11) DEFAULT '0',
  `weight` int(11) DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;