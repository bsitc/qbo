CREATE TABLE IF NOT EXISTS `mapping_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1ChannelId` varchar(255) NOT NULL,
  `account2ChannelId` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
