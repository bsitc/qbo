CREATE TABLE IF NOT EXISTS `mapping_shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1ShippingId` varchar(255) NOT NULL,
  `account2ShippingId` varchar(255) NOT NULL,
  `CarrierRouting` varchar(255) DEFAULT NULL,
  `CarrierTransMethodCode` varchar(255) DEFAULT NULL,
  `CarrierAlphaCode` varchar(255) DEFAULT NULL,
  `tradingPartnerId` varchar(255) DEFAULT NULL,
  `scac` varchar(255) DEFAULT NULL,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;