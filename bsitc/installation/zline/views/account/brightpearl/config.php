<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Brightpearl
            <small>Brightpearl</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Brightpearl Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Brightpearl Id</th>
                                    <th width="25%">Account Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"><?php echo $row['brightpearlAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Brightpearl Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-6">Brightpearl Id
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[brightpearlAccountId]" data-required="1" class="form-control brightpearlAccountId">
								<option value="">Select a save Brightpearl account</option>
								<?php
								foreach ($data['saveAccount'] as $saveAccount) {
									echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
								}
								?>
							</select>
						</div>
					</div>  			
					<div class="form-group">
						<label class="control-label col-md-6">Default warehouse
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[warehouse]" data-required="1" class="form-control warehouse">
								<?php
								foreach ($data['warehouse'][$row['brightpearlAccountId']] as $warehouse) {
									echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label col-md-6">Default Channel
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[channelId]"  class="form-control channelId">
								<?php
								foreach ($data['channel'][$row['brightpearlAccountId']] as $channel) {
									echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Create sales order status
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[createOrderStatus]" data-required="1" class="form-control createOrderStatus">
								<?php
								foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
									if($orderstatus['orderTypeCode'] == 'SO')
										echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Backorder status
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[backorderStatus]" data-required="1" class="form-control backorderStatus">
								<?php
								foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
									if($orderstatus['orderTypeCode'] == 'SO')
										echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					
					<div class="form-group hide">
						<label class="control-label col-md-6">PO channel name to fetch inventory
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[poInventoryChannelName]"  class="form-control poInventoryChannelName">
								<?php
								foreach ($data['channel'][$row['brightpearlAccountId']] as $channel) {
									echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Default shipping method
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[shippingmethods]" data-required="1" class="form-control shippingmethods">
								<?php
								foreach ($data['shippingmethods'][$row['brightpearlAccountId']] as $shippingmethods) {
									echo '<option value="'.$shippingmethods['id'].'">'.ucwords($shippingmethods['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Cost Price List
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[defaultProductPriceList]" data-required="1" class="form-control defaultProductPriceList">
								<?php
								foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
									echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label col-md-6">Pricelist for customer
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[contactPriceList]" class="form-control contactPriceList">
								<?php
								foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
									echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label col-md-6">Default Customer Tag
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[contactDefaultTag]" class="form-control contactDefaultTag">
								<?php
								foreach ($data['tag'][$row['brightpearlAccountId']] as $tag) {
									echo '<option value="'.$tag['id'].'">'.ucwords($tag['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-6">Default tax code
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[taxCode]" data-required="1" class="form-control taxCode">
								<?php
								foreach ($data['tax'][$row['brightpearlAccountId']] as $tax) {
									echo '<option value="'.$tax['id'].'">'.ucwords($tax['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label col-md-6">Default nominal code
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[nominalCode]" class="form-control nominalCode">
								<?php
								foreach ($data['nominalCode'][$row['brightpearlAccountId']] as $nominalCode) {
									echo '<option value="'.$nominalCode['id'].'">'.ucwords($nominalCode['name']).'</option>';
								}
								?>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Print Label Content 
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[contentInLabel]" class="form-control contentInLabel">
							   <option value="0">No</option>
							   <option value="1">Yes</option>
							</select> 
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">Automatically upload packing file 
							<span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-6">
							<select name="data[autoUploadPackedFile]" class="form-control autoUploadPackedFile">
							   <option value="0">No</option>
							   <option value="1">Yes</option>
							</select> 
						</div>
					</div> 
					

					<div class="form-group">
						<label class="control-label col-md-6">Default Currency
						</label>
						<div class="col-md-6">
							<input type="text" name="data[currencyCode]"  class="form-control currencyCode" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-6">Additional Charges Code
						</label>
						<div class="col-md-6">
							<input type="text" name="data[additionalcharge]"  class="form-control additionalcharge" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Discount Code
						</label>
						<div class="col-md-6">
							<input type="text" name="data[discount]"  class="form-control discount" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">Shipment Identification
						</label>
						<div class="col-md-6">
							<input type="text" name="data[ShipmentIdentification]"  class="form-control ShipmentIdentification" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-6">GS1 Global Company Extension (1 Digit)
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[companyExtension]" class="form-control companyExtension">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">GS1 Global Company Prefix (7 Digit)
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[companyPrefix]" class="form-control companyPrefix">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">GS1 Global Company Serial Number (10 Digit)
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[serialNumber]" class="form-control serialNumber">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group hide">
						<label class="control-label col-md-6">Product wholesale custom field
						</label>
						<div class="col-md-6">
							<input type="text" name="data[productCustField]"  class="form-control productCustField" />
						</div>
					</div> 
					<div class="form-group hide">
						<label class="control-label col-md-6">Product style number custom field
						</label>
						<div class="col-md-6">
							<input type="text" name="data[productStyleNumber]"  class="form-control productStyleNumber" />
						</div>
					</div> 
					
					<div class="form-group hide">
						<label class="control-label col-md-6">Customer wholesale custom field
						</label>
						<div class="col-md-6 hide">
							<input type="text" name="data[customerCustomField]"  class="form-control customerCustomField" />
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label col-md-6">Ship start custom field
						</label>
						<div class="col-md-6">
							<input type="text" name="data[shipStartDate]"  class="form-control shipStartDate" />
						</div>
					</div>
					<div class="form-group hide">
						<label class="control-label col-md-6">Ship end custom field
						</label>
						<div class="col-md-6">
							<input type="text" name="data[shipEndDate]"  class="form-control shipEndDate" />
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-md-6">PCF_STORENO custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_STORENO]" class="form-control PCF_STORENO">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_CUSTORD custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_CUSTORD]" class="form-control PCF_CUSTORD">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_DEPTNO custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_DEPTNO]" class="form-control PCF_DEPTNO">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_POVENDNO custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_POVENDNO]" class="form-control PCF_POVENDNO">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_ORDDATE custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_ORDDATE]" class="form-control PCF_ORDDATE">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_EXPDEL custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_EXPDEL]" class="form-control PCF_EXPDEL">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_CANCEL custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_CANCEL]" class="form-control PCF_CANCEL">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_CANCODE custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_CANCODE]" class="form-control PCF_CANCODE">
						</div>
					</div> 
					
					<div class="form-group">
						<label class="control-label col-md-6">PCF_ESTSHIP custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_ESTSHIP]" class="form-control PCF_ESTSHIP">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_ESTDELIV custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_ESTDELIV]" class="form-control PCF_ESTDELIV">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_EXPQTY custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_EXPQTY]" class="form-control PCF_EXPQTY">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_ETA custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_ETA]" class="form-control PCF_ETA">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_WAYSPEED custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_WAYSPEED]" class="form-control PCF_WAYSPEED">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-md-6">PCF_DISCONT  custom field
							<span class="required" > * </span>
						</label>
						<div class="col-md-6">                                    
							<input type="text" name="data[PCF_DISCONT]" class="form-control PCF_DISCONT">
						</div>
					</div> 
					
				</div> 
			</div> 
			
			

        </div> 
    <?php } ?>
</div>