<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Settings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> SSCC 
            <small>Settings</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>SSCC Settings </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Setting </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
									<th width="10%">Trading Partner Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Save Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="tradingPartnerId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/sscc/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									<td><span class="value" data-value="tradingPartnerId"><?php echo @$row['tradingPartnerId'];?></span></td>
                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
										<script> var data<?php echo $value['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick="editAction(data<?php echo $value['id'];?>)" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/sscc/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">SSCC Settings</h4> 
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('account/sscc/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
                                        <?php
                                        foreach ($data['account2Id'] as $account2Id) {
                                            echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>  
							
							<div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Trading Partner Id
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['tradingPartnerId']){ ?> 
                                    <select name="data[tradingPartnerId]" data-required="1" class="form-control tradingPartnerId acc1listoption">
                                        <option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Allowance Name</option>
                                        <?php
                                        foreach ($data['tradingPartnerId'] as $accountId => $account1AllowanceIds) {
                                            foreach ($account1AllowanceIds as $account1AllowanceId) {
                                                echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1AllowanceId['id'].'">'.ucwords($account1AllowanceId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[tradingPartnerId]" data-required="1" class="form-control tradingPartnerId" />
                                    <?php } ?>
                                </div>
                            </div>							
							<div class="form-group">
                                <label class="control-label col-md-4">GS1 Global Company Extension (1 Digit)
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[companyExtension]" class="form-control companyExtension">
                                </div>
                            </div> 
							<div class="form-group">
                                <label class="control-label col-md-4">GS1 Global Company Prefix (7 Digit)
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[companyPrefix]" class="form-control companyPrefix">
                                </div>
                            </div> 
							<div class="form-group">
                                <label class="control-label col-md-4">GS1 Global Company Serial Number  (10 Digit)
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[serialNumber]" class="form-control serialNumber">
                                </div>
                            </div> 
							
							
							<div class="form-group">
                                <label class="control-label col-md-4">Company (line 1)
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[companyLine1]" class="form-control companyLine1">
                                </div>
                            </div> 
							<div class="form-group">
                                <label class="control-label col-md-4">Company (line 2)
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[companyLine2]" class="form-control companyLine2">
                                </div>
                            </div> 
							<div class="form-group">
                                <label class="control-label col-md-4">Street
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[street]" class="form-control street">
                                </div>
                            </div> 
							<div class="form-group">
                                <label class="control-label col-md-4">Postcode + City
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[postcodeCity]" class="form-control postcodeCity">
                                </div>
                            </div> 
                        
						</div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>