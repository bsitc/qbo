<?php
$tradingPartnerDatas = array_unique(array_column(($this->db->get_where('mapping_inventoryadvice')->result_array()),'tradingIdPartnerId'));
?>
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Product Details</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE-->
	<h3 class="page-title"> Products
		<small>Products</small>
	</h3>
	<!-- END PAGE TITLE-->
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i>Product Listing </div>
					<div class="actions">
						<a href="<?php echo base_url('products/products/fetchProducts');?>" class="btn btn-circle btn-info btnactionsubmit"> 
							<i class="fa fa-download"></i>
							<span class="hidden-xs"> Fetch Products </span>
						</a>						
						<a href="<?php echo base_url('products/products/postProducts');?>" class="btn btn-circle green-meadow btnactionsubmit hide">
							<i class="fa fa-upload"></i>
							<span class="hidden-xs"> Post Products </span>
						</a>						
						<a href="<?php echo base_url('products/products/downlaodProductCsvInventor');?>" class="btn btn-circle btn-warning"> 
							<i class="fa fa-download"></i>
							<span class="hidden-xs"> Download Product File </span>
						</a>
						<a href="#" class="btn btn-circle btn-danger uplaodpreorder"> 
							<i class="fa fa-upload"></i>
							<span class="hidden-xs"> Upload Product File </span>
						</a>
						
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<div class="table-actions-wrapper">
							<span> </span>
							<select class="table-group-action-input form-control input-inline input-small input-sm">
								<option value="">Select...</option>
								<option value="0">Pending</option>
								<option value="1">Sent</option>
								<option value="2">Updated</option>
								<option value="3">Error</option>
								<option value="4">Archive</option>
							</select>
							<button class="btn btn-sm btn-success table-group-action-submit">
								<i class="fa fa-check"></i> Submit</button>
						</div>
						<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="1%"><input type="checkbox" class="group-checkable"> </th>
									<th width="5%"> <?php echo $this->globalConfig['account1Name'];?>&nbsp;Id </th>
									<th width="10%"> Product&nbsp;SKU </th>
									<th width="10%"> Product&nbsp;Name </th>
									<?php
									$widthSize = 55 / count($tradingPartnerDatas);
									foreach($tradingPartnerDatas as $tradingPartnerData){
										echo '<th width="'.$widthSize.'%">'.$this->tradingPartnerDatas[strtolower($tradingPartnerData)]['tradingIdPartnerName'].'</th>';
									}
									?>
									<th width="10%"> Date&nbsp;Updated </th>
									<th width="10%"> Actions </th>
								</tr>
								<tr role="row" class="filter">
									<td> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="productId"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="sku"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="name"> </td>
									<?php
									foreach($tradingPartnerDatas as $tradingPartnerData){
										echo '<td></td>';
									}
									?>
									<td><div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">	<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From">	<span class="input-group-btn">		<button class="btn btn-sm default" type="button">			<i class="fa fa-calendar"></i>		</button>	</span></div><div class="input-group date date-picker" data-date-format="yyyy-mm-dd">	<input type="text" class="form-control form-filter input-sm" readonly name="updated_to " placeholder="To">	<span class="input-group-btn">		<button class="btn btn-sm default" type="button">			<i class="fa fa-calendar"></i>		</button>	</span></div>
									</td>									
									<td><div class="margin-bottom-5">	<button class="btn btn-sm btn-success filter-submit margin-bottom">		<i class="fa fa-search"></i> Search</button></div><button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i> Reset</button>
									</td>
								</tr>
							</thead>
							<tbody> </tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<!-- END CONTENT BODY -->
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
		<div class="body-text">
			
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="popup" role="dialog">
    <div class="modal-dialog">        
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Prebook csv file</h4>
        </div>
        <div class="modal-body" style="display: inline-block;">
           <form method="post"  action ="<?php echo base_url("products/products/uploadInventorAdvice");?>" id="uploadform" enctype="multipart/form-data">
	    		<div class="" style="float: left;text-align: center;width: 100%;">
	       		<input required="true" type="file" name="uploadprefile"  accept=".csv" style="display: inline;" />
	       		<input type="Submit" class="btn btn-primary uploadform" value="upload" value="Submit" /> 
	       	</div>
	       </form>                       
        </div>
        <div class="modal-footer">
          
        </div>
      </div>                  
    </div>
</div>

<div class="modal fade" id="actionmodal" role="dialog">
    <div class="modal-dialog modal-lg">        
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Product</h4>
        </div>
		<form method="post"  action ="<?php echo base_url("products/products/editproduct");?>" class="form-horizontal saveActionForm" id="saveActionForm" enctype="multipart/form-data">
			<div class="modal-body" style="">
				<div class="form-group">
					<label class="control-label col-md-4">Product Id 
						<span class="required" > * </span>
					</label>
					<div class="col-md-7">                                    
						<input type="text" name="productId" class="form-control productId">
					</div>
				</div>
				<?php foreach($this->tradingPartnerDatas as $tradingPartnerId => $tradingPartnerData){ ?>
				<div class="form-group">
					<label class="control-label col-md-4">Product Name for <b><?php echo $tradingPartnerData['tradingIdPartnerName'];?></b>
						<span class="required" > * </span>
					</label>
					<div class="col-md-7">                                    
						<input type="text" name="data[<?php echo strtolower($tradingPartnerId);?>]" class="form-control <?php echo strtolower($tradingPartnerId);?>">
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="modal-footer">
			  <div class="modal-footer">
				  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
				  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
				</div>
			</div>
	   </form>                       
      </div>                  
  </div>                  
</div>



<div class="loader" style="display:none;">
	<div class="loader-inner">
		<img src="<?php echo base_url('assets/img/ajax-loader.gif');?>" />
	</div>
</div>
	
<script src="<?php echo $this->config->item('script_url');?>assets/pages/scripts/ecommerce-products.js" type="text/javascript"></script>
<script>
jQuery(document).on("click",".showDetails",function(e){
	e.preventDefault();
	newSku = jQuery(this).attr('data-newsku');
	color = jQuery(this).attr('data-color');
	console.log("newSku",newSku)
	if(newSku == ''){
		alert("Product have no any varient");
	}
	else{
		jQuery(".loader").show();
		jQuery.post('<?php echo base_url('products/products/getVarient');?>',{'newSku': newSku,'color':color},function(res){
			jQuery("#myModal").modal('show');
			jQuery(".modal-title").text(name);
			jQuery(".body-text").html(res); 
			jQuery(".loader").hide();
		});
	}		 
});
function editproduct (data){
	jQuery("#actionmodal").modal('show');
	jQuery("#editproduct input").val("");
	var tradingPartnerMappings = JSON.parse(data.tradingPartnerMapping);
	for (var tradingPartnerId in tradingPartnerMappings){
		jQuery(".productId").val(data.productId);
		if (tradingPartnerMappings.hasOwnProperty(tradingPartnerId)) {
			//console.log(tradingPartnerId);
			 jQuery("."+tradingPartnerId).val(tradingPartnerMappings[tradingPartnerId]);
		}
	}
	
}
</script>
<script type="text/javascript">
	jQuery(".uplaodpreorder").click(function(e){
		e.preventDefault();
		jQuery("#popup").modal('show');
	})
</script>
