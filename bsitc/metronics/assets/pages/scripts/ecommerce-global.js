var EcommerceProducts = function () {
    var initPickers = function () {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }
	console.log(jsOrder);
    var handleProducts = function() {
        var grid = new Datatable();
        grid.init({
            src: $("#datatable_products"),
            onSuccess: function (grid) {},
            onError: function (grid) {},
            dataTable: {
                "lengthMenu": [
                    [10, 20, 50, 100, 150, 999999],
                    [10, 20, 50, 100, 150, 'All']
                ],
                "pageLength": 10,
                "ajax": {
                    "url": loadUrl,
                },
                "order": [
                    jsOrder
                ]
            }
        });

        jQuery(document).on("click",".btnactionsubmit",function(e){
            e.preventDefault();
            var url = jQuery(this).attr('href');
            jQuery.get(url,function(res){
                grid.getDataTable().ajax.reload();
            })
        })
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });
    }

    return {
        init: function () {
            handleProducts();
            initPickers();
        }
    };
}();
jQuery(document).ready(function() {    
   EcommerceProducts.init();
});