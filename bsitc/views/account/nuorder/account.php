<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> NuOrder
            <small>NuOrder</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>NuOrder Accounts </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Account </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <?php if($data['type'] == 'account2'){
                                       echo '<th width="15%">'.$this->globalConfig['account1Name'].' Name</th>';
                                    }?>
                                    <th width="10%">NuOrder Name</th>
                                    <th width="15%">App Key</th>
                                    <th width="15%">App Secret</th>
                                    <th width="15%">Token</th>
                                    <th width="15%">Token Secret</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                   <?php if($data['type'] == 'account2'){ ?>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <?php } ?>
                                    <td><span class="value" data-value="name"></span></td>
                                    <td><span class="value" data-value="appKey"></span></td>
                                    <td><span class="value" data-value="appSecret"></span></td>
                                    <td><span class="value" data-value="token"></span></td>
                                    <td><span class="value" data-value="tokenSecret"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/account/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                                
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <?php if($data['type'] == 'account2'){ ?>
                                     <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <?php } ?>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td><span class="value" data-value="appKey"><?php echo $row['appKey'];?></span></td>
                                    <td><span class="value" data-value="appSecret"><?php echo $row['appSecret'];?></span></td>
                                    <td><span class="value" data-value="token"><?php echo $row['token'];?></span></td>
                                    <td><span class="value" data-value="tokenSecret"><?php echo $row['tokenSecret'];?></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(<?php echo json_encode($row);?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/account/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">NuOrder Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('account/'.$data['type'].'/account/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <?php if($data['type'] == 'account2'){ ?>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->globalConfig['account1Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id">
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>  

                            <?php } ?>
                            <div class="form-group">
                                <label class="control-label col-md-3">Application Mode
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[url]" data-required="1" class="form-control url">
                                       <option value="https://wholesale.sandbox1.nuorder.com/api/">Sandbox</option>
                                       <option value="https://wholesale.nuorder.com/api/">Live</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Account Name
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <input name="data[name]" data-required="1" class="form-control name" type="text"> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">App Key
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <input name="data[appKey]" data-required="1" class="form-control appKey" type="text"> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">App Secret
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <input name="data[appSecret]" data-required="1" class="form-control appSecret" type="text"> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Token
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <input name="data[token]" data-required="1" class="form-control token" type="text"> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Token Secret
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <input name="data[tokenSecret]" data-required="1" class="form-control tokenSecret" type="text"> </div>
                            </div>                           
                                                    
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>