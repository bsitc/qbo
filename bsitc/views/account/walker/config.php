<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li> 
                <li>
                    <span>Account Settings</span>
                </li>

            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['account2Name']);?> 3PL Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%"><?php echo ucwords($this->globalConfig['account2Name']);?> Id</th>
                                    <th width="25%">Store Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="walkerAccountId"></span></td>
                                    <td><span class="value" data-value="name"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="walkerAccountId"><?php echo $row['walkerAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><?php echo ucwords($this->globalConfig['account2Name']);?> Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form id="saveActionForm" action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
						<div class="form-group">
							<label class="control-label col-md-5">Channel for Customer
								<span class="required" aria-required="true"> * </span> 
							</label>
							<div class="col-md-6">
								<select name="data[consumerChannelList][]"  multiple="multiple" class="form-control chosen-select">
									<?php
									$saveChanels = explode(",",$row['consumerChannelList']);
									foreach ($data['channel'] as $channels) {
										foreach ($channels as $channel) {
											$selected = (in_array($channel['id'],$saveChanels))?('selected="selected"'):('');
											echo '<option value="'.$channel['id'].'" '.$selected.'>'.ucwords($channel['name']).'</option>';
										}
									}
									?>
								</select> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5">Channel for Trade
								<span class="required" aria-required="true"> * </span> 
							</label>
							<div class="col-md-6">
								<select name="data[tradeChannelList][]"  multiple="multiple" class="form-control chosen-select">
									<?php
									$saveChanels = explode(",",$row['tradeChannelList']);
									foreach ($data['channel'] as $channels) {
										foreach ($channels as $channel) {
											$selected = (in_array($channel['id'],$saveChanels))?('selected="selected"'):('');
											echo '<option value="'.$channel['id'].'" '.$selected.'>'.ucwords($channel['name']).'</option>';
										}
									}
									?>
								</select> 
							</div>
						</div>						
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>                 
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
            <div class="form-group">
                <label class="control-label col-md-5"><?php echo ucwords($this->globalConfig['account2Name']);?> Id
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[walkerAccountId]" data-required="1" class="form-control walkerAccountId">
                        <option value="">Select a save <?php echo strtolower($this->globalConfig['account2Name']);?> account</option>
                        <?php
                        foreach ($data['saveAccount'] as $saveAccount) {
                            echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Customer Code 
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[CustomerCode]"data-required="1" class="form-control CustomerCode" type="text"> </div>
            </div> 	
			<div class="form-group">
                <label class="control-label col-md-5">Consumer Order File Code  
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[OrderFileCode]"data-required="1" class="form-control OrderFileCode" type="text"> </div>
            </div> 	
			<div class="form-group">
                <label class="control-label col-md-5">Trade Order File Code  
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[TradeOrderFileCode]"data-required="1" class="form-control TradeOrderFileCode" type="text"> </div>
            </div> 	
			<div class="form-group">
				<label class="control-label col-md-5">Special instructions custom field
					<span class="required"> * </span>
				</label>
				<div class="col-md-6">
					<input name="data[spclinst]" class="form-control spclinst" type="text">
				</div> 
			</div>
            <div class="form-group">
                <label class="control-label col-md-5">Product Configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[Configuration]"data-required="1" class="form-control Configuration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Product Configuration custom field
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[Configuration]"data-required="1" class="form-control Configuration" type="text"> </div>
            </div>
			
			
			
			<div class="form-group">
                <label class="control-label col-md-5">Second level of product configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[secondLevelConfiguration]"data-required="1" class="form-control secondLevelConfiguration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Second level of product Made up of configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[secondLevelMadeUpConfiguration]"data-required="1" class="form-control secondLevelMadeUpConfiguration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Second level of product Made up of quantity
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[secondLevelMadeUpQuantity]"data-required="1" class="form-control secondLevelMadeUpQuantity" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Second level of product Article number
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[secondLevelArticleNumber]"data-required="1" class="form-control secondLevelArticleNumber" type="text"> </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-5">Third level of product configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[thirdLevelConfiguration]"data-required="1" class="form-control thirdLevelConfiguration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Third level of product Made up of configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[thirdLevelMadeUpConfiguration]"data-required="1" class="form-control thirdLevelMadeUpConfiguration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Third level of product Made up of quantity
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[thirdLevelMadeUpQuantity]"data-required="1" class="form-control thirdLevelMadeUpQuantity" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Third level of product Article number
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[thirdLevelArticleNumber]"data-required="1" class="form-control thirdLevelArticleNumber" type="text"> </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-5">Fourth level of product configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[fouthLevelConfiguration]"data-required="1" class="form-control fouthLevelConfiguration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Fourth level of product Made up of configuration
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[fouthLevelMadeUpConfiguration]"data-required="1" class="form-control fouthLevelMadeUpConfiguration" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Fourth level of product Made up of quantity
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[fouthLevelMadeUpQuantity]"data-required="1" class="form-control fouthLevelMadeUpQuantity" type="text"> </div>
            </div>
			<div class="form-group">
				<label class="control-label col-md-5">Fourth level of product Article number
					<span class="required"> * </span>
				</label>
				<div class="col-md-6">
					<input name="data[fouthLevelArticleNumber]"data-required="1" class="form-control fouthLevelArticleNumber" type="text">
				</div> 
			</div>
			
			
			
        </div> 
    <?php } ?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
$(".chosen-select").chosen({width: "100%"}); 
jQuery(".actioneditbtn").on("click",function(){
	 $(".chosen-select").trigger("liszt:updated");
});
</script>