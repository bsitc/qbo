<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span> 
                </li>

            </ul>
        </div>
        <h3 class="page-title"> <?php echo $this->globalConfig['account2Name'];?>
            <small><?php echo $this->globalConfig['account2Name'];?></small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i> <?php echo $this->globalConfig['account2Name'];?> Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="25%"> <?php echo $this->globalConfig['account2Name'];?> Id</th>
                                    <th width="25%"><?php echo $this->globalConfig['account2Name'];?> Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="name"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="walmartAccountId"><?php echo $row['walmartAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"> <?php echo $this->globalConfig['account2Name'];?> Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
			<div class="form-group">
				<label class="control-label col-md-4"> <?php echo $this->globalConfig['account2Name'];?> Id
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-7">
					<select name="data[walmartAccountId]" data-required="1" class="form-control walmartAccountId">
						<option value="">Select a save <?php echo $this->globalConfig['account2Name'];?> account</option>
						<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
						?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Default Product Identifier <span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[productIdentifier]" data-required="1" class="form-control productIdentifier" type="text"></div>
			</div>			
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Fetch Order Status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[fetchOrderStatus]" data-required="1" class="form-control fetchOrderStatus">
                        <?php
                        foreach ($data['orderStatus'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Acknowledged Order Status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[ackOrderStatus]" data-required="1" class="form-control ackOrderStatus">
                        <?php
                        foreach ($data['orderStatus'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Shipped Order Status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[shippedOrderStatus]" data-required="1" class="form-control shippedOrderStatus">
                        <?php
                        foreach ($data['orderStatus'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Canceled Order Status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[cancelOrderStatus]" data-required="1" class="form-control cancelOrderStatus">
                        <?php
                        foreach ($data['orderStatus'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			
			
			

        </div> 
    <?php } ?>
</div>