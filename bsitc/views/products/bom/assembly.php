<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Product Details</span> 
			</li>
		</ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE-->
	<h3 class="page-title"> Products
		<small>Assembly of products</small>
	</h3>
	<!-- END PAGE TITLE-->
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i>Assembly Product Listing </div>	
					<div class="actions">
						<a href="<?php echo base_url('products/assembly/addNewAssembly');?>" class="btn btn-circle btn-info"> 
							<i class="fa fa-plus"></i>
							<span class="hidden-xs"> Create New Assembly </span>
						</a>												
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">						
						<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="1%"><input type="checkbox" class="group-checkable"> </th>
									<th width="7%"> Auto Assembly </th>
									<th width="10%"> Created By </th>
									<th width="10%"> Assembly&nbsp;Id </th>
									<th width="10%"> <?php echo $this->globalConfig['account1Name'];?>&nbsp;Product Id </th>
									<th width="15%"> <?php echo $this->globalConfig['account1Name'];?>&nbsp;Product SKU </th>	
									<th width="15%"> <?php echo $this->globalConfig['account1Name'];?>&nbsp;Product Name </th>	
									<th width="15%"> Created </th>	
									<th width="10%"> Actions </th>
								</tr>
								<tr role="row" class="filter">
									<td> </td> 
									<td> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="username"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="createdId"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="productId"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="sku"> </td>	
									<td><input type="text" class="form-control form-filter input-sm" name="name"> </td>	
									<td>
										<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
										<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control form-filter input-sm" readonly name="updated_to " placeholder="To">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</td>
									<td>
										<div class="margin-bottom-5">
											<button class="btn btn-sm btn-success filter-submit margin-bottom">
												<i class="fa fa-search"></i> Search</button>
										<button class="btn btn-sm btn-default filter-cancel">
											<i class="fa fa-times"></i> Reset</button>
										</div>
									</td>
								</tr>
							</thead>
							<tbody> </tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<!-- END CONTENT BODY -->
</div>		
<script src="<?php echo $this->config->item('script_url');?>assets/pages/scripts/ecommerce-productsassembly.js" type="text/javascript"></script>
