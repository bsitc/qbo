<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li> 
				<span>Assembly Details</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE--> 
	<h3 class="page-title"> Assembly
		<small> Details</small>
	</h3>
	<!-- END PAGE TITLE-->
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<form action="<?php echo base_url('products/assembly/saveassembly');?>" method = "post" id="assemblyform" >
			<!-- Begin: life time stats -->
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption" style="width: 100%;">
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover table-checkable">
								<thead>
									<tr>
										<th width="15%">Assembly Id</th>
										<th width="10%">Product ID</th>
										<th width="10%">Product SKU</th>
										<th width="15%">Product Name</th>
										<th width="12%">Warehouse</th>										
										<th width="5%">Qty</th>										
										<th width="12%">Recipe</th> 										
										<th width="17%">Created</th>										
									</tr>
								</thead>
								<tbody>
								<?php 
								foreach($allproducts as $allproduct){ 
									if($allproduct['isAssembly']){ ?> 
										<tr>
											<td><?php echo $allproduct['createdId'];?></td>
											<td><?php echo $allproduct['productId'];?></td>
											<td><?php echo $allproduct['sku'];?></td>
											<td><?php echo $allproduct['name'];?></td>
											<td><?php echo $warehouseList[$allproduct['warehouse']]['name'];?></td>
											<td><?php echo $allproduct['qty'];?></td>
											<td><?php echo '('.$allproduct['receipId'].') '.@$recipeData[$allproduct['receipId']]['recipename'];?></td>
											<td><?php echo date('M d,Y H:i:s',strtotime($allproduct['created']));?></td> 
										</tr>
									<?php }
								}
								
								?>
									
								</tbody>
							</table>
						</div>					
					</div>					
				</div>				
				<div class="portlet-body">
					<div class="portlet-title">
						<div class="caption" style="width: 100%;">
							<h3 class="page-title"> Component Details</h3>			
						</div>					
					</div>
					<div class="table-container">	
						<table class="table table-striped table-bordered table-hover table-checkable receipecontainer datatable_products" id="datatable_products">	
							<thead>
								<tr>
									<th width="5%">Recipe #</th>
									<th width="20%">Component Brightpearl Product Id</th>
									<th width="20%">Component Brightpearl Product SKU</th>
									<th width="20%">Component Brightpearl Product Name</th>
									<th width="20%" >Warehouse</th>
									<th width="5%" >Qty</th>									
								</tr>
							</thead>
							<tbody>
								<?php 
								foreach($allproducts as $allproduct){
									if($allproduct['isAssembly'] == '0'){ ?> 
										<tr>
											<td><?php echo $allproduct['receipId'];?></td>
											<td><?php echo $allproduct['productId'];?></td>
											<td><?php echo $allproduct['sku'];?></td>
											<td><?php echo $allproduct['name'];?></td>
											<td><?php echo $warehouseList[$allproduct['warehouse']]['name'];?></td>
											<td><?php echo $allproduct['qty'];?></td>
										</tr>
									<?php }
								}
								
								?>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
			
		</form>
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<br><br><br><br><br><br>
<!-- END CONTENT BODY -->
</div>
