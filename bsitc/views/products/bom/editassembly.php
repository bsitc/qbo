<?php 
$config = $this->db->get('account_brightpearl_config')->row_array();
?>
<style>
	#sourecewarehouse .show{display: contents !important;}
</style>
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li> 
				<span>Assembly Details</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE--> 
	<h3 class="page-title"> Assembly
		<small> Details</small>
	</h3>
	<!-- END PAGE TITLE-->
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<form action="<?php echo base_url('products/assembly/saveassembly');?>" method = "post" id="assemblyform" >
			<input type="hidden" name="data[productId]" value="<?php echo $productId;?>" />
			<input type="hidden" name="data[sku]" value="<?php echo $products['sku'];?>" />
			<input type="hidden" name="data[name]" value="<?php echo $products['name'];?>" />
			<!-- Begin: life time stats -->
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption" style="width: 100%;">
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover table-checkable">
								<thead>
									<tr>
										<th width="17%">Brightpearl Product ID</th>
										<th width="17%">Brightpearl Product SKU</th>
										<th width="17%">Brightpearl Product Name</th>
										<?php
										sort($warehouseList);
										$listTemps = array();
										$width = 50 / count($warehouseList);
										foreach ($warehouseList as $warehouse) {
											$listTemps[] = $warehouse['id'];
											echo '<td width="'.$width.'%">'.$warehouse['name'].'</td>';
										}
										?>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $products['productId'];?></td>
										<td><?php echo $products['sku'];?></td>
										<td><?php echo $products['name'];?></td>
										<?php
											foreach ($listTemps as $listTemp) {
												echo '<td width="'.$width.'%">'.@(int)$productStock[$products['productId']]['warehouses'][$listTemp]['onHand'].'</td>';
											}
										?>
													
									</tr>
								</tbody>
							</table>
						</div>					
					</div>					
				</div>
				<div class="portlet-title">
					<div class="caption" style="width: 100%">
						<div class="form-group col-md-6">
                            <label class="control-label col-md-3">Select Recipe</label>
                            <div class="col-md-7">
                                <select class="form-control receipeidselect" name="data[receipeid]" >
									<option value="0" data-bomQty="0">Select Recipe</option>
									<?php foreach ($billcomponents as $receipeid => $billcomponent) { ?>
									<option value="<?php echo $receipeid;?>" data-bomQty="<?php echo $billcomponent['0']['bomQty'];?>"><?php echo '('.$receipeid.') '.$billcomponent['0']['recipename'];?></option>
									<?php } ?>
								</select>
                            </div>
                        </div>
						<div class="form-group col-md-6">
                            <label class="control-label col-md-3" style="font-size:20px;">BOM Recipe Qty : <span class="productQty"></span></label>
                        </div>
						
					</div>					
				</div>
				<div class="portlet-body">
					<div class="table-container">	
						<table class="table table-striped table-bordered table-hover table-checkable receipecontainer datatable_products hide" id="datatable_products">							
							<tbody>
								<tr>
									<td width="5%">Recipe #</td>
									<td width="20%">Component Brightpearl Product SKU</td>
									<td width="20%">Component Brightpearl Product Name</td>
									<td width="5%" >Qty</td>
									<?php
									sort($warehouseList);
									$listTemps = array();$defaultDisplay = 0; $maxDisambleArray = array(); $wareHouseListDatas = array();
									$width = 50 / count($warehouseList);
									foreach ($warehouseList as $warehouse) {
										$listTemps[] = $warehouse['id'];
										echo '<td width="'.$width.'%">'.$warehouse['name'].'</td>';
									}
									?>
								</tr>
								<?php 
								$proBinLocationCheck = array();
								if(isset($productStock[$products['productId']]['warehouses']))
								foreach($productStock[$products['productId']]['warehouses'] as $warehouseId => $warehousesTemp){
									if(isset($warehousesTemp['byLocation']))
									foreach($warehousesTemp['byLocation'] as $bLocId => $byLocation){
										$proBinLocationCheck[$warehouseId][$bLocId] = $byLocation['onHand'];
										break;
									}									
								}
								foreach ($billcomponents as $receipeid => $billcomponent) {
									$receipeid = ($receipeid)?($receipeid):'1';
									?>
									<tr class="receipid<?php echo $receipeid;?>" style="display: none;">
										<td colspan="<?php echo (4 + count($warehouseList));?>" class="receipe">
											<table class="table table-striped table-bordered table-hover">
											<?php foreach ($billcomponent as $billcompo) { ?>
												<tr data-id="<?php echo $billcompo['id'];?>">
													<td width="5%"><span class="receipeid"><?php echo $receipeid;?></span></td>
													<td width="20%"><input value="<?php echo $billcompo['sku'];?>" readonly="true" placeholder="Enter sku" class="atutocomplate form-control ui-autocomplete-input" autocomplete="off" type="text"></td>
													<td width="20%"><input value="<?php echo $billcompo['name'];?>" readonly="true"  placeholder="Enter name" class="name form-control" type="text"></td>
													<td width="5%"><input value="<?php echo $billcompo['qty'];?>" placeholder="Qty" readonly="true"  class="qty form-control pro<?php echo $billcompo['componentProductId'];?>" style="width:80px;" type="text"></td>
													<?php
														foreach ($listTemps as $listTemp) {
															echo '<td width="'.$width.'%">'.@(int)$productStock[$billcompo['componentProductId']]['warehouses'][$listTemp]['onHand'].'</td>';
															$proQtyDatas = @$productStock[$billcompo['componentProductId']]['warehouses'][$listTemp];	
															if(@$proQtyDatas['byLocation'])
															foreach($proQtyDatas['byLocation'] as $loationId => $byLocation){
																$wareHouseListDatas[$billcompo['componentProductId']][$listTemp][$loationId] = $byLocation['onHand'];
															}
														}	
													?>
												</tr>											
											<?php } 												
											?>
											</table>
										</td>
									</tr>
									<?php }
										foreach($wareHouseListDatas as $productId => $wareHouseListData1){
											foreach($wareHouseListData1 as $warehouseId => $wareHouseListData){
												arsort($wareHouseListData);
												$sortTemp = $wareHouseListData;
												$wareHouseListDatas[$productId][$warehouseId] = $sortTemp;
												foreach($sortTemp as $key => $val){
													$wareHouseListDatas[$productId][$warehouseId]['maxdata'] = array('location' => $key, 'value' => $val);
													break;
												}
											}
										}
										$maxDisambleArray = array();
										foreach($billcomponents as $recipeId => $billcomponent){
											foreach($billcomponent as $billcomps){
												if(@$productBySku[strtolower($billcomps['sku'])]['isStockTracked']){
													$maxAssemble = 0;$tempWareHouses = array();
													if(@$wareHouseListDatas[$billcomps['componentProductId']]){
														foreach($wareHouseListDatas[$billcomps['componentProductId']] as $warehouseId => $wareHouseListData){
															$tempWareHouses[$warehouseId] = $wareHouseListData['maxdata']['value'];
														}
													}
													arsort($tempWareHouses);
													foreach($tempWareHouses as $warehouse => $tempWareHouse){
														break;
													}														
													$maxDisambleArray[$recipeId][$billcomps['componentProductId']] = array(
														'sku' 					=> $billcomps['sku'],
														'componentProductId' 	=> $billcomps['componentProductId'],
														'warehouse' 			=> $warehouse,
														'qty' 		=> @(int)$wareHouseListDatas[$billcomps['componentProductId']][$warehouse]['maxdata']['value'],
														'binlocation' => @(int)$wareHouseListDatas[$billcomps['componentProductId']][$warehouse]['maxdata']['location'],
													);
												}
											}
										}

									?>
							</tbody>							
						</table>
					</div>
				</div>

				<div class="portlet-title step3" style="display: none;">
					<div class="caption" style="width: 100%;">
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover table-checkable">
								<thead>
									<tr>
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Qty to assemble</label>
					                            <div class="col-md-4"> 
					                                <input type="text" name="data[qtydiassemble]" class="form-control qtydiassemble">
					                            </div>
					                        </div>
											Max :<span class="qtydisthmax"><span class="qtydisthmaxval"><?php 
												$defaultDisplay = 0;
												foreach($maxDisambleArray as $maxDisambleArra){
													foreach($maxDisambleArra as $maxDisamble){
														$defaultDisplay = $maxDisamble;
														break;
													}
												}			 								
											//echo (string)$defaultDisplay;
											?></span></span>
                    					</th>
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Target warehouse</label>
					                            <div class="col-md-6">
					                                <select class="form-control targetwarehouse" name="data[targetwarehouse]" >
														<?php
														$selectedTargeWarehouse = 0;
														foreach ($warehouseList as $warehouse) {
															if(!$selectedTargeWarehouse) $selectedTargeWarehouse = $warehouse['id'];
															?>
															<option value="<?php echo $warehouse['id'];?>"><?php echo $warehouse['name'];?></option>
														<?php } ?>
													</select>
					                            </div>
					                        </div>
										</th>
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Costing Method</label>
					                            <div class="col-md-6">
					                                <select class="form-control costingmethod" name="data[costingmethod]" >
														<option value="<?php echo $config['costPriceListbom'];?>">Cost Pricelist</option>
													</select>
					                            </div>
					                        </div>
										</th>
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Bin Location</label>
					                            <div class="col-md-6">
					                                <select class="form-control targetBinLocation" name="data[targetBinLocation]" >
														<?php														
														if(!$products['binlocation']){
															$products['binlocation'] = $config['location'];
														}
														if(@!$proBinLocationCheck[$selectedTargeWarehouse]){
															$proBinLocationCheck[$selectedTargeWarehouse][$products['binlocation']] = $products['binlocation'];
														}														
														foreach($getAllWarehouseLocation as $accountId => $getAllWarehouses){
															foreach($getAllWarehouses as $getAllWarehouse){
																$selected = '';
																if(@$proBinLocationCheck[$selectedTargeWarehouse][$getAllWarehouse['id']])
																$selected = 'selected="selected"';
																echo '<option value="'.$getAllWarehouse['id'].'" '.$selected.'>'.$getAllWarehouse['name'].'</option>';
															}												
														} 
														?>
													</select>
					                            </div>
					                        </div>
										</th>
										
									</tr>
								</thead>								
							</table>
						</div>					
					</div>					
				</div>
				<div class="step4" style="display: none;">
					<div class="portlet-title">
						<div class="caption" style="width: 100%;">
							<h3 class="page-title"> Source warehouses</h3>			
						</div>					
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable datatable_products" id="sourecewarehouse">
							<tbody>
								<tr class="show">
									<td width="10%">Recipe #</td>
									<td width="25%">Component Brightpearl Product SKU</td>
									<td width="25%">Component Brightpearl Product Name</td>
									<td width="20%" >Source warehouse</td>
									<td width="20%" >Source bin warehouse</td>
								</tr>
								<?php foreach ($billcomponents as $receipeid => $billcomponent) { ?>
									<tr class="receipid<?php echo $receipeid;?>" style="display: none;">
										<td colspan="5" class="receipe">
											<table class="table table-striped table-bordered table-hover">
											<?php foreach ($billcomponent as $billcompo) { 
											if(@$productBySku[strtolower($billcompo['sku'])]['isStockTracked']){
											?>
												<tr data-id="<?php echo $billcompo['id'];?>" class="pro<?php echo $receipeid.$billcompo['componentProductId'];?>">
													<td width="10%"><span class="receipeid"><?php echo ($receipeid)?($receipeid):'1';?></span></td>
													<td width="25%"><input value="<?php echo $billcompo['componentProductId'];?>" type="hidden" name="data[<?php echo $receipeid;?>][productId][]"><input value="<?php echo $billcompo['sku'];?>" readonly="true" placeholder="Enter sku" name="data[<?php echo $receipeid;?>][sku][]" class="atutocomplate form-control ui-autocomplete-input" autocomplete="off" type="text"></td>
													<td width="25%"><input value="<?php echo $billcompo['name'];?>" readonly="true"  placeholder="Enter name" name="data[<?php echo $receipeid;?>][name][]" class="name form-control" type="text"></td>
													<td width="20%">
														<select class="form-control sourcewarehouse" name="data[<?php echo $receipeid;?>][sourcewarehouse][]" >
														<?php
														foreach ($warehouseList as $warehouse) { ?>
															<option value="<?php echo $warehouse['id'];?>" ><?php echo $warehouse['name'];?></option>
														<?php } ?>
													</select>
													</td>
													<td width="20%">
														<div class="form-group">
															<label class="control-label col-md-5">Bin Location</label>
															<div class="col-md-6">
																<select class="form-control sourceBinLocation" name="data[<?php echo $receipeid;?>][sourceBinLocation][]" > 
																	<option value="">Select bin location</option>
																	<?php
																	foreach ($listTemps as $listTemp) {
																		$binProductChecks = $productStock[$billcompo['componentProductId']]['warehouses'][$listTemp];
																		if(@$binProductChecks['byLocation']){
																			foreach($binProductChecks['byLocation'] as $loationId => $byLocation){
																				echo '<option class="warehouse'.$listTemp.' location'.$loationId.'" data-qty="'.$byLocation['onHand'].'"  data-warehouse="'.$listTemp.'"  data-location="'.$getAllWarehouses[$loationId]['name'].'"  data-receipeid="'.$receipeid.'" data-compId="'.$billcompo['componentProductId'].'" value="'.$getAllWarehouses[$loationId]['id'].'" >'.$getAllWarehouses[$loationId]['name'].' - '.$byLocation['onHand'].'</option>';
																			}
																		} 
																	}
																	?>
																</select>
															</div>
														</div>
													</td>												
												</tr>											
											<?php }} ?>
											</table>
										</td>
									</tr>
									<?php }   ?>

							</tbody>
							<tfoot class="footaction hide">
								<tr><td colspan="4"></td></tr>
								<tr>
									<td colspan="4">
										<button class="btn btn-circle btn-info btnsavediassembly"> 
											<i class="fa fa-save"></i>
											<span class="hidden-xs"> Submit </span>
										</button>
										<a href="<?php echo base_url('/products/assembly');?>" class="btn btn-circle red">
											<i class="fa fa-close"></i>
											<span class="hidden-xs"> Cancel </span>
										</a>																			
									</td>
								</tr>
							</tfoot>

						</table>
					</div>
				</div>

			</div>
			<div class="message">
				<div class="alert alert-danger" style="display: none;">
				  <strong>Success!</strong> Indicates a successful or positive action.
				</div>
				<div class="alert alert-success" style="display: none;">
				  <strong>Success!</strong> assembly successfully created.
				</div>

			</div>
		</form>
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<br><br><br><br><br><br>
<style>
body #datatable_products {
    margin-bottom: 10px !important;
}
</style>
<!-- END CONTENT BODY -->
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
	var wareHouseListDatas = <?php echo json_encode($wareHouseListDatas)?>; 
	var maxDisambleArray = <?php echo json_encode($maxDisambleArray)?>; 
	bomQty = 0; receipid = 0;
	function calculateMaxAssemble(){
		maxDisambleArrayObjs = maxDisambleArray[receipid];
		qtydisthmaxval = 99999999;
		for(key in maxDisambleArrayObjs){
			maxDisambleArrayObj = maxDisambleArrayObjs[key];
			warehouseclass = ".pro"+receipid+key+" .sourcewarehouse option[value='"+maxDisambleArrayObj['warehouse']+"']";
			if(jQuery(warehouseclass).length){
				jQuery(warehouseclass).attr('selected',true);
			}
			bin = ".pro"+receipid+key+" .sourceBinLocation .warehouse"+maxDisambleArrayObj['warehouse']+".location"+maxDisambleArrayObj['binlocation'];
			if(jQuery(bin).length){
				jQuery(bin).attr('selected',true);
			}	
			onHandQty 		= parseInt(maxDisambleArrayObj['qty']);
			compQty 		= parseInt(jQuery(".receipid"+receipid+" .qty.pro"+key).val());
			if(onHandQty > 0){
				t1 = (onHandQty / compQty) * bomqty;
				if(t1 < qtydisthmaxval){
					qtydisthmaxval = t1;
				}
			}
			else{
				qtydisthmaxval = 0;
			}
			
		}
		if(qtydisthmaxval == 99999999){
			qtydisthmaxval = 0;
		}
		else{
			if(qtydisthmaxval < bomqty){					
				qtydisthmaxval = 0;
			}
			else{
				qtydisthmaxval = qtydisthmaxval - (qtydisthmaxval % bomqty);
			}
		}
		jQuery(".qtydisthmaxval").html(qtydisthmaxval);
	}
	jQuery(".receipeidselect").on("change",function(){
		receipid = jQuery(this).val();
		jQuery(".productQty").html('');
		if(receipid != 0){
			bomqty = parseInt(jQuery(this).find("option[value='"+receipid+"']").attr('data-bomqty')); 
			jQuery(".datatable_products").removeClass("hide");
			jQuery(".datatable_products").show();
			jQuery(".step3").show();
			jQuery(".datatable_products > tbody > tr").hide();
			jQuery(".datatable_products > tbody > tr").eq('0').show();
			jQuery(".datatable_products .receipid"+receipid).show();
			targetwarehouse = jQuery(".targetwarehouse").val();
			jQuery(".productQty").html(bomqty);
			calculateMaxAssemble();
		}
	})
	jQuery(document).on("change",".sourcewarehouse",function(){		
		jQuery(this).closest("td").next("td").find(".sourceBinLocation option").hide();
		jQuery(this).closest("td").next("td").find(".sourceBinLocation option").prop('selected', false);
		jQuery(this).closest("td").next("td").find(".sourceBinLocation option.warehouse" + jQuery(this).val()).show();
	});
	jQuery(document).on("change",".sourceBinLocation",function(){		
		receipid = jQuery(this).find("option:selected").attr('data-receipeid');
		targetBinLocationClass = '.receipid'+receipid;
		qtydisthmaxval = 99999999;
		jQuery(targetBinLocationClass + ' .sourceBinLocation').each(function(){
			selectedoption = jQuery(this).find("option:selected");
			key = selectedoption.attr('data-compId');	
			onHandQty 		= selectedoption.attr('data-qty');		
			compQty 		= parseInt(jQuery(".receipid"+receipid+" .qty.pro"+key).val());
			if(onHandQty > 0){
				t1 = (onHandQty / compQty) * bomqty;
				if(t1 < qtydisthmaxval){
					qtydisthmaxval = t1;
				}
			}
			else{
				qtydisthmaxval = 0;
			} 
		})
		if(qtydisthmaxval == 99999999){
			qtydisthmaxval = 0;
		}
		else{
			if(qtydisthmaxval < bomqty){					
				qtydisthmaxval = 0;
			}
			else{
				qtydisthmaxval = qtydisthmaxval - (qtydisthmaxval % bomqty);
			}
		}
		jQuery(".qtydisthmaxval").html(qtydisthmaxval);
	});	
	jQuery(".qtydiassemble").on("change",function(){
		jQuery(".step4").show();
		jQuery(".footaction").removeClass("hide");
	})
	jQuery(".btnsavediassembly").on("click",function(e){
		e.preventDefault();
		qtydiassemble = parseInt(jQuery(".qtydiassemble").val());
		qtydisthmax = parseInt(jQuery(".qtydisthmax").html());
		productQty = parseInt(jQuery(".productQty").html());
		if(!(productQty > 0)){
			alert("Please select recipe");
			return false;
		}
		if(!(qtydiassemble > 0)){
			alert("Please enter qty to assemble ");
			return false;
		}		
		if(qtydiassemble > qtydisthmax){
			alert("You can not assemble more than : "+qtydisthmax);
			return false;
		}
		if((qtydiassemble % productQty) != 0){
			alert("Qty to assemble must be multiple of BOM Recipe Qty ( "+productQty+" )");
			return false;
		}
		if(productQty > 0){
			jQuery(".message .alert").hide();
			jQuery.ajax({ method:'post',url:jQuery("#assemblyform").attr('action') ,data:jQuery("#assemblyform").serialize(),success:function (obj) {
					res = JSON.parse(obj);	
					if(res.status == '1'){
						jQuery(".portlet").hide();
						jQuery(".message .alert-success").html(res.message);
						jQuery(".message .alert-success").show();
					}
					else{
						jQuery(".message .alert-danger").show();
						jQuery(".message .alert-danger").html(res.message);
					}
				}
			})
		}
	})

</script>	
