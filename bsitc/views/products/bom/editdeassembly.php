<?php 
$config = $this->db->get('account_brightpearl_config')->row_array();
?>
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Disassembly  Details</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE-->
	<h3 class="page-title"> Disassembly 
		<small> Details</small>
	</h3>
	<!-- END PAGE TITLE-->
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<form action="<?php echo base_url('products/deassembly/saveDeassembly');?>" method = "post" id="disassembleform" >
			<input type="hidden" name="data[productId]" value="<?php echo $productId;?>" />
			<input type="hidden" name="data[sku]" value="<?php echo $products['sku'];?>" />
			<input type="hidden" name="data[name]" value="<?php echo $products['name'];?>" />

			<!-- Begin: life time stats -->
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption" style="width: 100%;">
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover table-checkable">
								<thead>
									<tr>
										<th width="17%">Brightpearl Product ID</th>
										<th width="17%">Brightpearl Product SKU</th>
										<th width="17%">Brightpearl Product Name</th>
										<?php
										sort($warehouseList);
										$listTemps = array();
										$width = 50 / count($warehouseList);
										$maxDisambleArray = array();
										$defaultDisplay = 0;
										foreach ($warehouseList as $warehouse) {
											$listTemps[] = $warehouse['id'];
											echo '<td width="'.$width.'%">'.$warehouse['name'].'</td>';
										}
										?>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $products['productId'];?></td>
										<td><?php echo $products['sku'];?></td>
										<td><?php echo $products['name'];?></td>
										<?php
											foreach ($listTemps as $listTemp) {
												echo '<td width="'.$width.'%">'.@(int)$productStock[$products['productId']]['warehouses'][$listTemp]['onHand'].'</td>';
												if(@$listTemp){
													$maxDisambleArray[$listTemp] = @(int)$productStock[$products['productId']]['warehouses'][$listTemp]['onHand'];
													if(!$defaultDisplay)
													$defaultDisplay = @(int)$productStock[$products['productId']]['warehouses'][$listTemp]['onHand'];
												}
											}
										?>
													
									</tr>
								</tbody>
							</table>
						</div>					
					</div>					
				</div>
				<div class="portlet-title">
					<div class="caption" style="width: 100%">
						<div class="form-group col-md-6">
                            <label class="control-label col-md-3">Select Recipe</label>
                            <div class="col-md-7">
                                <select class="form-control receipeidselect" name="data[receipeid]" >
									<option value="0" data-bomQty="0">Select Recipe</option>
									<?php foreach ($billcomponents as $receipeid => $billcomponent) { ?>
									<option value="<?php echo $receipeid;?>" data-bomQty="<?php echo $billcomponent['0']['bomQty'];?>"><?php echo '('.$receipeid.') '.$billcomponent['0']['recipename'];?></option>
									<?php } ?>
								</select>
                            </div>
                        </div>
						<div class="form-group col-md-6">
                            <label class="control-label col-md-3" style="font-size:20px;">BOM Recipe Qty : <span class="productQty"></span></label>
                        </div>
					</div>					
				</div>
				<div class="portlet-body">
					<div class="table-container">	
						<table class="table table-striped table-bordered table-hover table-checkable receipecontainer datatable_products hide" id="datatable_products" style="margin-bottom: 23px !important;">							
							<tbody>
								<tr>
									<td width="5%">Recipe #</td>
									<td width="20%">Component Brightpearl Product SKU</td>
									<td width="20%">Component Brightpearl Product Name</td>
									<td width="5%" >Qty</td>
									<?php
									sort($warehouseList);
									$listTemps = array();
									$width = 50 / count($warehouseList);
									foreach ($warehouseList as $warehouse) {
										$listTemps[] = $warehouse['id'];
										echo '<td width="'.$width.'%">'.$warehouse['name'].'</td>';
									}
									?>
								</tr>
								<?php 
								if(!$products['binlocation']){
									$products['binlocation'] = $config['location'];
								}
								$defaultBinlocation = array();
								foreach ($billcomponents as $receipeid => $billcomponent) {
								$defaultBinlocation[$receipeid] = $products['binlocation'];
								?>
									<tr class="receipid<?php echo $receipeid;?>" style="display: none;">
										<td colspan="<?php echo (4 + count($warehouseList));?>" class="receipe">
											<table class="table table-striped table-bordered table-hover">
											<?php foreach ($billcomponent as $billcompo) { ?>
												<tr data-id="<?php echo $billcompo['id'];?>">
													<td width="5%"><span class="receipeid"><?php echo ($receipeid)?($receipeid):'1';?></span></td>
													<td width="20%"><input value="<?php echo $billcompo['sku'];?>" readonly="true" placeholder="Enter sku" class="atutocomplate form-control ui-autocomplete-input" autocomplete="off" type="text"></td>
													<td width="20%"><input value="<?php echo $billcompo['name'];?>" readonly="true"  placeholder="Enter name" class="name form-control" type="text"></td>
													<td width="5%"><input value="<?php echo $billcompo['qty'];?>" placeholder="Qty" readonly="true"  class="qty form-control" style="width:80px;" type="text"></td>
													<?php
														foreach ($listTemps as $listTemp) {
															echo '<td width="'.$width.'%">'.@(int)$productStock[$billcompo['componentProductId']]['warehouses'][$listTemp]['onHand'].'</td>';
														}
													?>
												</tr>											
											<?php } ?>
											</table>
										</td>
									</tr>
									<?php 
									}
									$productStocks = $productStock[$products['productId']]['warehouses'];
									$maxDisambleArray = array();
									$t1Max = array();$t2Max = array();$t3Max = array();
									foreach($productStocks as $warehouseId => $productStk){
										foreach($productStk['byLocation'] as $locationId =>  $byLocation){
											$t1Max[$locationId] = $byLocation['onHand'];
										}
										arsort($t1Max);
										foreach($t1Max as $key => $val){break;}
										$t2Max[$warehouseId] = $val;
										$t3Max[$warehouseId] = array('max' => array('location' => $key,'value' => $val));
									}
									arsort($t2Max);
									foreach($t2Max as $key => $val){break;}
									$maxDisambleArray = array(
										'warehouse' => @$key,
										'location' 	=> @$t3Max[$key]['max']['location'],
										'qty' 		=> @$val,
									);
									?>
							</tbody>							
						</table>
					</div>
				</div>

				<div class="portlet-title step3" style="display: none;">
					<div class="caption" style="width: 100%;">
						<div class="table-container">
							<table class="table table-striped table-bordered table-hover table-checkable">
								<thead>
									<tr>
										<th width="25%" class="qtydisth">
											<div class="form-group">
					                            <label class="control-label col-md-5">Qty to disassemble</label>
					                            <div class="col-md-4">
					                                <input type="text" name="data[qtydiassemble]" class="form-control qtydiassemble"> 
					                            </div>
												<span class="qtydisthmax">Max : <span class="qtydisthmaxval"><?php echo $maxDisambleArray['qty'];?></span></span>
					                        </div>
                    					</th>  
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Source warehouse</label>
					                            <div class="col-md-6">
					                                <select class="form-control targetwarehouse" name="data[targetwarehouse]" >
														<?php
														foreach ($warehouseList as $warehouse) {
														$selected = '';
														if(($maxDisambleArray['warehouse'] == $warehouse['id'])){
															$selected = 'selected="selected"';
														}
														?>
														<option <?php echo $selected;?> value="<?php echo $warehouse['id'];?>"><?php echo $warehouse['name'];?></option>
														<?php } ?>
													</select>
					                            </div>
					                        </div>
										</th>
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Costing Method</label>
					                            <div class="col-md-6">
					                                <select class="form-control costingmethod" name="data[costingmethod]" >
														<option value="<?php echo $config['costPriceListbom'];?>">Cost Pricelist</option>

													</select>
					                            </div>
					                        </div>
										</th>
										<th width="25%">
											<div class="form-group">
					                            <label class="control-label col-md-5">Bin Location</label>
					                            <div class="col-md-6">
					                                <select class="form-control targetBinLocation" name="data[targetBinLocation]" >
														<option value="">Select bin location</option>
															<?php
															foreach ($listTemps as $listTemp) {
																$binProductChecks = $productStock[$products['productId']]['warehouses'][$listTemp];
																if(@$binProductChecks['byLocation']){
																	foreach($binProductChecks['byLocation'] as $loationId => $byLocation){
																		$selected = '';
																		if(($maxDisambleArray['warehouse'] == $listTemp)&&($maxDisambleArray['location'] == $loationId)){
																			$selected = 'selected="selected"';
																		}
																		echo '<option '.$selected.' class="warehouse'.$listTemp.' location'.$loationId.'" data-qty="'.$byLocation['onHand'].'"  data-warehouse="'.$listTemp.'"  data-location="'.$getAllWarehouses[$loationId]['name'].'"  data-receipeid="'.$receipeid.'" data-proId="'.$products['productId'].'" value="'.$getAllWarehouses[$loationId]['id'].'" >'.$getAllWarehouses[$loationId]['name'].' - '.$byLocation['onHand'].'</option>';
																	} 
																} 
															}
															?>
													</select>
					                            </div>
					                        </div>
										</th>
										
									</tr>
								</thead>								
							</table>
						</div>					
					</div>					
				</div>
				<div class="step4" style="display: none;">
					<div class="portlet-title">
						<div class="caption" style="width: 100%;">
							<h3 class="page-title"> Source warehouses</h3>			
						</div>					
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable datatable_products" id="sourecewarehouse" style="margin-bottom: 23px !important;">
							<tbody>
								<tr class="show">
									<td width="5%">Recipe #</td>
									<td width="15%">Component Brightpearl Product SKU</td>
									<td width="15%">Component Brightpearl Product Name</td>
									<td width="15%">Cost Price</td>
									<td width="15%" >Destination warehouse</td>
									<td width="15%" >Bin Location</td>
								</tr>
								<?php foreach ($billcomponents as $receipeid => $billcomponent) { ?>
									<tr class="receipid<?php echo $receipeid;?>" style="display: none;">
										<td colspan="4" class="receipe">
											<table class="table table-striped table-bordered table-hover">
											<?php foreach ($billcomponent as $billcompo) { 
											if(@$productBySku[strtolower($billcompo['sku'])]['isStockTracked']){
											?>
												<tr data-id="<?php echo $billcompo['id'];?>">
													<td width="5%"><span class="receipeid"><?php echo ($receipeid)?($receipeid):'1';?></span></td>
													<td width="15%"><input value="<?php echo $billcompo['componentProductId'];?>" type="hidden" name="data[<?php echo $receipeid;?>][productId][]"><input value="<?php echo $billcompo['sku'];?>" readonly="true" placeholder="Enter sku" name="data[<?php echo $receipeid;?>][sku][]" class="atutocomplate form-control ui-autocomplete-input" autocomplete="off" type="text"></td>
													<td width="15%"><input value="<?php echo $billcompo['name'];?>" readonly="true"  placeholder="Enter name" name="data[<?php echo $receipeid;?>][name][]" class="name form-control" type="text"></td>
													<td width="15%"><input value="<?php echo $getProductPrice[$billcompo['componentProductId']];?>" placeholder="Enter cost price" name="data[<?php echo $receipeid;?>][deassemblyPrice][]" class="deassemblyPrice form-control" type="text"></td>
													<td width="15%">
														<select class="form-control sourcewarehouse" name="data[<?php echo $receipeid;?>][sourcewarehouse][]" >
														<?php
														foreach ($warehouseList as $warehouse) { ?>
															<option value="<?php echo $warehouse['id'];?>"><?php echo $warehouse['name'];?></option>
														<?php } ?>
													</select>
													</td>
													<td width="15%">
														<div class="form-group">
															<label class="control-label col-md-5">Bin Location</label>
															<div class="col-md-6">
																<select class="form-control sourceBinLocation" name="data[<?php echo $receipeid;?>][sourceBinLocation][]" > 
																	<?php
																	foreach($getAllWarehouseLocation as $accountId => $getAllWarehouses){
																		foreach($getAllWarehouses as $getAllWarehouse){
																			$selected = ($getAllWarehouse['id'] == $products['binlocation'])?('selected="selected"'):('');
																			echo '<option value="'.$getAllWarehouse['id'].'" '.$selected.'>'.$getAllWarehouse['name'].'</option>';
																		}												
																	} 
																	?>
																</select>
															</div>
														</div>
													</td>	
												</tr>											
											<?php }} ?>
											</table>
										</td>
									</tr>
									<?php } ?>

							</tbody>
							<tfoot class="footaction hide">
								<tr><td colspan="4"></td></tr>
								<tr>
									<td colspan="4">
										<button class="btn btn-circle btn-info btnsavediassembly"> 
											<i class="fa fa-save"></i>
											<span class="hidden-xs"> Submit </span>
										</button>
										<a href="<?php echo base_url('/products/deassembly');?>" class="btn btn-circle red">
											<i class="fa fa-close"></i>
											<span class="hidden-xs"> Cancel </span>
										</a>																			
									</td>
								</tr>
							</tfoot>

						</table>
					</div>
				</div>

			</div>
			<div class="message">
				<div class="alert alert-danger" style="display: none;">
				  <strong>Success!</strong> Indicates a successful or positive action.
				</div>
				<div class="alert alert-success" style="display: none;">
				  <strong>Success!</strong> Deassembly successfully created.
				</div>

			</div>
		</form>
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<br><br><br><br><br><br>
<!-- END CONTENT BODY -->
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">	
<script type="text/javascript">
	var maxDisambleArray = <?php echo json_encode($maxDisambleArray)?>; 
	var defaultBinlocation = <?php echo json_encode($defaultBinlocation); ?>; 
		jQuery(".receipeidselect").on("change",function(){
		receipid = jQuery(this).val();
		jQuery(".productQty").html('');
		if(receipid != 0){
			jQuery(".datatable_products").removeClass("hide");
			jQuery(".datatable_products").show();
			jQuery(".step3").show();
			jQuery(".datatable_products > tbody > tr").hide();
			jQuery(".datatable_products > tbody > tr").eq('0').show();
			jQuery(".datatable_products .receipid"+receipid).show();
			bomqty = jQuery(this).find("option[value='"+receipid+"']").attr('data-bomqty');
			jQuery(".productQty").html(bomqty);
			jQuery(".sourceBinLocation").find("option[value='"+defaultBinlocation[receipid]+"']").prop('selected', true);
		}
	})
	jQuery(".qtydiassemble").on("change",function(){
		jQuery(".step4").show();
		jQuery(".footaction").removeClass("hide");
	})
	
	jQuery(document).on("change",".targetwarehouse",function(){
		jQuery(this).closest("tr").find(".targetBinLocation option").hide();
		jQuery(this).closest("tr").find(".targetBinLocation option").prop('selected', false);
		jQuery(this).closest("tr").find(".targetBinLocation option.warehouse" + jQuery(this).val()).show();		
	})
	
	jQuery(document).on("change",".targetBinLocation",function(){
		jQuery(".qtydisthmaxval").html(jQuery(this).find("option:selected").attr('data-qty'));
	})	
	 
	jQuery(".btnsavediassembly").on("click",function(e){
		e.preventDefault();
		qtydiassemble = parseInt(jQuery(".qtydiassemble").val());
		productQty = parseInt(jQuery(".productQty").html());
		qtydisthmaxval = parseInt(jQuery(".qtydisthmaxval").html());
		if(!(productQty > 0)){
			alert("Please select recipe");
			return false;
		}
		if(!(qtydiassemble > 0)){
			alert("Please enter qty to assemble ");
			return false;
		}	
		if(qtydiassemble > qtydisthmaxval){
			alert("You can not assemble more than : "+qtydisthmaxval);
			return false;
		}
		if((qtydiassemble % productQty) != 0){
			alert("Qty to assemble must be multiple of BOM Recipe Qty ( "+productQty+" )");
			return false;
		}
		if(productQty > 0){
			jQuery(".message .alert").hide();
			jQuery.ajax({ method:'post',url:jQuery("#disassembleform").attr('action') ,data:jQuery("#disassembleform").serialize(),success:function (obj) {
					res = JSON.parse(obj);	
					if(res.status == '1'){
						jQuery(".portlet").hide();
						jQuery(".message .alert-success").html(res.message);
						jQuery(".message .alert-success").show();
					}
					else{
						jQuery(".message .alert-danger").show();
						jQuery(".message .alert-danger").html(res.message);
					}
				}
			})
		}
	})

</script>	
