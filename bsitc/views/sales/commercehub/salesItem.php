<?php
$status              = array('0' => 'Pending', '1' => 'Sent', '2' => 'Acknowledged', '3' => 'Partially Packed', '4' => 'Packed', '5' => 'Invoiced');
$statusColor         = array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'success', '4' => 'success', '5' => 'success');
$saveOrderInfo = json_decode($orderInfo['rowData'],true);
$delAddress = array(); $billAddress = array();
foreach($address as $addressTmp){
	if(($addressTmp['type'] == 'ST')||($addressTmp['type'] == 'BY')){
		$delAddress = $addressTmp;
	}
	else{
		$billAddress = $addressTmp;
	}
}
?>
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Order Details</span>
				</li>
			</ul>			
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title"> Order View
			<small>Order View</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- Begin: life time stats -->
				<div class="portlet light portlet-fit portlet-datatable bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-settings font-dark"></i>
							<span class="caption-subject font-dark sbold uppercase"> Order #<?php echo ($orderInfo['orderNo'])?($orderInfo['orderNo']):($orderInfo['orderId']);?>
								<span class="hidden-xs">| <?php echo ($orderInfo['created'])?(date('M d, Y H:i:s',strtotime($orderInfo['created']))):('');?></span>
							</span>
						</div>
						<div class="actions hide">
							<div class="btn-group btn-group-devided" data-toggle="buttons">
								<label class="btn btn-transparent green btn-outline btn-circle btn-sm active">
									<input type="radio" name="options" class="toggle" id="option1">Actions</label>
								<label class="btn btn-transparent blue btn-outline btn-circle btn-sm">
									<input type="radio" name="options" class="toggle" id="option2">Settings</label>
							</div>
							<div class="btn-group">
								<a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
									<i class="fa fa-share"></i>
									<span class="hidden-xs"> Tools </span>
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> Export to Excel </a>
									</li>
									<li>
										<a href="javascript:;"> Export to CSV </a>
									</li>
									<li>
										<a href="javascript:;"> Export to XML </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;"> Print Invoices </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="tabbable-line">
							<ul class="nav nav-tabs nav-tabs-lg">
								<li class="active">
									<a href="#tab_1" data-toggle="tab"> Details </a>
								</li>								
								<li>
									<a href="#tab_4" data-toggle="tab"> Acknowledgment
										<span class="badge badge-danger hide"> 2 </span>
									</a>
								</li>								
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Order Details </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-5 name"> Order #: </div>
														<div class="col-md-7 value"> <?php echo ($orderInfo['orderNo'])?($orderInfo['orderNo']):($orderInfo['orderId']);?>
														</div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Order Date & Time: </div>
														<div class="col-md-7 value"> <?php echo ($orderInfo['created'])?(date('M d, Y H:i:s',strtotime($orderInfo['created']))):('');?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Order Status: </div>
														<div class="col-md-7 value">
															 <?php echo '<span class="label label-sm label-' . @$statusColor[$orderInfo['status']] . '">' . @$status[$orderInfo['status']] . '</span>';?>
														</div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Total: </div>
														<div class="col-md-7 value"> <?php echo sprintf("%.2f",$orderInfo['totalAmount']);?></div>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Customer Information </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-5 name"> Customer Name: </div>
														<div class="col-md-7 value"> <?php echo $delAddress['fname'] . ' ' . $address['lname'];?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Email: </div>
														<div class="col-md-7 value"> <?php echo $delAddress['email'];?></div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> State: </div>
														<div class="col-md-7 value"> <?php echo $delAddress['line4'];?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Phone Number: </div>
														<div class="col-md-7 value"> <?php echo @$delAddress['telephone'];?> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Billing Address </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-12 value"> <?php echo $billAddress['fname'] . ' ' . $billAddress['lname'];?>
															<br> <?php echo $billAddress['line1'];?>
															<br> <?php echo $billAddress['line2'];?>
															<br> <?php echo $billAddress['line3'] .', '.$billAddress['line4'] .' ' .$billAddress['postalCode'];?>
															<br> <?php echo $billAddress['countryName'];?>
															<br> </div>
														
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Shipping Address </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-12 value"> <?php echo $delAddress['fname'] . ' ' . $delAddress['lname'];?>
															<br> <?php echo $delAddress['line1'];?>
															<br> <?php echo $delAddress['line2'];?>
															<br> <?php echo $delAddress['line3'] .', '.$delAddress['line4'] .' ' .$delAddress['postalCode'];?>
															<br> <?php echo $delAddress['countryName'];?>
															<br> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Order Items </div>
												</div>
												<div class="portlet-body">
												<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th> S.No</th>
																	<th> Order Row Id</th>
																	<th> Product Id</th>
																	<th> Product Sku</th>
																	<th> Quantity </th>
																	<th> Price </th>
																</tr>
															</thead>
															<tbody>
															<?php
															foreach($items as $i => $item){
															?>
																<tr>
																	<td><?php echo $i + 1; ?> </td>
																	<td> <?php echo $item['rowId'];?> </td>
																	<td>
																		<a href="javascript:;"> <?php echo $item['productId'];?> </a>
																	</td>
																	<td> <?php echo ($item['sku'])?($item['sku']):($item['venderSku']);?></td>
																	<td> <?php echo $item['qty'];?> </td>
																	<td> <?php echo sprintf("%.2f",$item['price']);?> </td>
																	
																</tr>
															<?php
															}
															?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="tab-pane" id="tab_4">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Acknowledgement Details 													
													</div>
													<div class="action">
														<a class="btn btn-circle btn-info  pull-right" style="margin-top: 3px;" href="javascript:void(0)" id="fetchSales"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp; Generate Acknowledgement File </a>
													</div>
												</div>
												<div class="portlet-body">
												<?php
												if(@$this->session->flashdata('success')){ ?>
													<div class="alert alert-success">
													  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>.
													</div>
												<?php } else
												if($data['changeFlag']['orderDeleted']){
												?>
												<div class="alert alert-warning">
												  <strong>Warning!</strong> Order has been deleted .
												</div>
												<?php } else if($data['changeFlag']['foundChange']){ 
												?>
												<div class="alert alert-warning">
												  <strong>Warning!</strong> Update found in sales order .
												</div>
												
												<?php } else { ?> 				
												<div class="alert alert-success">
												  <strong>Success!</strong> No any change found in sales order.
												</div>
												<?php } ?>
												<div class="table-responsive">
												<form action="<?php echo base_url('sales/sales/generateAckFile/');?>" method="post" id="reviewform">
													<table class="table table-hover table-bordered table-striped">
														<thead>
															<tr>
																<th>S.No</th>
																<th>BP Order #</th>
																<th>Product Id</th> 
																<th>Product Sku</th> 
																<th>Allocated Qty</th> 
																<th>Qty</th> 
																<th>Price</th> 
																<th>isDeleted</th> 
																<th>isUpdated</th> 
																<th>isNewadded</th> 
																<th>Mapped with</th> 
																<th>Action</th> 
															</tr>
														</thead>
														<tbody>
														<?php
														$count = 1;  
														foreach($data['finalDisplayArray'] as $key=>$value): ?>
														<tr>
															<td ><?php echo $count++; ?></td>
															<td ><?php echo $data['bp']['orderInfo']['id'];?></td>
															<td ><?php echo $value['productId']; ?></td>					
															<td ><?php echo @$value['productSku']; ?></td>					
															<td><?php echo (int)$value['reserveQty'];?></td>
															<td ><?php 
																$oldPrice = @ ($data['save']['items'][strtolower($value['productSku'])]['totalPrice'] / $data['save']['items'][strtolower($value['productSku'])]['qty']);
																$newPrice = @($value['totalPrice'] / $value['quantity']);
																$priceDiff = (int)($newPrice - $oldPrice);
																$qtyDiff = @(int)($value['quantity'] - @$data['save']['items'][strtolower($value['productSku'])]['qty']);
																if(@$value['isUpdated'] && ($qtyDiff != 0)){
																	echo 'New Qty : '.@$value['quantity'].'<br>Old Qty : '.@$data['save']['items'][strtolower($value['productSku'])]['qty']; 
																}
																else{
																	echo @$value['quantity'];
																}										
															?></td>		
															<td ><?php 
																if(@$value['isUpdated'] && ($priceDiff != 0 )){
																	echo 'New Price : '.sprintf("%.2f",$newPrice).'<br>Old Price : '.sprintf("%.2f",$oldPrice);  
																}
																else{
																	echo sprintf("%.2f",$newPrice);
																}										
															?></td>		
															
															<td ><?php echo @$value['isDeleted']; ?></td>					
															<td ><?php echo @$value['isUpdated']; ?></td>					
															<td ><?php echo @$value['isNewadded']; ?></td>		
															<td>
															<?php
																$showClass = 'hide';
																if(@$value['isNewadded']){
																	$showClass = '';
																}
																echo '<select name="data[mappedwith][]" class="mappedwith '.$showClass.'" >';
																echo '<option value="">Select appropriate sku</option>';
																foreach($data['save']['items'] as $items){
																	if($items){
																		echo '<option value="'.$items['sku'].'">'.$items['sku'].'</option>';
																	}
																}
																echo '</select>';
																
															?>
															</td>
															<td>
															<div class="btn-group pull-right">
																<button class="btn dropdown-toggle pull-left"  data-toggle="dropdown">
																	<i class="icon-cog"></i>
																	<span class="caret"></span>
																</button>
																<ul class="dropdown-menu">												
																	<li><a href="#" class="deleteRow">Delete row</a></li>
																	<li class="divider hide"></li>	 											
																</ul>
															</div>
															</td>
														<input type="hidden" name="data[productId][]" value="<?php echo @$value['productId']; ?>" />
														<input type="hidden" name="data[productSku][]" value="<?php echo @$value['productSku']; ?>" />
														<input type="hidden" name="data[newQty][]" value="<?php echo @$value['quantity']; ?>" />
														<input type="hidden" name="data[oldQty][]" value="<?php echo @$data['save']['items'][strtolower($value['productSku'])]['qty']; ?>" />
														<input type="hidden" name="data[newPrice][]" value="<?php echo @$value['totalPrice']; ?>" />
														<input type="hidden" name="data[isDeleted][]" value="<?php echo @$value['isDeleted']; ?>" />
														<input type="hidden" name="data[isUpdated][]" value="<?php echo @$value['isUpdated']; ?>" />
														<input type="hidden" name="data[isNewadded][]" value="<?php echo @$value['isNewadded']; ?>" />
														<input type="hidden" name="data[newPrice][]" value="<?php echo @$value['totalPrice']; ?>" />
														<input type="hidden" name="data[oldPrice][]" value="<?php echo @$data['save']['items'][strtolower($value['productSku'])]['totalPrice']; ?>" />
														</tr>															
														<?php endforeach; ?>
														<input type="hidden" name="orderId" value="<?php echo $data['bp']['orderInfo']['id'];?>" />
														</tbody>
													</table>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End: life time stats -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<script type="text/javascript">
var xhr;
jQuery("document").ready(function(){                 
	jQuery(document).on('click','.deleteRow', function(e) {
		e.preventDefault();
		jQuery(this).closest("tr").remove();
	});	
	jQuery("#fetchSales").click(function(){
		jQuery( '#reviewform').submit(); 		
	});
});  
</script>
