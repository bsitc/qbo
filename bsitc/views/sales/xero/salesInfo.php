<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Sales Order Details</span>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet ">		
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> Sales Order Data</div
					</div>		
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<?php
						echo "<pre>";
						print_r(json_decode($salesInfo['rowData'],true));
						echo "</pre>";
						?>
					</div>
				</div>
			</div>
			<div class="portlet ">		
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> Sales Order Data</div
					</div>		
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<?php
						echo "<pre>";
						print_r(json_decode($salesInfo['createdRowData'],true));
						echo "</pre>";
						?>
					</div>
				</div>
			</div>
			
			<!-- End: life time stats -->
		</div>
	</div>
</div>
</div>