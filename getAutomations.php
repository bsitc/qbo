<?php
$html	= '';
$jobs	= array();
$cpid	= posix_getpid(); 
exec('ps aux | grep php', $outputs);
foreach($outputs as $output){
	if(substr_count($output,__DIR__)){
		$runningCronArray	= preg_split('/ +/', $output);
		$cronStartTime		= strtotime($runningCronArray['8']);
		$cronStartTime		= date('Y-m-d H:i:s',$cronStartTime);
		$cronId				= $runningCronArray[1];
		$key				= '';
		$jobName			= '';
		foreach($runningCronArray as $t => $p){
			if(substr_count($p,__DIR__)){ 
				$key = $t;
				if((isset($runningCronArray[$t + 1])) AND $runningCronArray[$t + 2]){
					$jobName = $p.' '.$runningCronArray[$t + 1].' '.$runningCronArray[$t + 2];
				}
			}
		}
		if($jobName){
			$jobName	= str_replace("'","",$jobName);
		}
		if((!empty($jobs)) AND (isset($jobs[$jobName]))){
			continue;
		}
		else{
			$jobs[$jobName]	= $jobName;
		}
		$cronArray[]		= array(
			'cronId' 			=> $cronId,
			'function' 			=> $jobName,
			'cronStartTime' 	=> $cronStartTime,
		);
		$html	.=	'<tr>';
		$html	.=		'<td>'.$cronId.'</td>';
		$html	.=		'<td>'.$cronStartTime.'</td>';
		$html	.=		'<td>'.$jobName.'</td>';
		$html	.=	'</tr>';
	}
}
echo $html;