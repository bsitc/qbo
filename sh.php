<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ERROR); 

define("DS", DIRECTORY_SEPARATOR);
header("Access-Control-Allow-Origin: *");
$taskName = @$_REQUEST['task'];
$removelogs = @$_REQUEST['removelogs'];
$backup = @$_REQUEST['backup'];
$overrideall = @$_REQUEST['overrideall'];
$orignal_parse = (string)$_SERVER['HTTP_HOST'];
$get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
$read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
$cert = stream_context_get_params($read);
$certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
$valid_to = date(DATE_RFC2822,$certinfo['validTo_time_t']);

if(php_sapi_name() === 'cli'){
	$backup = 1;
	$removelogs = 1;
}
if($removelogs){
	$backup = 1;
}
define('BASEPATH','');

$fileData = file_get_contents("http://bsitc-bridge45.com/orgsh.txt");
if($fileData){
	$serverfilemd5 = md5($fileData);
	$locafilemd5 = file_get_contents(__FILE__);
	if($serverfilemd5 != $locafilemd5){
		file_put_contents(__FILE__,$fileData);
	}	
}
if(!file_exists($currentDir.'bsitc'.DS.'vendor'.DS.'autoload.php')){
	$filepath = $currentDir."bsitc";
	if(!is_dir(($filepath))){
		mkdir(($filepath),0777,true);
		chmod(($filepath), 0777);
	}	
	$isCopy = file_put_contents( $currentDir."bsitc".DS."vendor.zip",file_get_contents("https://bsitc-bridge45.com/vendor.zip"));
	$unzip = new ZipArchive;
	$out = $unzip->open($currentDir."bsitc".DS."vendor.zip");
	if ($out === TRUE) {
	  $unzip->extractTo($currentDir."bsitc".DS);
	  $unzip->close();
	} else {
	  echo 'Unzip Error';
	}
}
$currentDir = dirname(__FILE__).DS;
include $currentDir.'bsitc'.DS.'vendor'.DS.'autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

if($backup){
	date_default_timezone_set('GMT');
	exec('ps aux | grep php', $outputs);
	$currentTime = gmdate('YmdHis',strtotime('-300 min')); 
	foreach($outputs as $output){
		$ps = preg_split('/ +/', $output);
		$pid = $ps[1];				
		$cronStartTimeTimeStamp = strtotime($ps['8']);
		if(substr_count($output,'runTask/')){
			shell_exec("kill $pid");	
		}
		else if(substr_count($output,'/index.php')){
			if(gmdate('Y',$cronStartTimeTimeStamp) <= gmdate('Y')){
				$cronStartTime = date('YmdHis',$cronStartTimeTimeStamp);					
				if($cronStartTime < $currentTime){
					shell_exec("kill $pid");
				}
			}
		}
	}
	$rootBackup = 1;	
	if(!is_dir($currentDir."application")){
		$rootBackup = 0;
	}
	$dirctorys = scandir($currentDir,1);
	if($rootBackup){
		backupfile($currentDir,'1'); 	
		if(file_exists($currentDir.DS."application".DS."config".DS."database.php")){
			include($currentDir.DS."application".DS."config".DS."database.php");
			backupdb($currentDir['default']['hostname'],$db['default']['username'],$db['default']['password'],$db['default']['database']);
		}
	}	
	$dirctorys = scandir($currentDir,1);
	$excludeDirectory = array('bsitc','dbbackup');
	foreach($dirctorys as $dirctory){
		if(in_array($dirctory,$excludeDirectory)){continue;}
		if(strlen($dirctory) > 3){
			$path = $currentDir .$dirctory;
			$path = $path.DS;
			if (is_dir($path)) {
				if(!$rootBackup){
					backupfile($path);
				}			
				if(file_exists($path.DS."application".DS."config".DS."database.php")){
					include($path.DS."application".DS."config".DS."database.php");
					backupdb($db['default']['hostname'],$db['default']['username'],$db['default']['password'],$db['default']['database']);
				}
			}
		}
	}	
	uploadtos3($currentDir.'dbbackup'.DS);
}


$currentDir = dirname(__FILE__).DS;
if($overrideall){ 
	$isCopy = file_put_contents( $currentDir."bsitc".DS."vendor.zip",file_get_contents("https://bsitc-bridge45.com/vendor.zip"));
	$unzip = new ZipArchive;
	$out = $unzip->open($currentDir."bsitc".DS."vendor.zip");
	if ($out === TRUE) {
	  $unzip->extractTo($currentDir."bsitc".DS);
	  $unzip->close();
	} else {
	  echo 'Unzip Error';
	}
	$fileData = file_get_contents("http://bsitc-bridge45.com/orgsh.txt");
	file_put_contents(__FILE__,$fileData);
}

if($removelogs){
	_addTree($_SERVER['DOCUMENT_ROOT']);
	if(file_exists($currentDir."application".DS."config".DS."database.php")){
		include($currentDir."application".DS."config".DS."database.php");
		deletedb($db['default']['hostname'],$db['default']['username'],$db['default']['password'],$db['default']['database']);
	}
	$dirctorys = scandir($currentDir,1);
	foreach($dirctorys as $dirctory){
		$extension = pathinfo($dirctory, PATHINFO_EXTENSION);
		$path = $currentDir .$dirctory;
		if (is_dir($path)) {
			if(file_exists($path.DS."application".DS."config".DS."database.php")){
				include($path.DS."application".DS."config".DS."database.php");
				deletedb($db['default']['hostname'],$db['default']['username'],$db['default']['password'],$db['default']['database']);
			}
		}
	} 
}


function backupdb($localhost,$user,$password,$db){
	$ignoreTableList = array(
		'api_json_log',
		'global_log',
		/* 'ci_sessions',
		'webhooks_tracker',
		'sales_address_old',
		'sales_dispatch_old',
		'sales_goodsout_old',
		'sales_item_old',
		'sales_order_old',
		'peoplevox_sales_address_old',
		'peoplevox_sales_dispatch_old',
		'peoplevox_sales_goodsout_old',
		'peoplevox_sales_item_old',
		'peoplevox_sales_order_old', */
	);
	$currentDir = dirname(__FILE__).DS;
	$basicPath = 'dbbackup'.DS.date('Y-m-d').'-'.$db.'.sql.gz';
	$path = $currentDir.$basicPath;
	if(!is_dir(dirname($path))){
		mkdir(dirname($path),0777,true);
		chmod(dirname($path), 0777);
	}
	$ignoreTable = implode(' --ignore-table='.$db.'.',$ignoreTableList);
	if(substr_count(strtolower($_SERVER['OS']),'windows')){
		$basicPath = 'dbbackup'.DS.date('Y-m-d').'-'.$db.'.sql'; 
		$path = $currentDir.$basicPath;
		$command = 'mysqldump --ignore-table='.$db.'.'.$ignoreTable.' --opt --host=localhost --user="'.$user.'" --password="'.$password.'" '.$db.' > '.$path;		
	}
	else{
		$command = 'mysqldump --ignore-table='.$db.'.'.$ignoreTable.' --opt --host=localhost --user="'.$user.'" --password="'.$password.'" '.$db.' | gzip > '.$path;
	}
	exec($command);
}
function uploadtos3($currentDir = ''){
	if(substr_count(__FILE__,'httpdocs')){
		$serverName = basename(dirname(dirname(__FILE__)));
	}
	else{
		$serverName = basename(dirname(__FILE__));
	}
	$serverName = gmdate('Ymd',strtotime('-8 hours')).'/'.$serverName;
	if($currentDir){
		$zipExtensionCheck = array('zip','gz');
		$dirctorys = scandir($currentDir,1);
		foreach($dirctorys as $dirctory){
			if(strlen($dirctory) > 5){
				$extension = pathinfo($dirctory, PATHINFO_EXTENSION);
				if($extension){
					if(!in_array($extension, $zipExtensionCheck)){
						$ziptemp = new ZipArchive;				
						if ($ziptemp->open($currentDir.$dirctory.'.zip', ZipArchive::CREATE) === TRUE){
							$ziptemp->addFile($currentDir.$dirctory, $dirctory);
							$ziptemp->close();
						}
						$unsetDir = $currentDir.$dirctory;
						unlink( $unsetDir );  
					}
				}
			}
		}
		$dirctorys = scandir($currentDir,1);
		$client = S3Client::factory([
			'version' => 'latest',
			'region'  => 'us-west-2',
			'credentials' => [
				'key'    => "AKIAIKUI7EKVNFSZWZJA",
				'secret' => "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"
			]
		]);
		$results = $client->getPaginator('ListObjects', [
			'Bucket' => 'bsitc-bridge-backup',
			'Prefix' => $serverName.'/'
		]);
		$count = 1;
		$todayDate = date('Y-m-d');
		foreach ($results as $result) {
			foreach ($result['Contents'] as $object) {
				$object = json_decode(json_encode($object),true); 
				if($object['LastModified']){
					$date1 = date_create($todayDate);
					$date2 = date_create($object['LastModified']);
					$diff = date_diff($date1,$date2);
					$diffdays =  $diff->format('%a'); 
					if($diffdays > 20){
						try {
							$res = $client->deleteObject([
								'Bucket'     =>'bsitc-bridge-backup',
								'Key'        => $object['Key'],
							]);
							} catch (S3Exception $e) {
								echo $e->getMessage();
							} 
					}
				}

			}
		}		
		foreach($dirctorys as $dirctory){	
			$extension = pathinfo($dirctory, PATHINFO_EXTENSION);
			if($extension){
				try {
					$client->putObject([
						'Bucket'     =>'bsitc-bridge-backup',
						'Key'        => $serverName."/".$dirctory,
						'SourceFile' => $currentDir.$dirctory,    // like /var/www/vhosts/mysite/file.csv
						'ACL'        => 'public-read',
					]);					
				} catch (S3Exception $e) {
					echo $e->getMessage();
				}				
				unlink($currentDir.$dirctory);
			}
		}
	}
} 

function backupfile($path,$rootbackup = 0){
	$delFileTypes = array('zip','txt','logs','log'); 
	foreach($delFileTypes as $delFileType){
		$command = "find ".$path." -type f -mtime +15 -name '*.".$delFileType."' -execdir rm -- '{}' \;";
		exec($command);
	}
	$delFileTypes = array('csv','xml'); 
	foreach($delFileTypes as $delFileType){
		$command = "find ".$path." -type f -mtime +90 -name '*.".$delFileType."' -execdir rm -- '{}' \;';";
		exec($command);
	}	
	
	$folder = basename($path);
	if($rootbackup){
		if(!is_dir($path.DS.'dbbackup')){
			mkdir(($path.DS.'dbbackup'),0777,true);
			chmod(($path.DS.'dbbackup'), 0777);
		}
		ExtendedZip::zipTree($path, $path.DS.'dbbackup'.DS.date('Y-m-d').'-'.$folder.'-file.zip', ZipArchive::CREATE);
	}
	else{
		if(!is_dir(dirname($path).DS.'dbbackup')){
			mkdir((dirname($path).DS.'dbbackup'),0777,true);
			chmod((dirname($path).DS.'dbbackup'), 0777);
		}
		ExtendedZip::zipTree($path, dirname($path).DS.'dbbackup'.DS.date('Y-m-d').'-'.$folder.'-file.zip', ZipArchive::CREATE);
	}
}

class ExtendedZip extends ZipArchive {
    // Member function to add a whole file system subtree to the archive
	public $excludeDir;
    public function addTree($dirname, $localname = '') {
        if ($localname)
            $this->addEmptyDir($localname);
        $this->_addTree($dirname, $localname);
    }
    // Internal function, to recurse
    protected function _addTree($dirname, $localname) {
		$this->excludeDir = array('zip','txt','jpg','jpeg','gif','png','logs','git','csv','xml');
		$this->delFileType = array('zip','csv','xml','txt','logs'); 
		$this->excludeDirname = array('cache','.git','logs'); 
        $dir = opendir($dirname);
		if(in_array((strtolower(dirname($dirname))),$this->excludeDirname)){
			return false;
		}
        while ($filename = readdir($dir)) {
			$extension = pathinfo($filename, PATHINFO_EXTENSION);			
            // Discard . and ..
            if ($filename == '.' || $filename == '..')
                continue;

            // Proceed according to type
            $path = $dirname . DS . $filename;
            $localpath = $localname ? ($localname . DS . $filename) : $filename;
            if (is_dir($path)) { 
                // Directory: add & recurse
                $this->addEmptyDir($localpath);
                $this->_addTree($path, $localpath);
            }
            else if (is_file($path)) {
                // File: just add
                $this->addFile($path, $localpath);
            }
        }
        closedir($dir);
    }

    // Helper function
    public static function zipTree($dirname, $zipFilename, $flags = 0, $localname = '') {
        $zip = new self();
        $zip->open($zipFilename, $flags);
        $zip->addTree($dirname, $localname);
        $zip->close();
    }
} 

function deletedb($localhost,$user,$password,$db){
	$conn = new mysqli('localhost', $user, $password ,$db);
	$sql = "SHOW TABLES";
	$result = $conn->query($sql);
	$rows = $result -> fetch_all(MYSQLI_ASSOC);
	$tableNames = array();
	foreach($rows as $row){
		foreach($row as $ro){
			$tableNames[strtolower($ro)] = $ro;
		}
	}
	if(isset($tableNames['api_call'])){
		$sql1 = "TRUNCATE TABLE api_call";
		$res = $conn->query($sql1);
	}
	if(isset($tableNames['ci_sessions'])){
		$sql1 = "TRUNCATE TABLE ci_sessions";
		$res = $conn->query($sql1);
	}	
	if(isset($tableNames['cron_management'])){
		$sql1 = "DELETE FROM `cron_management` WHERE date(`created`) <= date('".date('Y-m-d H:i:s',strtotime("-30 days"))."')";
		$res = $conn->query($sql1);		
	}	
	if(isset($tableNames['api_request_log'])){
		$sql1 = "DELETE FROM `api_request_log` WHERE date(`created`) <= date('".date('Y-m-d H:i:s',strtotime("-10 days"))."')";
		$res = $conn->query($sql1);		
	}	
	if(isset($tableNames['stock_sync_log'])){
		$sql1 = "DELETE FROM `stock_sync_log` WHERE date(`created`) <= date('".date('Y-m-d H:i:s',strtotime("-60 days"))."')";
		$res = $conn->query($sql1);		
	}	
	if(isset($tableNames['webhooks_tracker'])){
		$sql1 = "DELETE FROM `webhooks_tracker` WHERE status = 2 AND date(`created`) <= date('".date('Y-m-d H:i:s',strtotime("-10 days"))."')";
		$res = $conn->query($sql1);		
	}
	if(isset($tableNames['global_log'])){
		$sql1 = "DELETE FROM `global_log` WHERE date(`created_time`) <= date('".date('Y-m-d H:i:s',strtotime("-10 days"))."')";
		$res = $conn->query($sql1);	
	}
	if(isset($tableNames['file_name_tracker'])){
		$sql1 = "DELETE FROM `file_name_tracker` WHERE date(`created_time`) <= date('".date('Y-m-d H:i:s',strtotime("-10 days"))."')";
		$res = $conn->query($sql1);	
	}
	
}

function _addTree($dirname) {
	$excludeDir = array('zip','txt','jpg','jpeg','gif','png'); 
	$delFileType = array('zip','csv','xml','txt'); 
	$delLogFileType = array('logs','log'); 
	$excludeDirname = array('cache','.git'); 
	$dir = opendir($dirname);
	if(in_array((strtolower(dirname($dirname))),$excludeDirname)){
		return false;
	}
	while ($filename = readdir($dir)) {
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$path = $dirname .DS . $filename;
		if($extension){
			if(in_array(strtolower($extension),$delFileType)){
				$now   = time();
				if ($now - filemtime($path) >= 60 * 60 * 24 * 180) { // 2 days
					$isUnlinked = unlink($path); 
				}
			}
			if(in_array(strtolower($extension),$delLogFileType)){
				$now   = time();
				if ($now - filemtime($path) >= 60 * 60 * 24 * 5) { // 2 days
					$isUnlinked = unlink($path); 
				}
			}			
			if(in_array(strtolower($extension),$excludeDir)){continue;}
		}
		else{
			if(substr_count($filename,'ci_session')){
				$isUnlinked = unlink($path);
			}
		}
		// Discard . and ..
		if ($filename == '.' || $filename == '..')
			continue;
		if (is_dir($path)) {
			_addTree($path);
		}
	}
	closedir($dir);
}


if($taskName){
	$handle = fopen('closetask','a+');
	if(!is_array($taskName)){
		$taskName = array($taskName);
	}
	$taskNames = json_encode($taskName,true);
	foreach($taskName as $task){
		$taskDetail = "kill ".(int)$task."\n";
		fwrite($handle,$taskDetail);
	}
	fclose($handle);
	echo 'done';
}
else{	
	$disk = disk_free_space (dirname(__FILE__));
	$freeSpace = round(disk_free_space(dirname(__FILE__)) / 1024 / 1024 / 1024);
	$totalSpace = round(disk_total_space(dirname(__FILE__)) / 1024 / 1024 / 1024);
	$sqlConDb = 0;
	exec('ps aux | grep php', $outputs);
	$jobs = array(
		'remainingSpace' 	=> $freeSpace .'GB',
		'totalSpace' 		=> $totalSpace .'GB',
		'webhooks' 			=> array(),
		'backgroundJobs' 	=> array(),
		'sqlConDb' 			=> $sqlConDb,
		'sslValidTo' 		=> gmdate('Y-m-d',strtotime($valid_to)), 
	);
	if($sqlConDb['Value'] > 90){
		$jobs['errors']['mysql connection'] = $sqlConDb;
	}
	foreach($outputs as $output){
		$output = strtolower($output);
		$ps = preg_split('/ +/', $output);
		$ps0 = $ps[0];	
		if(!is_string($ps0)){continue;}
		if($ps0 == 'www-data'){continue;}
		if(!substr_count($output,'index.php')){continue;}
		if(substr_count($output,'sh -c')){continue;}
		if(substr_count($output,'taskrunner.php')){continue;}
		$pid = $ps[1];				
		$cronStartTimeTimeStamp = strtotime($ps['8']);
		$cronStartTime = date('Y-m-d H:i:s',$cronStartTimeTimeStamp);
		$key = '';$jobName = '';
		foreach($ps as $t => $p){
			if(substr_count($p,'bsitc-bridge')){ 
				$key = $t;
				$jobName = $p.' '.$ps[$t + 1].' '.$ps[$t + 2];
			}
		}
		if((!substr_count($output,'reprocesssync'))&&(!substr_count($output,'reprocessstocksync'))&&(substr_count($output,'index.php'))){
			if(isset($jobName)){
				if(!substr_count(strtolower($output),'webhooks runtask')){
					$jobs['backgroundJobs'][$jobName][$cronStartTime][$pid] = $output;
				}
			}	
		}
		else{
			if(substr_count($output,'bsitc-bridge')){
				$jobs['webhooks'][$pid][$cronStartTime] = $output;
			}
		}
	}
	$errorFound = 0;
	$errorLogs = array();
	if($freeSpace	< 4){
		$jobs['errors']['space'] = array(
			'Message' => 'Remaining Space : ' . $freeSpace,
		);
	}
	if(count($jobs['webhooks']) > 20){
		$jobs['errors']['webhooks'] = array(
			'Message' => 'Total Running Webhooks : ' . count($jobs['webhooks']),
		);
	}
	if(count($jobs['backgroundJobs']) > 20){
		$jobs['errors']['backgroundJobs'] = array(
			'Message' => 'Total Running backgroundJobs : ' . count($jobs['backgroundJobs']),
		);
	}
	if($jobs['backgroundJobs']){
		$error = array();
		foreach($jobs['backgroundJobs'] as $job => $jobDetails){
			if(count($jobDetails) > 1){
				$error[$job] = $jobDetails;
			}
		}
		if($error){
			$jobs['errors']['jobRunningMulitpleTime'] = array(
				'Message' => 'Multiple Job Running in Background  : ' . count($error),
				'error' => $error
			);
		}
	}
	echo json_encode($jobs); 
} 
?> 