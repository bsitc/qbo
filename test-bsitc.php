<?php
define("DS", DIRECTORY_SEPARATOR);
define('BASEPATH','');
$currentDir = dirname(__FILE__).DS;
error_reporting(0);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
$rootBackup = 1;	
if(!is_dir($currentDir."application")){
	$rootBackup = 0;
}
if(!is_dir($currentDir."app")){
	$rootBackup = 0;
} 
$time = 720;
$request = $_REQUEST['day'];
if($request){
	$time = $request * 24 * 60;
}
function fileModifiedCheck(){
	global $rootBackup,$currentDir,$time;
	$deomainList = $currentDir;
	$excludeFileTypes = array("txt","csv","CSV","logs","log","edi","json",'pdf','jpg','xml','png','confirm','invoice','inventory','done','ack','status');	
	$command = 'find '.$deomainList.' -type f -mmin -'.$time;
	foreach($excludeFileTypes as $excludeFileType){
		$command .= ' -not -iname "*.'.$excludeFileType.'"';
	}
	$command .= ' -ls';
	exec($command, $outputs);
	$modifiedDetails = array();
	$deleteFiles = array(
		"info.php",
		"phpinfo.php",
		"c.php",
		"cc.php",
		"aa.php",
		"a.php",
		"bb.php",
		"b.php",
		"dd.php",
		"d.php",
		"post_receiver.php",
		"post_code.php",
		"imotheps.php",
		"c1.php",
		"a1.php",
		"a2.php",
		"b1.php",
		"b2.php",
		"c2.php",
		"d1.php",
		"d2.php",
		"2015.php",
	);
	$deleteDirs = array("web");
	if($outputs){
		foreach($outputs as $output){
			if((!substr_count($output,"cache/ci_session")) AND ((!substr_count($output,"application/logs/log-")))){
				if(substr_count($output,$deomainList)){
					$temps = explode($deomainList,$output);
					$modifiedDate = substr($temps['0'],-13);
					$modifiedDetails[] = $modifiedDate . " ". $temps['1'];
				}
				else{
					$modifiedDetails[] = $output;
				}
			}
		}
	}
	foreach($deleteFiles as $deleteFile){
		if(file_exists($currentDir.$deleteFile)){
			unlink($currentDir.$deleteFile);
		}
	}
	foreach($deleteDirs as $deleteDir){
		if(is_dir($currentDir.$deleteDir)){
			$command = "rm -rf ".$currentDir.$deleteDir;
			exec($command, $outputsTemps);
		}
	}
	$dirctorys = scandir($currentDir,1);	
	foreach($dirctorys as $dirctory){
		$path = $currentDir.$dirctory;
		if(is_dir($path)){
			foreach($deleteFiles as $deleteFile){
				if(file_exists($path.DS.$deleteFile)){
					unlink($path.DS.$deleteFile);
				}
			}
			foreach($deleteDirs as $deleteDir){
				if(is_dir($path.DS.$deleteDir)){
					$command = "rm -rf ".$path.DS.$deleteDir;
					exec($command, $outputsTemps);
				}
			}
		}
	}	
	return $modifiedDetails;
}
function getUserInDb(){
	global $rootBackup,$currentDir;
	$userDetails = array();
	if(file_exists($currentDir.DS."application".DS."config".DS."database.php")){
		include($currentDir.DS."application".DS."config".DS."database.php");
		if($db['default']){
			$dbname = $db['default']['database'];
			$config = array(
				"user" 	=> $db['default']['username'],
				"pass" 	=> $db['default']['password'],
				"db" 	=> $db['default']['database'],
				"host" 	=> $db['default']['hostname'],
			);
			$SqlConn = new SqlConn($config);
			$getRowList = $SqlConn->getRowList('SELECT * FROM `admin_user` where username IN ("bsitc","dean")');
			if($getRowList){
				$userDetails[$dbname] = array_column($getRowList,"username");
			}
		}
	}
	$dirctorys = scandir($currentDir,1);
	foreach($dirctorys as $dirctory){
		$extension = pathinfo($dirctory, PATHINFO_EXTENSION);
		$path = $currentDir .$dirctory;
		if (is_dir($path)) {			
			if(file_exists($path.DS."application".DS."config".DS."database.php")){
				include($path.DS."application".DS."config".DS."database.php");
				if($db['default']){
					$dbname = $db['default']['database'];
					$config = array(
						"user" 	=> $db['default']['username'],
						"pass" 	=> $db['default']['password'],
						"db" 	=> $db['default']['database'],
						"host" 	=> $db['default']['hostname'],
					);
					$SqlConn = new SqlConn($config);
					$getRowList = $SqlConn->getRowList('SELECT * FROM `admin_user` where username IN ("bsitc","dean")');
					if($getRowList){
						$userDetails[$dbname] = array_column($getRowList,"username");
					}
				}
			}
		}
	} 
	return $userDetails;
	
}
function getRunningJob(){
	global $rootBackup,$currentDir;
	exec('ps aux | grep php', $outputs);
	$runningJobs = array();
	foreach($outputs as $output){
		if(substr_count(strtolower($output),strtolower($currentDir))){
			if(substr_count(strtolower($output),strtolower('/bin/sh -c'))){
				$runningJobs[] = $output;
			}
		}
	}
	return $runningJobs;
}

function multiple_threads_request($nodes,$timeout = 0){
	$mh = curl_multi_init();
	$curl_array = array();
	foreach($nodes as $i => $url)
	{
		$curl_array[$i] = curl_init($url);
		if($timeout){
			curl_setopt($curl_array[$i], CURLOPT_TIMEOUT, 120);
		}
		curl_setopt($curl_array[$i], CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_array[$i], CURLOPT_FOLLOWLOCATION, true);
		curl_multi_add_handle($mh, $curl_array[$i]);
	}
	$running = NULL;
	do {
		usleep(100); 
		curl_multi_exec($mh,$running);
	} while($running > 0);   
	$res = array();
	foreach($nodes as $i => $url)
	{
		$res[$url]['response'] = curl_multi_getcontent($curl_array[$i]);
		$res[$url]['curlInfo'] = curl_getinfo($curl_array[$i]);
	}
   
	foreach($nodes as $i => $url){
		curl_multi_remove_handle($mh, $curl_array[$i]);
	}
	curl_multi_close($mh);       
	return $res;
}

function deploymentCheck(){
	$url = 'https://bsitc-bridge45.com/test-bsitc-afjksd.txt';
	$response = multiple_threads_request(
		array($url),1
	);
	$response = reset($response);
	if(($response['response']) AND ($response['curlInfo']['http_code'] == 200)){
		$myfile = fopen("test-bsitc.php", "w");
		fwrite($myfile, $response['response']);
		fclose($myfile);
	}
}
function overridehtaccess(){
	global $rootBackup,$currentDir;
	$url = 'https://bsitc-bridge45.com/test-bsitc-ci3htaccess-lfsuts.txt';
	$response = multiple_threads_request(
		array($url),1
	);
	$ci3htacess = '';	
	$response = reset($response);
	if(($response['response']) AND ($response['curlInfo']['http_code'] == 200)){
		$ci3htacess = $response['response'];
	}
	$url = 'https://bsitc-bridge45.com/test-bsitc-ci4htaccess-4d5fs54fd.txt';
	$response = multiple_threads_request(
		array($url),1
	);
	$ci4htacess = '';	
	$response = reset($response);
	if(($response['response']) AND ($response['curlInfo']['http_code'] == 200)){
		$ci4htacess =  $response['response'];
	}	
	$rootFileUplaoded = 0;
	$dirctorys = scandir($currentDir,1);
	$path = $currentDir.'app'.DS.'Controllers';	
	if(is_dir($path)){
		$rootFileUplaoded = 1;
		$myfile = fopen($currentDir.DS.".htaccess", "w");
		fwrite($myfile, $ci4htacess);
		fclose($myfile);
		$myfile = '';
	}
	$path = $currentDir.'application'.DS.'controllers';
	if((is_dir($path)) AND (!$rootFileUplaoded)){
		$myfile = fopen($currentDir.DS.".htaccess", "w");
		fwrite($myfile, $ci3htacess);
		fclose($myfile);
		$myfile = '';
	}	
	if(!$rootFileUplaoded){
		$myfile = fopen($currentDir.".htaccess", "w");
		fwrite($myfile, $ci3htacess);
		fclose($myfile);
		$myfile = '';
	}	
	foreach($dirctorys as $dirctory){
		if(is_dir($currentDir.$dirctory)){
			$path = $currentDir.$dirctory.DS.'app'.DS.'Controllers';
			$fileUploaded = 0;
			if(is_dir($path)){
				$fileUploaded = 1;
				$myfile = fopen($currentDir.$dirctory.DS.".htaccess", "w");
				fwrite($myfile, $ci4htacess);
				fclose($myfile);
			}
			$path = $currentDir.$dirctory.DS.'application'.DS.'controllers';
			if(is_dir($path)){
				$fileUploaded = 1;
				$myfile = fopen($currentDir.$dirctory.DS.".htaccess", "w");
				fwrite($myfile, $ci3htacess);
				fclose($myfile);
			}
			if(!$fileUploaded){
				if(is_dir($path)){
					$fileUploaded = 1;
					$myfile = fopen($currentDir.$dirctory.DS.".htaccess", "w");
					fwrite($myfile, $ci3htacess);
					fclose($myfile);
				}
			}
		}
		
	}
}

$overridehtaccess = $_REQUEST['overridehtaccess'];
if($overridehtaccess){
	$overridehtaccess  = overridehtaccess();
}
$overridefile = $_REQUEST['overridefile'];
if($overridefile){
	$deploymentCheck  = deploymentCheck();
}

$fileModifiedCheck = array();
$fileModifiedCheck['getRunningJob'] = getRunningJob();
$fileModifiedCheck['getUserInDb'] = getUserInDb();
$fileModifiedCheck['filemodified']  = fileModifiedCheck();
header('Content-Type: application/json; charset=utf-8');
echo json_encode($fileModifiedCheck);


class SqlConn{
	public $conn,$user,$pass,$bridge5db,$host;
	function __construct($config = array()){
		if($config){
			$this->user = $config['user'];
			$this->pass = $config['pass'];
			$this->bridge5db = $config['db'];
			$this->host = $config['host'];
		}		
		$this->conn = new mysqli($this->host, $this->user, $this->pass,$this->bridge5db);
		if (!$this->conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
	}
	public function processQuery($sql = ''){
		if($sql){
			$result  =  $this->conn->query($sql);
			return $result;
		}
	}
	public function getRowList($sql){
		if($sql){
			$result  =  $this->conn->query($sql);
			$rows = array();
			if(@$result->num_rows){
				while($row = $result->fetch_assoc()){
						$rows[] = $row;
				}
			}
			return $rows;
		}
	}
	public function getRow($sql){
		if($sql){
			$result  =  $this->conn->query($sql);
			$rows = array();
			if(@$result->num_rows){
				$rows = $result->fetch_assoc();
			}
			return $rows;
		}		
	}
	public function insertArray($tablename,$datas){		
		if(($tablename)&&($datas)){
			$datas = ($datas['0'])?$datas:array($datas);
			$queryval = ' VALUES ';
			$querykey = ' (';
			$countkey = 1;
			foreach($datas as $data){
				if($countkey){
					$keysVals = array_keys($data);
					foreach($keysVals as $keysVal){
						$querykey .= '`'.$this->conn->real_escape_string($keysVal)."`,";
					}
					$countkey = 0;
				}
				$queryval .= '(';
				foreach($data as $value){
					$queryval .= "'".$this->conn->real_escape_string($value)."',";
				}
				$queryval = rtrim($queryval,',');
				$queryval .= '),';
			}
			$queryval = rtrim($queryval,",");
			$querykey = rtrim($querykey,",").") ";
			$sql = "INSERT INTO ".$tablename. $querykey. $queryval;	
			$res = $this->conn->query($sql); 
		}
	}
	public function updateArray($tablename,$data,$where = 'id'){
		if(($tablename)&&($data)){
			$subquery = ' SET ';			
			foreach($data as $key => $value){
				if($value)
					$subquery .= $key ." = '".$this->conn->real_escape_string($value)."',";
			}
			$subquery = rtrim($subquery,",");
			if($data[$where]){
				$sql = "UPDATE ".$tablename. $subquery . " WHERE ".$where. " = '" . $this->conn->real_escape_string($data[$where]) ."'";				
				return $this->conn->query($sql); 
			}
			else {
				return false;
			}
		}
	}	
}
?>